<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UNPROV_INFO extends Model
{
    //
	protected $table ="UNPROV_INFO";

    public $timestamps = false;
}

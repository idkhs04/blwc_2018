<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CUST_CD extends Model
{
    //
	protected $table ="CUST_CD";

	public $timestamps = false;
}

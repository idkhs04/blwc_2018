<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ACCOUNT_CD extends Model
{
    //
	protected $table ="ACCOUNT_CD";
		
    public $timestamps = false;
	
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_GRP;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class accountgrpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{
		
		$ACCOUNT_GRP = ACCOUNT_GRP::where("CORP_MK", $this->getCorpId());
						
		return Datatables::of($ACCOUNT_GRP)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "ACCOUNT_GRP_CD"){
							$query->where("ACCOUNT_GRP_CD",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "ACCOUNT_GRP_NM"){
							$query->where("ACCOUNT_GRP_NM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("ACCOUNT_GRP_CD",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("ACCOUNT_GRP_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}

	public function index($cust)
	{
		return view("accountgrp.list",[ "cust" => $cust ] );
	}

	// 비용계정그룹 등록
    public function insert($cust)
    {

        $validator = Validator::make( Request::Input(), [
            'ACCOUNT_GRP_CD' => 'required|max:2,NOT_NULL',
            'ACCOUNT_GRP_NM' => 'required|max:20,NOT_NULL'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$count = DB::table("ACCOUNT_GRP")
			->where('CORP_MK', $this->getCorpId())
			->where('ACCOUNT_GRP_CD',  Request::Input("ACCOUNT_GRP_CD"))
			->count();

		if( $count > 0){
			return response()->json(['result' => 'fail', 'reason' => 'PrimaryKey', 'message' => '동일한 계정그룹코드가 존재합니다.'], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("ACCOUNT_GRP")->insert(
                [
                    'CORP_MK' => $this->getCorpId(),
                    'ACCOUNT_GRP_CD' => Request::input('ACCOUNT_GRP_CD'),
                    'ACCOUNT_GRP_NM' => Request::input('ACCOUNT_GRP_NM')
                ]
            );
        }
        catch(ValidationException $e){
            DB::rollback();

            return Redirect::to("/accountgrp/" . $cust)
                    ->withErrors($e->getErrors())
                    ->withInput();

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	// 선택된 비용계정그룹 정보
	public function edit($cust, $id)
	{	
		$model = DB::table("ACCOUNT_GRP")
            ->where("CORP_MK", $this->getCorpId())
            ->where("ACCOUNT_GRP_CD", $id)->first();

		return response()->json(['ACCOUNT_GRP_CD' => $model->ACCOUNT_GRP_CD, 'ACCOUNT_GRP_NM' => $model->ACCOUNT_GRP_NM]);
	}

	//선택된 비용계정그룹 정보 수정
    public function update($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'ACCOUNT_GRP_CD' => 'required|max:2,NOT_NULL',
            'ACCOUNT_GRP_NM' => 'required|max:20,NOT_NULL'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("ACCOUNT_GRP")
				->where("CORP_MK", $this->getCorpId())
                ->where("ACCOUNT_GRP_CD", Request::input('ACCOUNT_GRP_CD'))
                ->update(
					[
						"ACCOUNT_GRP_NM" => Request::input('ACCOUNT_GRP_NM')
					]
				);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//선택된 비용계정그룹 정보 삭제
    public function _delete()
    {


        if (Request::ajax()) {
            DB::table("ACCOUNT_GRP")
				->where("CORP_MK", $this->getCorpId())
                ->whereIn("ACCOUNT_GRP_CD", Request::get('cd'))
                ->delete();

            return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
        }
        return Response::json(['result' => 'failed']);
    }
/*
    public function create($cust)
    {
        $model = new CUST_GRP_INFO();
        return view("custGrp.Edit", ["model" => $model, "cust" => $cust]);
    }




    public function Excel()
    {
        // 컬럼을 지정해줘야 에러가 안남
        $model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
            ->where("CORP_MK", $this->getCorpId())
            ->get();

        Excel::create('거래처그룹', function ($excel) use ($model) {
            $excel->sheet('거래처그룹', function ($sheet) use ($model) {
                $sheet->fromArray($model);
            });
        })->export('xls');
    }

    public function Pdf()
    {

        $pdf = PDF::loadHTML("<h1>Test</h1>");

        // $pdf->save('myfile.pdf');
        return $pdf->stream();

    }
*/

}
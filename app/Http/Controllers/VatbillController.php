<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\AREA_CD;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use PHPExcel_Style_Border;

class VatbillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	// 매출세금계산서(국세청) 엑셀
	public function getExcelTaxBill(){
		
		return Excel::load('taxbill.xlsx', function($reader) {

			$TAX_CCOUNT_SALE = DB::table("TAX_ACCOUNT_M AS TAX_M")
								->select(
											"TAX_M.CUST_MK" , 
											DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
											DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
											DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
											DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
											"TAX_M.SEQ" , 
											"TAX_M.BUY_SALE_DIV" , 
											"TAX_M.RECPT_CLAIM_DIV" ,
											DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
											"TAX_M.REMARK" , 
											"TAX_D.MONTHS",
											"TAX_D.DAYS",
											"TAX_D.GOODS",
											"TAX_D.QTY",
											"TAX_D.UNCS",
											"TAX_D.SUPPLY_AMT",
											"TAX_D.TAX",
											"TAX_D.SRL_NO",
											"CUST.FRNM" , 
											"CUST.ETPR_NO" , 
											"CUST.RPST" , 
											"CUST.UPJONG",
											"CUST.UPTE",
											"CUST.JUMIN_NO" , 
											"CUST.FAX" , 
											"CUST.PHONE_NO" , 
											"CUST.ADDR1" , 
											"CUST.ADDR2" ,
											DB::raw("CORP.FRNM AS CORP_NM"),
											DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
											DB::raw("CORP.RPST AS CORP_RPST"),
											DB::raw("CORP.UPJONG AS CORP_UPJONG"),
											DB::raw("CORP.UPTE AS CORP_UPTE"),
											DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
											DB::raw("CORP.ADDR1 AS CORP_ADDR2")
								)->leftjoin("CUST_CD AS CUST", function($join){
										$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
										$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
								})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
										$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
										$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
										$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
										$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
								})->join("CORP_INFO AS CORP", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
								})->where("TAX_M.CORP_MK", $this->getCorpId())
								->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
								->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
								->where("TAX_M.SEQ" , Request::Input("SEQ"))->first();
			

			$TAX_CCOUNT_D = DB::table("TAX_ACCOUNT_M AS TAX_M")
								->select(
											"TAX_M.CUST_MK" , 
											DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS YY"),
											DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS MM"),
											DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS DD"),
											"TAX_M.SEQ" , 
											"TAX_M.REMARK" , 
											"TAX_D.MONTHS",
											"TAX_D.DAYS",
											"TAX_D.GOODS",
											"TAX_D.QTY",
											"TAX_D.UNCS",
											"TAX_D.SUPPLY_AMT",
											"TAX_D.TAX",
											"TAX_D.SRL_NO"
								)->join("TAX_ACCOUNT_D AS TAX_D", function($join){
										$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
										$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
										$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
										$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
								})
								->where("TAX_M.CORP_MK", $this->getCorpId())
								->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))->get();


			// 일련번호
			$M_SRL_NO = DB::table("DOC_NUMBER")
						->select("SRL_NO")
						->where("CORP_MK", $this->getCorpId())
						->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
						->where("MONTH", $TAX_CCOUNT_SALE->M_MM );
			
			$SRL_NO = 0;
			// 일련번호가 없으면 생성해줌
			if( count($M_SRL_NO->get()) == 0 ){
				DB::table("DOC_NUMBER")->insert(
						[
							 "CORP_MK"	=> $this->getCorpId()
							,"YEAR"		=> $TAX_CCOUNT_SALE->M_YY 
							,"MONTH"	=> $TAX_CCOUNT_SALE->M_MM
							,"SRL_NO"	=> 1
						]
				);

				$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM."01";

			}else{
				// 일련번호 최대값 가져오고 update(srl_no +1) 
				$M_SRL_NO = DB::table("DOC_NUMBER")
					->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM )->first();
				
				$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

				$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM.$M_SRL_NO->NEW_SRL_NO;

				DB::table("DOC_NUMBER")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM )
					->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);

			}

			$strAMT = strval((int)$TAX_CCOUNT_SALE->SUPPLY_AMT);
			$strSum = [];

			// 초기화
			for($i=0; $i<11; $i++){
				$strSum[$i] = 0;
			}
			$index = 10;
			for( $i=strlen($strAMT)-1; $i >= 0 ; $i--) { 
				$strSum[$index--] = $strAMT[$i];	
			}
			
			// 여기서부터 엑셀 저장

			$results = $reader->all();
			$sheet = $reader->getSheetByName('세금계산서');

			// 일련번호 
			$sheet->setCellValue('AA4', $SRL_NO);
			
			// 공급자 등록번호 
			$sheet->setCellValue('F5', $TAX_CCOUNT_SALE->CORP_ETPR_NO);
			// 공급자 상호명
			$sheet->setCellValue('F7', $TAX_CCOUNT_SALE->CORP_NM);
			// 공급자 대표자명
			$sheet->setCellValue('M7', $TAX_CCOUNT_SALE->CORP_RPST);
			// 공급자 주소
			$sheet->setCellValue('F9', $TAX_CCOUNT_SALE->CORP_ADDR1." ".$TAX_CCOUNT_SALE->CORP_ADDR2 );
			// 업태 
			$sheet->setCellValue('F11', $TAX_CCOUNT_SALE->CORP_UPTE);
			// 종목
			$sheet->setCellValue('M11', $TAX_CCOUNT_SALE->CORP_UPJONG);

			// 공급받는자 등록번호 
			$sheet->setCellValue('V5', $TAX_CCOUNT_SALE->ETPR_NO);
			// 공급받는자 상호명
			$sheet->setCellValue('V7', $TAX_CCOUNT_SALE->FRNM);
			// 공급받는자 대표자명
			$sheet->setCellValue('AC7', $TAX_CCOUNT_SALE->RPST);
			// 공급받는자 주소
			$sheet->setCellValue('V9', $TAX_CCOUNT_SALE->ADDR1." ".$TAX_CCOUNT_SALE->ADDR2 );
			// 공급받는자 업태 
			$sheet->setCellValue('V11', $TAX_CCOUNT_SALE->UPTE);
			// 공급받는자 종목
			$sheet->setCellValue('AC11', $TAX_CCOUNT_SALE->UPJONG);

			// 작성 : 년 / 월 /일
			$sheet->setCellValue('B15', $TAX_CCOUNT_SALE->M_YY);
			$sheet->setCellValue('D15', (int)$TAX_CCOUNT_SALE->M_MM >= 10 ? $TAX_CCOUNT_SALE->M_MM : "0".$TAX_CCOUNT_SALE->M_MM );
			$sheet->setCellValue('E15', (int)$TAX_CCOUNT_SALE->M_DD >= 10 ? $TAX_CCOUNT_SALE->M_DD : "0".$TAX_CCOUNT_SALE->M_DD);

			// 공란수
			$sheet->setCellValue('F15', 11-strlen($strAMT) );
			
			// 공급가액
			$ch = 'H';
			$cell = "";
			foreach($strSum as $item){
				$cell = $ch."15";
				$sheet->setCellValue($cell, $item);
				$ch++;
			}
			
			// 품목
			$cell = "";
			$row = 17;
			foreach($TAX_CCOUNT_D as $list){
				// 월
				$cell = 'B'.$row;
				$sheet->setCellValue($cell, (int)$list->MM >= 10 ? $list->MM : "0".$list->MM);
				//일
				$cell = 'C'.$row;
				$sheet->setCellValue($cell, $list->DD);
				// 품목
				$cell = 'D'.$row;
				$sheet->setCellValue($cell, $list->GOODS);
				// 수량

				$cell = 'N'.$row;
				$sheet->setCellValue($cell, $list->QTY);
				// 단가
				$cell = 'Q'.$row;
				$sheet->setCellValue($cell, $list->UNCS);
				// 공급가액
				$cell = 'U'.$row;
				$sheet->setCellValue($cell, $list->SUPPLY_AMT);
				//++$ch; // 세액 건너띔
				// 비고
				$cell = 'AD'.$row;
				$sheet->setCellValue($cell, $list->REMARK);
				
				$row++;
			}
			
			// 합계금액
			$sheet->setCellValue('B22', $TAX_CCOUNT_SALE->SUPPLY_AMT );
			
			
			$sheet->getStyle('R5')->applyFromArray(array(
				'borders' => array( 'left' => array( 'style' => PHPExcel_Style_Border::BORDER_DOTTED  , 'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AA4')->applyFromArray(array(
				'borders' => array( 'right' => array( 'style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('V5')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));
			
			$sheet->getStyle('AC7')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('V9')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AC11')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AC13')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AC14')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AD16')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AD17')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD18')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD19')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD20')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AA21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AE21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AG21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			


		})->stream('xlsx');;

		//return $data->stream('xlsx');
		
		
	
	}


	public function indexBuy($tax)
	{
		return view("account.tax_buy",[ "tax" => $tax ] );
	}

	public function detailBuy($tax)
	{
		$TAX_CCOUNT_M = DB::table("CUST_CD AS CUST")
						->select(
									"CUST.FRNM" , 
									"CUST.ETPR_NO" , 
									"CUST.RPST" , 
									"CUST.JUMIN_NO" , 
									"CUST.FAX" , 
									"CUST.PHONE_NO" , 
									"CUST.ADDR1" , 
									"CUST.ADDR2" ,
									"CUST.UPJONG" ,
									"CUST.UPTE" 
						)
						->where("CUST.CORP_MK", $this->getCorpId())
						->where("CUST.CUST_MK", Request::segment(4))->first();

		//d($TAX_CCOUNT_M);
		return view("account.tax_buyDetail",[ "tax" => $tax, "model" => $TAX_CCOUNT_M ] );
	}

	// DataTable : 매출세금계산서 목록
	public function getTaxAccountList(){

		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"));
			
		return Datatables::of($TAX_CCOUNT_M)
					
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("TAX_M.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}
				}
		)->make(true);
	
	}

	// 매출 계산서 수정 불러오기
	public function getTaxSaleItem(){
	
		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
							->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
							->where("TAX_M.SEQ", Request::Input("SEQ"))
							->where("TAX_M.BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
							->where("TAX_D.SRL_NO", Request::Input("SRL_NO"))->first();
		
		//return response()->json($TAX_CCOUNT_M);
		return $TAX_CCOUNT_M;							
	}

	// 매입계산서 자동생성
	public function getTaxBuyGeneration(){

		$STOCK_INFO_SUM	 = DB::table("STOCK_INFO AS ST")
							->select( DB::raw( "SUM(QTY * UNCS - INPUT_DISCOUNT) AS SUM")	)
							->where("CORP_MK", $this->getCorpId())
							->where(DB::raw("DATEPART(YY, INPUT_DATE)"), Request::Input("INPUT_YEAR"))
							->where(DB::raw("DATEPART(MM, INPUT_DATE)"), Request::Input("INPUT_MONTH"))
							->where("CUST_MK", Request::Input("CUST_MK"))
							->first()->SUM;
			
		return response()->json(
									[ 
									  'SUM' => $STOCK_INFO_SUM
									, 'WRITE_DATE'=> $this->endday(Request::Input("INPUT_YEAR"), Request::Input("INPUT_MONTH")) 
									, 'GOODS' => '상품'
									, 'REMARK' => ''
									]
								);
	
	}

	// 매출계산서 자동생성
	public function getTaxSaleGeneration(){

		$STOCK_INFO	 = DB::table("SALE_INFO_M")
							->select( DB::raw( "SUM(TOTAL_SALE_AMT) AS SUM")	)
							->where("CORP_MK", $this->getCorpId())
							->where(DB::raw("DATEPART(YY, WRITE_DATE)"), Request::Input("INPUT_YEAR"))
							->where(DB::raw("DATEPART(MM, WRITE_DATE)"), Request::Input("INPUT_MONTH"))
							->where("CUST_MK", Request::Input("CUST_MK"))
							->groupBy(DB::raw("DATEPART(MM, WRITE_DATE)"))
							->first();
		
		return response()->json(
									[ 
									  'SUM' => $STOCK_INFO == null ? 0 : $STOCK_INFO->SUM
									, 'WRITE_DATE'=> $this->endday(Request::Input("INPUT_YEAR"), Request::Input("INPUT_MONTH")) 
									, 'GOODS' => '활어'
									, 'REMARK' => ''
									]
								);
	
	}

	// 매입계산서 저장
	public function setTaxBuy(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'numeric'
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );
				DB::table("TAX_ACCOUNT_M")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'I'
								,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
								,"CUST_MK"			=> Request::Input("CUST_MK")
								,"REMARK"			=> Request::Input("REAMRK")
							]
						);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );
				DB::table("TAX_ACCOUNT_D")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'I'
								,"SRL_NO"			=> (int)$TAX_ACCOUNT_MAX_SRL + 1
								,"MONTHS"			=> Request::Input("MONTHS")
								,"DAYS"				=> Request::Input("DAYS")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	private function endday($ddlYear, $ddlMonth)
	{
		$mon;
		$monthDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];


		$yy = (int)($ddlYear);
		$mm = (int)($ddlMonth);

		if ((($yy % 4 == 0) && ($yy % 100 != 0)) || ($yy % 400 == 0)){ 
			$monthDays[1] = 29; 
		}

		$nDays = $monthDays[$mm - 1];

		
		if (strlen($mm) < 2)
		{
			$mon = "0".$mm;
		}
		else { 
			$mon = strlen($mm); 
		}

		return $yy."-".$mon."-".$nDays;

	}

	

	public function indexSale($tax)
	{
		$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")
					->where("CORP_MK", $this->getCorpId())->get();

		return view("account.tax_sale",[ "tax" => $tax, "CUST_GRP_INFO" => $CUST_GRP_INFO ] );
	}

	public function getIndexSaleCust()
	{
		$CUST_CD = DB::table("CUST_CD AS CC")
							->select(
										  "CC.CUST_MK"
										, "CC.CORP_MK"
										, "CC.CUST_GRP_CD"
										, "CC.CORP_DIV"
										, "CC.FRNM"
										, "CC.RPST"
										, "CC.RANK"
										, DB::raw(" CASE WHEN CC.RANK ='SU' THEN '우수'
										     WHEN CC.RANK ='GE' THEN '일반'
										     WHEN CC.RANK ='BD' THEN '불량' END AS RANK_NM ")

										, "CC.PHONE_NO"
										, "CG.CUST_GRP_NM"
										, "CC.AREA_CD"
										, "AC.AREA_NM"
							)->leftjoin("CUST_GRP_INFO AS CG", function($join){
									$join->on("CC.CORP_MK", "=", "CG.CORP_MK");
									$join->on("CC.CUST_GRP_CD", "=", "CG.CUST_GRP_CD");
							})->leftjoin("AREA_CD AS AC", function($join){
									$join->on("CC.AREA_CD", "=", "AC.AREA_CD ");
							})
							->where("CC.CORP_MK", $this->getCorpId())
							->where("CC.CORP_DIV", 'S');

		return Datatables::of($CUST_CD)
					
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("CC.FRNM", 'LIKE', '%'.Request::Input("textSearch").'%' );
					}
					if( Request::Has('srtCondition') && Request::Input('srtCondition') != 'ALL'){
						$query->where("CC.CUST_GRP_CD", Request::Input("srtCondition") );
					}
				}
		)->make(true);
	
	}

	// PDF : 매출세금계산서 선택 출력
	public function getPdfSaleTax(){
		
		$TAX_CCOUNT_SALE = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										DB::raw("CORP.FRNM AS CORP_NM"),
										DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR2")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
							->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
							->where("TAX_M.SEQ" , Request::Input("SEQ"))->first();
		

		$TAX_CCOUNT_D = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS DD"),
										"TAX_M.SEQ" , 
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO"
							)->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))->get();


		// 일련번호
		$M_SRL_NO = DB::table("DOC_NUMBER")
					->select("SRL_NO")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM );
		
		$SRL_NO = 0;
		// 일련번호가 없으면 생성해줌
		if( count($M_SRL_NO->get()) == 0 ){
			DB::table("DOC_NUMBER")->insert(
					[
						 "CORP_MK"	=> $this->getCorpId()
						,"YEAR"		=> $TAX_CCOUNT_SALE->M_YY 
						,"MONTH"	=> $TAX_CCOUNT_SALE->M_MM
						,"SRL_NO"	=> 1
					]
			);

			$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM."01";

		}else{
			// 일련번호 최대값 가져오고 update(srl_no +1) 
			$M_SRL_NO = DB::table("DOC_NUMBER")
				->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
				->where("MONTH", $TAX_CCOUNT_SALE->M_MM )->first();
			
			$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

			$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM.$M_SRL_NO->NEW_SRL_NO;

			DB::table("DOC_NUMBER")
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
				->where("MONTH", $TAX_CCOUNT_SALE->M_MM )
				->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);

		}

		$strAMT = strval((int)$TAX_CCOUNT_SALE->SUPPLY_AMT);
		$strSum = [];

		// 초기화
		for($i=0; $i<11; $i++){
			$strSum[$i] = 0;
		}
		$index = 10;
		for( $i=strlen($strAMT)-1; $i >= 0 ; $i--) { 
			$strSum[$index--] = $strAMT[$i];
			
		}
		

		$viewBlade = "account.pdf_TaxSale";
		if( Request::Input("WHITE") == "1"){
			$viewBlade = "account.pdf_TaxSaleW";
		}

		$pdf = PDF::loadView($viewBlade, 
								[
									'model'			=> $TAX_CCOUNT_SALE,
									'mlist'			=> $TAX_CCOUNT_D,
									'title'			=> '세금계산서',
									'srl_no'		=> $SRL_NO,
									'cntNull'		=> 11-strlen($strAMT),
									'arrStrSum'		=> $strSum,
									
								]
							);
		
		return $pdf->stream();

	}

	public function detailSale($tax){
		
		// 해당거래처정보 가져오기
		$TAX_CCOUNT_M = DB::table("CUST_CD AS CUST")
						->select(
									"CUST.FRNM" , 
									"CUST.ETPR_NO" , 
									"CUST.RPST" , 
									"CUST.JUMIN_NO" , 
									"CUST.FAX" , 
									"CUST.PHONE_NO" , 
									"CUST.ADDR1" , 
									"CUST.ADDR2" ,
									"CUST.UPJONG" ,
									"CUST.UPTE" 
						)
						->where("CUST.CORP_MK", $this->getCorpId())
						->where("CUST.CUST_MK", Request::segment(4))->first();

		//d($TAX_CCOUNT_M);
		return view("account.tax_SaleDetail",[ "tax" => $tax, "model" => $TAX_CCOUNT_M ] );
	
	}
					
	// DATATABLE : 매출계산서 목록 
	public function getTaxSaleAccountList(){

		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"));
			
		return Datatables::of($TAX_CCOUNT_M)
					
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("TAX_M.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}
				}
		)->make(true);
	
	}

	// JSON : 매출계산서 등록
	public function setTaxSale(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'numeric'
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );
				DB::table("TAX_ACCOUNT_M")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'O'
								,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
								,"CUST_MK"			=> Request::Input("CUST_MK")
								,"REMARK"			=> Request::Input("REAMRK")
							]
						);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );
				DB::table("TAX_ACCOUNT_D")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'O'
								,"SRL_NO"			=> (int)$TAX_ACCOUNT_MAX_SRL + 1
								,"MONTHS"			=> Request::Input("MONTHS")
								,"DAYS"				=> Request::Input("DAYS")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	// View : 세금계산서 합계
	public function index($tax)
	{
		$CUST_GRP = DB::table("CUST_GRP_INFO")
					->select("CUST_GRP_CD" ,"CUST_GRP_NM")
					->where("CORP_MK", $this->getCorpId())->get();
		
		return view("account.list",[ "tax" => $tax, "custGrp" => $CUST_GRP] );
	}

	// DataTable : 세금계산서 거래처별 합계 목록
	public function getTaxCustSumList(){
	
		$TAX_SUM_CUST = DB::table(DB::raw("
							 ( SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, M.CUST_MK
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
							    FROM TAX_ACCOUNT_D AS D 
									 INNER JOIN TAX_ACCOUNT_M AS M 
											  ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
							   WHERE M.CORP_MK = '".$this->getCorpId()."' 
								 AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
								 AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
								GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T")
					)->leftjoin("CUST_CD AS C", function($join){
						$join->on("T.CORP_MK", "=", "C.CORP_MK");
						$join->on("T.CUST_MK", "=", "C.CUST_MK");
					})->select(
						  "T.CORP_MK"
						, "T.CUST_MK"
						, "C.FRNM"
						, DB::raw("SUM(T.INPUT) AS INPUT")
						, DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						, DB::raw("SUM(T.OUT) AS OUT")
						, DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
					)->where(function($query){
					
						if( Request::has("srtCondition") && Request::Input("srtCondition") !== "ALL"){
							$query->where("C.CUST_GRP_CD", Request::Input("srtCondition"));
						}
						if( Request::has("textSearch") && Request::Input("textSearch") !== ""){
							$query->where("C.FRNM", "LIKE", "%".Request::Input("textSearch")."%");
						}
					})->groupBy("T.CORP_MK", "T.CUST_MK", "C.FRNM");
		
		return Datatables::of($TAX_SUM_CUST)->make(true);		
	
	}

	// DataTable : 세금계산서 매출계산서 합계 목록
	public function getTaxSaleSumList(){
	
		$TAX_SUM_SALE = DB::table(DB::raw("
							 (SELECT T.CORP_MK
								   , T.CUST_MK
								    , C.FRNM
								    , SUM(T.OUT) AS OUT
								    , SUM(T.OUT_CNT) AS OUT_CNT
								    , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								FROM CUST_CD AS C 
									 INNER JOIN (SELECT D.BUY_SALE_DIV
													  , M.CORP_MK
													  , M.CUST_MK
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT
												   FROM TAX_ACCOUNT_D AS D 
												  	  INNER JOIN TAX_ACCOUNT_M AS M 
																			ON D.CORP_MK = M.CORP_MK 
																			 AND D.WRITE_DATE = M.WRITE_DATE 
																			 AND D.SEQ = M.SEQ 
																			 AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
															 WHERE M.CORP_MK = '".$this->getCorpId()."' 
															   AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
															   AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."' 
												   GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
													ON C.CORP_MK = T.CORP_MK 
													 AND C.CUST_MK = T.CUST_MK 
												GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS sale")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "OUT"
						, "OUT_CNT"
						, "ETPR_NO"
					)->where("OUT_CNT",">", 0);

		
		return Datatables::of($TAX_SUM_SALE)->make(true);		
	
	}

	// DataTable : 세금계산서 매입계산서 합계 목록
	public function getTaxInputSumList(){
	
		$TAX_SUM_STOCK = DB::table(DB::raw("
							   (SELECT T.CORP_MK
									 , T.CUST_MK
									 , C.FRNM
									 , SUM(T.INPUT) AS INPUT
									 , SUM(T.INPUT_CNT) AS INPUT_CNT
									 , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								 FROM CUST_CD AS C 
									  INNER JOIN (SELECT D.BUY_SALE_DIV
														 , M.CORP_MK
														  , M.CUST_MK
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT 
													FROM TAX_ACCOUNT_D AS D 
														 INNER JOIN TAX_ACCOUNT_M AS M 
																 ON D.CORP_MK = M.CORP_MK 
																AND D.WRITE_DATE = M.WRITE_DATE 
																AND D.SEQ = M.SEQ 
																AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
														  WHERE M.CORP_MK = '".$this->getCorpId()."'
															AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
															AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
														  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
										   ON C.CORP_MK = T.CORP_MK 
										  AND C.CUST_MK = T.CUST_MK 
										  GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS buy")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "INPUT"
						, "INPUT_CNT"
						, "ETPR_NO"
					)->where("INPUT",">", 0);

		
		return Datatables::of($TAX_SUM_STOCK)->make(true);		
	
	}

	// DataTable : 세금계산서 연간 합계 목록
	public function getTaxYearSumList(){
	
		$TAX_SUM_YEAR = DB::table(DB::raw("
							  (SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, MONTH(M.WRITE_DATE) AS MONTHS
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
								 FROM TAX_ACCOUNT_D AS D 
									  INNER JOIN (  SELECT  A.CORP_MK
														  , A.WRITE_DATE
														  , A.SEQ
														  , A.BUY_SALE_DIV
														  , A.RECPT_CLAIM_DIV
														  , A.CUST_MK
														  , A.ETPR_NO
														  , A.FRNM
														  , A.NAME
														  , A.TAX_ADDR1
														  , A.TAX_ADDR2
														  , A.UPTE
														  , A.UPJONG
														  , A.SUM_AMT
														  , A.CASH_AMT
														  , A.CHECK_AMT
														  , A.CARD_AMT
														  , A.UNCL_AMT
														  , A.ACCOUNT_DIV
														  , A.REMARK 
													 FROM TAX_ACCOUNT_M AS A 
														  INNER JOIN CUST_CD AS B 
																  ON A.CUST_MK = B.CUST_MK 
																 AND A.CORP_MK = B.CORP_MK) AS M 
												ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
								WHERE M.CORP_MK = '".$this->getCorpId()."' 
								  AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
								  AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
						  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, MONTH(M.WRITE_DATE)) AS T "
					))->join("CORP_INFO", function($join){
							$join->on("T.CORP_MK", "=", "CORP_INFO.CORP_MK");
					})->leftjoin("DECLARATION AS D", function($join){
							$join->on("T.CORP_MK", "=", "D.corp_mk");
							$join->on("T.MONTHS" , "=", DB::raw("MONTH(D.dec_month) "));
					})->select(
						    "T.CORP_MK"
						  , "T.MONTHS"
						  , DB::raw("SUM(T.INPUT) AS INPUT")
						  , DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						  , DB::raw("SUM(T.OUT) AS OUT")
						  , DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
						  , "D.dec_money"
						  , DB::raw("ROUND(SUM(T.INPUT) / D.dec_money * 100, 0) AS saleVSbuy")
						  , DB::raw("D.dec_money - SUM(T.OUT) AS overSale")
						  , DB::raw("ROUND(D.dec_money * (MAX(D.dec_percent) * 0.01), 0) - SUM(T.INPUT) AS overBuy")
						  , "D.dec_percent"
						  , "CORP_INFO.FRNM"
					)->groupBy("T.CORP_MK", "T.MONTHS", "D.dec_money", "D.dec_percent", "CORP_INFO.FRNM");

		
		return Datatables::of($TAX_SUM_YEAR)->make(true);		
	
	}

	// PDF : 1. 매출계산서 합계 출력
	public function PdfsaleSum(){
		
		
		$TAX_SUM_SALE = DB::table(DB::raw("

				(SELECT T.CORP_MK
					   , T.CUST_MK
						, C.FRNM
						, SUM(T.OUT) AS OUT
						, SUM(T.OUT_CNT) AS OUT_CNT
						, STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
					FROM CUST_CD AS C 
						 INNER JOIN (SELECT D.BUY_SALE_DIV
										  , M.CORP_MK
										  , M.CUST_MK
										  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
										  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT
									   FROM TAX_ACCOUNT_D AS D 
										  INNER JOIN TAX_ACCOUNT_M AS M 
																ON D.CORP_MK = M.CORP_MK 
																 AND D.WRITE_DATE = M.WRITE_DATE 
																 AND D.SEQ = M.SEQ 
																 AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
												 WHERE M.CORP_MK = '".$this->getCorpId()."' 
												   AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
												   AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."' 
									   GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
										ON C.CORP_MK = T.CORP_MK 
										 AND C.CUST_MK = T.CUST_MK 
									GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS sale")
				)->select(
					  "CORP_MK"
					, "CUST_MK"
					, "FRNM"
					, "OUT"
					, "OUT_CNT"
					, "ETPR_NO"
				)->where("OUT_CNT",">", 0)->get();



		$sumSale		= 0; // 매출 금액 합계
		$saumSaleQty	= 0; // 매출 매수 합계
	
		foreach($TAX_SUM_SALE as $info){
			$sumSale		+= $info->OUT;
			$saumSaleQty	+= $info->OUT_CNT;
		}

		$pdf = PDF::loadView("account.pdf_SumSale", 
								[
									'model'			=> $TAX_SUM_SALE,
									'title'			=> "매출계산서 합계",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'start'			=> Request::Input('WRITE_DATE_START'),
									'end'			=> Request::Input('WRITE_DATE_END'),
									'sumSale'		=> $sumSale,
									'saumSaleQty'	=> $saumSaleQty,
								]
							);

		return $pdf->stream();
	
	}

	// PDF : 2. 매입계산서 합계 출력
	public function PdfbuySum(){
		
		
		$TAX_SUM_STOCK = DB::table(DB::raw("
							   (SELECT T.CORP_MK
									 , T.CUST_MK
									 , C.FRNM
									 , SUM(T.INPUT) AS INPUT
									 , SUM(T.INPUT_CNT) AS INPUT_CNT
									 , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								 FROM CUST_CD AS C 
									  INNER JOIN (SELECT D.BUY_SALE_DIV
														 , M.CORP_MK
														  , M.CUST_MK
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT 
													FROM TAX_ACCOUNT_D AS D 
														 INNER JOIN TAX_ACCOUNT_M AS M 
																 ON D.CORP_MK = M.CORP_MK 
																AND D.WRITE_DATE = M.WRITE_DATE 
																AND D.SEQ = M.SEQ 
																AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
														  WHERE M.CORP_MK = '".$this->getCorpId()."'
															AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
															AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
														  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
										   ON C.CORP_MK = T.CORP_MK 
										  AND C.CUST_MK = T.CUST_MK 
										  GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS buy")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "INPUT"
						, "INPUT_CNT"
						, "ETPR_NO"
					)->where("INPUT",">", 0)->get();



		$sumInput		= 0; // 매입 금액 합계
		$sumInputQty	= 0; // 매입 매수 합계
	
		foreach($TAX_SUM_STOCK as $info){
			$sumInput		+= $info->INPUT;
			$sumInputQty	+= $info->INPUT_CNT;
		}

		$pdf = PDF::loadView("account.pdf_SumBuy", 
								[
									'model'			=> $TAX_SUM_STOCK,
									'title'			=> "매입계산서 합계",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'start'			=> Request::Input('WRITE_DATE_START'),
									'end'			=> Request::Input('WRITE_DATE_END'),
									'sumInput'		=> $sumInput,
									'sumInputQty'	=> $sumInputQty,
								]
							);

		return $pdf->stream();
	
	}
	
	// PDF : 3. 연간합계 계산서 총괄표
	public function PdfyearSum(){
	
		$TAX_SUM_YEAR = DB::table(DB::raw("
							  (SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, MONTH(M.WRITE_DATE) AS MONTHS
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
								 FROM TAX_ACCOUNT_D AS D 
									  INNER JOIN (  SELECT  A.CORP_MK
														  , A.WRITE_DATE
														  , A.SEQ
														  , A.BUY_SALE_DIV
														  , A.RECPT_CLAIM_DIV
														  , A.CUST_MK
														  , A.ETPR_NO
														  , A.FRNM
														  , A.NAME
														  , A.TAX_ADDR1
														  , A.TAX_ADDR2
														  , A.UPTE
														  , A.UPJONG
														  , A.SUM_AMT
														  , A.CASH_AMT
														  , A.CHECK_AMT
														  , A.CARD_AMT
														  , A.UNCL_AMT
														  , A.ACCOUNT_DIV
														  , A.REMARK 
													 FROM TAX_ACCOUNT_M AS A 
														  INNER JOIN CUST_CD AS B 
																  ON A.CUST_MK = B.CUST_MK 
																 AND A.CORP_MK = B.CORP_MK) AS M 
												ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
								WHERE M.CORP_MK = '".$this->getCorpId()."' 
								  AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
								  AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
						  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, MONTH(M.WRITE_DATE)) AS T "
					))->join("CORP_INFO", function($join){
							$join->on("T.CORP_MK", "=", "CORP_INFO.CORP_MK");
					})->leftjoin("DECLARATION AS D", function($join){
							$join->on("T.CORP_MK", "=", "D.corp_mk");
							$join->on("T.MONTHS" , "=", DB::raw("MONTH(D.dec_month) "));
					})->select(
						    "T.CORP_MK"
						  , "T.MONTHS"
						  , DB::raw("SUM(T.INPUT) AS INPUT")
						  , DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						  , DB::raw("SUM(T.OUT) AS OUT")
						  , DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
						  , "D.dec_money"
						  , DB::raw("ROUND(SUM(T.INPUT) / D.dec_money * 100, 0) AS saleVSbuy")
						  , DB::raw("D.dec_money - SUM(T.OUT) AS overSale")
						  , DB::raw("ROUND(D.dec_money * (MAX(D.dec_percent) * 0.01), 0) - SUM(T.INPUT) AS overBuy")
						  , "D.dec_percent"
						  , "CORP_INFO.FRNM"
					)->groupBy("T.CORP_MK", "T.MONTHS", "D.dec_money", "D.dec_percent", "CORP_INFO.FRNM")->get();



		$sumInput		= 0; // 매입 금액 합계
		$sumInputQty	= 0; // 매입 매수 합계
		$sumOut			= 0;
		$sumOutQty		= 0;
	
		foreach($TAX_SUM_YEAR as $info){

			$sumInput		+= $info->INPUT;
			$sumInputQty	+= $info->INPUT_CNT;
			$sumOut			+= $info->OUT;
			$sumOutQty		+= $info->OUT_CNT;
		}


		$pdf = PDF::loadView("account.pdf_SumYear", 
								[
									'model'			=> $TAX_SUM_YEAR,
									'title'			=> "계산서 총괄표",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'sumInput'		=> $sumInput,
									'sumInputQty'	=> $sumInputQty,
									'sumOut'		=> $sumOut,
									'sumOutQty'		=> $sumOutQty,
								]
							);

		return $pdf->stream();
	}

	public function setChangeDeclaration(){


		$validator = Validator::make( Request::Input(), [

			'YEAR'			=> 'required|max:4',
			'PERCENTER'		=> 'required|numeric',

		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				DB::table("DECLARATION")
						->where("CORP_MK", $this->getCorpId())
						->where(DB::raw("year(dec_month)"), Request::Input("YEAR"))
						->update(
							[
								"dec_percent"		=> Request::Input("PERCENTER")
							]
						);
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	// 매입계산서_M 최대 SEQ 값 조회
	private function getTaxAccountMSeq($pCorpId, $pWRITE_DATE){
		
		return $max_seq = DB::table("TAX_ACCOUNT_M")
									->select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $pCorpId)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
				
	}

	// 매입계산서_D 최대 SRL_NO 값 조회
	private function getTaxAccountDSrlNo($pCorpId, $pWRITE_DATE, $pSEQ){
		
		return $max_seq = DB::table("TAX_ACCOUNT_D")
									->select( DB::raw('ISNULL(max("SRL_NO"), 0) AS SRL_NO') )
									->where ("CORP_MK", $pCorpId)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->where ("SEQ", $pSEQ)
									->first()->SRL_NO;
				
	}

}
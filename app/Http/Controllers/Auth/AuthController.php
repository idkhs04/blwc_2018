<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Redirect;
use Zizaco\Entrust\EntrustPermission;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;
use DB;
use Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
	
    use AuthenticatesAndRegistersUsers, ThrottlesLogins, EntrustUserTrait;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $username = 'MEM_ID';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:MEM_INFO,MEM_ID',
           // 'email' => 'required|email|max:255|unique:users',
           // 'password' => 'required|min:1|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        return User::create([
            'MEM_ID' => $data['name'],
           // 'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

	 
	public function logout()
	{
		
		Auth::logout();
		Session::flush();
		return redirect('/member/login');
	}

	public function getLogin() {
		
        return View('auth.login',[

        ]);
    }

	public function postLogin() {

	/*	DB::table("users")
		->insert([
			'name' =>Request::input('id'), 
			'email' => "email", 
			'remember_token' => "token",
			'created_at' =>date('Y-m-d H:i:s'),
			'updated_at' =>date('Y-m-d H:i:s'),
            'password' => bcrypt(Request::input('password')),
        ]);
		DB::table("MEM_INFO")
		->where("MEM_ID","=","kdbin2011")
		->update([
            'remember_token' => Request::input('_token'),
        ]);
	*/
		if (
			Auth::attempt(
				[	
						'MEM_ID' => Request::input('id'), 
						'password' => Request::input('password')
				]/*,
				Request::input('remember') !== null ? true : false*/
			)  //test1234/1234
		) {
			
			$CORP_INFO = DB::table("CORP_INFO")
							->where('CORP_MK', Auth::user()->CORP_MK)
							->first();
				

			if( $CORP_INFO->ACTIVE == "0"){
				Auth::logout();
				Session::flush();
				return redirect('member/login')
				->withErrors([
					'use_yn' => '사용자는 사용권한이 없습니다.',
				])
				->withinput();
			}
			
			Session::put('CORP_MK', Auth::user()->CORP_MK);
			Session::put('MEM_ID', Auth::user()->MEM_ID);
			return redirect('/');
		}else{
			return redirect('member/login')
				->withErrors([
					'id' => '아이디 또는 비밀번호를 확인해 주세요.',
				])
				->withinput();
		}
	}
}

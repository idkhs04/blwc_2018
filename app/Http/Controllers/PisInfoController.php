<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\Http\Requests;
use App\Fileentry;
use App\PIS_INFO;
use App\CUST_CD;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;

use Watson\Validating\ValidationException;

class PisInfoController extends Controller
{
    //
	public function index($pis)
	{
		return view("pis.list", [ "pis" => $pis] );
	}

	public function detail($pis)
	{
		$PIS_MK = Request::segment(4);

		$PIS_INFO = DB::table("PIS_INFO")
					->select( "PIS_MK"
							, "PIS_NM"
							, "HAKNM"
							, "CLASS"
							, "FORM_PLCE"
							, "CLASS_AREA"
							, "NORMAL_TEMP"
							, "SALE_TARGET"
							, "FILENAME"
					)
					->where("PIS_MK", $PIS_MK)->first();
		
		return view("pis.detail", [ "pis" => $pis, "PIS_INFO" => $PIS_INFO] );
	}

	public function listData()
	{

		$PIS_INFO = DB::table("PIS_INFO AS PIS")
						->where("PIS.CORP_MK", "=", $this->getCorpId())
						->select(  "PIS.PIS_MK"
								 , "PIS.PIS_NM"
								 , "PIS.HAKNM"
								 , "PIS.CLASS"
								 , "PIS.FORM_PLCE"
								 , "PIS.CLASS_AREA"
								 , "PIS.NORMAL_TEMP"
								 , "PIS.CORP_MK"
								 , DB::raw(" STUFF ( (SELECT  PIS.PIS_MK
								                             , PIS_C.SIZES
														     , PIS_C.REMARK
														FROM PIS_CLASS_CD AS PIS_C
														where 1 = 1
														  AND PIS_C.CORP_MK = '".$this->getCorpId()."'
														  AND PIS_C.PIS_MK =  PIS.PIS_MK
									FOR XML RAW('PIS_SIZE'), ROOT ('PIS_CLASS_CD'), ELEMENTS), 1, 1, '<') AS DD")
								);

		return Datatables::of($PIS_INFO)
				->filter(function($query) {
					

					/*
					if( Request::Has('chkSizeOrQty') && Request::Input("chkSizeOrQty")!= "ALL" ){
						if( Request::Input("chkSizeOrQty") == "QTYS" ){
							$query->where("PIS.SIZES",  "QTYS" );
						}else{
							$query->where("PIS.SIZES", "<>",  "QTYS" );
						}
					}
					*/	

					if( Request::Has('chkSale') && Request::Input("chkCommon") == "0"){
						$query->where("SALE_TARGET",  True );
					}

					if( Request::Has('chkSale') && Request::Input("chkCommon") == "1"){
						$query->where("SALE_TARGET",  false );
					}

					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("SM.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_MK"){
							$query->where("PIS_MK",  "like", "%".Request::Input('textSearch')."%" );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
							$query->where("PIS_NM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("PIS_MK",  "like", "%".Request::Input('textSearch')."%" )
								->orwhere("PIS_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
					
					
				})->make(true);
	}
	
	// 어종 상세 가져오기(수정시)
	public function getDetailData(){
		
		$PIS_INFO = DB::table("PIS_INFO")
						->where("CORP_MK" , $this->getCorpId())
						->where("PIS_MK" , Request::Input("PIS_MK"))
						->select(  "PIS_MK"
								 , "PIS_NM"
								 , "HAKNM"
								 , "CLASS"
								 , "FORM_PLCE"
								 , "CLASS_AREA"
								 , "NORMAL_TEMP"
								 , "FILENAME"
								 , "SALE_TARGET"
								 , "CORP_MK"
						)->first();
	
		return response()->json(['result' => $PIS_INFO]);
	}
	
	// 어종정보 상세조회
	public function detailListData()
	{	
		$PIS_INFO = DB::table("PIS_CLASS_CD")
							->select(  "PIS_MK"
									  ,"SIZES"
									  ,"REMARK"
									)
							->where("PIS_MK", Request::Input("PIS_MK") );
									
		return Datatables::of($PIS_INFO)
				->filter(function($query) {

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && !empty('textSearch') =="PIS_MK"){
							$query->where("PIS_MK",  "like", "%".Request::Input('textSearch')."%");

						}if( Request::Has('srtCondition') && !empty('srtCondition') == "PIS_NM"){
							$query->where("PIS_NM",  "like", "%".Request::Input('textSearch')."%");

						}if( Request::Has('srtCondition') && !empty('srtCondition') == "REMARK"){
							$query->where("REMARK",  "like", "%".Request::Input('textSearch')."%");
						}else{
							$query->where(function($q){
								$q->orwhere("PIS_MK",  "like", "%".Request::Input('textSearch')."%");
								$q->orwhere("PIS_NM",  "like", "%".Request::Input('textSearch')."%");
								$q->where("REMARK",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
		})->make(true);
	}

	
	public function updpisData(){


		$validator = Validator::make( Request::Input(), [
			'PIS_MK'	=> 'required|max:4,NOT_NULL',
			'PIS_NM'	=> 'required|max:20,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		
		DB::beginTransaction();
		
		try {
			
			if( Request::Has(Request::file("FILENAME")) ){

			}

			DB::table("PIS_INFO")
					->where("PIS_MK", Request::Input("PIS_MK"))
					->where('CORP_MK', $this->getCorpId())
					->update( 
								[
									//'PIS_MK'	=> Request::Input("PIS_MK"),
									'PIS_NM'	=> Request::Input("PIS_NM"),
									'HAKNM'		=> Request::Input("HAKNM"),
									'CLASS'		=> Request::Input("CLASS"),
									'FORM_PLCE'	=> Request::Input("FORM_PLCE"),
									'CLASS_AREA'=> Request::Input("CLASS_AREA"),
									'NORMAL_TEMP'=> Request::Input("NORMAL_TEMP"),
									'FILENAME'	=> !empty(Request::file("FILENAME")) ? Request::file("FILENAME")->getClientOriginalName() : DB::raw("null"),
									//'SALE_TARGET'	=> Request::Input("SALE_TARGET"),
									// 판매어종만 수정됨..
									
								]
					);
			// 파일 중복체크 필요
			if( !empty(Request::file("FILENAME"))){
				$destinationPath = '/uploads/';		
				Request::file("FILENAME")->move(public_path().$destinationPath, Request::file("FILENAME")->getClientOriginalName());
			}

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);

	}

	// 어종추가 : 어종코드 자동생성하여 입력
	public function setpisData(){

		$validator = Validator::make( Request::Input(), [
			'PIS_NM'	=> 'required|max:20,NOT_NULL',			
		]);
		
		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		
		return $exception = DB::transaction(function(){
		
			try {
				
				$PIS_INFO = DB::table("PIS_INFO")
								->where("CORP_MK", $this->getCorpId())
								->where("PIS_NM", Request::Input("PIS_NM"))->count();

				if( (int)$PIS_INFO == 0){
					
					$PIS_INFO = DB::table("PIS_INFO")
								->select("PIS_MK")
								->where("CORP_MK", $this->getCorpId())
								->get();
					
					$NEW_PIS_MK = "B".rand(1,999);

					do{
						//$NEW_PIS_MK = "BA17";
						
						$NEW_PIS_MK = $this->chkPIS_MK2($PIS_INFO, $NEW_PIS_MK);
						//dd($NEW_PIS_MK);
								
					} while( empty($NEW_PIS_MK) );
										
				}else{
					return response()->json(['result' => 'primary_key', 'message' => '동일한 어종이 존재합니다.'], 422);
				}

				if( DB::table("PIS_INFO")
					->where("PIS_MK", $NEW_PIS_MK)
					->where("CORP_MK", $this->getCorpId())	
					->count() == 0){
			
					if( Request::Has("FILENAME") ){
						
						$file = Request::file('filefield');
						$extension = $file->getClientOriginalExtension();
						Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));

						$entry = new Fileentry();
						$entry->mime = $file->getClientMimeType();
						$entry->original_filename = $file->getClientOriginalName();
						$entry->filename = $file->getFilename().'.'.$extension;
						$entry->save();
					}
					
					DB::table("PIS_INFO")->insert( 
						[
							'PIS_MK'	=> $NEW_PIS_MK,
							'PIS_NM'	=> Request::Input("PIS_NM"),
							'HAKNM'		=> Request::Input("HAKNM"),
							'CLASS'		=> Request::Input("CLASS"),
							'FORM_PLCE'	=> Request::Input("FORM_PLCE"),
							'CLASS_AREA'=> Request::Input("CLASS_AREA"),
							'NORMAL_TEMP'=> Request::Input("NORMAL_TEMP"),
							'FILENAME'	=> !empty(Request::file("FILENAME")) ? Request::file("FILENAME")->getClientOriginalName() : DB::raw("null"),
							'SALE_TARGET'=> '1', //Request::Input("SALE_TARGET"),
							// 무조건 판매로 저장
							'CORP_MK'	=> $this->getCorpId(),
						]
					);
					
					// 파일 중복체크 필요
					if( !empty(Request::file("FILENAME"))){
						$destinationPath = '/uploads/';		
						Request::file("FILENAME")->move(public_path().$destinationPath, Request::file("FILENAME")->getClientOriginalName());
					}

					return response()->json(['result' => 'success']);
				}else{
					//throw new Exception();
					return response()->json(['result' => 'primary_key', 'message' => '오류 동일한 어종코드가 존재합니다. 관리자에게 문의바랍니다'], 422);
				}

			}catch(ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(Exception $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
		});

	}

	private function chkPIS_MK2($PIS_INFO, $NEW_PIS_MK){
		
		//dd($PIS_INFO);
		foreach($PIS_INFO as $pis){
			if( $pis->PIS_MK == $NEW_PIS_MK ){
				return "" ;
			}
		}
		return $NEW_PIS_MK;
	}

	public function chkPIS_MK(){
		
		$validator = Validator::make( Request::Input(), [
			'PIS_MK'	=> 'required|max:4,NOT_NULL',
		]);
		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}else{
			return response()->json(['result' => DB::table("PIS_INFO")->where("PIS_MK", Request::Input("PIS_MK"))->count() > 0 ? "fail" : "success" ]);
		}
	}


	public function setUnprovData(){
		
		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'PROV_CUST_MK'	=> 'required|max:20,NOT_NULL',
			'AMT'			=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$AMT		= Request::Input("AMT");
		$REMARK_DIV	= Request::Input("REMARK_DIV");
		
		DB::beginTransaction();
		$seq = $this->getMaxSeqInUNPROVINFO(Request::input('WRITE_DATE')) + 1 ;
		
		$data = [	
					  'CORP_MK'			=> $this->getCorpId()
					, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
					, 'SEQ'				=> $seq
					, 'PROV_CUST_MK'	=> Request::input('PROV_CUST_MK')
					, 'PROV_DIV'		=> Request::input('PROV_DIV')
					, 'AMT'				=> $AMT
					, 'REMARK'			=> Request::Input('REMARK')
				];

		try {
			// 미수금
			DB::table("UNPROV_INFO")->insert( $data );

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	public function _delete(){

		$validator = Validator::make( Request::Input(), [
			'PIS_MK'   => 'required|max:4,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				DB::table("PIS_INFO")
					->where('CORP_MK' , $this->getCorpId())
					->where('PIS_MK' , Request::input('PIS_MK'))
					->delete();

				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}
		});
		
	}

	// 해당거래처의 총미지급금 불러오기
	public function getTotalUnprov(){

		$UNPROV_INFO = DB::table( DB::raw("(
											SELECT PROV_CUST_MK,CORP_MK, SUM(UNPROV)AS UNPROV,SUM(PROV) AS PROV FROM 
											(
												SELECT PROV_CUST_MK,CORP_MK,
														ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END, 0) UNPROV, 
														ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END, 0) PROV
												FROM UNPROV_INFO GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
											) AS A
											GROUP BY PROV_CUST_MK,CORP_MK
										) AS A
										")
								)
								->select( DB::raw("(UNPROV - PROV) AS TOTAL_PROV"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.PROV_CUST_MK', Request::Input("PROV_CUST_MK"))
								->first();
		
		return response()->json($UNPROV_INFO);
	}
	

	public function getCustGrp(){
		$UNPROV_INFO = UNPROV_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($UNPROV_INFO);
	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNPROVINFO($pWRITE_DATE){
		
		return $max_seq = UNPROV_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
				
	}
}


<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\PIS_INFO;
use App\PIS_CLASS_CD;
use App\CUST_CD;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;

use Watson\Validating\ValidationException;

class PisClassController extends Controller
{
    //
	public function index($pisCls)
	{
		return view("pisCls.list", [ "pisCls" => $pisCls] );
	}

	public function detail($pisCls)
	{
		$PIS_MK	= Request::segment(4);
		$SIZES	= Request::segment(5);
		

		$PIS_CLASS_CD = DB::table("PIS_CLASS_CD")
					->select( "PIS_MK"
							, "SIZES"
							, "REMARK"
							, "CORP_MK"
							)
				->where("PIS_MK", $PIS_MK)
				->where("CORP_MK", $this->getCorpId())
				->where("SIZES", $SIZES)->first();

		return view("pis.detail", [ "pisCls" => $pisCls, "PIS_CLASS_CD" => $PIS_CLASS_CD] );
	}


	public function listData()
	{
		
		$PIS_CLASS_CD = DB::table("PIS_CLASS_CD AS CLS")
						->join("PIS_INFO AS PIS", function($join){
							$join->on("PIS.PIS_MK", "=", "CLS.PIS_MK");
							$join->on("PIS.CORP_MK", "=", "CLS.CORP_MK");
						})->select(  "CLS.PIS_MK"
									, DB::raw("CASE WHEN CLS.SIZES = 'QTYS' THEN '마리' ELSE CLS.SIZES END AS SIZES")
									, "PIS.PIS_NM"
									, "CLS.REMARK"

						)->where("CLS.CORP_MK", $this->getCorpId());
						
				
		return Datatables::of($PIS_CLASS_CD)
				->filter(function($query) {
					
					
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							//$query->whereBetween("WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}

					if( Request::Has('chkSizeOrQty') && Request::Input("chkSizeOrQty")!= "ALL" ){
						if( Request::Input("chkSizeOrQty") == "QTYS" ){
							$query->where("CLS.SIZES",  "QTYS" );
						}else{
							$query->where("CLS.SIZES", "<>",  "QTYS" );
						}

					}

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_MK"){
							$query->where("CLS.PIS_MK",  "like", "%".Request::Input('textSearch')."%" );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SIZES"){
							$query->where("CLS.SIZES",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "REMARK"){
							$query->where("CLS.REMARK",  "like", "%".Request::Input('textSearch')."%");
			
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
							$query->where("PIS.PIS_NM",  "like", "%".Request::Input('textSearch')."%");
			
						}else{
							$query->where(function($q){
								$q->where("CLS.PIS_MK",  "like", "%".Request::Input('textSearch')."%" )
								->orwhere("CLS.SIZES",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CLS.REMARK",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("PIS.PIS_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				
				})->make(true);
	}

	public function getDataDetail(){
		
		$PIS_CLASS_CD = DB::table("PIS_CLASS_CD AS CLS")
							->join("PIS_INFO AS PIS", function($join){
								$join->on("PIS.PIS_MK", "=", "CLS.PIS_MK");
								$join->on("PIS.CORP_MK", "=", "CLS.CORP_MK");
							})->where("CLS.PIS_MK", Request::Input("PIS_MK"))
							->where("CLS.CORP_MK", $this->getCorpId() )
							->where("CLS.SIZES", Request::Input("SIZES"))
							->select(  "CLS.PIS_MK"
										, DB::raw("CASE WHEN CLS.SIZES = 'QTYS' THEN '마리' ELSE CLS.SIZES END AS SIZES")
										, "PIS.PIS_NM"
										, "CLS.REMARK"

							)->first();
			
		return response()->json([$PIS_CLASS_CD]); 
	}

	
	// 어종정보 상세조회
	public function detailListData()
	{	
		$PIS_INFO = DB::table("PIS_CLASS_CD")
							->select(  "PIS_MK"
									  , DB::raw("CASE WHEN SIZES = 'QTYS' THEN '마리' ELSE SIZES END AS SIZES")
									  ,"REMARK"
									)
							->where("PIS_MK", Request::Input("PIS_MK") )
							->where("CORP_MK", $this->getCorpId() );
									

		return Datatables::of($PIS_INFO)
				->filter(function($query) {

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && !empty('textSearch') =="PIS_MK"){
							$query->where("PIS_MK",  "like", "%".Request::Input('textSearch')."%");

						}if( Request::Has('srtCondition') && !empty('srtCondition') == "PIS_NM"){
							$query->where("PIS_NM",  "like", "%".Request::Input('textSearch')."%");

						}if( Request::Has('srtCondition') && !empty('srtCondition') == "REMARK"){
							$query->where("REMARK",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->orwhere("PIS_MK",  "like", "%".Request::Input('textSearch')."%");
								$q->orwhere("PIS_NM",  "like", "%".Request::Input('textSearch')."%");
								$q->where("REMARK",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
		})->make(true);
	}

	public function setpisClsData(){


		$validator = Validator::make( Request::Input(), [
			'PIS_MK'	=> 'required|max:4,NOT_NULL',
			'SIZES'		=> 'required|max:20,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		return $exception = DB::transaction(function(){
		
			try {
				
				if(  DB::table("PIS_CLASS_CD")
						->where("CORP_MK", $this->getCorpId())
						->where("PIS_MK", Request::Input("PIS_MK"))
						->where("SIZES", Request::Input("SIZES"))->count() > 0 ){
					return response()->json(['result' => 'DUPLICATION', 'message' => "같은 데이터가 존재합니다."], 422);	
				}

				
				DB::table("PIS_CLASS_CD")->insert( 
					[
						'PIS_MK'	=> Request::Input("PIS_MK"),
						'SIZES'		=> Request::Input("SIZES"),
						'REMARK'	=> Request::Input("REMARK"),
						'CORP_MK'	=> $this->getCorpId(),
					]
				);
				return response()->json(['result' => 'success']);

				

			}catch(\ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}
			}
		});		
	}

	public function updpisClsData(){

		$validator = Validator::make( Request::Input(), [
			'PIS_MK'	=> 'required|max:4,NOT_NULL',
			'SIZES'		=> 'required|max:20,NOT_NULL',
			'OLD_SIZES'	=> 'required|max:20,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		return $exception = DB::transaction(function(){
		
			try {
				DB::table("PIS_CLASS_CD")
					->where("PIS_MK", (string)Request::Input("PIS_MK"))
					->where("SIZES", (string)Request::Input("OLD_SIZES"))
					->where("CORP_MK", $this->getCorpId())
					->update( 
						[
							'REMARK'	=> (string)Request::Input("REMARK"),
						]
					);
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}
			}
		});	
	
	}
	
	public function _delete(){
		
		return $exception = DB::transaction(function(){
			
			try {
				
				DB::table("PIS_CLASS_CD")
					->where("PIS_MK", Request::Input("PIS_MK"))
					->where("SIZES", Request::Input("SIZES"))
					->where("CORP_MK", $this->getCorpId())
					->delete();
				return response()->json(['result' => 'success']);
				
			}catch(PDOException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}catch(QueryException  $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}catch(ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}
		});	

		
	
	}

	public function setUnprovData(){
		
		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'PROV_CUST_MK'	=> 'required|max:20,NOT_NULL',
			'AMT'			=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$AMT		= Request::Input("AMT");
		$REMARK_DIV	= Request::Input("REMARK_DIV");
		
		DB::beginTransaction();
		$seq = $this->getMaxSeqInUNPROVINFO(Request::input('WRITE_DATE')) + 1 ;
		
		$data = [	
					  'CORP_MK'			=> $this->getCorpId()
					, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
					, 'SEQ'				=> $seq
					, 'PROV_CUST_MK'	=> Request::input('PROV_CUST_MK')
					, 'PROV_DIV'		=> Request::input('PROV_DIV')
					, 'AMT'				=> $AMT
					, 'REMARK'			=> Request::Input('REMARK')
				];

		try {
			// 미수금
			DB::table("UNPROV_INFO")->insert( $data );

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	// 해당거래처의 총미지급금 불러오기
	public function getTotalUnprov(){

		$UNPROV_INFO = DB::table( DB::raw("(
											SELECT PROV_CUST_MK,CORP_MK, SUM(UNPROV)AS UNPROV,SUM(PROV) AS PROV FROM 
											(
												SELECT PROV_CUST_MK,CORP_MK,
														ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END, 0) UNPROV, 
														ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END, 0) PROV
												FROM UNPROV_INFO GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
											) AS A
											GROUP BY PROV_CUST_MK,CORP_MK
										) AS A
										")
								)
								->select( DB::raw("(UNPROV - PROV) AS TOTAL_PROV"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.PROV_CUST_MK', Request::Input("PROV_CUST_MK"))
								->first();
		
		return response()->json($UNPROV_INFO);
	}
	

	public function getCustGrp(){
		$UNPROV_INFO = UNPROV_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($UNPROV_INFO);
	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNPROVINFO($pWRITE_DATE){
		
		return $max_seq = UNPROV_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
				
	}
}


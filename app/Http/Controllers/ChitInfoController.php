<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_GRP;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use App\User;

class ChitInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{
		$chitinfolist = DB::table(
									DB::raw("
										(SELECT 
											CHIT_INFO.CORP_MK,
											CHIT_INFO.WRITE_DATE,
											SEQ,
											CHIT_INFO.CUST_MK, 
											CUST_CD.FRNM,
											CHIT_INFO.DE_CR_DIV,
											CHIT_INFO.ACCOUNT_MK,
											ACCOUNT_CD.ACCOUNT_NM,
											case CHIT_INFO.DE_CR_DIV 
												when '1' then AMT end as AMT1,
											case CHIT_INFO.DE_CR_DIV
												when '0' then AMT end as AMT2,
											OUTLINE,
											AMT,
											CHIT_INFO.REMARK
										FROM CHIT_INFO                                  
											left JOIN ACCOUNT_CD ON (CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK and CHIT_INFO.CORP_MK = ACCOUNT_CD.CORP_MK)                                 
											left JOIN CUST_CD ON (CHIT_INFO.CUST_MK = CUST_CD.CUST_MK and CHIT_INFO.CORP_MK=CUST_CD.CORP_MK) ) A"
									)
						)
			->select(
				'CORP_MK',
				DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" ),
				'SEQ',
				'FRNM',
				'CUST_MK',
				'DE_CR_DIV',
				'ACCOUNT_MK',

				'ACCOUNT_NM',
				'OUTLINE',
				'AMT',
				'REMARK',
				DB::raw('isnull(AMT1,0) as AMT1'),
				DB::raw('isnull(AMT2,0) as AMT2' )
			)
			->where("A.CORP_MK","=", $this->getCorpId());

		return Datatables::of($chitinfolist)
				 ->filter(function($query) {
					if( Request::Has('start_date') && Request::Input('end_date')){
						$query->where(function($q){
								$q->where("WRITE_DATE",  ">=", Request::Input('start_date'))
								->where("WRITE_DATE",  "<=", Request::Input('end_date'));
							});
					}
				
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "ACCOUNT_NM"){
							$query->where("ACCOUNT_NM",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRNM"){
							$query->where("FRNM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("ACCOUNT_NM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("FRNM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}) 
				->make(true);
	}

	public function listData2()
	{
	
		$ACCOUNT_GRP_CD	= Request::Input("accountGrp");
		$CUST_GRP_CD	= Request::Input("custGrp");


		$CHIT_INFO = DB::select("EXEC SP_CHIT_INFO '".$this->getCorpId()."','".Request::Input('start_date')."', '".Request::Input('end_date')."'");
		$totaldata = count($CHIT_INFO);
				
		$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => intval( $totaldata ),
                "recordsFiltered" => intval( $_GET['length'] ),
                "data"            => $CHIT_INFO
            );
		echo json_encode($json_data);
	}

	public function index($cust)
	{
		$CUST_GRP_INFO = CUST_GRP_INFO::where("CORP_MK", $this->getCorpId())->get();
		
		$ACCOUNT_GRP = DB::table("ACCOUNT_GRP")
			->select('ACCOUNT_GRP_NM','ACCOUNT_GRP_CD')
			->where("CORP_MK","=", $this->getCorpId())
			->get();
		return view("chitinfo.list",[ "cust" => $cust,  "custGrp" => $CUST_GRP_INFO, "accountGrp" => $ACCOUNT_GRP] );
	}

	// 조합 회비 목록 화면
	public function index2()
	{
		return view("chitinfo.list2");
	}

	// 조합용
	// 회원들의 회비 목록 (년별 => 월별로 조회
	public function getlistFee(){
	
		$pYear = substr(Request::Input("year"), 0, 4);
		$pcode = Request::Input("ACCOUNT_MK");
		
		$FeeLIST = 
			DB::table("CUST_CD AS C")
				->join(DB::raw("(
									SELECT CORP_MK, CUST_MK, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]
									  FROM (	  
									        SELECT C.CORP_MK,
									               C.CUST_MK, 
									               C.FRNM, 
									               MONTH(CH.WRITE_DATE) AS MON,
									               CH.REMARK
									          FROM CUST_CD AS C
									               LEFT OUTER JOIN CHIT_INFO AS CH
									                            ON C.CORP_MK = CH.CORP_MK
									                           AND C.CUST_MK = CH.CUST_MK
									         WHERE C.CORP_MK = '".$this->getCorpId()."'
									           AND YEAR(CH.WRITE_DATE) = '$pYear'
									           AND CH.ACCOUNT_MK = '$pcode'
									        ) AS M
									PIVOT 
									(
									    MIN ( REMARK )
									    FOR MON IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12])
									) AS PV			   
								) AS CH") , function($join){

					$join->on("C.CORP_MK", "=", "CH.CORP_MK");
					$join->on("C.CUST_MK", "=", "CH.CUST_MK");
				})
				->join(DB::raw("(
									SELECT CORP_MK, CUST_MK, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]
									  FROM (	  
									        SELECT C.CORP_MK,
									               C.CUST_MK, 
									               C.FRNM, 
									               MONTH(CH.WRITE_DATE) AS MON,
									               CH.AMT
									          FROM CUST_CD AS C
									               LEFT OUTER JOIN CHIT_INFO AS CH
									                            ON C.CORP_MK = CH.CORP_MK
									                           AND C.CUST_MK = CH.CUST_MK
									         WHERE C.CORP_MK = '".$this->getCorpId()."'
									           AND YEAR(CH.WRITE_DATE) = '$pYear'
									           AND CH.ACCOUNT_MK = '$pcode'
									        ) AS M
									PIVOT 
									(
									    SUM ( AMT )
									    FOR MON IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12])
									) AS PV1			   
								) AS CH1") , function($join){

					$join->on("CH1.CORP_MK", "=", "CH.CORP_MK");
					$join->on("CH1.CUST_MK", "=", "CH.CUST_MK");
				})
				->where("C.CORP_MK", $this->getCorpId())
				->where("C.CUST_GRP_CD", '01')
				->select(
					DB::raw("ROW_NUMBER() OVER(ORDER BY C.FRNM) AS IDX"),
					"C.CUST_MK", 
					"C.FRNM",
					"C.CORP_MK",
					DB::raw("ISNULL(CH.[1] , '-') AS M1"),
					DB::raw("ISNULL(CH.[2] , '-') AS M2"),
					DB::raw("ISNULL(CH.[3] , '-') AS M3"),
					DB::raw("ISNULL(CH.[4] , '-') AS M4"),
					DB::raw("ISNULL(CH.[5] , '-') AS M5"),
					DB::raw("ISNULL(CH.[6] , '-') AS M6"),
					DB::raw("ISNULL(CH.[7] , '-') AS M7"),
					DB::raw("ISNULL(CH.[8] , '-') AS M8"),
					DB::raw("ISNULL(CH.[9] , '-') AS M9"),
					DB::raw("ISNULL(CH.[10] , '-') AS M10"),
					DB::raw("ISNULL(CH.[11] , '-') AS M11"),
					DB::raw("ISNULL(CH.[12] , '-') AS M12"),

					DB::raw("CH1.[1] AS A1"),
					DB::raw("CH1.[2] AS A2"),
					DB::raw("CH1.[3] AS A3"),
					DB::raw("CH1.[4] AS A4"),
					DB::raw("CH1.[5] AS A5"),
					DB::raw("CH1.[6] AS A6"),
					DB::raw("CH1.[7] AS A7"),
					DB::raw("CH1.[8] AS A8"),
					DB::raw("CH1.[9] AS A9"),
					DB::raw("CH1.[10] AS A10"),
					DB::raw("CH1.[11] AS A11"),
					DB::raw("CH1.[12] AS A12")
					
				);
		
		return Datatables::of($FeeLIST)
				->filter(function($query) {
					// 거래처 검색
					if( Request::Has('textSearch') ) {
						$query->where("C.FRNM",  "like", "%".Request::Input('textSearch')."%");
					}
				})->make(true);
	}

	// 조합 회비 목록 화면
	public function index3()
	{
		return view("chitinfo.list3");
	}

	
	public function searchAccountList(){
		
		$ACCOUNT = DB::table("ACCOUNT_CD AS A")
						->select(
							 "A.CORP_MK"
							,"A.ACCOUNT_MK"
							,"A.ACCOUNT_GRP_CD"
							,"A.ACCOUNT_NM"
							, DB::raw("CASE A.DE_CR_DIV WHEN 1 THEN '차변'
										    WHEN 0 THEN '대변' END as DC_NM")
							,"A.DE_CR_DIV"
							, DB::raw("B.ACCOUNT_GRP_CD AS Expr1")
							,"B.ACCOUNT_GRP_NM"
						)->join("ACCOUNT_GRP AS B", function($join){
							$join->on("A.CORP_MK", "=", "B.CORP_MK");
							$join->on("A.ACCOUNT_GRP_CD", "=", "B.ACCOUNT_GRP_CD");
						})->where("A.CORP_MK", $this->getCorpId());
		
		return Datatables::of($ACCOUNT)
				->filter(function($query) {		
					if( Request::Has('textSearch') ) {
						$query->where("A.ACCOUNT_NM", "like", "%".Request::Input('textSearch')."%");
					}

				})->make(true);
	}



	// 비용계정그룹 등록
	public function insert($cust)
	{

		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date',
			'ACCOUNT_MK'	=> 'required|max:3',
			'CUST_MK'		=> 'required|max:20',
			'AMT'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::beginTransaction();
		try {

			DB::table("CHIT_INFO")->insert(
				[
					'CORP_MK'		=> $this->getCorpId(),
					'WRITE_DATE'	=> Request::input('WRITE_DATE'),
					'SEQ'			=> $this->getMaxChitInfoSeq( Request::input('WRITE_DATE') ) + 1,
					'DE_CR_DIV'		=> Request::input('DE_CR_DIV'),
					'CUST_MK'		=> Request::input('CUST_MK'),
					'ACCOUNT_MK'	=> Request::input('ACCOUNT_MK'),
					'AMT'			=> Request::input('AMT'),
					'OUTLINE'		=> Request::input('OUTLINE'),
					'REMARK'		=> Request::input('REMARK'),
				]
			);
		}
		catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	// 비용계정그룹 수정
	public function update($cust)
	{

		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date',
			'ACCOUNT_MK'	=> 'required|max:3',
			'CUST_MK'		=> 'required|max:20',
			'AMT'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::beginTransaction();
		try {

			DB::table("CHIT_INFO")
				->where("WRITE_DATE", Request::Input("OLD_WRITE_DATE"))
				->where("SEQ", Request::Input("SEQ"))
				->where("CORP_MK", $this->getCorpId())
				->update(
				[
					"WRITE_DATE"	=> Request::Input("WRITE_DATE"),
					'DE_CR_DIV'		=> Request::input('DE_CR_DIV'),
					'CUST_MK'		=> Request::input('CUST_MK'),
					'ACCOUNT_MK'	=> Request::input('ACCOUNT_MK'),
					'AMT'			=> Request::input('AMT'),
					'OUTLINE'		=> Request::input('OUTLINE'),
					'REMARK'		=> Request::input('REMARK'),
				]
			);
		}
		catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}


	public function getData(){


		//CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK and CHIT_INFO.CORP_MK = ACCOUNT_CD.CORP_MK
		//dd(Request::Input());
		$CHIT_INFO = DB::table("CHIT_INFO AS CHIT")
						->join("CUST_CD AS CUST", function($join){
							$join->on("CUST.CUST_MK", "=", "CHIT.CUST_MK");
							$join->on("CUST.CORP_MK", "=", "CHIT.CORP_MK");
						})->join("ACCOUNT_CD AS ACNT", function($join){
							$join->on("ACNT.ACCOUNT_MK", "=", "CHIT.ACCOUNT_MK");
							$join->on("ACNT.CORP_MK", "=", "CHIT.CORP_MK");
						})->where("CHIT.CORP_MK", $this->getCorpId())
						->where("CHIT.WRITE_DATE", Request::Input("WRITE_DATE"))
						->where("CHIT.SEQ", Request::Input("SEQ"))
						->where("CUST.CUST_MK",  Request::Input("CUST_MK"))
						->select(
									 'CHIT.CORP_MK'		
									, DB::raw("CONVERT(CHAR(10), CHIT.WRITE_DATE, 23) AS WRITE_DATE")
									,'CHIT.SEQ'			
									,'CHIT.DE_CR_DIV'		
									,'CHIT.CUST_MK'		
									,'CHIT.ACCOUNT_MK'	
									,'CHIT.AMT'			
									,'CHIT.OUTLINE'		
									,'CHIT.REMARK'		
									,'CUST.FRNM'
									,'ACNT.ACCOUNT_NM'
						)->first();
		
		return response()->json([$CHIT_INFO]);
	
	}

	public function PdfDetail(){
		
		$chitinfolist = DB::table(
									DB::raw("
										(SELECT 
											CHIT_INFO.CORP_MK,
											WRITE_DATE,
											SEQ,
											CHIT_INFO.CUST_MK, 
											CUST_CD.FRNM,
											CHIT_INFO.DE_CR_DIV,
											CHIT_INFO.ACCOUNT_MK,
											ACCOUNT_CD.ACCOUNT_NM,
											CASE CHIT_INFO.DE_CR_DIV 
												WHEN'1' then AMT end as AMT1,
											CASE CHIT_INFO.DE_CR_DIV
												WHEN '0' then AMT end as AMT2,
											OUTLINE,
											AMT,
											CHIT_INFO.REMARK
										FROM CHIT_INFO
											LEFT OUTER JOIN ACCOUNT_CD 
											             ON CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK 
														AND CHIT_INFO.CORP_MK = ACCOUNT_CD.CORP_MK
											LEFT OUTER JOIN CUST_CD 
											             ON CHIT_INFO.CUST_MK = CUST_CD.CUST_MK 
														AND CHIT_INFO.CORP_MK=CUST_CD.CORP_MK ) A"
									)
						)
			->select(
				'A.CORP_MK',
				DB::raw( "CONVERT(CHAR(10), A.WRITE_DATE, 23) AS WRITE_DATE" ),
				'A.SEQ',
				'A.FRNM',
				'A.CUST_MK',
				'A.DE_CR_DIV',
				'A.ACCOUNT_MK',
				'A.ACCOUNT_NM',
				'A.OUTLINE',
				'A.AMT',
				'A.REMARK',
				DB::raw('isnull(A.AMT1,0) as AMT1'),
				DB::raw('isnull(A.AMT2,0) as AMT2' ),
				DB::raw("CORP.FRNM AS CORP_FRNM")
									
			)->join("CORP_INFO AS CORP", function($join){
				$join->on("A.CORP_MK", "=", "CORP.CORP_MK");
									
			})
			->where("A.CORP_MK", $this->getCorpId())
			->where("A.WRITE_DATE", ">=", Request::Input("start_date"))
			->where("A.WRITE_DATE", "<=", Request::Input("end_date"))
			->get();
			
			$sumAmt1 = 0;
			$sumAmt2 = 0;

			foreach($chitinfolist as $chit){
			
				$sumAmt1 = $sumAmt1 + $chit->AMT1;
				$sumAmt2 = $sumAmt2 + $chit->AMT2;
			}

			$pdf = PDF::loadView("chitinfo.pdfList", 
								[
									'list'		=> $chitinfolist,
									'start_date'=> Request::Input("start_date"),
									'end_date'	=> Request::Input("end_date"),
									'sumAmt1'	=> $sumAmt1,
									'sumAmt2'	=> $sumAmt2,
								]
							);

		return $pdf->stream("금천출납부.pdf");
	
	}

	public function PdfDetail2(){
		
		
		$CHIT_INFO = DB::select("EXEC SP_CHIT_INFO '".$this->getCorpId()."','".Request::Input('start_date')."', '".Request::Input('end_date')."'");
		

		
		$CORP_INFO	= $this->getCorpInfo();
		$pdf = PDF::loadView("chitinfo.pdfList2", 
							[
								'list'		=> $CHIT_INFO,
								'start_date'=> Request::Input("start_date"),
								'end_date'	=> Request::Input("end_date"),
								'corp'		=> $CORP_INFO,
							]
						);

		return $pdf->stream("금천출납부.pdf");
	
	}

	//선택된 비용전표 정보 삭제
	public function _delete()
	{
		if (Request::ajax()) {
			DB::table("CHIT_INFO")
				->where("CORP_MK", $this->getCorpId())
				->where("SEQ", Request::get('SEQ'))
				->where("WRITE_DATE", Request::get('WRITE_DATE'))
				->delete();

			return Response::json(['result' => 'success']);

		}else{
			return Response::json(['result' => 'failed'], 422);	
		}

		return Response::json(['result' => 'failed'], 422);	
	}

	private function getMaxChitInfoSeq($pWRITE_DATE){
		

		return $ChipInfo = DB::table("CHIT_INFO")
						->where("WRITE_DATE", $pWRITE_DATE)
						->where("CORP_MK", $this->getCorpId())
						->select(DB::raw('ISNULL(max("SEQ"), 0) AS SEQ'))->first()->SEQ;
	}

	
	/*

	// 선택된 비용계정그룹 정보
	public function edit($cust, $id)
	{	
		$model = DB::table("ACCOUNT_GRP")
            ->where("CORP_MK", $this->getCorpId())
            ->where("ACCOUNT_GRP_CD", $id)->first();

		return response()->json(['ACCOUNT_GRP_CD' => $model->ACCOUNT_GRP_CD, 'ACCOUNT_GRP_NM' => $model->ACCOUNT_GRP_NM]);
	}

	//선택된 비용계정그룹 정보 수정
    public function update($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'ACCOUNT_GRP_CD' => 'required|max:2,NOT_NULL',
            'ACCOUNT_GRP_NM' => 'required|max:20,NOT_NULL'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("ACCOUNT_GRP")
				->where("CORP_MK", $this->getCorpId())
                ->where("ACCOUNT_GRP_CD", Request::input('ACCOUNT_GRP_CD'))
                ->update(
					[
						"ACCOUNT_GRP_NM" => Request::input('ACCOUNT_GRP_NM')
					]
				);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//선택된 비용계정그룹 정보 삭제
    public function delete()
    {
        $corp_id = Auth::user("");

        if (Request::ajax()) {
            DB::table("ACCOUNT_GRP")
				->where("CORP_MK", $this->getCorpId())
                ->whereIn("ACCOUNT_GRP_CD", Request::get('cd'))
                ->delete();

            return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
        }
        return Response::json(['result' => 'failed']);
    }

    public function create($cust)
    {
        $model = new CUST_GRP_INFO();
        return view("custGrp.Edit", ["model" => $model, "cust" => $cust]);
    }




    public function Excel()
    {
        // 컬럼을 지정해줘야 에러가 안남
        $model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
            ->where("CORP_MK", $this->getCorpId())
            ->get();

        Excel::create('거래처그룹', function ($excel) use ($model) {
            $excel->sheet('거래처그룹', function ($sheet) use ($model) {
                $sheet->fromArray($model);
            });
        })->export('xls');
    }

    public function Pdf()
    {

        $pdf = PDF::loadHTML("<h1>Test</h1>");

        // $pdf->save('myfile.pdf');
        return $pdf->stream();

    }

    public function getCorpId(){
        Request::session()->put('corp_id', 'app0053'); //app0053 테스트용 kdbin2011
        return $corp_id = Request::session()->get('corp_id');
    }
	*/
}
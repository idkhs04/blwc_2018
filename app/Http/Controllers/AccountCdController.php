<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_CD;
use App\ACCOUNT_GRP;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class accountcdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	//비용계정 리스트
	public function listData()
	{
		
		$ACCOUNT_CD = ACCOUNT_CD::join('ACCOUNT_GRP', function($join){
				$join->on("ACCOUNT_CD.CORP_MK", "=" ,"ACCOUNT_GRP.CORP_MK");
				$join->on("ACCOUNT_CD.ACCOUNT_GRP_CD", "=" ,"ACCOUNT_GRP.ACCOUNT_GRP_CD");
			})->where("ACCOUNT_CD.CORP_MK", $this->getCorpId());

						
		return Datatables::of($ACCOUNT_CD)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "ACCOUNT_MK"){
							$query->where("ACCOUNT_CD.ACCOUNT_MK",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "ACCOUNT_NM"){
							$query->where("ACCOUNT_CD.ACCOUNT_NM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("ACCOUNT_CD.ACCOUNT_MK",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("ACCOUNT_CD.ACCOUNT_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}

	public function index($cust)
	{
		return view("accountcd.list",[ "cust" => $cust ] );
	}

	// 선택된 비용계정 등록
    public function insert($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'ACCOUNT_MK' => 'required|max:3,NOT_NULL',
            'ACCOUNT_NM' => 'required|max:20,NOT_NULL',
            'DE_CR_DIV' => '',
            'ACCOUNT_GRP_CD' => 'required|max:20,NOT_NULL',
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		// 동일한 키가 있는 경우 실패메시지
		$count = DB::table("ACCOUNT_CD")
			->where('CORP_MK', $this->getCorpId())
			->where('ACCOUNT_MK',  Request::Input("ACCOUNT_MK"))
			->count();
		if( $count > 0){
			return response()->json(['result' => 'fail', 'reason' => 'PrimaryKey', 'message' => '동일한 비용계정코드가 존재합니다'], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("ACCOUNT_CD")->insert(
                [
                    'CORP_MK' => $this->getCorpId(),
                    'ACCOUNT_MK' => Request::input('ACCOUNT_MK'),
                    'ACCOUNT_NM' => Request::input('ACCOUNT_NM'),
                    'DE_CR_DIV' => Request::input('DE_CR_DIV'),
                    'ACCOUNT_GRP_CD' => Request::input('ACCOUNT_GRP_CD')
                ]
            );
        }
        catch(ValidationException $e){
            DB::rollback();

            return Redirect::to("/accountcd/" . $cust)
                    ->withErrors($e->getErrors())
                    ->withInput();

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//계정그룹 리스트
	public function getAccountGrp($cust)
	{	
		$ACCOUNT_GRP = DB::table("ACCOUNT_GRP")
			->select('ACCOUNT_GRP_NM','ACCOUNT_GRP_CD')
			->where("CORP_MK",  "=", $this->getCorpId())
			->get();
		return response()->json(['ACCOUNT_GRP' => $ACCOUNT_GRP]);
	}

	// 선택된 비용계정정보
	public function edit($cust, $id)
	{	
		$model = DB::table("ACCOUNT_CD")
			->join('ACCOUNT_GRP', function($join){
				$join->on("ACCOUNT_CD.CORP_MK", "=" ,"ACCOUNT_GRP.CORP_MK");
				$join->on("ACCOUNT_CD.ACCOUNT_GRP_CD", "=" ,"ACCOUNT_GRP.ACCOUNT_GRP_CD");
			})
            ->where("ACCOUNT_CD.CORP_MK", $this->getCorpId())
            ->where("ACCOUNT_CD.ACCOUNT_MK", $id)
			->first();

		return response()->json(
			[
				'ACCOUNT_MK' => $model->ACCOUNT_MK, 
				'ACCOUNT_NM' => $model->ACCOUNT_NM, 
				'DE_CR_DIV' => $model->DE_CR_DIV,
				'ACCOUNT_GRP_CD' => $model->ACCOUNT_GRP_CD,
				'ACCOUNT_GRP_NM' => $model->ACCOUNT_GRP_NM
			]
		);
	}

	// 선택된 비용계정 수정
    public function update($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'ACCOUNT_MK' => 'required|max:2,NOT_NULL',
            'ACCOUNT_NM' => 'required|max:20,NOT_NULL',
            'ACCOUNT_GRP_CD' => 'required|max:20,NOT_NULL',
            'DE_CR_DIV' => ''
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("ACCOUNT_CD")
				->where("CORP_MK", $this->getCorpId())
                ->where("ACCOUNT_MK", Request::input('ACCOUNT_MK'))
                ->update([
					"ACCOUNT_NM" => Request::input('ACCOUNT_NM'),
					"DE_CR_DIV" => Request::input('DE_CR_DIV'),
					"ACCOUNT_GRP_CD" => Request::input('ACCOUNT_GRP_CD'),
					]
				);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	// 비용계정 삭제
    public function _delete()
    {

		if (Request::ajax()) {

			$count = DB::table("CHIT_INFO")
				->where("CORP_MK", $this->getCorpId())
				->where("ACCOUNT_MK", Request::Input('cd'))
				->count();

			// 현금출납부에서 사용중일때 삭제 메시지 
			if( $count > 0){
				return Response::json(['result' => 'fail'], 422);
			}else{
				DB::table("ACCOUNT_CD")
				->where("CORP_MK", $this->getCorpId())
				->where("ACCOUNT_MK", Request::Input('cd'))
				->delete();

				return Response::json(['result' => 'success', 'code' => Request::Input('cd')]);
			}

			
		}
		return Response::json(['result' => 'failed']);
    }

	/*


    public function create($cust)
    {
        $model = new CUST_GRP_INFO();
        return view("custGrp.Edit", ["model" => $model, "cust" => $cust]);
    }

    public function Excel()
    {
        // 컬럼을 지정해줘야 에러가 안남
        $model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
            ->where("CORP_MK", $this->getCorpId())
            ->get();

        Excel::create('거래처그룹', function ($excel) use ($model) {
            $excel->sheet('거래처그룹', function ($sheet) use ($model) {
                $sheet->fromArray($model);
            });
        })->export('xls');
    }

    public function Pdf()
    {

        $pdf = PDF::loadHTML("<h1>Test</h1>");

        // $pdf->save('myfile.pdf');
        return $pdf->stream();

    }
	*/

}
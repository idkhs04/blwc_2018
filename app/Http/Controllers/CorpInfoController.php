<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\Http\Requests;

use App\CORP_INFO;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_CD;
use App\ACCOUNT_GRP;
use App\CHIT_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use Zizaco\Entrust\EntrustPermission;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\User;
use URL;
use Session;

use PHPExcel_Style_Border;
use File;
use Config;

class CorpInfoController extends Controller 
{
	use EntrustUserTrait;
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	protected $throwValidationExceptions = true;

	function __construct(){
		
		parent::__construct();
		
		$user = User::where("MEM_ID", Auth::user()->MEM_ID)->first();

		if( ( !$user->hasRole(['admin']))) {
			return Redirect::back()->with('alert-success', '해당 업무에 권한이 없습니다.')->send();
		}

	}

	// 백업 범위 체크
	// 입고일을 기준으로 다 팔린 재고기간을 리턴한다
	public function chkCorpBackup(){
		
		$validator = Validator::make( Request::Input(), [
			'START_DATE'			=> 'required|date_format:Y-m-d',
			'END_DATE'				=> 'required|date_format:Y-m-d',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'vali_fail', 'message' => $errors], 422);
		}


		$SALE_INFO_DATE = DB::table(
				DB::raw("
								(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS QTY, SUM(INPUT_AMT) AS AMT, AVG(UNCS) AS UNCS, CORP_MK, CUST_MK
								   FROM STOCK_INFO
								   WHERE (CORP_MK = '".$this->getCorpId()."')
									 AND (INPUT_DATE >= '".Request::Input("START_DATE")."')
									 AND (INPUT_DATE <= '".Request::Input("END_DATE")."')
									 AND (DE_CR_DIV = 1)
									 AND QTY > 0
									 AND UNCS > 0
									 AND INPUT_AMT > 0
									 AND OUTPUT_DATE IS NULL
								   GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, CORP_MK, CUST_MK
								) AS I
							  ")
				)->leftjoin(DB::raw("
								(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS S_QTY, CORP_MK
								   FROM STOCK_INFO
								   WHERE (CORP_MK = '".$this->getCorpId()."')
									AND (INPUT_DATE >= '".Request::Input("START_DATE")."')
									AND (INPUT_DATE <= '".Request::Input("END_DATE")."')
									AND OUTPUT_DATE IS NOT NULL
								  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, CORP_MK
								) AS O
							  "), function($leftjoin){
									$leftjoin->on("I.CORP_MK", "=", "O.CORP_MK");
									$leftjoin->on("I.PIS_MK", "=", "O.PIS_MK");
									$leftjoin->on("I.SIZES", "=", "O.SIZES");
									$leftjoin->on("I.ORIGIN_NM", "=", "O.ORIGIN_NM");
									$leftjoin->on("I.INPUT_DATE", "=", "O.INPUT_DATE");
							  }
				)->leftjoin(DB::raw("
								(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, AVG(SALE_UNCS) AS SALE_UNCS
								   FROM STOCK_SUJO
								  WHERE CORP_MK = '".$this->getCorpId()."'
								  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
								) AS S
							"), function($leftjoin){
									$leftjoin->on("I.PIS_MK", "=", "S.PIS_MK");
									$leftjoin->on("I.SIZES", "=", "S.SIZES");
									$leftjoin->on("I.ORIGIN_NM", "=", "S.ORIGIN_NM");
									$leftjoin->on("I.INPUT_DATE", "=", "S.INPUT_DATE");
							}
				)
				->join("CUST_CD AS CUST", function($join){
					$join->on("I.CUST_MK", "=", "CUST.CUST_MK");
					$join->on("I.CORP_MK", "=", "CUST.CORP_MK");
				})
				
				->join("PIS_INFO AS P", function($join){
					$join->on("I.PIS_MK", "=", "P.PIS_MK");
					$join->on("I.CORP_MK", "=", "P.CORP_MK");
				})
				->where("P.CORP_MK", $this->getCorpId())
				->select(
						    "CUST.FRNM"
						  , "P.PIS_NM"
						  , "I.PIS_MK"
						  , "I.SIZES"
						  , "I.ORIGIN_NM"
						  , DB::raw("CONVERT(CHAR(10), I.INPUT_DATE, 23) AS INPUT_DATE" )
						  , DB::raw("ROUND(I.QTY, 1) AS QTY")
						  , DB::raw("FLOOR(I.UNCS) AS UNCS")
						  , DB::raw("ROUND(I.AMT, 2) AS AMT")
						  , DB::raw("ROUND(O.S_QTY, 2) AS S_QTY")
						  , DB::raw("ROUND( (I.QTY - ISNULL(O.S_QTY, 0)), 3) AS REST")
						  , DB::raw("ISNULL(S.SALE_UNCS, 0) AS SALE_UNCS")
						  , DB::raw("ISNULL( ROUND(S.SALE_UNCS * (ROUND(I.QTY, 1) - ISNULL(O.S_QTY, 0)), -3, 0), 0) AS RES_AMT")
				)
				->get();
		
		//dd($SALE_INFO_DATE);
		$message = null;

		if( $SALE_INFO_DATE == null){
			return response()->json(['result' => 'fail_none', 'message' => '해당기간에 데이터가 존재 하지 않습니다'], 422); 
		}else{

			//unset($message);
			foreach( $SALE_INFO_DATE as $model){
			
				if( (doubleval($model->RES_AMT)) > 0){
					$message[] = '('.$model->FRNM.") 의 ".$model->PIS_NM."[".$model->SIZES."] 가 ".$model->REST." 남았습니다.";
				}
			}

			if( $message != null && count($message) > 0){
				return response()->json(['result' => 'fail', 'message' => $message ], 422); 
			}else{
				
				$result = $this->exeBackup( Request::Input("START_DATE"), Request::Input("END_DATE"), Date("Ymd"),	Request::Input("REMARK"));
				if( $result['result'] == "fail"){
					return response()->json($result, 422); 
				}else{
					return response()->json($result); 
				}
			}
		}
	}

	// 백업관리(이력) 목록 조회 화면
	public function backIndex($corp, $corp_mk){
		
		return view("corp.backup", ['CORP_MK' => $corp_mk]);
	}

	// 백업관리(이력) 목록 조회 데이터
	public function getBackList(){
		
		$CORP_MK	= Request::Input("CORP_MK");

		$BACKUP_INFO = DB::table("BACKUP_INFO AS BK")
				->join('CORP_INFO AS CORP', function($join){
					$join->on("CORP.CORP_MK", "=" ,"BK.CORP_MK");
				})->select(
					   "BK.BK_CORP_MK"
					  ,"BK.BK_DATE"
					  , DB::raw("CONVERT(CHAR(10), BK.BK_DATE, 23) AS BK_DATE" )
					  , DB::raw("CONVERT(CHAR(10), BK.BK_START_DATE, 23) AS START_DATE" )
					  , DB::raw("CONVERT(CHAR(10), BK.BK_END_DATE, 23) AS END_DATE" )
					  ,"BK.CORP_MK"
					  ,"BK.BK_REMARK"
					  ,"CORP.FRNM"
				)
				->where("CORP.CORP_MK", $CORP_MK)
				->where (function($query){
										
					if( Request::Has('START_DATE') && $this->fnValidateDate(Request::Input("START_DATE"))){
						if( Request::Has('END_DATE') && $this->fnValidateDate(Request::Input("END_DATE"))){
							$query->whereBetween("BK.BK_DATE",  array(Request::Input("START_DATE"), Request::Input("END_DATE")));
							
						}
					}
				});
				
						
		return Datatables::of($BACKUP_INFO)->make(true);
	}

	// 백업실행
	private function exeBackup($pStart_dt, $pEnd_dt, $pToday, $pREMARK){
		
		$BACKUP_DATE	= $pToday;
		$BACKUP_INFO	= DB::table("BACKUP_INFO")
							->where("CORP_MK", $this->getCorpId())
							->where("BK_DATE", $BACKUP_DATE)
							->get();

		if( count($BACKUP_INFO) > 0){
			return ['result' => 'fail', 'message' => ['백업은 하루에 한번만 할 수 있습니다.']];
		}
		
		$BACKUP_CORP_MK	= $this->getCorpId()."_".$BACKUP_DATE;

		DB::statement(DB::raw("EXEC SP_BACKUP '".$this->getCorpId()."','".$pStart_dt."','".$pEnd_dt."'"));
			
		DB::table("BACKUP_INFO")
			->insert(
				[
					   "BK_CORP_MK"		=> $BACKUP_CORP_MK
					  ,"BK_DATE"		=> $BACKUP_DATE
					  ,"BK_START_DATE"	=> $pStart_dt
					  ,"BK_END_DATE"	=> $pEnd_dt
					  ,"CORP_MK"		=> $this->getCorpId()
					  ,"BK_REMARK"		=> $pREMARK
				]
			);
		
		return ['result' => 'success'];
		
	}

	public function setBackup(){
		
		$validator = Validator::make( Request::Input(), [	
			'START_DATE'		=> 'required|date_format:"Y-m-d',
			'END_DATE'			=> 'required|date_format:"Y-m-d',
		]);
		
		if ($validator->fails()) {
			return redirect()
				->back()
				->withInput(Request::all())
				->withErrors($validator)->withInput()->send();
		}

		// 1. 입고/재고내역
		$STOCK_INFO_LIST	= $this->getStockInfoData(Request::Input('START_DATE'), Request::Input('END_DATE'));
		// 2. 판매(매출)내역
		$SALE_INFO_LIST		= $this->getSaleInfoData(Request::Input('START_DATE'), Request::Input('END_DATE'));
		// 3. 기간별 판매현황
		$SALE_INFO_ARR_LIST	= $this->getSaleArrInfoData(Request::Input('START_DATE'), Request::Input('END_DATE'));
		// 4. 매출미수정보 
		$UNCL_INFO_LIST		= $this->getUnclData(Request::Input('START_DATE'), Request::Input('END_DATE'));
		// 5. 매입미지급정보
		$UNPROV_INFO_LIST	= $this->getUnprovData(Request::Input('START_DATE'), Request::Input('END_DATE'));
		// 6. 비용전표정보
		$CHIT_INFO_LIST		= $this->getChitInfoData(Request::Input('START_DATE'), Request::Input('END_DATE'));

		// Initialize the array which will be passed into the Excel
		// generator.
		$stockInfosArray	= []; 
		$saleInfosArray		= []; 
		$saleInfosArrArray	= []; 
		$unclInfoArray		= [];
		$unprovInfoArray	= [];
		$chitInfoArray		= [];

		// Define the Excel spreadsheet headers
		$stockInfosArray[]		= ['id', 'customer','email','total','created_at'];
		$saleInfosArray[]		= ['id', 'customer','email','total','created_at'];
		$saleInfosArrArray[]	= ['id', 'customer','email','total','created_at'];
		$unclInfoArray[]		= ['id', 'customer','email','total','created_at'];
		$unprovInfoArray[]		= ['id', 'customer','email','total','created_at'];
		$chitInfoArray[]		= ['작성일자', '거래처상호','계정명','지출금액','수출금액', '적요', '비고'];

		// Convert each member of the returned collection into an array,
		// and append it to the payments array.
		foreach ($STOCK_INFO_LIST as $STOCK_INFO) {
			$stockInfosArray[] = get_object_vars($STOCK_INFO);
		}

		foreach ($SALE_INFO_LIST as $SALE_INFO) {
			$saleInfosArray[] = get_object_vars($SALE_INFO);
		}

		foreach ($SALE_INFO_ARR_LIST as $SALE_INFO_ARR) {
			$saleInfosArrArray[] = get_object_vars($SALE_INFO_ARR);
		}

		foreach ($UNCL_INFO_LIST as $UNCL_INFO) {
			$unclInfoArray[] = get_object_vars($UNCL_INFO);
		}

		foreach($UNPROV_INFO_LIST as $UNPROV_INFO){
			$unprovInfoArray[] = get_object_vars($UNPROV_INFO);
		}

		$SUM_AMT = 0;
		foreach($CHIT_INFO_LIST as $CHIT_INFO){
			$SUM_AMT += $CHIT_INFO->AMT1 + $CHIT_INFO->AMT1;
			$chitInfoArray[] = get_object_vars($CHIT_INFO);
		}
		$chitInfoArray[] = array('합계', $SUM_AMT);

		

		// Generate and return the spreadsheet
		Excel::create('전체백업', function($excel) use ($stockInfosArray, $saleInfosArray, $saleInfosArrArray, $unclInfoArray, $unprovInfoArray, $chitInfoArray) {

			// Set the spreadsheet title, creator, and description
			$excel->setTitle('STOCK_INFO');
			$excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
			$excel->setDescription('payments file');

			// Build the spreadsheet, passing in the payments array
			$excel->sheet('재고(입고)내역', function($sheet) use ($stockInfosArray) {
				$sheet->fromArray($stockInfosArray, null, 'A1', false, false);
			});

			$excel->sheet('판매내역', function($sheet) use ($saleInfosArray) {
				$sheet->fromArray($saleInfosArray, null, 'A1', false, false);
			});

			$excel->sheet('기간별 판매내역', function($sheet) use ($saleInfosArrArray) {
				$sheet->fromArray($saleInfosArrArray, null, 'A1', false, false);
			});

			$excel->sheet('미수내역', function($sheet) use ($unclInfoArray) {
				$sheet->fromArray($unclInfoArray, null, 'A1', false, false);
			});

			$excel->sheet('미지급내역', function($sheet) use ($unprovInfoArray) {
				$sheet->fromArray($unprovInfoArray, null, 'A1', false, false);
			});

			$excel->sheet('비용전표', function($sheet) use ($chitInfoArray) {
				$sheet->fromArray($chitInfoArray, null, 'A1', false, false);
			});


		})->stream('xlsx');

	}

	public function getChitInfoData($pWrite_date, $pEnd_date){

		return  DB::table(
							DB::raw("
								(SELECT 
									CHIT_INFO.CORP_MK,
									WRITE_DATE,
									SEQ,
									CHIT_INFO.CUST_MK, 
									CUST_CD.FRNM,
									CHIT_INFO.DE_CR_DIV,
									CHIT_INFO.ACCOUNT_MK,
									ACCOUNT_CD.ACCOUNT_NM,
									case CHIT_INFO.DE_CR_DIV 
										when '1' then AMT end as AMT1,
									case CHIT_INFO.DE_CR_DIV
										when '0' then AMT end as AMT2,
									OUTLINE,
									AMT,
									CHIT_INFO.REMARK
								FROM CHIT_INFO                                  
									left JOIN ACCOUNT_CD ON (CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK and CHIT_INFO.CORP_MK = ACCOUNT_CD.CORP_MK)                                 
									left JOIN CUST_CD ON (CHIT_INFO.CUST_MK = CUST_CD.CUST_MK and CHIT_INFO.CORP_MK=CUST_CD.CORP_MK) ) A"
							)
						)
			->select(
				
				DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" ),
				'FRNM',
				'ACCOUNT_NM',
				DB::raw('isnull(AMT1,0) as AMT1'),
				DB::raw('isnull(AMT2,0) as AMT2' ),
				'OUTLINE',
				'REMARK'
				
			)
			->where("A.CORP_MK","=", $this->getCorpId())
			->where("WRITE_DATE",  ">=", $pWrite_date)
			->where("WRITE_DATE",  "<=", $pEnd_date)
			->get();
	
	}

	// 미지급금 데이터
	public function getUnprovData($pWrite_date, $pEnd_date){
		
		return $UNPROV_INFO = DB::table("UNPROV_INFO AS U")
							->select(  "U.CORP_MK"
									 , DB::raw( "CONVERT(CHAR(10), U.WRITE_DATE, 23) AS WRITE_DATE" )
									 , "U.SEQ"
									 , "U.PROV_CUST_MK"
									 , "U.PROV_DIV"
									 , "U.AMT"
									 , "U.REMARK"
									 , "U.STOCK_PIS_MK"
									 , "S.SIZES"
									 , "S.INPUT_DATE"
									 , "S.ORIGIN_CD"
									 , "S.ORIGIN_NM"
									 , "S.QTY"
									 , "S.UNCS"
									 , "S.INPUT_DISCOUNT"
									 , "S.INPUT_UNPROV"
									 , "S.PIS_NM"
									 , "CUST.FRNM"
									 , DB::raw("(S.UNCS*S.QTY-S.INPUT_DISCOUNT) AS INPUT_AMT")
									 , DB::raw("floor(S.INPUT_AMT/S.QTY) as DAN")
									 , DB::raw("CASE WHEN U.PROV_DIV = '0' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_CHA") 
									 , DB::raw("CASE WHEN U.PROV_DIV = '1' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_DAE")
									 , DB::raw(" STUFF ( (SELECT 
															     P1.PIS_NM 
															   , S1.SIZES
															   , CONVERT(INT, S1.QTY) QTY
															   , CONVERT(INT, S1.UNCS) UNCS
															   , CONVERT(INT, S1.INPUT_AMT) INPUT_AMT
															   , S1.OUTLINE
															   , S1.ORIGIN_NM
															   , CONVERT(CHAR(10), S1.INPUT_DATE, 23)  INPUT_DATE
															   , CONVERT(INT, S1.INPUT_DISCOUNT) INPUT_DISCOUNT
															   , CONVERT(INT, S1.INPUT_UNPROV) INPUT_UNPROV
														FROM STOCK_INFO AS S1 
															INNER JOIN PIS_INFO AS P1 
																	 ON S1.PIS_MK=P1.PIS_MK
														where 1 = 1
														  AND S1.CORP_MK = '".$this->getCorpId()."'
														  AND S1.SIZES    = S.SIZES
														  AND S1.SEQ      = S.SEQ
														  AND S1.CUST_MK =  U.PROV_CUST_MK
									 FOR XML RAW('STOCK'), ROOT ('SALE_INFO'), ELEMENTS), 1, 1, '<') AS DD")
							)->leftjoin(DB::raw("
										 (		SELECT S.*
													 , P.PIS_NM 
												  FROM STOCK_INFO AS S 
													   INNER JOIN PIS_INFO AS P ON S.PIS_MK=P.PIS_MK
												 WHERE S.CORP_MK = '".$this->getCorpId()."'
										 ) AS S"), function($join){
													$join->on("U.CORP_MK", "=", "S.CORP_MK");
													$join->on("U.STOCK_PIS_MK ", "=", "S.PIS_MK");
													$join->on("U.STOCK_SIZES", "=", "S.SIZES");
													$join->on("U.STOCK_SEQ", "=", "S.SEQ");
							})->join("CUST_CD AS CUST", function($join){
									$join->on("CUST.CUST_MK", "=", "U.PROV_CUST_MK");
									$join->on("CUST.CORP_MK", "=", "U.CORP_MK");
									 
							})->where("U.CORP_MK", $this->getCorpId())
							  ->whereBetween("U.WRITE_DATE",  array($pWrite_date, $pEnd_date))
							  ->get();
	
	}
	
	// 미수금 데이터
	public function getUnclData($pWrite_date, $pEnd_date){

		return $UNCL_INFO = DB::table("UNCL_INFO AS U")
							->select(  "U.CORP_MK"
									 , DB::raw("CONVERT(CHAR(10), U.WRITE_DATE, 23) AS WRITE_DATE")
									 , "U.UNCL_CUST_MK"
									 , "U.PROV_DIV"
									 , "U.UNCL_AMT"
									 , "U.AMT"
									 , "U.REMARK"
									 , "U.SALE_SEQ"
									 , "saleT.TOTAL_SALE_AMT"
									 , "saleT.FRNM"
									 , "saleT.SALE_DISCOUNT"
									 , "saleT.CASH_AMT"
									 , "saleT.PIS"
									 , "U.SEQ"
									 , "CUST.FRNM"
									 , DB::raw("CASE WHEN PROV_DIV = 1 THEN U.AMT ELSE 0 END AS ACCRUED_AMT")
									 , DB::raw("CASE WHEN PROV_DIV = 0 THEN U.AMT ELSE 0 END AS COLLECTION_AMT")
									 , DB::raw(" STUFF ( (SELECT 
														     CONVERT(CHAR(10), LINE_D.WRITE_DATE, 23) AS WRITE_DATE 
														   , LINE_D.CUST_MK
														   , LINE_D.SEQ
														   , CONVERT(INT, LINE_D.QTY) QTY
														   , LINE_D.PIS_MK
														   , LINE_D.SIZES
														   , LINE_D.ORIGIN_NM
														   , CONVERT(INT, LINE_D.UNCS) UNCS
														   , CONVERT(INT, LINE_D.AMT ) AMT
														   , LINE_D.REMARK
														   , P1.PIS_NM
														FROM SALE_INFO_D AS LINE_D
														     INNER JOIN PIS_INFO AS P1
														           ON LINE_D.PIS_MK = P1.PIS_MK
														where 1 = 1
														  AND LINE_D.CORP_MK = '".$this->getCorpId()."'
														  AND LINE_D.WRITE_DATE = U.WRITE_DATE
									 FOR XML RAW('SALE'), ROOT ('SALE_INFO_D'), ELEMENTS), 1, 1, '<') AS DD")
							)->join("CUST_CD AS CUST", function($join){
									$join->on("CUST.CUST_MK", "=", "U.UNCL_CUST_MK");
									$join->on("CUST.CORP_MK", "=", "U.CORP_MK");
									 
							})->leftjoin(DB::raw("
										 (SELECT SM.CORP_MK
										      , SM.WRITE_DATE
										      , SM.CUST_MK
										      , SM.SEQ
										      , SM.CASH_AMT
										      , SM.CHECK_AMT
										      , SM.UNCL_AMT
										      , SM.CARD_AMT
										      , SM.TOTAL_SALE_AMT
										      , C.FRNM
										      , SM.SALE_DISCOUNT
										      , CASE SD.CNT WHEN 1 THEN P.PIS_NM ELSE P.PIS_NM + '외' + CONVERT (VARCHAR , (SD.CNT - 1)) + '종' END AS PIS
										      , SD.SUMQTY 
										   FROM SALE_INFO_M AS SM 
										        INNER JOIN (SELECT CORP_MK
										                         , WRITE_DATE
										                         , CUST_MK, SEQ
										                         , COUNT(*) AS CNT
										                         , SUM(QTY) AS SUMQTY
										                         , MAX(PIS_MK) AS PIS_MK 
										                      FROM SALE_INFO_D AS D 
										                      GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ
										                    ) AS SD 
										                ON SM.CORP_MK = SD.CORP_MK 
										               AND SM.WRITE_DATE = SD.WRITE_DATE 
										               AND SM.CUST_MK = SD.CUST_MK 
										               AND SM.SEQ = SD.SEQ 
										        INNER JOIN CUST_CD AS C 
										                ON SM.CUST_MK = C.CUST_MK 
										               AND SM.CORP_MK = C.CORP_MK 
										        INNER JOIN PIS_INFO AS P 
										                ON SD.PIS_MK = P.PIS_MK
										 ) AS saleT"), function($join){
									$join->on("U.CORP_MK", "=", "saleT.CORP_MK");
									$join->on("U.WRITE_DATE", "=", "saleT.WRITE_DATE");
									$join->on("U.UNCL_CUST_MK", "=", "saleT.CUST_MK");
									$join->on("U.SALE_SEQ", "=", "saleT.SEQ");
							})->where("U.CORP_MK", $this->getCorpId())
							 ->whereBetween("U.WRITE_DATE",  array($pWrite_date, $pEnd_date ))
							 ->get();

	}
	
	// 기간별 판매현황
	public function getSaleArrInfoData($pWrite_date, $pEnd_date){

		return $SALE_INFO_DATE = DB::table(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS QTY, SUM(INPUT_AMT) AS AMT, AVG(UNCS) AS UNCS 
										   FROM STOCK_INFO 
										   WHERE (CORP_MK = '".$this->getCorpId()."') 
										     AND (INPUT_DATE >= '".$pWrite_date."') 
										     AND (INPUT_DATE <= '".$pEnd_date."') 
										     AND (DE_CR_DIV = 1) 
										     AND (OUTPUT_DATE IS NULL) 
										   GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS I
									  ")
						)->select(  "P.PIS_NM"
								  , "I.PIS_MK"
								  , "I.SIZES"
								  , "I.ORIGIN_NM"
								  , DB::raw( "CONVERT(CHAR(10), I.INPUT_DATE, 23) AS INPUT_DATE" )
								  , "I.QTY"
								  , DB::raw( "FLOOR(I.UNCS) AS UNCS")
								  , "I.AMT"
								  , "O.S_QTY"
								  , "O.S_AMT"
								  , DB::raw("I.QTY - ISNULL(O.S_QTY, 0) AS REST")
								  , DB::raw("O.S_AMT - I.AMT AS BENEFIT")
								  , DB::raw("ISNULL(S.SALE_UNCS, 0) AS SALE_UNCS")
								  , DB::raw("ISNULL( ROUND(S.SALE_UNCS * (I.QTY - ISNULL(O.S_QTY, 0)), -2), 0) AS RES_AMT")
								  , DB::raw("
											 STUFF ( (SELECT DISTINCT ',' + (CUST.FRNM)
														FROM STOCK_INFO AS ST
															 LEFT OUTER JOIN CUST_CD AS CUST
																		  ON CUST.CUST_MK = ST.CUST_MK
																	     AND CUST.CORP_MK = ST.CORP_MK
												WHERE ST.CORP_MK = '".$this->getCorpId()."'
												  AND ST.PIS_MK     = I.PIS_MK
												  AND ST.SIZES      = I.SIZES
												  AND ST.ORIGIN_NM  = I.ORIGIN_NM
												  AND ST.INPUT_DATE = I.INPUT_DATE
												  AND ST.DE_CR_DIV  = 1
												  AND ST.OUTPUT_DATE IS NULL
												  AND CUST.CORP_DIV = 'P'
												  FOR XML PATH('')), 1, 1, '') AS CUST_FRNM ")
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS S_QTY, SUM(AMT) AS S_AMT 
										   FROM SALE_INFO_D 
										   WHERE (CORP_MK = '".$this->getCorpId()."') 
										    AND (INPUT_DATE >= '".$pWrite_date."') 
										    AND (INPUT_DATE <= '".$pEnd_date."') 
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS O
									  "), function($leftjoin){
											$leftjoin->on("I.PIS_MK", "=", "O.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "O.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "O.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "O.INPUT_DATE");
									  }
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, AVG(SALE_UNCS) AS SALE_UNCS 
										   FROM STOCK_SUJO 
										  WHERE (CORP_MK = '".$this->getCorpId()."') 
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS S 
									"), function($leftjoin){
											$leftjoin->on("I.PIS_MK", "=", "S.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "S.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "S.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "S.INPUT_DATE");
									  }
						)->join("PIS_INFO AS P", function($join){
							$join->on("I.PIS_MK", "=", "P.PIS_MK");
						})->where("I.UNCS", ">", "0")
						 ->get();

	}

	// 일일판매전체(매출)
	public function getSaleInfoData($pWrite_date, $pEnd_date){
		/*
		return $SALE_INFO = DB::table("SALE_INFO_D AS D")
						->select( "C.FRNM"
								, "P.PIS_NM"
								, "D.CORP_MK"
								, DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								, "D.ORIGIN_NM"
								, "D.ORIGIN_CD"
								, "D.CUST_MK"
								, "D.SEQ"
								, "D.PIS_MK"
								, "D.SIZES"
								, "D.QTY"
								, "D.UNCS"
								, "D.AMT"
								, "D.SRL_NO"
								, "D.REMARK"
						)->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");	
							}
						)->join("CUST_CD AS C", function($join){
								$join->on("D.CUST_MK", "=", "C.CUST_MK");
								$join->on("D.CORP_MK", "=", "C.CORP_MK");	
							}
						)->where( 'D.CORP_MK', $this->getCorpId())
						 ->where( 'D.WRITE_DATE', ">=", $pWrite_date)
						 ->where( 'D.WRITE_DATE', "<=",$pEnd_date)
		*/
		
		return $SALE_INFO = DB::table('SALE_INFO_M AS SM')
						->select( 'SM.CORP_MK'
								, DB::raw( "CONVERT(CHAR(10), SM.WRITE_DATE, 23) AS WRITE_DATE" )
								, 'SM.CUST_MK'
								, 'SM.SEQ'
								, 'SM.CASH_AMT'
								, 'SM.CHECK_AMT'
								, 'SM.UNCL_AMT'
								, 'SM.CARD_AMT'
								, 'SM.TOTAL_SALE_AMT'
								, 'C.FRNM'
								, DB::raw( "CASE SD.CNT WHEN 1 THEN P.PIS_NM 
											ELSE P.PIS_NM + '외' + CONVERT (VARCHAR , (SD.CNT - 1)) + '종' END AS PIS")
								, 'SD.SUMQTY'
								, 'SM.SALE_DISCOUNT'
								, 'P.PIS_MK'
								//, DB::raw("CASE WHEN SD.SIZES = 'QTYS' THEN '마리' END AS SIZES")

								, DB::raw("STUFF ( (SELECT DISTINCT ', <' + (P1.PIS_NM) + (D1.SIZES) + '(' + convert(VARCHAR, D1.QTY) + ') : ' +  replace( convert( VARCHAR, convert( MONEY, D1.UNCS ), 1 ), '.00', '' )  + '>' 
													  FROM SALE_INFO_D AS D1
													       INNER JOIN PIS_INFO AS P1
													               ON P1.PIS_MK = D1.PIS_MK
													 WHERE D1.CORP_MK    = '".$this->getCorpId()."'
													   AND D1.WRITE_DATE = SM.WRITE_DATE
													   AND D1.CUST_MK    = SM.CUST_MK
													   AND D1.SEQ        = SM.SEQ
													 GROUP BY D1.CORP_MK, D1.WRITE_DATE, D1.CUST_MK, D1.SEQ, D1.SIZES, P1.PIS_NM, D1.UNCS, D1.QTY
										  FOR XML PATH('')), 1, 1, '') AS PIS_LIST ")
								

						)->join(DB::raw(" 
									( SELECT CORP_MK, WRITE_DATE, CUST_MK, SEQ, COUNT(CORP_MK) AS CNT, SUM(QTY) AS SUMQTY, MAX(PIS_MK) AS PIS_MK
									  FROM SALE_INFO_D AS D 
									 WHERE CORP_MK = '".$this->getCorpId()."'
									 GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ ) SD  ")
								, function($join){
									$join->on("SM.CORP_MK", "=", "SD.CORP_MK" );
									$join->on("SM.WRITE_DATE", "=" ,"SD.WRITE_DATE");
									$join->on("SM.CUST_MK", "=" ,"SD.CUST_MK");
									$join->on("SM.SEQ", "=" ,"SD.SEQ");
						})->join("CUST_CD AS C", function($join){
								$join->on("SM.CUST_MK", "=" ,"C.CUST_MK");
								$join->on("SM.CORP_MK", "=" ,"C.CORP_MK");	
						})->join("PIS_INFO AS P", function($join){
								$join->on("SD.PIS_MK", "=" ,"P.PIS_MK");
						})->where('SM.CORP_MK', $this->getCorpId())
						 ->where( 'SM.WRITE_DATE', ">=", $pWrite_date)
						 ->where( 'SM.WRITE_DATE', "<=",$pEnd_date)
						 ->get();

	}


	// Excel용 전체 (입고)재고내역
	public function getStockInfoData($pWrite_date, $pEnd_date){
		
		return $STOCK_INFO_LIST = DB::table("STOCK_INFO")
					->select(
							   'PIS_INFO.PIS_NM'
							 , 'CUST_CD.FRNM'
							 , 'STOCK_INFO.SUJO_NO'
							 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.INPUT_DATE, 23) AS INPUT_DATE" )
							 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
							 , 'STOCK_INFO.PIS_MK'
							 , 'STOCK_INFO.SIZES'
							 , 'STOCK_INFO.SEQ'
							 , 'STOCK_INFO.REASON'
							 , 'STOCK_INFO.OUTLINE'
							 , 'STOCK_INFO.DE_CR_DIV'
							 , 'STOCK_INFO.QTY'
							 , 'STOCK_INFO.CUST_MK'
							 , 'STOCK_INFO.UNCS'
							 , 'STOCK_INFO.INPUT_AMT'
							 , 'STOCK_INFO.INPUT_DISCOUNT'
							 , 'STOCK_INFO.CURR_QTY'
							 , 'STOCK_INFO.ORIGIN_CD'
							 , 'STOCK_INFO.ORIGIN_NM'
							 , 'STOCK_INFO.INPUT_UNPROV'
							 , 'STOCK_INFO.CORP_MK'
							// , 'D.UNCS AS SALE_UNCS'
							 ,  DB::raw( "CASE WHEN STOCK_INFO.REASON = 1 THEN '감량' 
												WHEN STOCK_INFO.REASON = 2 THEN '폐사' 
												WHEN STOCK_INFO.REASON = 3 THEN '증량'
												WHEN STOCK_INFO.REASON =  4 THEN '기타'
																 ELSE '' END AS REASON_NM")
							 ,  DB::raw( "CASE STOCK_INFO.DE_CR_DIV WHEN 1 THEN ROUND(STOCK_INFO.QTY, 2) END AS IN_QTY " )
							 ,  DB::raw( "CASE STOCK_INFO.DE_CR_DIV WHEN 0 THEN ROUND(STOCK_INFO.QTY, 2) END AS OUT_QTY ")
					)->join("PIS_INFO", function($join){
						$join->on("STOCK_INFO.PIS_MK", '=',"PIS_INFO.PIS_MK");
					})->leftjoin("CUST_CD",  function($leftjoin){
						$leftjoin->on("STOCK_INFO.CORP_MK",'=', "CUST_CD.CORP_MK")
						->on("STOCK_INFO.CUST_MK", '=',"CUST_CD.CUST_MK");
					})
					/*
					->leftjoin("SALE_INFO_D AS D", function($join){
						$join->on("D.CORP_MK", '=',"STOCK_INFO.CORP_MK")
							 ->on("D.PIS_MK", '=',"STOCK_INFO.PIS_MK")
							 ->on("D.STOCK_INFO_SEQ", '=',"STOCK_INFO.SEQ") 
							 ->on("D.SIZES", '=',"STOCK_INFO.SIZES");
					})
					*/
					->where( 'STOCK_INFO.CORP_MK', $this->getCorpId())
					->where( 'STOCK_INFO.INPUT_DATE', ">=", $pWrite_date)
					->where( 'STOCK_INFO.INPUT_DATE', "<=",$pEnd_date)
					->orderBy('CUST_CD.FRNM', 'asc')
					->orderBy('STOCK_INFO.PIS_MK', 'asc')
					->orderBy('STOCK_INFO.SIZES', 'asc')
					->get();
	
	}

	//업체정보 리스트
	public function listData()
	{
		$CORP_INFO = DB::table('CORP_INFO')
			->select(
						DB::raw(" CASE	WHEN CORP_DIV = 'A' THEN '도매업체' 
										WHEN CORP_DIV = 'J' THEN '조합'
										WHEN CORP_DIV = 'P' THEN '생산자'
										ELSE NULL END AS CORP_DIV_NM"
								),
						'CORP_MK',
						'FRNM',
						'RPST',
						'PHONE_NO',
						'AREA_NM',
						'ACTIVE',
						'TAX_USE_YN'
					)
			->leftjoin('AREA_CD', function($join){
				$join->on("AREA_CD.AREA_CD", "=" ,"CORP_INFO.AREA_CD");
			})
			->where("FLAG","=", true)
			->where(function($query){
				
				if(!$this->chkSysAdmin()){
					$query->where("CORP_MK", $this->getCorpId());
				}
			
			});
						
		return Datatables::of($CORP_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CORP_MK" ){
							$query->where("CORP_INFO.CORP_MK",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRNM" ){
							$query->where("CORP_INFO.FRNM",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "RPST" ){
							$query->where("CORP_INFO.RPST",  "like", "%".Request::Input('textSearch')."%");
						}else{
							$query->where(function($q){
								$q->where("CORP_INFO.CORP_MK",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CORP_INFO.FRNM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CORP_INFO.RPST",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}
	
	public function setCorpTax(){
		
		DB::table("CORP_INFO")
			->where("CORP_MK", Request::input('CORP_MK') )
			->update(
				[
					"TAX_USE_YN" => Request::input('TAX_USE_YN'),
				]
			);

		return response()->json(['result' => 'success']);
	
	}

	public function setCorpActive(){
		
		DB::table("CORP_INFO")
			->where("CORP_MK", Request::input('CORP_MK') )
			->update(
				[
					"ACTIVE" => Request::input('ACTIVE'),
				]
			);

		return response()->json(['result' => 'success']);
	}

	//업체정보 인덱스페이지
	public function index($corp)
	{
		return view("corp.list",[ "corp" => $corp ] );
	}

	//업체정보 상세페이지
	public function detail($corp, $corp_mk)
	{
		$CORP_INFO = DB::table('CORP_INFO')
			->select(
						DB::raw(" CASE WHEN CORP_DIV = 'A' THEN '도매업체' 
										WHEN CORP_DIV = 'J' THEN '조합'
										WHEN CORP_DIV = 'P' THEN '생산자'
										ELSE NULL END AS CORP_DIV_NM"
								),
						DB::raw(" CASE WHEN ACTIVE = 'true' THEN '활성' 
										WHEN CORP_DIV = 'false' THEN '비활성'
										ELSE NULL END AS ACTIVE1"
								),
						DB::raw(" CASE WHEN APP_DIV = 'true' THEN '조합원' 
										WHEN CORP_DIV = 'false' THEN '비조합원'
										ELSE NULL END AS APP_DIV1"
								),
						DB::raw(" CASE WHEN TAX_USE_YN = 'Y' THEN '활성' 
										ELSE '비활성' END AS TAX_USE_YN1"
								),
						'*',
						DB::raw("CONVERT(CHAR(10), REG_DATE, 23) AS REG_DATE" )
					)
			->leftjoin('AREA_CD', function($join){
				$join->on("AREA_CD.AREA_CD", "=" ,"CORP_INFO.AREA_CD");
			})
			->where("FLAG","=", true)
			->where("CORP_MK","=", $corp_mk)
			->first();

		return view("corp.detail",
			[ 
				"corp" => $corp ,
				"corp_info" => $CORP_INFO
			] 
		);
	}

	//업체정보 수정
    public function update($corp, $corp_mk)
    {
		if( Request::isMethod('post') ){
			

			$validator = Validator::make( Request::Input(), [
				'CORP_MK'			=> 'required|max:20,NOT_NULL',
				'FRNM'				=> 'required|max:28,NOT_NULL',
				'RPST'				=> 'required|max:20,NOT_NULL',
				'CORP_DIV'			=> 'required|max:4, NOT_NULL',
				'ETPR_NO'			=> 'required|min:10,max:4, NOT_NULL',	
				'PHONE_NO'			=> 'required|max:13, NOT_NULL',	
				'RPST_EMAIL'		=> 'email',
				'REG_DATE'			=> 'date_format:"Y-m-d',
				'TAX_EDI_ID'		=> 'min:4',
				'TAX_EDI_PASS'		=> 'min:4',
				
			]);

			if ($validator->fails()) {
				return redirect()
					->back()
					->withInput(Request::all())
					->withErrors($validator)->withInput()->send();
			}
			
			// 식별자 부호 존재할 경우 체크
			if( Request::Has("TAX_EDI_ID") ){
				if( !$this->fnIsValidTaxIdentity( Request::Input("TAX_EDI_ID") ) ){
					return back()->withErrors([
						'TAX_EDI_ID' => '동일한 식별자 부호가 존재합니다.',
					])->withInput();
				}
			}

			if( !Request::Has('ACTIVE') ){
				$ACTIVE = "0";
			}else{
				$ACTIVE = "1";
			}

			if( !Request::Has('APP_DIV') ){
				$APP_DIV = "0";
			}else{
				$APP_DIV = "1";
			}


			DB::beginTransaction();
			try {
				DB::table("CORP_INFO")
					->where("CORP_MK", Request::input('CORP_MK') )
					->update(
						[
							"FRNM" =>	Request::input('FRNM'),
							"RPST" =>	Request::input('RPST'),
							"ETPR_NO" =>	Request::input('ETPR_NO'),
							"CORP_DIV" =>	Request::input('CORP_DIV'),
							"PHONE_NO" =>	Request::input('PHONE_NO'),
							"UPJONG" =>	Request::input('UPJONG'),
							"FAX" =>	Request::input('FAX'),
							"UPTE" =>	Request::input('UPTE'),
							"HP_NO" =>	Request::input('HP_NO'),
							"AREA_CD" =>	Request::input('AREA_CD'),
							"POST_NO" =>	Request::input('POST_NO'),
							"ADDR1" =>	Request::input('ADDR1'),
							"ADDR2" =>	Request::input('ADDR2'),
							"RPST_EMAIL" =>	Request::input('RPST_EMAIL'),
							"HOMEPAGE" =>	Request::input('HOMEPAGE'),
							"BANK_NM" =>	Request::input('BANK_NM'),
							"ACCOUNT_NO" =>	Request::input('ACCOUNT_NO'),
							"SAVER"		=> Request::input('SAVER'),
							"INTRO" =>	Request::input('INTRO'),
							"ACTIVE" => $ACTIVE,
							"APP_DIV"=> $APP_DIV,
							"REG_DATE"=>Request::Input("REG_DATE"),
							"TAX_USE_YN"=>Request::Input("TAX_USE_YN"),

							"TAX_EDI_ID"=>Request::Input("TAX_EDI_ID"),
							"TAX_EDI_PASS"=>Request::Input("TAX_EDI_PASS"),
						]
					);

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}

			DB::commit();
			//return response()->json(['result' => 'success']);
			//return view("corp.list",[ "corp" => $corp ] );
			return Redirect('/corp/cust_grp_info/index');
		}else{
			$CORP_INFO = DB::table('CORP_INFO')
				->select(
							DB::raw(" CASE WHEN CORP_DIV = 'A' THEN '도매업체' 
											WHEN CORP_DIV = 'J' THEN '조합'
											WHEN CORP_DIV = 'P' THEN '생산자'
											ELSE NULL END AS CORP_DIV_NM"
									),
							DB::raw(" CASE WHEN ACTIVE = 'true' THEN '활성' 
											WHEN CORP_DIV = 'false' THEN '비활성'
											ELSE NULL END AS ACTIVE1"
									),
							DB::raw(" CASE WHEN APP_DIV = 'true' THEN '조합원' 
											WHEN CORP_DIV = 'false' THEN '비조합원'
											ELSE NULL END AS APP_DIV1"
									),
							'*',
							DB::raw("CONVERT(CHAR(10), REG_DATE, 23) AS REG_DATE" )
						)
				->leftjoin('AREA_CD', function($join){
					$join->on("AREA_CD.AREA_CD", "=" ,"CORP_INFO.AREA_CD");
				})
				->where("FLAG","=", true)
				->where("CORP_MK","=", $corp_mk)
				->first();

			return view("corp.update",
				[ 
					"corp" => $corp ,
					"corp_info" => $CORP_INFO
				] 
			);
		}      
    }
	
	//업체정보 등록
    public function insert($corp)
    {
		if( Request::isMethod('post') ){

			$validator = Validator::make( Request::Input(), [
				'CORP_MK'			=> 'required|max:20,NOT_NULL',
				'FRNM'				=> 'required|max:28,NOT_NULL',
				'RPST'				=> 'required|max:20,NOT_NULL',
				'CORP_DIV'			=> 'required|max:4, NOT_NULL',
				'ETPR_NO'			=> 'required|min:10,max:4, NOT_NULL',	
				'PHONE_NO'			=> 'required|max:13, NOT_NULL',	
				'RPST_EMAIL'		=> 'email',
				'REG_DATE'			=> 'date_format:"Y-m-d',
				
			]);

			if ($validator->fails()) {
				return redirect()
					->back()
					->withInput(Request::all())
					->withErrors($validator)->withInput()->send();
			}

			// 식별자 부호 존재할 경우 체크
			if( Request::Has("TAX_EDI_ID") ){
				if( !$this->fnIsValidTaxIdentity( Request::Input("TAX_EDI_ID") ) ){
					return back()->withErrors([
						'TAX_EDI_ID' => '동일한 식별자 부호가 존재합니다.',
					])->withInput();
				}
			}
			
			if( !Request::Has('ACTIVE') ){
				$ACTIVE = "0";
			}else{
				$ACTIVE = "1";
			}

			if( !Request::Has('APP_DIV') ){
				$APP_DIV = "0";
			}else{
				$APP_DIV = "1";
			}

			// 동일한 키가 있는 경우 실패메시지
			$count = DB::table("CORP_INFO")
				->where('CORP_MK', Request::Input("CORP_MK"))
				->count();

			if( $count > 0){

				return back()->withErrors([
		            'CORP_MK' => '입력한 업체부호가 있습니다.',
	            ])->withInput();;
			}

			$valiEtpry_no = DB::table("CORP_INFO")->where("ETPR_NO", Request::Input("ETPR_NO"))->count();

			if( $valiEtpry_no > 0){
				return back()->withErrors([
		            'ETPR_NO' => '동일한 사업자 번호를 가진 업체가 있습니다',
	            ])->withInput();;
			}
			
			DB::beginTransaction();
			try {
				DB::table("CORP_INFO")
				->insert(
					[
						"CORP_MK" =>	Request::input('CORP_MK'),
						"FRNM" =>		Request::input('FRNM'),
						"RPST" =>		Request::input('RPST'),
						"ETPR_NO" =>	Request::input('ETPR_NO'),
						"CORP_DIV" =>	Request::input('CORP_DIV'),
						"PHONE_NO" =>	Request::input('PHONE_NO'),
						"UPJONG" =>	Request::input('UPJONG'),
						"FAX" =>	Request::input('FAX'),
						"UPTE" =>	Request::input('UPTE'),
						"HP_NO" =>	Request::input('HP_NO'),
						"AREA_CD" =>	Request::input('AREA_CD'),
						"POST_NO" =>	Request::input('POST_NO'),
						"ADDR1" =>	Request::input('ADDR1'),
						"ADDR2" =>	Request::input('ADDR2'),
						"RPST_EMAIL" =>	Request::input('RPST_EMAIL'),
						"HOMEPAGE" =>	Request::input('HOMEPAGE'),
						"BANK_NM" =>	Request::input('BANK_NM'),
						"ACCOUNT_NO" =>	Request::input('ACCOUNT_NO'),
						"SAVER"		=> Request::input('SAVER'),
						"INTRO" =>	Request::input('INTRO'),
						"ACTIVE" => $ACTIVE,
						"APP_DIV"=> $APP_DIV,
						"REG_DATE"=>Request::input('REG_DATE'),
						"TAX_USE_YN"=>Request::Input("TAX_USE_YN"),

						"TAX_EDI_ID"=>Request::Input("TAX_EDI_ID"),
						"TAX_EDI_PASS"=>Request::Input("TAX_EDI_PASS"),
					]
				);


				// 회사추가시 기본 그룹정보 입력
				DB::table("GRP_CD")
				->insert([
					"CORP_MK" =>	Request::input('CORP_MK'),
					"GRP_CD" =>		'A',
					"GRP_NM" =>		'관리자',
				]);
			}
			catch(ValidationException $e){
				DB::rollback();

				return Redirect::to("/accountcd/" . $corp)
						->withErrors($e->getErrors())
						->withInput();

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}

			DB::commit();
			
			return Redirect('/corp/cust_grp_info/index');

		}else{
			return view("corp.insert",
				[ 
					"corp" => $corp
				] 
			);
		}
	}

	// 세금계산서 식별자 아이디 유효성 체크
	private function fnIsValidTaxIdentity($pTAX_ID){
	
		$sETBIdenty	= $pTAX_ID;		//$eTaxInfo->TAX_EDI_ID;

		$nListCORP	= DB::table("CORP_INFO")
						->where("TAX_EDI_ID", $pTAX_ID)
						->where("CORP_MK", "<>", $this->getCorpId())
						->get();
		

		if( count($nListCORP) > 0){
			return false;
		}else{
			return true;
		}
	}

	// 회사/회원/기초정보 복사
	public function setCorpCopy(){
		
		return $exception = DB::transaction(function(){

			$validator = Validator::make( Request::Input(), [
				'CORP_MK'		=> 'required|max:20,NOT_NULL',
				'NEW_CORP_MK'	=> 'required|max:20,NOT_NULL',
				'NEW_CORP_FRNM'	=> 'required|max:28,NOT_NULL',
				'NEW_RPST'		=> 'required|max:20,NOT_NULL',
				'NEW_ETPR_NO'	=> 'required|max:10,NOT_NULL',
				
			]);

			if ($validator->fails()) {
				$errors = $validator->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'duplicate', 'message' => $errors], 422);
			}
			
			$cntCorpMk = DB::table("CORP_INFO")->where("CORP_MK", Request::Input("NEW_CORP_MK"))->count();
			if( $cntCorpMk > 0){
				return response()->json(['result' => 'duplicate', 'message' => '새로운 회사코드는 이미 존재합니다.'], 422);
			}

			$cntRpst = DB::table("CORP_INFO")->where("ETPR_NO", Request::Input("NEW_ETPR_NO"))->count();
			if( $cntRpst > 0){
				return response()->json(['result' => 'duplicate', 'message' => '새로운 사업자등록번호는 이미 존재합니다.'], 422);
			}


			try {
				
				// 1. 회사정보 복사
				$CORP_INFO = DB::table("CORP_INFO")->where("CORP_MK", Request::Input("CORP_MK"))->first();
				DB::table("CORP_INFO")
					->insert(
						[
							"CORP_MK"		=>	Request::Input('NEW_CORP_MK')
							,"FRNM"			=>	Request::Input('NEW_CORP_FRNM')
							,"RPST"			=>	Request::Input('NEW_RPST')
							,"ETPR_NO"		=>	Request::Input('NEW_ETPR_NO')
							,"CORP_DIV"		=>	$CORP_INFO->CORP_DIV
							,"PHONE_NO"		=>	$CORP_INFO->PHONE_NO
							,"UPJONG"		=>	$CORP_INFO->UPJONG
							,"FAX"			=>	$CORP_INFO->FAX
							,"UPTE"			=>	$CORP_INFO->UPTE
							,"HP_NO"		=>	$CORP_INFO->HP_NO
							,"AREA_CD"		=>	$CORP_INFO->AREA_CD
							,"POST_NO"		=>	$CORP_INFO->POST_NO
							,"ADDR1"		=>	$CORP_INFO->ADDR1
							,"ADDR2"		=>	$CORP_INFO->ADDR2
							,"RPST_EMAIL"	=>	$CORP_INFO->RPST_EMAIL
							,"HOMEPAGE"		=>	$CORP_INFO->HOMEPAGE
							,"BANK_NM"		=>	$CORP_INFO->BANK_NM
							,"ACCOUNT_NO"	=>	$CORP_INFO->ACCOUNT_NO
							,"SAVER"		=>	$CORP_INFO->SAVER
							,"INTRO"		=>	$CORP_INFO->INTRO
							,"ACTIVE"		=>	$CORP_INFO->ACTIVE
							,"APP_DIV"		=>	$CORP_INFO->APP_DIV
							,"REG_DATE"		=>	$CORP_INFO->REG_DATE
						]
				);

				// 2. 그룹코드 복사
				$GRP_CD	 = DB::table("GRP_CD")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach($GRP_CD	 as $grp_cd){
					DB::table("GRP_CD")->insert(
						[
							 'CORP_MK' => Request::Input('NEW_CORP_MK')
							,'GRP_CD' => $grp_cd->GRP_CD
							,'GRP_NM' => $grp_cd->GRP_NM
						]
					);
				}

				// 3. 사용자 복사(우선 한개만 복사)
				$member = DB::table("MEM_INFO")->where("CORP_MK", Request::Input("CORP_MK"))->first();

				//foreach( $MEM_INFO AS $member){
				
				if( $member !== null){
					DB::table("MEM_INFO")
						->insert(
							[
								"MEM_ID"		=>	Request::Input('NEW_CORP_MK')
								,"PWD_NO"		=>	DB::raw("PWDENCRYPT(1234)")
								,"GRP_CD"		=>	$member->GRP_CD
								,"CORP_MK"		=>	Request::Input('NEW_CORP_MK')
								,"GRADE"		=>	$member->GRADE
								,"PSTN"			=>	$member->PSTN
								,"NAME"			=>	$member->NAME
								,"JUMIN_NO"		=>	str_random(14)
								,"ADDR1"		=>	$member->ADDR1
								,"ADDR2"		=>	$member->ADDR2
								,"POST_NO"		=>	$member->POST_NO
								,"PHONE_NO"		=>	$member->PHONE_NO
								,"HP_NO"		=>	$member->HP_NO
								,"EMAIL"		=>	$member->EMAIL
								,"STATE"		=>	$member->STATE
								,"REG_IP"		=>	$member->REG_IP
								,"password"		=>	bcrypt('1111')
								,"remember_token" => $member->remember_token
							]
						);
					$user = User::where("MEM_ID", Request::Input('NEW_CORP_MK'))->first();

					$user->roles()->detach(10);
					if( $user->STATE == "1"){
						$user->roles()->attach(10);	 
					}else{
						$user->roles()->detach(10);
					}
								
				}

				// 4. 거래처정보 그룹
				$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach($CUST_GRP_INFO	 as $cust_grp_info){
					DB::table("CUST_GRP_INFO")
						->insert(
							[
								 'CORP_MK'		=> Request::Input('NEW_CORP_MK')
								,'CUST_GRP_CD'	=> $cust_grp_info->CUST_GRP_CD
								,'CUST_GRP_NM'	=> $cust_grp_info->CUST_GRP_NM
							]
					);
				}

				// 5. 거래처정보 
				$CUST_CD = DB::table("CUST_CD")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach($CUST_CD	as $cust){

					DB::table("CUST_CD")
						->insert(
							[
								'CORP_MK'		=> Request::Input("NEW_CORP_MK")
								,'CUST_MK'		=> $cust->CUST_MK
								,'CUST_GRP_CD'	=> $cust->CUST_GRP_CD
								,'CORP_DIV'		=> $cust->CORP_DIV
								,'FRNM'			=> $cust->FRNM
								,'RPST'			=> $cust->RPST
								,'RANK'			=> $cust->RANK
								,'ETPR_NO'		=> $cust->ETPR_NO
								,'JUMIN_NO'		=> $cust->JUMIN_NO
								,'PHONE_NO'		=> $cust->PHONE_NO
								,'FAX'			=> $cust->FAX
								,'ADDR1'		=> $cust->ADDR1
								,'ADDR2'		=> $cust->ADDR2
								,'POST_NO'		=> $cust->POST_NO
								,'UPJONG'		=> $cust->UPJONG
								,'UPTE'			=> $cust->UPTE
								,'AREA_CD'		=> $cust->AREA_CD
								,'REMARK'		=> $cust->REMARK
								,'ADDR_3'		=> $cust->ADDR_3
							]
					);
				}
				

				// 6. 비용계정그룹
				$ACCOUNT_GRP = DB::table("ACCOUNT_GRP")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach( $ACCOUNT_GRP as $accountGrp){
					DB::table("ACCOUNT_GRP")
						->insert(
							[
								'CORP_MK'			=> Request::Input("NEW_CORP_MK")
								,'ACCOUNT_GRP_CD'	=> $accountGrp->ACCOUNT_GRP_CD
								,'ACCOUNT_GRP_NM'	=> $accountGrp->ACCOUNT_GRP_NM
							]
						);
						
				}
				
				// 7. 비용계정
				$ACCOUNT_CD = DB::table("ACCOUNT_CD")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach( $ACCOUNT_CD as $account){
					DB::table("ACCOUNT_CD")->insert(
							[
								'CORP_MK'			=> Request::Input("NEW_CORP_MK")
								,'ACCOUNT_MK'		=> $account->ACCOUNT_MK
								,'ACCOUNT_NM'		=> $account->ACCOUNT_NM
								,'DE_CR_DIV'		=> $account->DE_CR_DIV
								,'ACCOUNT_GRP_CD'	=> $account->ACCOUNT_GRP_CD
							]
					);
				}
				
				//8. 비용전표 
				$CHIT_INFO = DB::table("CHIT_INFO")->where("CORP_MK", Request::Input("CORP_MK"))->get();
				foreach( $CHIT_INFO as $chitInfo){

					DB::table("CHIT_INFO")->insert(
						[
							'CORP_MK'		=> Request::Input("NEW_CORP_MK")
							,'WRITE_DATE'	=> $chitInfo->WRITE_DATE
							,'SEQ'			=> $chitInfo->SEQ
							,'DE_CR_DIV'	=> $chitInfo->DE_CR_DIV
							,'CUST_MK'		=> $chitInfo->CUST_MK
							,'ACCOUNT_MK'	=> $chitInfo->ACCOUNT_MK
							,'AMT'			=> $chitInfo->AMT
							,'OUTLINE'		=> $chitInfo->OUTLINE
							,'REMARK'		=> $chitInfo->REMARK
						]
					);
				}


				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			} catch(Exception $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}
		});
		
	
	}
}

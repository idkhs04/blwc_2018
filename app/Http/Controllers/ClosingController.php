<?php

namespace App\Http\Controllers;

use App\CLOSING;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\Http\Requests;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class ClosingController extends Controller
{
    // 결산표 조회화면
	public function index($closing)
	{
		
		$today		= date('Y-m-d');
		$yesterDay	= date('Y-m-d', strtotime('-1 day', time()));

		//DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".$yesterDay."','".$yesterDay."'"));
		//DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".$today."','".$today."'"));

		return view("closing.list", [ "closing" => $closing ] );
	}

	// 기간별 판매 목록 조회
	public function listData()
	{
		//DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".Request::Input("before_start")."','".Request::Input("start_date")."'"));
		DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".Request::Input("start_date")."','".Request::Input("end_date")."'"));
		
		$CLOSING = DB::table("CLOSING")
							->select(  "SALE_AMT"
									  , "BUY_AMT"
									  , DB::raw("SALE_AMT - BUY_AMT - COST AS TOTAL_GAIN")
									  , "CLOSING_SEQ"
									  , "CORP_MK"
									  , DB::raw("SUBSTRING(CONVERT (CHAR(10), CLOSING_DATE, 20), 1, 10) AS CLOSING_DATE")
									  , DB::raw("SUBSTRING(CONVERT (CHAR(10), CLOSING_UPDATE_DATE, 20), 1, 10) AS CLOSING_UPDATE_DATE")
									  , "ACCRUED"
									  , "COLLECTION_AMT"
									  , "COLLECTION_CASH"
									  , "COLLECTION_BANK"
									  , "COLLECTION_CARD"
									  , "CREDIT"
									  , "PAYMENT"
									  , "PAYMENT_CASH"
									  , "PAYMENT_BANK"
									  , "PAYMENT_CARD"
									  , "SALE_WEIGHT"
									  , "SALE_ACCRUED"
									  , "SALE_COLLECTION"
									  , "BUY_WEIGHT"
									  , "BUY_CREDIT"
									  , "BUY_PAYMENT"
									  , "COST"
									  , "COST_CASH"
									  , "COST_BANK"
									  , "COST_CARD")
							->where("CORP_MK", $this->getCorpId())
							->where(function($query){
									if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
										if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
											$query->whereBetween("CLOSING_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
										}
									}		  
							});

		return Datatables::of($CLOSING)->make(true);
	}

	// 결산하기
	public function setClosing(){

		$validator = Validator::make( Request::Input(), [
			'CLOSING_DATE'		=> 'required|date_format:"Y-m-d',
			'CLOSING_DATE_END'	=> 'required|date_format:"Y-m-d',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){

			try {

				DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".Request::Input("CLOSING_DATE")."','".Request::Input("CLOSING_DATE_END")."'"));
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();

					if (is_object($message)) {
						$message = $message->toArray();
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	}
	// 지출내역 목록
	public function detailList(){
		

		$CHIT_INFO = DB::table("CHIT_INFO AS I")
							->select(  "I.CORP_MK"
									  ,"I.WRITE_DATE"
									  ,"I.SEQ"
									  ,"I.DE_CR_DIV"
									  , DB::raw(" CASE WHEN I.DE_CR_DIV = '1' THEN '지출' ELSE '수입' END AS DIV_NM ")
									  ,"I.CUST_MK"
									  ,"I.ACCOUNT_MK"
									  ,"I.OUTLINE"
									  , DB::raw(" CASE WHEN I.DE_CR_DIV = '0' THEN ( I.AMT * (-1))  ELSE I.AMT END AS AMT ")
									 // ,"I.AMT"
									  ,"I.REMARK"
									  ,"AC.ACCOUNT_NM"
									  ,DB::raw("(CASE OUTLINE WHEN '현금' THEN AMT ELSE NULL END) AS CASH")
									  ,DB::raw("(CASE OUTLINE WHEN '카드' THEN AMT ELSE NULL END) AS CARD")
									  ,DB::raw("(CASE OUTLINE WHEN '통장' THEN AMT ELSE NULL END) AS ONLINE")
									  ,"C.FRNM")
							->leftjoin("CUST_CD AS C", function($join){
									$join->on("I.CORP_MK", "=", "C.CORP_MK");
									$join->on("I.CUST_MK", "=", "C.CUST_MK" );
								}
							)->leftjoin("ACCOUNT_CD AS AC", function($join){
									$join->on("I.CORP_MK", "=", "AC.CORP_MK");
									$join->on("I.ACCOUNT_MK", "=", "AC.ACCOUNT_MK");
								}
							)
							->where("I.CORP_MK", $this->getCorpId())
							//->where("I.DE_CR_DIV", "1")
							->where("I.WRITE_DATE", ">=",  Request::Input("start_date") )
							->where("I.WRITE_DATE", "<=", Request::Input("end_date") );


		return Datatables::of($CHIT_INFO)->make(true);
	}

	// 결산표 조회 (상세페이지임)
	public function detail($closing, $START_DATE, $END_DATE)
	{

		$closing_date		= Request::segment(4);
		$closing_date_end	= Request::segment(5);
		
		$CLOSING = DB::table(DB::raw("(SELECT    MAX(CORP_MK) AS CORP_MK, 
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) AS SALE_AMOUNT,    
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) - SUM(COST) AS TOTAL_GAIN,    
												 SUM(ACCRUED) as ACCRUED, 
												 SUM(COLLECTION_AMT) as COLLECTION_AMT, 
												 SUM(COLLECTION_CASH) as COLLECTION_CASH, 
												 SUM(COLLECTION_BANK) as COLLECTION_BANK, 
												 SUM(COLLECTION_CARD) as COLLECTION_CARD, 
												 SUM(COLLECTION_ETC) as COLLECTION_ETC,   
												 SUM(CREDIT) as CREDIT, 
												 SUM(PAYMENT) as PAYMENT, 
												 SUM(PAYMENT_CASH) as PAYMENT_CASH, SUM(PAYMENT_BANK) as PAYMENT_BANK, 
												 SUM(PAYMENT_CARD) as PAYMENT_CARD, SUM(PAYMENT_ETC) as PAYMENT_ETC,  
												 SUM(SALE_WEIGHT) as SALE_WEIGHT, 
												 SUM(SALE_AMT) as SALE_AMT, 
												 SUM(SALE_ACCRUED) as SALE_ACCRUED, 
												 SUM(SALE_COLLECTION) as SALE_COLLECTION,   
												 SUM(BUY_WEIGHT) as BUY_WEIGHT, 
												 SUM(BUY_AMT) as BUY_AMT, 
												 SUM(BUY_CREDIT) as BUY_CREDIT, 
												 SUM(BUY_PAYMENT) as BUY_PAYMENT,   
												 SUM(COST) as COST, 
												 SUM(COST_CASH) as COST_CASH, 
												 SUM(COST_BANK) as COST_BANK, 
												 SUM(COST_CARD) as COST_CARD 
									   FROM  CLOSING
									  WHERE CORP_MK =  '".$this->getCorpId()."'
									    AND CLOSING_DATE >=  '".$closing_date."'
									    AND CLOSING_DATE <=  '".$closing_date_end."'
									  GROUP BY CORP_MK ) AS A")
							)->leftjoin(DB::raw("
									 (SELECT SALE_ACCRUED + SALE_COLLECTION AS SALE_AMOUNT
										, SALE_ACCRUED + SALE_COLLECTION - COST AS TOTAL_GAIN
										, CLOSING_SEQ
										, CORP_MK
										, ACCRUED
										, COLLECTION_AMT
										, COLLECTION_CASH
										, COLLECTION_BANK
										, COLLECTION_CARD
										, CREDIT
										, PAYMENT
										, PAYMENT_CASH
										, PAYMENT_BANK
										, PAYMENT_CARD
										, SALE_WEIGHT
										, SALE_AMT
										, SALE_ACCRUED
										, SALE_COLLECTION
										, BUY_WEIGHT
										, BUY_AMT
										, BUY_CREDIT
										, BUY_PAYMENT
									 FROM CLOSING
									 WHERE CORP_MK =  '".$this->getCorpId()."'
									   AND closing_date = DATEADD(dd,-1, '".$closing_date."' )
									) AS B"), function($join){
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin(
								DB::raw("(SELECT   MAX(CORP_MK) AS CORP_MK
												 , SUM(CAST(ACCRUED AS BIGINT) - cast(COLLECTION_AMT as BIGINT)) AS ALL_ACCRUED
												 , SUM(CAST(CREDIT AS BIGINT)  - cast(PAYMENT as BIGINT)) AS ALL_CREDIT
											FROM CLOSING
										   WHERE CORP_MK =  '".$this->getCorpId()."'
										   GROUP BY CORP_MK
										 ) AS C"), function($join){
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->select( DB::raw("ISNULL(A.SALE_AMOUNT, 0) as SALE_AMOUNT")
									 , DB::raw("ISNULL(A.TOTAL_GAIN, 0) as TOTAL_GAIN")
									 , DB::raw("ISNULL(A.ACCRUED, 0) as ACCRUED")
									 , DB::raw("ISNULL(A.COLLECTION_AMT, 0) as COLLECTION_AMT")
									 , DB::raw("ISNULL(A.COLLECTION_CASH, 0) as COLLECTION_CASH")
									 , DB::raw("ISNULL(A.COLLECTION_BANK, 0) as COLLECTION_BANK")
									 , DB::raw("ISNULL(A.COLLECTION_CARD, 0) as COLLECTION_CARD")
									 , DB::raw("ISNULL(A.COLLECTION_ETC, 0) as COLLECTION_ETC")
									 , DB::raw("ISNULL(A.CREDIT, 0) as CREDIT")
									 , DB::raw("ISNULL(A.PAYMENT, 0) as PAYMENT")
									 , DB::raw("ISNULL(A.PAYMENT_CASH, 0) as PAYMENT_CASH")
									 , DB::raw("ISNULL(A.PAYMENT_BANK, 0) as PAYMENT_BANK")
									 , DB::raw("ISNULL(A.PAYMENT_CARD, 0) as PAYMENT_CARD")
									 , DB::raw("ISNULL(A.PAYMENT_ETC, 0) as PAYMENT_ETC")
									 , DB::raw("ROUND(ISNULL(A.SALE_WEIGHT, 0),1) as SALE_WEIGHT")
									 , DB::raw("ISNULL(A.SALE_AMT, 0) as SALE_AMT")
									 , DB::raw("ISNULL(A.SALE_ACCRUED, 0) as SALE_ACCRUED")
									 , DB::raw("ISNULL(A.SALE_COLLECTION, 0) as SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(A.BUY_WEIGHT, 0),1) as BUY_WEIGHT")
									 , DB::raw("ISNULL(A.BUY_AMT, 0) as BUY_AMT")
									 , DB::raw("ISNULL(A.BUY_CREDIT, 0) as BUY_CREDIT")
									 , DB::raw("ISNULL(A.BUY_PAYMENT, 0) as BUY_PAYMENT")
									 , DB::raw("ISNULL(A.COST, 0) as COST")
									 , DB::raw("ISNULL(A.COST_CASH, 0) as COST_CASH")
									 , DB::raw("ISNULL(A.COST_BANK, 0) as COST_BANK")
									 , DB::raw("ISNULL(A.COST_CARD, 0) as COST_CARD")
									 , DB::raw("ISNULL(B.ACCRUED, 0) AS BEFORE_ACCRUED")
									 , DB::raw("ISNULL(B.CREDIT, 0) AS BEFORE_CREDIT")
									 , DB::raw("ROUND(ISNULL(B.SALE_WEIGHT, 0),1) AS BEFORE_SALE_WEIGHT")
									 , DB::raw("ISNULL(B.SALE_AMT, 0) AS BEFORE_SALE_AMT")
									 , DB::raw("ISNULL(B.SALE_ACCRUED, 0) AS BEFORE_SALE_ACCRUED")
									 , DB::raw("ISNULL(B.SALE_COLLECTION, 0) AS BEFORE_SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(B.BUY_WEIGHT, 0),1) AS BEFORE_BUY_WEIGHT")
									 , DB::raw("ISNULL(B.BUY_AMT, 0) AS BEFORE_BUY_AMT")
									 , DB::raw("ISNULL(B.BUY_CREDIT, 0) AS BEFORE_BUY_CREDIT")
									 , DB::raw("ISNULL(B.BUY_PAYMENT, 0) AS BEFORE_BUY_PAYMENT")
									 , DB::raw("ISNULL(C.ALL_ACCRUED, 0) AS ALL_ACCRUED")
									 , DB::raw("ISNULL(C.ALL_CREDIT, 0) AS ALL_CREDIT")
									 , DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', NULL, NULL) ) AS UNPROV_ALL")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', NULL, NULL)) AS UNCL_ALL")

							)->first();

		//dd( DB::getQueryLog() );
		//dd($CLOSING);
		if( $CLOSING === null){
			return Redirect::back()->with('alert-success', '결산데이터가 존재 하지 않습니다.')->send();
		}
		return view("closing.detail", [ "closing" => $closing, "model" => $CLOSING, "START_DATE" => strVal($START_DATE), "END_DATE" => strVal($END_DATE)] );
	}

	

	// 기간별 결산표
	public function detailListArrange($closing,$START_DATE, $END_DATE){

		$closing_date		= Request::segment(4);
		$closing_date_end	= Request::segment(5);
		
		$CLOSING = DB::table(DB::raw("(SELECT    MAX(CORP_MK) AS CORP_MK, 
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) AS SALE_AMOUNT,    
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) - SUM(COST) AS TOTAL_GAIN,    
												 SUM(ACCRUED) as ACCRUED, 
												 SUM(COLLECTION_AMT) as COLLECTION_AMT, 
												 SUM(COLLECTION_CASH) as COLLECTION_CASH, 
												 SUM(COLLECTION_BANK) as COLLECTION_BANK, 
												 SUM(COLLECTION_CARD) as COLLECTION_CARD, 
												 SUM(COLLECTION_ETC) as COLLECTION_ETC,   
												 SUM(CREDIT) as CREDIT, 
												 SUM(PAYMENT) as PAYMENT, 
												 SUM(PAYMENT_CASH) as PAYMENT_CASH, SUM(PAYMENT_BANK) as PAYMENT_BANK, 
												 SUM(PAYMENT_CARD) as PAYMENT_CARD, SUM(PAYMENT_ETC) as PAYMENT_ETC,  
												 SUM(SALE_WEIGHT) as SALE_WEIGHT, 
												 SUM(SALE_AMT) as SALE_AMT, 
												 SUM(SALE_ACCRUED) as SALE_ACCRUED, 
												 SUM(SALE_COLLECTION) as SALE_COLLECTION,   
												 SUM(BUY_WEIGHT) as BUY_WEIGHT, 
												 SUM(BUY_AMT) as BUY_AMT, 
												 SUM(BUY_CREDIT) as BUY_CREDIT, 
												 SUM(BUY_PAYMENT) as BUY_PAYMENT,   
												 SUM(COST) as COST, 
												 SUM(COST_CASH) as COST_CASH, 
												 SUM(COST_BANK) as COST_BANK, 
												 SUM(COST_CARD) as COST_CARD 
									   FROM  CLOSING
									  WHERE CORP_MK =  '".$this->getCorpId()."'
									    AND CLOSING_DATE >=  '".$closing_date."'
									    AND CLOSING_DATE <=  '".$closing_date_end."'
									  GROUP BY CORP_MK ) AS A")
							)->leftjoin(DB::raw("
									 (SELECT SALE_ACCRUED + SALE_COLLECTION AS SALE_AMOUNT
										, SALE_ACCRUED + SALE_COLLECTION - COST AS TOTAL_GAIN
										, CLOSING_SEQ
										, CORP_MK
										, ACCRUED
										, COLLECTION_AMT
										, COLLECTION_CASH
										, COLLECTION_BANK
										, COLLECTION_CARD
										, CREDIT
										, PAYMENT
										, PAYMENT_CASH
										, PAYMENT_BANK
										, PAYMENT_CARD
										, SALE_WEIGHT
										, SALE_AMT
										, SALE_ACCRUED
										, SALE_COLLECTION
										, BUY_WEIGHT
										, BUY_AMT
										, BUY_CREDIT
										, BUY_PAYMENT
									 FROM CLOSING
									 WHERE CORP_MK =  '".$this->getCorpId()."'
									   AND closing_date = DATEADD(dd,-1, '".$closing_date."' )
									) AS B"), function($join){
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin(
								DB::raw("(SELECT   MAX(CORP_MK) AS CORP_MK
												 , SUM(CAST(ACCRUED AS BIGINT) - cast(COLLECTION_AMT as BIGINT)) AS ALL_ACCRUED
												 , SUM(CAST(CREDIT AS BIGINT)  - cast(PAYMENT as BIGINT)) AS ALL_CREDIT
											FROM CLOSING
										   WHERE CORP_MK =  '".$this->getCorpId()."'
										   GROUP BY CORP_MK
										 ) AS C"), function($join){
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->select( DB::raw("ISNULL(A.SALE_AMOUNT, 0) as SALE_AMOUNT")
									 , DB::raw("ISNULL(A.TOTAL_GAIN, 0) as TOTAL_GAIN")
									 , DB::raw("ISNULL(A.ACCRUED, 0) as ACCRUED")
									 , DB::raw("ISNULL(A.COLLECTION_AMT, 0) as COLLECTION_AMT")
									 , DB::raw("ISNULL(A.COLLECTION_CASH, 0) as COLLECTION_CASH")
									 , DB::raw("ISNULL(A.COLLECTION_BANK, 0) as COLLECTION_BANK")
									 , DB::raw("ISNULL(A.COLLECTION_CARD, 0) as COLLECTION_CARD")
									 , DB::raw("ISNULL(A.COLLECTION_ETC, 0) as COLLECTION_ETC")
									 , DB::raw("ISNULL(A.CREDIT, 0) as CREDIT")
									 , DB::raw("ISNULL(A.PAYMENT, 0) as PAYMENT")
									 , DB::raw("ISNULL(A.PAYMENT_CASH, 0) as PAYMENT_CASH")
									 , DB::raw("ISNULL(A.PAYMENT_BANK, 0) as PAYMENT_BANK")
									 , DB::raw("ISNULL(A.PAYMENT_CARD, 0) as PAYMENT_CARD")
									 , DB::raw("ISNULL(A.PAYMENT_ETC, 0) as PAYMENT_ETC")
									 , DB::raw("ROUND(ISNULL(A.SALE_WEIGHT, 0),1) as SALE_WEIGHT")
									 , DB::raw("ISNULL(A.SALE_AMT, 0) as SALE_AMT")
									 , DB::raw("ISNULL(A.SALE_ACCRUED, 0) as SALE_ACCRUED")
									 , DB::raw("ISNULL(A.SALE_COLLECTION, 0) as SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(A.BUY_WEIGHT, 0),1) as BUY_WEIGHT")
									 , DB::raw("ISNULL(A.BUY_AMT, 0) as BUY_AMT")
									 , DB::raw("ISNULL(A.BUY_CREDIT, 0) as BUY_CREDIT")
									 , DB::raw("ISNULL(A.BUY_PAYMENT, 0) as BUY_PAYMENT")
									 , DB::raw("ISNULL(A.COST, 0) as COST")
									 , DB::raw("ISNULL(A.COST_CASH, 0) as COST_CASH")
									 , DB::raw("ISNULL(A.COST_BANK, 0) as COST_BANK")
									 , DB::raw("ISNULL(A.COST_CARD, 0) as COST_CARD")
									 , DB::raw("ISNULL(B.ACCRUED, 0) AS BEFORE_ACCRUED")
									 , DB::raw("ISNULL(B.CREDIT, 0) AS BEFORE_CREDIT")
									 , DB::raw("ROUND(ISNULL(B.SALE_WEIGHT, 0),1) AS BEFORE_SALE_WEIGHT")
									 , DB::raw("ISNULL(B.SALE_AMT, 0) AS BEFORE_SALE_AMT")
									 , DB::raw("ISNULL(B.SALE_ACCRUED, 0) AS BEFORE_SALE_ACCRUED")
									 , DB::raw("ISNULL(B.SALE_COLLECTION, 0) AS BEFORE_SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(B.BUY_WEIGHT, 0),1) AS BEFORE_BUY_WEIGHT")
									 , DB::raw("ISNULL(B.BUY_AMT, 0) AS BEFORE_BUY_AMT")
									 , DB::raw("ISNULL(B.BUY_CREDIT, 0) AS BEFORE_BUY_CREDIT")
									 , DB::raw("ISNULL(B.BUY_PAYMENT, 0) AS BEFORE_BUY_PAYMENT")
									 , DB::raw("ISNULL(C.ALL_ACCRUED, 0) AS ALL_ACCRUED")
									 , DB::raw("ISNULL(C.ALL_CREDIT, 0) AS ALL_CREDIT")
									 , DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', NULL, NULL) ) AS UNPROV_ALL")
									, DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."') ) AS UNPROV_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."')) AS UNCL_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', NULL, NULL)) AS UNCL_ALL")

							)->first();

		if( $CLOSING === null){
			return Redirect::back()->with('alert-success', '해당기간엔 기간별결산표 데이터가 없습니다.')->send();
		}
		return view("closing.listDetailArrange", [ "closing" => $closing, "model" => $CLOSING, "START_DATE" => strVal($closing_date), "END_DATE" => strVal($closing_date_end)] );
	}

	public function getClosingUnclList(){

		$UNCL_CLS = DB::table("UNCL_INFO AS UNCL")
							->join("CUST_CD AS CUST" , function($join){
									$join->on("UNCL.UNCL_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNCL.CORP_MK", "=", "CUST.CORP_MK");
							})->select("UNCL.CORP_MK"
									 , DB::raw("CONVERT(CHAR(10), UNCL.WRITE_DATE, 23) AS WRITE_DATE")
									 , "UNCL.UNCL_CUST_MK"
									 , "UNCL.SEQ"
									 , "UNCL.PROV_DIV"
									 , DB::raw(" CASE WHEN UNCL.REMARK = '현금' AND UNCL.PROV_DIV <> '0' THEN AMT END AS 'CASH'")
									 , DB::raw(" CASE WHEN UNCL.REMARK = 'CARD' AND UNCL.PROV_DIV <> '0' THEN AMT END AS 'CARD'")
									 , DB::raw(" CASE WHEN UNCL.REMARK = '통장(입금)' AND UNCL.PROV_DIV <> '0' THEN AMT END AS 'CHECK'")
									 , DB::raw(" CASE WHEN UNCL.REMARK = '판매미수'  AND UNCL.PROV_DIV <> '0' THEN AMT END AS 'UNCL'")
									 , DB::raw(" CASE WHEN UNCL.REMARK <> '현금' AND UNCL.REMARK <> 'CARD' AND UNCL.REMARK <> '통장(입금)' AND UNCL.REMARK <> '판매미수' AND UNCL.PROV_DIV <> '0' THEN AMT END AS 'REC'")
									 , "UNCL.CASH_AMT"
									 , "UNCL.CHECK_AMT"
									 , "UNCL.CARD_AMT"
									 , "UNCL.UNCL_AMT"
									 , "UNCL.AMT"
									 , "CUST.FRNM")
							->where("UNCL.CORP_MK", $this->getCorpId())
							/*
							->where("CUST.CORP_DIV", 'S')
							->where("CUST.INTERFACE_YN", 'Y')
							*/
							->where("UNCL.PROV_DIV", "1")
							->where("UNCL.WRITE_DATE", ">=", Request::Input("WRITE_DATE_START"))
							->where("UNCL.WRITE_DATE", "<=", Request::Input("WRITE_DATE_END"))
							->where(function($query){
								//$query->whereNotNull("UNCL.CASH_AMT");
								//$query->whereNotNull("UNCL.CHECK_AMT");
								//$query->whereNotNull("UNCL.CARD_AMT");
								//$query->whereNotNull("UNCL.UNCL_AMT");
							});


		return Datatables::of($UNCL_CLS)->make(true);

	}

	public function getClosingUnprovList(){

		$UNPROV_CLS = DB::table("UNPROV_INFO AS UNP")
							->join("CUST_CD AS CUST" , function($join){
									$join->on("UNP.PROV_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNP.CORP_MK", "=", "CUST.CORP_MK");
							})->select("UNP.CORP_MK"
									 , DB::raw("CONVERT(CHAR(10), UNP.WRITE_DATE, 23) AS WRITE_DATE")
									 , "UNP.PROV_CUST_MK"
									 , "UNP.PROV_DIV"
									 , DB::raw(" CASE WHEN UNP.REMARK = '현금' AND UNP.PROV_DIV = '0' THEN AMT END AS 'CASH' ")
									 , DB::raw(" CASE WHEN UNP.REMARK = '통장(입금)' AND UNP.PROV_DIV = '0' THEN AMT END AS 'CHECK' ")
									 , DB::raw(" CASE WHEN UNP.REMARK = 'CARD' AND UNP.PROV_DIV = '0' THEN AMT END AS 'CARD' ")
									 , DB::raw(" CASE WHEN UNP.REMARK <> '현금' AND UNP.REMARK <> '통장(입금)' AND UNP.REMARK <> 'CARD' AND UNP.PROV_DIV = '0' THEN AMT END AS 'ETC' ")
									 , "CUST.FRNM")
							->where("UNP.CORP_MK", $this->getCorpId())
							/*
							->where("CUST.CORP_DIV", 'P')
							->where("CUST.INTERFACE_YN", 'Y')
							*/
							->whereNull("UNP.STOCK_PIS_MK")
							->where("UNP.WRITE_DATE", ">=", Request::Input("WRITE_DATE_START"))
							->where("UNP.WRITE_DATE", "<=", Request::Input("WRITE_DATE_END"));


		return Datatables::of($UNPROV_CLS)->make(true);
	}

	public function pdf(){

		$closing_date		= Request::segment(4);
		$closing_date_end	= Request::segment(5);

		$CLOSING = DB::table(DB::raw("(SELECT    MAX(CORP_MK) AS CORP_MK, 
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) AS SALE_AMOUNT,    
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) - SUM(COST) AS TOTAL_GAIN,    
												 SUM(ACCRUED) as ACCRUED, 
												 SUM(COLLECTION_AMT) as COLLECTION_AMT, 
												 SUM(COLLECTION_CASH) as COLLECTION_CASH, 
												 SUM(COLLECTION_BANK) as COLLECTION_BANK, 
												 SUM(COLLECTION_CARD) as COLLECTION_CARD, 
												 SUM(COLLECTION_ETC) as COLLECTION_ETC,   
												 SUM(CREDIT) as CREDIT, SUM(PAYMENT) as PAYMENT, 
												 SUM(PAYMENT_CASH) as PAYMENT_CASH, SUM(PAYMENT_BANK) as PAYMENT_BANK, 
												 SUM(PAYMENT_CARD) as PAYMENT_CARD, SUM(PAYMENT_ETC) as PAYMENT_ETC,  
												 SUM(SALE_WEIGHT) as SALE_WEIGHT, 
												 SUM(SALE_AMT) as SALE_AMT, 
												 SUM(SALE_ACCRUED) as SALE_ACCRUED, 
												 SUM(SALE_COLLECTION) as SALE_COLLECTION,   
												 SUM(BUY_WEIGHT) as BUY_WEIGHT, 
												 SUM(BUY_AMT) as BUY_AMT, 
												 SUM(BUY_CREDIT) as BUY_CREDIT, 
												 SUM(BUY_PAYMENT) as BUY_PAYMENT,   
												 SUM(COST) as COST, 
												 SUM(COST_CASH) as COST_CASH, 
												 SUM(COST_BANK) as COST_BANK, 
												 SUM(COST_CARD) as COST_CARD 
									   FROM  CLOSING
									  WHERE CORP_MK =  '".$this->getCorpId()."'
									    AND CLOSING_DATE >=  '".$closing_date."'
									    AND CLOSING_DATE <=  '".$closing_date_end."'
									  GROUP BY CORP_MK ) AS A")
							)->leftjoin(DB::raw("(SELECT SALE_ACCRUED + SALE_COLLECTION AS SALE_AMOUNT
										, SALE_ACCRUED + SALE_COLLECTION - COST AS TOTAL_GAIN
										, CLOSING_SEQ
										, CORP_MK
										, ACCRUED
										, COLLECTION_AMT
										, COLLECTION_CASH
										, COLLECTION_BANK
										, COLLECTION_CARD
										, CREDIT
										, PAYMENT
										, PAYMENT_CASH
										, PAYMENT_BANK
										, PAYMENT_CARD
										, SALE_WEIGHT
										, SALE_AMT
										, SALE_ACCRUED
										, SALE_COLLECTION
										, BUY_WEIGHT
										, BUY_AMT
										, BUY_CREDIT
										, BUY_PAYMENT
									 FROM CLOSING
									 WHERE CORP_MK =  '".$this->getCorpId()."'
									   AND closing_date = DATEADD(dd,-1, '".$closing_date."' )
									) AS B"), function($join){
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin(
								DB::raw("(SELECT MAX(CORP_MK) AS CORP_MK
												 , SUM(CAST(ACCRUED AS BIGINT) - cast(COLLECTION_AMT as BIGINT)) AS ALL_ACCRUED
												 , SUM(CAST(CREDIT AS BIGINT) - cast(PAYMENT as BIGINT)) AS ALL_CREDIT
											FROM CLOSING
										    WHERE CORP_MK =  '".$this->getCorpId()."'
											  AND closing_date >= '2006-12-01'
											  AND closing_date <= '".$closing_date_end."'
										     GROUP BY CORP_MK
										 ) AS C"), function($join){
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->select( DB::raw("ISNULL(A.SALE_AMOUNT, 0) as SALE_AMOUNT")
									 , DB::raw("ISNULL(A.TOTAL_GAIN, 0) as TOTAL_GAIN")
									 , DB::raw("ISNULL(A.ACCRUED, 0) as ACCRUED")
									 , DB::raw("ISNULL(A.COLLECTION_AMT, 0) as COLLECTION_AMT")
									 , DB::raw("ISNULL(A.COLLECTION_CASH, 0) as COLLECTION_CASH")
									 , DB::raw("ISNULL(A.COLLECTION_BANK, 0) as COLLECTION_BANK")
									 , DB::raw("ISNULL(A.COLLECTION_CARD, 0) as COLLECTION_CARD")
									 , DB::raw("ISNULL(A.COLLECTION_ETC, 0) as COLLECTION_ETC")
									 , DB::raw("ISNULL(A.CREDIT, 0) as CREDIT")
									 , DB::raw("ISNULL(A.PAYMENT, 0) as PAYMENT")
									 , DB::raw("ISNULL(A.PAYMENT_CASH, 0) as PAYMENT_CASH")
									 , DB::raw("ISNULL(A.PAYMENT_BANK, 0) as PAYMENT_BANK")
									 , DB::raw("ISNULL(A.PAYMENT_CARD, 0) as PAYMENT_CARD")
									 , DB::raw("ISNULL(A.PAYMENT_ETC, 0) as PAYMENT_ETC")
									 , DB::raw("ROUND(ISNULL(A.SALE_WEIGHT, 0),1) as SALE_WEIGHT")
									 , DB::raw("ISNULL(A.SALE_AMT, 0) as SALE_AMT")
									 , DB::raw("ISNULL(A.SALE_ACCRUED, 0) as SALE_ACCRUED")
									 , DB::raw("ISNULL(A.SALE_COLLECTION, 0) as SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(A.BUY_WEIGHT, 0),1) as BUY_WEIGHT")
									 , DB::raw("ISNULL(A.BUY_AMT, 0) as BUY_AMT")
									 , DB::raw("ISNULL(A.BUY_CREDIT, 0) as BUY_CREDIT")
									 , DB::raw("ISNULL(A.BUY_PAYMENT, 0) as BUY_PAYMENT")
									 , DB::raw("ISNULL(A.COST, 0) as COST")
									 , DB::raw("ISNULL(A.COST_CASH, 0) as COST_CASH")
									 , DB::raw("ISNULL(A.COST_BANK, 0) as COST_BANK")
									 , DB::raw("ISNULL(A.COST_CARD, 0) as COST_CARD")
									 , DB::raw("ISNULL(B.ACCRUED, 0) AS BEFORE_ACCRUED")
									 , DB::raw("ISNULL(B.CREDIT, 0) AS BEFORE_CREDIT")
									 , DB::raw("ROUND(ISNULL(B.SALE_WEIGHT, 0),1) AS BEFORE_SALE_WEIGHT")
									 , DB::raw("ISNULL(B.SALE_AMT, 0) AS BEFORE_SALE_AMT")
									 , DB::raw("ISNULL(B.SALE_ACCRUED, 0) AS BEFORE_SALE_ACCRUED")
									 , DB::raw("ISNULL(B.SALE_COLLECTION, 0) AS BEFORE_SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(B.BUY_WEIGHT, 0),1) AS BEFORE_BUY_WEIGHT")
									 , DB::raw("ISNULL(B.BUY_AMT, 0) AS BEFORE_BUY_AMT")
									 , DB::raw("ISNULL(B.BUY_CREDIT, 0) AS BEFORE_BUY_CREDIT")
									 , DB::raw("ISNULL(B.BUY_PAYMENT, 0) AS BEFORE_BUY_PAYMENT")
									 , DB::raw("ISNULL(C.ALL_ACCRUED, 0) AS ALL_ACCRUED")
									 , DB::raw("ISNULL(C.ALL_CREDIT, 0) AS ALL_CREDIT")
									 , DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', NULL, NULL) ) AS UNPROV_ALL")
									, DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."') ) AS UNPROV_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."')) AS UNCL_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', NULL, NULL)) AS UNCL_ALL")

							)->first();

		$CHIT_INFO = DB::table("CHIT_INFO AS I")
							->select(  "I.CORP_MK"
									  ,"I.WRITE_DATE"
									  ,"I.SEQ"
									  ,"I.DE_CR_DIV"
									  , DB::raw(" CASE WHEN I.DE_CR_DIV = '1' THEN '지출' ELSE '수입' END AS DIV_NM ")
									  ,"I.CUST_MK"
									  ,"I.ACCOUNT_MK"
									  ,"I.OUTLINE"
									  ,"I.AMT"
									  ,"I.REMARK"
									  ,"AC.ACCOUNT_NM"
									  ,DB::raw("(CASE OUTLINE WHEN '현금' THEN AMT ELSE NULL END) AS CASH")
									  ,DB::raw("(CASE OUTLINE WHEN '카드' THEN AMT ELSE NULL END) AS CARD")
									  ,DB::raw("(CASE OUTLINE WHEN '통장' THEN AMT ELSE NULL END) AS ONLINE")
									  ,"C.FRNM")
							->leftjoin("CUST_CD AS C", function($join){
									$join->on("I.CORP_MK", "=", "C.CORP_MK");
									$join->on("I.CUST_MK", "=", "C.CUST_MK" );
								}
							)->leftjoin("ACCOUNT_CD AS AC", function($join){
									$join->on("I.CORP_MK", "=", "AC.CORP_MK");
									$join->on("I.ACCOUNT_MK", "=", "AC.ACCOUNT_MK");
								}
							)
							->where("I.CORP_MK", $this->getCorpId())
							->where("I.WRITE_DATE", ">=",  $closing_date )
							->where("I.WRITE_DATE", "<=", $closing_date_end )
							->orderBy("C.FRNM", "DESC")->get();

		$sumCASH = 0;
		$sumCARD = 0;
		$sumONLINE = 0;
		foreach( $CHIT_INFO AS $chipInfo){
			
			if( $chipInfo->DE_CR_DIV == "0"){
				$sumCASH -= $chipInfo->CASH;
				$sumCARD -= $chipInfo->CARD;
				$sumONLINE -= $chipInfo->ONLINE;
			}else{

				$sumCASH += $chipInfo->CASH;
				$sumCARD += $chipInfo->CARD;
				$sumONLINE += $chipInfo->ONLINE;
			}
		}

		$pdf = PDF::loadView("closing.Pdf",
								[
									'model'		=> $CLOSING,
									'model1'	=> $CHIT_INFO,
									'title'		=> $closing_date."  결   산   표",
									'sumCASH'	=> $sumCASH,
									'sumCARD'	=> $sumCARD,
									'sumONLINE'	=> $sumONLINE,
								]
							);

		return $pdf->stream();
	}

	public function PdfArrange(){
		$closing_date		= Request::segment(4);
		$closing_date_end	= Request::segment(5);

		$CLOSING = DB::table(DB::raw("(SELECT    MAX(CORP_MK) AS CORP_MK, 
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) AS SALE_AMOUNT,    
												 SUM(SALE_ACCRUED) + SUM(SALE_COLLECTION) - SUM(COST) AS TOTAL_GAIN,    
												 SUM(ACCRUED) as ACCRUED, 
												 SUM(COLLECTION_AMT) as COLLECTION_AMT, 
												 SUM(COLLECTION_CASH) as COLLECTION_CASH, 
												 SUM(COLLECTION_BANK) as COLLECTION_BANK, 
												 SUM(COLLECTION_CARD) as COLLECTION_CARD, 
												 SUM(COLLECTION_ETC) as COLLECTION_ETC,   
												 SUM(CREDIT) as CREDIT, 
												 SUM(PAYMENT) as PAYMENT, 
												 SUM(PAYMENT_CASH) as PAYMENT_CASH, SUM(PAYMENT_BANK) as PAYMENT_BANK, 
												 SUM(PAYMENT_CARD) as PAYMENT_CARD, SUM(PAYMENT_ETC) as PAYMENT_ETC,  
												 SUM(SALE_WEIGHT) as SALE_WEIGHT, 
												 SUM(SALE_AMT) as SALE_AMT, 
												 SUM(SALE_ACCRUED) as SALE_ACCRUED, 
												 SUM(SALE_COLLECTION) as SALE_COLLECTION,   
												 SUM(BUY_WEIGHT) as BUY_WEIGHT, 
												 SUM(BUY_AMT) as BUY_AMT, 
												 SUM(BUY_CREDIT) as BUY_CREDIT, 
												 SUM(BUY_PAYMENT) as BUY_PAYMENT,   
												 SUM(COST) as COST, 
												 SUM(COST_CASH) as COST_CASH, 
												 SUM(COST_BANK) as COST_BANK, 
												 SUM(COST_CARD) as COST_CARD 
									   FROM  CLOSING
									  WHERE CORP_MK =  '".$this->getCorpId()."'
									    AND CLOSING_DATE >=  '".$closing_date."'
									    AND CLOSING_DATE <=  '".$closing_date_end."'
									  GROUP BY CORP_MK ) AS A")
							)->leftjoin(DB::raw("
									 (SELECT SALE_ACCRUED + SALE_COLLECTION AS SALE_AMOUNT
										, SALE_ACCRUED + SALE_COLLECTION - COST AS TOTAL_GAIN
										, CLOSING_SEQ
										, CORP_MK
										, ACCRUED
										, COLLECTION_AMT
										, COLLECTION_CASH
										, COLLECTION_BANK
										, COLLECTION_CARD
										, CREDIT
										, PAYMENT
										, PAYMENT_CASH
										, PAYMENT_BANK
										, PAYMENT_CARD
										, SALE_WEIGHT
										, SALE_AMT
										, SALE_ACCRUED
										, SALE_COLLECTION
										, BUY_WEIGHT
										, BUY_AMT
										, BUY_CREDIT
										, BUY_PAYMENT
									 FROM CLOSING
									 WHERE CORP_MK =  '".$this->getCorpId()."'
									   AND closing_date = DATEADD(dd,-1, '".$closing_date."' )
									) AS B"), function($join){
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin(
								DB::raw("(SELECT   MAX(CORP_MK) AS CORP_MK
												 , SUM(CAST(ACCRUED AS BIGINT) - cast(COLLECTION_AMT as BIGINT)) AS ALL_ACCRUED
												 , SUM(CAST(CREDIT AS BIGINT)  - cast(PAYMENT as BIGINT)) AS ALL_CREDIT
											FROM CLOSING
										   WHERE CORP_MK =  '".$this->getCorpId()."'
										   GROUP BY CORP_MK
										 ) AS C"), function($join){
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->select( DB::raw("ISNULL(A.SALE_AMOUNT, 0) as SALE_AMOUNT")
									 , DB::raw("ISNULL(A.TOTAL_GAIN, 0) as TOTAL_GAIN")
									 , DB::raw("ISNULL(A.ACCRUED, 0) as ACCRUED")
									 , DB::raw("ISNULL(A.COLLECTION_AMT, 0) as COLLECTION_AMT")
									 , DB::raw("ISNULL(A.COLLECTION_CASH, 0) as COLLECTION_CASH")
									 , DB::raw("ISNULL(A.COLLECTION_BANK, 0) as COLLECTION_BANK")
									 , DB::raw("ISNULL(A.COLLECTION_CARD, 0) as COLLECTION_CARD")
									 , DB::raw("ISNULL(A.COLLECTION_ETC, 0) as COLLECTION_ETC")
									 , DB::raw("ISNULL(A.CREDIT, 0) as CREDIT")
									 , DB::raw("ISNULL(A.PAYMENT, 0) as PAYMENT")
									 , DB::raw("ISNULL(A.PAYMENT_CASH, 0) as PAYMENT_CASH")
									 , DB::raw("ISNULL(A.PAYMENT_BANK, 0) as PAYMENT_BANK")
									 , DB::raw("ISNULL(A.PAYMENT_CARD, 0) as PAYMENT_CARD")
									 , DB::raw("ISNULL(A.PAYMENT_ETC, 0) as PAYMENT_ETC")
									 , DB::raw("ROUND(ISNULL(A.SALE_WEIGHT, 0),1) as SALE_WEIGHT")
									 , DB::raw("ISNULL(A.SALE_AMT, 0) as SALE_AMT")
									 , DB::raw("ISNULL(A.SALE_ACCRUED, 0) as SALE_ACCRUED")
									 , DB::raw("ISNULL(A.SALE_COLLECTION, 0) as SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(A.BUY_WEIGHT, 0),1) as BUY_WEIGHT")
									 , DB::raw("ISNULL(A.BUY_AMT, 0) as BUY_AMT")
									 , DB::raw("ISNULL(A.BUY_CREDIT, 0) as BUY_CREDIT")
									 , DB::raw("ISNULL(A.BUY_PAYMENT, 0) as BUY_PAYMENT")
									 , DB::raw("ISNULL(A.COST, 0) as COST")
									 , DB::raw("ISNULL(A.COST_CASH, 0) as COST_CASH")
									 , DB::raw("ISNULL(A.COST_BANK, 0) as COST_BANK")
									 , DB::raw("ISNULL(A.COST_CARD, 0) as COST_CARD")
									 , DB::raw("ISNULL(B.ACCRUED, 0) AS BEFORE_ACCRUED")
									 , DB::raw("ISNULL(B.CREDIT, 0) AS BEFORE_CREDIT")
									 , DB::raw("ROUND(ISNULL(B.SALE_WEIGHT, 0),1) AS BEFORE_SALE_WEIGHT")
									 , DB::raw("ISNULL(B.SALE_AMT, 0) AS BEFORE_SALE_AMT")
									 , DB::raw("ISNULL(B.SALE_ACCRUED, 0) AS BEFORE_SALE_ACCRUED")
									 , DB::raw("ISNULL(B.SALE_COLLECTION, 0) AS BEFORE_SALE_COLLECTION")
									 , DB::raw("ROUND(ISNULL(B.BUY_WEIGHT, 0),1) AS BEFORE_BUY_WEIGHT")
									 , DB::raw("ISNULL(B.BUY_AMT, 0) AS BEFORE_BUY_AMT")
									 , DB::raw("ISNULL(B.BUY_CREDIT, 0) AS BEFORE_BUY_CREDIT")
									 , DB::raw("ISNULL(B.BUY_PAYMENT, 0) AS BEFORE_BUY_PAYMENT")
									 , DB::raw("ISNULL(C.ALL_ACCRUED, 0) AS ALL_ACCRUED")
									 , DB::raw("ISNULL(C.ALL_CREDIT, 0) AS ALL_CREDIT")
									 , DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', NULL, NULL) ) AS UNPROV_ALL")
									, DB::raw("( select dbo.FN_GET_UNPROV_ALL ('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."') ) AS UNPROV_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', '".$closing_date."', '".$closing_date_end."')) AS UNCL_ARRANGE")
									 , DB::raw("( select dbo.FN_GET_UNCL_ALL('".$this->getCorpId()."', NULL, NULL)) AS UNCL_ALL")

							)->first();

		$CHIT_INFO = DB::table("CHIT_INFO AS I")
							->select(  "I.CORP_MK"
									  ,"I.WRITE_DATE"
									  ,"I.SEQ"
									  ,"I.DE_CR_DIV"
									  ,"I.CUST_MK"
									  ,"I.ACCOUNT_MK"
									  , DB::raw(" CASE WHEN I.DE_CR_DIV = '1' THEN '지출' ELSE '수입' END AS DIV_NM ")
									  ,"I.OUTLINE"
									  ,"I.AMT"
									  ,"I.REMARK"
									  ,"AC.ACCOUNT_NM"
									  ,DB::raw("(CASE OUTLINE WHEN '현금' THEN AMT ELSE NULL END) AS CASH")
									  ,DB::raw("(CASE OUTLINE WHEN '카드' THEN AMT ELSE NULL END) AS CARD")
									  ,DB::raw("(CASE OUTLINE WHEN '통장' THEN AMT ELSE NULL END) AS ONLINE")
									  ,"C.FRNM")
							->leftjoin("CUST_CD AS C", function($join){
									$join->on("I.CORP_MK", "=", "C.CORP_MK");
									$join->on("I.CUST_MK", "=", "C.CUST_MK" );
								}
							)->leftjoin("ACCOUNT_CD AS AC", function($join){
									$join->on("I.CORP_MK", "=", "AC.CORP_MK");
									$join->on("I.ACCOUNT_MK", "=", "AC.ACCOUNT_MK");
								}
							)
							->where("I.CORP_MK", $this->getCorpId())
							->where("I.WRITE_DATE", ">=",  $closing_date )
							->where("I.WRITE_DATE", "<=", $closing_date_end )
							->orderBy("C.FRNM", "DESC")->get();

		$sumCASH = 0;
		$sumCARD = 0;
		$sumONLINE = 0;
		foreach( $CHIT_INFO AS $chipInfo){

			if( $chipInfo->DE_CR_DIV == "0"){
				$sumCASH -= $chipInfo->CASH;
				$sumCARD -= $chipInfo->CARD;
				$sumONLINE -= $chipInfo->ONLINE;
			}else{

				$sumCASH += $chipInfo->CASH;
				$sumCARD += $chipInfo->CARD;
				$sumONLINE += $chipInfo->ONLINE;
			}
		}

		$pdf = PDF::loadView("closing.PdfArrange",
								[
									'model'		=> $CLOSING,
									'model1'	=> $CHIT_INFO,
									'title'		=> $closing_date." ~ ".$closing_date_end. " 결   산   표",
									'sumCASH'	=> $sumCASH,
									'sumCARD'	=> $sumCARD,
									'sumONLINE'	=> $sumONLINE,
								]
							);

		return $pdf->stream();
	}

}


<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_CD;
use App\ACCOUNT_GRP;
use App\CHIT_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class AccountGrpListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{
		$CHIT_INFO_list = DB::table("CHIT_INFO")
			->select(
				DB::raw('ACCOUNT_CD.ACCOUNT_NM'), 
				DB::raw("
					case CHIT_INFO.DE_CR_DIV when '1' then isnull(sum(AMT),0) end as AMT1,
					case CHIT_INFO.DE_CR_DIV when '0' then isnull(sum(AMT),0) end as AMT2
				")
			)
			->join('ACCOUNT_CD', function($join){
				$join->on("CHIT_INFO.CORP_MK", "=" ,"ACCOUNT_CD.CORP_MK");
				$join->on("CHIT_INFO.ACCOUNT_MK", "=" ,"ACCOUNT_CD.ACCOUNT_MK");
			})
			->where("CHIT_INFO.CORP_MK","=", $this->getCorpId())
			->groupBy('ACCOUNT_CD.ACCOUNT_NM','CHIT_INFO.DE_CR_DIV');

		return Datatables::of($CHIT_INFO_list)
				->filter(function($query) {
					if( Request::Has('start_date') && Request::Input('end_date')){
						$query->where(function($q){
								$q->where("CHIT_INFO.WRITE_DATE",  ">=", Request::Input('start_date'))
								->where("CHIT_INFO.WRITE_DATE",  "<=", Request::Input('end_date'));
							});
					}

					if( Request::Has('ACCOUNT_GRP_CD') ){
						$query->where("ACCOUNT_CD.ACCOUNT_GRP_CD","=", Request::Input('ACCOUNT_GRP_CD'));
					}
				}
		)->make(true);
	}

	public function index($cust)
	{
		return view("accountgrplist.list",[ "cust" => $cust ] );
	}

	//계정그룹 리스트
	public function getAccountGrp()
	{	
		$ACCOUNT_GRP = DB::table("ACCOUNT_GRP")
			->select('ACCOUNT_GRP_NM','ACCOUNT_GRP_CD')
			->where("CORP_MK","=", $this->getCorpId());

		return Datatables::of($ACCOUNT_GRP)->make(true);
	}


}
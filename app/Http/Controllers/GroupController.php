<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\Http\Requests;

use App\CORP_INFO;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_CD;
use App\ACCOUNT_GRP;
use App\CHIT_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class GroupController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	//그룹정보 리스트
	public function listData()
	{
		$GRP_CD = DB::table('GRP_CD')
			->where("CORP_MK","=", $this->getCorpId());
						
		return Datatables::of($GRP_CD)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "GRP_CD" ){
							$query->where("GRP_CD",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "GRP_NM" ){
							$query->where("GRP_NM",  "like", "%".Request::Input('textSearch')."%");
						}else{
							$query->where(function($q){
								$q->where("GRP_CD",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("GRP_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}

	//그룹정보 인덱스페이지
	public function index($corp)
	{
		return view("group.list",[ "corp" => $corp ] );
	}

	//그룹정보 정보 등록
    public function insert($cust)
    {

        $validator = Validator::make( Request::Input(), [
            'GRP_CD' => 'required',
            'GRP_NM' => 'required'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$count = DB::table("GRP_CD")
			->where('CORP_MK', $this->getCorpId())
			->where('GRP_CD',  Request::Input("GRP_CD"))
			->count();

		if( $count > 0){
			return response()->json(['result' => 'fail', 'reason' => 'PrimaryKey', 'message' => ' 동일한 코드가 존재합니다'], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("GRP_CD")->insert(
                [
                    'CORP_MK' => $this->getCorpId(),
                    'GRP_CD' => Request::input('GRP_CD'),
                    'GRP_NM' => Request::input('GRP_NM')
                ]
            );
        }
        catch(ValidationException $e){
            DB::rollback();

            return Redirect::to("/accountgrp/" . $cust)
                    ->withErrors($e->getErrors())
                    ->withInput();

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//선택된 그룹정보
	public function edit($cust, $id)
	{	
		$model = DB::table("GRP_CD")
			->where("CORP_MK", $this->getCorpId())
            ->where("GRP_CD", $id)
			->first();

		return response()->json(
			[
				'GRP_CD' => $model->GRP_CD, 
				'GRP_NM' => $model->GRP_NM
			]);
	}

	//선택된 그룹정보 수정
    public function update($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'GRP_NM' => 'required'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

        DB::beginTransaction();
        try {
            DB::table("GRP_CD")
				->where("CORP_MK", $this->getCorpId())
                ->where("GRP_CD", Request::input('GRP_CD'))
                ->update(
					[
						"GRP_NM" => Request::input('GRP_NM')
					]
				);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//선택된 그룹정보 삭제
    public function _delete()
    {
        if (Request::ajax()) {
            DB::table("GRP_CD")
				->where("CORP_MK", $this->getCorpId())
                ->whereIn("GRP_CD", Request::get('cd'))
                ->delete();

            return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
        }
        return Response::json(['result' => 'failed']);
    }
}

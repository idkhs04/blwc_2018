<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\UNPROV_INFO;
use App\CUST_CD;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class UnprovInfoController extends Controller
{
    //
	public function index($unprov)
	{
		return view("unprov.list", [ "unprov" => $unprov] );
	}

	public function detail($unprov)
	{
		$CUST_MK = urldecode(Request::segment(4));

		$CUST = DB::table("CUST_CD")
				->select( "FRNM"
						, "CUST_MK"
						, DB::raw(" (SUBSTRING(ETPR_NO, 1, 3) + '-' + SUBSTRING(ETPR_NO, 4, 2) + '-' + SUBSTRING(ETPR_NO, 6, 5) + '*'+ ETPR_NO) AS ETPR_NO")
						, DB::raw("CASE WHEN LEN(RPST) >= 1 AND LEN(RPST) <= 5 THEN RPST
								        WHEN RPST IS NULL OR LEN(RPST)=0 THEN 'unknown'
								        WHEN LEN(RPST) > 6  THEN substring(RPST,1,5) END AS RPST")
				)->where("CORP_MK", $this->getCorpId())
				->where("CUST_MK", $CUST_MK)->first();

		return view("unprov.detail", [ "unprov" => $unprov, "CUST" => $CUST] );
	}

	public function detail2($unprov)
	{
		$CUST_MK = urldecode(Request::segment(4));

		$CUST = DB::table("CUST_CD AS C")
				->leftjoin("CUST_GRP_INFO AS G", function($join){
			
								$join->on("G.CORP_MK", "=", "C.CORP_MK");
								$join->on("G.CUST_GRP_CD", "=", "C.CUST_GRP_CD");
							})
				->select( "C.FRNM"
						, "C.CUST_MK"
						, "G.CUST_GRP_NM"
						, DB::raw(" (SUBSTRING(C.ETPR_NO, 1, 3) + '-' + SUBSTRING(C.ETPR_NO, 4, 2) + '-' + SUBSTRING(C.ETPR_NO, 6, 5))  AS ETPR_NO")
						, DB::raw("CASE WHEN LEN(C.RPST) >= 1 AND LEN(C.RPST) <= 5 THEN RPST
								        WHEN C.RPST IS NULL OR LEN(C.RPST)=0 THEN 'unknown'
								        WHEN LEN(C.RPST) > 6  THEN substring(C.RPST,1,5) END AS RPST")
				)->where("C.CORP_MK", $this->getCorpId())
				->where("C.CUST_MK", $CUST_MK)->first();
		
		$MAX_WRITE_DATE	= DB::table("UNPROV_INFO")
						->select(DB::raw('CONVERT(CHAR(10), ISNULL(MAX(WRITE_DATE), GETDATE()), 23) AS MAX_WRITE_DATE'))
						->where("CORP_MK", $this->getCorpId())
						->where("PROV_CUST_MK", $CUST_MK)
						->first();

		return view("unprov.detail2", [ "unprov" => $unprov, "CUST" => $CUST, 'MAX_T' => $MAX_WRITE_DATE->MAX_WRITE_DATE] );
	}

	// 수정 데이터 가져오기
	public function getDataDetail()
	{


		$UNPROV_INFO = DB::table("UNPROV_INFO")
				->select( "CORP_MK"
						,DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" )
						, "SEQ"
						, "PROV_CUST_MK"
						, "PROV_DIV"
						, "AMT"
						, "REMARK"
						, "MEMO"
				)->where("CORP_MK", $this->getCorpId())
				->where("SEQ", Request::Input("SEQ"))
				->where("WRITE_DATE", Request::Input("WRITE_DATE"))
				->first();

		return response()->json([$UNPROV_INFO]);
	}

	public function listData()
	{
		
		$UNPROV_INFO = null;

		// 기간일때 선택
		if( Request::Input("chkZero") == "2" ){
			if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
				if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
					
					$UNPROV_INFO 
						= DB::table(DB::raw("	
									(   SELECT PROV_CUST_MK
											 , CORP_MK
											 , SUM(UNPROV) AS UNPROV
											 , SUM(PROV) AS PROV
										  FROM  ( SELECT PROV_CUST_MK
														, CORP_MK
														, ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNPROV
														, ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) PROV
													FROM UNPROV_INFO
												   WHERE 1 = 1
												     AND WRITE_DATE >= '".Request::Input("start_date")."'
													 AND WRITE_DATE <= '".Request::Input("end_date")."' 
													 AND CORP_MK= '".$this->getCorpId()."'
												   GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
												) AS A
										GROUP BY PROV_CUST_MK,CORP_MK
									) AS A
								")
						);
				}
			}
		}else{
			$UNPROV_INFO 
				= DB::table(DB::raw("	
							(   SELECT PROV_CUST_MK
									 , CORP_MK
									 , SUM(UNPROV) AS UNPROV
									 , SUM(PROV) AS PROV
								  FROM  ( SELECT PROV_CUST_MK
												, CORP_MK
												, ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNPROV
												, ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) PROV
											FROM UNPROV_INFO
										   WHERE 1 = 1
										     AND CORP_MK= '".$this->getCorpId()."'
										   GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
										) AS A
								GROUP BY PROV_CUST_MK,CORP_MK
							) AS A
						")
				);
		}
			

		$UNPROV_INFO = $UNPROV_INFO->select( "PROV_CUST_MK"
						 , "A.CORP_MK"
						 , "FRNM"
						 , DB::raw("(UNPROV - PROV) AS TOTAL_PROV ")
						 , "CC.ETPR_NO"
						 //, DB::raw("(SELECT CUST_GRP_NM FROM CUST_GRP_INFO WHERE CORP_MK = CC.CORP_MK AND CUST_GRP_CD = CC.CUST_GRP_CD ) AS CUST_GRP_NM ")
						 , "GRP.CUST_GRP_NM"
						 , "CC.ADDR_3"
				)->join(DB::raw("(
									SELECT CUST_MK
										 , FRNM
										 , CUST_GRP_CD
										 , CORP_MK
										 , ETPR_NO
										 , ADDR_3
									  FROM CUST_CD
									 WHERE CORP_MK= '".$this->getCorpId()."'
									   AND INTERFACE_YN = 'Y' 
								) AS CC "), function($join){
									$join->on("A.PROV_CUST_MK", "=", "CC.CUST_MK");
								}
				)->join("CUST_GRP_INFO AS GRP", function($join){
						$join->on("GRP.CUST_GRP_CD", "=", "CC.CUST_GRP_CD" );
						$join->on("GRP.CORP_MK", "=", "CC.CORP_MK" );
				})->where("A.CORP_MK", $this->getCorpId());
				
				if( Request::Input("chkZero") == "0"){
					$UNPROV_INFO->where(DB::raw("(UNPROV - PROV)"), "<>", 0);
				}
				
		return Datatables::of($UNPROV_INFO)
				->filter(function($query) {


					if( Request::Has('srtCondition') || Request::Has('textSearch')){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') != "ALL"){
							$query->where("GRP.CUST_GRP_CD", Request::Input('srtCondition'));

							if( Request::Has('textSearch') ){

								$query->where("CC.FRNM",  "like", "%".Request::Input('textSearch')."%" );
							}
						} else{
							$query->where(function($q){
									$q->where("GRP.CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
									->orwhere("CC.FRNM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}

				})->make(true);
	}


	// 미수금정보 상세조회
	public function detailListData()
	{
		$UNPROV_INFO = DB::table("UNPROV_INFO AS U")
							->select(  "U.CORP_MK"
									 , DB::raw( "CONVERT(CHAR(10), U.WRITE_DATE, 23) AS WRITE_DATE" )
									 , "U.SEQ"
									 , "U.PROV_CUST_MK"
									 , "U.PROV_DIV"
									 , "U.AMT"
									 , "U.REMARK"
									 , "U.STOCK_PIS_MK"
									 , "S.SIZES"
									 , "S.INPUT_DATE"
									 , "S.ORIGIN_CD"
									 , "S.ORIGIN_NM"
									 , "S.QTY"
									 , "S.UNCS"
									 , "S.INPUT_DISCOUNT"
									 , "S.INPUT_UNPROV"
									 , "S.PIS_NM"
									 , DB::raw("(S.UNCS*S.QTY-S.INPUT_DISCOUNT) AS INPUT_AMT")
									 , DB::raw("floor(S.INPUT_AMT/S.QTY) as DAN")
									 , DB::raw("CASE WHEN U.PROV_DIV = '0' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_CHA")
									 , DB::raw("CASE WHEN U.PROV_DIV = '1' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_DAE")
									 , DB::raw(" STUFF ( (SELECT
															     P1.PIS_NM
															   , S1.SIZES
															   , CONVERT(INT, S1.QTY) QTY
															   , CONVERT(INT, S1.UNCS) UNCS
															   , CONVERT(INT, S1.INPUT_AMT) INPUT_AMT
															   , S1.OUTLINE
															   , S1.ORIGIN_NM
															   , CONVERT(CHAR(10), S1.INPUT_DATE, 23)  INPUT_DATE
															   , CONVERT(INT, S1.INPUT_DISCOUNT) INPUT_DISCOUNT
															   , CONVERT(INT, S1.INPUT_UNPROV) INPUT_UNPROV
														FROM STOCK_INFO AS S1
															INNER JOIN PIS_INFO AS P1
																	 ON S1.PIS_MK=P1.PIS_MK
																	AND S1.CORP_MK=P1.CORP_MK
														where 1 = 1
														  AND S1.CORP_MK = '".$this->getCorpId()."'
														  AND S1.SIZES    = S.SIZES
														  AND S1.SEQ      = S.SEQ
														  AND S1.CUST_MK =  '".Request::Input("cust_mk")."'
									 FOR XML RAW('STOCK'), ROOT ('SALE_INFO'), ELEMENTS), 1, 1, '<') AS DD")
							)->leftjoin(DB::raw("
										 (		SELECT S.*
													 , P.PIS_NM
												  FROM STOCK_INFO AS S
													   INNER JOIN PIS_INFO AS P 
													           ON S.PIS_MK=P.PIS_MK
															  AND S.CORP_MK=P.CORP_MK
												 WHERE S.CORP_MK = '".$this->getCorpId()."'
										 ) AS S"), function($join){
													$join->on("U.CORP_MK", "=", "S.CORP_MK");
													$join->on("U.STOCK_PIS_MK ", "=", "S.PIS_MK");
													$join->on("U.STOCK_SIZES", "=", "S.SIZES");
													$join->on("U.STOCK_SEQ", "=", "S.SEQ");
							})->where("U.CORP_MK", $this->getCorpId())
							  ->where("U.PROV_CUST_MK", Request::Input("cust_mk"));

		return Datatables::of($UNPROV_INFO)
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("U.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition')){
							$query->where("U.REMARK",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("U.REMARK",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
		})->make(true);
	}

	// 미지급금정보 상세조회2
	public function detailListData2()
	{
		$cust_mk = urldecode(Request::Input("cust_mk"));
		$UNPROV_INFO = DB::select("EXEC SP_UNPROV_INFO '".$this->getCorpId()."','".$cust_mk."', '".Request::Input('start_date')."', '".Request::Input('end_date')."'");
		$totaldata = count($UNPROV_INFO);
				
		$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => intval( $totaldata ),
                "recordsFiltered" => intval( $_GET['length'] ),
                "data"            => $UNPROV_INFO
            );
		echo json_encode($json_data);
		
	}

	// 미지급금 수정
	public function updUnprovData(){
		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'PROV_CUST_MK'	=> 'required|max:20,NOT_NULL',
			'AMT'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}


		DB::beginTransaction();

		$data = [
					  'PROV_DIV'		=> Request::input('PROV_DIV')
					, 'AMT'				=> Request::input("AMT")
					, 'REMARK'			=> Request::Input('REMARK')
					, 'MEMO'			=> Request::Input("MEMO")
				];

		try {
			// 미지급금
			DB::table("UNPROV_INFO")
					->where('CORP_MK',$this->getCorpId())
					->where('SEQ',Request::Input("SEQ"))
					->where('WRITE_DATE',Request::Input("WRITE_DATE"))
					->update( $data );

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);

	}
	public function setUnprovData(){

		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'PROV_CUST_MK'	=> 'required|max:20,NOT_NULL',
			'AMT'			=> 'required|numeric',
			'MEMO'			=> 'max:100',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$AMT		= Request::Input("AMT");
		$REMARK_DIV	= Request::Input("REMARK_DIV");

		DB::beginTransaction();
		$seq = $this->getMaxSeqInUNPROVINFO(Request::input('WRITE_DATE')) + 1 ;

		$data = [
					  'CORP_MK'			=> $this->getCorpId()
					, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
					, 'SEQ'				=> $seq
					, 'PROV_CUST_MK'	=> urldecode(Request::input('PROV_CUST_MK'))
					, 'PROV_DIV'		=> Request::input('PROV_DIV')
					, 'AMT'				=> $AMT
					, 'REMARK'			=> Request::Input('REMARK')
					, 'MEMO'			=> Request::Input("MEMO")
				];

		try {
			// 미지급금
			DB::table("UNPROV_INFO")->insert( $data );

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	// 미지급금 삭제
	public function _delete(){

		DB::beginTransaction();

		try{
			UNPROV_INFO::where( 'CORP_MK'	,	$this->getCorpId() )
					->where( 'SEQ' , Request::Input("SEQ") )
					->where( 'WRITE_DATE', Request::Input("WRITE_DATE") )
					->delete();

		}catch( Exception $e){
			DB::rollback();
			if ($e->fails()) {
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
		}

		DB::commit();

		return response()->json(['result' => 'success']);

	}


	public function log($unprov){

		return view("unprov.log", [ "unprov" => $unprov] );
	}

	// 미지급금 로그기록
	public function listLog(){

		$UNPROV_LOG = DB::table("UNPROV_INFO_LOG AS LG")
						->select(
							 "LG.IDX"
							,DB::raw("CASE WHEN LG.TYPE = 'C' THEN '추가'
													WHEN LG.TYPE = 'D' THEN '삭제'
													WHEN LG.TYPE = 'U' THEN '수정'
													ELSE '에러' END AS TYPE_NM
									")
							,DB::raw( "CONVERT(CHAR(10), LG.WRITE_DATE, 23) AS WRITE_DATE" )
							,"LG.SEQ"
							,"LG.PROV_DIV"
							,"LG.AMT"
							,"LG.REMARK"
							,"LG.STOCK_SIZES"
							,"LG.STOCK_SEQ"
							,DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.WRITE_DT,101),121) AS WRITE_DT")
							,DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.UPDATE_DT,101),121) AS UPDATE_DT")
							,DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.DELETE_DT,101),121) AS DELETE_DT")
							,"CUST.FRNM"
							,"PIS.PIS_NM"
						)->leftjoin("CUST_CD AS CUST", function($join){
							$join->on("LG.PROV_CUST_MK", "=", "CUST.CUST_MK");
							$join->on("LG.CORP_MK", "=", "CUST.CORP_MK");
						})->leftjoin("PIS_INFO AS PIS", function($join){
							$join->on("LG.STOCK_PIS_MK", "=", "PIS.PIS_MK");
							$join->on("LG.CORP_MK", "=", "PIS.CORP_MK");
						})->where("LG.CORP_MK", $this->getCorpId());

		return Datatables::of($UNPROV_LOG)->make(true);
	}

	// 해당거래처의 총미지급금 불러오기
	public function getTotalUnprov(){

		$UNPROV_INFO = DB::table( DB::raw("(
											SELECT PROV_CUST_MK,CORP_MK, SUM(UNPROV)AS UNPROV,SUM(PROV) AS PROV FROM
											(
												SELECT PROV_CUST_MK,CORP_MK,
														ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END, 0) UNPROV,
														ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END, 0) PROV
												  FROM UNPROV_INFO 
												 WHERE CORP_MK =  '".$this->getCorpId()."'
 												   AND PROV_CUST_MK =  '".urldecode(Request::Input("PROV_CUST_MK"))."'
												 GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
											) AS A
											GROUP BY PROV_CUST_MK,CORP_MK
										) AS A
									")
								)
								->select( DB::raw("(UNPROV - PROV) AS TOTAL_PROV"))
								->first();

		return response()->json($UNPROV_INFO);
	}

	// 미지급금 업체별 출력
	public function pdfEmpty(){
		

		$UNPROV_INFO = DB::table(DB::raw("	(   SELECT PROV_CUST_MK
													 , CORP_MK
													 , SUM(UNPROV) AS UNPROV
													 , SUM(PROV) AS PROV
												  FROM  ( SELECT PROV_CUST_MK
																 , CORP_MK
																  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNPROV
																  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END,0) PROV
															  FROM UNPROV_INFO
															  GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
														) AS A
												GROUP BY PROV_CUST_MK,CORP_MK
											) AS A"
										)
								)->select( "PROV_CUST_MK"
										 , "A.CORP_MK"
										 , "FRNM"
										 , DB::raw("(UNPROV - PROV) AS TOTAL_PROV ")
										 //, DB::raw("(SELECT CUST_GRP_NM FROM CUST_GRP_INFO WHERE CORP_MK = CC.CORP_MK AND CUST_GRP_CD = CC.CUST_GRP_CD ) AS CUST_GRP_NM ")
										 , "GRP.CUST_GRP_NM"
										 , DB::raw(" CASE WHEN LEN(CC.ETPR_NO) = 10 THEN SUBSTRING(CC.ETPR_NO, 1, 3) + '-' + SUBSTRING(CC.ETPR_NO, 4, 2) + '-' + SUBSTRING(CC.ETPR_NO, 6, 5) ELSE CC.ETPR_NO END AS ETPR_NO")
								)->join(DB::raw("(
													SELECT CUST_MK
														 , FRNM
														 , CUST_GRP_CD
														 , CORP_MK
														 , ETPR_NO
													  FROM CUST_CD
													 WHERE CORP_MK= '".$this->getCorpId()."'
													   AND INTERFACE_YN = 'Y' 
												) AS CC "), function($join){
													$join->on("A.PROV_CUST_MK", "=", "CC.CUST_MK");
												}
								)->join("CUST_GRP_INFO AS GRP", function($join){
										$join->on("GRP.CUST_GRP_CD", "=", "CC.CUST_GRP_CD" );
										$join->on("GRP.CORP_MK", "=", "CC.CORP_MK" );
								})->where("A.CORP_MK", $this->getCorpId())
								->where(DB::raw("(UNPROV - PROV)"), "<>", 0)
								->where( function($query) {
								
									$CUST_GRP_CD	= urldecode(Request::segment(4));
									$searchText		= urldecode(Request::segment(5));

									if( $CUST_GRP_CD != "ALL" ){
										$query->where("CC.CUST_GRP_CD", $CUST_GRP_CD);
									}

									if( $searchText != "" ){
										$query->where("CC.FRNM", "LIKE", "%".$searchText."%");
									}
								})
								->orderBy("CC.FRNM",  "ASC")
								->get();



		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";

		$sum = 0;
		foreach( $UNPROV_INFO as $unprov){
			$sum += $unprov->TOTAL_PROV;
		}

		$pdf = PDF::loadView("unprov.pdfListEmpty",
								[
									'list'	=> $UNPROV_INFO,
									'today'	=> $today,
									'sum'	=> $sum,
								]
							);

		return $pdf->stream('미수금 수금용.pdf');

	}

	// 미지급금 목록 상세출력
	public function pdfListDetail(){

		

		$UNCL_M = DB::table("UNCL_INFO AS UNCL")
							->select( "CU_GRP.CUST_GRP_CD"
									, "UNCL.UNCL_CUST_MK"
									, "CU_GRP.CUST_GRP_NM"
									, "UNCL.CORP_MK"
									, DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END ) AS TOTAL_UNCL_AMT")
									, "CUST.FRNM"
									, "CUST.PHONE_NO"
							)->join("CUST_CD AS CUST", function($join){
									$join->on("UNCL.UNCL_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNCL.CORP_MK", "=", "CUST.CORP_MK");

							})->leftjoin("CUST_GRP_INFO AS CU_GRP", function($join){
									$join->on("CUST.CORP_MK" ,"=", "CU_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD" ,"=", "CU_GRP.CUST_GRP_CD");
							})->where("UNCL.CORP_MK", $this->getCorpId())
							->where("UNCL.AMT", "<>", 0)
							->where( function($query) {
								
								$CUST_GRP_CD	= urldecode(Request::segment(4));
								$searchText		= urldecode(Request::segment(5));

								if( $CUST_GRP_CD != "ALL" ){
									$query->where("CUST.CUST_GRP_CD", $CUST_GRP_CD);
								}

								if( $searchText != "" ){
									$query->where("CUST.FRNM", "LIKE", "%".$searchText."%");
								}
							})
							->groupby("CU_GRP.CUST_GRP_CD","UNCL.CORP_MK" ,"UNCL.UNCL_CUST_MK" ,"CUST.FRNM" ,"CU_GRP.CUST_GRP_NM", "CUST.PHONE_NO")
							->having(DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END )") , "<>", 0)
							->orderBy("CUST.FRNM",  "ASC")
							->get();

		//dd($UNCL_M);
		$UNCL_D = DB::table("SALE_INFO_D AS LINE_D")
						->join("PIS_INFO AS P1", function($join){
							$join->on("LINE_D.PIS_MK", "=",  "P1.PIS_MK");
							$join->on("LINE_D.CORP_MK", "=",  "P1.CORP_MK");

						})->select(
									 DB::raw("CONVERT(CHAR(10), LINE_D.WRITE_DATE, 23) AS WRITE_DATE")
								   , "LINE_D.CUST_MK"
								   , "LINE_D.SEQ"
								   , DB::raw("CONVERT(INT, LINE_D.QTY) QTY")
								   , "LINE_D.PIS_MK"
								   , "LINE_D.SIZES"
								   , "LINE_D.ORIGIN_NM"
								   , DB::raw("CONVERT(INT, LINE_D.UNCS) UNCS")
								   , DB::raw("CONVERT(INT, LINE_D.AMT ) AMT")
								   , "LINE_D.REMARK"
								   , "P1.PIS_NM"
									)
						->where("LINE_D.CORP_MK", $this->getCorpId())
						//->where("LINE_D.WRITE_DATE", U.WRITE_DATE)
						//->where("LINE_D.CUST_MK", Request::Input("cust_mk"))
						->get();

		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";
		
		ini_set("memory_limit", "999M");
		ini_set("max_execution_time", "999");

		//dd($UNCL_D);
		$pdf = PDF::loadView("uncl.pdfListDetail",
								[
									'list'		=> $UNCL_M,
									'detail'	=> $UNCL_D,
									'today'		=> $today,
								]
							);

		return $pdf->stream('미수금전체보기.pdf');

	}







	// 매입금액정보 상세조회 PDF
	public function PdfDetail(){
		$start_date		= Request::segment(4);
		$end_date	= Request::segment(5);
		$PROV_CUST_MK = Request::segment(6);
		
		
		$UNPROV_INFO = DB::select("EXEC SP_UNPROV_INFO '".$this->getCorpId()."','".$PROV_CUST_MK."', '".$start_date."', '".$end_date."'");
		
		/*
		$UNPROV_INFO = DB::table("UNPROV_INFO AS U")
							->select(  "U.CORP_MK"
									 , DB::raw( "CONVERT(CHAR(10), U.WRITE_DATE, 23) AS WRITE_DATE" )
									 , "U.SEQ"
									 , "U.PROV_CUST_MK"
									 , "U.PROV_DIV"
									 , "U.AMT"
									 , "U.REMARK"
									 , "U.STOCK_PIS_MK"
									 , "S.SIZES"
									 , "S.INPUT_DATE"
									 , "S.ORIGIN_CD"
									 , "S.ORIGIN_NM"
									 , "S.QTY"
									 , "S.UNCS"
									 , "S.INPUT_DISCOUNT"
									 , "S.INPUT_UNPROV"
									 , "S.PIS_NM"
									 , "CUST.FRNM"
									 , "CUST.RPST"
									 , "CUST.ETPR_NO"
									 , DB::raw("(S.UNCS*S.QTY-S.INPUT_DISCOUNT) AS INPUT_AMT")
									 , DB::raw("floor(S.INPUT_AMT/S.QTY) as DAN")
									 , DB::raw("CASE WHEN U.PROV_DIV = '0' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_CHA")
									 , DB::raw("CASE WHEN U.PROV_DIV = '1' THEN CONVERT(INT, U.AMT) ELSE '-' END AS AMT_DAE")
									 , DB::raw("( SELECT ISNULL(SUM(UL.AMT), 0) AS UNPROV_AMT
													FROM UNPROV_INFO AS UL
												   WHERE UL.SEQ <= U.SEQ
													 AND UL.CORP_MK = '".$this->getCorpId()."'
													 AND UL.PROV_CUST_MK = '".$PROV_CUST_MK."'
												   GROUP BY UL.CORP_MK, UL.PROV_CUST_MK
												) AS PRE_UNPROV_SUM" )
							)->leftjoin(DB::raw("
										 (		SELECT S.*
													 , P.PIS_NM
												  FROM STOCK_INFO AS S
													   INNER JOIN PIS_INFO AS P 
													           ON S.PIS_MK=P.PIS_MK
													          AND S.CORP_MK=P.CORP_MK
												 WHERE S.CORP_MK = '".$this->getCorpId()."'
										 ) AS S"), function($join){
													$join->on("U.CORP_MK", "=", "S.CORP_MK");
													$join->on("U.STOCK_PIS_MK ", "=", "S.PIS_MK");
													$join->on("U.STOCK_SIZES", "=", "S.SIZES");
													$join->on("U.STOCK_SEQ", "=", "S.SEQ");
							})->join("CUST_CD AS CUST", function($join){
								$join->on("CUST.CUST_MK", "=", "U.PROV_CUST_MK");
								$join->on("CUST.CORP_MK", "=", "U.CORP_MK");

							})->where("U.CORP_MK", $this->getCorpId())
							  ->where("U.PROV_CUST_MK", $PROV_CUST_MK)
							  ->where("U.WRITE_DATE", ">=", $start_date	)
							  ->where("U.WRITE_DATE", "<=", $end_date)
							  ->orderBy("U.SEQ", "desc")
							  ->get();


		if( $UNPROV_INFO === null || empty($UNPROV_INFO)){
			return Redirect::back()->with('alert-success', '해당기간엔 미수장부 데이터가 존재 하지 않습니다.')->send();
		}
		
		$qtySum		 = 0;
		$uncsSum	 = 0;
		$inputAmtSum = 0;
		$daeSum		 = 0;
		$chaSum		 = 0;
		$disCountSum = 0;

		foreach( $UNPROV_INFO  as $unrpov){

			$qtySum		 = $qtySum + $unrpov->QTY;
			$uncsSum	 = $uncsSum + $unrpov->UNCS;
			$inputAmtSum = $inputAmtSum + $unrpov->INPUT_AMT;
			$daeSum		 = $daeSum + $unrpov->AMT_DAE;
			$chaSum		 = $chaSum + $unrpov->AMT_CHA;
			$disCountSum = $disCountSum + $unrpov->INPUT_DISCOUNT;
		}
		

		$UNPROV_SUM = DB::table( DB::raw("(
											SELECT PROV_CUST_MK,CORP_MK, SUM(UNPROV)AS UNPROV,SUM(PROV) AS PROV FROM
											(
												SELECT PROV_CUST_MK,CORP_MK,
														ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END, 0) UNPROV,
														ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END, 0) PROV
												FROM UNPROV_INFO GROUP BY PROV_CUST_MK,CORP_MK,PROV_DIV
											) AS A
											GROUP BY PROV_CUST_MK,CORP_MK
										) AS A
										")
								)
								->select( DB::raw("(UNPROV - PROV) AS TOTAL_PROV"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.PROV_CUST_MK', $PROV_CUST_MK)
								->first();
		*/

		//dd($UNPROV_INFO );
		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";
		
		$CUST_CD	= DB::table('CUST_CD AS C')
						->leftjoin("CUST_GRP_INFO AS G", function($join){
			
								$join->on("G.CORP_MK", "=", "C.CORP_MK");
								$join->on("G.CUST_GRP_CD", "=", "C.CUST_GRP_CD");
							})
						->where('C.CORP_MK', $this->getCorpId())
						->where('C.CUST_MK', $PROV_CUST_MK)
						->select(
							"C.FRNM",
							"G.CUST_GRP_NM",
							"C.RPST",
							DB::raw(" (SUBSTRING(C.ETPR_NO, 1, 3) + '-' + SUBSTRING(C.ETPR_NO, 4, 2) + '-' + SUBSTRING(C.ETPR_NO, 6, 5)) AS ETPR_NO")
						)
						->first();
		$CORP_INFO	= $this->getCorpInfo();

		//dd($UNPROV_INFO);
		$pdf =  PDF::loadView("unprov.pdfDetail", [
									'list'		=> $UNPROV_INFO,
									'cust'		=> $CUST_CD,
									'today'		=> $today,
									'start_date'=> $start_date,
									'end_date'	=> $end_date,
									'corp'		=> $CORP_INFO,
									//'sum'		=> $UNPROV_SUM->TOTAL_PROV,
									/*
									'qtySum'	 => $qtySum ,
									'uncsSum'	 => $uncsSum,
									'inputAmtSum'=> $inputAmtSum,
									'daeSum'	 => $daeSum,
									'chaSum'	 => $chaSum,
									'disCountSum'=> $disCountSum,*/

								]);
		return $pdf->stream('미지급내역.pdf');

	}

	public function getCustGrp(){
		$UNPROV_INFO = UNPROV_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($UNPROV_INFO);
	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNPROVINFO($pWRITE_DATE){

		return $max_seq = UNPROV_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;

	}
}


<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class CustGrpInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{
		
		$CUST_GRP_INFO = CUST_GRP_INFO::select( 'CORP_MK', 'CUST_GRP_CD', 'CUST_GRP_NM' )->where("CORP_MK", $this->getCorpId());
						
		return Datatables::of($CUST_GRP_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_CD"){
							$query->where("CUST_GRP_CD",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_NM"){
							$query->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CUST_GRP_CD",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}

	public function index($cust)
	{
		return view("custGrp.list",[ "cust" => $cust ] );
	}

	public function log($cust)
	{
		return view("custGrp.log",[ "cust" => $cust ] );
	}

    public function view($cust, $id)
    {

        $model = DB::table("CUST_GRP_INFO")
            ->where("CORP_MK", $this->getCorpId())
            ->where("CUST_GRP_CD", $id)->first();

        return view(
            "custGrp.View",
            [
                "model" => $model,
                "cust" => $cust
            ]
        );
    }

	// 선택된 거래처 수정
	public function edit($cust, $id)
	{	
		$model = DB::table("CUST_GRP_INFO")
            ->where("CORP_MK", $this->getCorpId())
            ->where("CUST_GRP_CD", $id)->first();

		return response()->json(['CUST_GRP_NM' => $model->CUST_GRP_NM, 'CUST_GRP_CD' => $model->CUST_GRP_CD]);
	}


    public function create($cust)
    {
        $model = new CUST_GRP_INFO();
        return view("custGrp.Edit", ["model" => $model, "cust" => $cust]);
    }

	public function listLog()
    {

        $CUST_GRP_INFO_LOG = DB::table("CUST_GRP_INFO_LOG")
								->select(
									 'IDX'
									,DB::raw("CASE WHEN TYPE = 'C' THEN '추가'
													WHEN TYPE = 'D' THEN '삭제'
													WHEN TYPE = 'U' THEN '수정'
													ELSE '에러' END AS TYPE_NM
									")
									,'CUST_GRP_CD' 
									,'CUST_GRP_NM' 
									,'WRITE_DT' 
									,'UPDATE_DT' 
									,'DELETE_DT' 
								)
								->where("CORP_MK", $this->getCorpId());
						
		return Datatables::of($CUST_GRP_INFO_LOG)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_CD"){
							$query->where("CUST_GRP_CD",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_NM"){
							$query->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CUST_GRP_CD",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
    }

	//거래처 그룹 수정
    public function update($cust)
    {
        $validator = Validator::make( Request::Input(), [
            'CUST_GRP_CD' => 'required|max:2,NOT_NULL',
            'CUST_GRP_NM' => 'required|max:20,NOT_NULL'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            CUST_GRP_INFO::where("CORP_MK", $this->getCorpId())
                ->where("CUST_GRP_CD", Request::input('CUST_GRP_CD'))
                ->update(["CUST_GRP_NM" => Request::input('CUST_GRP_NM')]);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	// 거래처 그룹 등록
    public function insert($cust)
    {

        $validator = Validator::make( Request::Input(), [
            'CUST_GRP_CD' => 'required|max:2,NOT_NULL',
            'CUST_GRP_NM' => 'required|max:20,NOT_NULL'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$count = CUST_GRP_INFO::where('CORP_MK', $this->getCorpId())->where('CUST_GRP_CD',  Request::Input("CUST_GRP_CD"))->count();
		if( $count > 0){
			return response()->json(['result' => 'DUPLICATION', 'message' => '동일한 거래처 그룹이 존재합니다'], 422);
		}

        return $exception = DB::transaction(function(){
		
			try {
				
				if(  DB::table("PIS_CLASS_CD")->where("PIS_MK", Request::Input("PIS_MK"))->where("SIZES", Request::Input("SIZES"))->count() > 0 ){
					return response()->json(['result' => 'DUPLICATION', 'message' => "같은 데이터가 존재합니다."], 422);	
				}

				//dd($request);
				DB::table("CUST_GRP_INFO")->insert(
					[
						'CORP_MK' => $this->getCorpId(),
						'CUST_GRP_CD' => Request::input('CUST_GRP_CD'),
						'CUST_GRP_NM' => Request::input('CUST_GRP_NM')
					]
				);
				return response()->json(['result' => 'success']);
			}
			catch(ValidationException $e){
				DB::rollback();

				return Redirect::to("/cust/" . $cust)
						->withErrors($e->getErrors())
						->withInput();

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}
		});
    }

	public function _delete()
	{
		// 거래처부호 사용중일떄는 실패메시지 전송
		$count = CUST_CD::where('CORP_MK', $this->getCorpId())->where('CUST_GRP_CD',  Request::get('cd'))->count();
		if( $count > 0){
			return response()->json(['result' => 'failed', 'message' => '현재 거래처부호를 사용중이므로 삭제할수없습니다.']);
		}

        if (Request::ajax()) {
            CUST_GRP_INFO::where("CORP_MK", $this->getCorpId())
                ->whereIn("CUST_GRP_CD", Request::get('cd'))
                ->delete();

            return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
        }
        return Response::json(['result' => 'failed']);
    }

	public function getCustGrp(){
		$CUST_GRP = CUST_GRP_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($CUST_GRP);
	}


    public function Excel()
    {

		Excel::load('taxbill.xlsx', function($reader) {
			$results = $reader->all();

			$sheet = $reader->getSheetByName('세금계산서');
			
			echo $sheet->getCell('F5');
			exit;

			foreach ($reader as $row) {
				print_r($row );
			}
			//dd($reader->toArray() );
		});
		
		exit;
        // 컬럼을 지정해줘야 에러가 안남
        $model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
            ->where("CORP_MK", $this->getCorpId())
            ->get();

        Excel::create('거래처그룹', function ($excel) use ($model) {
            $excel->sheet('거래처그룹', function ($sheet) use ($model) {
                $sheet->fromArray($model);
            });
        })->export('xls');
    }

    public function Pdf()
    {

        $pdf = PDF::loadHTML("<h1>Test</h1>");

        // $pdf->save('myfile.pdf');
        return $pdf->stream();

    }

}
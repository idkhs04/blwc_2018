<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\AREA_CD;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use PHPExcel_Style_Border;
use File;

class TaxAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	// 매출/매입계산서 + 전자세금계산서 통합 수정
	public function updTaxIntergration(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'			=> 'required|max:1,NOT_NULL',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'numeric',
			'GOODS'					=> 'required',
			'CUST_MK'				=> 'required',
			//'TAX_USE_YN'			=> 'required',
			'SRL_NO'				=> 'required',
			'SEQ'					=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		//dd($data);
		//return $exception = DB::transaction(function(){
		
			try {
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );

				$data = [
							 "RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
							,"CUST_MK"			=> urldecode(Request::Input("CUST_MK"))
							,"REMARK"			=> Request::Input("REMARK")
							,"TAX_STATTUS"		=> '1'
						];
				
				// 세금계산서 사용시
				if( Request::Input("TAX_USE_YN") == "Y"){
				
					$CUST	= DB::table("CUST_CD")->where("CUST_MK", urldecode(Request::Input("CUST_MK")))->where("CORP_MK", $this->getCorpId())->first();
					$TYPE	= Request::Input("TAX_TYPE");
					if( $CUST !== null){
						

						$tax_data = [
										  "ETPR_NO"						=> $CUST->ETPR_NO					// 사업자번호
										, "FRNM"						=> $CUST->FRNM						// 상호
										, "NAME"						=> $CUST->RPST						// 대표자명
										, "TAX_ADDR1"					=> $CUST->ADDR1						// 주소1
										, "TAX_ADDR2"					=> $CUST->ADDR2						// 주소2
										, "UPTE"						=> $CUST->UPJONG					// 업종
										, "UPJONG"						=> $CUST->UPTE						// 업태
										, "SUM_AMT"						=> Request::Input("SUPPLY_AMT")		// 총 합계
										, "CASH_AMT"					=> Request::Input("SUPPLY_AMT")		// 현금합계
										
										//, "TAX_ACCOUNT_NUMBER"			=> $this->getTaxNo(Request::Input("YEAR"), Request::Input("MONTH"))		// 자체 계산서 번호
										//, "TAX_STATTUS"					=> "1"								// 계산서 상태
										//, "TAX_SEND_STATUS"				=> "1"								// 계산서 송신 상태

										, "TAX_ACCOUNT_TYPE"			=>	Request::Input("TAX_TYPE")		// 세금계산서 종류
										, "TAX_ACCOUNT_PART"			=>	Request::Input("TAX_PART")		// 세금계산서 분류
										, "TAX_PAYWAY"					=>	Request::Input("BILLING_TYPE")	// 결제방법 (현금,카드, 통장)
										, "TAX_MODIFY_CODE"				=>	Request::Input("MODIFY_CODE")	// 수정코드
										//, "TAX_ISSUE_NUMBER"			=>  Request::Input("TAX_ISSUE_NUMBER")
										, "TAX_REPORT_DIVISION"			=>  Request::Input("TAX_REPORT_DIVISION")	// 신고구분
										, "TAX_ISSUE_DIVISION"			=>  Request::Input("TAX_ISSUE_DIVISION")

									];
						
						$data = array_merge($data, $tax_data);

					}
				}
				//dd($data);
				DB::table("TAX_ACCOUNT_M")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",	Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->where("CUST_MK", urldecode(Request::Input("CUST_MK")))

						->update($data);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );
				DB::table("TAX_ACCOUNT_D")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",		Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV",		Request::Input("BUY_SALE_DIV"))
		
						->where("SRL_NO",		Request::Input("SRL_NO"))
						->update(
							[
								 "MONTHS"			=> Request::Input("MONTH")
								,"DAYS"				=> Request::Input("DAY")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);

				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		//});
	
	}


	// 매출/매입계산서 + 전자세금계산서 통합저장
	public function setTaxIntergration(){
		
		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'			=> 'required|max:1,NOT_NULL',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'numeric',
			'GOODS'					=> 'required',
			'CUST_MK'				=> 'required',
			//'TAX_USE_YN'			=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		//return $exception = DB::transaction(function(){
		
			try {
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );

				$data = [
							 "CORP_MK"			=> $this->getCorpId()
							,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
							,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
							,"BUY_SALE_DIV"		=> Request::Input("BUY_SALE_DIV")
							,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
							,"CUST_MK"			=> urldecode(Request::Input("CUST_MK"))
							,"REMARK"			=> Request::Input("REMARK")
							,"TAX_STATTUS"		=> '1'
										
						];
				
				// 세금계산서 사용시
				if( Request::Input("TAX_USE_YN") == "Y"){
				
					$CUST	= DB::table("CUST_CD")->where("CUST_MK", urldecode(Request::Input("CUST_MK")))->where("CORP_MK", $this->getCorpId())->first();
					$TYPE	= Request::Input("TAX_TYPE");
					if( $CUST !== null){
						/*
								  ,"TAX_ISSUE_DIVISION"	=> Request::Input("TAX_ISSUE_DIVISION")		// 발급구분
								  ,"REMARK"				=> Request::Input("REMARK")					// 비고
								  ,"TAX_ACCOUNT_NUMBER"	=> Request::Input("TAX_ACCOUNT_NUMBER")		// 계산서번호
								  ,"TAX_STATTUS"		=> "1"										// 상태값 : 저장

								  ,"TAX_ACKNOWLEDGMENT_NUMBER"	=> Request::Input("TAX_ACKNOWLEDGMENT_NUMBER")	// 승인번호 
								  ,"TAX_ISSUE_NUMBER"	=> Request::Input("TAX_ISSUE_NUMBER")		// 기승인번호
								  ,"TAX_BOOK"			=> Request::Input("TAX_BOOK")				// 책번호
								  ,"TAX_NO"				=> Request::Input("TAX_NO")					// 권
								  ,"TAX_ISSUE_DATE"		=> Request::Input("TAX_ISSUE_DATE")			// 발행일자
								  ,"TAX_ADDITIONAL_TAX_DIVISION" => Request::Input("TAX_ADDITIONAL_TAX_DIVISION")	// 부가세구분
								  ,"TAX_REPORT_DIVISION"=> Request::Input("TAX_REPORT_DIVISION")	// 신고구분
								  ,"TAX_ACCOUNT_PART"	=> Request::Input("TAX_ACCOUNT_PART")		// 계산서 분류
								  ,"TAX_PAYWAY"			=> Request::Input("TAX_PAYWAY")				// 결제구분
								  ,"TAX_ACCOUNT_TYPE"	=> Request::Input("TAX_ACCOUNT_TYPE")		// 계산서 종류
								  ,"TAX_MODIFY_CODE"	=> Request::Input("TAX_MODIFY_CODE")		// 수정코드
						*/
						

						$tax_data = [
										  "ETPR_NO"						=> $CUST->ETPR_NO					// 사업자번호
										, "FRNM"						=> $CUST->FRNM						// 상호
										, "NAME"						=> $CUST->RPST						// 대표자명
										, "TAX_ADDR1"					=> $CUST->ADDR1						// 주소1
										, "TAX_ADDR2"					=> $CUST->ADDR2						// 주소2
										, "UPTE"						=> $CUST->UPJONG					// 업종
										, "UPJONG"						=> $CUST->UPTE						// 업태
										, "SUM_AMT"						=> Request::Input("SUPPLY_AMT")		// 총 합계
										, "CASH_AMT"					=> Request::Input("SUPPLY_AMT")		// 현금합계

										, "TAX_ISSUE_DIVISION"			=> Request::Input("TAX_ISSUE_DIVISION")
										, "TAX_ACCOUNT_NUMBER"			=> $this->getTaxNo(Request::Input("YEAR"), Request::Input("MONTH"))		// 자체 계산서 번호
										, "TAX_STATTUS"					=> "1"								// 계산서 상태
										, "TAX_SEND_STATUS"				=> "1"								// 계산서 송신 상태
										, "TAX_REPORT_DIVISION"			=>	Request::Input("TAX_REPORT_DIVISION")	// 계산서 구분(확인후신고, 즉시신고, 익일신고, 이메일, 결제용 취소)
										, "TAX_ACCOUNT_TYPE"			=>	Request::Input("TAX_TYPE")		// 세금계산서 종류
										, "TAX_ACCOUNT_PART"			=>	Request::Input("TAX_PART")		// 세금계산서 분류
										, "TAX_PAYWAY"					=>	Request::Input("BILLING_TYPE")	// 결제방법 (현금,카드, 통장)

									];
						// 수정/수정세금계산서 일경우
						// 수정코드, 기승인번호를 포함시킴
						if( $TYPE == "02" || $TYPE == "04"){
							array_push($tax_data, "TAX_MODIFY_CODE", Request::Input("MODIFY_CODE"));			// 수정코드
							array_push($tax_data, "TAX_ISSUE_NUMBER", Request::Input("TAX_ISSUE_NUMBER"));		// 기승인번호
						}
						$data = array_merge($data, $tax_data);

					}
				}
		
				
				DB::table("TAX_ACCOUNT_M")
						->insert($data);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );
				DB::table("TAX_ACCOUNT_D")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> Request::Input("BUY_SALE_DIV")
								,"SRL_NO"			=> (int)$TAX_ACCOUNT_MAX_SRL + 1
								,"MONTHS"			=> Request::Input("MONTH")
								,"DAYS"				=> Request::Input("DAY")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		//});
	
	}


	// 년단위 계산서 관리 화면
	public function listTaxCust($tax){
		$CORP_INFO = DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->select("TAX_USE_YN")->first();
		return view("tax.list", ["corp_info" => $CORP_INFO]);
	
	}

	// 년단위 계산서 관리 데이터 조회
	public function listTaxCustData(){
		
		// 매출일 경우
		// 해당 리스트에서 합계를 추출할때 
		// 세금계산서 합계 + 일반 합계를 더해서 조회하도록 수정
		// 전송상태 => 상태 해서 일반 으로 변경

		$chkSaleStock = Request::Input('chkSaleStock');

		$TAX_SALE_INFO = DB::table("CUST_CD AS CUST")
							->select(  "CG.CUST_GRP_CD"
									 , "CG.CUST_GRP_NM"
									 , "CUST.CUST_MK"
									 , "CUST.FRNM"
									 , "CUST.ETPR_NO"
									 , DB::raw("ROUND(ISNULL(SALE.SUM_AMT, 0), 2) AS SALE_SUM")
							)->join("CUST_GRP_INFO AS CG", function($join){
									$join->on("CUST.CORP_MK", "=", "CG.CORP_MK");
									$join->on("CUST.CUST_GRP_CD", "=", "CG.CUST_GRP_CD");
							})->where("CUST.CORP_MK", $this->getCorpId());
							
		// 매입일경우
		if( $chkSaleStock == "0"){
			$TAX_SALE_INFO = $TAX_SALE_INFO->leftjoin(DB::raw(" ( SELECT SUM(INPUT_AMT - INPUT_DISCOUNT) AS SUM_AMT , CUST_MK, CORP_MK
												    FROM STOCK_INFO
												   WHERE CORP_MK = '".$this->getCorpId()."'
												     AND INPUT_DATE BETWEEN '".Request::Input('START_DATE')."' AND '".Request::Input('END_DATE')."'
													 AND OUTPUT_DATE IS NULL
												   GROUP BY CUST_MK, CORP_MK
												) AS SALE"), function($join){
									$join->on("SALE.CUST_MK" ,"=", "CUST.CUST_MK");
							})->where("CUST.CORP_DIV", 'P')
							->where(function($query){
								if( Request::Input("chkZero") == "0"){
									$query->where("SALE.SUM_AMT", ">", "0");
								}
							});;

		}else{
		// 매출일경우
			$TAX_SALE_INFO = $TAX_SALE_INFO->leftjoin(DB::raw(" ( SELECT SUM(TOTAL_SALE_AMT) AS SUM_AMT , CUST_MK
												    FROM SALE_INFO_M
												   WHERE CORP_MK = '".$this->getCorpId()."'
												     AND WRITE_DATE BETWEEN '".Request::Input('START_DATE')."' AND '".Request::Input('END_DATE')."'
												   GROUP BY CUST_MK
												) AS SALE"), function($join){
									$join->on("SALE.CUST_MK" ,"=", "CUST.CUST_MK");
									
							})
							->where("CUST.CORP_DIV", 'S')
							->where(function($query){
								if( Request::Input("chkZero") == "0"){
									$query->where("SALE.SUM_AMT", ">", "0");
								}
							});

							// 합계가 0이 아닌 업체 조회
		
		}

		

		return Datatables::of($TAX_SALE_INFO)
				->filter(function($query) {
					if( Request::Has('srtCondition') || Request::Has('textSearch')){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') != "ALL"){

							$query->where("CG.CUST_GRP_CD", Request::Input('srtCondition'));
							
							if(  Request::Has('textSearch') ){
								$query->where("CUST.FRNM", "like", "%".Request::Input('textSearch')."%");
							}

						} else{
							
							$query->where(function($q){
								$q->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				})->make(true);
	
	}

	public function getCustSaleDetailData(){

		// 매입/매출 구분
		$chkSaleStock = Request::Input('chkSaleStock');
		
		// 매출 일 경우
		$SALE_CUST ;
		if( $chkSaleStock == "1" ){

			$SALE_CUST = DB::table("SALE_INFO_M AS A")
							->select( DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q1M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q2M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q3M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q4M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q5M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q6M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q7M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q8M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q9M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q10M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q11M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN TOTAL_SALE_AMT ELSE 0 END), 2) AS Q12M")
									, DB::raw("ROUND(SUM(A.TOTAL_SALE_AMT), 2) AS TAMT")
									, DB::raw("1 AS SEQ")
								)
									
							->where("A.CORP_MK", $this->getCorpId())
							->where("A.CUST_MK", urldecode(Request::Input("CUST_MK")))
							//->where("TAX.BUY_SALE_DIV", "O")
							->whereBetween("A.WRITE_DATE", array(Request::Input("WRITE_DATE"), Request::Input("END_DATE")))
							->groupBy("A.CORP_MK", "A.CUST_MK");
		}else{
			$SALE_CUST = DB::table("STOCK_INFO AS A")
							->select( DB::raw("ISNULL(ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '01' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2), 0) AS Q1M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '02' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q2M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '03' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q3M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '04' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q4M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '05' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q5M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '06' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q6M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '07' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q7M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '08' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q8M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '09' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q9M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '10' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q10M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '11' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q11M")
									, DB::raw("ROUND(SUM(CASE MONTH(A.INPUT_DATE) WHEN '12' THEN A.INPUT_AMT - A.INPUT_DISCOUNT ELSE 0 END), 2) AS Q12M")
									, DB::raw("ROUND(SUM( A.INPUT_AMT - A.INPUT_DISCOUNT ), 2) AS TAMT")
									, DB::raw("1 AS SEQ")
							)->where("A.CORP_MK", $this->getCorpId())
							->where("A.CUST_MK", urldecode(Request::Input("CUST_MK")))
							->where("A.DE_CR_DIV", "1")

							->whereBetween("A.INPUT_DATE", array(Request::Input("WRITE_DATE"), Request::Input("END_DATE")))
							->groupBy( "A.CORP_MK", "A.CUST_MK");
		}

		// 만일 판매내역이 년 전체로 없을 경우 아래의 계산서 내역과 Union이 되지 않으므로
		// 빈값을 넣어줘야 한다
		// 2016-12-20 김현섭 (from. 삼호유통 검색하다가 발견)
		if( $SALE_CUST->count() == 0 ){
			$SALE_CUST = DB::table("SALE_INFO_D")
				->select(   DB::raw(" '0' AS Q1M"),
							DB::raw(" '0' AS Q2M"),
							DB::raw(" '0' AS Q3M"),
							DB::raw(" '0' AS Q4M"),
							DB::raw(" '0' AS Q5M"),
							DB::raw(" '0' AS Q6M"),
							DB::raw(" '0' AS Q7M"),
							DB::raw(" '0' AS Q8M"),
							DB::raw(" '0' AS Q9M"),
							DB::raw(" '0' AS Q10M"),
							DB::raw(" '0' AS Q11M"),
							DB::raw(" '0' AS Q12M"),
							DB::raw(" '0' AS TAMT"),
							DB::raw(" '1' AS SEQ")
				);
			
		}
		
		$TAX_SALE = DB::table("TAX_ACCOUNT_D AS D")
						->join("TAX_ACCOUNT_M AS M", function($join){
							$join->on("D.CORP_MK", "=", "M.CORP_MK");
							$join->on("D.WRITE_DATE","=", "M.WRITE_DATE");
							$join->on("D.SEQ", "=", "M.SEQ");
							$join->on("D.BUY_SALE_DIV", "=", "M.BUY_SALE_DIV");
						})->select( DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '01' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T1M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '02' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T2M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '03' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T3M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '04' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T4M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '05' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T5M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '06' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T6M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '07' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T7M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '08' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T8M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '09' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T9M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '10' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T10M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '11' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T11M")
								  , DB::raw("ISNULL(ROUND(SUM(CASE MONTH(D.WRITE_DATE) WHEN '12' THEN SUPPLY_AMT ELSE 0 END), 2), 0) AS T12M")
								  , DB::raw("ISNULL(ROUND(SUM(D.SUPPLY_AMT), 2), 0) AS TAMT")
								  , DB::raw("0 AS SEQ"))
						->where("M.CORP_MK", $this->getCorpId())
						->where("M.CUST_MK", urldecode(Request::Input("CUST_MK")))
						->where(function($query) {
							if( Request::Input("chkSaleStock") == "1"){
								$query->where("D.BUY_SALE_DIV", 'O');
							}else{
								$query->where("D.BUY_SALE_DIV", 'I');
							}
						})
						->whereBetween("D.WRITE_DATE", array(Request::Input("WRITE_DATE"), Request::Input("END_DATE")))
						->union($SALE_CUST)
						->orderBy( 'SEQ', 'DESC' )
						->get();
		
		//return Datatables::of($TAX_SALE)->make(true);
		return response()->json(['result' => 'success', 'TAX_SALE' => $TAX_SALE]);
	}

	// 국세청 신고용 업로드 엑셀양식 (Multi)
	public function ExcelTaxBillDownload(){
		
		return Excel::load(public_path().'/taxbillZero.xls', function($reader) {

			$param = Request::Input("query");
			
			//print_r($param);
			$ETAX_LIST = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 112) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										DB::raw( "CASE WHEN TAX_M.BUY_SALE_DIV = 'O' THEN '매출' ELSE '매입' END AS 'SALE_DIV_NM'"),
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw( "CASE WHEN TAX_M.RECPT_CLAIM_DIV = '0' THEN '청구' ELSE '영수' END AS 'RECPT_CLAIM_DIV_NM'"),
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										
										"TAX_M.TAX_ACCOUNT_NUMBER" , 
										"TAX_M.TAX_STATTUS" , 
										"TAX_M.TAX_SEND_STATUS" , 
										"TAX_M.TAX_SEND_DATE" , 
										"TAX_M.TAX_RECEIVE_DATE" , 
										"TAX_M.TAX_ACCOUNT_TYPE",
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER",
										"TAX_M.TAX_ACCOUNT_PART",
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										"CUST.EMAIL" ,
										DB::raw("CORP.FRNM AS CORP_NM"),
										DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR2"),
										DB::raw("CORP.POST_NO AS CORP_POST_NO"),
										DB::raw("CORP.PHONE_NO AS CORP_PHONE_NO"),
										DB::raw("CORP.RPST_EMAIL AS CORP_RPST_EMAIL")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
								
							})->leftjoin("CUST_GRP_INFO AS CUST_GRP", function($join){
									$join->on("CUST.CORP_MK", "=", "CUST_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD", "=", "CUST_GRP.CUST_GRP_CD");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");

							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.BUY_SALE_DIV", $param[0]['BUY_SALE_DIV'])
							//->where("TAX_M.CUST_MK", $p[0]['CUST_MK'])
							//->where("TAX_M.SEQ" , $p[0]['SEQ'])->first()
							->where( function($query) use ($param) {

								foreach($param as $p){
									$query->orwhere("CUST.CUST_MK", $p['CUST_MK'])
										->where("TAX_M.SEQ", $p['SEQ'])
										->where("TAX_M.WRITE_DATE", $p['WRITE_DATE'])
										->where("TAX_M.BUY_SALE_DIV", $p['BUY_SALE_DIV']);
								}
							})
							->orderBy('TAX_M.WRITE_DATE', 'desc')
							->orderBy('CUST.FRNM', 'asc')
							->get(); 
		
			$results = $reader->all();
			$sheet = $reader->getSheetByName('엑셀업로드양식');

			// 공급자 등록번호 
			
			$start_row	= 7;
			$cntErrorNumber	= 0;
			$arrErrorMessage= [];


			foreach( $ETAX_LIST as $data){
				
				// 전자세금계산서 종류
				// 입력을 하지 않을 경우(ktnet 전자세금계산서 사용하지 않는 업체는 기본 02로 영세률로 지정)
				// 면세용은 무조건 05로 고정 : 2017-04-05
				$sheet->setCellValue('A'.$start_row , "05");
				/*
				if( $data->TAX_ACCOUNT_PART == null ){
					$sheet->setCellValue('A'.$start_row , "02");
				}else{
					$sheet->setCellValue('A'.$start_row , $data->TAX_ACCOUNT_PART);
				}
				*/

				// 작성일자
				$sheet->setCellValue('B'.$start_row , trim($data->WRITE_DATE));
				// 공급자사업자번호
				$sheet->setCellValue('C'.$start_row , trim($data->CORP_ETPR_NO));
				// 공급자 상호
				$sheet->setCellValue('E'.$start_row , $data->CORP_NM);
				// 공급자 성명
				$sheet->setCellValue('F'.$start_row , $data->CORP_RPST);
				// 공급자 사업자주소
				$sheet->setCellValue('G'.$start_row , $data->CORP_ADDR1.' '.$data->CORP_ADDR2);
				// 공급자 업태
				$sheet->setCellValue('H'.$start_row , $data->CORP_UPTE);
				// 공급자 종목
				$sheet->setCellValue('I'.$start_row , $data->CORP_UPJONG);
				// 공급자 이메일
				$sheet->setCellValue('J'.$start_row , $data->CORP_RPST_EMAIL);

				// 공급받는자 등록번호
				$sheet->setCellValue('K'.$start_row , trim($data->ETPR_NO));
				// 공급받는자 상호
				$sheet->setCellValue('M'.$start_row , $data->FRNM);
				// 공급받는자 성명
				$sheet->setCellValue('N'.$start_row , $data->RPST);
				// 공급받는자 사업자주소
				$sheet->setCellValue('O'.$start_row , $data->ADDR1.' '.$data->ADDR2);

				// 공급받는자 업태
				$sheet->setCellValue('P'.$start_row , $data->UPTE);
				// 공급받는자 업종
				$sheet->setCellValue('Q'.$start_row , $data->UPJONG);
				// 공급받는자 이메일
				$sheet->setCellValue('R'.$start_row , $data->EMAIL);
				
				// 공급가액
				$sheet->setCellValue('T'.$start_row , $data->SUPPLY_AMT);

				// 세액
				//$sheet->setCellValue('U'.$start_row , 0);

				// 품목
				$sheet->setCellValue('W'.$start_row , $data->GOODS);
				
				// 작성일(2자리 , 년월제외)
				$sheet->setCellValue('V'.$start_row , $data->M_DD);
			
				// 공급가액 - 1
				$sheet->setCellValue('AA'.$start_row , $data->SUPPLY_AMT);

				//현금
				//$sheet->setCellValue('AX'.$start_row , $data->SUPPLY_AMT);
				// 영수/청구
				$sheet->setCellValue('BB'.$start_row , $data->RECPT_CLAIM_DIV == "1" ? "02" :  "02");

				$start_row++;			
			}

		})->setFilename('세금계산서등록양식(일반)')
			->download('xls');

		return response()->json(['result' => 'success', 'TAX_SALE' => $TAX_SALE]);

	}

	// 매출세금계산서(국세청) 엑셀
	public function getExcelTaxBill(){
		
		return Excel::load(public_path().'/taxbill.xlsx', function($reader) {

			$WRITE_DATE	= Request::segment(4);
			$SEQ		= Request::segment(5);
			$CUST_MK	= Request::segment(6);

			$TAX_CCOUNT_SALE = DB::table("TAX_ACCOUNT_M AS TAX_M")
								->select(
											"TAX_M.CUST_MK" , 
											DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
											DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
											DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
											DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
											"TAX_M.SEQ" , 
											"TAX_M.BUY_SALE_DIV" , 
											"TAX_M.RECPT_CLAIM_DIV" ,
											DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
											"TAX_M.REMARK" , 
											"TAX_D.MONTHS",
											"TAX_D.DAYS",
											"TAX_D.GOODS",
											"TAX_D.QTY",
											"TAX_D.UNCS",
											"TAX_D.SUPPLY_AMT",
											"TAX_D.TAX",
											"TAX_D.SRL_NO",
											"CUST.FRNM" , 
											//"CUST.ETPR_NO" , 
				DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
						
											"CUST.RPST" , 
											"CUST.UPJONG",
											"CUST.UPTE",
											"CUST.JUMIN_NO" , 
											"CUST.FAX" , 
											"CUST.PHONE_NO" , 
											"CUST.ADDR1" , 
											"CUST.ADDR2" ,
											DB::raw("CORP.FRNM AS CORP_NM"),
											//DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
											DB::raw( "( SUBSTRING(CORP.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CORP.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CORP.ETPR_NO, 6, 5) ) AS CORP_ETPR_NO"),
											DB::raw("CORP.RPST AS CORP_RPST"),
											DB::raw("CORP.UPJONG AS CORP_UPJONG"),
											DB::raw("CORP.UPTE AS CORP_UPTE"),
											DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
											DB::raw("CORP.ADDR1 AS CORP_ADDR2")
								)->leftjoin("CUST_CD AS CUST", function($join){
										$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
										$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
								})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
										$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
										$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
										$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
										$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
								})->join("CORP_INFO AS CORP", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
								})->where("TAX_M.CORP_MK", $this->getCorpId())
								->where("TAX_M.CUST_MK", $CUST_MK)
								->where("TAX_M.WRITE_DATE", $WRITE_DATE)
								->where("TAX_M.SEQ" , $SEQ)->first();
			
			$TAX_CCOUNT_D = DB::table("TAX_ACCOUNT_M AS TAX_M")
								->select(
											"TAX_M.CUST_MK" , 
											DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS YY"),
											DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS MM"),
											DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS DD"),
											"TAX_M.SEQ" , 
											"TAX_M.REMARK" , 
											"TAX_D.MONTHS",
											"TAX_D.DAYS",
											"TAX_D.GOODS",
											"TAX_D.QTY",
											"TAX_D.UNCS",
											"TAX_D.SUPPLY_AMT",
											"TAX_D.TAX",
											"TAX_D.SRL_NO"
								)->join("TAX_ACCOUNT_D AS TAX_D", function($join){
										$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
										$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
										$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
										$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
								})
								->where("TAX_M.CORP_MK", $this->getCorpId())
								->where("TAX_M.WRITE_DATE", $WRITE_DATE)
								->where("TAX_M.SEQ" , $SEQ)
								->where("TAX_M.CUST_MK", $CUST_MK)->get();


			// 일련번호
			$M_SRL_NO = DB::table("DOC_NUMBER")
						->select("SRL_NO")
						->where("CORP_MK", $this->getCorpId())
						->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
						->where("MONTH", $TAX_CCOUNT_SALE->M_MM );
			
			$SRL_NO = 0;
			// 일련번호가 없으면 생성해줌
			if( count($M_SRL_NO->get()) == 0 ){
				DB::table("DOC_NUMBER")->insert(
						[
							 "CORP_MK"	=> $this->getCorpId()
							,"YEAR"		=> $TAX_CCOUNT_SALE->M_YY 
							,"MONTH"	=> $TAX_CCOUNT_SALE->M_MM
							,"SRL_NO"	=> 1
						]
				);

				$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM."01";

			}else{
				// 일련번호 최대값 가져오고 update(srl_no +1) 
				$M_SRL_NO = DB::table("DOC_NUMBER")
					->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM )->first();
				
				$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

				$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM.$M_SRL_NO->NEW_SRL_NO;

				DB::table("DOC_NUMBER")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM )
					->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);

			}

			$strAMT = strval((int)$TAX_CCOUNT_SALE->SUPPLY_AMT);
			$strSum = [];

			// 초기화
			for($i=0; $i<11; $i++){
				$strSum[$i] = 0;
			}
			$index = 10;
			for( $i=strlen($strAMT)-1; $i >= 0 ; $i--) { 
				$strSum[$index--] = $strAMT[$i];	
			}
			//11-strlen($strAMT)
			// 여기서부터 엑셀 저장

			$results = $reader->all();
			$sheet = $reader->getSheetByName('세금계산서');

			// 일련번호 
			$sheet->setCellValue('AA4', $SRL_NO);
			
			// 공급자 등록번호 
			$sheet->setCellValue('F5', $TAX_CCOUNT_SALE->CORP_ETPR_NO);
			// 공급자 상호명
			$sheet->setCellValue('F7', $TAX_CCOUNT_SALE->CORP_NM);
			// 공급자 대표자명
			$sheet->setCellValue('M7', $TAX_CCOUNT_SALE->CORP_RPST);
			// 공급자 주소
			$sheet->setCellValue('F9', $TAX_CCOUNT_SALE->CORP_ADDR1." ".$TAX_CCOUNT_SALE->CORP_ADDR2 );
			// 업태 
			$sheet->setCellValue('F11', $TAX_CCOUNT_SALE->CORP_UPTE);
			// 종목
			$sheet->setCellValue('M11', $TAX_CCOUNT_SALE->CORP_UPJONG);

			// 공급받는자 등록번호 
			$sheet->setCellValue('V5', $TAX_CCOUNT_SALE->ETPR_NO);
			// 공급받는자 상호명
			$sheet->setCellValue('V7', $TAX_CCOUNT_SALE->FRNM);
			// 공급받는자 대표자명
			$sheet->setCellValue('AC7', $TAX_CCOUNT_SALE->RPST);
			// 공급받는자 주소
			$sheet->setCellValue('V9', $TAX_CCOUNT_SALE->ADDR1." ".$TAX_CCOUNT_SALE->ADDR2 );
			// 공급받는자 업태 
			$sheet->setCellValue('V11', $TAX_CCOUNT_SALE->UPTE);
			// 공급받는자 종목
			$sheet->setCellValue('AC11', $TAX_CCOUNT_SALE->UPJONG);

			// 작성 : 년 / 월 /일
			$sheet->setCellValue('B15', $TAX_CCOUNT_SALE->M_YY);
			$sheet->setCellValue('D15', (int)$TAX_CCOUNT_SALE->M_MM >= 10 ? $TAX_CCOUNT_SALE->M_MM : "0".$TAX_CCOUNT_SALE->M_MM );
			$sheet->setCellValue('E15', (int)$TAX_CCOUNT_SALE->M_DD >= 10 ? $TAX_CCOUNT_SALE->M_DD : "0".$TAX_CCOUNT_SALE->M_DD);

			// 공란수
			$sheet->setCellValue('F15', 11-strlen($strAMT) );
			
			// 공급가액
			$ch = 'H';
			$cell = "";
			foreach($strSum as $item){
				$cell = $ch."15";
				$sheet->setCellValue($cell, $item);
				$ch++;
			}
			
			// 품목
			$cell = "";
			$row = 17;
			foreach($TAX_CCOUNT_D as $list){
				// 월
				$cell = 'B'.$row;
				$sheet->setCellValue($cell, (int)$list->MM >= 10 ? $list->MM : "0".$list->MM);
				//일
				$cell = 'C'.$row;
				$sheet->setCellValue($cell, $list->DD);
				// 품목
				$cell = 'D'.$row;
				$sheet->setCellValue($cell, $list->GOODS);
				// 수량

				$cell = 'N'.$row;
				$sheet->setCellValue($cell, $list->QTY);
				// 단가
				$cell = 'Q'.$row;
				$sheet->setCellValue($cell, $list->UNCS);
				// 공급가액
				$cell = 'U'.$row;
				$sheet->setCellValue($cell, $list->SUPPLY_AMT);
				//++$ch; // 세액 건너띔
				// 비고
				$cell = 'AD'.$row;
				$sheet->setCellValue($cell, $list->REMARK);
				
				$row++;
			}
			
			// 합계금액
			$sheet->setCellValue('B22', $TAX_CCOUNT_SALE->SUPPLY_AMT );
			
			
			$sheet->getStyle('R5')->applyFromArray(array(
				'borders' => array( 'left' => array( 'style' => PHPExcel_Style_Border::BORDER_DOTTED  , 'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AA4')->applyFromArray(array(
				'borders' => array( 'right' => array( 'style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('V5')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));
			
			$sheet->getStyle('AC7')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('V9')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AC11')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')) )
			));

			$sheet->getStyle('AC13')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AC14')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AD16')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AD17')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD18')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD19')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			$sheet->getStyle('AD20')->applyFromArray(array(
				'borders' => array( 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AA21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AE21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));

			$sheet->getStyle('AG21')->applyFromArray(array(
				'borders' => array( 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'ff0000')))
			));
			


		})->download('xlsx');

		//return $data->stream('xlsx');
	}


	public function indexBuy($tax)
	{
		return view("account.tax_buy",[ "tax" => $tax ] );
	}

	public function detailBuy($tax)
	{
		$TAX_CCOUNT_M = DB::table("CUST_CD AS CUST")
						->select(
									"CUST.FRNM" , 
									DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
									"CUST.RPST" , 
									"CUST.JUMIN_NO" , 
									"CUST.FAX" , 
									"CUST.PHONE_NO" , 
									"CUST.ADDR1" , 
									"CUST.ADDR2" ,
									"CUST.UPJONG" ,
									"CUST.UPTE" 
						)
						->where("CUST.CORP_MK", $this->getCorpId())
						->where("CUST.CUST_MK", Request::segment(4))->first();

		//d($TAX_CCOUNT_M);
		return view("account.tax_buyDetail",[ "tax" => $tax, "model" => $TAX_CCOUNT_M ] );
	}

	// DataTable : 매출세금계산서 목록
	public function getTaxAccountList(){

		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.REMARK" , 
										"TAX_D.SRL_NO",
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"));
			
		return Datatables::of($TAX_CCOUNT_M)
					
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("TAX_M.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}
				}
		)->make(true);
	
	}

	public function uptTaxSale(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'			=> 'required|max:1,NOT_NULL',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SEQ'					=> 'required|numeric',
			'SRL_NO'				=> 'required|numeric',
			'SUPPLY_AMT'			=> 'required|numeric',
			'GOODS'					=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				DB::table("TAX_ACCOUNT_M")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",	Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->where("CUST_MK", Request::Input("CUST_MK"))
						->update(
							[
								 "RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
								,"REMARK"			=> Request::Input("REMARK")
							]
						);

				DB::table("TAX_ACCOUNT_D")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",	Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->where("SRL_NO", Request::Input("SRL_NO"))
						
						->update(
							[
								 "GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	// 매출 계산서 수정 불러오기
	public function getTaxSaleItem(){
	

		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.TAX_ACCOUNT_TYPE" , 
										"TAX_M.TAX_PAYWAY" , 
										"TAX_M.TAX_ACCOUNT_PART" , 
										"TAX_M.TAX_ISSUE_NUMBER" , 
										"TAX_M.TAX_MODIFY_CODE" , 
										"TAX_M.TAX_REPORT_DIVISION",
										"TAX_M.TAX_ISSUE_DIVISION",
										"TAX_M.REMARK" , 
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
							->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
							->where("TAX_M.SEQ", Request::Input("SEQ"))
							->where("TAX_M.BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
							->where("TAX_D.SRL_NO", Request::Input("SRL_NO"))->first();
		
		
		return response()->json($TAX_CCOUNT_M);
		//return $TAX_CCOUNT_M;							
	}

	// 매입계산서 자동생성
	public function getTaxBuyGeneration(){

		$STOCK_INFO_SUM	 = DB::table("STOCK_INFO AS ST")
							->select( DB::raw( "SUM(INPUT_AMT - INPUT_DISCOUNT) AS SUM")	)
							->where("CORP_MK", $this->getCorpId())
							->where(DB::raw("DATEPART(YY, INPUT_DATE)"), Request::Input("INPUT_YEAR"))
							->where(DB::raw("DATEPART(MM, INPUT_DATE)"), Request::Input("INPUT_MONTH"))
							->where("CUST_MK", Request::Input("CUST_MK"))
							->first()->SUM;
			
		return response()->json(
									[ 
									  'SUM' => $STOCK_INFO_SUM
									, 'WRITE_DATE'=> $this->endday(Request::Input("INPUT_YEAR"), Request::Input("INPUT_MONTH")) 
									, 'GOODS' => '활어'
									, 'REMARK' => ''
									]
								);
	
	}

	// 매출계산서 자동생성
	public function getTaxSaleGeneration(){

		$STOCK_INFO = null;

		if( Request::Has("ONLY_CASH") && Request::Input("ONLY_CASH") == "1"){
			$STOCK_INFO	 = DB::table("SALE_INFO_M")
							->select( DB::raw( "( SUM(TOTAL_SALE_AMT) - SUM(CARD_AMT)) AS SUM")	)
							->where("CORP_MK", $this->getCorpId())
							->where(DB::raw("DATEPART(YY, WRITE_DATE)"), Request::Input("INPUT_YEAR"))
							->where(DB::raw("DATEPART(MM, WRITE_DATE)"), Request::Input("INPUT_MONTH"))
							->where("CUST_MK", Request::Input("CUST_MK"))
							
							->groupBy(DB::raw("DATEPART(MM, WRITE_DATE)"))
							->first();

			
		}else{

			$STOCK_INFO	 = DB::table("SALE_INFO_M")
								->select( DB::raw( "SUM(TOTAL_SALE_AMT) AS SUM")	)
								->where("CORP_MK", $this->getCorpId())
								->where(DB::raw("DATEPART(YY, WRITE_DATE)"), Request::Input("INPUT_YEAR"))
								->where(DB::raw("DATEPART(MM, WRITE_DATE)"), Request::Input("INPUT_MONTH"))
								->where("CUST_MK", Request::Input("CUST_MK"))
								
								->groupBy(DB::raw("DATEPART(MM, WRITE_DATE)"))
								->first();
		
		}

		return response()->json(
									[ 
									  'SUM' => $STOCK_INFO == null ? 0 : $STOCK_INFO->SUM
									, 'WRITE_DATE'=> $this->endday(Request::Input("INPUT_YEAR"), Request::Input("INPUT_MONTH")) 
									, 'GOODS' => '활어'
									, 'REMARK' => ''
									]
								);
	
	}

	// 매입계산서 저장
	public function setTaxBuy(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'required|numeric',
			'GOODS'					=> 'required',
			
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );
				DB::table("TAX_ACCOUNT_M")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'I'
								,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
								,"CUST_MK"			=> Request::Input("CUST_MK")
								,"REMARK"			=> Request::Input("REMARK")
								,"TAX_STATTUS"		=> '1'
							]
						);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );
				DB::table("TAX_ACCOUNT_D")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> 'I'
								,"SRL_NO"			=> (int)$TAX_ACCOUNT_MAX_SRL + 1
								,"MONTHS"			=> Request::Input("MONTHS")
								,"DAYS"				=> Request::Input("DAYS")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	// 매입계산서 수정=>불러오기
	public function getTaxBuyItem(){
		
		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
							->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
							->where("TAX_M.SEQ", Request::Input("SEQ"))
							->where("TAX_M.BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
							->where("TAX_D.SRL_NO", Request::Input("SRL_NO"))->first();
		
		
		return response()->json($TAX_CCOUNT_M);
	
	}

	private function endday($ddlYear, $ddlMonth)
	{
		$mon;
		$monthDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];


		$yy = (int)($ddlYear);
		$mm = (int)($ddlMonth);

		if ((($yy % 4 == 0) && ($yy % 100 != 0)) || ($yy % 400 == 0)){ 
			$monthDays[1] = 29; 
		}

		$nDays = $monthDays[$mm - 1];

		
		if (strlen($mm) < 2)
		{
			$mon = "0".$mm;
		}else{
			$mon = $mm;
		}
		
		return $yy."-".$mon."-".$nDays;

	}

	

	public function indexSale($tax)
	{
		$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")
					->where("CORP_MK", $this->getCorpId())->get();

		return view("account.tax_sale",[ "tax" => $tax, "CUST_GRP_INFO" => $CUST_GRP_INFO ] );
	}

	public function getIndexSaleCust()
	{
		$CUST_CD = DB::table("CUST_CD AS CC")
							->select(
										  "CC.CUST_MK"
										, "CC.CORP_MK"
										, "CC.CUST_GRP_CD"
										, "CC.CORP_DIV"
										, "CC.FRNM"
										, "CC.RPST"
										, "CC.RANK"
										,DB::raw( "( SUBSTRING(CC.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CC.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CC.ETPR_NO, 6, 5) ) AS ETPR_NO") 
										, DB::raw(" CASE WHEN CC.RANK ='SU' THEN '우수'
										     WHEN CC.RANK ='GE' THEN '일반'
										     WHEN CC.RANK ='BD' THEN '불량' END AS RANK_NM ")

										, "CC.PHONE_NO"
										, "CG.CUST_GRP_NM"
										, "CC.AREA_CD"
										, "AC.AREA_NM"
							)->leftjoin("CUST_GRP_INFO AS CG", function($join){
									$join->on("CC.CORP_MK", "=", "CG.CORP_MK");
									$join->on("CC.CUST_GRP_CD", "=", "CG.CUST_GRP_CD");
							})->leftjoin("AREA_CD AS AC", function($join){
									$join->on("CC.AREA_CD", "=", "AC.AREA_CD ");
							})
							->where("CC.CORP_MK", $this->getCorpId())
							->where("CC.CORP_DIV", Request::Input("corp_div"));

		return Datatables::of($CUST_CD)
					
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("CC.FRNM", 'LIKE', '%'.Request::Input("textSearch").'%' );
					}
					if( Request::Has('srtCondition') && Request::Input('srtCondition') != 'ALL'){
						$query->where("CC.CUST_GRP_CD", Request::Input("srtCondition") );
					}
				}
		)->make(true);
	
	}

	// PDF : 매출세금계산서 선택 출력
	public function getPdfSaleTax(){
		

		$WRITE_DATE	= Request::segment(4);
		$SEQ		= Request::segment(5);
		$CUST_MK	= Request::segment(6);
		$WHITE		= Request::segment(7);
		
		$TAX_CCOUNT_SALE = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										//"CUST.ETPR_NO" , 
			DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										DB::raw("CORP.FRNM AS CORP_NM"),
										//DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw( "( SUBSTRING(CORP.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CORP.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CORP.ETPR_NO, 6, 5) ) AS CORP_ETPR_NO"), 
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR2 AS CORP_ADDR2")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", $CUST_MK)
							->where("TAX_M.WRITE_DATE", $WRITE_DATE)
							->where("TAX_M.SEQ" , $SEQ)->first();
		

		$TAX_CCOUNT_D = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS DD"),
										"TAX_M.SEQ" , 
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO"
							)->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.WRITE_DATE", $WRITE_DATE)
							->where("TAX_M.SEQ", $SEQ)
							->where("TAX_M.CUST_MK", $CUST_MK)->get();


		// 일련번호
		$M_SRL_NO = DB::table("DOC_NUMBER")
					->select("SRL_NO")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
					->where("MONTH", $TAX_CCOUNT_SALE->M_MM );
		
		$SRL_NO = 0;
		// 일련번호가 없으면 생성해줌
		if( count($M_SRL_NO->get()) == 0 ){
			DB::table("DOC_NUMBER")->insert(
					[
						 "CORP_MK"	=> $this->getCorpId()
						,"YEAR"		=> $TAX_CCOUNT_SALE->M_YY 
						,"MONTH"	=> $TAX_CCOUNT_SALE->M_MM
						,"SRL_NO"	=> 1
					]
			);

			$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM."01";

		}else{
			// 일련번호 최대값 가져오고 update(srl_no +1) 
			$M_SRL_NO = DB::table("DOC_NUMBER")
				->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
				->where("MONTH", $TAX_CCOUNT_SALE->M_MM )->first();
			
			$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

			$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM.$M_SRL_NO->NEW_SRL_NO;

			DB::table("DOC_NUMBER")
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
				->where("MONTH", $TAX_CCOUNT_SALE->M_MM )
				->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);

		}

		$strAMT = strval((int)$TAX_CCOUNT_SALE->SUPPLY_AMT);
		$strSum = [];

		// 초기화
		for($i=0; $i<11; $i++){
			$strSum[$i] = 0;
		}
		$index = 10;
		for( $i=strlen($strAMT)-1; $i >= 0 ; $i--) { 
			$strSum[$index--] = $strAMT[$i];
		}
		
		$viewBlade = "account.pdf_TaxSale";
		if( $WHITE == "1"){
			$viewBlade = "account.pdf_TaxSaleW";
		}
			
		$pdf = PDF::loadView($viewBlade, 
								[
									'model'			=> $TAX_CCOUNT_SALE,
									'mlist'			=> $TAX_CCOUNT_D,
									'title'			=> '세금계산서',
									'srl_no'		=> $SRL_NO,
									'cntNull'		=> 11-strlen($strAMT),
									'arrStrSum'		=> $strSum,
								]
							);
		
		//$lastPage = $pdf->getPage();
		//$pdf->deletePage($lastPage);
		return $pdf->stream();

	}

	public function detailSale($tax){
		
		// 해당거래처정보 가져오기
		$TAX_CCOUNT_M = DB::table("CUST_CD AS CUST")
						->select(
									"CUST.FRNM" , 
									DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
									"CUST.RPST" , 
									"CUST.JUMIN_NO" , 
									"CUST.FAX" , 
									"CUST.PHONE_NO" , 
									"CUST.ADDR1" , 
									"CUST.ADDR2" ,
									"CUST.UPJONG" ,
									"CUST.UPTE" 
						)
						->where("CUST.CORP_MK", $this->getCorpId())
						->where("CUST.CUST_MK", urldecode(Request::segment(4)))->first();

		//d($TAX_CCOUNT_M);
		return view("account.tax_SaleDetail",[ "tax" => $tax, "model" => $TAX_CCOUNT_M ] );
	
	}

	// PDF : 매출세금계산서 전체 출력
	public function getPdfSaleTaxAll($tax, $WRITE_DATE_START, $WRITE_DATE_END, $CUST_MK){

		
		$TAX_CCOUNT_SALE = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
			DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										DB::raw("CORP.FRNM AS CORP_NM"),
										//DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw( "( SUBSTRING(CORP.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CORP.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CORP.ETPR_NO, 6, 5) ) AS CORP_ETPR_NO"), 
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR2 AS CORP_ADDR2"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS DD")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", $CUST_MK)
							->where("TAX_M.WRITE_DATE", ">=", $WRITE_DATE_START)
							->where("TAX_M.WRITE_DATE", "<=", $WRITE_DATE_END)
							->where(function($query){
								
								$arrDt = null;
								$arrCod = null;
								if( Request::segment(8) != "" &&  !empty(Request::segment(8)) ){
									$arrDt = explode(',', Request::segment(8));
								}

								if( Request::segment(7) != "" &&  !empty(Request::segment(7)) ){
									$arrCode = explode(',', Request::segment(7));
								}
								
								if( count($arrDt) > 0 && count($arrDt) == count($arrCode)){

									for($i=0; $i<count($arrDt); $i++){
										$query->orwhere("TAX_M.WRITE_DATE", $arrDt[$i])
												->where("TAX_M.SEQ", $arrCode[$i]);
									}
								}
							})
							->get();
		
		$ARR_SRL_NO = [];
		
		foreach( $TAX_CCOUNT_SALE as $item){
			$M_SRL_NO = DB::table("DOC_NUMBER")
						->select("SRL_NO")
						->where("CORP_MK", $this->getCorpId())
						->where("YEAR", $item->M_YY )
						->where("MONTH", $item->M_MM );
			
			$SRL_NO = 0;
			// 일련번호가 없으면 생성해줌
			if( count($M_SRL_NO->get()) == 0 ){
				DB::table("DOC_NUMBER")->insert(
						[
							 "CORP_MK"	=> $this->getCorpId()
							,"YEAR"		=> $item->M_YY 
							,"MONTH"	=> $item->M_MM
							,"SRL_NO"	=> 1
						]
				);

				$ARR_SRL_NO[] = $item->M_YY.$item->M_MM."01";

			}else{
				// 일련번호 최대값 가져오고 update(srl_no +1) 
				$M_SRL_NO = DB::table("DOC_NUMBER")
					->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $item->M_YY )
					->where("MONTH", $item->M_MM )->first();
				
				$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

				$ARR_SRL_NO[] = $item->M_YY.$item->M_MM.$M_SRL_NO->NEW_SRL_NO;

				DB::table("DOC_NUMBER")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $item->M_YY )
					->where("MONTH", $item->M_MM )
					->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);

			}
		}

		$strAMT = [];
		$strSum[][] = 0;
		$cntNull = [];
		
		for( $k=0; $k<count($TAX_CCOUNT_SALE); $k++){
			$index = 10;
			// 초기화
			for($i=0; $i<11; $i++){
				$strSum[$k][$i] = 0;
			}
			$strAMT[$k] = strval((int)$TAX_CCOUNT_SALE[$k]->SUPPLY_AMT);

			for( $i=strlen($strAMT[$k])-1; $i >= 0 ; $i--) { 
				$strSum[$k][$index--] = $strAMT[$k][$i];
			}
			$cntNull[$k] = 11-strlen($strAMT[$k]);
		}
		
		$viewBlade = "account.pdf_TaxSaleAll";

		if( Request::input("W") == "1"){
			$viewBlade = "account.pdf_TaxSaleAllW";
		}
		
		$pdf = PDF::loadView($viewBlade, 
								[
									'model'			=> $TAX_CCOUNT_SALE,
									'srl_no'		=> $ARR_SRL_NO,
									'title'			=> '세금계산서',
									'cntNull'		=> $cntNull,
									'arrStrSum'		=> $strSum,
								]
							);

	
		return $pdf->stream();

	}
					
	// DATATABLE : 매출계산서 목록 
	public function getTaxSaleAccountList(){

		$TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", urldecode(Request::Input("CUST_MK")));
			
		return Datatables::of($TAX_CCOUNT_M)
					
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("TAX_M.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}
				}
		)->make(true);
	
	}

	// JSON : 매출계산서 등록
	public function setTaxSale(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'			=> 'required|max:1,NOT_NULL',
			'RECPT_CLAIM_DIV'		=> 'required|max:1,NOT_NULL',
			'SUPPLY_AMT'			=> 'numeric'
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
			//DB::beginTransaction();
			try {
				
				$TAX_ACCOUNT_MAX_SEQ = $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") );
				
				DB::table("TAX_ACCOUNT_M")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> Request::Input("BUY_SALE_DIV")
								,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")
								,"CUST_MK"			=> urldecode(Request::Input("CUST_MK"))
								,"REMARK"			=> Request::Input("REMARK")
							]
						);
				
				$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), Request::Input("WRITE_DATE"), (int)$TAX_ACCOUNT_MAX_SEQ + 1 );

				DB::table("TAX_ACCOUNT_D")
						->insert(
							[
								 "CORP_MK"			=> $this->getCorpId()
								,"WRITE_DATE"		=> Request::Input("WRITE_DATE")
								,"SEQ"				=> (int)$TAX_ACCOUNT_MAX_SEQ + 1
								,"BUY_SALE_DIV"		=> Request::Input("BUY_SALE_DIV")
								,"SRL_NO"			=> (int)$TAX_ACCOUNT_MAX_SRL + 1
								,"MONTHS"			=> Request::Input("MONTHS")
								,"DAYS"				=> Request::Input("DAYS")
								,"GOODS"			=> Request::Input("GOODS")
								,"SUPPLY_AMT"		=> Request::Input("SUPPLY_AMT") 
							]
						);
				
				
				return response()->json(['result' => 'success', 'SEQ' => (int)$TAX_ACCOUNT_MAX_SEQ + 1, ]);

			}catch(\ValidationException $e){
				//DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	
	}

	public function deleteTaxSale(){
		
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'			=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'			=> 'required|max:1,NOT_NULL',
			'SEQ'					=> 'required|numeric',
			'SRL_NO'				=> 'required|numeric',
			//'SUPPLY_AMT'			=> 'numeric'

		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		//return $exception = DB::transaction(function(){
		
			try {
				
				DB::table("TAX_ACCOUNT_D")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",	Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->where("SRL_NO", Request::Input("SRL_NO"))
						->delete();

				DB::table("TAX_ACCOUNT_M")
						->where("CORP_MK", $this->getCorpId())
						->where("SEQ",	Request::Input("SEQ"))
						->where("WRITE_DATE",	Request::Input("WRITE_DATE"))
						->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->delete();

				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors = json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		//});
	
	}

	// View : 세금계산서 합계
	public function index($tax)
	{
		$CUST_GRP = DB::table("CUST_GRP_INFO")
					->select("CUST_GRP_CD" ,"CUST_GRP_NM")
					->where("CORP_MK", $this->getCorpId())->get();
		
		return view("account.list",[ "tax" => $tax, "custGrp" => $CUST_GRP] );
	}

	// DataTable : 세금계산서 거래처별 합계 목록
	public function getTaxCustSumList(){
	
		$TAX_SUM_CUST = DB::table(DB::raw("
							 ( SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, M.CUST_MK
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
							    FROM TAX_ACCOUNT_D AS D 
									 INNER JOIN TAX_ACCOUNT_M AS M 
											  ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
							   WHERE M.CORP_MK = '".$this->getCorpId()."' 
								 AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
								 AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
								GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T")
					)->leftjoin("CUST_CD AS C", function($join){
						$join->on("T.CORP_MK", "=", "C.CORP_MK");
						$join->on("T.CUST_MK", "=", "C.CUST_MK");
					})->select(
						  "T.CORP_MK"
						, "T.CUST_MK"
						, "C.FRNM"
						, DB::raw("SUM(T.INPUT) AS INPUT")
						, DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						, DB::raw("SUM(T.OUT) AS OUT")
						, DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
					)->where(function($query){
					
						if( Request::has("srtCondition") && Request::Input("srtCondition") !== "ALL"){
							$query->where("C.CUST_GRP_CD", Request::Input("srtCondition"));
						}
						if( Request::has("textSearch") && Request::Input("textSearch") !== ""){
							$query->where("C.FRNM", "LIKE", "%".Request::Input("textSearch")."%");
						}
					})->groupBy("T.CORP_MK", "T.CUST_MK", "C.FRNM");
		
		return Datatables::of($TAX_SUM_CUST)->make(true);		
	
	}

	// DataTable : 세금계산서 매출계산서 합계 목록
	public function getTaxSaleSumList(){
	
		$tax_sum_sale = DB::table(DB::raw("
							 (SELECT T.CORP_MK
								   , T.CUST_MK
								    , C.FRNM
								    , SUM(T.OUT) AS OUT
								    , SUM(T.OUT_CNT) AS OUT_CNT
								    , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								FROM CUST_CD AS C 
									 INNER JOIN (SELECT D.BUY_SALE_DIV
													  , M.CORP_MK
													  , M.CUST_MK
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT
												   FROM TAX_ACCOUNT_D AS D 
												         INNER JOIN TAX_ACCOUNT_M AS M 
																			 ON D.CORP_MK = M.CORP_MK 
																			 AND D.WRITE_DATE = M.WRITE_DATE 
																			 AND D.SEQ = M.SEQ 
																			 AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
												   WHERE M.CORP_MK = '".$this->getCorpId()."' 
												    AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
												    AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."' 

												   GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
													 ON C.CORP_MK = T.CORP_MK 
													 AND C.CUST_MK = T.CUST_MK 
												GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS sale")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "OUT"
						, "OUT_CNT"
						, "ETPR_NO"
					)->where(function($query){
					
						if( Request::has("srtCondition") && Request::Input("srtCondition") !== "ALL"){
							//$query->where("CUST_GRP_CD", Request::Input("srtCondition"));
						}
						if( Request::has("textSearch") && Request::Input("textSearch") !== ""){
							$query->where("FRNM", "LIKE", "%".Request::Input("textSearch")."%");
						}
					})->where("OUT_CNT",">", 0);
		
		return Datatables::of($tax_sum_sale)->make(true);		
	
	}

	// DataTable : 세금계산서 매입계산서 합계 목록
	public function getTaxInputSumList(){
	
		$TAX_SUM_STOCK = DB::table(DB::raw("
							   (SELECT T.CORP_MK
									 , T.CUST_MK
									 , C.FRNM
									 , SUM(T.INPUT) AS INPUT
									 , SUM(T.INPUT_CNT) AS INPUT_CNT
									 , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								 FROM CUST_CD AS C 
									  INNER JOIN (SELECT D.BUY_SALE_DIV
														 , M.CORP_MK
														  , M.CUST_MK
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT 
													FROM TAX_ACCOUNT_D AS D 
														 INNER JOIN TAX_ACCOUNT_M AS M 
																 ON D.CORP_MK = M.CORP_MK 
																AND D.WRITE_DATE = M.WRITE_DATE 
																AND D.SEQ = M.SEQ 
																AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
														  WHERE M.CORP_MK = '".$this->getCorpId()."'
															AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
															AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
														  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
										   ON C.CORP_MK = T.CORP_MK 
										  AND C.CUST_MK = T.CUST_MK 
										  GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS buy")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "INPUT"
						, "INPUT_CNT"
						, "ETPR_NO"
					)->where("INPUT",">", 0);

		
		return Datatables::of($TAX_SUM_STOCK)->make(true);		
	
	}

	// DataTable : 세금계산서 연간 합계 목록
	public function getTaxYearSumList(){
	
		$TAX_SUM_YEAR = DB::table(DB::raw("
							  (SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, MONTH(M.WRITE_DATE) AS MONTHS
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
								 FROM TAX_ACCOUNT_D AS D 
									  INNER JOIN (  SELECT  A.CORP_MK
														  , A.WRITE_DATE
														  , A.SEQ
														  , A.BUY_SALE_DIV
														  , A.RECPT_CLAIM_DIV
														  , A.CUST_MK
														  , A.ETPR_NO
														  , A.FRNM
														  , A.NAME
														  , A.TAX_ADDR1
														  , A.TAX_ADDR2
														  , A.UPTE
														  , A.UPJONG
														  , A.SUM_AMT
														  , A.CASH_AMT
														  , A.CHECK_AMT
														  , A.CARD_AMT
														  , A.UNCL_AMT
														  , A.ACCOUNT_DIV
														  , A.REMARK 
													 FROM TAX_ACCOUNT_M AS A 
														  INNER JOIN CUST_CD AS B 
																  ON A.CUST_MK = B.CUST_MK 
																 AND A.CORP_MK = B.CORP_MK) AS M 
												ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
								WHERE M.CORP_MK = '".$this->getCorpId()."' 
								  AND M.WRITE_DATE >= '".Request::Input("WRITE_DATE_START")."'
								  AND M.WRITE_DATE <= '".Request::Input("WRITE_DATE_END")."'
						  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, MONTH(M.WRITE_DATE)) AS T "
					))->join("CORP_INFO", function($join){
							$join->on("T.CORP_MK", "=", "CORP_INFO.CORP_MK");
					})->leftjoin("DECLARATION AS D", function($join){
							$join->on("T.CORP_MK", "=", "D.corp_mk");
							$join->on("T.MONTHS" , "=", DB::raw("MONTH(D.dec_month) "));
					})->select(
						    "T.CORP_MK"
						  , "T.MONTHS"
						  , DB::raw("SUM(T.INPUT) AS INPUT")
						  , DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						  , DB::raw("SUM(T.OUT) AS OUT")
						  , DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
						  , "D.dec_money"
						  , DB::raw("ROUND(SUM(T.INPUT) / D.dec_money * 100, 0) AS saleVSbuy")
						  , DB::raw("D.dec_money - SUM(T.OUT) AS overSale")
						  , DB::raw("ROUND(D.dec_money * (MAX(D.dec_percent) * 0.01), 0) - SUM(T.INPUT) AS overBuy")
						  , "D.dec_percent"
						  , "CORP_INFO.FRNM"
					)->groupBy("T.CORP_MK", "T.MONTHS", "D.dec_money", "D.dec_percent", "CORP_INFO.FRNM");

		
		return Datatables::of($TAX_SUM_YEAR)->make(true);		
	
	}

	// PDF : 1. 매출계산서 합계 출력
	public function PdfsaleSum(){
		
		$START_DATE = Request::segment(4);
		$END_DATE = Request::segment(5);
		//$CUST_GRP = Request::segment(6);

		$TAX_SUM_SALE = DB::table(DB::raw("
							 (SELECT T.CORP_MK
								   , T.CUST_MK
								    , C.FRNM
								    , SUM(T.OUT) AS OUT
								    , SUM(T.OUT_CNT) AS OUT_CNT
								    , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								FROM CUST_CD AS C 
									 INNER JOIN (SELECT D.BUY_SALE_DIV
													  , M.CORP_MK
													  , M.CUST_MK
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
													  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT
												   FROM TAX_ACCOUNT_D AS D 
												         INNER JOIN TAX_ACCOUNT_M AS M 
																			 ON D.CORP_MK = M.CORP_MK 
																			 AND D.WRITE_DATE = M.WRITE_DATE 
																			 AND D.SEQ = M.SEQ 
																			 AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
															 WHERE M.CORP_MK = '".$this->getCorpId()."' 
															   AND M.WRITE_DATE >= '".$START_DATE."'
															   AND M.WRITE_DATE <= '".$END_DATE."' 
												   GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
													 ON C.CORP_MK = T.CORP_MK 
													 AND C.CUST_MK = T.CUST_MK 
												GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS sale")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "OUT"
						, "OUT_CNT"
						, "ETPR_NO"
						//, DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO")
					)->where("OUT_CNT",">", 0)
					->orderBy("FRNM", "ASC")
					->get();

		
		$sumSale		= 0; // 매출 금액 합계
		$saumSaleQty	= 0; // 매출 매수 합계
	
		foreach($TAX_SUM_SALE as $info){
			$sumSale		+= $info->OUT;
			$saumSaleQty	+= $info->OUT_CNT;
		}

		$pdf = PDF::loadView("account.pdf_SumSale", 
								[
									'model'			=> $TAX_SUM_SALE,
									'title'			=> "매출계산서 합계",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'start'			=> $START_DATE ,
									'end'			=> $END_DATE,
									'sumSale'		=> $sumSale,
									'saumSaleQty'	=> $saumSaleQty,
								]
							);

		return $pdf->stream();
	
	}

	// PDF : 2. 매입계산서 합계 출력
	public function PdfbuySum(){
		
		$START_DATE = Request::segment(4);
		$END_DATE = Request::segment(5);

		$TAX_SUM_STOCK = DB::table(DB::raw("
							   (SELECT T.CORP_MK
									 , T.CUST_MK
									 , C.FRNM
									 , SUM(T.INPUT) AS INPUT
									 , SUM(T.INPUT_CNT) AS INPUT_CNT
									 , STUFF(STUFF(C.ETPR_NO, 4, 0, '-'), 7, 0, '-') AS ETPR_NO 
								 FROM CUST_CD AS C 
									  INNER JOIN (SELECT D.BUY_SALE_DIV
														 , M.CORP_MK
														  , M.CUST_MK
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
														  , ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT 
													FROM TAX_ACCOUNT_D AS D 
														 INNER JOIN TAX_ACCOUNT_M AS M 
																 ON D.CORP_MK = M.CORP_MK 
																AND D.WRITE_DATE = M.WRITE_DATE 
																AND D.SEQ = M.SEQ 
																AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
														  WHERE M.CORP_MK = '".$this->getCorpId()."'
															AND M.WRITE_DATE >= '".$START_DATE."'
															AND M.WRITE_DATE <= '".$END_DATE."'
														  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, M.CUST_MK) AS T 
										   ON C.CORP_MK = T.CORP_MK 
										  AND C.CUST_MK = T.CUST_MK 
										  GROUP BY T.CORP_MK, T.CUST_MK, C.FRNM, C.ETPR_NO) AS buy")
					)->select(
						  "CORP_MK"
						, "CUST_MK"
						, "FRNM"
						, "INPUT"
						, "INPUT_CNT"
						, "ETPR_NO"
						//, DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO")
					)->where("INPUT",">", 0)
					->where(function($query){

						if( Request::segment(6) != "ALL" ){
							$query->where("C.CUST_GRP_CD", Request::segment(6));
						}
						
					
					})->orderBy("FRNM", "asc")->get();



		$sumInput		= 0; // 매입 금액 합계
		$sumInputQty	= 0; // 매입 매수 합계
	
		foreach($TAX_SUM_STOCK as $info){
			$sumInput		+= $info->INPUT;
			$sumInputQty	+= $info->INPUT_CNT;
		}

		$pdf = PDF::loadView("account.pdf_SumBuy", 
								[
									'model'			=> $TAX_SUM_STOCK,
									'title'			=> "매입계산서 합계",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'start'			=> $START_DATE,
									'end'			=> $END_DATE,
									'sumInput'		=> $sumInput,
									'sumInputQty'	=> $sumInputQty,
								]
							);

		return $pdf->stream();
	
	}
	
	// PDF : 3. 연간합계 계산서 총괄표
	public function PdfyearSum(){
		
		$START_DATE = Request::segment(4);
		$END_DATE = Request::segment(5);

		$TAX_SUM_YEAR = DB::table(DB::raw("
							  (SELECT D.BUY_SALE_DIV
									, M.CORP_MK
									, MONTH(M.WRITE_DATE) AS MONTHS
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN SUM(D.SUPPLY_AMT) END, 0) AS INPUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'I' THEN COUNT(D.SUPPLY_AMT) END, 0) AS INPUT_CNT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN SUM(D.SUPPLY_AMT) END, 0) AS OUT
									, ISNULL(CASE D.BUY_SALE_DIV WHEN 'O' THEN COUNT(D.SUPPLY_AMT) END, 0) AS OUT_CNT 
								 FROM TAX_ACCOUNT_D AS D 
									  INNER JOIN (  SELECT  A.CORP_MK
														  , A.WRITE_DATE
														  , A.SEQ
														  , A.BUY_SALE_DIV
														  , A.RECPT_CLAIM_DIV
														  , A.CUST_MK
														  , A.ETPR_NO
														  , A.FRNM
														  , A.NAME
														  , A.TAX_ADDR1
														  , A.TAX_ADDR2
														  , A.UPTE
														  , A.UPJONG
														  , A.SUM_AMT
														  , A.CASH_AMT
														  , A.CHECK_AMT
														  , A.CARD_AMT
														  , A.UNCL_AMT
														  , A.ACCOUNT_DIV
														  , A.REMARK 
													 FROM TAX_ACCOUNT_M AS A 
														  INNER JOIN CUST_CD AS B 
																  ON A.CUST_MK = B.CUST_MK 
																 AND A.CORP_MK = B.CORP_MK) AS M 
												ON D.CORP_MK = M.CORP_MK 
												AND D.WRITE_DATE = M.WRITE_DATE 
												AND D.SEQ = M.SEQ 
												AND D.BUY_SALE_DIV = M.BUY_SALE_DIV 
								WHERE M.CORP_MK = '".$this->getCorpId()."' 
								  AND M.WRITE_DATE >= '".$START_DATE."'
								  AND M.WRITE_DATE <= '".$END_DATE."'
						  GROUP BY D.BUY_SALE_DIV, M.CORP_MK, MONTH(M.WRITE_DATE)) AS T "
					))->join("CORP_INFO", function($join){
							$join->on("T.CORP_MK", "=", "CORP_INFO.CORP_MK");
					})->leftjoin("DECLARATION AS D", function($join){
							$join->on("T.CORP_MK", "=", "D.corp_mk");
							$join->on("T.MONTHS" , "=", DB::raw("MONTH(D.dec_month) "));
					})->select(
						    "T.CORP_MK"
						  , "T.MONTHS"
						  , DB::raw("SUM(T.INPUT) AS INPUT")
						  , DB::raw("SUM(T.INPUT_CNT) AS INPUT_CNT")
						  , DB::raw("SUM(T.OUT) AS OUT")
						  , DB::raw("SUM(T.OUT_CNT) AS OUT_CNT")
						  , "D.dec_money"
						  , DB::raw("ROUND(SUM(T.INPUT) / D.dec_money * 100, 0) AS saleVSbuy")
						  , DB::raw("D.dec_money - SUM(T.OUT) AS overSale")
						  , DB::raw("ROUND(D.dec_money * (MAX(D.dec_percent) * 0.01), 0) - SUM(T.INPUT) AS overBuy")
						  , "D.dec_percent"
						  , "CORP_INFO.FRNM"
					)->groupBy("T.CORP_MK", "T.MONTHS", "D.dec_money", "D.dec_percent", "CORP_INFO.FRNM")->get();



		$sumInput		= 0; // 매입 금액 합계
		$sumInputQty	= 0; // 매입 매수 합계
		$sumOut			= 0;
		$sumOutQty		= 0;
		

		foreach($TAX_SUM_YEAR as $info){

			$sumInput		+= $info->INPUT;
			$sumInputQty	+= $info->INPUT_CNT;
			$sumOut			+= $info->OUT;
			$sumOutQty		+= $info->OUT_CNT;
		}


		$pdf = PDF::loadView("account.pdf_SumYear", 
								[
									'model'			=> $TAX_SUM_YEAR,
									'title'			=> "계산서 총괄표",
									'corpnm'		=> $this->getCorpInfo()->FRNM,
									'sumInput'		=> $sumInput,
									'sumInputQty'	=> $sumInputQty,
									'sumOut'		=> $sumOut,
									'sumOutQty'		=> $sumOutQty,
								]
							)->setPaper('a4', 'landscape');
							

		return $pdf->stream();
	}

	public function setChangeDeclaration(){

		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|max:4',
			'PERCENTER'		=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {
				
				DB::table("DECLARATION")
						->where("CORP_MK", $this->getCorpId())
						->where(DB::raw("year(dec_month)"), Request::Input("YEAR"))
						->update(
							[
								"dec_percent"		=> Request::Input("PERCENTER")
							]
						);
				
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	}

	// 국세청 세금계산서 조회
	public function elist($tax){
		
		$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")
					->where("CORP_MK", $this->getCorpId())->get();

		$CORP_INFO = DB::table("CORP_INFO")
					->where("CORP_MK", $this->getCorpId())->first();

		return view("account.tax_elist",[ "tax" => $tax , "CUST_GRP_INFO" => $CUST_GRP_INFO, "CORP_INFO" => $CORP_INFO ]  );
	}

	public function elistData(){
		
		// 일련번호 최대값 가져오고 update(srl_no +1) 
		/*
		$M_SRL_NO = DB::table("DOC_NUMBER")
			->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
			->where("CORP_MK", $this->getCorpId())
			->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
			->where("MONTH", $TAX_CCOUNT_SALE->M_MM )->first();
		
		$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

		$SRL_NO = $TAX_CCOUNT_SALE->M_YY.$TAX_CCOUNT_SALE->M_MM.$M_SRL_NO->NEW_SRL_NO;

		DB::table("DOC_NUMBER")
			->where("CORP_MK", $this->getCorpId())
			->where("YEAR", $TAX_CCOUNT_SALE->M_YY )
			->where("MONTH", $TAX_CCOUNT_SALE->M_MM )
			->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);
		*/

	
		$ETAX_LIST = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										DB::raw( "CASE WHEN TAX_M.BUY_SALE_DIV = 'O' THEN '매출' ELSE '매입' END AS 'SALE_DIV_NM'"),
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw( "CASE WHEN TAX_M.RECPT_CLAIM_DIV = '0' THEN '청구' ELSE '영수' END AS 'RECPT_CLAIM_DIV_NM'"),
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										
										"TAX_M.TAX_ACCOUNT_NUMBER" , 
										"TAX_M.TAX_STATTUS" , 
										"TAX_M.TAX_SEND_STATUS" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.TAX_SEND_DATE, 23) AS TAX_SEND_DATE"),

										"TAX_M.TAX_RECEIVE_DATE" , 
										"TAX_M.TAX_ACCOUNT_TYPE",
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER",
										"TAX_M.TAX_ISSUE_NUMBER",
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										//"CUST.ETPR_NO" , 
										DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										"CUST.EMAIL", 
										DB::raw("CORP.FRNM AS CORP_NM"),
										DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR2"),
										DB::raw("CORP.POST_NO AS CORP_POST_NO"),
										DB::raw("CORP.PHONE_NO AS CORP_PHONE_NO"),
										DB::raw("CORP.RPST_EMAIL AS CORP_RPST_EMAIL")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
								
							})->leftjoin("CUST_GRP_INFO AS CUST_GRP", function($join){
									$join->on("CUST.CORP_MK", "=", "CUST_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD", "=", "CUST_GRP.CUST_GRP_CD");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.BUY_SALE_DIV", Request::Input("chkSaleStock"))
							->where(function($query){
								if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
									if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
										$query->whereBetween("TAX_M.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
									}
								}
							});
		
		return Datatables::of($ETAX_LIST)
					
				->filter(function($query) {
					

					if( Request::Has('textSearch') && Request::Input('textSearch') != "" ){
						$query->where("CUST.FRNM", "LIKE", "%".Request::Input("textSearch")."%" );
					}
					
					if( Request::Has('cust_grp') && Request::Input('cust_grp') != "ALL" ){
						$query->where("CUST_GRP.CUST_GRP_CD", Request::Input("cust_grp"));
					}

					if( Request::Has('cust_mk') && Request::Input('cust_mk') != "" ){
						$query->where("TAX_M.CUST_MK", Request::Input("cust_mk") );
					}
					if( Request::Has('status') && Request::Input('status') != "" && Request::Input('status') != "all" ){
						$query->where("TAX_M.TAX_STATTUS",  Request::Input("status"));
					
					}
				}
		)->make(true);
	}

	public function getPdfeTax($tax, $START_DATE, $END_DATE, $CUST_GRP, $CHKST_SA, $STTUS, $SEARCH = ""){
		
		$ETAX_LIST = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
										DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
										DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										DB::raw( "CASE WHEN TAX_M.BUY_SALE_DIV = 'O' THEN '매출' ELSE '매입' END AS 'SALE_DIV_NM'"),
										"TAX_M.RECPT_CLAIM_DIV" ,
										DB::raw( "CASE WHEN TAX_M.RECPT_CLAIM_DIV = '0' THEN '청구' ELSE '영수' END AS 'RECPT_CLAIM_DIV_NM'"),
										DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
										"TAX_M.REMARK" , 
										
										"TAX_M.TAX_ACCOUNT_NUMBER" , 
										"TAX_M.TAX_STATTUS" , 
										"TAX_M.TAX_SEND_STATUS" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.TAX_SEND_DATE, 23) AS TAX_SEND_DATE"),

										"TAX_M.TAX_RECEIVE_DATE" , 
										"TAX_M.TAX_ACCOUNT_TYPE",
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER",
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										//"CUST.ETPR_NO" , 
										DB::raw( "( SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' +  SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' +  SUBSTRING(CUST.ETPR_NO, 6, 5) ) AS ETPR_NO"), 
										"CUST.RPST" , 
										"CUST.UPJONG",
										"CUST.UPTE",
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										"CUST.EMAIL", 
										DB::raw("CORP.FRNM AS CORP_NM"),
										DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR2"),
										DB::raw("CORP.POST_NO AS CORP_POST_NO"),
										DB::raw("CORP.PHONE_NO AS CORP_PHONE_NO"),
										DB::raw("CORP.RPST_EMAIL AS CORP_RPST_EMAIL")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
								
							})->leftjoin("CUST_GRP_INFO AS CUST_GRP", function($join){
									$join->on("CUST.CORP_MK", "=", "CUST_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD", "=", "CUST_GRP.CUST_GRP_CD");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.BUY_SALE_DIV", $CHKST_SA)
							->where(function($query) use($START_DATE, $END_DATE, $CUST_GRP, $CHKST_SA, $STTUS, $SEARCH )  {

								if( $this->fnValidateDate( $START_DATE )){
									if( $this->fnValidateDate( $END_DATE )){
										$query->whereBetween("TAX_M.WRITE_DATE",  array($START_DATE, $END_DATE ));
									}
								}

								if(  $SEARCH != "" ){
									$query->where("CUST.FRNM", "LIKE", "%".$SEARCH."%" );
								}
								
								if( $CUST_GRP != "ALL" ){
									$query->where("CUST_GRP.CUST_GRP_CD", $CUST_GRP);
								}

								if( $STTUS !== "" && $STTUS != "all" ){
									$query->where("TAX_M.TAX_STATTUS",  $STTUS );
								
								}
							})
							->orderBy('TAX_M.WRITE_DATE', 'DESC')
							->orderBy('CUST.FRNM', 'ASC')
							->get();

		$saumSaleQty = 0;
		foreach($ETAX_LIST as $list){
			$saumSaleQty+= $list->SUPPLY_AMT;
		}
		
		$pdf = PDF::loadView('account.pdf_ETax', 
								[
									'model'			=> $ETAX_LIST,
									'title'			=> '계 산 서',
									'saumSaleQty'	=> $saumSaleQty,
									'start'			=> $START_DATE,
									'end'			=> $END_DATE,
								]
							);
		
		return $pdf->stream();
	}

	// 전자세금계산서 정정 
	// Copy => 저장
	
	public function eSetCopyNMod(){
		
		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:Y-m-d',
			'BUY_SALE_DIV'	=> 'required|max:1',
			'SEQ'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		
		$TAX_INFO_M = DB::table("TAX_ACCOUNT_M")
					->where("CORP_MK", $this->getCorpId())
					->where("WRITE_DATE", Request::Input("WRITE_DATE"))
					->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
					->where("SEQ", Request::Input("SEQ"))
					->first();

		$TAX_INFO_D = DB::table("TAX_ACCOUNT_D")
					->where("CORP_MK", $this->getCorpId())
					->where("WRITE_DATE", Request::Input("WRITE_DATE"))
					->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
					->where("SEQ", Request::Input("SEQ"))
					->get();
		
		DB::beginTransaction();

		if( $TAX_INFO_M != null) {
			if( $TAX_INFO_D != null && count($TAX_INFO_D) > 0){
				try{
					// 새로운 SEQ 할당
					$TAX_INFO_M->SEQ = ( $this->getTaxAccountMSeq( $this->getCorpId(), Request::Input("WRITE_DATE") )) + 1;
					$TAX_INFO_M->TAX_ACKNOWLEDGMENT_NUMBER	= $TAX_INFO_M->TAX_ISSUE_NUMBER;
					$TAX_INFO_M->TAX_ISSUE_NUMBER	= null;
					$TAX_INFO_M->TAX_ERR_MSG		= null;
					$TAX_INFO_M->TAX_STATTUS		= '1';
					$TAX_INFO_M->TAX_SEND_STATUS	= '1';
					$TAX_INFO_M->TAX_SEND_DATE		= null;
					$TAX_INFO_M->TAX_RECEIVE_DATE	= null;
					$TAX_INFO_M->TAX_ISSUE_DATE		= null;
					
					DB::table("TAX_ACCOUNT_M")
						->insert(get_object_vars($TAX_INFO_M));
					
					foreach($TAX_INFO_D as $tax_d){
						$TAX_ACCOUNT_MAX_SRL = $this->getTaxAccountDSrlNo($this->getCorpId(), $TAX_INFO_M->WRITE_DATE , $TAX_INFO_M->SEQ );
						
						$tax_d->SEQ = $TAX_INFO_M->SEQ;
						$tax_d->WRITE_DATE = $TAX_INFO_M->WRITE_DATE;
						$tax_d->BUY_SALE_DIV = $TAX_INFO_M->BUY_SALE_DIV;
						$tax_d->SRL_NO = (int)$TAX_ACCOUNT_MAX_SRL + 1;
					
						DB::table("TAX_ACCOUNT_D")->insert(get_object_vars($tax_d));
					}
					DB::commit();

				}catch (Exception $e){
					DB::rollback();
					return response()->json(['result' =>'fail'], 422); 
				}

				$data = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_M.RECPT_CLAIM_DIV" , 
										"TAX_M.TAX_ACCOUNT_TYPE" , 
										"TAX_M.TAX_PAYWAY" , 
										"TAX_M.TAX_ACCOUNT_PART" , 
										"TAX_M.TAX_ISSUE_NUMBER" , 
										"TAX_M.TAX_MODIFY_CODE" , 
										"TAX_M.TAX_REPORT_DIVISION",
										"TAX_M.TAX_ISSUE_DIVISION",
										"TAX_M.REMARK" , 
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" 
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.WRITE_DATE", $TAX_INFO_M->WRITE_DATE )
							->where("TAX_M.SEQ", $TAX_INFO_M->SEQ)
							->where("TAX_M.BUY_SALE_DIV", $TAX_INFO_M->BUY_SALE_DIV)
							->first();
	
				return response()->json(['result' =>'success', 'data' => $data]);
			}
		}
		
		DB::rollback();
		return response()->json(['result' =>'fail'], 422); 


	}

	// 전자세금계산서 작성 저장
	public function eUpdateData(){
		
		// 1. 세금계산서 Master 정보를 밸리데이션
		$RequestM = (object) [];
		$RequestArr = [];

		foreach (Request::Input()['param'] as $item){
			$RequestM->$item['name'] = $item['value'];
			$RequestArr[$item['name']] = $item['value'];
		}
		
		$validatorM = Validator::make($RequestArr, [
			'WRITE_DATE'		=> 'required',
			'BUY_SALE_DIV'		=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 2. 세금계산서 Detail 정보를 밸리데이션
		$RequestD = (object) [];
		$RequestDArr = [];

		foreach (Request::Input()['param'] as $item){
			$RequestD->$item['name'] = $item['value'];
			$RequestDArr[$item['name']] = $item['value'];
		}


		$validatorV = Validator::make($RequestArr, [
			'SRL_NO'		=> 'required',
			'SEQ'			=> 'required',
		]);

		if ($validatorV->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){

			try{
				DB::table("TB_ACCOUNT_M")
					->where("CORP_MK", $this->getCorpId())
					->where("WRITE_DATE", Request::Input("WRITE_DATE"))
					->where("SEQ", Request::Input("SEQ"))
					->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
					->update(
								[
								   "BUY_SALE_DIV"		=> Request::Input("BUY_SALE_DIV")			// 매입매출구분
								  ,"RECPT_CLAIM_DIV"	=> Request::Input("RECPT_CLAIM_DIV")		// 영수/청구구분
								
								/*
								  ,"CUST_MK"			=> CUST_MK
								  ,"ETPR_NO"			=> ETPR_NO
								  ,"FRNM"				=> FRNM
								  ,"NAME"				=> NAME
								  ,"TAX_ADDR1"			=> TAX_ADDR1
								  ,"TAX_ADDR2"			=> TAX_ADDR2
								  ,"UPTE"				=> UPTE
								  ,"UPJONG"				=> UPJONG
								  ,"SUM_AMT"			=> SUM_AMT
								  ,"CASH_AMT"			=> CASH_AMT
								  ,"CHECK_AMT"			=> CHECK_AMT
								  ,"CARD_AMT"			=> CARD_AMT
								  ,"UNCL_AMT"			=> UNCL_AMT
								  ,"ACCOUNT_DIV"		=> ACCOUNT_DIV
								*/
								  ,"TAX_ISSUE_DIVISION"	=> Request::Input("TAX_ISSUE_DIVISION")		// 발급구분
								  ,"REMARK"				=> Request::Input("REMARK")					// 비고
								  ,"TAX_ACCOUNT_NUMBER"	=> Request::Input("TAX_ACCOUNT_NUMBER")		// 계산서번호
								  ,"TAX_STATTUS"		=> "1"										// 상태값 : 저장

								  ,"TAX_ACKNOWLEDGMENT_NUMBER"	=> Request::Input("TAX_ACKNOWLEDGMENT_NUMBER")	// 승인번호 
								  ,"TAX_ISSUE_NUMBER"	=> Request::Input("TAX_ISSUE_NUMBER")		// 기승인번호
								  ,"TAX_BOOK"			=> Request::Input("TAX_BOOK")				// 책번호
								  ,"TAX_NO"				=> Request::Input("TAX_NO")					// 권
								  ,"TAX_ISSUE_DATE"		=> Request::Input("TAX_ISSUE_DATE")			// 발행일자
								  ,"TAX_ADDITIONAL_TAX_DIVISION" => Request::Input("TAX_ADDITIONAL_TAX_DIVISION")	// 부가세구분
								  ,"TAX_REPORT_DIVISION"=> Request::Input("TAX_REPORT_DIVISION")	// 신고구분
								  ,"TAX_ACCOUNT_PART"	=> Request::Input("TAX_ACCOUNT_PART")		// 계산서 분류
								  ,"TAX_PAYWAY"			=> Request::Input("TAX_PAYWAY")				// 결제구분
								  ,"TAX_ACCOUNT_TYPE"	=> Request::Input("TAX_ACCOUNT_TYPE")		// 계산서 종류
								  ,"TAX_MODIFY_CODE"	=> Request::Input("TAX_MODIFY_CODE")		// 수정코드
								]
						);
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);

				}else{
					return $e;
				}
			}
		});
	}

	private function getTaxInfo($pWRITE_DATE, $pCUST_MK, $pBUY_SALE_DIV, $pSEQ, $pSRL_NO  ){

		return $ETAX = DB::table("TAX_ACCOUNT_M AS TAX_M")
					->select(
								"TAX_M.CUST_MK" , 
								DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 23) AS WRITE_DATE"),
								DB::raw( "datepart(yyyy, TAX_M.WRITE_DATE) AS M_YY"),
								DB::raw( "datepart(mm, TAX_M.WRITE_DATE) AS M_MM"),
								DB::raw( "datepart(dd, TAX_M.WRITE_DATE) AS M_DD"),
								"TAX_M.SEQ" , 
								"TAX_M.BUY_SALE_DIV" , 
								"TAX_M.RECPT_CLAIM_DIV" ,
								DB::raw("CONVERT(bit, CASE TAX_M.RECPT_CLAIM_DIV WHEN 1 THEN 0 ELSE 1 END) AS CLAIM"),
								"TAX_M.REMARK" , 

								"TAX_M.TAX_ACCOUNT_NUMBER" , 
								"TAX_M.TAX_TYPE" , 
								"TAX_M.TAX_STATTUS" , 
								"TAX_M.TAX_SEND_STATUS" , 
								"TAX_M.TAX_SEND_DATE" , 
								"TAX_M.TAX_RECEIVE_DATE" , 

								DB::raw("CORP.FRNM AS CORP_NM"),
								DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
								DB::raw("CORP.RPST AS CORP_RPST"),
								DB::raw("CORP.UPJONG AS CORP_UPJONG"),
								DB::raw("CORP.UPTE AS CORP_UPTE"),
								DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
								DB::raw("CORP.ADDR1 AS CORP_ADDR2")
					
					)->join("CORP_INFO AS CORP", function($join){
						$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
					})->where("TAX_M.CORP_MK", $this->getCorpId())
					->where("TAX_M.CUST_MK", $pCUST_MK)
					->where("TAX_M.BUY_SALE_DIV", $pBUY_SALE_DIV)
					->where("TAX_M.SEQ", $pSEQ);

	}

	// 국세청 세금계산서 상세 조회
	public function eDetailData(){
	
		$TAX_D = DB::table("TAX_ACCOUNT_D AS D")
						->join("TAX_ACCOUNT_M AS M", function($join){
							$join->on("D.CORP_MK", "=", "M.CORP_MK");
							$join->on("D.WRITE_DATE", "=", "M.WRITE_DATE");
							$join->on("D.BUY_SALE_DIV", "=", "M.BUY_SALE_DIV");
							$join->on("D.SEQ", "=", "M.SEQ");		
						})->where("D.CORP_MK", $this->getCorpId())
						->where("D.WRITE_DATE", Request::Input("WRITE_DATE"))
						->where("D.SEQ", Request::Input("SEQ"))
						->where("D.BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
						->select("D.CORP_MK"
								, DB::raw( "CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								, "D.SEQ"
								, "D.BUY_SALE_DIV"
								, "D.SRL_NO"
								, "D.MONTHS"
								, "D.DAYS"
								, "D.GOODS"
								, "D.QTY"
								, "D.UNCS"
								, "D.SUPPLY_AMT"
								, "D.TAX"
								, "M.RECPT_CLAIM_DIV"
						);
			
		return Datatables::of($TAX_D)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("GOODS",  "like", "%".Request::Input('textSearch')."%");
					}
		})->make(true);
	
	}

	private function getTaxInfForSend($CUST_MK, $WRITE_DATE, $SEQ, $BUY_SALE_DIV, $SRL_NO){

		return $TAX_CCOUNT_M = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(10), TAX_M.WRITE_DATE, 112) AS WRITE_DATE"),
										DB::raw( "CONVERT(CHAR(10), TAX_M.TAX_SEND_DATE, 112) AS TAX_SEND_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" ,
										"TAX_M.RECPT_CLAIM_DIV" ,
										"TAX_M.TAX_ACCOUNT_TYPE" ,
										"TAX_M.TAX_PAYWAY" ,
										"TAX_M.TAX_ACCOUNT_NUMBER",
										"TAX_M.TAX_ACCOUNT_PART" ,
										"TAX_M.TAX_ISSUE_NUMBER" ,
										"TAX_M.TAX_MODIFY_CODE" ,
										"TAX_M.TAX_STATTUS", 
										"TAX_M.TAX_SEND_STATUS", 
										"TAX_M.TAX_ACKNOWLEDGMENT_NUMBER",
										"TAX_M.TAX_REPORT_DIVISION",
										"TAX_M.TAX_ISSUE_DIVISION",
										"TAX_M.REMARK" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										"TAX_D.QTY",
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO",
										"CUST.FRNM" , 
										"CUST.ETPR_NO" , 
										"CUST.RPST" , 
										"CUST.JUMIN_NO" , 
										"CUST.FAX" , 
										"CUST.PHONE_NO" , 
										"CUST.ADDR1" , 
										"CUST.ADDR2" ,
										"CUST.UPTE",
										"CUST.UPJONG",
										"CUST.EMAIL",
										DB::raw("CORP.FRNM AS CORP_NM"),
										DB::raw("CORP.ETPR_NO AS CORP_ETPR_NO"),
										DB::raw("CORP.RPST AS CORP_RPST"),
										DB::raw("CORP.UPJONG AS CORP_UPJONG"),
										DB::raw("CORP.UPTE AS CORP_UPTE"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR1"),
										DB::raw("CORP.ADDR1 AS CORP_ADDR2"),
										DB::raw("CORP.POST_NO AS CORP_POST_NO"),
										DB::raw("CORP.PHONE_NO AS CORP_PHONE_NO"),
										DB::raw("CORP.RPST_EMAIL AS CORP_RPST_EMAIL"),
										"CORP.TAX_EDI_ID",
										"CORP.TAX_EDI_PASS",
										"CORP.CORP_MK"
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("TAX_M.CORP_MK", "=", "CUST.CORP_MK");
									$join->on("TAX_M.CUST_MK", "=", "CUST.CUST_MK");
							})->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})->join("CORP_INFO AS CORP", function($join){
								$join->on("TAX_M.CORP_MK", "=", "CORP.CORP_MK");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", Request::Input("CUST_MK"))
							->where("TAX_M.WRITE_DATE", Request::Input("WRITE_DATE"))
							->where("TAX_M.SEQ", Request::Input("SEQ"))
							->where("TAX_M.BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
							->where("TAX_D.SRL_NO", Request::Input("SRL_NO"))->get();
	

	}

	public function getTaxItem($CUST_MK, $WRITE_DATE, $SEQ, $BUY_SALE_DIV, $SRL_NO){
	
		return $TAX_CCOUNT_D = DB::table("TAX_ACCOUNT_M AS TAX_M")
							->select(
										"TAX_M.CUST_MK" , 
										DB::raw( "CONVERT(CHAR(8), TAX_M.WRITE_DATE, 112) AS WRITE_DATE"),
										"TAX_M.SEQ" , 
										"TAX_M.BUY_SALE_DIV" , 
										"TAX_D.MONTHS",
										"TAX_D.DAYS",
										"TAX_D.GOODS",
										DB::raw("ROUND(TAX_D.QTY, 2) AS QTY"),
										"TAX_D.UNCS",
										"TAX_D.SUPPLY_AMT",
										"TAX_D.TAX",
										"TAX_D.SRL_NO"
										
							)->join("TAX_ACCOUNT_D AS TAX_D", function($join){
									$join->on("TAX_M.CORP_MK", "=", "TAX_D.CORP_MK");
									$join->on("TAX_M.WRITE_DATE", "=", "TAX_D.WRITE_DATE");
									$join->on("TAX_M.SEQ", "=", "TAX_D.SEQ");
									$join->on("TAX_M.BUY_SALE_DIV", "=", "TAX_D.BUY_SALE_DIV");
							})
							->where("TAX_M.CORP_MK", $this->getCorpId())
							->where("TAX_M.CUST_MK", $CUST_MK)
							->where("TAX_M.WRITE_DATE", $WRITE_DATE)
							->where("TAX_M.SEQ", $SEQ)
							->where("TAX_M.BUY_SALE_DIV", $BUY_SALE_DIV)
							->where("TAX_D.SRL_NO", $SRL_NO)->get();
	}
	
	function filter_email_address($email) {  
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);  
	
		if(filter_var($email, FILTER_VALIDATE_EMAIL))   
			return true;
		else
			return false;
	}


	// 국세청 송신 부 : KTNET => 국세청
	// EDI 파일로 만든 후 송신 (FTP)
	public function eTaxSend(){
	
		// 넘어온값 Validation 체크 해야..
		$validator = Validator::make( Request::Input(), [

			'WRITE_DATE'		=> 'required|date_format:Y-m-d',
			'CUST_MK'			=> 'required|max:20,NOT_NULL',
			'BUY_SALE_DIV'		=> 'required|max:1',
			'SEQ'				=> 'required|numeric',
			'SRL_NO'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		/*
			송신체크사항
				- 송신식별자ID 를 등록확인
				- 공급자 Email 체크
				- 송신 상태값에 대한 체크
				- 공급받는자 Email 체크		
		*/

		$eTaxInfo	= $this->getTaxInfForSend(Request::Input("CUST_MK"), Request::Input("WRITE_DATE"),  Request::Input("SEQ"), Request::Input("BUY_SALE_DIV"), Request::Input("SRL_NO") );
		$errCheck = [[]];
		unset($errCheck);
		
		foreach($eTaxInfo as $drTax){
				
			// ETP Identity
			$sETBIdenty	= $drTax->TAX_EDI_ID;
			$sETBIPass	= $drTax->TAX_EDI_PASS;
			
			// 송신식별 ID / PW 체크
			if( !isset($sETBIdenty) || empty($sETBIdenty) ){
				if( !isset($sETBIPass) || empty($sETBIPass) ){
					$errCheck[]['TAX_EDI_ID'] = "송신식별자 계정을 확인해주세요";
				}
			}
			
			// 공급자 사업자번호 입력체크
			if( $drTax->CORP_ETPR_NO == null || strlen($drTax->CORP_ETPR_NO) != 10 ){
				$errCheck[]['CORP_ETPR_NO'] = "공급자 사업자등록번호를 확인해주세요(10자리)";
			}

			// 공급받는자 사업자번호 입력체크
			if( $drTax->ETPR_NO == null || strlen($drTax->ETPR_NO) != 10 ){
				$errCheck[]['CUST_ETPR_NO'] = "(공급받는자)".$drTax->FRNM."의 사업자등록번호를 확인해주세요(10자리)";
			}
			
			// 공급자 이메일 입력체크
			if( $drTax->CORP_RPST_EMAIL == null || !$this->filter_email_address($drTax->CORP_RPST_EMAIL) ){
				$errCheck[]['CORP_RPST_EMAIL'] = "(공급자)".$drTax->CORP_NM."의 이메일을 확인해주세요";
			}

			// 공급받는자 이메일 입력체크
			if( $drTax->EMAIL == null || !$this->filter_email_address($drTax->EMAIL) ){
				$errCheck[]['EMAIL'] = "(공급받는자)".$drTax->FRNM."의 이메일을 확인해주세요";
			}
		}
		
		if( !empty($errCheck) ){
			return response()->json(['result' => 'fail', 'type' => 'infonull', 'message' => $errCheck ], 422);
		}

		return $exception = DB::transaction(function(){

			try {

					$eTaxInfo	= $this->getTaxInfForSend(Request::Input("CUST_MK"), Request::Input("WRITE_DATE"),  Request::Input("SEQ"), Request::Input("BUY_SALE_DIV"), Request::Input("SRL_NO") );
					
					$result = File::makeDirectory(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK , 0777, true, true);
					
					if(!$result) {
						// path does not exist
						File::makeDirectory(public_path("ETB/".$eTaxInfo[0]->CORP_MK), 0777, true, true);
					}

					// 수신파일은 모두 삭제
					if(!File::exists(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/statistics_s.txt")) {
						// path does not exist
						File::delete(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/statistics_s.txt");
					}
					
					// 해당 업체의 수신폴더 생성
					if(!File::exists(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/RECV")) {
						// path does not exist
						File::makeDirectory(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/RECV", 0777, true, true);
					}

					// 해당 업체의 송신폴더 생성
					if(!File::exists(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/SEND")) {
						// path does not exist
						File::makeDirectory(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/SEND", 0777, true, true);
					}
					
					// 해당 업체의 수신폴더 날짜별로 생성
					if(!File::exists(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/RECV/".date('Ymd'))) {
						// path does not exist
						File::makeDirectory(public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/RECV/".date('Ymd'), 0777, true, true);
					}
					
					
					$sETBIdenty	= $eTaxInfo[0]->TAX_EDI_ID;			//'ECIS1';		//$eTaxInfo->TAX_EDI_ID;
					$sETBIPass	= $eTaxInfo[0]->TAX_EDI_PASS;		//'ECIS0001';	// $eTaxInfo->TAX_EDI_PASS;
					
					//내부 Seq번호
					$nSeqNo		= 0;

					//EDI파일 세그먼트 증감변수
					$nSegCnt	= 0;

					$nSeqNo++;
					$nUnz =  $nSeqNo;
					
					$y = date('Y');
					$m = date('m');
					
					$sText = "";
					$sFilename = public_path()."/ETB/".$eTaxInfo[0]->CORP_MK."/SEND/UPLOAD.XXX";
					
					// 1.문서 헤더 만들기 (꼭 필요한 이유?)
					//$sText.= $sText."UNB+KECA:4+".$sETBIdenty.":57+ETRADEBILL:57+".date('Ymd').":".date("Hi", time())."+". $nSeqNo."'";

					foreach($eTaxInfo as $drTax){
						
						$sText.= $sText."UNB+KECA:4+".$sETBIdenty.":57+ETRADEBILL:57+".date('Ymd').":".date("Hi", time())."+".$nSeqNo."'";

						// 1. UNH 헤더
						$nSegCnt = 0;
						$sText .= "UNH+"."1"."+TAXBIL:S:93A:KE'"; 
						$nSegCnt++;
						
						// 2. TAX:국세청용 세금계산서 / 신고구분 (CD:승인후 신고) / (발급번호)사업자관리번호 / 계산서 종류(계산서:03) / 계산서 분류(일반:01/영세율:02)
						$sText.= "BGM+TAX:".$drTax->TAX_REPORT_DIVISION."+BW.".$drTax->CORP_ETPR_NO.".".$drTax->TAX_ACCOUNT_NUMBER."+".$drTax->TAX_ACCOUNT_TYPE."+".$drTax->TAX_ACCOUNT_PART."'"; 
						$nSegCnt++;

						// 책번호-권/책번호-호 생략함
						// 3. 세금계산서 일련번호 (생략가능)
						$sText .= "RFF+FE:".$drTax->TAX_ACCOUNT_NUMBER."'";
						$nSegCnt++;
						
						// 4. 기승인번호(수정계산서/수정전자세금계서 모드일경우)
						if ($drTax->TAX_ACCOUNT_TYPE == "02" || $drTax->TAX_ACCOUNT_TYPE =="04")
						{
							$sText .= "RFF+ACE:".$drTax->TAX_ISSUE_NUMBER."'";
							$nSegCnt++;
						}
						
						// RECPT_CLAIM_DIV" :  영수/청구구분( 1 : 청구 : 0 : 영수)
						// 5. 영수(매입),청구(매출) 구분 (매출입구분)
						if ($drTax->RECPT_CLAIM_DIV == "1")
						{
							$sText .= "GIS+01'"; // 
						}else{
							$sText .= "GIS+02'"; 
						}
						$nSegCnt++;

						// 신규(05),정정(06),취소(07) , 수정세금계산서 취소 코드 기재
						// 6. 발급구분 :: 수정코드
						$sAmdReason = "";
						// 정정,취소,폐기일때만 수정코드 들어가도록 조건명시
						if( $drTax->TAX_ISSUE_DIVISION == "05" ){
							$sText .= "FTX+AAI+++".$drTax->TAX_ISSUE_DIVISION."::'"; 
						}else{
							$sText .= "FTX+AAI+++".$drTax->TAX_ISSUE_DIVISION."::".$drTax->TAX_MODIFY_CODE."'"; 
						}
						$nSegCnt++;
						
						// 7. 공급자정보
						$sText .= "NAD+SE+".$drTax->CORP_ETPR_NO.":167+".$drTax->CORP_NM.":".$drTax->CORP_RPST."::+".$drTax->CORP_RPST.":+" 
								  .$drTax->CORP_ADDR1.":'"; 
								   $nSegCnt++;

						// 8. 공급자 업태
						$sText .= "IMD+++SG:::".$drTax->CORP_UPTE."'"; $nSegCnt++;
						// 9. 공급자 업종
						$sText .= "IMD+++HN:::".$drTax->CORP_UPJONG."'"; $nSegCnt++;

						// 10. 공급받는자 
						$sBizNoFlag = "01";
						//외국인
						if ( substr($drTax->ETPR_NO,0, 1) == "F")
							$sBizNoFlag = "03";
						else
						{
							if ( strlen($drTax->ETPR_NO) == 10)
								$sBizNoFlag = "01";
							else
								$sBizNoFlag = "02";
						}
						$sText .= "NAD+BY+".$drTax->ETPR_NO.":".$sBizNoFlag."+".$drTax->FRNM."::::".$drTax->EMAIL."+".$drTax->RPST.":+".$drTax->ADDR1.":";
						$nSegCnt++;

						// 11. 공급받는자 업태
						$sText .= "IMD+++SG:::".$drTax->UPTE."'"; $nSegCnt++;
						// 12. 공급받는자 업종
						$sText .= "IMD+++HN:::".$drTax->UPJONG."'"; $nSegCnt++;

						// 13. 세금계산서 발행일자..
						$sText .= "DTM+202:".date("Ymd").":6TA'"; $nSegCnt++;
						
						// 14. 공란수
						$sText .= "SEQ++".(12 - strLen($drTax->SUPPLY_AMT))."'"; $nSegCnt++;
						//공급가액
						$sText .= "MOA+78:".$drTax->SUPPLY_AMT.":KRW'"; $nSegCnt++;
						//세액
						if( empty($drTax->TAX) || $drTax->TAX == "") { 
							$drTax->TAX = 0;
						}
						$sText .= "MOA+161:".$drTax->TAX.":KRW'"; $nSegCnt++;
						//비고
						
						if( strlen($drTax->REMARK) > 1) {
							$sText .= "FTX+AB+++".$drTax->REMARK."::'"; $nSegCnt++;
						}
						$i = 0;
						$TaxItems = $this->getTaxItem(Request::Input("CUST_MK"), Request::Input("WRITE_DATE"),  Request::Input("SEQ"), Request::Input("BUY_SALE_DIV"), Request::Input("SRL_NO") );
						foreach ($TaxItems as $drTaxItems)
						{
							if ($i > 4) break;       //최대 4란 까지 가능

							$sText .= "LIN+".++$i."'"; 
							$nSegCnt++;

							$sText .= "DTM+35:".$drTaxItems->WRITE_DATE.":6TA"."'"; 
							$nSegCnt++;

							$sText .= "IMD+++SG:::".$drTaxItems->GOODS."'"; 
							$nSegCnt++;
							// 비고.규격없음

							// 단위 없음
							if ($drTaxItems->QTY != ""){
								$sText .= "QTY+1:".(float)($drTaxItems->QTY)."'";
								$nSegCnt++;
							}
							// 단가
							if ((int)($drTaxItems->UNCS) != 0){
								$sText .= "PRI+CAL:".$drTaxItems->UNCS."'";
								$nSegCnt++;
							}
							// 공급가액
							if ( (int)($drTaxItems->SUPPLY_AMT) != 0)
							{
								$sText .= "MOA+203:".(int)($drTaxItems->SUPPLY_AMT).":KRW"."'";
								$nSegCnt++;
							}
							// 세액
							if ((int)($drTaxItems->TAX) != 0)
							{
								$sText .= "MOA+124:".(int)($drTaxItems->TAX).":KRW"."'";
								$nSegCnt++;
							}
						}
						$sText .= "RFF+AON:0"."'"; $nSegCnt++;
						$sText .= "PAI+::".$drTax->TAX_PAYWAY."'"; $nSegCnt++;
						$sText .= "MOA+2AE:".$drTax->SUPPLY_AMT.":KRW"."'"; $nSegCnt++; 
						$sText .= "UNT+".$nSegCnt."+1'";
						$nSeqNo++;
					}
						
					$sText .= "UNZ+1+".$nUnz."'";
					
					$sText = iconv("UTF-8", "EUC-KR", $sText);
					
					File::put($sFilename,$sText );	
					
					// 전송스크립트 작성
					$fShellSend = public_path()."/ETB/send.sh";
					
					// ftp 전송스크랩트 실행( 아이디 / 비밀번호)
					exec($fShellSend." ".$sETBIdenty." ".$sETBIPass." ".$drTax->CORP_MK, $output, $result);
					
					if($result > 0) 
					{
						die('error message here'); 
						return response()->json(['result' => 'fail', 'message' => '전송중 실패했습니다.'], 422);
					}else{
						
						//if( strpos($output[22], "MDEDI  SENT") !== false ) {
							
							DB::table("TAX_ACCOUNT_M")
								->where("CORP_MK", $this->getCorpId())
								->where("WRITE_DATE", Request::Input("WRITE_DATE"))
								->where("SEQ", Request::Input("SEQ"))
								->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
								->update([
										 "TAX_STATTUS"			=> "2"
										,"TAX_SEND_STATUS"		=> "2"
										,"TAX_SEND_DATE"		=> DB::raw("GETDATE()")
										, 
								]);
							return response()->json(['result' => 'success']);

						//}else{
							
							DB::table("TAX_ACCOUNT_M")
								->where("CORP_MK", $this->getCorpId())
								->where("WRITE_DATE", Request::Input("WRITE_DATE"))
								->where("SEQ", Request::Input("SEQ"))
								->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
								->update([
										 "TAX_STATTUS"			=> "3"
										,"TAX_SEND_STATUS"		=> "3"
										,"TAX_SEND_DATE"		=> DB::raw("GETDATE()")
								]);
							return response()->json(['result' => 'fail', 'message' => '전송중 실패했습니다!!.'], 422);
						//}
					}
		
				} catch(ValidationException $e){
					$errors = $e->errors();
					$errors =  json_decode($errors); 
					return response()->json(['result' => 'fail', 'message' => $errors], 422);
				} catch(Exception $e){
					$errors = $e->errors();
					$errors =  json_decode($errors); 
					return response()->json(['result' => 'fail', 'message' => $errors], 422);
				}
		});
	}

	// 국세청 전자세금계사서 수신
	public function eTaxReceive(){
		
		$corpId = $this->getCorpId();

		if(is_file(public_path("ETB/".$corpId.'/statistics_s.txt') ) ) {
			unlink(public_path("ETB/".$corpId.'/statistics_s.txt'));
		}
		
		if(!File::exists(public_path()."/ETB/".$corpId)) {
			// path does not exist
			File::makeDirectory(public_path("ETB/".$corpId), 0777, true, true);
		}

		if(!File::exists(public_path()."/ETB/".$corpId."/RECV")) {
			// path does not exist
			File::makeDirectory(public_path()."/ETB/".$corpId."/RECV", 0777, true, true);
		}

		if(!File::exists(public_path()."/ETB/".$corpId."/RECV/".date('Ymd'))) {
			// path does not exist
			File::makeDirectory(public_path()."/ETB/".$corpId."/RECV/".date('Ymd'), 0777, true, true);
		}

		// 수신스크립트 작성
		// 전송스크립트 작성
		$fShellReceive = public_path()."/ETB/receive.sh";
		
		$corpInfo = DB::table("CORP_INFO")
					->where("CORP_MK", $this->getCorpId())
					->first();
		
		$sETBIdenty	= $corpInfo->TAX_EDI_ID;
		$sETBIPass	= $corpInfo->TAX_EDI_PASS;
		// ftp 수신스크랩트 실행( 아이디 / 비밀번호)
		exec($fShellReceive." ".$sETBIdenty." ".$sETBIPass." ".$corpInfo->CORP_MK, $output, $result);
		
		// 에러 일경우
		if( $result > 0){
			return response()->json(['result' => 'fail', 'message' => '수신중 오류입니다'], 422);
		}

		$file_nm = public_path()."/ETB/".$corpId."/RECV/".date("Ymd")."/ktnet_data";

		// 우선 파일 복사함.
		copy($file_nm, $file_nm.date('Ymd').date("Hi", time()));

		$sTempDocGu = '';
		$sTemp		= '';
		
		if(File::exists($file_nm)) {

			$fp = fopen($file_nm, "r");
		
			$fr = fread($fp,filesize($file_nm));	
			$sText = iconv("EUC-KR", "UTF-8", $fr);
			fclose($fp);
			
			
			$sText = "UNB+KECA:4+ETRADEBILL:57+ECIS1:57+20170211:1055+8IYx4ZFO6'UNH+1+GENRES:2:911:KE'BGM+962+BW.6028117892.20170212+05'FTX+AAP+++F::CD:2014011541000012gsie4uau'DTM+137:20170303105524:204'NAD+MR+++ECIS1'NAD+MS+++ETRADEBILL'RFF+ACE:TAXBILTAX'UNT+8+1'UNZ+1+8IYx4ZFO6'UNB+KECA:4+ETRADEBILL:57+ECIS1:57+20170303:1055+8IYx4ZFO6'UNH+1+GENRES:2:911:KE'BGM+962+BW.6028117892.20170212+05'FTX+AAP+++A::DD'DTM+137:20170303105524:204'NAD+MR+++ECIS1'NAD+MS+++ETRADEBILL'RFF+ACE:TAXBILTAX'UNT+8+1'UNZ+1+8IYx4ZFO6'";
			
			$arr = explode("UNB", $sText);

			/*	
			  0 => "KECA:4+ETRADEBILL:57+ECIS1:57+20170303:1055+8IYx4ZFO6"
			  1 => "UNH+1+GENRES:2:911:KE"
			  2 => "BGM+962+BW.6028117892.20170212+05"
			  3 => "FTX+AAP+++A::DD"
			  4 => "DTM+137:20170303105524:204"
			  5 => "NAD+MR+++ECIS1"
			  6 => "NAD+MS+++ETRADEBILL"
			  7 => "RFF+ACE:TAXBILTAX"
			  8 => "UNT+8+1"
			  9 => "UNZ+1+8IYx4ZFO6"
			  10 => ""
			]
			*/

			// 첫번째 란은 무조건 0은 비어있음
			if( count($arr) > 1){

				for($i=1; $i<count($arr); $i++){

					$arrText = explode("'", $arr[$i]);
					
					if( substr($arrText[1], 0, 3) == "UNH" && substr($arrText[2], 0, 3) == "BGM" ){

						// 발급번호
						$balgub_no = substr($arrText[0], 31, 8);

						if( substr($arrText[3], 0, 3) == "FTX") {
							
							$sTempDocGu = substr($arrText[3], 10, 1);

							// 국세청 최종승인
							if( $sTempDocGu == "F"){
								// 국세청 승인번호
								$eTaxprov_no = substr($arrText[3], 16);
								

								$arrSttus = $this->getArrSndSeq($sTempDocGu);

								$nTmpRcvGu	= $arrSttus['nTmpRcvGu'];
								$sTmpDoc	= $arrSttus['sTmpDoc'];
								

								//dd($balgub_no, $eTaxprov_no, Request::Input("BUY_SALE_DIV"));
								// 발급번호로 업데이트
								DB::table("TAX_ACCOUNT_M")
									->where("CORP_MK", $this->getCorpId())
									->where("TAX_ACCOUNT_NUMBER", $balgub_no)
									->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
									->update([
											 "TAX_STATTUS"			=> "4"
											,"TAX_SEND_STATUS"		=> "4"
											,"TAX_RECEIVE_DATE"		=> DB::raw("GETDATE()")
											,"TAX_ISSUE_DATE"		=> DB::raw("GETDATE()")
											,"TAX_ACKNOWLEDGMENT_NUMBER" => $eTaxprov_no
											,"TAX_STTUS_MSG"		=> $sTmpDoc
											
									]);
								
								return response()->json(['result' => 'success', 'sTmpDoc'=>$sTmpDoc]);

							}else{
					
								$sTemp = substr($arrText[3], 12, 210);
								$stmpMsg	= "";
								$stmpMsg1	= "";
								$stmpMsg2	= "";
								$stmpMsg3	= "";

								//3 => "FTX+AAP+++A::DD"

								if( $sTemp != ":CD"){
									$nPos = strpos($arrText[3], ":");

									if( $nPos > 0) {
										$stmpMsg1	= substr($arrText[3], 0, $nPos-1);		// 오류메시지
										$sTemp		= substr($arrText[3], $nPos, 210);
										$nPos		= strpos(":", $arrText[3]);

										if( $nPos > 0){
											$stempMsg2 = substr($arrText[3], 0, $nPos-1);
											$stempMsg3 = substr($arrText[3], $nPos+1, 70);
										}else{
											$stempMsg2 = $arrText[3];
										}

									}
									$stmpMsg = $stmpMsg1.$stmpMsg2.$stmpMsg3;
								}

								$arrSttus = $this->getArrSndSeq($sTempDocGu);
								$nTmpRcvGu	= $arrSttus['nTmpRcvGu'];
								$sTmpDoc	= $arrSttus['sTmpDoc'];
								
								
								DB::table("TAX_ACCOUNT_M")
									->where("CORP_MK", $this->getCorpId())
									->where("TAX_ACCOUNT_NUMBER", $balgub_no)
									->where("BUY_SALE_DIV", Request::Input("BUY_SALE_DIV"))
									->update([
											 "TAX_STATTUS"			=> $nTmpRcvGu
											,"TAX_SEND_STATUS"		=> $nTmpRcvGu
											,"TAX_RECEIVE_DATE"		=> DB::raw("GETDATE()")
											,"TAX_ERR_MSG"			=> $stmpMsg
											,"TAX_STTUS_MSG"		=> $sTmpDoc
									]);
									return response()->json(['result' => 'success', 'sTmpDoc'=>$sTmpDoc]);
									//return response()->json(['result' => 'fail', 'message' => $stmpMsg], 422);
								
							}
						}
					}
				}
			}
		}else{
			return response()->json(['result' => 'fail', 'message' => '수신문서가 없습니다.'], 422);
		}
	}

	// 매입계산서_M 최대 SEQ 값 조회
	private function getTaxAccountMSeq($pCorpId, $pWRITE_DATE){
		
		return $max_seq = DB::table("TAX_ACCOUNT_M")
									->select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $pCorpId)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
				
	}

	// 매입계산서_D 최대 SRL_NO 값 조회
	private function getTaxAccountDSrlNo($pCorpId, $pWRITE_DATE, $pSEQ){
		
		return $max_seq = DB::table("TAX_ACCOUNT_D")
									->select( DB::raw('ISNULL(max("SRL_NO"), 0) AS SRL_NO') )
									->where ("CORP_MK", $pCorpId)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->where ("SEQ", $pSEQ)
									->first()->SRL_NO;
				
	}

	public function getTaxNo($YYYY, $MM){
		
		// 일련번호
		$M_SRL_NO = DB::table("DOC_NUMBER")
					->select("SRL_NO")
					->where("CORP_MK", $this->getCorpId())
					->where("YEAR", $YYYY )
					->where("MONTH", $MM );
		
		$SRL_NO = 0;
		// 일련번호가 없으면 생성해줌
		if( count($M_SRL_NO->get()) == 0 ){
			DB::table("DOC_NUMBER")->insert(
					[
						 "CORP_MK"	=> $this->getCorpId()
						,"YEAR"		=> $YYYY
						,"MONTH"	=> $MM
						,"SRL_NO"	=> 1
					]
			);

			$SRL_NO = $YYYY.$MM."01";
			return $SRL_NO;

		}else{
			// 일련번호 최대값 가져오고 update(srl_no +1) 
			$M_SRL_NO = DB::table("DOC_NUMBER")
				->select(DB::raw("ISNULL(MAX(SRL_NO)+1, 1) AS NEW_SRL_NO"))
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR",  $YYYY)
				->where("MONTH", $MM )->first();
			
			$M_SRL_NO->NEW_SRL_NO = $M_SRL_NO->NEW_SRL_NO >= 10 ? $M_SRL_NO->NEW_SRL_NO : "0".$M_SRL_NO->NEW_SRL_NO;

			$SRL_NO = $YYYY.$MM.$M_SRL_NO->NEW_SRL_NO;
			
			DB::table("DOC_NUMBER")
				->where("CORP_MK", $this->getCorpId())
				->where("YEAR", $YYYY )
				->where("MONTH", $MM)
				->update(["SRL_NO"	=> DB::raw("SRL_NO + 1")]);
			
			return $SRL_NO;
		}
	
	}

	private function getArrSndSeq($sTmpDocGu){

		if ($sTmpDocGu == 'E'){
			$nTmpRcvGu = 1;
			$sTmpDoc = 'KTNET 오류';
		} else if ( $sTmpDocGu == 'A' ) { 
			$nTmpRcvGu = 2;
			$sTmpDoc = 'KTNET 접수';
		} else if ( $sTmpDocGu == 'D' ) { 
			$nTmpRcvGu = 3;
			$sTmpDoc = '거부(화주)';
		} else if ( $sTmpDocGu == 'L' ) { 
			$nTmpRcvGu = 4;
			$sTmpDoc = '승인(화주)';
		} else if ( $sTmpDocGu == 'N' ) { 
			$nTmpRcvGu = 5;
			$sTmpDoc = '이메일전송 실패';
		} else if ( $sTmpDocGu == 'T' ) { 
			$nTmpRcvGu = 6;
			$sTmpDoc = '국세청 1차접수';
		} else if ( $sTmpDocGu == 'X' ) { 
			$nTmpRcvGu = 7;
			$sTmpDoc = '국세청 오류';
		} else if ( $sTmpDocGu == 'F' ) { 
			$nTmpRcvGu = 8;
			$sTmpDoc = '국세청 최종접수';
		}
		
		return ['nTmpRcvGu' => $nTmpRcvGu, 'sTmpDoc' => $sTmpDoc ];
	}
		
}
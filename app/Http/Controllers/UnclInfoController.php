<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use App\Http\Requests;

use App\UNCL_INFO;
use App\CUST_CD;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use PHPExcel_Style_Border;

class UnclInfoController extends Controller
{
    //
	public function index($uncl)
	{
		return view("uncl.list", [ "uncl" => $uncl] );
	}

	public function detail($uncl)
	{

		$CUST_MK = urldecode(Request::segment(4));

		$CUST = DB::table("CUST_CD")
				->select( "FRNM"
						, "CUST_MK"
						, DB::raw(" (SUBSTRING(ETPR_NO, 1, 3) + '-' + SUBSTRING(ETPR_NO, 4, 2) + '-' + SUBSTRING(ETPR_NO, 6, 5)) AS ETPR_NO")
						, DB::raw("CASE WHEN LEN(RPST) >= 1 AND LEN(RPST) <= 5 THEN RPST
								        WHEN RPST IS NULL OR LEN(RPST)=0 THEN 'unknown'
								        WHEN LEN(RPST) > 6  THEN substring(RPST,1,5) END AS RPST")
				)->where("CORP_MK", $this->getCorpId())
				->where("CUST_MK", $CUST_MK)->first();

		return view("uncl.detail", [ "uncl" => $uncl, "CUST" => $CUST] );
	}

	public function detail2($uncl)
	{
		
		$MODE	=  isset($_GET['MODE']) ? $_GET['MODE'] : null ;
		
		//dd($MODE);
		$CUST_MK = urldecode( Request::segment(4) );
		$CUST = DB::table("CUST_CD AS C")
				->leftjoin("CUST_GRP_INFO AS G", function($join){
			
								$join->on("G.CORP_MK", "=", "C.CORP_MK");
								$join->on("G.CUST_GRP_CD", "=", "C.CUST_GRP_CD");
							})
				->select( "C.FRNM"
						, "C.CUST_MK"
						, "G.CUST_GRP_NM"
						, DB::raw(" (SUBSTRING(C.ETPR_NO, 1, 3) + '-' + SUBSTRING(C.ETPR_NO, 4, 2) + '-' + SUBSTRING(C.ETPR_NO, 6, 5)) AS ETPR_NO")
						, DB::raw("CASE WHEN LEN(C.RPST) >= 1 AND LEN(C.RPST) <= 5 THEN RPST
								        WHEN C.RPST IS NULL OR LEN(C.RPST)=0 THEN 'unknown'
								        WHEN LEN(C.RPST) > 6  THEN substring(C.RPST,1,5) END AS RPST")
				)->where("C.CORP_MK", $this->getCorpId())
				->where("C.CUST_MK", $CUST_MK)->first();

		$MAX_WRITE_DATE	= DB::table("UNCL_INFO")
						->select(DB::raw('CONVERT(CHAR(10), ISNULL(MAX(WRITE_DATE), GETDATE()), 23) AS MAX_WRITE_DATE'))
						->where("CORP_MK", $this->getCorpId())
						->where("UNCL_CUST_MK", $CUST_MK)
						->first();
		

		$test = DB::table("UNCL_INFO")->where("UNCL_CUST_MK", "B505")->get();
		//dd($test);
		return view("uncl.detail2", [ "uncl" => $uncl, "CUST" => $CUST , 'MAX_T' => $MAX_WRITE_DATE->MAX_WRITE_DATE, 'MODE' => $MODE] );
	}

	// 미수금 목록 조회
	public function listData()
	{

		$UNCL_INFO = DB::table("UNCL_INFO AS UNCL")
							->select( "CU_GRP.CUST_GRP_CD"
									, "UNCL.UNCL_CUST_MK"
									, "CU_GRP.CUST_GRP_NM"
									, "UNCL.CORP_MK"
									, DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END ) AS TOTAL_UNCL_AMT")
									, "CUST.FRNM"
									, "CUST.ETPR_NO"
									, "CUST.UNCL_MEMO"
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("UNCL.UNCL_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNCL.CORP_MK", "=", "CUST.CORP_MK");
							})->leftjoin("CUST_GRP_INFO AS CU_GRP", function($join){
									$join->on("CUST.CORP_MK" ,"=", "CU_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD" ,"=", "CU_GRP.CUST_GRP_CD");
							})->where("UNCL.CORP_MK", $this->getCorpId())
							  ->where("CUST.INTERFACE_YN", 'Y')
							  ->where("CUST.CORP_DIV", 'S')
							  ->where(function($query){

									// 기간일때 선택
									if( Request::Input("chkZero") == "2" ){
										if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
											if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
												$query->whereBetween("UNCL.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
											}
										}
									}
							})
							  ->groupby("CU_GRP.CUST_GRP_CD","UNCL.CORP_MK" ,"UNCL.UNCL_CUST_MK" ,"CUST.FRNM", "CUST.ETPR_NO" ,"CU_GRP.CUST_GRP_NM", "CUST.UNCL_MEMO");
	
							// 미수합계가 0이 아닌 업체 조회
							// having 절에 if function이 지원되지 않아서 아래처럼 구현함..
							if( Request::Input("chkZero") == "0"){
								$UNCL_INFO->havingRaw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
						WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END ) <> 0 ");
							}

		return Datatables::of($UNCL_INFO)
			->filter(function($query) {
				if( Request::Has('srtCondition') || Request::Has('textSearch')){
					if( Request::Has('srtCondition') && Request::Input('srtCondition') != "ALL"){
						$query->where("CU_GRP.CUST_GRP_CD", Request::Input('srtCondition'));

						if( Request::Has('textSearch') ){
							$query->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%" );
						}
					} else{
						$query->where(function($q){
								$q->where("CU_GRP.CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
						});
					
					}
				}

			})->make(true);
	}

	// 미수금정보 상세조회
	public function detailListData()
	{
		/*
		$cust_mk = Request::Input("cust_mk");
		$UNCL_INFO = DB::select("EXEC SP_UNCL_INFO '".$this->getCorpId()."','".$cust_mk."', '".Request::Input('start_date')."', '".Request::Input('end_date')."'");
		$totaldata = count($UNCL_INFO);
				
		$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => intval( $totaldata ),
                "recordsFiltered" => intval( $_GET['length'] ),
                "data"            => $UNCL_INFO
            );
		echo json_encode($json_data);
		*/
		$UNCL_INFO = DB::table("UNCL_INFO AS U")
							->select(  "U.CORP_MK"
									 , DB::raw("CONVERT(CHAR(10), U.WRITE_DATE, 23) AS WRITE_DATE")
									 , "U.UNCL_CUST_MK"
									 , "U.PROV_DIV"
									 , "U.UNCL_AMT"
									 , "U.AMT"
									 , "U.REMARK"
									 , "U.SALE_SEQ"
									 , "saleT.TOTAL_SALE_AMT"
									 , "saleT.FRNM"
									 , "saleT.SALE_DISCOUNT"
									 , "saleT.CASH_AMT"
									 , "saleT.PIS"
									 , "U.SEQ"
									 , DB::raw("CASE WHEN PROV_DIV = 1 THEN U.AMT ELSE 0 END AS ACCRUED_AMT")
									 , DB::raw("CASE WHEN PROV_DIV = 0 THEN U.AMT ELSE 0 END AS COLLECTION_AMT")
									 , DB::raw(" STUFF ( (SELECT
														     CONVERT(CHAR(10), LINE_D.WRITE_DATE, 23) AS WRITE_DATE
														   , LINE_D.CUST_MK
														   , LINE_D.SEQ
														   , CONVERT(INT, LINE_D.QTY) QTY
														   , LINE_D.PIS_MK
														   , LINE_D.SIZES
														   , LINE_D.ORIGIN_NM
														   , CONVERT(INT, LINE_D.UNCS) UNCS
														   , CONVERT(INT, LINE_D.AMT ) AMT
														   , LINE_D.REMARK
														   , P1.PIS_NM
														FROM SALE_INFO_D AS LINE_D
														     INNER JOIN PIS_INFO AS P1
														           ON LINE_D.PIS_MK = P1.PIS_MK
																  AND LINE_D.CORP_MK = P1.CORP_MK
														where 1 = 1
														  AND LINE_D.CORP_MK = '".$this->getCorpId()."'
														  AND LINE_D.WRITE_DATE = U.WRITE_DATE
														  AND LINE_D.CUST_MK =  '".Request::Input("cust_mk")."'
														  AND LINE_D.SEQ = saleT.SEQ
									 FOR XML RAW('SALE'), ROOT ('SALE_INFO_D'), ELEMENTS), 1, 1, '<') AS DD")
							
							)->leftjoin(DB::raw("
										 (SELECT SM.CORP_MK
										      , SM.WRITE_DATE
										      , SM.CUST_MK
										      , SM.SEQ
										      , SM.CASH_AMT
										      , SM.CHECK_AMT
										      , SM.UNCL_AMT
										      , SM.CARD_AMT
										      , SM.TOTAL_SALE_AMT
										      , C.FRNM
										      , SM.SALE_DISCOUNT
										      , CASE SD.CNT WHEN 1 THEN P.PIS_NM ELSE P.PIS_NM + '외' + CONVERT (VARCHAR , (SD.CNT - 1)) + '종' END AS PIS
										      , SD.SUMQTY
										   FROM SALE_INFO_M AS SM
										        INNER JOIN (SELECT CORP_MK
										                         , WRITE_DATE
										                         , CUST_MK, SEQ
										                         , COUNT(*) AS CNT
										                         , SUM(QTY) AS SUMQTY
										                         , MAX(PIS_MK) AS PIS_MK
										                      FROM SALE_INFO_D AS D
										                      GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ
										                    ) AS SD
										                ON SM.CORP_MK = SD.CORP_MK
										               AND SM.WRITE_DATE = SD.WRITE_DATE
										               AND SM.CUST_MK = SD.CUST_MK
										               AND SM.SEQ = SD.SEQ
										        INNER JOIN CUST_CD AS C
										                ON SM.CUST_MK = C.CUST_MK
										               AND SM.CORP_MK = C.CORP_MK
										        INNER JOIN PIS_INFO AS P
										                ON SD.PIS_MK = P.PIS_MK
										               AND SD.CORP_MK = P.CORP_MK
										 ) AS saleT"), function($join){
									$join->on("U.CORP_MK", "=", "saleT.CORP_MK");
									$join->on("U.WRITE_DATE", "=", "saleT.WRITE_DATE");
									$join->on("U.UNCL_CUST_MK", "=", "saleT.CUST_MK");
									$join->on("U.SALE_SEQ", "=", "saleT.SEQ");
							})->where("U.CORP_MK", $this->getCorpId())
							  ->where("U.UNCL_CUST_MK", urldecode(Request::Input("cust_mk")));
		
		return Datatables::of($UNCL_INFO)
				->filter(function($query) {
					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("U.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}

					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SALE_UNCS"){
							$query->where("U.REMARK",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("U.REMARK",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
		})->make(true);
	
	}

	public function detailListData2()
	{
		
		$cust_mk = urldecode(Request::Input("cust_mk"));
		$UNCL_INFO = DB::select("EXEC SP_UNCL_INFO '".$this->getCorpId()."','".$cust_mk."', '".Request::Input('start_date')."', '".Request::Input('end_date')."', 'W'");
		$totaldata = count($UNCL_INFO);
				
		$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => intval( $totaldata ),
                "recordsFiltered" => intval( $_GET['length'] ),
                "data"            => $UNCL_INFO
            );
		echo json_encode($json_data);
		
	}

	public function setUnclData(){

		$validator = Validator::make( Request::Input(), [
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'UNCL_CUST_MK'	=> 'required|max:20,NOT_NULL',
			'AMT'			=> 'required|numeric',
			'MEMO'			=> 'max:100',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$AMT		= Request::Input("AMT");
		$REMARK_DIV	= Request::Input("REMARK_DIV");

		DB::beginTransaction();
		$seq = $this->getMaxSeqInUNCLINFO(Request::input('WRITE_DATE')) + 1 ;

		$data = [
					  'CORP_MK'			=> $this->getCorpId()
					, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
					, 'SEQ'				=> $seq
					, 'UNCL_CUST_MK'	=> urldecode(Request::input('UNCL_CUST_MK'))
					, 'PROV_DIV'		=> Request::input('PROV_DIV')
					, 'AMT'				=> $AMT
					, 'REMARK'			=> Request::Input('REMARK')
					, 'MEMO'			=> Request::Input('MEMO')
				];

		try {
			if( Request::Has("UNCL_SALE_SEQ") && Request::Input("UNCL_SALE_SEQ") != ""){
				$data["SALE_SEQ"] = Request::Input("UNCL_SALE_SEQ");
				
			}

			if( Request::Has("SALE_WRITE_DATE") && Request::Input("SALE_WRITE_DATE") != ""){
				$data["SALE_WRITE_DATE"] = Request::Input("SALE_WRITE_DATE");
			}
			// 미수금
			DB::table("UNCL_INFO")->insert( $data );

		}catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	public function _delete(){

		DB::beginTransaction();

		try{
			UNCL_INFO::where( 'CORP_MK'	,	$this->getCorpId() )
					->where( 'SEQ' , Request::Input("SEQ") )
					->where( 'WRITE_DATE', Request::Input("WRITE_DATE") )
					->delete();

		}catch( Exception $e){
			DB::rollback();
			if ($e->fails()) {
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
		}

		DB::commit();

		return response()->json(['result' => 'success']);
	}

	// 수금용 출력
	public function pdfEmpty(){
		

		$UNCL_CUST = DB::table("UNCL_INFO AS UNCL")
							->select( "CU_GRP.CUST_GRP_CD"
									, "UNCL.UNCL_CUST_MK"
									, "CU_GRP.CUST_GRP_NM"
									, "UNCL.CORP_MK"
									, DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END ) AS TOTAL_UNCL_AMT")
									, "CUST.FRNM"

									, DB::raw(" CASE WHEN LEN(CUST.ETPR_NO) = 10 THEN SUBSTRING(CUST.ETPR_NO, 1, 3) + '-' + SUBSTRING(CUST.ETPR_NO, 4, 2) + '-' + SUBSTRING(CUST.ETPR_NO, 6, 5) ELSE CUST.ETPR_NO END AS ETPR_NO")
							)->leftjoin("CUST_CD AS CUST", function($join){
									$join->on("UNCL.UNCL_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNCL.CORP_MK", "=", "CUST.CORP_MK");

							})->leftjoin("CUST_GRP_INFO AS CU_GRP", function($join){
									$join->on("CUST.CORP_MK" ,"=", "CU_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD" ,"=", "CU_GRP.CUST_GRP_CD");
							})
							->where("UNCL.CORP_MK", $this->getCorpId())
							->where("CUST.INTERFACE_YN", 'Y')
							->where("CUST.CORP_DIV", 'S')
							->whereNotNull('CU_GRP.CUST_GRP_CD')
							->where( function($query) {
								

								$CUST_GRP_CD	= urldecode(Request::segment(4));
								$searchText		= urldecode(Request::segment(5));

								if( $CUST_GRP_CD != "ALL" ){
									$query->where("CUST.CUST_GRP_CD", $CUST_GRP_CD);
								}

								if( Request::Input("textSearch") != "" ){
									$query->where("CUST.FRNM", "LIKE", "%".Request::Input("textSearch")."%");
								}
								
							})
							->groupby("CU_GRP.CUST_GRP_CD","UNCL.CORP_MK" ,"UNCL.UNCL_CUST_MK" ,"CUST.FRNM" ,"CU_GRP.CUST_GRP_NM", "CUST.ETPR_NO")
							->having(DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END )") , "<>", 0)
							->orderBy("CUST.FRNM",  "ASC")
							->get();


		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";

		$sum = 0;
		foreach( $UNCL_CUST as $uncl){
			$sum += $uncl->TOTAL_UNCL_AMT;
		}

		$pdf = PDF::loadView("uncl.pdfListEmpty",
								[
									'list'	=> $UNCL_CUST,
									'today'	=> $today,
									'sum'	=> $sum,
								]
							);

		return $pdf->stream('미수금 수금용.pdf');

	}

	// 미수금 상세출력
	public function pdfDetail(){

		$UNCL_M = DB::table("UNCL_INFO AS UNCL")
							->select( "CU_GRP.CUST_GRP_CD"
									, "UNCL.UNCL_CUST_MK"
									, "CU_GRP.CUST_GRP_NM"
									, "UNCL.CORP_MK"
									, DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END ) AS TOTAL_UNCL_AMT")
									, "CUST.FRNM"
									, "CUST.PHONE_NO"
							)->join("CUST_CD AS CUST", function($join){
									$join->on("UNCL.UNCL_CUST_MK", "=", "CUST.CUST_MK");
									$join->on("UNCL.CORP_MK", "=", "CUST.CORP_MK");

							})->leftjoin("CUST_GRP_INFO AS CU_GRP", function($join){
									$join->on("CUST.CORP_MK" ,"=", "CU_GRP.CORP_MK");
									$join->on("CUST.CUST_GRP_CD" ,"=", "CU_GRP.CUST_GRP_CD");
							})->where("UNCL.CORP_MK", $this->getCorpId())
							->where("UNCL.AMT", "<>", 0)
							->where( function($query) {
								
								$CUST_GRP_CD	= urldecode(Request::segment(4));
								$searchText		= urldecode(Request::segment(5));

								if( $CUST_GRP_CD != "ALL" ){
									$query->where("CUST.CUST_GRP_CD", $CUST_GRP_CD);
								}

								if( $searchText != "" ){
									$query->where("CUST.FRNM", "LIKE", "%".$searchText."%");
								}
							})
							->groupby("CU_GRP.CUST_GRP_CD","UNCL.CORP_MK" ,"UNCL.UNCL_CUST_MK" ,"CUST.FRNM" ,"CU_GRP.CUST_GRP_NM", "CUST.PHONE_NO")
							->having(DB::raw("SUM(CASE WHEN UNCL.PROV_DIV = '1' THEN UNCL.AMT * (-1)
									                    WHEN UNCL.PROV_DIV = '0' THEN UNCL.AMT END )") , "<>", 0)
							->orderBy("CUST.FRNM",  "ASC")
							->get();
		
		ini_set("memory_limit", "999M");
		ini_set("max_execution_time", "999");

		$UNCL_D = DB::table("SALE_INFO_D AS LINE_D")
						->join("PIS_INFO AS P1", function($join){
							$join->on("LINE_D.PIS_MK", "=",  "P1.PIS_MK");
							$join->on("LINE_D.CORP_MK", "=",  "P1.CORP_MK");
						})->join("UNCL_INFO AS U", function($join){
							$join->on("LINE_D.CORP_MK", "=", "U.CORP_MK");
							$join->on("LINE_D.CUST_MK", "=", "U.UNCL_CUST_MK");
						})->select(
									 DB::raw("CONVERT(CHAR(10), LINE_D.WRITE_DATE, 23) AS WRITE_DATE")
								   , "LINE_D.CUST_MK"
								   , "LINE_D.SEQ"
								   , DB::raw("CONVERT(INT, LINE_D.QTY) QTY")
								   , "LINE_D.PIS_MK"
								   , "LINE_D.SIZES"
								   , "LINE_D.ORIGIN_NM"
								   , DB::raw("CONVERT(INT, LINE_D.UNCS) UNCS")
								   , DB::raw("CONVERT(INT, LINE_D.AMT ) AMT")
								   , "LINE_D.REMARK"
								   , "P1.PIS_NM"
									)
						->where("LINE_D.CORP_MK", $this->getCorpId())
						->where("U.AMT", "<>", 0)
						->get();
		
		dd($UNCL_D);
		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";
		
		

		//dd($UNCL_D);
		$pdf = PDF::loadView("uncl.pdfListDetail",
								[
									'list'		=> $UNCL_M,
									'detail'	=> $UNCL_D,
									'today'		=> $today,
								]
							);

		return $pdf->stream('미수금전체보기.pdf');

	}

	// 미수장부
	public function pdfDetailCust(){

		$cust_mk = urldecode(Request::segment(4));
		

		$UNCL_INFO = DB::select("EXEC SP_UNCL_INFO '".$this->getCorpId()."','".$cust_mk."', '".Request::Input('start_date')."', '".Request::Input('end_date')."', 'W'");
		
		$UNCL_SUM = DB::table( DB::raw("
										( SELECT  UNCL_CUST_MK
											  , CORP_MK
											  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL
											  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
										   FROM UNCL_INFO
										GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
										) AS A
									")
								)
								->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.UNCL_CUST_MK', $cust_mk)
								->groupBy("A.UNCL_CUST_MK","A.CORP_MK")->first();
		
		//dd($UNCL_D);
		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";

		//dd($UNCL_INFO);
		if( $UNCL_INFO === null || empty($UNCL_INFO) || count($UNCL_INFO) == 0 ){
			return Redirect::back()->with('alert-success', '해당기간엔 미수장부 데이터가 존재 하지 않습니다.')->send();
		}
		
		//ini_set("memory_limit", "1000M");

		//dd($UNCL_D);
		ini_set("memory_limit", "999M");
		ini_set("max_execution_time", "999");

		$CORP_INFO	= $this->getCorpInfo();
		$CUST_INFO	= DB::table("CUST_CD AS C")
							->leftjoin("CUST_GRP_INFO AS G", function($join){
			
								$join->on("G.CORP_MK", "=", "C.CORP_MK");
								$join->on("G.CUST_GRP_CD", "=", "C.CUST_GRP_CD");
							})
							->where("C.CORP_MK", $this->getcorpId())
							->where("C.CUST_MK", $cust_mk)
							
							->select(DB::raw(" (SUBSTRING(C.ETPR_NO, 1, 3) + '-' + SUBSTRING(C.ETPR_NO, 4, 2) + '-' + SUBSTRING(C.ETPR_NO, 6, 5)) AS ETPR_NO")
							, "G.CUST_GRP_NM")
							->first();
		//DD($CORP_INFO);
		$pdf = PDF::loadView("uncl.pdfDetailCust",
								[
									'list'		=> $UNCL_INFO,
									//'detail'	=> $UNCL_D,
									'today'		=> $today,
									'sumUncl'	=> $UNCL_SUM,
									'corp'		=> $CORP_INFO,
									'cust'		=> $CUST_INFO,
								]
							);
		//dd($pdf);
		return $pdf->stream('미수장부.pdf');

	}

	// 미수장부 출력 테스트
	public function pdfDetailCustTest(){

		$cust_mk = Request::segment(4);

		$UNCL_INFO = DB::select("EXEC SP_UNCL_INFO '".$this->getCorpId()."','".$cust_mk."', '".Request::Input('start_date')."', '".Request::Input('end_date')."', 'W'");
		
		//dd($UNCL_INFO);
		
		$UNCL_SUM = DB::table( DB::raw("
										( SELECT  UNCL_CUST_MK
											  , CORP_MK
											  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL
											  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
										   FROM UNCL_INFO
										GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
										) AS A
									")
								)
								->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.UNCL_CUST_MK', $cust_mk)
								->groupBy("A.UNCL_CUST_MK","A.CORP_MK")->first();
		
		//dd($UNCL_D);
		$today = date("Y-m-d");
		$yoil = array("일","월","화","수","목","금","토");
		$today = $today."  ".$yoil[date('w', strtotime($today))]. "요일";

		//dd($UNCL_INFO);
		if( $UNCL_INFO === null || empty($UNCL_INFO) || count($UNCL_INFO) == 0 ){
			return Redirect::back()->with('alert-success', '해당기간엔 미수장부 데이터가 존재 하지 않습니다.')->send();
		}
		
		//ini_set("memory_limit", "1000M");

		//dd($UNCL_D);
		$CORP_INFO	= $this->getCorpInfo();
		$view = view('uncl.pdfTest', [
									'list'		=> $UNCL_INFO,
									//'detail'	=> $UNCL_D,
									'today'		=> $today,
									'sumUncl'	=> $UNCL_SUM,
									'corp'		=> $CORP_INFO,
								]);
		//$contents = (string) $view;
		// or
		$contents = $view->render();

		$pdf = PDF::loadHTML($contents);

		ini_set("memory_limit", "999M");
		ini_set("max_execution_time", "999");

		return $pdf->stream('미수장부.pdf');
		
	
	}

	// 해당거래처의 총미수금 불러오기
	public function getTotalUncl(){

		$UNCL_INFO = DB::table( DB::raw("
										( SELECT  UNCL_CUST_MK
											  , CORP_MK
											  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL
											  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
										   FROM UNCL_INFO
									    GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
										) AS A
									")
								)
								->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.UNCL_CUST_MK', Request::Input("UNCL_CUST_MK"))
								->groupBy("A.UNCL_CUST_MK","A.CORP_MK")->first();

		return response()->json($UNCL_INFO);
	}


	public function getCustGrp(){
		$UNCL_INFO = UNCL_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($UNCL_INFO);
	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNCLINFO($pWRITE_DATE){

		return $max_seq = UNCL_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;

	}
}


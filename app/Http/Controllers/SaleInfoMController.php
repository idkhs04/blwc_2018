<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\CUST_GRP_INFO;
use App\STOCK_SUJO;
use App\PIS_INFO;
use App\CUST_CD;
use App\SALE_INFO_M;
use App\SALE_INFO_D;
use App\AREA_CD;
use App\UNCL_INFO;
use App\STOCK_INFO;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;
use Illuminate\Database\Migrations\Migration;

class SaleInfoMController extends Controller
{
	// view : 판매내역  목록조회
	public function index($sale){	
		$CUST_GRP_INFO = CUST_GRP_INFO::where("CORP_MK", $this->getCorpId())->get();
		return view("sale.list", [ "sale" => $sale, 'custGrp' => $CUST_GRP_INFO] );
	}

	// view : 판매내역 상세조회
	public function detailList($sale){
		return view("sale.detailList", [ "sale" => $sale] );
	}
	
	// view : 판매 수정 상세보기 Data
	public function detailForm($sale){	
		$cust_mk	= urldecode(Request::segment(4));
		$seq		= urldecode(Request::segment(5));
		$wrtie_dt	= urldecode(Request::segment(6));

		$SALE_INFO_D = DB::table("SALE_INFO_D AS D")
						->select(  "D.CORP_MK"
								 , "P.PIS_MK"
								 , "D.SIZES"
								 , DB::raw("CONVERT(CHAR(10), D.INPUT_DATE, 23) AS INPUT_DATE")
								 , "D.SRL_NO"
								 , DB::raw("ISNULL(ROUND(D.QTY, 2), 0) AS QTY")
								 , DB::raw("ISNULL(ROUND(D.UNCS, 2), 0) AS SALE_UNCS")
								 , "D.ORIGIN_CD"
								 , "D.ORIGIN_NM"
								 , "P.PIS_NM"
								 , "D.REMARK"
								 , DB::raw("ISNULL(D.AMT, 0) AS AMT")
								 , "D.WRITE_DATE"
								 , DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								 )
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
						})->where("D.CORP_MK" , $this->getCorpId())
						->where("D.CUST_MK", $cust_mk)
						->where("D.WRITE_DATE", $wrtie_dt)
						->where("D.SEQ", $seq)->get();
		$where = "";
		foreach ($SALE_INFO_D as $info){
			$where.= " AND NOT ( S.PIS_MK='".$info->PIS_MK."' AND SIZES='".$info->SIZES."' AND INPUT_DATE='".$info->INPUT_DATE."' AND ORIGIN_CD='".$info->ORIGIN_CD."' AND ORIGIN_NM='".$info->ORIGIN_NM."') ";
		}
		
		$sql = " SELECT S.CORP_MK
				      , S.SUJO_NO
				      , S.PIS_MK
				      , S.SIZES
				      , LEFT(CONVERT(VARCHAR, S.INPUT_DATE, 120),10) AS INPUT_DATE
				      , ISNULL(S.SALE_UNCS, 0) AS SALE_UNCS
				      , S.ORIGIN_CD
				      , S.ORIGIN_NM
				      , P.PIS_NM
					  , '' AS REMARK
					  , '0' AS AMT
					  , '0' AS QTY
					  , 'false' AS ISMOD
				  FROM STOCK_SUJO AS S
				       INNER JOIN PIS_INFO AS P
				               ON S.PIS_MK=P.PIS_MK
				              AND S.CORP_MK=P.CORP_MK
				 WHERE S.CORP_MK = '".$this->getCorpId()."' ".$where;
				
		$model = DB::select($sql);
		
		foreach ($SALE_INFO_D as $info){

			$myobj = (object) [];
			$myobj->CORP_MK		= $this->getCorpId();
			$myobj->PIS_MK		= $info->PIS_MK;
			$myobj->SIZES		= $info->SIZES;
			$myobj->INPUT_DATE	= substr($info->INPUT_DATE, 0, 10);
			$myobj->QTY			= (float)$info->QTY;
			$myobj->SALE_UNCS	= (float)$info->SALE_UNCS;
			$myobj->ORIGIN_CD	= $info->ORIGIN_CD;
			$myobj->ORIGIN_NM	= $info->ORIGIN_NM;
			$myobj->PIS_NM		= $info->PIS_NM;
			$myobj->REMARK		= $info->REMARK;
			$myobj->AMT			= $info->AMT;
			$myobj->WRITE_DATE	= substr($info->WRITE_DATE, 0, 10);
			$myobj->SUJO_NO		= '';
			$myobj->ISMOD		= true;
			$myobj->SRL_NO		= $info->SRL_NO;
			array_push($model, $myobj);
		}

		$model = array_reverse($model);
		return view("sale.detailForm", [ "sale" => $sale, 'model' => $model ] );
	}

	// view : 반품 내역 상세 조회
	public function detailReturn($sale){	
		$cust_mk	= urldecode(Request::segment(4));
		$seq		= Request::segment(5);
		$wrtie_dt	= Request::segment(6);

		$SALE_INFO_D = DB::table("SALE_INFO_D AS D")
						->select(  "D.CORP_MK"
								 , "P.PIS_MK"
								 , "D.SIZES"
								 , DB::raw("CONVERT(CHAR(10), D.INPUT_DATE, 23) AS INPUT_DATE")
								 , "D.SRL_NO"
								 , "D.QTY"
								 , "D.UNCS AS SALE_UNCS"
								 , "D.ORIGIN_CD"
								 , "D.ORIGIN_NM"
								 , "P.PIS_NM"
								 , "D.REMARK"
								 , "D.AMT"
								 , DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								 )
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
							})->where("D.CORP_MK" , $this->getCorpId())
						->where("D.CUST_MK", $cust_mk)
						->where("D.WRITE_DATE", $wrtie_dt)
						->where("D.SEQ", $seq)->get();
		$where = "";

		foreach ($SALE_INFO_D as $info){
			$where.= " AND NOT ( S.PIS_MK='".$info->PIS_MK."' AND SIZES='".$info->SIZES."' AND INPUT_DATE='".$info->INPUT_DATE."' AND ORIGIN_CD='".$info->ORIGIN_CD."' AND ORIGIN_NM='".$info->ORIGIN_NM."') ";
		}
		
		$sql = " SELECT S.CORP_MK
				      , S.SUJO_NO
				      , S.PIS_MK
				      , S.SIZES
				      , LEFT(CONVERT(VARCHAR, S.INPUT_DATE, 120),10) AS INPUT_DATE
				      , S.QTY
				      , S.SALE_UNCS
				      , S.ORIGIN_CD
				      , S.ORIGIN_NM
				      , P.PIS_NM
					  , '' AS REMARK
					  , '' AS AMT
					  , 'false' AS ISMOD
				  FROM STOCK_SUJO AS S
				       INNER JOIN PIS_INFO AS P
				               ON S.PIS_MK=P.PIS_MK
				              AND S.CORP_MK=P.CORP_MK
				 WHERE S.CORP_MK = '".$this->getCorpId()."' ".$where;
				
		$model = [];
		$myobj =(object) [];
		
		foreach ($SALE_INFO_D as $d ){

			$myobj = (object) [];
			$myobj->CORP_MK		= $this->getCorpId();
			$myobj->PIS_MK		= $d->PIS_MK;
			$myobj->SIZES		= $d->SIZES;
			$myobj->INPUT_DATE	= substr($d->INPUT_DATE, 0, 10);
			$myobj->QTY			= $d->QTY;
			$myobj->SALE_UNCS	= $d->SALE_UNCS;
			$myobj->ORIGIN_CD	= $d->ORIGIN_CD;
			$myobj->ORIGIN_NM	= $d->ORIGIN_NM;
			$myobj->PIS_NM		= $d->PIS_NM;
			$myobj->REMARK		= $d->REMARK;
			$myobj->AMT			= $d->AMT;
			$myobj->WRITE_DATE	= substr($d->WRITE_DATE, 0, 10);
			$myobj->SUJO_NO		= '';
			$myobj->ISMOD		= true;
			$myobj->SRL_NO		= $d->SRL_NO;
			array_push($model, $myobj);
		}

		return view("sale.detailReturn", [ "sale" => $sale, 'model' => $model ] );
	}

	public function getUpdateList()
	{
		$cust_mk = urldecode((Request::segment(4)));
		$seq =  Request::segment(5);
		$wrtie_dt =  Request::segment(6);

		$SALE_INFO_D = DB::table("SALE_INFO_D AS D")
						->select(  "D.CORP_MK"
								 , "P.PIS_MK"
								 , "D.SIZES"
								 , "D.INPUT_DATE"
								 , "D.QTY"
								 , "D.UNCS"
								 , "D.ORIGIN_CD"
								 , "D.ORIGIN_NM"
								 , "P.PIS_NM"
								 , "D.REMARK"
								 , "D.AMT"
								 , "D.WRITE_DATE"
								 )
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
						})->where("D.CORP_MK" , $this->getCorpId())
						->where("D.CUST_MK", $cust_mk)
						->where("D.WRITE_DATE", $wrtie_dt)
						->where("D.SEQ", 6)->get();
		$where = "";
		foreach ($SALE_INFO_D as $info){
			$where.= " AND NOT ( S.PIS_MK='".$info->PIS_MK."' AND SIZES='".$info->SIZES."' AND INPUT_DATE='".$info->INPUT_DATE."' AND ORIGIN_CD='".$info->ORIGIN_CD."' AND ORIGIN_NM='".$info->ORIGIN_NM."') ";
		}

		$sql = " SELECT S.CORP_MK
				      , S.SUJO_NO
				      , S.PIS_MK
				      , S.SIZES
				      , LEFT(CONVERT(VARCHAR, S.INPUT_DATE, 120),10) AS INPUT_DATE
				      , S.QTY
				      , S.SALE_UNCS
				      , S.ORIGIN_CD
				      , S.ORIGIN_NM
				      , P.PIS_NM
					  , '' AS REMARK
					  , '' AS AMT
				  FROM STOCK_SUJO AS S
				       INNER JOIN PIS_INFO AS P
				               ON S.PIS_MK=P.PIS_MK
				              AND S.CORP_MK = P.CORP_MK
				 WHERE S.CORP_MK = '".$this->getCorpId()."' ".$where;
				
		$model = DB::select($sql);
		
		foreach ($SALE_INFO_D as $info){

			$myobj = (object) [];
			$myobj->CORP_MK		= $this->getCorpId();
			$myobj->PIS_MK		= $info->PIS_MK;
			$myobj->SIZES		= $info->SIZES;
			$myobj->INPUT_DATE	= substr($info->INPUT_DATE, 0, 10);
			$myobj->QTY			= $info->QTY;
			$myobj->SALE_UNCS	= $info->UNCS;
			$myobj->ORIGIN_CD	= $info->ORIGIN_CD;
			$myobj->ORIGIN_NM	= $info->ORIGIN_NM;
			$myobj->PIS_NM		= $info->PIS_NM;
			$myobj->WRITE_DATE	= $info->WRITE_DATE;
			$myobj->REMARK		= $info->REMARK;
			$myobj->AMT			= $info->AMT;
			$myobj->SUJO_NO		= '';
			
			array_push($model, $myobj);
		}
		return response()->json(['list' => $model]);
	}

	// 일일판매 목록 조회
	public function listData(){
		$SALE_INFO = DB::table('SALE_INFO_M AS SM')
						->select( 'SM.CORP_MK'
								, DB::raw( "CONVERT(CHAR(10), SM.WRITE_DATE, 23) AS WRITE_DATE" )
								, DB::raw( "SM.WRITE_DATE AS WRITE_DATE_ORDER" )
								, 'SM.CUST_MK'
								, 'SM.SEQ'
								, 'SM.CASH_AMT'
								, 'SM.CHECK_AMT'
								, 'SM.UNCL_AMT'
								, 'SM.CARD_AMT'
								, 'SM.TOTAL_SALE_AMT'
								, 'CUST_GRP.CUST_GRP_NM'
								, 'C.FRNM'
								, DB::raw( "CASE SD.CNT WHEN 1 THEN P.PIS_NM 
											ELSE P.PIS_NM + '외' + CONVERT (VARCHAR , (SD.CNT - 1)) + '종' END AS PIS")
								, DB::raw( "ROUND(SD.SUMQTY, 2) AS SUMQTY")
								, 'SM.SALE_DISCOUNT'
								, 'SM.SALE_DISCOUNT_REMARK'
								, 'P.PIS_MK'
								//, 'UNCL.REMARK'
								//, 'UNCL.AMT'
								//, DB::raw("CASE WHEN SD.SIZES = 'QTYS' THEN '마리' END AS SIZES")

								, DB::raw("STUFF ( (SELECT DISTINCT '/' + (P1.PIS_NM) + ' ' + (D1.SIZES) + ' ' + convert(VARCHAR, D1.QTY) + ' ' +  replace( convert( VARCHAR, convert( MONEY, D1.UNCS ), 1 ), '.00', '' )  + '' 
													  FROM SALE_INFO_D AS D1
													       INNER JOIN PIS_INFO AS P1
													               ON P1.PIS_MK = D1.PIS_MK
													              AND P1.CORP_MK = D1.CORP_MK
													 WHERE D1.CORP_MK    = '".$this->getCorpId()."'
													   AND D1.WRITE_DATE = SM.WRITE_DATE
													   AND D1.CUST_MK    = SM.CUST_MK
													   AND D1.SEQ        = SM.SEQ
													 GROUP BY D1.CORP_MK, D1.WRITE_DATE, D1.CUST_MK, D1.SEQ, D1.SIZES, P1.PIS_NM, D1.UNCS, D1.QTY
										  FOR XML PATH('')), 1, 1, '') AS PIS_LIST ")

								, DB::raw("STUFF ( (SELECT DISTINCT ',' + U.REMARK + ':' + replace(convert(VARCHAR, CONVERT(MONEY, U.AMT), 1), '.00', '')
													  FROM SALE_INFO_M AS M1
													       LEFT OUTER JOIN UNCL_INFO AS U
													               ON M1.SEQ = U.SALE_SEQ
													              AND M1.CORP_MK = U.CORP_MK
																  AND M1.CUST_MK = U.UNCL_CUST_MK
																  AND M1.WRITE_DATE = U.SALE_WRITE_DATE
													 WHERE M1.CORP_MK    = '".$this->getCorpId()."'
													   AND M1.WRITE_DATE = SM.WRITE_DATE
													   AND M1.CUST_MK    = SM.CUST_MK
													   AND M1.SEQ        = SM.SEQ
										  FOR XML PATH('')), 1, 1, '') AS UNCL_LIST ")
						)->join(DB::raw(" 
									( SELECT CORP_MK, WRITE_DATE, CUST_MK, SEQ, COUNT(CORP_MK) AS CNT, SUM(QTY) AS SUMQTY, MAX(PIS_MK) AS PIS_MK
									  FROM SALE_INFO_D AS D 
									 WHERE CORP_MK = '".$this->getCorpId()."'
									 GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ ) SD  ")
								, function($join){
									$join->on("SM.CORP_MK", "=", "SD.CORP_MK" );
									$join->on("SM.WRITE_DATE", "=" ,"SD.WRITE_DATE");
									$join->on("SM.CUST_MK", "=" ,"SD.CUST_MK");
									$join->on("SM.SEQ", "=" ,"SD.SEQ");
						})->join("CUST_CD AS C", function($join){
								$join->on("SM.CUST_MK", "=" ,"C.CUST_MK");
								$join->on("SM.CORP_MK", "=" ,"C.CORP_MK");	
						})->leftjoin("CUST_GRP_INFO AS CUST_GRP", function($join){
								$join->on("CUST_GRP.CUST_GRP_CD", "=" ,"C.CUST_GRP_CD");
								$join->on("CUST_GRP.CORP_MK", "=" ,"C.CORP_MK");	
						})->join("PIS_INFO AS P", function($join){
								$join->on("SD.PIS_MK", "=" ,"P.PIS_MK");
								$join->on("SD.CORP_MK", "=" ,"P.CORP_MK");
						})
						->where('SM.CORP_MK', $this->getCorpId())
						->where(function($query){
							if( Request::Has('custGrp') && Request::Input('custGrp') != "ALL"){
								$query->where("C.CUST_GRP_CD",  Request::Input('custGrp'));
							}

							if( Request::Has("CUST_MK") ){
								$query->where("C.CUST_MK",  Request::Input('CUST_MK'));
							}

							if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
								if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
									$query->whereBetween("SM.WRITE_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
								}
							}
						});
		
		return Datatables::of($SALE_INFO)
					
				->filter(function($query) {
					
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRNM"){
							$query->where("C.FRNM",  "like", "%".Request::Input('textSearch')."%" );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
							$query->where("P.PIS_NM",  "like", "%".Request::Input('textSearch')."%");
						}else{
							$query->where(function($q){
								$q->where("C.FRNM",  "like", "%".Request::Input('textSearch')."%" )
								->orwhere("P.PIS_NM",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
		
	}

	public function getTotal(){

		$SALE_INFO = DB::table('SALE_INFO_M AS SM')
						->select( DB::raw("ISNULL(SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1))-1), 0) AS TUNCL")
								, DB::raw("ISNULL(SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1))-1), 0) AS TSALE  ")
								, DB::raw("ISNULL(CONVERT(varchar(18),cast(SUM(SD.SUMQTY) as money),1), 0) AS TQTY ")
								, DB::raw("ISNULL(SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1))-1), 0) AS TCASH ")
								, DB::raw("ISNULL(SUM(SD.QTY_SUM), 0) AS QTY_SUM")
								, DB::raw("ISNULL(SUM(SD.KG_SUM), 0) AS KG_SUM")
						)->join(DB::raw(" (SELECT CORP_MK
											   , WRITE_DATE
											   , CUST_MK
											   , SEQ
											   , SUM(QTY) AS SUMQTY 
											   , SUM(CASE WHEN SIZES = 'QTYS' THEN QTY END) AS QTY_SUM
											   , SUM(CASE WHEN SIZES <> 'QTYS' THEN QTY END) AS KG_SUM
											FROM SALE_INFO_D 
										   WHERE CORP_MK = '".$this->getCorpId()."'
										   GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ ) AS SD "), function($join){

								$join->on("SM.CORP_MK", "=", "SD.CORP_MK");
								$join->on("SM.WRITE_DATE", "=", "SD.WRITE_DATE");
								$join->on("SM.CUST_MK", "=", "SD.CUST_MK" );
								$join->on("SM.SEQ", "=", "SD.SEQ" );
							
						})->where ( 'SM.CORP_MK', $this->getCorpId())
						->where(function($query){
										
								if( Request::Input('START_DATE') !=''  && Request::has('START_DATE')) {
									if( Request::Input('END_DATE') !='' && Request::has('END_DATE')){

										return $query->where('SM.WRITE_DATE', '>=', Request::Input('START_DATE'))
												->where('SM.WRITE_DATE', '<=', Request::Input('END_DATE'));
									}
								}
							
						})->first();
		
		return response()->json($SALE_INFO, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
	}

	// 일일판매 상세조회(목록형)
	public function detailData(){
		$SALE_INFO = DB::table("SALE_INFO_D AS D")
						->select("P.PIS_NM"
								, "D.CORP_MK"
								, DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								, "D.ORIGIN_NM"
								, "D.ORIGIN_CD"
								, "D.CUST_MK"
								, "D.SEQ"
								, "D.PIS_MK"
								, "D.SIZES"
								, "D.QTY"
								, "D.UNCS"
								, "D.AMT"
								, "D.SRL_NO"
								, "D.REMARK")
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");	
								$join->on("D.CORP_MK", "=" ,"P.CORP_MK");
							}
						)->where( 'D.CORP_MK', $this->getCorpId())
						->where( 'D.CUST_MK', urldecode(Request::Input("cust_mk")))
						->where( 'D.SEQ', Request::Input("seq"))
						->where( 'D.WRITE_DATE', Request::Input("write_date"));
		
		
		return Datatables::of($SALE_INFO)->make(true);
	}

	// 판매저장
	public function setSaleInfoInsert(){
		
		DB::beginTransaction();
		$seq = $this->getMaxSeqInSALEINFO_M(Request::input('WRITE_DATE'), urldecode(Request::input('CUST_MK'))) + 1 ;
		
		try {
			
			$CUST_FRNM = DB::table("CUST_CD")->where("CUST_MK", urldecode(Request::Input('CUST_MK')))->where("CORP_MK", $this->getCorpId())->first()->FRNM;

			// 판매내역 입력
			DB::table("SALE_INFO_M")->insert( 
				[
					  'CORP_MK'				=> $this->getCorpId()
					, 'WRITE_DATE'			=> Request::input('WRITE_DATE')
					, 'CUST_MK'				=> urldecode(Request::input('CUST_MK'))
					, 'SEQ'					=> $seq
					, 'CASH_AMT'			=> Request::input('CASH_AMT')
					, 'CHECK_AMT'			=> Request::input('CHECK_AMT')
					, 'CARD_AMT'			=> Request::input('CARD_AMT')
					, 'UNCL_AMT'			=> Request::input('UNCL_AMT')
					, 'TOTAL_SALE_AMT'		=> Request::input('TOTAL_SALE_AMT')
					, 'SALE_DISCOUNT'		=> Request::input('SALE_DISCOUNT')
					, 'SALE_DISCOUNT_REMARK'=> Request::input('SALE_DISCOUNT_REMARK')
					//, 'OUTLINE'				=> $CUST_FRNM."에게 판매"
				] );

			$i = 0;
			foreach (Request::Input("DETAIL") as $item) {
				
				$qty = (float) $item[7];
				
				$stock_seq = $this->getMaxSeqInSTOCKINFO($item[4] , $item[12] ) + 1;

				$data = [
					
						  'CORP_MK'			=> $this->getCorpId()
						, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
						, 'CUST_MK'			=> urldecode(Request::input('CUST_MK'))
						, 'SEQ'				=> $seq
						, 'SRL_NO'			=> ++$i
						, 'PIS_MK'			=> $item[12]
						, 'SIZES'			=> $item[4]
						, 'INPUT_DATE'		=> $item[3]
						, 'ORIGIN_CD'		=> $item[11]
						, 'ORIGIN_NM'		=> $item[2]
						, 'QTY'				=> $item[7]
						, 'UNCS'			=> $item[8]
						, 'AMT'				=> $item[9]
						, 'STOCK_INFO_SEQ'	=> $stock_seq

					];

				DB::table("SALE_INFO_D")->insert( $data );
				DB::table("STOCK_INFO")->insert(
					[
						  'CORP_MK'			=> $this->getCorpId()
						, 'PIS_MK'			=> $item[12]
						, 'SIZES'			=> $item[4]
						, 'SEQ'				=> $stock_seq
						, 'CUST_MK'			=> urldecode(Request::input('CUST_MK'))
						, 'ORIGIN_CD'		=> $item[11]
						, 'ORIGIN_NM'		=> $item[2]
						, 'INPUT_DATE'		=> $item[3]
						, 'OUTPUT_DATE'		=> Request::input('WRITE_DATE')
						//, 'OUTLINE'			=> $CUST_FRNM."에게 판매"
						, 'DE_CR_DIV'		=> 0
						, 'QTY'				=> $qty
						, 'CURR_QTY'		=> $qty
						, 'SUJO_NO'			=> $item[0]
					]
				);
				
				STOCK_SUJO::where("CORP_MK", $this->getCorpId())
					->where("SUJO_NO", $item[0])
					->where("PIS_MK", $item[12])
					->where("SIZES",  $item[4])
					->update(
						[
							'QTY' => DB::raw("QTY - ".$qty)
						]
					);
			}
			
			if( (int)Request::input('UNCL_AMT') != 0) { 
				// 미수정보

				$PROV_DIV = 0;
				$UNCL_AMT = Request::input('UNCL_AMT');

				// 만약에 미수금이 (-)가 될때는..미수금을 입금으로 잡아서 처리
				if( (int)Request::input('UNCL_AMT') < 0){
					$PROV_DIV = 1;
					$UNCL_AMT *=(-1);
				}

				DB::table("UNCL_INFO")->insert(
					[	
						  'CORP_MK'			=> $this->getCorpId()
						, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
						, 'SEQ'				=> (int)($this->getMaxSeqInUNCLINFO(Request::input('WRITE_DATE'))) + 1
						, 'SALE_SEQ'		=> $seq
						, 'UNCL_CUST_MK'	=> urldecode(Request::input('CUST_MK'))
						, 'PROV_DIV'		=> $PROV_DIV
						, 'CASH_AMT'		=> 0
						, 'CHECK_AMT'		=> 0
						, 'CARD_AMT'		=> 0
						, 'UNCL_AMT'		=> $UNCL_AMT
						, 'AMT'				=> $UNCL_AMT
						, 'REMARK'			=> '판매미수'
					]
				);
			}
		}
		catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success', 'seq' => $seq, 'write_date'=> Request::input('WRITE_DATE') ]);
		
	}

	// 일일판매 내역 수정
	// 1. 로그남기기
	// 2. 판매정보 수조반환
	// 3. 기존 미수정보 삭제
	// 4. 기존 재고정보 삭제
	// 5. 기존 판매정보 삭제
	// 6. 새 미수 정보 추가
	// 7. 새 재고 정보 추가
	// 8. 새 판매정보 추가
	public function setUpdateSaleDetail(){
		
		return $exception = DB::transaction(function(){

			$validator = Validator::make( Request::Input(), [
				'TYPE'			=> 'required',
				'CUST_MK'		=> 'required|max:20,NOT_NULL',
				'SEQ'			=> 'required',
				'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
				'OLD_WRITE_DATE'=> 'required|date_format:"Y-m-d',
				'CASH_AMT'		=> 'numeric',
				'CARD_AMT'		=> 'numeric',
				'UNCL_AMT'		=> 'numeric',
				'TOTAL_SALE_AMT'=> 'numeric',
			]);
			
			$type	= Request::Input("TYPE");
			$cash	= (int)Request::Input("CASH_AMT");
			$card	= (int)Request::Input("CARD_AMT");
			$remark	= Request::Input("SALE_DISCOUNT_REMARK");
			$uncl	= (int)Request::Input("UNCL_AMT");
			$total	= (int)Request::Input("TOTAL_SALE_AMT");	// 정상합계
			$list	= Request::Input("list");
			$uncl_yn= Request::Input("UNCL_ONLY_YN") == "Y" ? "Y" : "N";// 단가만 수정 여부 (Y/N)

			if( $total != ( $cash + $card + $uncl )) {
				//return response()->json(['result' => 'failSum', 'message' => "합계"], 422);
			}

			if ($validator->fails()) {
				$errors = $validator->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
			
			$SALE_M_VM	= (object) [];
			$SALE_D_VM	= [ [] ];

			$SALE_M_NewSeq	= $this->getMaxSeqInSALEINFO_M( Request::Input("WRITE_DATE"), urldecode(Request::Input("CUST_MK"))) + 1;

			$SALE_M_VM->CORP_MK				= $this->getCorpId();
			$SALE_M_VM->WRITE_DATE			= Request::Input("WRITE_DATE");
			$SALE_M_VM->CUST_MK				= urldecode(Request::Input("CUST_MK"));
			$SALE_M_VM->SEQ					= $SALE_M_NewSeq;
			$SALE_M_VM->SALE_DISCOUNT		= Request::Input("SALE_DISCOUNT");
			$SALE_M_VM->SALE_DISCOUNT_REMARK= Request::Input("SALE_DISCOUNT_REMARK");
			$SALE_M_VM->CASH_AMT			= Request::Input("CASH_AMT");
			$SALE_M_VM->CARD_AMT			= Request::Input("CARD_AMT");
			$SALE_M_VM->UNCL_AMT			= Request::Input("UNCL_AMT");
			$SALE_M_VM->TOTAL_SALE_AMT		= Request::Input("TOTAL_SALE_AMT");
			
			$SRL_NO = 0;
			$INDEX = 0;
		
			foreach($list as $dlist){
				
				// 기존 값이 돌아가야 할 수조 값을 가상 필드인 sujo_no필드에 넣어 줌
				// 수량이 0일때
				if( $dlist[5] === null || ( $dlist[5] == 0 && $dlist[0] == "" ) ) {
					
					continue;
				}
				// 새수조가 있을때..
				if( $dlist[10] !== null && $dlist[10] != "" ){
					
					// 단가만 수정일때 시행되지 않음
					if( $uncl_yn == "N"){

						$countSujo = DB::table("STOCK_SUJO")->where("CORP_MK", $this->getCorpId())->where("SUJO_NO",  $dlist[10])->count();
						if( $countSujo == 0){
							DB::table("STOCK_SUJO")
								->insert(
									[
										  "CORP_MK"		=> $this->getCorpId()
										, "SUJO_NO"		=> $dlist[10]
										, "PIS_MK"		=> $dlist[13]
										, "SIZES"		=> $dlist[2]
										, "INPUT_DATE"	=> $dlist[4]
										, "QTY"			=> 0
										, "SALE_UNCS"	=> $dlist[6]
										, "ORIGIN_CD"	=> $dlist[14]
										, "ORIGIN_NM"	=> $dlist[3]
										//, "CUST_MK"		=> $dlist[3]
									]
							);
						}else{
							$dlist[10] .= $dlist[10].$countSujo;

							DB::table("STOCK_SUJO")
								->insert(
									[
										  "CORP_MK"		=> $this->getCorpId()
										, "SUJO_NO"		=> $dlist[10]
										, "PIS_MK"		=> $dlist[13]
										, "SIZES"		=> $dlist[2]
										, "INPUT_DATE"	=> $dlist[4]
										, "QTY"			=> 0
										, "SALE_UNCS"	=> $dlist[6]
										, "ORIGIN_CD"	=> $dlist[14]
										, "ORIGIN_NM"	=> $dlist[3]
										//, "CUST_MK"		=> $dlist[3]
									]
							);
						}
					}
				}

				// 수정시 삭제 선택되었을때(삭제기능 우선 보류 요청)
				if($dlist[0] == "1" && $type == "UPDATE"){
					//continue;
				}
				
				$new_sujo_no = $dlist[10];
				if( $new_sujo_no == ""){
					$new_sujo_no = $dlist[9];
				}

				if( $new_sujo_no == "" || $new_sujo_no == "---"){
					//throw new Exception();
				}

				$SALE_D_VM[$INDEX++] = 
					[
						"CORP_MK"			=>$SALE_M_VM->CORP_MK
						,"WRITE_DATE"		=>$SALE_M_VM->WRITE_DATE
						,"CUST_MK"			=>urldecode($SALE_M_VM->CUST_MK)
						,"SEQ"				=>$SALE_M_VM->SEQ
						,"SRL_NO"			=>$SRL_NO++
						,"PIS_MK"			=>$dlist[13]
						,"SIZES"			=>$dlist[2]
						,"INPUT_DATE"		=>$dlist[4]
						,"ORIGIN_CD"		=>$dlist[14]
						,"ORIGIN_NM"		=>$dlist[3]
						,"QTY"				=>$dlist[5]
						,"UNCS"				=>$dlist[6]
						,"AMT"				=>$dlist[7]
						,"REMARK"			=>$dlist[8]
						,"SUJO_NO"			=> $new_sujo_no
						,"STOCK_INFO_SEQ"	=> ''
					];
			}
			
			// 1. 판매정보 M SELECT
			$SALE_INFO_M =  DB::table("SALE_INFO_M AS SM")
							->join("CUST_CD AS C", function($join){
									$join->on("SM.CUST_MK", "=", "C.CUST_MK");
									$join->on("SM.CORP_MK", "=", "C.CORP_MK");
								}
							)->where("SM.CORP_MK",		$this->getCorpId())
							->where("SM.WRITE_DATE",	Request::Input("OLD_WRITE_DATE"))
							->where("SM.CUST_MK",		urldecode(Request::Input("CUST_MK")))
							->where("SM.SEQ" ,			Request::Input("SEQ"))
							->select(
										"SM.CORP_MK"
										,DB::raw( "CONVERT(CHAR(10), SM.WRITE_DATE, 23) AS WRITE_DATE" )
										,"SM.CUST_MK" 
										,"SM.SEQ" 
										,"SM.CASH_AMT" 
										,"SM.CHECK_AMT"
										,"SM.CARD_AMT" 
										,"SM.UNCL_AMT" 
										,"SM.TOTAL_SALE_AMT" 
										,"SM.SALE_DISCOUNT" 
										,"SM.SALE_DISCOUNT_REMARK" 
										,"C.FRNM"
							)->first();

				

			// 2. 판매정보 D SELECT
			$SALE_INFO_D = DB::table("SALE_INFO_D AS D")
							->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
							}
						)
					->where("D.CORP_MK",		$this->getCorpId())
					->where("D.WRITE_DATE",		Request::Input("OLD_WRITE_DATE"))
					->where("D.CUST_MK",		urldecode(Request::Input("CUST_MK")))
					->where("D.SEQ" ,			Request::Input("SEQ"))
					->select(
								"D.CORP_MK" 
								,DB::raw( "CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE" )
								,"D.CUST_MK" 
								,"D.SEQ" 
								,"D.SRL_NO" 
								,"D.PIS_MK" 
								,"D.SIZES" 
								,DB::raw( "CONVERT(CHAR(10), D.INPUT_DATE, 23) AS INPUT_DATE" )
								,"D.ORIGIN_CD" 
								,"D.ORIGIN_NM" 
								,"D.QTY" 
								,"D.UNCS" 
								,"D.AMT" 
								,"D.REMARK"
								,"D.STOCK_INFO_SEQ" 
								,"P.PIS_NM"
								,DB::raw("''AS SUJO_NO ")
					)->get();
			
			// 3. 수조정보
			$SUJO_INFO = DB::table("STOCK_SUJO AS S")
							->join("PIS_INFO AS P", function($join){
								$join->on("S.PIS_MK", "=", "P.PIS_MK");
								$join->on("S.CORP_MK", "=", "P.CORP_MK");
							}
						)->where("S.CORP_MK", $this->getCorpId())
						->select(
								  "S.CORP_MK"
								, "S.SUJO_NO"
								, "S.PIS_MK"
								, "S.SIZES"
								, DB::raw("LEFT(CONVERT(VARCHAR, S.INPUT_DATE, 120),10) AS INPUT_DATE")
								, "S.QTY"
								, "S.SALE_UNCS"
								, "S.ORIGIN_CD"
								, "S.ORIGIN_NM"
								, "P.PIS_NM"
						)->get();
			
			if( $SALE_INFO_M == null) return;

			try {
				// 반품일 경우
				if( $type == "RETURN"){
					
					$this->setSaleLog( $SALE_M_VM, $SALE_INFO_M, "U", $SALE_D_VM, $SALE_INFO_D);
					// 2. 반품 : 새값을 수조에서 빼기 (반품을 수정 삭제한 경우는 더하기)
					foreach($SALE_D_VM as $sale_d){

						// 이전 값을 수조에 더해주기(반품을 수정 삭제한 경우는  빼주기)
						$QTY = 0;
						if( (float)$SALE_M_VM->TOTAL_SALE_AMT < 0){
							$QTY = (float)($sale_d["QTY"]) * (-1);
						}else{
							$QTY = (float)$sale_d["QTY"];
						}

						DB::table("STOCK_SUJO")
							->where("CORP_MK", $this->getCorpId())
							->where("SUJO_NO", $sale_d["SUJO_NO"])
							->update([
								'QTY' => DB::raw("QTY + $QTY")
							]);
					}

					// 3. 반품 : 기존미수금 정보 삭제
					DB::table("UNCL_INFO")
						->where("CORP_MK",		$this->getCorpId())
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("SALE_SEQ" ,	$SALE_INFO_M->SEQ)
						->where("UNCL_CUST_MK" ,$SALE_INFO_M->CUST_MK)
						->delete();	
					
					// 반품. 4 :. 기존재고정보 삭제
					// 반품이 됐을 경우는 기존의 판매한 수조정보를 삭제 하지 않는다.
					// 단지 반품 내역을 다시 입고시키므로 해당 부분은 주석처리
					/*
					foreach($SALE_INFO_D as $row){
						
						DB::table("STOCK_INFO")
							->where("CORP_MK",			$row->CORP_MK)
							->where("PIS_MK",			$row->PIS_MK)
							->where("SIZES",			$row->SIZES)
							->where("INPUT_DATE",		$row->INPUT_DATE)
							->where("DE_CR_DIV",		"0")
							->where("ORIGIN_CD" ,		$row->ORIGIN_CD)
							->where("QTY" ,				$row->QTY)
							->where("SEQ",				$row->STOCK_INFO_SEQ)
							->delete();
					}
					*/
					
					// 4. 반품 : 새 재고정보 추가(입고)
					$D_IDEX = 0;
					foreach($SALE_D_VM as $row){
						$maxSeqStockInfo = $this->getMaxSeqInSTOCKINFO($row["SIZES"], $row["PIS_MK"]) + 1;
						$SALE_D_VM[$D_IDEX++]["STOCK_INFO_SEQ"] = $maxSeqStockInfo;
						
						// 반품은 거래처 
						$sale_cusk_mk = DB::table("STOCK_INFO")
										->where("CORP_MK",		$row["CORP_MK"])
										->where("PIS_MK",		$row["PIS_MK"] )
										->where("SIZES" ,		$row["SIZES"])
										->where("INPUT_DATE" ,	$row["INPUT_DATE"])
										->where("OUTPUT_DATE",  $row["WRITE_DATE"])
										->where("ORIGIN_CD",  $row["ORIGIN_CD"])
										->select("CUST_MK")
										->first();

						DB::table("STOCK_INFO")
							->insert([
								 'CORP_MK'		=> $row["CORP_MK"]
								,'PIS_MK'		=> $row["PIS_MK"]
								,'SIZES'		=> $row["SIZES"]
								,'SEQ'			=> $maxSeqStockInfo

								//,'CUST_MK'		=> $sale_cusk_mk->CUST_MK
								,'CUST_MK'		=> $row["CUST_MK"]
								
								// 판매입고 된 날짜는 반품된 날짜를 입고날짜로 붙여야 의미상 맞음
								// 2017-08-07 : 김현섭
								,'INPUT_DATE'	=> $row["WRITE_DATE"]
								,'REASON'		=> '5'
								,'DE_CR_DIV'	=> 1
								,'QTY'			=> $row["QTY"]
								,'CURR_QTY'		=> $row["QTY"]
								,'ORIGIN_CD'	=> $row["ORIGIN_CD"]
								,'ORIGIN_NM'	=> $row["ORIGIN_NM"]
								,'SUJO_NO'		=> $row["SUJO_NO"]
						]);
					}
					
					// 5. 반품 : 기존판매정보 삭제
					DB::table("SALE_INFO_D")
						->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
						->where("SEQ" ,			$SALE_INFO_M->SEQ)
						->delete();
					
					DB::table("SALE_INFO_M")
						->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
						->where("SEQ" ,			$SALE_INFO_M->SEQ)
						->delete(); 

					// 일부 반품을 할 경우 로직이 빠져있음
					

				}
				else{
					
					// 수정. 0 : 로그추가
					//$this->setSaleLog( $SALE_M_VM, $SALE_INFO_M, "U", $SALE_D_VM, $SALE_INFO_D);
					// 수정. 1 :  이전 값을 수조에 더해주기(반품을 수정 삭제한 경우는  빼주기)
					// 단가만 수정일때 수조 수량은 변동없음
					if( $uncl_yn == "N"){
						if(!empty($SALE_D_VM[0]) && $SALE_D_VM !== null && count($SALE_D_VM) > 0){
							$i = 0;
							foreach(array_reverse($SALE_INFO_D) as $sale_d){
								
								// 이전 값을 수조에 더해주기(반품을 수정 삭제한 경우는  빼주기)
								$QTY = 0.0;
								if( (float)$SALE_INFO_M->TOTAL_SALE_AMT < 0){
									$QTY = (float)$sale_d->QTY * (-1);
								}else{
									$QTY = (float)$sale_d->QTY;
								}
								
								DB::table("STOCK_SUJO")
									->where("CORP_MK", $this->getCorpId())
									->where("SUJO_NO", $SALE_D_VM[$i++]["SUJO_NO"])
									->update([
										"QTY"	=>	DB::raw("QTY + ".($QTY))
									]);
							}
						}
					
						// 수정. 2 : 이전값을 더해주기(반품을 수정 삭제할 경우는 빼주기)
						foreach($SALE_D_VM as $sale_d){
							
							if( $sale_d !== null && !empty($sale_d) ){
								// 이전 값을 수조에 더해주기(반품을 수정 삭제한 경우는  빼주기)
								$QTY = 0.0;
								if( (float)$SALE_M_VM->TOTAL_SALE_AMT < 0){
									$QTY = (float)($sale_d["QTY"]) * (-1);
								}else{
									$QTY = (float)$sale_d["QTY"];
								}

								DB::table("STOCK_SUJO")
									->where("CORP_MK", $this->getCorpId())
									->where("SUJO_NO", $sale_d["SUJO_NO"])
									->update([
										'QTY' => DB::raw("[QTY] - $QTY")
									]);
							}
						}
					}
					
					// 수정. 3 :. 기존미수금 정보 삭제
					DB::table("UNCL_INFO")
						->where("CORP_MK",		$this->getCorpId())
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("SALE_SEQ" ,	$SALE_INFO_M->SEQ)
						->where("UNCL_CUST_MK" ,$SALE_INFO_M->CUST_MK)
						->delete();	
					
					
					// 수정. 4 :. 기존재고정보 삭제
					foreach($SALE_INFO_D as $row){
						
						DB::table("STOCK_INFO")
							->where("CORP_MK",			$row->CORP_MK)
							->where("PIS_MK",			$row->PIS_MK)
							->where("SIZES",			$row->SIZES)
							->where("INPUT_DATE",		$row->INPUT_DATE)
							->where("ORIGIN_CD" ,		$row->ORIGIN_CD)
							->where("QTY" ,				$row->QTY)
							->where("SEQ",				$row->STOCK_INFO_SEQ)
							->delete();
					}
					
					// 수정. 5 :. 기존판매정보 삭제
					DB::table("SALE_INFO_D")
						->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
						->where("SEQ" ,			$SALE_INFO_M->SEQ)
						->delete();
				
					DB::table("SALE_INFO_M")
						->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
						->where("SEQ" ,			$SALE_INFO_M->SEQ)
						->delete();
					
					// 수정 : 6. 새 미수정보 추가					
					$seq_unclinfo = $this->getMaxSeqInUNCLINFO(Request::input('WRITE_DATE'));
					DB::table("UNCL_INFO")
						->insert([
							 'CORP_MK'		=> $SALE_M_VM->CORP_MK
							//,'WRITE_DATE'	=> $SALE_M_VM->WRITE_DATE
							,'WRITE_DATE'	=> Request::Input("WRITE_DATE")
							,'SEQ'			=> (int)$seq_unclinfo + 1
							,'SALE_SEQ'		=> $SALE_M_VM->SEQ
							,'UNCL_CUST_MK'	=> $SALE_M_VM->CUST_MK
							,'PROV_DIV'		=> '0'
							,'CASH_AMT'		=> $SALE_M_VM->CASH_AMT
							,'CHECK_AMT'	=> 0
							,'CARD_AMT'		=> $SALE_M_VM->CARD_AMT
							,'UNCL_AMT'		=> $SALE_M_VM->UNCL_AMT
							,'AMT'			=> $SALE_M_VM->UNCL_AMT
							,'REMARK'		=> '판매미수'
					]);
				
					// 수정 : 7. 새 재고정보 추가
					$D_IDEX = 0;
					
					foreach($SALE_D_VM as $row){
						if( $row !== null && !empty($row) ){
							$maxSeqStockInfo = $this->getMaxSeqInSTOCKINFO($row["SIZES"], $row["PIS_MK"]) + 1;
							$SALE_D_VM[$D_IDEX++]["STOCK_INFO_SEQ"] = $maxSeqStockInfo;
							
							//dd($row);

							if( $row["QTY"] != 0 && $row["QTY"] != 0.0 && $row["QTY"] != null){
								DB::table("STOCK_INFO")
									->insert([
										 'CORP_MK'		=> $row["CORP_MK"]
										,'PIS_MK'		=> $row["PIS_MK"]
										,'SIZES'		=> $row["SIZES"]
										,'SEQ'			=> $maxSeqStockInfo

										,'CUST_MK'		=> $row["CUST_MK"]
										,'INPUT_DATE'	=> $row["INPUT_DATE"]
										//,'OUTPUT_DATE'	=> $row["WRITE_DATE"]
										,'OUTPUT_DATE'	=> Request::Input("WRITE_DATE")
										,'OUTLINE'		=> $SALE_INFO_M->FRNM + ' 에게 판매'
										,'DE_CR_DIV'	=> 0
										,'QTY'			=> $row["QTY"]
										,'CURR_QTY'		=> $row["QTY"]
										,'ORIGIN_CD'	=> $row["ORIGIN_CD"]
										,'ORIGIN_NM'	=> $row["ORIGIN_NM"]
										,'SUJO_NO'		=> $row["SUJO_NO"]
								]);
							}
						}
					}
				
					// 수정 8. 새판매정보 M 추가
					if( !empty( $SALE_D_VM ) && $SALE_D_VM !== null){
						DB::table("SALE_INFO_M")->insert([
								 "CORP_MK"				=> $SALE_M_VM->CORP_MK
								//,"WRITE_DATE"			=> $SALE_M_VM->WRITE_DATE
								,"WRITE_DATE"			=> Request::Input("WRITE_DATE")
								,"CUST_MK"				=> $SALE_M_VM->CUST_MK
								,"SEQ"					=> $SALE_M_VM->SEQ
								,"CASH_AMT"				=> $SALE_M_VM->CASH_AMT
								,"CARD_AMT"				=> $SALE_M_VM->CARD_AMT
								,"UNCL_AMT"				=> $SALE_M_VM->UNCL_AMT
								,"TOTAL_SALE_AMT"		=> $SALE_M_VM->TOTAL_SALE_AMT
								,"SALE_DISCOUNT"		=> $SALE_M_VM->SALE_DISCOUNT
								,"SALE_DISCOUNT_REMARK"	=> $SALE_M_VM->SALE_DISCOUNT_REMARK
						]);
					}
					
					// 수정 8-1. 새판매정보 D 추가
					foreach( $SALE_D_VM AS $row){

						if( $row !== null && !empty($row) ){

							if( $row["QTY"] != 0 && $row["QTY"] != 0.0 && $row["QTY"] != null){
								DB::table("SALE_INFO_D")->insert(
									[
										"CORP_MK"			=>$row["CORP_MK"]
										,"WRITE_DATE"		=>Request::Input("WRITE_DATE")
										,"CUST_MK"			=>$row["CUST_MK"]
										,"SEQ"				=>$row["SEQ"]
										,"SRL_NO"			=>$row["SRL_NO"]
										,"PIS_MK"			=>$row["PIS_MK"]
										,"SIZES"			=>$row["SIZES"]
										,"INPUT_DATE"		=>$row["INPUT_DATE"]
										,"ORIGIN_CD"		=>$row["ORIGIN_CD"]
										,"ORIGIN_NM"		=>$row["ORIGIN_NM"]
										,"QTY"				=>$row["QTY"]
										,"UNCS"				=>$row["UNCS"]
										,"AMT"				=>$row["AMT"]
										,"REMARK"			=>$row["REMARK"]
										,"STOCK_INFO_SEQ"	=>$row["STOCK_INFO_SEQ"]
									]
								);
							}
						}
					}
				}
				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}catch(Exception $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}
		});
	}


	// 일일판매 삭제
	public function setDeleteSaleDetail(){
		
		return $exception = DB::transaction(function(){

			$validator = Validator::make( Request::Input(), [
				'CUST_MK'		=> 'required|max:20,NOT_NULL',
				'SEQ'			=> 'required',
				'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			]);

			$list	= Request::Input("list");

			if ($validator->fails()) {
				$errors = $validator->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
			
			// 1. 판매정보 M SELECT
			$SALE_INFO_M =  DB::table("SALE_INFO_M AS SM")
							->join("CUST_CD AS C", function($join){
									$join->on("SM.CUST_MK", "=", "C.CUST_MK");
									$join->on("SM.CORP_MK", "=", "C.CORP_MK");
								}
							)->where("SM.CORP_MK",		$this->getCorpId())
							->where("SM.WRITE_DATE",	Request::Input("WRITE_DATE"))
							->where("SM.CUST_MK",		urldecode(Request::Input("CUST_MK")))
							->where("SM.SEQ" ,			Request::Input("SEQ"))
							->select(
										"SM.CORP_MK"
										,DB::raw( "CONVERT(CHAR(10), SM.WRITE_DATE, 23) AS WRITE_DATE" )
										,"SM.CUST_MK" 
										,"SM.SEQ" 
										,"SM.CASH_AMT" 
										,"SM.CHECK_AMT"
										,"SM.CARD_AMT" 
										,"SM.UNCL_AMT" 
										,"SM.TOTAL_SALE_AMT" 
										,"SM.SALE_DISCOUNT" 
										,"SM.SALE_DISCOUNT_REMARK" 
										,"C.FRNM"
							)->first();

			// 2. 판매정보 D SELECT
			$SALE_INFO_D = DB::table("SALE_INFO_D AS D")
							->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
							}
						)
					->where("D.CORP_MK",		$this->getCorpId())
					->where("D.WRITE_DATE",		Request::Input("WRITE_DATE"))
					->where("D.CUST_MK",		urldecode(Request::Input("CUST_MK")))
					->where("D.SEQ" ,			Request::Input("SEQ"))
					->select(
								"D.CORP_MK" 
								,DB::raw( "CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE" )
								,"D.CUST_MK" 
								,"D.SEQ" 
								,"D.SRL_NO" 
								,"D.PIS_MK" 
								,"D.SIZES" 
								,DB::raw( "CONVERT(CHAR(10), D.INPUT_DATE, 23) AS INPUT_DATE" )
								,"D.ORIGIN_CD" 
								,"D.ORIGIN_NM" 
								,"D.QTY" 
								,"D.UNCS" 
								,"D.AMT" 
								,"D.REMARK"
								,"D.STOCK_INFO_SEQ" 
								,"P.PIS_NM"
								,DB::raw("(SELECT TOP 1 SUJO_NO FROM STOCK_SUJO  WHERE CORP_MK = D.CORP_MK AND PIS_MK = D.PIS_MK AND SIZES = D.SIZES AND ORIGIN_CD = D.ORIGIN_CD AND INPUT_DATE = D.INPUT_DATE) AS SUJO_NO")
					)->get();
			
			// 3. 수조정보
			$SUJO_INFO = DB::table("STOCK_SUJO AS S")
							->join("PIS_INFO AS P", function($join){
								$join->on("S.PIS_MK", "=", "P.PIS_MK");
								$join->on("S.CORP_MK", "=", "P.CORP_MK");
							}
						)->where("S.CORP_MK", $this->getCorpId())
						->select(
								  "S.CORP_MK"
								, "S.SUJO_NO"
								, "S.PIS_MK"
								, "S.SIZES"
								, DB::raw("LEFT(CONVERT(VARCHAR, S.INPUT_DATE, 120),10) AS INPUT_DATE")
								, "S.QTY"
								, "S.SALE_UNCS"
								, "S.ORIGIN_CD"
								, "S.ORIGIN_NM"
								, "P.PIS_NM"
						)->get();

			try {
				
				$this->setSaleLog( null, $SALE_INFO_M, "D", null, $SALE_INFO_D);
				
				// 삭제. 1 : 이전값을 빼주기(반품을 수정 삭제할 경우는 빼주기)
				foreach($SALE_INFO_D as $sale_d){
					
					// 이전 값을 수조에 더해주기(반품을 수정 삭제한 경우는  빼주기)
					$QTY = 0;
					if( $SALE_INFO_M->TOTAL_SALE_AMT < 0){
						$QTY = ($sale_d->QTY) * (-1);
					}else{
						$QTY = $sale_d->QTY;
					}

					DB::table("STOCK_SUJO")
						->where("CORP_MK", $this->getCorpId())
						->where("SUJO_NO", $sale_d->SUJO_NO)
						->update([
							'QTY' => DB::raw("[QTY] + $QTY")
						]);
				}
		
				// 삭제. 2 :. 기존미수금 정보 삭제
				if( $SALE_INFO_M != null && count($SALE_INFO_M) > 0){
					
					DB::table("UNCL_INFO")
						->where("CORP_MK",		$this->getCorpId())
						->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
						->where("SALE_SEQ" ,	$SALE_INFO_M->SEQ)
						->where("UNCL_CUST_MK" ,$SALE_INFO_M->CUST_MK)
						->delete();	
				}

				// 삭제. 3 :. 기존재고정보 삭제
				foreach($SALE_INFO_D as $row){
					
					DB::table("STOCK_INFO")
						->where("CORP_MK",			$row->CORP_MK)
						->where("PIS_MK",			$row->PIS_MK)
						->where("SIZES",			$row->SIZES)
						->where("INPUT_DATE",		$row->INPUT_DATE)
						->where("ORIGIN_CD" ,		$row->ORIGIN_CD)
						->where("ORIGIN_NM" ,		$row->ORIGIN_NM)
						->where("QTY" ,				$row->QTY)
						->where("SEQ",				$row->STOCK_INFO_SEQ)
						->delete();

				}
				
				// 삭제. 4 :. 기존판매정보 삭제
				DB::table("SALE_INFO_D")
					->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
					->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
					->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
					->where("SEQ" ,			$SALE_INFO_M->SEQ)
					->delete();
			
				DB::table("SALE_INFO_M")
					->where("CORP_MK",		$SALE_INFO_M->CORP_MK)
					->where("WRITE_DATE",	$SALE_INFO_M->WRITE_DATE )
					->where("CUST_MK",		$SALE_INFO_M->CUST_MK )
					->where("SEQ" ,			$SALE_INFO_M->SEQ)
					->delete();
				
				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}catch(Exception $e){
				$errors = $e->errors();
				
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}
		});
	}

	// 일일판매 상단 뷰 합계 조회
	public function getViewSaleSum(){
		
		$SALE_INFO_M = DB::table("SALE_INFO_M AS S")
						->select( "S.CORP_MK"
								 , DB::raw("CONVERT(CHAR(10), S.WRITE_DATE, 23) AS WRITE_DATE")
								 ,"S.CUST_MK"
								 ,"S.SEQ"
								 ,"S.CASH_AMT"
								 ,"S.CHECK_AMT"
								 ,"S.CARD_AMT"
								 ,"S.UNCL_AMT"
								 ,"S.TOTAL_SALE_AMT"
								 ,"C.FRNM"
								 ,"S.SALE_DISCOUNT"
								 ,"S.SALE_DISCOUNT_REMARK"
						)->join("CUST_CD AS C", function($join){
							$join->on("S.CUST_MK", "=", "C.CUST_MK");
							$join->on("S.CORP_MK", "=", "C.CORP_MK");
						})->join("SALE_INFO_D AS D ", function($join){
							$join->on("S.CORP_MK", "=", "D.CORP_MK");
							$join->on("S.WRITE_DATE", "=", "D.WRITE_DATE");
							$join->on("S.CUST_MK", "=", "D.CUST_MK");
							$join->on("S.SEQ", "=", "D.SEQ");
						})->where("S.CORP_MK",$this->getCorpId())
						->where("S.CUST_MK", urldecode(Request::Input("CUST_MK")))
						->where("S.SEQ", Request::Input("SEQ"))
						->where("S.WRITE_DATE", Request::Input("WRITE_DATE"))->first();
	
		return response()->json($SALE_INFO_M);
	}

	// 일일판매 합계 수정
	public function setUpdateViewSaleSum(){

		$validator = Validator::make( Request::Input(), [
			'CUST_MK'		=> 'required|max:20,NOT_NULL',
			'WRITE_DATE'	=> 'required|date_format:"Y-m-d',
			'WRITE_DATE1'	=> 'required|date_format:"Y-m-d',
			'CASH_AMT'		=> 'numeric',
			'CARD_AMT'		=> 'numeric',
			'UNCL_AMT'		=> 'numeric',
			'TOTAL_SALE_AMT'=> 'numeric',
			'SALE_DISCOUNT'	=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::beginTransaction();
		
		try{
			DB::table("SALE_INFO_M")
				->where("CUST_MK", Request::Input('CUST_MK'))
				->where("SEQ", Request::Input('SEQ'))
				->where("WRITE_DATE", Request::Input('WRITE_DATE'))
				->update(
					[
						'WRITE_DATE'			=> Request::Input("WRITE_DATE1"),
						'CASH_AMT'				=> Request::Input("CASH_AMT"),
						'CARD_AMT'				=> Request::Input("CARD_AMT"),
						'UNCL_AMT'				=> Request::Input("UNCL_AMT"),
						'TOTAL_SALE_AMT'		=> Request::Input("TOTAL_SALE_AMT"),
						'SALE_DISCOUNT_REMARK'	=> Request::Input("SALE_DISCOUNT_REMARK"),
						'SALE_DISCOUNT'			=> Request::Input("SALE_DISCOUNT"),
					]
				);

		}catch(ValidationException $e){
			DB::rollback();
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'DB_ERROR', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	// 일일판매 거래처 조회(목록형)
	public function getCustList(){
		
		$CUST_CD = DB::table("CUST_CD AS CUST")
						->select( "GRP.CORP_MK"
								 , "GRP.CUST_GRP_NM"
								 , "CUST.CUST_MK"
								 , "CUST.FRNM"
								 , "CUST.RPST"
								 , "CUST.PHONE_NO"
								 , "CUST.RANK"
								 , "CUST.ETPR_NO"
								 , DB::raw(" CASE WHEN CUST.RANK ='SU' THEN '우수'
										     WHEN CUST.RANK ='GE' THEN '일반'
										     WHEN CUST.RANK ='BD' THEN '불량' END AS RANK_NM ")
								 , "CUST.AREA_CD"
								 , "AREA.AREA_NM")
						->leftjoin("CUST_GRP_INFO AS GRP", function($join){
								$join->on("GRP.CUST_GRP_CD", "=", "CUST.CUST_GRP_CD");	
								$join->on("GRP.CORP_MK", "=", "CUST.CORP_MK");	
							}
						)->leftjoin( "AREA_CD AS AREA", function($join){
								$join->on("AREA.AREA_CD", "=", "CUST.AREA_CD");	
							}
						)->where( 'GRP.CORP_MK', $this->getCorpId());
		
		return Datatables::of($CUST_CD)
					
				->filter(function($query) {

					
					if( Request::Has('srtCondition') && Request::Input('srtCondition') != ''){
						$query->where("GRP.CUST_GRP_CD",  Request::Input('srtCondition') )
							->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
					}else{
						$query->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
					}

					if( Request::Has("CHK_ALL") && Request::Input('CHK_ALL') == '1'){
						$query->whereNotIn('CUST.CORP_DIV', ['J', 'A', 'P']);
					}else{
						$query->where( 'CUST.CORP_DIV', 'S');
					}
				
				})->make(true);
	
	}

	private function setSaleLog($new_sale, $old_sale, $flag, $new_sale_d, $old_sale_d){
		
		$SALE_LOG = $this->getMaxSeqSaleInfoM( date("Y-m-d") );
		$data =	[
					"CORP_MK"			=> $this->getCorpId(),	
					"LOG_DATE"			=> date("Y-m-d"),
					"LOG_SEQ"			=> (int)$SALE_LOG + 1,
					"LOG_DIV"			=> $flag,  // D : U 
					"MEM_ID"			=> $this->getCorpId(),
					"CUST_MK"			=> $old_sale->CUST_MK,
					"WRITE_DATE"		=> $old_sale->WRITE_DATE,
					"SEQ"				=> $old_sale->SEQ,
					"BEFORE_CASH_AMT"	=> $old_sale->CASH_AMT,
					"BEFORE_CHECK_AMT"	=> $old_sale->CHECK_AMT,
					"BEFORE_CARD_AMT"	=> $old_sale->CARD_AMT,
					"BEFORE_UNCL_AMT"	=> $old_sale->UNCL_AMT,
					"BEFORE_TOTAL_SALE_AMT" =>$old_sale->TOTAL_SALE_AMT,
			];
		
		
		if( $flag == "U"){
			$data["CASH_AMT"] = $new_sale->CASH_AMT ;
			$data["UNCL_AMT"] = $new_sale->UNCL_AMT ;
			$data["TOTAL_SALE_AMT"] = $new_sale->TOTAL_SALE_AMT ;
		}

		DB::table("SALE_INFO_LOG_M")->insert($data);
	
		$i = 1;
		$datail_old_data = [];
		foreach($old_sale_d as $d){
			
			$d_data = [
						"CORP_MK"		=> $data["CORP_MK"],
						"LOG_DATE"		=> $data["LOG_DATE"],
						"LOG_SEQ"		=> (int)$SALE_LOG + 1,
						"LOG_SEQ_SEQ"	=> $i++,
						"LOG_DIV"		=> "D",

						"WRITE_DATE"	=> $d->WRITE_DATE,
						"CUST_MK"		=> $d->CUST_MK,
						"SEQ"			=> $d->SEQ,
						"SRL_NO"		=> $d->SRL_NO,
						"PIS_MK"		=> $d->PIS_MK,
						"SIZES"			=> $d->SIZES,
						"INPUT_DATE"	=> $d->INPUT_DATE,
						"ORIGIN_CD"		=> $d->ORIGIN_CD,
						"ORIGIN_NM"		=> $d->ORIGIN_NM,
						"QTY"			=> $d->QTY,
						"UNCS"			=> $d->UNCS,
						"AMT"			=> $d->AMT,
						"REMARK"		=> $d->REMARK,
					];
			$datail_old_data[] = $d_data;
		}
		
		$datail_new_data = [];
		DB::table("SALE_INFO_LOG_D")->insert($datail_old_data);

		if( $flag != "D" || $new_sale_d != null){

			foreach($new_sale_d as $d){
				$d_data = [
							"CORP_MK"		=> $data["CORP_MK"],
							"LOG_DATE"		=> $data["LOG_DATE"],
							"LOG_SEQ"		=> (int)$SALE_LOG + 1,
							"LOG_SEQ_SEQ"	=> $i++,
							"LOG_DIV"		=> "U",

							"WRITE_DATE"	=> $d["WRITE_DATE"],
							"CUST_MK"		=> $d["CUST_MK"],
							"SEQ"			=> $d["SEQ"],
							"SRL_NO"		=> $d["SRL_NO"],
							"PIS_MK"		=> $d["PIS_MK"],
							"SIZES"			=> $d["SIZES"],
							"INPUT_DATE"	=> $d["INPUT_DATE"],
							"ORIGIN_CD"		=> $d["ORIGIN_CD"],
							"ORIGIN_NM"		=> $d["ORIGIN_NM"],
							"QTY"			=> $d["QTY"],
							"UNCS"			=> $d["UNCS"],
							"AMT"			=> $d["AMT"],
							"REMARK"		=> $d["REMARK"],
						];

				$datail_new_data[] = $d_data;
			}

			DB::table("SALE_INFO_LOG_D")->insert($datail_new_data);	
		}
	}
	
	// 총미수금 불러오기
	public function getTotalUncl(){

		$UNCL_INFO = DB::table( DB::raw("
										( SELECT  UNCL_CUST_MK
											  , CORP_MK
											  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL 
											  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
										   FROM UNCL_INFO 
										--  WHERE WRITE_DATE >= Request::Input('START_DATE')
										--    AND WRITE_DATE <= Request::Input('END_DATE')
									    GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
										) AS A
									")
								)
								->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.UNCL_CUST_MK', urldecode(Request::Input("UNCL_CUST_MK")))
								->groupBy("A.UNCL_CUST_MK","A.CORP_MK")->first();
		
		return response()->json($UNCL_INFO);
	}

	public function log($sale){
		return view("sale.log", [ "sale" => $sale] ); 
	}
	
	public function listLogM(){

		$SALE_LOG = DB::table("SALE_INFO_LOG_M AS M")
					->select(   "M.CORP_MK"
							  , DB::raw("CONVERT(CHAR(10), M.LOG_DATE, 23) AS LOG_DATE")
							  , "M.LOG_SEQ"
							  , "M.LOG_DIV"
							  , DB::raw("CASE WHEN M.LOG_DIV = 'U' THEN '수정' ELSE '삭제' END AS LOG_DIV_NM")
							  , "M.MEM_ID"
							  , "M.CUST_MK"
							  , DB::raw("CONVERT(CHAR(10), M.WRITE_DATE, 23) AS WRITE_DATE")
							  , "M.SEQ"
							  , "M.BEFORE_CASH_AMT"
							  , "M.BEFORE_CHECK_AMT"
							  , "M.BEFORE_CARD_AMT"
							  , "M.BEFORE_UNCL_AMT"
							  , "M.BEFORE_TOTAL_SALE_AMT"
							  , "M.CASH_AMT"
							  , "M.CHECK_AMT"
							  , "M.CARD_AMT"
							  , "M.UNCL_AMT"
							  , "M.TOTAL_SALE_AMT"
							  , "C.FRNM"
							)
					->join("CUST_CD AS C", function($join){
						$join->on("C.CORP_MK", "=", "M.CORP_MK");
						$join->on("C.CUST_MK", "=", "M.CUST_MK");							
					})->where("M.CORP_MK", $this->getCorpId());
	
		return Datatables::of($SALE_LOG)
			
				->filter(function($query) {
					
					if( Request::Has('LOG_DATE') ){
						$query->where("M.LOG_DATE",  Request::Input('LOG_DATE') );
					}

					if( Request::Has('LOG_SEQ') ){
						$query->where("M.LOG_SEQ",  Request::Input('LOG_SEQ') );
					}
				}
		)->make(true);
	}

	public function listLogD1(){

		$SALE_LOG_D1 = DB::table("SALE_INFO_LOG_D AS D")
					->select(  "D.CORP_MK"
							  ,DB::raw("CONVERT(CHAR(10), D.LOG_DATE, 120) AS LOG_DATE") 
							  ,"D.LOG_SEQ"
							  ,"D.LOG_SEQ_SEQ"
							  ,"D.LOG_DIV"
							  ,"D.WRITE_DATE"
							  ,"D.CUST_MK"
							  ,"D.SEQ"
							  ,"D.SRL_NO"
							  ,"D.PIS_MK"
							  ,"D.SIZES"
							  , DB::raw("CONVERT(CHAR(10), D.INPUT_DATE, 120) AS INPUT_DATE")
							  ,"D.ORIGIN_CD"
							  ,"D.ORIGIN_NM"
							  ,"D.QTY"
							  ,"D.UNCS"
							  ,"D.AMT"
							  ,"D.REMARK"
							  ,"P.PIS_NM"
							)
					->join("PIS_INFO AS P", function($join){
						$join->on("D.PIS_MK", "=", "P.PIS_MK");
						$join->on("D.CORP_MK", "=", "P.CORP_MK");
					})->where("D.CORP_MK", $this->getCorpId())
					->where("D.LOG_DIV", "D")
					->where(DB::raw("CONVERT(CHAR(10), D.LOG_DATE, 120)"), Request::Input('LOG_DATE'))
					->where("D.LOG_SEQ", Request::Input('LOG_SEQ'));
	
		return Datatables::of($SALE_LOG_D1)->make(true);
	}

	public function listLogD2(){

		$SALE_LOG_D1 = DB::table("SALE_INFO_LOG_D AS D")
					->select(  "D.CORP_MK"
							  ,DB::raw("CONVERT(CHAR(10), D.LOG_DATE, 120) AS LOG_DATE") 
							  ,"D.LOG_SEQ"
							  ,"D.LOG_SEQ_SEQ"
							  ,"D.LOG_DIV"
							  , DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 120) AS WRITE_DATE")
							  ,"D.CUST_MK"
							  ,"D.SEQ"
							  ,"D.SRL_NO"
							  ,"D.PIS_MK"
							  ,"D.SIZES"
							  , DB::raw("CONVERT(CHAR(10), D.INPUT_DATE, 120) AS INPUT_DATE")
							  ,"D.ORIGIN_CD"
							  ,"D.ORIGIN_NM"
							  ,"D.QTY"
							  ,"D.UNCS"
							  ,"D.AMT"
							  ,"D.REMARK"
							  ,"P.PIS_NM"
							)
					->join("PIS_INFO AS P", function($join){
						$join->on("D.PIS_MK", "=", "P.PIS_MK");
						$join->on("D.CORP_MK", "=", "P.CORP_MK");
					})->where("D.CORP_MK", $this->getCorpId())
					->where("D.LOG_DIV", "U")
					->where(DB::raw("CONVERT(CHAR(10), D.LOG_DATE, 120)"), Request::Input('LOG_DATE'))
					->where("D.LOG_SEQ", Request::Input('LOG_SEQ'));
	
		return Datatables::of($SALE_LOG_D1)->make(true);
	}

	public function getCustGrp(){
		$CUST_GRP = CUST_GRP_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($CUST_GRP);
	}

	public function getTblTempl(){
		return View('template.table', ['id' => Request::Input("id")]);
	}

	public function getSujoNo(){
		$objs_Data = Request::Input('arrData');
		$arrSujoList = [];

		foreach($objs_Data as $data){

			$SUJO = DB::table("STOCK_SUJO")
						->select("SUJO_NO")
						->where("CORP_MK", $this->getCorpId())
						->where("PIS_MK", $data['PIS_MK'])
						->where("SIZES",$data['SIZES'])
						->where("INPUT_DATE",$data['INPUT_DATE'])
						->where("ORIGIN_CD",$data['ORIGIN_CD'])
						->where("ORIGIN_NM",$data['ORIGIN_NM'])->get();
		
			if( count( $SUJO ) == 0 ){
				array_push($arrSujoList, [array('SUJO_NO' => 'RE', 'NEW_SUJO_NO' => substr(uniqid(''), -6))]);
			}else{
				array_push($arrSujoList,  $SUJO);
			}
		}
		return response()->json($arrSujoList);
	}

	// 판매일보 출력
	public function pdf()
	{
		$START_DATE	= Request::segment(4);
		$END_DATE	= Request::segment(5);
		$CORP_INFO	= DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->first();
		$SALE_INFO	= DB::table('SALE_INFO_M AS SM')
						->select( 'SM.CORP_MK'
								, DB::raw( "CONVERT(CHAR(10), SM.WRITE_DATE, 23) AS WRITE_DATE" )
								, 'SM.CUST_MK'
								, 'SM.SEQ'
								, 'SM.CASH_AMT'
								, 'SM.CHECK_AMT'
								, 'SM.UNCL_AMT'
								, 'SM.CARD_AMT'
								, 'SM.TOTAL_SALE_AMT'
								, 'C.FRNM'
								, DB::raw( "CASE SD.CNT WHEN 1 THEN P.PIS_NM ELSE P.PIS_NM + '외' + CONVERT (VARCHAR , (SD.CNT - 1)) + '종' END AS PIS")
								, 'SD.SUMQTY'
								, 'SM.SALE_DISCOUNT'
								, 'P.PIS_MK'
								//, DB::raw("CASE WHEN SD.SIZES = 'QTYS' THEN '마리' END AS SIZES")
						)->join(DB::raw(" 
									( SELECT CORP_MK, WRITE_DATE, CUST_MK, SEQ, COUNT(CORP_MK) AS CNT, SUM(QTY) AS SUMQTY, MAX(PIS_MK) AS PIS_MK
									  FROM SALE_INFO_D AS D 
									 WHERE CORP_MK = '".$this->getCorpId()."'
									 GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ) SD  ")
								, function($join){
									$join->on("SM.CORP_MK", "=", "SD.CORP_MK" );
									$join->on("SM.WRITE_DATE", "=" ,"SD.WRITE_DATE");
									$join->on("SM.CUST_MK", "=" ,"SD.CUST_MK");
									$join->on("SM.SEQ", "=" ,"SD.SEQ");			 
						})->join("CUST_CD AS C", function($join){
								$join->on("SM.CUST_MK", "=" ,"C.CUST_MK");
								$join->on("SM.CORP_MK", "=" ,"C.CORP_MK");	
						})->join("PIS_INFO AS P", function($join){
								$join->on("SD.PIS_MK", "=" ,"P.PIS_MK");
								$join->on("SD.CORP_MK", "=" ,"P.CORP_MK");

						})->where('SM.CORP_MK', $this->getCorpId())
						->whereBetween("SM.WRITE_DATE",  array( $START_DATE, $END_DATE ))
						->orderBy("SM.WRITE_DATE", "DESC")
						->where(function($query){

							$srtCondition	= urldecode(Request::segment(6));
							$textSearch		= urldecode(Request::segment(7));
							
							if( $textSearch == ""){
								if( $srtCondition != "ALL"){
									$query->where("C.CUST_GRP_CD",  "like", "%".$srtCondition."%" );
								}
							}else{
								if( $srtCondition == "FRNM"){
									$query->where("C.FRNM",  "like", "%".$textSearch."%" );

								}else if( $srtCondition != "" && $srtCondition == "PIS_NM"){
									$query->where("P.PIS_NM",  "like", "%".$textSearch."%");
								}else{
									$query->where(function($q) use ($srtCondition, $textSearch){
										$q->where("C.FRNM",  "like", "%".$textSearch."%" )
										->orwhere("P.PIS_NM",  "like", "%".$textSearch."%");
									});
								}
							}
						
						})
						->get();
		
		// 판매일보 합계
		$SALE_INFO_SUMMARY = DB::table('SALE_INFO_M AS SM')
					->select( DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1))-1) AS TUNCL")
							, DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1))-1) AS TSALE  ")
							, DB::raw("CONVERT(varchar(18),cast(SUM(SD.SUMQTY) as money),1) AS TQTY ")
							, DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1))-1) AS TCASH ")
							, DB::raw("SUM(SD.QTY_SUM) AS QTY_SUM")
							, DB::raw("SUM(SD.KG_SUM) AS KG_SUM")
					)->join(DB::raw(" (SELECT CORP_MK
										   , WRITE_DATE
										   , CUST_MK
										   , SEQ
										   , PIS_MK
										   , SUM(QTY) AS SUMQTY 
										   , SUM(CASE WHEN SIZES = 'QTYS' THEN QTY END) AS QTY_SUM
										   , SUM(CASE WHEN SIZES <> 'QTYS' THEN QTY END) AS KG_SUM
										FROM SALE_INFO_D 
									   WHERE CORP_MK = '".$this->getCorpId()."'
									   GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ, PIS_MK ) AS SD "), function($join){

							$join->on("SM.CORP_MK", "=", "SD.CORP_MK");
							$join->on("SM.WRITE_DATE", "=", "SD.WRITE_DATE");
							$join->on("SM.CUST_MK", "=", "SD.CUST_MK" );
							$join->on("SM.SEQ", "=", "SD.SEQ" );
						
					})->join("CUST_CD AS C", function($join){
							$join->on("SM.CUST_MK", "=" ,"C.CUST_MK");
							$join->on("SM.CORP_MK", "=" ,"C.CORP_MK");	
					})->join("PIS_INFO AS P", function($join){
							$join->on("SD.PIS_MK", "=" ,"P.PIS_MK");
							$join->on("SD.CORP_MK", "=" ,"P.CORP_MK");
					})
					->where ( 'SM.CORP_MK', $this->getCorpId())
					->where ( 'SM.WRITE_DATE', ">=", $START_DATE)
					->where ( 'SM.WRITE_DATE', "<=", $END_DATE)
					->where (function($query){
							
							$srtCondition	= urldecode(Request::segment(6));
							$textSearch		= urldecode(Request::segment(7));
							
							if( $textSearch == ""){
								if( $srtCondition != "ALL"){
									$query->where("C.CUST_GRP_CD",  "like", "%".$srtCondition."%" );
								}
							}else{
								if( $srtCondition == "FRNM"){
									$query->where("C.FRNM",  "like", "%".$textSearch."%" );

								}else if( $srtCondition != "" && $srtCondition == "PIS_NM"){
									$query->where("P.PIS_NM",  "like", "%".$textSearch."%");
								}else{
									$query->where(function($q) use ($srtCondition, $textSearch){
										$q->where("C.FRNM",  "like", "%".$textSearch."%" )
										->orwhere("P.PIS_NM",  "like", "%".$textSearch."%");
									});
								}
							}
						
					})
					->first();

		
		$qtySum		= 0; // 수량합계
		$saleSum	= 0; // 판매합계
		$inputSum	= 0; // 입금액(원)합계
		$unclSum	= 0; // 미입금액(원) 합계

		foreach($SALE_INFO as $info){
			$qtySum		+= (float)$info->SUMQTY;
			$saleSum	+= $info->TOTAL_SALE_AMT;
			$inputSum	+= $info->CASH_AMT;
			$unclSum	+= $info->UNCL_AMT;
		}
		
		//dd($SALE_INFO_SUMMARY);

		$pdf = PDF::loadView("sale.Pdf", 
								[
									'model'		=> $SALE_INFO,
									'title'		=> "판   매   일   보((".$CORP_INFO->FRNM."))",
									'start'		=> $START_DATE,
									'end'		=> $END_DATE,
									'qtySum'	=> $qtySum,
									'saleSum'	=> $saleSum,
									'inputSum'	=> $inputSum,
									'unclSum'	=> $unclSum,
									'summary'	=> $SALE_INFO_SUMMARY,
								]
							);

		return $pdf->stream();

	}

	// 거래명세서 출력
	public function PdfDetail(){
		
		$CUST_MK	= urldecode(Request::segment(4));
		$SEQ		= Request::segment(5);
		$WRITE_DATE	= Request::segment(6);

		$SALE_INFO = DB::table("SALE_INFO_D AS D")
						
						->leftjoin("STOCK_INFO AS SD", function($join){
							$join->on("D.CORP_MK", "=", "SD.CORP_MK");
							$join->on("D.STOCK_INFO_SEQ", "=", "SD.SEQ");
							$join->on("D.CUST_MK", "=", "SD.CUST_MK");
							$join->on("D.WRITE_DATE", "=", "SD.OUTPUT_DATE");
							$join->on("D.WRITE_DATE", "=", "SD.OUTPUT_DATE");
							$join->on("D.PIS_MK", "=", "SD.PIS_MK");
							$join->on("D.SIZES", "=", "SD.SIZES");
							
						})
						->select("P.PIS_NM"
								, "D.CORP_MK"
								, DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
								, "D.ORIGIN_NM"
								, "D.ORIGIN_CD"
								, "D.CUST_MK"
								, "D.SEQ"
								, "D.PIS_MK"
								, "D.SIZES"
								, "D.QTY"
								, "D.UNCS"
								, "D.AMT"
								, "D.SRL_NO"
								, "SD.SUJO_NO"
								, "D.REMARK")
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");	
								$join->on("D.CORP_MK", "=" ,"P.CORP_MK");
							}
						)->where( 'D.CORP_MK', $this->getCorpId())
						->where( 'D.CUST_MK', $CUST_MK)
						->where( 'D.SEQ', $SEQ)
						->where( 'D.WRITE_DATE', $WRITE_DATE)
						->get();
		
		
		$SALE_SUM = DB::table("SALE_INFO_M AS S")
						->select( "S.CORP_MK"
								 , DB::raw("CONVERT(CHAR(10), S.WRITE_DATE, 23) AS WRITE_DATE")
								 ,"S.CUST_MK"
								 ,"S.SEQ"
								 ,"S.CASH_AMT"
								 ,"S.CHECK_AMT"
								 ,"S.CARD_AMT"
								 ,"S.UNCL_AMT"
								 ,"S.TOTAL_SALE_AMT"
								 //, DB::raw("ISNULL( SUM(D.QTY), 0) AS SUM_QTY")
								 ,"C.FRNM"
								 ,"S.SALE_DISCOUNT"
								 ,"S.SALE_DISCOUNT_REMARK"
						)->join("CUST_CD AS C", function($join){
							$join->on("S.CUST_MK", "=", "C.CUST_MK");
							$join->on("S.CORP_MK", "=", "C.CORP_MK");
						})->join("SALE_INFO_D AS D ", function($join){
							$join->on("S.CORP_MK", "=", "D.CORP_MK");
							$join->on("S.WRITE_DATE", "=", "D.WRITE_DATE");
							$join->on("S.CUST_MK", "=", "D.CUST_MK");
							$join->on("S.SEQ", "=", "D.SEQ");
						})->where("S.CORP_MK",$this->getCorpId())
						->where("S.CUST_MK", $CUST_MK)
						->where("S.SEQ", $SEQ)
						->where("S.WRITE_DATE", $WRITE_DATE )->first();
		
		$CORP_INFO = DB::table("CORP_INFO")
						->where("CORP_MK", $this->getCorpId())
						->first();
		
		$CUST_INFO = DB::table("CUST_CD")
						->where("CORP_MK", $this->getCorpId())
						->where("CUST_MK", $CUST_MK)
						->first();

		
		$UNCL_INFO = DB::table( DB::raw("
							( SELECT  UNCL_CUST_MK
								  , CORP_MK
								  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL 
								  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
							   FROM UNCL_INFO 
							GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
							) AS A
						")
					)
					->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
					->where ( 'A.CORP_MK', $this->getCorpId())
					->where ( 'A.UNCL_CUST_MK', $CUST_MK)
					->groupBy("A.UNCL_CUST_MK","A.CORP_MK")
					->first();
		
		// 미수금 정보가 없을 수도 있으므로 UNCL_INFO 가 null 일경우 전체미수금, 이전미수금은 0 처리 함
		$totalUncl	= 0;
		$preUncl	= 0;

		if( $UNCL_INFO  != null){
			$totalUncl	= $UNCL_INFO->TOTAL_UNCL;
			$preUncl	= (int)$UNCL_INFO->TOTAL_UNCL - (int)$SALE_SUM->UNCL_AMT;
		}

		$sum_qty = 0;

		if( $SALE_INFO != null && count($SALE_INFO) > 0){
			
			foreach($SALE_INFO as $sale){
				$sum_qty += $sale->QTY;
			}
		}
		$pdf = PDF::loadView("sale.PdfSaleDetail", 
								[
									'model'		=> $SALE_INFO,
									'sum'		=> $SALE_SUM,
									'sum_qty'	=> $sum_qty,
									'corp'		=> $CORP_INFO,
									'cust'		=> $CUST_INFO,
									'totalUncl'	=> $totalUncl,
									'preUncl'	=> $preUncl,
								]
							)->setPaper('a4', 'landscape');

		return $pdf->stream("거래명세서.pdf");
	}


	// 거래명세서 선택출력
	// 조아수산 요청( 2017-03-13)
	public function PdfDetailSelected(){
		
		$CUST_MK	= urldecode(Request::segment(4));
		$SEQ		= Request::segment(5);
		$WRITE_DATE	= Request::segment(6);
		$END_DATE	= Request::segment(7);
		
		$arrCUST_MK	= explode(',', trim($CUST_MK));
		$arrSEQ		= explode(',', trim($SEQ));
		//dd($arrSEQ,$arrCUST_MK); 
		
		$i = 0;
		foreach($arrSEQ as $seq){ 
			
			$SALE_INFO[$i] = DB::table("SALE_INFO_D AS D")
							->leftjoin("STOCK_INFO AS SD", function($join){
								$join->on("D.CORP_MK", "=", "SD.CORP_MK");
								$join->on("D.STOCK_INFO_SEQ", "=", "SD.SEQ");
								$join->on("D.CUST_MK", "=", "SD.CUST_MK");
								$join->on("D.WRITE_DATE", "=", "SD.OUTPUT_DATE");
								$join->on("D.WRITE_DATE", "=", "SD.OUTPUT_DATE");
								$join->on("D.PIS_MK", "=", "SD.PIS_MK");
								$join->on("D.SIZES", "=", "SD.SIZES");
								
							})
							->select("P.PIS_NM"
									, "D.CORP_MK"
									, DB::raw("CONVERT(CHAR(10), D.WRITE_DATE, 23) AS WRITE_DATE")
									, "D.ORIGIN_NM"
									, "D.ORIGIN_CD"
									, "D.CUST_MK"
									, "D.SEQ"
									, "D.PIS_MK"
									, "D.SIZES"
									, "D.QTY"
									, "D.UNCS"
									, "D.AMT"
									, "D.SRL_NO"
									, "SD.SUJO_NO"
									, "D.REMARK")
							->join("PIS_INFO AS P", function($join){
									$join->on("D.PIS_MK", "=", "P.PIS_MK");	
									$join->on("D.CORP_MK", "=" ,"P.CORP_MK");
								}
							)->where( 'D.CORP_MK', $this->getCorpId())
							->where( 'D.CUST_MK', $arrCUST_MK[$i])
							->where( 'D.SEQ', $seq)
							->where( 'D.WRITE_DATE', ">=",  $WRITE_DATE)
							->where( 'D.WRITE_DATE', "<=",  $END_DATE)
							->get();

			
			$SALE_SUM[$i] = DB::table("SALE_INFO_M AS S")
							->select( "S.CORP_MK"
									 , DB::raw("CONVERT(CHAR(10), S.WRITE_DATE, 23) AS WRITE_DATE")
									 ,"S.CUST_MK"
									 ,"S.SEQ"
									 ,"S.CASH_AMT"
									 ,"S.CHECK_AMT"
									 ,"S.CARD_AMT"
									 ,"S.UNCL_AMT"
									 ,"S.TOTAL_SALE_AMT"
									 //, DB::raw("ISNULL( SUM(D.QTY), 0) AS SUM_QTY")
									 ,"C.FRNM"
									 ,"S.SALE_DISCOUNT"
									 ,"S.SALE_DISCOUNT_REMARK"
							)->join("CUST_CD AS C", function($join){
								$join->on("S.CUST_MK", "=", "C.CUST_MK");
								$join->on("S.CORP_MK", "=", "C.CORP_MK");
							})->join("SALE_INFO_D AS D ", function($join){
								$join->on("S.CORP_MK", "=", "D.CORP_MK");
								$join->on("S.WRITE_DATE", "=", "D.WRITE_DATE");
								$join->on("S.CUST_MK", "=", "D.CUST_MK");
								$join->on("S.SEQ", "=", "D.SEQ");
							})->where("S.CORP_MK",$this->getCorpId())
							->where("S.CUST_MK", $arrCUST_MK[$i])
							->where("S.SEQ", $seq)
							->where("S.WRITE_DATE", ">=", $WRITE_DATE )
							->where("S.WRITE_DATE", "<=",$END_DATE )
							->first();


			$CORP_INFO = DB::table("CORP_INFO")
							->where("CORP_MK", $this->getCorpId())
							->first();
			
			$CUST_INFO[$i] = DB::table("CUST_CD")
							->where("CORP_MK", $this->getCorpId())
							->where("CUST_MK", $arrCUST_MK[$i])
							->first();
			
			$UNCL_INFO[$i] = DB::table( DB::raw("
								( SELECT  UNCL_CUST_MK
									  , CORP_MK
									  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL 
									  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
								   FROM UNCL_INFO 
								GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
								) AS A
							")
						)
						->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
						->where ( 'A.CORP_MK', $this->getCorpId())
						->where ( 'A.UNCL_CUST_MK', $arrCUST_MK[$i])
						->groupBy("A.UNCL_CUST_MK","A.CORP_MK")
						->first();
			
			// 미수금 정보가 없을 수도 있으므로 UNCL_INFO 가 null 일경우 전체미수금, 이전미수금은 0 처리 함
			$totalUncl;
			$preUncl;
			
			if( $UNCL_INFO[$i] != null){
				$totalUncl[$i]	= $UNCL_INFO[$i]->TOTAL_UNCL;
				$preUncl[$i]	= ((int)$UNCL_INFO[$i]->TOTAL_UNCL - (int)$SALE_SUM[$i]->UNCL_AMT);
			}else{
				$totalUncl[$i]	= 0;
				$preUncl[$i]	= - (int)$SALE_SUM[$i]->UNCL_AMT;
			}

			$sum_qty[$i] = 0;

			if( $SALE_INFO[$i] != null && count($SALE_INFO[$i]) > 0){
				
				foreach($SALE_INFO[$i] as $sale){
					$sum_qty[$i] += $sale->QTY;
				}
			}
			$i++;
		}
		
		$pdf = PDF::loadView("sale.pdfSaleDetailSelected", 
								[
									'models'	=> $SALE_INFO,
									'sums'		=> $SALE_SUM,
									'sum_qtys'	=> $sum_qty,
									'corp'		=> $CORP_INFO,
									'custs'		=> $CUST_INFO,
									'totalUncls'=> $totalUncl,
									'preUncls'	=> $preUncl,
								]
							)->setPaper('a4', 'landscape');

		return $pdf->stream("거래명세서.pdf");
	}


	// 판매정보 최대 SEQ 값 조회
	private function getMaxSeqInSALEINFO_M($pWRITE_DATE, $pCUST_MK){
		
		return $max_seq = SALE_INFO_M::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									//->where ("CUST_MK", $pCUST_MK)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
	}


	// 재고정보 최대 SEQ 값 조회
	private function getMaxSeqInSTOCKINFO($pSIZES, $pPIS_MK){
		
		return $max_seq = STOCK_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("PIS_MK", $pPIS_MK)
									->where ("SIZES", $pSIZES)
									->first()->SEQ;
	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNCLINFO($pWRITE_DATE){
		
		return $max_seq = UNCL_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
	}

	// 판매로그M seq 값 얻기
	private function getMaxSeqSaleInfoM($pWRITE_DATE){
		
		return $max_seq = DB::table("SALE_INFO_LOG_M")
					->select( DB::raw('ISNULL(max("LOG_SEQ"), 0) AS LOG_SEQ') )
					->where ("CORP_MK", $this->getCorpId())
					->where ("LOG_DATE", $pWRITE_DATE)
					->first()->LOG_SEQ;
	
	}
}
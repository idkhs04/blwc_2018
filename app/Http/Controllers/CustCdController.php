<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\AREA_CD;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class CustCdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{
		
		$CUST_CD = DB::table("CUST_CD AS CUST")
				->select(
						   "CUST.CUST_MK"
						  ,"CUST.CORP_DIV"
						  ,"CUST.FRNM"
						  ,"CUST.RPST"
						  ,"CUST.RANK"
						  ,"CUST.ETPR_NO"
						  ,"CUST.JUMIN_NO"
						  ,"CUST.PHONE_NO"
						  ,"CUST.FAX"
						  ,"CUST.ADDR1"
						  ,"CUST.ADDR2"
						  ,"CUST.POST_NO"
						  ,"CUST.UPJONG"
						  ,"CUST.UPTE"
						  ,"CUST.AREA_CD"
						  ,"CUST.REMARK"
						  ,"CUST.ADDR_3"
						  ,"CUST.EMAIL"
						  ,"CUST.WRITE_DT"
						  ,"CUST.INTERFACE_YN"
						  ,"GRP.*"
						  ,"AREA.AREA_NM"
						  , DB::raw("CONVERT(CHAR(10), CUST.WRITE_DT, 23) AS WRITE_DATE")
							//, DB::raw("(SELECT TOP 1 FROM UNPROV_INFO WHERE CORP_MK = '$this->getCorpId()' AND CUST_MK )")	
						)
				->join('CUST_GRP_INFO AS GRP', function($join){
					$join->on("CUST.CORP_MK", "=" ,"GRP.CORP_MK");
					$join->on("CUST.CUST_GRP_CD", "=" ,"GRP.CUST_GRP_CD");
				})->join('AREA_CD AS AREA', function($join){
					$join->on("CUST.AREA_CD", "=" ,"AREA.AREA_CD");
				
				})
				->where("CUST.CORP_MK", $this->getCorpId());
				
						
		return Datatables::of($CUST_CD)
				->filter(function($query) {
					
					if( Request::Has('srtCondition1') && Request::Input('srtCondition1') != "ALL"){
						$query->where("GRP.CUST_GRP_CD",  Request::Input('srtCondition1'));
					}

					if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRNM"){
						$query->where("FRNM",  "like", "%".Request::Input('textSearch')."%");

					}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_NM"){
						$query->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%");

					}else if(Request::Has('srtCondition') && Request::Input('srtCondition') == "RPST"){
						$query->where("RPST",  "like", "%".Request::Input('textSearch')."%");	

					}else{
						$query->where(function($q){
							$q->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
							->orwhere("FRNM",  "like", "%".Request::Input('textSearch')."%")
							->orwhere("RPST",  "like", "%".Request::Input('textSearch')."%");
						});
					}

					if( Request::Has('cust_grp_cd') && Request::Input('cust_grp_cd') != "ALL"){	
						$query->where("GRP.CUST_GRP_CD",  Request::Input('cust_grp_cd'));
					}
					
					if( Request::Has('corp_divNo')){
						$query->where("CORP_DIV",  "<>", Request::Input('corp_divNo'));
					}
					if( Request::Has('corp_div') ){
						$query->where("CORP_DIV",  Request::Input('corp_div'));
					}

				}
		)->make(true);
	}

	//거래처 그룹 리스트
	public function getCustGrp()
	{	
		$CUST_GRP_INFO = CUST_GRP_INFO::where("CORP_MK", $this->getCorpId());
		return Datatables::of($CUST_GRP_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("CUST_GRP_CD",  "like", "%".Request::Input('textSearch')."%");
					}
				}
		)->make(true);
	}

	//지역 리스트
	public function getAreaCd()
	{	
		if( Request::Has('textSearch') ){
			$AREA_CD = DB::table("AREA_CD")
						->select(DB::raw(" CONVERT(VARCHAR, AREA_CD) AS AREA_CD") , 'AREA_NM')
						->where("AREA_NM",  "like", "%".Request::Input('textSearch')."%");
		}else{
			$AREA_CD = DB::table("AREA_CD")
						->select('AREA_CD', 'AREA_NM');
		}

		return Datatables::of($AREA_CD)->make(true);
	}

	public function index($cust)
	{
		$CUST_GRP_INFO = CUST_GRP_INFO::where("CORP_MK", $this->getCorpId())->get();

		return view("custCd.list",[ "cust" => $cust, 'custGrp' => $CUST_GRP_INFO ] );
	}

	public function log($cust){
		return view("custCd.log",[ "cust" => $cust ] );
	}

	public function listLog()
    {

        $CUST_CD_LOG = DB::table("CUST_CD_LOG")
								->select(
									 'IDX'
									,DB::raw("CASE WHEN TYPE = 'C' THEN '추가'
													WHEN TYPE = 'D' THEN '삭제'
													WHEN TYPE = 'U' THEN '수정'
													ELSE '에러' END AS TYPE_NM
									")
									,'CUST_MK' 
									,'FRNM' 
									,'RPST'
									,'ETPR_NO'
									,'PHONE_NO'
									,'RANK'
									,'WRITE_DT' 
									,'UPDATE_DT' 
									,'DELETE_DT' 
								)
								->where("CORP_MK", $this->getCorpId());
						
		return Datatables::of($CUST_CD_LOG)->make(true);
    }


	// 거래처 등록
	public function insert($cust)
	{
		$validator = Validator::make( Request::Input(), [
			//'CUST_MK' => 'required|max:20',
			'CUST_GRP_CD' => 'required|max:20',
			//'CUST_GRP_NM' => 'required|max:20',
			'CORP_DIV' => 'required|max:20',
			'FRNM' => 'required|max:28',
			'RPST' => 'max:20',
			'ETPR_NO' => 'min:10|max:10',	//'ETPR_NO' => 'required|min:10|max:10',
			'JUMIN_NO' => 'date_format:"Y-m-d',			// 'JUMIN_NO' => 'required|max:20',
			'PHONE_NO' => 'max:13',			//'PHONE_NO' => 'required|max:20',
			'UPTE' => 'max:20',
			'FAX' => 'max:13',				// 'FAX' => 'required|max:20',
			'UPJONG' => 'max:20',
			'AREA_CD' => 'required|max:3',			//'AREA_CD' => 'required|max:20',
			//'RANK' => 'max:20',				// 'RANK' => 'required|max:20',
			'POST_NO' => 'max:7',
			'ADDR1' => 'max:100',
			'ADDR2' => 'max:100',
			'REMARK' => 'max:100',			// 'REMARK' => 'required|max:20'
			'WRITE_DATE' => 'date_format:"Y-m-d',			
			'EMAIL' => 'email|max:100',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지

		$ALL_CNT = DB::table("CUST_CD")->where('CORP_MK', $this->getCorpId())->count();

		$CUST_CD = DB::table("CUST_CD")
						->where('CORP_MK', $this->getCorpId())
						->where('CUST_GRP_CD', Request::Input("CUST_GRP_CD"))
						->select(DB::raw("ISNULL(COUNT(*), 0) AS CNT"))->first();
		
		
		$CUST_MK = rtrim(Request::input('CUST_GRP_CD')).$ALL_CNT."E".((int)$CUST_CD->CNT + 1);
		
		DB::beginTransaction();
        try {
            DB::table("CUST_CD")->insert(
                [
                    'CORP_MK' => $this->getCorpId(),
                    'CUST_MK' => $CUST_MK,
                    'CUST_GRP_CD' => Request::input('CUST_GRP_CD'),
					'CORP_DIV' => Request::input('CORP_DIV'),
					'FRNM' => Request::input('FRNM'),
					'RPST' => Request::input('RPST'),
					//'RANK' => Request::input('RANK'),GE
					'RANK' => 'GE',
					'ETPR_NO' => Request::input('ETPR_NO'),
					'JUMIN_NO' => Request::input('JUMIN_NO'),
					'PHONE_NO' => Request::input('PHONE_NO'),
					'FAX' => Request::input('FAX'),
					'ADDR1' => Request::input('ADDR1'),
					'ADDR2' => Request::input('ADDR2'),
					'POST_NO' => ( Request::input('POST_NO') == "" || Request::input('POST_NO') === null ) ? DB::raw("null") : Request::input('POST_NO') ,
					'UPJONG' => Request::input('UPJONG'),
					'UPTE' => Request::input('UPTE'),
					'AREA_CD' => Request::input('AREA_CD'),
					'REMARK' => Request::input('REMARK'),
					'ADDR_3' => DB::raw('NULL'),
					'WRITE_DT' => Request::input('WRITE_DATE'),
                    'EMAIL' => Request::input('EMAIL'),
					'INTERFACE_YN'=> 'Y',
                ]
            );
        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }


	// 선택된 거래처 수정
	public function edit($cust, $id)
	{	
		$model = DB::table("CUST_CD")
			->join('CUST_GRP_INFO', function($join){
				$join->on("CUST_CD.CORP_MK", "=" ,"CUST_GRP_INFO.CORP_MK");
				$join->on("CUST_CD.CUST_GRP_CD", "=" ,"CUST_GRP_INFO.CUST_GRP_CD");
			})
			->join('AREA_CD','AREA_CD.AREA_CD','=','CUST_CD.AREA_CD')
            ->where("CUST_CD.CORP_MK", $this->getCorpId())
            ->where("CUST_CD.CUST_MK", $id)
			->select(
				 "CUST_CD.*"
				,"CUST_GRP_INFO.*"
				,"AREA_CD.*"
				, DB::raw("CONVERT(CHAR(10), CUST_CD.WRITE_DT, 23) AS WRITE_DATE")
			)->first();

		return response()->json(
			[
				'CORP_MK' => $model->CORP_MK, 
				'CUST_MK' => $model->CUST_MK,
				'CUST_GRP_CD' => $model->CUST_GRP_CD,
				'CORP_DIV' => $model->CORP_DIV,
				'FRNM' => $model->FRNM,
				'RPST' => $model->RPST,
				//'RANK' => $model->RANK,
				'ETPR_NO' => $model->ETPR_NO,
				'JUMIN_NO' => $model->JUMIN_NO,
				'PHONE_NO' => $model->PHONE_NO,
				'FAX' => $model->FAX,
				'ADDR1' => $model->ADDR1,
				'ADDR2' => $model->ADDR2,
				'POST_NO' => $model->POST_NO,
				'UPJONG' => $model->UPJONG,
				'UPTE' => $model->UPTE,
				'AREA_CD' => $model->AREA_CD,
				'REMARK' => $model->REMARK,
				'CUST_GRP_NM' => $model->CUST_GRP_NM,
				'AREA_NM' => $model->AREA_NM,
				'WRITE_DATE' => $model->WRITE_DATE,
				'EMAIL' => $model->EMAIL,
				'INTERFACE_YN'=> $model->INTERFACE_YN,
			]
		);
	}

	//거래처 수정
    public function update($cust)
    {
		$validator = Validator::make( Request::Input(), [
            'CUST_MK_EDIT' => 'required|max:20',
            'CUST_GRP_CD_EDIT' => 'required|max:20',
			'CORP_DIV_EDIT' => 'required|max:20',
			'FRNM_EDIT' => 'required|max:28',
			'RPST_EDIT' => 'max:20',
			'ETPR_NO_EDIT' => 'min:10|max:10',
			'JUMIN_NO_EDIT' => 'date_format:"Y-m-d',
			'PHONE_NO_EDIT' => 'max:13',
			'UPTE_EDIT' => 'max:20',
			'FAX_EDIT' => 'max:13',
			'UPJONG_EDIT' => 'max:20',
			'AREA_CD_EDIT' => 'required|max:3',
			'POST_NO_EDIT' => 'max:20',
			'ADDR1_EDIT' => 'max:100',
			'ADDR2_EDIT' => 'max:100',
			'REMARK_EDIT' => 'max:100',
			'WRITE_DATE_EDIT' => 'date_format:"Y-m-d',			
			'EMAIL_EDIT' => 'email|max:100',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		//DB::beginTransaction();
		try {
			
			DB::table("CUST_CD")
				->where("CORP_MK", $this->getCorpId())
				->where("CUST_MK", Request::input('CUST_MK_EDIT'))
				->update(
					[
						'CORP_DIV' => Request::input('CORP_DIV_EDIT'),
						'FRNM' => Request::input('FRNM_EDIT'),
						'RPST' => Request::input('RPST_EDIT'),
						'ETPR_NO' => Request::input('ETPR_NO_EDIT'),
						'JUMIN_NO' => Request::input('JUMIN_NO_EDIT'),
						'PHONE_NO' => Request::input('PHONE_NO_EDIT'),
						'FAX' => Request::input('FAX_EDIT'),
						'ADDR1' => Request::input('ADDR1_EDIT'),
						'ADDR2' => Request::input('ADDR2_EDIT'),
						'POST_NO' => ( Request::input('POST_NO_EDIT') == "" || Request::input('POST_NO_EDIT') === null ) ? DB::raw("null") : Request::input('POST_NO_EDIT') ,
						'UPJONG' => Request::input('UPJONG_EDIT'),
						'UPTE' => Request::input('UPTE_EDIT'),
						'AREA_CD' => Request::input('AREA_CD_EDIT'),
						'REMARK' => Request::input('REMARK_EDIT'),
						'WRITE_DT' => Request::input('WRITE_DATE_EDIT'),
						'EMAIL' => Request::input('EMAIL_EDIT'),
						'CUST_GRP_CD'=>Request::input('CUST_GRP_CD_EDIT'),
					]
				);
			//DB::commit();

		}catch(Exception $e){
			DB::rollback();
			throw $e;
		}

		return response()->json(['result' => 'success']);
	}

	//거래처 미지급금 메모 수정
    public function updateMemo($cust)
    {
		$validator = Validator::make( Request::Input(), [
			'CUST_MK' => 'required|max:20',
			'CUST_UNPROV_MEMO' => 'max:100',
		]);
			
		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		//DB::beginTransaction();
		try {
			
			DB::table("CUST_CD")
				->where("CORP_MK", $this->getCorpId())
				->where("CUST_MK", Request::input('CUST_MK'))
				->update(
					[
						'ADDR_3' => Request::input('CUST_UNPROV_MEMO')
					]
				);
			//DB::commit();

		}catch(Exception $e){
			DB::rollback();
			throw $e;
		}
		return response()->json(['result' => 'success']);
	}

	//거래처 미수금 메모 수정
    public function updateMemo1($cust)
    {
		$validator = Validator::make( Request::Input(), [
			'CUST_MK' => 'required|max:20',
			'CUST_UNCL_MEMO' => 'max:100',
		]);
			
		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		//DB::beginTransaction();
		try {
			
			DB::table("CUST_CD")
				->where("CORP_MK", $this->getCorpId())
				->where("CUST_MK", Request::input('CUST_MK'))
				->update(
					[
						'UNCL_MEMO' => Request::input('CUST_UNCL_MEMO')
					]
				);
			//DB::commit();

		}catch(Exception $e){
			DB::rollback();
			throw $e;
		}
		return response()->json(['result' => 'success']);
	}

	public function _delete()
	{

		//return $exception = DB::transaction(function(){
			try{
				
				$cntStock	= DB::table("STOCK_INFO")->where("CORP_MK", $this->getCorpId())->where("CUST_MK",Request::get('cd') )->count();
				$cntSale	= DB::table("SALE_INFO_M")->where("CORP_MK", $this->getCorpId())->where("CUST_MK",Request::get('cd') )->count();
				$cntUncl	= DB::table("UNCL_INFO")->where("CORP_MK", $this->getCorpId())->where("UNCL_CUST_MK",Request::get('cd') )->count();
				$cntUnporv	= DB::table("UNPROV_INFO")->where("CORP_MK", $this->getCorpId())->where("PROV_CUST_MK",Request::get('cd') )->count();
				$cntTax		= DB::table("TAX_ACCOUNT_M")->where("CORP_MK", $this->getCorpId())->where("CUST_MK",Request::get('cd') )->count();
				$cntChit	= DB::table("CHIT_INFO")->where("CORP_MK", $this->getCorpId())->where("CUST_MK",Request::get('cd') )->count();

				if( ($cntStock + $cntSale + $cntUncl + $cntUnporv + $cntTax + $cntChit) == 0){
					if (Request::ajax()) {

						CUST_CD::where("CORP_MK", $this->getCorpId())
							->where("CUST_MK", Request::get('cd'))
							->delete();

						return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
					}
				}else{
					return response()->json(['result' => 'fail', 'message' => $errors], 422);
				}
			}catch(ValidationException $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}catch(Exception $e){
				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}
		//});
	}

	public function Excel()
	{
		// 컬럼을 지정해줘야 에러가 안남
		$CUST_CD = CUST_CD::select(
						  
						   "GRP.CUST_GRP_NM AS 거래처그룹"
						  ,"CUST_CD.FRNM AS 거래처명"
						  ,"CUST_CD.RPST AS 대표자"
						  //,"CUST_CD.RANK AS "
						  ,"CUST_CD.ETPR_NO AS 사업자번호"
						  ,"CUST_CD.PHONE_NO AS 전화번호"
						  ,"CUST_CD.FAX AS 팩스번호"
						  ,"CUST_CD.ADDR1 AS 주소"
						  ,"CUST_CD.ADDR2 AS 상세주소"
						  ,"CUST_CD.POST_NO AS 우편번호"
						  ,"CUST_CD.UPJONG AS 업종"
						  ,"CUST_CD.UPTE AS 업태"
						  ,"CUST_CD.EMAIL AS 이메일"
						  ,"AREA.AREA_NM AS 지역"
						  ,"CUST_CD.REMARK AS 비고"
						  //,"CUST_CD.WRITE_DT AS 입력일자"
						  , DB::raw("CONVERT(CHAR(10), CUST_CD.WRITE_DT, 23) AS 입력일자")
							//, DB::raw("(SELECT TOP 1 FROM UNPROV_INFO WHERE CORP_MK = '$this->getCorpId()' AND CUST_MK )")	
						)
				->join('CUST_GRP_INFO AS GRP', function($join){
					$join->on("CUST_CD.CORP_MK", "=" ,"GRP.CORP_MK");
					$join->on("CUST_CD.CUST_GRP_CD", "=" ,"GRP.CUST_GRP_CD");
				})->join('AREA_CD AS AREA', function($join){
					$join->on("CUST_CD.AREA_CD", "=" ,"AREA.AREA_CD");
				})
				->where("CUST_CD.CORP_MK", $this->getCorpId())
				->where(function($query) {
				
					if( Request::Has('srtCondition1') && Request::Input('srtCondition1') != "ALL"){
						$query->where("GRP.CUST_GRP_CD",  Request::Input('srtCondition1'));
					}

					if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRNM"){
						$query->where("FRNM",  "like", "%".Request::Input('textSearch')."%");

					}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CUST_GRP_NM"){
						$query->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%");

					}else if(Request::Has('srtCondition') && Request::Input('srtCondition') == "RPST"){
						$query->where("RPST",  "like", "%".Request::Input('textSearch')."%");	

					}else{
						$query->where(function($q){
							$q->where("CUST_GRP_NM",  "like", "%".Request::Input('textSearch')."%")
							->orwhere("CUST_CD.FRNM",  "like", "%".Request::Input('textSearch')."%")
							->orwhere("CUST_CD.RPST",  "like", "%".Request::Input('textSearch')."%");
						});
					}

					if( Request::Has('cust_grp_cd') && Request::Input('cust_grp_cd') != "ALL"){	
						$query->where("GRP.CUST_GRP_CD",  Request::Input('cust_grp_cd'));
					}
					
					if( Request::Has('corp_divNo')){
						$query->where("CUST_CD.CORP_DIV",  "<>", Request::Input('corp_divNo'));
					}
					if( Request::Has('corp_div') ){
						$query->where("CUST_CD.CORP_DIV",  Request::Input('corp_div'));
					}
				})->get();

		// $CORP = DB::table("CORP_IM"
		Excel::create('거래처', function ($excel) use ($CUST_CD) {

			$excel->sheet('거래처', function ($sheet) use ($CUST_CD) {
				$sheet->fromArray($CUST_CD);
			});
		})->export('xls');
	}

}
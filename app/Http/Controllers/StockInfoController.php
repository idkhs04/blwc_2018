<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\STOCK_SUJO;
use App\PIS_INFO;
use App\ORIGIN_CD;
use App\PIS_CLASS_CD;
use App\STOCK_INFO;
use App\UNPROV_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use DateTime;
use Watson\Validating\ValidationException;

use Barryvdh\DomPDF\wrapper;


class StockInfoController extends Controller
{
    //
	public function index($stock)
	{
		return view("stock.list", [ "stock" => $stock] );
	}

	public function detail($stock, $PIS_MK, $SIZES)
	{
	
		return view("stock.detail", [ "stock" => $stock, "PIS_MK" => $PIS_MK, "SIZES" =>  $SIZES] );
	}

	public function listPis($stock){
		
		return view("stock.listPis", [ "stock" => $stock] );
	
	}

	public function listAllData($stock){
		

		return view("stock.listAll", [ "stock" => $stock] );
	
	}


	public function listDataPisData(){
		
		$STOCK_INFO = DB::table("STOCK_INFO AS ST")
						->join("PIS_INFO", function($join){
							$join->on("ST.PIS_MK", '=',"PIS_INFO.PIS_MK");
							$join->on("ST.CORP_MK", '=',"PIS_INFO.CORP_MK");
						})->select(
							   'PIS_INFO.PIS_NM'
							 ,  DB::raw( "C.FRNM AS IN_FRNM")
							 , 'ST.SUJO_NO'
							 , DB::raw( "CONVERT(CHAR(10), ST.INPUT_DATE, 23) AS INPUT_DATE" )
							 , DB::raw( "CONVERT(CHAR(10), ST.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
							 , 'ST.PIS_MK'
							 , 'ST.SIZES'
							 , 'ST.SEQ'
							 , 'ST.REASON'
							 , 'ST.OUTLINE'
							 , 'ST.DE_CR_DIV'
							 , DB::raw('ROUND( ST.QTY, 1) AS QTY')
							 , 'ST.CUST_MK'
							 , 'ST.UNCS'
							 , 'ST.INPUT_AMT'
							 , 'ST.INPUT_DISCOUNT'
							 , 'ST.CURR_QTY'
							 , 'ST.ORIGIN_CD'
							 , 'ST.ORIGIN_NM'
							 , 'ST.INPUT_UNPROV'
							 , 'ST.CORP_MK'
							 //,  DB::raw( " ROUND( ROUND( ST.QTY, 2) * ST.UNCS, -3, 0.5)   AS PROFIT " )
							 ,  DB::raw( " ROUND( ROUND( ST.QTY, 1) * ST.UNCS, -3, 0)   AS PROFIT " )
							 ,  DB::raw( "CASE ST.DE_CR_DIV WHEN 1 THEN ROUND(ST.QTY, 2) END AS IN_QTY " )
							 , DB::raw( "CONVERT(CHAR(19), ST.UPDATE_DATE, 120) AS UPDATE_DATE" ) // 2017-07-06 입고수정일 추가(진영수산)

						)->leftjoin("CUST_CD AS C",  function($leftjoin){
							$leftjoin->on("ST.CORP_MK",'=', "C.CORP_MK")
									 ->on("ST.CUST_MK", '=',"C.CUST_MK");
						})
						->where ( 'ST.CORP_MK', $this->getCorpId())
						/*
						->where ( 'C.CORP_DIV', 'P')
						->where ( 'C.INTERFACE_YN', 'Y')
						*/
						->where ( 'ST.QTY', ">", 0)
						->where ( 'ST.INPUT_AMT', ">", 0)
						
						->where (function($query){
										
							if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
								if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){

									$query->whereNull("ST.OUTPUT_DATE")
											->whereBetween("ST.INPUT_DATE",  array(Request::Input("start_date"), Request::Input("end_date")));
									
								}
							}
						});
		//return Datatables::of($STOCK_INFO)->make(true);

		return Datatables::of($STOCK_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') && Request::Input('textSearch') != ""){
						$query->where("C.FRNM", "LIKE", "%".Request::Input('textSearch')."%" );
					}
			}
		)->make(true);

	}

	// 수조(매입관리) 조회
	public function listData()
	{
		
		$STOCK_INFO ;
		if( Request::Input("chkStock") == "1"){
			$STOCK_INFO = DB::table('STOCK_SUJO_VIEW AS ST')
							->select( 'PIS_NM', 'ST.PIS_MK' , 'ST.SIZES', DB::raw('ROUND(SUM( ST.QTY ), 2) as QTY'))
							->where ( 'ST.CORP_MK', $this->getCorpId())
							->whereNotNull('ST.PIS_MK')

							->where(function($query){

								if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
									if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
										$query->whereBetween("ST.INPUT_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
									}
								}

								if( Request::Has('textSearch') ){
									if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SALE_UNCS"){
										$query->where("ST.SALE_UNCS",  Request::Input('textSearch') );

									}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
										$query->where("ST.PIS_NM",  "like", "%".Request::Input('textSearch')."%");

									}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SIZES"){
										$query->where("ST.SIZES",  "like", "%".Request::Input('textSearch')."%");

									}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "QTY"){
										$query->where("ST.QTY",  Request::Input('textSearch') );

									}
									/*
									else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SUJO_NO"){
										$query->where("SUJO_NO",  "like", "%".Request::Input('textSearch')."%");

									}
									*/
									else{
										$query->where(function($q){
											$q->where("SALE_UNCS", is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 )
											->orwhere("PIS_NM",  "like", "%".Request::Input('textSearch')."%")
											->orwhere("SIZES",  "like", "%".Request::Input('textSearch')."%")
											->orwhere("QTY", is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 );
											//->orwhere("SUJO_NO",  "like", "%".Request::Input('textSearch')."%");
										});
									}
								}
							})->groupby('PIS_NM', 'PIS_MK', 'SIZES');
		}else{
			
			$STOCK_INFO = DB::table('STOCK_INFO AS ST')
							->leftjoin("STOCK_SUJO AS SUJO", function($join){	
								$join->on("ST.CORP_MK", "=", "SUJO.CORP_MK")
									 ->on("ST.PIS_MK", "=", "SUJO.PIS_MK")
									 ->on("ST.SIZES" , "=", "SUJO.SIZES");
							})->join("PIS_INFO AS P", function($join){
								$join->on("ST.CORP_MK", "=", "P.CORP_MK")
									 ->on("ST.PIS_MK", "=", "P.PIS_MK");
							})->select(  'P.PIS_NM'
										,'ST.PIS_MK'
										,'ST.SIZES'
										//, DB::raw("ST.QTY AS QTY1")
										, DB::raw("'' AS QTY")
							)->where ( 'ST.CORP_MK', $this->getCorpId())
							->whereNull( 'SUJO.PIS_MK')
							->whereNull( 'SUJO.SIZES')
							->groupby('P.PIS_NM', 'ST.PIS_MK', 'ST.SIZES');
		
		}
		
		return Datatables::of($STOCK_INFO)->make(true);

	}

	public function detailData(){
	
		$STOCK_INFO = DB::table("STOCK_INFO AS ST")
						->join("PIS_INFO", function($join){
							$join->on("ST.CORP_MK", '=',"PIS_INFO.CORP_MK");
							$join->on("ST.PIS_MK", '=',"PIS_INFO.PIS_MK");
						})
						->leftjoin("CUST_CD",  function($join){
							$join->on("ST.CORP_MK",'=', "CUST_CD.CORP_MK")
								 ->on("ST.CUST_MK", '=',"CUST_CD.CUST_MK");
						})
						->leftjoin("SALE_INFO_D AS D", function($join){
							$join->on("D.CORP_MK", '=',"ST.CORP_MK")
								 ->on("D.PIS_MK", '=',"ST.PIS_MK")
								 ->on("D.STOCK_INFO_SEQ", '=',"ST.SEQ")
								 ->on("D.CUST_MK", "=", "ST.CUST_MK")
								 ->on("D.WRITE_DATE", "=", "ST.OUTPUT_DATE")
								 ->on("D.SIZES", '=',"ST.SIZES");
						})
						->select(
							   'PIS_INFO.PIS_NM'
							 , 'CUST_CD.FRNM'
							 , 'ST.SUJO_NO'
							 , DB::raw( "CONVERT(CHAR(10), ST.INPUT_DATE, 23) AS INPUT_DATE" )
							 , DB::raw( "CONVERT(CHAR(10), ST.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
							 , 'ST.PIS_MK'
							 , 'ST.SIZES'
							 , 'ST.SEQ'
							 , 'ST.REASON'
							 , 'ST.OUTLINE'
							 , 'ST.DE_CR_DIV'
							 , 'ST.QTY'
							 , 'ST.CUST_MK'
							 , 'ST.UNCS'
							 , 'ST.INPUT_AMT'
							 , 'ST.INPUT_DISCOUNT'
							 , 'ST.CURR_QTY'
							 , 'ST.ORIGIN_CD'
							 , 'ST.ORIGIN_NM'
							 , 'ST.INPUT_UNPROV'
							 , 'ST.CORP_MK'
							 , 'D.UNCS AS SALE_UNCS'
							 ,  DB::raw( "CASE WHEN ST.DE_CR_DIV = 1 THEN ROUND(ROUND(ST.QTY, 2)*(-1) * (ST.UNCS), 2)
							 				   ELSE ROUND(ROUND(ST.QTY, 2) * (D.UNCS),2) END AS PROFIT " )
							 ,  DB::raw( "CASE WHEN ST.REASON = 1 THEN '감량'
											   WHEN ST.REASON = 2 THEN '폐사'
											   WHEN ST.REASON = 3 THEN '증량'
											   WHEN ST.REASON = 4 THEN '기타'
											   WHEN ST.REASON = 5 THEN '반품입고'
																 ELSE '' END AS REASON_NM")
							 ,  DB::raw( "CASE ST.DE_CR_DIV WHEN 1 THEN ROUND(ST.QTY, 2) END AS IN_QTY " )
							 ,  DB::raw( "CASE ST.DE_CR_DIV WHEN 0 THEN ROUND(ST.QTY, 2) END AS OUT_QTY ")
						)
						->where ( 'ST.CORP_MK', $this->getCorpId())
						->where (function($query){
							// 어종과 사이즈가 없을 경우 전체 어종에 대한 재고목록이 조회 되도록 	
							if( Request::Has("pis_mk") && Request::Has("sizes") ){
								$query->where ( 'ST.PIS_MK', Request::Input("pis_mk"))
									  ->where ( 'ST.SIZES',  Request::Input("sizes"));
							}
																	 
						})
						->where(function($query){
							if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
								if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
									if( Request::Input("inOutDate") == "srt_output" ){
										$query->whereBetween("ST.OUTPUT_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
									}else {
										$query->whereBetween("ST.INPUT_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
									}
								}
							}										 
						});

		return Datatables::of($STOCK_INFO)
				->filter(function($query) {

					if( Request::Has('textSearch') && ( Request::Input('textSearch') != "" || Request::Input('textSearch') != "none") ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SALE_UNCS"){
							if( is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 ){
								$query->where("D.UNCS",  Request::Input('textSearch') );
							}

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
							$query->where("PIS_INFO.PIS_NM",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SIZES"){
							$query->where("ST.SIZES", "like", "%".Request::Input('textSearch')."%" );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "QTY"){
							if( is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 ){
								$query->where("ST.QTY",  Request::Input('textSearch') );
							}

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "FRMN"){
							$query->where("CUST_CD.FRNM",  "like", "%".Request::Input('textSearch')."%" );
						}
						else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SUJO_NO"){
							$query->where("ST.SUJO_NO",  "like", "%".Request::Input('textSearch')."%");

						}else if(  Request::Has('srtCondition') && Request::Input('srtCondition') == "OUTLINE"){
							$query->where("ST.OUTLINE",  "like", "%".Request::Input('textSearch')."%");

						}else if(  Request::Has('srtCondition') && Request::Input('srtCondition') == "DE_CR_DIV_1"){
							if( is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 ){
								$query->where("ST.DE_CR_DIV",  1 )
									->where("ST.QTY",  Request::Input('textSearch') );
							}
						}else if(  Request::Has('srtCondition') && Request::Input('srtCondition') == "DE_CR_DIV_0"){
							if( is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 ){
								$query->where("ST.DE_CR_DIV",  0 )
									->where("ST.QTY",  Request::Input('textSearch') );
							} 
						}else if(  Request::Has('srtCondition') && Request::Input('srtCondition') == "ALL") {
								
							$query->where(function($q) {
								$q->orwhere("ST.SUJO_NO",  "like", "%".Request::Input('textSearch')."%")
									->orwhere("CUST_CD.FRNM",  "like", "%".Request::Input('textSearch')."%" )
									->orwhere("PIS_INFO.PIS_NM",  "like", "%".Request::Input('textSearch')."%")
									->orwhere("ST.OUTLINE",  "like", "%".Request::Input('textSearch')."%")
									->orwhere("ST.SIZES", "like", "%".Request::Input('textSearch')."%" )
									->orwhere("D.UNCS", ( is_numeric(Request::Input('textSearch')) ? (float)Request::Input('textSearch') : -99999999 ) )
									->orwhere("ST.QTY", ( is_numeric(Request::Input('textSearch')) ? (float)Request::Input('textSearch') : -99999999 ) );
							});
										
						}
					}

				})->make(true);

	}

	public function dataInputModList(){

		$STOCK_INFO = DB::table("STOCK_INFO AS STOCK")->select(
						   'PIS.PIS_NM'
						 , 'CUST.FRNM'
						 , DB::raw('ISNULL((STOCK.UNCS * STOCK.QTY - STOCK.INPUT_DISCOUNT), 0) AS REAL_AMT')
						 , DB::raw('ISNULL(UNPV.AMT,0) AS UNPROV')
						 , DB::raw( "CONVERT(CHAR(10), STOCK.INPUT_DATE, 23) AS INPUT_DATE" )
						 , DB::raw( "CONVERT(CHAR(10), STOCK.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
						 , 'STOCK.PIS_MK'
						 , 'STOCK.SIZES'
						 , 'STOCK.SEQ'
						 , 'STOCK.REASON'
						 , 'STOCK.OUTLINE'
						 , 'STOCK.DE_CR_DIV'
						 , 'STOCK.QTY'
						 , 'STOCK.UNCS'
						 , 'STOCK.INPUT_AMT'
						 , 'STOCK.INPUT_DISCOUNT'
						 , 'STOCK.CURR_QTY'
						 , 'STOCK.ORIGIN_CD'
						 , 'STOCK.ORIGIN_NM'
						 , 'STOCK.INPUT_UNPROV'
						 , 'STOCK.CORP_MK'
						 ,  DB::raw( "CASE STOCK.DE_CR_DIV WHEN 1 THEN ROUND(STOCK.QTY, 2) END AS IN_QTY " )
						 ,  DB::raw( "CASE STOCK.DE_CR_DIV WHEN 0 THEN ROUND(STOCK.QTY, 2) END AS OUT_QTY ")
						)->join("PIS_INFO AS PIS", function($join){
							$join->on("STOCK.PIS_MK", '=',"PIS.PIS_MK");
							$join->on("STOCK.CORP_MK", '=',"PIS.CORP_MK");
						})->leftjoin("CUST_CD AS CUST",  function($leftjoin){
							$leftjoin->on("STOCK.CORP_MK",'=', "CUST.CORP_MK")
									 ->on("STOCK.CUST_MK", '=',"CUST.CUST_MK");
						})->leftjoin("UNPROV_INFO AS UNPV", function($leftjoin){
							$leftjoin->on("STOCK.CORP_MK", '=',"UNPV.CORP_MK")
									 ->on("STOCK.PIS_MK", '=',"UNPV.STOCK_PIS_MK")
									 ->on("STOCK.SIZES", '=',"UNPV.STOCK_SIZES")
									 ->on("STOCK.SEQ", '=',"UNPV.STOCK_SEQ");
						})
						->where ( 'STOCK.CORP_MK', $this->getCorpId())
						->where ( 'STOCK.PIS_MK', Request::Input("pis_mk"))
						->where ( 'STOCK.SIZES', Request::Input("sizes"))
						->where ( 'STOCK.INPUT_DATE', Request::Input("input_date"))
						->where ( 'STOCK.ORIGIN_NM', Request::Input("origin_nm"))->where(function($query){
							$query->orwhereNull("STOCK.OUTPUT_DATE")
								->orwhereNotNull("STOCK.REASON");
						});

						//->orderby("INPUT_DATE, SEQ", "DESC");

		return Datatables::of($STOCK_INFO)->make(true);

	}

	// 입고수정조회
	public function detailEdit($stock, Request $request){

		$pis_mk		= urldecode(Request::segment(4));
		$sizes		= urldecode(Request::segment(5));
		$seq		= urldecode(Request::segment(6));
		$sujo_no	= urldecode(Request::segment(7));
		
		
		$model = STOCK_INFO::select(
					  'PIS_INFO.PIS_NM'
					 ,'CUST_CD.CUST_MK'
					 , DB::raw("ISNULL(CUST_CD.FRNM, '') AS FRNM")
					 , DB::raw('(STOCK_INFO.INPUT_AMT) AS AMT')
					 , DB::raw('(STOCK_INFO.INPUT_AMT - STOCK_INFO.INPUT_DISCOUNT) AS REAL_AMT')
					 , DB::raw('STOCK_INFO.INPUT_UNPROV AS UNPROV')
					 , DB::raw('(STOCK_INFO.INPUT_AMT - INPUT_DISCOUNT - STOCK_INFO.INPUT_UNPROV) AS CASH')
					 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.INPUT_DATE, 23) AS INPUT_DATE" )
					 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
					 , 'STOCK_INFO.PIS_MK'
					 , 'STOCK_INFO.SIZES'
					 , 'STOCK_INFO.SEQ'
					 , 'STOCK_INFO.REASON'
					 , 'STOCK_INFO.OUTLINE'
					 , 'STOCK_INFO.DE_CR_DIV'
					 , 'STOCK_INFO.QTY'
					 , 'STOCK_INFO.UNCS'
					 , 'STOCK_INFO.INPUT_AMT'
					 , 'STOCK_INFO.INPUT_DISCOUNT'
					 , 'STOCK_INFO.CURR_QTY'
					 , 'STOCK_INFO.ORIGIN_CD'
					 , 'STOCK_INFO.ORIGIN_NM'
					 , 'STOCK_INFO.INPUT_UNPROV'
					 , 'STOCK_INFO.CORP_MK'
					)->join("PIS_INFO", function($join){
						$join->on("STOCK_INFO.PIS_MK", '=',"PIS_INFO.PIS_MK");
						$join->on("STOCK_INFO.CORP_MK", '=',"PIS_INFO.CORP_MK");

					})->leftjoin("CUST_CD",  function($leftjoin){
						$leftjoin->on("STOCK_INFO.CORP_MK",'=', "CUST_CD.CORP_MK")
								 ->on("STOCK_INFO.CUST_MK", '=',"CUST_CD.CUST_MK");

					})->where ( 'STOCK_INFO.CORP_MK', $this->getCorpId())
					  ->where ( 'STOCK_INFO.PIS_MK', $pis_mk)
					  ->where ( 'STOCK_INFO.SIZES', $sizes)
					  ->where ( 'STOCK_INFO.SEQ', $seq)->first();

		
		//dd($model);
		$modelOrgin = $this->getOrginCD();
		return view("stock.detailEdit", ["model" => $model, "stock" => $stock, 'sujo_no' => $sujo_no ])->with('modelOrgin', $modelOrgin);
	}

	// 입고수정 저장
	public function setUpdateStock($stock){
		$validator = Validator::make( Request::Input(), [
			'PIS_MK'	=> 'required|max:4,NOT_NULL',
			'SIZES'		=> 'required|max:20,NOT_NULL',
			'SEQ'		=> 'required|numeric',
			'SUJO_NO'	=> 'required|max:6,NOT_NULL',
			'CUST_MK'	=> 'required',
			'QTY'		=>'required|numeric',
			'PRE_QTY'	=>'numeric',
			'UNCS'		=>'required|numeric',
			'INPUT_AMT'	=>'numeric',
			'INPUT_DISCOUNT'=>'numeric',
			'INPUT_UNPROV'=>'numeric',
			'CASH'		=>'numeric',
			'REAL_AMT'	=>'required|numeric',
		]);

		if ($validator->fails()) {

			return Redirect::to("/stock/" . $stock. "/detailEdit/".Request::Input("PIS_MK")."/".Request::Input("SIZES")."/".Request::Input("SEQ")."/".Request::Input("SUJO_NO") )
					->withErrors($validator)
					->withInput();
		}

		DB::beginTransaction();
		try{
			
			// timeZone 동기화가 되지 않아서 임시로 추가.
			date_default_timezone_set('Asia/Seoul');

			// 2. 재고 내역 수정
			STOCK_INFO::where ('CORP_MK', $this->getCorpId())
					  ->where ( 'PIS_MK', Request::Input("PIS_MK"))
					  ->where ( 'SIZES',  Request::Input("SIZES"))
					  ->where ( 'SEQ',  Request::Input("SEQ"))
					  ->update(
							[
								 'CUST_MK'			=> Request::Input("CUST_MK")
								,'INPUT_DATE'		=> Request::Input("INPUT_DATE")
								,'OUTLINE'			=> Request::Input("OUTLINE")
								,'QTY'				=> Request::Input("QTY")
								,'UNCS'				=> Request::Input("UNCS")
								,'INPUT_AMT'		=> Request::Input("REAL_AMT")
								,'INPUT_DISCOUNT'	=> Request::Input("INPUT_DISCOUNT")
								,'INPUT_UNPROV'		=> Request::Input("UNPROV")
								,'CURR_QTY'			=> Request::Input("CURR_QTY") 
								,'ORIGIN_CD'		=> Request::Input("ORIGIN_CD")
								,'ORIGIN_NM'		=> Request::Input("ORIGIN_NM")
								,'REASON'			=> Request::Input("REASON")
								,'UPDATE_DATE'		=> date("Y-m-d H:i:s")
								//,'SUJO_NO'			=> Request::Input("SUJO_NO")
							]
					);
			
			$STOCK_INFO = DB::table("STOCK_INFO")
							->where ( 'CORP_MK', $this->getCorpId())
							->where ( 'PIS_MK', Request::Input("PIS_MK"))
							->where ( 'SIZES',  Request::Input("SIZES"))
							->where ( 'SEQ',  Request::Input("SEQ"))->first();

			// 3. 기존 미지급금 삭제
			// 거래처가 변경되었을수도 있으므로 기존 미지급금의 거래처 정보 삭제 후 새로 등록
			DB::table("UNPROV_INFO")
					->where ( 'CORP_MK', $this->getCorpId())
					->where ( 'PROV_CUST_MK',  Request::Input("PRE_CUST_MK"))
					->where ( 'STOCK_PIS_MK',  Request::Input("PIS_MK"))
					->where ( 'STOCK_SIZES',  Request::Input("SIZES"))
					->where ( 'STOCK_SEQ',  Request::Input("SEQ"))
					->delete();

			// 4. 새 미지급금 정보로 입력
			DB::table("UNPROV_INFO")
					->insert(
						[
							'CORP_MK'		=> $this->getCorpId(),
							'WRITE_DATE'	=> $STOCK_INFO->INPUT_DATE,
							'SEQ'			=> ($this->getMaxSeqInUNPROVINFO($STOCK_INFO->INPUT_DATE) ) + 1,
							'PROV_CUST_MK'	=> $STOCK_INFO->CUST_MK,
							'PROV_DIV'		=> 1,
							'AMT'			=> $STOCK_INFO->INPUT_UNPROV,
							'REMARK'		=> '입고 미수',
							'STOCK_PIS_MK'	=> $STOCK_INFO->PIS_MK,
							'STOCK_SIZES'	=> $STOCK_INFO->SIZES,
							'STOCK_SEQ'		=> $STOCK_INFO->SEQ,
						]
					);

			
			

			// 원산지, 입고날짜, 합계 변경(단가, 거래처 변경없음)
			// 일단 문제가 있는것으로 판단되어 원산지/입고수정은 변경을 해야하나..?
		
			$OLD_STOCK_SUJO = STOCK_SUJO::select('QTY')
						->where ( 'CORP_MK', $this->getCorpId())
						->where ( 'SUJO_NO', Request::Input("SUJO_NO"))->first();

			$stock_sujo = STOCK_SUJO::where ( 'CORP_MK', $this->getCorpId())
						  ->where ( 'SUJO_NO', Request::Input("SUJO_NO"))
						  ->first();

			if( $stock_sujo != null){

				STOCK_SUJO::where ( 'CORP_MK', $this->getCorpId())
					  ->where ( 'SUJO_NO', Request::Input("SUJO_NO"))
					  ->update(
							[
								// 'INPUT_DATE' => Request::Input("INPUT_DATE")
								 'QTY' => ( $OLD_STOCK_SUJO->QTY - Request::Input("PRE_QTY") + Request::Input("QTY") )
								,'CUST_MK' => Request::Input("CUST_MK")
							]);
			}
			
			
		} catch( ValidationException $e){
			throw $e;
			DB::rollback();
			return Redirect::to("/stock/" . $stock. "/detailEdit/".Request::Input("PIS_MK")."/".Request::Input("SIZES")."/".Request::Input("SEQ")."/".Request::Input("SUJO_NO") )
					->withErrors($e->getErrors())
					->withInput();

		}catch( Exception $e){
			DB::rollback();
			return Redirect::to("/stock/" . $stock. "/detailEdit/".Request::Input("PIS_MK")."/".Request::Input("SIZES")."/".Request::Input("SEQ")."/".Request::Input("SUJO_NO") )
					->withErrors($e->getErrors())
					->withInput();

		}

		DB::commit();
		//return Redirect("/stock/stock/detail/".Request::Input("PIS_MK")."/".Request::Input("SIZES"));
		return Redirect("/buy/sujo/index?page=1");
	}

	// 입고수정->삭제
	// 입고삭제시 미수금 정보도 같이 삭제 되어야함 => 해당수조 중량 (-)
	public function removeStockInSujo(){

		return $exception = DB::transaction(function(){

			try{
				
				$STOCK_SUJO = STOCK_SUJO::where("CORP_MK", $this->getCorpId())
								->where("PIS_MK", Request::Input("PIS_MK"))
								->where("SIZES", Request::Input("SIZES"))
								->where("SUJO_NO", Request::Input("SUJO_NO"))->first();

				$STOCK_INFO = STOCK_INFO::where("CORP_MK", $this->getCorpId())
								->where("PIS_MK", Request::Input("PIS_MK"))
								->where("SIZES", Request::Input("SIZES"))
								->where("SEQ", Request::Input("SEQ"))->first();
				
				
				// 1. 수조총중량 수정
				// 판매/감량된 재고는 현재 수조정보를 더해줌
				// 판매/감량되지 않은 재고는 현재 수조 수량을 빼준다
				if( $STOCK_SUJO != null && count($STOCK_SUJO) > 0){
					
					if( $STOCK_INFO->DE_CR_DIV == "0" && $STOCK_INFO->OUTPUT_DATE !== ""){
						$STOCK_SUJO->QTY = (float)$STOCK_SUJO->QTY + (float)( number_format($STOCK_INFO->QTY, 2));
					}else{
						$STOCK_SUJO->QTY = (float)$STOCK_SUJO->QTY - (float)( number_format($STOCK_INFO->QTY, 2));
					}

					STOCK_SUJO::where("CORP_MK", $this->getCorpId())
								->where("SUJO_NO", Request::Input("SUJO_NO"))
								->where("PIS_MK", Request::Input("PIS_MK"))
								->where("SIZES", Request::Input("SIZES"))
								->update(
									[
										'QTY' => round(number_format($STOCK_SUJO->QTY, 2), 2)
									]
								);
				}

				// 3. 재고정보 삭제
				$return = STOCK_INFO::where("CORP_MK", $this->getCorpId())
					->where("PIS_MK", Request::Input("PIS_MK"))
					->where("SIZES", Request::Input("SIZES"))
					->where("SEQ", Request::Input("SEQ"))
					->delete();

				// 2. 미지급금 정보 삭제
				UNPROV_INFO::where( 'CORP_MK'	,	$this->getCorpId() )
								->where( 'STOCK_PIS_MK' , Request::Input("PIS_MK") )
								->where( 'STOCK_SIZES', Request::Input("SIZES") )
								->where( 'STOCK_SEQ', Request::Input("SEQ") )
								->delete();

				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			} catch(Exception $e){
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}
		});
	}

	// 입고재고에 대한 판매 리스트 정보
	public function getSaleListForInput(){
		
		$sizes = urldecode(Request::Input("SIZES"));

		$model = DB::table("STOCK_INFO AS ST")
			                ->select( DB::raw("P_CUST.FRNM AS P_FRNM")
									 ,DB::raw("S_CUST.FRNM AS S_FRNM")
									 ,"PIS.PIS_NM"
									 ,"SALE_D.SIZES"
									 ,"SALE_D.ORIGIN_NM"
									 ,"SALE_D.QTY"
									 ,"SALE_D.UNCS"
									 ,"SALE_D.AMT"
									 , DB::raw( "CONVERT(CHAR(10), SALE_D.WRITE_DATE, 23) AS WRITE_DATE" )
							)->leftjoin("SALE_INFO_D AS SALE_D",   function($leftjoin){
								$leftjoin->on("ST.CORP_MK"		, "=", "SALE_D.CORP_MK");
								$leftjoin->on("ST.INPUT_DATE"	, "=", "SALE_D.INPUT_DATE");
								$leftjoin->on("ST.PIS_MK"		, "=", "SALE_D.PIS_MK");
								$leftjoin->on("ST.SIZES"		, "=", "SALE_D.SIZES");
								$leftjoin->on("ST.ORIGIN_NM"	, "=", "SALE_D.ORIGIN_NM");
							//	$leftjoin->on("ST.OUTPUT_DATE"	, "=", "SALE_D.WRITE_DATE");
							})->leftjoin("CUST_CD AS S_CUST", function($leftjoin){
								$leftjoin->on("SALE_D.CORP_MK"	, "=", "S_CUST.CORP_MK");
								$leftjoin->on("SALE_D.CUST_MK"	, "=", "S_CUST.CUST_MK");
							})->leftjoin("PIS_INFO AS PIS",   function($leftjoin){
								$leftjoin->on("PIS.CORP_MK"		, "=", "SALE_D.CORP_MK");
								$leftjoin->on("SALE_D.PIS_MK"	, "=", "PIS.PIS_MK");
							})->leftjoin("CUST_CD AS P_CUST", function($leftjoin){
								$leftjoin->on("ST.CORP_MK"		, "=", "P_CUST.CORP_MK");
								$leftjoin->on("ST.CUST_MK"		, "=", "P_CUST.CUST_MK");
							})
							->where("SALE_D.CORP_MK", $this->getCorpId())
							->where("SALE_D.INPUT_DATE", urldecode(Request::Input("INPUT_DATE")))
							->where("SALE_D.PIS_MK", urldecode(Request::Input("PIS_MK")))
							->where("SALE_D.SIZES", urldecode(Request::Input("SIZES")))
							->where("SALE_D.ORIGIN_NM", urldecode(Request::Input("ORIGIN_NM")))
							->where("P_CUST.FRNM", urldecode(Request::Input("FRNM")))
							->whereNull("ST.OUTPUT_DATE")
							->get();
		
		return response()->json($model);
	}

	public function setArrangeStock(){
	}

	public function detailEditArr($stock){
		$pis_mk = Request::segment(4);
		$sizes = urldecode(Request::segment(5));
		$seq = Request::segment(6);
		$sujo_no = urldecode(Request::segment(7));

		$model = STOCK_INFO::select(
					  'PIS_INFO.PIS_NM'
					 ,'CUST_CD.CUST_MK'
					 , 'STOCK_INFO.PIS_MK'
					 , 'STOCK_INFO.SIZES'
					 , 'STOCK_INFO.SEQ'
					 , 'STOCK_INFO.REASON'
					 , 'STOCK_INFO.OUTLINE'
					 , 'STOCK_INFO.DE_CR_DIV'
					 , 'STOCK_INFO.QTY'
					 , 'STOCK_INFO.UNCS'
					 , 'STOCK_INFO.INPUT_AMT'
					 , 'STOCK_INFO.INPUT_DISCOUNT'
					 , 'STOCK_INFO.CURR_QTY'
					 , 'STOCK_INFO.ORIGIN_CD'
					 , 'STOCK_INFO.ORIGIN_NM'
					 , 'STOCK_INFO.INPUT_UNPROV'
					 , 'STOCK_INFO.CORP_MK'
					 , DB::raw("ISNULL(CUST_CD.FRNM, '') AS FRNM")
					 , DB::raw('(STOCK_INFO.INPUT_AMT) AS AMT')
					 , DB::raw('(STOCK_INFO.INPUT_AMT - STOCK_INFO.INPUT_DISCOUNT) AS REAL_AMT')
					 , DB::raw('STOCK_INFO.INPUT_UNPROV AS UNPROV')
					 , DB::raw('(STOCK_INFO.INPUT_AMT - INPUT_DISCOUNT - STOCK_INFO.INPUT_UNPROV) AS CASH')
					 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.INPUT_DATE, 23) AS INPUT_DATE" )
					 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
					)->join("PIS_INFO", function($join){
						$join->on("STOCK_INFO.PIS_MK", '=',"PIS_INFO.PIS_MK");
						$join->on("STOCK_INFO.CORP_MK", '=',"PIS_INFO.CORP_MK");

					})->leftjoin("CUST_CD",  function($leftjoin){
						$leftjoin->on("STOCK_INFO.CORP_MK",'=', "CUST_CD.CORP_MK")
								 ->on("STOCK_INFO.CUST_MK", '=',"CUST_CD.CUST_MK");

					})->where ( 'STOCK_INFO.CORP_MK', $this->getCorpId())
					  ->where ( 'STOCK_INFO.PIS_MK', $pis_mk)
					  ->where ( 'STOCK_INFO.SIZES', $sizes)
					  ->where ( 'STOCK_INFO.SEQ', $seq)->first();

		$modelOrgin = $this->getOrginCD();
		return view("stock.detailEditArr", ["model" => $model, "stock" => $stock, 'sujo_no' => $sujo_no ])->with('modelOrgin', $modelOrgin);
	}

	// 총재고 가져오기
	public function getQtySum(){

		$QtySum = DB::table("STOCK_SUJO_VIEW")
					->select(DB::raw("SUM(QTY) AS SUM_QTY"))
					->where("CORP_MK", $this->getCorpId())
					->where("PIS_MK", Request::Input("PIS_MK"))
					->where("SIZES", Request::Input("SIZES"))->first();

		return response()->json(['result' => 'success', 'data' => ['SUM_QTY' => round ($QtySum->SUM_QTY, 2) ]]);
	}

	// 수정상세리스트 수정 버튼 클릭
	public function getStockDetailUpdateList(){
		
		$sizes		= urldecode(trim(Request::Input("sizes")));
		$origin_cd	= urldecode(trim(Request::Input("origin_cd")));
		$pis_mk		= urldecode(trim(Request::Input("pis_mk")));
		$sujo_no	= urldecode(trim(Request::Input("sujo_no")));
		$seq		= urldecode(trim(Request::Input("seq")));
		$cust_mk	= urldecode(trim(Request::Input("cust_mk")));
		
		// 재고수정일 경우
		if( Request::Has('option')){

			$qty = Request::Input("QTY");

			$STOCK_SUJO = DB::table("STOCK_INFO AS ST")
						->select(	'ST.CORP_MK',
									DB::raw("ISNULL(SUJO.SUJO_NO, '없음') AS SUJO_NO"),
									'ST.PIS_MK',
									'ST.SIZES',
									DB::raw( "CONVERT(CHAR(10), ST.INPUT_DATE, 23) AS INPUT_DATE" ),
									'ST.QTY',
									//'SUJO.SALE_UNCS',
									'ST.ORIGIN_CD',
									'ST.ORIGIN_NM',
									'PIS.PIS_NM',
									'ST.SEQ'
						)
						->leftjoin("STOCK_SUJO AS SUJO", function($join){
								$join->on("SUJO.CORP_MK", "=", "ST.CORP_MK");
								$join->on("SUJO.PIS_MK", "=", "ST.PIS_MK");
								$join->on("SUJO.SIZES", "=", "ST.SIZES");
								$join->on("SUJO.ORIGIN_CD", "=", "ST.ORIGIN_CD");
								$join->on("SUJO.SUJO_NO", "=", "ST.SUJO_NO");
								// 수조입고일과 재고의 입고일자는 입고시마다 변경되므르 수정
								// $join->on("SUJO.INPUT_DATE", "=", "ST.INPUT_DATE");
						})->join("PIS_INFO AS PIS", function($join){
								$join->on("ST.PIS_MK", "=", "PIS.PIS_MK");
								$join->on("ST.CORP_MK", "=", "PIS.CORP_MK");
						})->where("ST.CORP_MK", $this->getCorpId())
						  ->where("ST.PIS_MK", $pis_mk)
						  ->where("ST.SIZES", $sizes)
						  ->where("ST.ORIGIN_CD", $origin_cd)
						  ->where("ST.INPUT_DATE", Request::Input("input_date"))
						  ->where("ST.QTY", Request::Input("qty"))
						  ->where("ST.DE_CR_DIV", "1")
						  ->where("ST.CUST_MK", Request::Input("cust_mk"))
						  ->where("ST.SEQ",$seq)
						  ->whereNull("ST.REASON")
						  ->whereNull("ST.OUTPUT_DATE");

		}else{
			// 입고수정일때
			$STOCK_SUJO = DB::table("STOCK_SUJO AS SUJO")
						->select(	'SUJO.CORP_MK',
									'SUJO.SUJO_NO',
									'SUJO.PIS_MK',
									'SUJO.SIZES',
									DB::raw( "CONVERT(CHAR(10), SUJO.INPUT_DATE, 23) AS INPUT_DATE" ),
									'ST.QTY',
									'SUJO.SALE_UNCS',
									'SUJO.ORIGIN_CD',
									'SUJO.ORIGIN_NM',
									'PIS.PIS_NM',
									'ST.SEQ',
									'CUST.FRNM'
						)->join("PIS_INFO AS PIS", function($join){
								$join->on("SUJO.PIS_MK", "=", "PIS.PIS_MK");
								$join->on("SUJO.CORP_MK", "=", "PIS.CORP_MK");
						})->join("STOCK_INFO AS ST", function($join){
								$join->on("SUJO.CORP_MK", "=", "ST.CORP_MK");
								$join->on("SUJO.PIS_MK", "=", "ST.PIS_MK");
								$join->on("SUJO.SIZES", "=", "ST.SIZES");
								$join->on("SUJO.ORIGIN_CD", "=", "ST.ORIGIN_CD");
								// 수조입고일과 재고의 입고일자는 입고시마다 변경되므르 수정
								$join->on("SUJO.INPUT_DATE", "=", "ST.INPUT_DATE");
								$join->on("SUJO.CUST_MK", "=", "ST.CUST_MK");

						})->leftjoin("CUST_CD AS CUST",  function($leftjoin){
							$leftjoin->on("ST.CORP_MK",'=', "CUST.CORP_MK");
							$leftjoin->on("ST.CUST_MK", '=',"CUST.CUST_MK");
						})->where("ST.CORP_MK", $this->getCorpId())
						  ->where("ST.PIS_MK", $pis_mk)
						  ->where("ST.SIZES", $sizes)
						  ->where("ST.ORIGIN_CD", $origin_cd)
						  ->where("ST.INPUT_DATE", Request::Input("input_date"))		
						  ->where("ST.DE_CR_DIV", "1")
						  ->where("ST.CUST_MK", $cust_mk)
						  ->where("SUJO.SUJO_NO", $sujo_no)
						  ->where("CUST.CORP_DIV", "P")
						  ->whereNull("ST.OUTPUT_DATE")
						  ->whereNull("ST.REASON");
						//  ->where("SUJO.ORIGIN_NM", Request::Input("origin_nm"))
		}
		
		//dd($STOCK_SUJO->get());
		return Datatables::of($STOCK_SUJO)->make(true);
	}

	// 재고정리
	public function setSujoOut(){

		$validator = Validator::make( Request::Input(), [
			'SUJO_NO' => 'required|max:6,NOT_NULL',
			'PIS_MK' => 'required|max:4,NOT_NULL',
			'SIZES' => 'required|max:20,NOT_NULL',
			'ORGIN_CD' => 'required|max:20,NOT_NULL',
			'ORGIN_NM' => 'required|max:20,NOT_NULL',
			'INPUT_DATE' => 'required|date',
			'OUTPUT_DATE' => 'required|date',
			'QTY'=>'numeric',
			'QTY_TOBE'=>'required|numeric',
			'REASON'=>'required',
			'CUST_CD' => 'max:4',
			'CUST_NM' => 'max:20',

		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::beginTransaction();
		try
		{
			$STOCK_SUJO = STOCK_SUJO::where("SUJO_NO", Request::Input("SUJO_NO"))
									->where("CORP_MK", $this->getCorpId())->first();
			
			STOCK_INFO::insert(
					[
						'CORP_MK'		=> $this->getCorpId(),
						'PIS_MK'		=> $STOCK_SUJO->PIS_MK,
						'SIZES'			=> $STOCK_SUJO->SIZES,
						'SEQ'			=> $this->getMaxSeqInSTOCKINFO($STOCK_SUJO->SIZES , $STOCK_SUJO->PIS_MK ) + 1,
						'ORIGIN_CD'		=> $STOCK_SUJO->ORIGIN_CD,
						'ORIGIN_NM'		=> $STOCK_SUJO->ORIGIN_NM,
						'CUST_MK'		=> empty(Request::Input("CUST_CD")) ? DB::raw("null") : Request::Input("CUST_CD"),
						'INPUT_DATE'	=> Request::Input("INPUT_DATE"),
						'OUTPUT_DATE'	=> Request::Input("OUTPUT_DATE"),
						'REASON'		=> Request::Input("REASON"),
						'OUTLINE'		=> Request::Input("OUTLINE"),
						'DE_CR_DIV'		=> 0,
						'QTY'			=> number_format(Request::Input("QTY_TOBE"), 2),
						'SUJO_NO'		=> Request::Input("SUJO_NO"),
					]
			);
			
			
			$STOCK_SUJO->QTY = ( (number_format($STOCK_SUJO->QTY, 2) - number_format(Request::Input("QTY_TOBE"), 2)) * 100) / 100;
			
			if( $STOCK_SUJO->QTY == 0){

				// 현재 재고상태가 0이면 수조가 정보가 초기화 됨..잠시 보류함(2016-09-06)
				STOCK_SUJO::where('SUJO_NO', $STOCK_SUJO->SUJO_NO)
						->where('CORP_MK', $this->getCorpId())
						->update(
							[
								'PIS_MK'		=> DB::raw("null") ,
								'SIZES'			=> DB::raw("null") ,
								'ORIGIN_CD'		=> DB::raw("null") ,
								'ORIGIN_NM'		=> DB::raw("null") ,
								'INPUT_DATE'	=> DB::raw("null") ,
								'QTY'			=> DB::raw("null") ,
								'SALE_UNCS'		=> DB::raw("null") ,
								'CUST_MK'		=> DB::raw("null") ,
							]
				);


			}else{
				STOCK_SUJO::where('SUJO_NO', $STOCK_SUJO->SUJO_NO)
						->where('CORP_MK', $this->getCorpId())
						->update(
							[
								'QTY' => $STOCK_SUJO->QTY
							]
				);

			}

		} catch( ValidationException $e){

			DB::rollback();
			if ($e->fails()) {
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}

		}catch( Exception $e){
			if ($e->fails()) {
				$errors = $e->errors();
				$errors =  json_decode($errors);
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}
		}

		DB::commit();
		return response()->json(['result' => 'success', 'message' => '재고정리가 정상적으로 저장완료 되었습니다']);


	}

	public function log($stock, $PIS_MK, $SIZES)
	{
		return view("stock.log", [ "stock" => $stock, "PIS_MK" => $PIS_MK, "SIZES" =>  $SIZES] );
	}


	public function listLog(){

		$STOCK_LOG = DB::table("STOCK_INFO_LOG")
					->select( "CORP_MK"
							, "PIS_MK"
							, "SIZES"
							, "SEQ"
							, "SRL_NO"
							, "LOG_DIV"
							, "QTY"
							, "BEFORE_QTY"
							, DB::raw("QTY * UNCS - INPUT_DISCOUNT AS REAL_AMT")
							, DB::raw("BEFORE_QTY * BEFORE_UNCS - BEFORE_INPUT_DISCOUNT AS BEFORE_REAL_AMT")
							, "BEFORE_UNPROV"
							, "UNPROV"
							, DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" )
							)
					->where("CORP_MK", $this->getCorpId())
					->where("PIS_MK", Request::Input("PIS_MK"))
					->where("SIZES", Request::Input("SIZES"));

		return Datatables::of($STOCK_LOG)->make(true);
	}

	public function detailLog(){

		$STOCK_LOG = DB::table("STOCK_INFO_LOG AS S")
					->select(   DB::raw("S.*")
							  , DB::raw( "CONVERT(CHAR(10), S.WRITE_DATE, 23) AS WRITE_DATE" )
							  , "PIS.PIS_NM"
							  , DB::raw("S.QTY * UNCS AS AMT")
							  , DB::raw("S.QTY * UNCS - INPUT_DISCOUNT AS REAL_AMT")
							  , DB::raw("S.QTY * UNCS - INPUT_DISCOUNT - UNPROV AS CASH")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS AS BEFORE_AMT")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS - BEFORE_INPUT_DISCOUNT AS BEFORE_REAL_AMT")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS - BEFORE_INPUT_DISCOUNT - BEFORE_UNPROV AS BEFORE_CASH")
							  , "C.FRNM"
							  , DB::raw("BC.FRNM AS BEFORE_FRNM")
							)
					->leftjoin("CUST_CD AS C", function($join){
						$join->on("S.CORP_MK", "=", "C.CORP_MK");
						$join->on("S.CUST_MK", "=", "C.CUST_MK");

					})->leftjoin("CUST_CD AS BC", function($join){
						$join->on("S.CORP_MK", "=", "BC.CORP_MK");
						$join->on("S.BEFORE_CUST_MK", "=", "BC.CUST_MK");

					})->leftjoin("PIS_INFO AS PIS", function($join){
						$join->on("PIS.PIS_MK", "=", "S.PIS_MK");

					})->where("S.CORP_MK", $this->getCorpId())
					->where("S.SIZES", Request::Input("SIZES"))
					->where("S.SEQ", Request::Input("SEQ"))
					->where("S.SRL_NO", Request::Input("SRL_NO"))->first();

		return response()->json(['result' => $STOCK_LOG]);


	}

	public function detailLogAfter(){
	}



	public function Excel()
	{
		// 컬럼을 지정해줘야 에러가 안남
		$model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
			->where("CORP_MK", $this->getCorpId())
			->get();

		Excel::create('거래처그룹', function ($excel) use ($model) {
			$excel->sheet('거래처그룹', function ($sheet) use ($model) {
				$sheet->fromArray($model);
			});
		})->export('xls');
	}

	public function Pdf()
	{
		// 한글이 깨지므로 urldecode 수행
		$TF_SIZE		= urldecode(Request::segment(5));
		$PIS_MK			= Request::segment(4);
		$INPUT_DATE		= Request::segment(6);
		$OUTPUT_DATE	= Request::segment(7);

		$CONDITION		= urldecode(Request::segment(8));
		$SEARCH_TEXT	= urldecode(Request::segment(9));
		
		$INOUT		= urldecode(Request::segment(10));
	
		$STOCK_INFO = STOCK_INFO::select(
						   'PIS_INFO.PIS_NM'
						 , 'CUST_CD.FRNM'
						 //, 'STOCK_SUJO.SUJO_NO'
						 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.INPUT_DATE, 23) AS INPUT_DATE" )
						 , DB::raw( "CONVERT(CHAR(10), STOCK_INFO.OUTPUT_DATE, 23) AS OUTPUT_DATE" )
						 , 'STOCK_INFO.PIS_MK'
						 , 'STOCK_INFO.SIZES'
						 , 'STOCK_INFO.SEQ'
						 , 'STOCK_INFO.REASON'
						 , 'STOCK_INFO.OUTLINE'
						 , 'STOCK_INFO.DE_CR_DIV'
						 , 'STOCK_INFO.QTY'
						 , 'STOCK_INFO.CUST_MK'
						 , 'STOCK_INFO.UNCS'
						 , 'STOCK_INFO.INPUT_AMT'
						 , 'STOCK_INFO.INPUT_DISCOUNT'
						 , 'STOCK_INFO.CURR_QTY'
						 , 'STOCK_INFO.ORIGIN_CD'
						 , 'STOCK_INFO.ORIGIN_NM'
						 , 'STOCK_INFO.INPUT_UNPROV'
						 , 'STOCK_INFO.CORP_MK'
						 ,  DB::raw( "CASE WHEN STOCK_INFO.REASON = 1 THEN '감량'
											WHEN STOCK_INFO.REASON = 2 THEN '폐사'
											WHEN STOCK_INFO.REASON = 3 THEN '증량'
											WHEN STOCK_INFO.REASON =  4 THEN '기타'
															 ELSE '' END AS REASON_NM")
						 ,  DB::raw( "ISNULL(CASE STOCK_INFO.DE_CR_DIV WHEN 1 THEN ROUND(STOCK_INFO.QTY, 2) END, 0) AS IN_QTY " )
						 ,  DB::raw( "ISNULL(CASE STOCK_INFO.DE_CR_DIV WHEN 0 THEN ROUND(STOCK_INFO.QTY, 2) END, 0) AS OUT_QTY ")
						 , 'D.UNCS AS SALE_UNCS'
						 ,  DB::raw( "CASE WHEN STOCK_INFO.DE_CR_DIV = 1 THEN ROUND(ROUND(STOCK_INFO.QTY, 2)*(-1) * (STOCK_INFO.UNCS), 2)
										   ELSE ROUND(ROUND(STOCK_INFO.QTY, 2) * (D.UNCS),2) END AS PROFIT " )
						)->join("PIS_INFO", function($join){
							$join->on("STOCK_INFO.PIS_MK", '=',"PIS_INFO.PIS_MK");
							$join->on("STOCK_INFO.CORP_MK", "=" ,"PIS_INFO.CORP_MK");
						})->leftjoin("CUST_CD",  function($leftjoin){
							$leftjoin->on("STOCK_INFO.CORP_MK",'=', "CUST_CD.CORP_MK")
									->on("STOCK_INFO.CUST_MK", '=',"CUST_CD.CUST_MK");
						})->leftjoin("SALE_INFO_D AS D", function($join){
							$join->on("D.CORP_MK", '=',"STOCK_INFO.CORP_MK")
								 ->on("D.PIS_MK", '=',"STOCK_INFO.PIS_MK")
								 ->on("D.STOCK_INFO_SEQ", '=',"STOCK_INFO.SEQ")
								 ->on("D.SIZES", '=',"STOCK_INFO.SIZES");
						})->where ( 'STOCK_INFO.CORP_MK', $this->getCorpId())
						->where ( 'PIS_INFO.CORP_MK', $this->getCorpId())
						->where(function($query) use ($INPUT_DATE, $OUTPUT_DATE, $INOUT) {
							if( $INPUT_DATE != "" && $this->fnValidateDate($INPUT_DATE)){
								if( $OUTPUT_DATE != "" && $this->fnValidateDate($OUTPUT_DATE)){
									if( $INOUT == "srt_output" ){
										$query->whereBetween("STOCK_INFO.OUTPUT_DATE",  array($INPUT_DATE, $OUTPUT_DATE ));
									}else {
										$query->whereBetween("STOCK_INFO.INPUT_DATE",  array($INPUT_DATE, $OUTPUT_DATE ));
									}
								}
							}
						})
						->where ( 'STOCK_INFO.INPUT_DATE', ">=", $INPUT_DATE)
						->where ( 'STOCK_INFO.INPUT_DATE', "<=", $OUTPUT_DATE)
						->where ( 'STOCK_INFO.SIZES', $TF_SIZE)
						->where ( 'PIS_INFO.PIS_MK', $PIS_MK)
						->where (function($query) use ($CONDITION, $SEARCH_TEXT) {
							
							if( $CONDITION != "" && $SEARCH_TEXT != "none" ){
													
								if( $CONDITION == "SALE_UNCS"){
									if( is_numeric($SEARCH_TEXT) ? $SEARCH_TEXT : -99999999 ){
										$query->where("D.UNCS",  $SEARCH_TEXT );
									}

								}else if( $CONDITION != "" && $CONDITION == "PIS_NM"){
									$query->where("PIS_INFO.PIS_NM",  "like", "%".$SEARCH_TEXT."%");

								}else if( $CONDITION != "" && $CONDITION == "SIZES"){
									$query->where("STOCK_INFO.SIZES", "like", "%".$SEARCH_TEXT."%" );

								}else if( $CONDITION != "" && $CONDITION == "QTY"){
									if( is_numeric($SEARCH_TEXT) ? $SEARCH_TEXT : -99999999 ){
										$query->where("STOCK_INFO.QTY",  $SEARCH_TEXT );
									}

								}else if( $CONDITION != "" && $CONDITION == "FRMN"){
									$query->where("CUST_CD.FRNM",  "like", "%".$SEARCH_TEXT."%" );
								}
								else if( $CONDITION != "" && $CONDITION == "SUJO_NO"){
									$query->where("STOCK_INFO.SUJO_NO",  "like", "%".$SEARCH_TEXT."%");

								}else if(  $CONDITION != "" && $CONDITION == "OUTLINE"){
									$query->where("STOCK_INFO.OUTLINE",  "like", "%".$SEARCH_TEXT."%");

								}else if(  $CONDITION != "" && $CONDITION == "DE_CR_DIV_1"){
									if( is_numeric($SEARCH_TEXT) ? $SEARCH_TEXT : -99999999 ){
										$query->where("STOCK_INFO.DE_CR_DIV",  1 )
											->where("STOCK_INFO.QTY",  $SEARCH_TEXT );
									}
								}else if(  $CONDITION != "" && $CONDITION == "DE_CR_DIV_0"){
									if( is_numeric($SEARCH_TEXT) ? $SEARCH_TEXT : -99999999 ){
										$query->where("STOCK_INFO.DE_CR_DIV",  0 )
											->where("STOCK_INFO.QTY",  $SEARCH_TEXT );
									} 
								}else if(  $CONDITION != "" && $CONDITION == "ALL"){
									
									$query->where(function($q) use ($CONDITION, $SEARCH_TEXT ){
											$q->where("STOCK_INFO.SUJO_NO",  "like", "%".$SEARCH_TEXT."%")
												->orwhere("CUST_CD.FRNM",  "like", "%".$SEARCH_TEXT."%" )
												->orwhere("PIS_INFO.PIS_NM",  "like", "%".$SEARCH_TEXT."%")
												->orwhere("STOCK_INFO.OUTLINE",  "like", "%".$SEARCH_TEXT."%")
												->orwhere("STOCK_INFO.SIZES", "like", "%".$SEARCH_TEXT."%" )
												->orwhere("D.UNCS", ( is_numeric($SEARCH_TEXT) ? (float)$SEARCH_TEXT : -99999999 ) )
												->orwhere("STOCK_INFO.QTY",  ( is_numeric($SEARCH_TEXT) ? (float)$SEARCH_TEXT : -99999999 ) );
									});
												
								}
							}
						
						})
						->orderBy('STOCK_INFO.INPUT_DATE','DESC') 
						->orderBy('STOCK_INFO.SEQ','DESC') 
						->orderBy('STOCK_INFO.OUTPUT_DATE','DESC') 
						->get();

		if( $STOCK_INFO === null || empty($STOCK_INFO) || count($STOCK_INFO) == 0){
			return Redirect::back()->with('alert-success', '재고데이터가 존재 하지 않습니다.')->send();
		}

		$QtySum = DB::table("STOCK_SUJO_VIEW")
					->select(DB::raw("SUM(QTY) AS SUM_QTY"))
					->where("CORP_MK", $this->getCorpId())
					->where("PIS_MK", $PIS_MK)
					->where("SIZES", $TF_SIZE)->first();


		$qtyInputAll	= 0;
		$qtySaleAll		= 0;
		$qtyPROFITAll	= 0;
		foreach($STOCK_INFO as $info){
			$qtyInputAll	+= $info->IN_QTY;
			$qtySaleAll		+= $info->OUT_QTY;
			$qtyPROFITAll	+= $info->PROFIT;
		}

		$pdf = PDF::loadView("stock.Pdf",
								[
									'model'			=> $STOCK_INFO,
									'title'			=> $STOCK_INFO[0]->PIS_NM." [".$STOCK_INFO[0]->SIZES."] 상세재고정보",
									'start'			=> $INPUT_DATE,
									'end'			=> $OUTPUT_DATE,
									'qty'			=> $QtySum->SUM_QTY,
									'qtyInputAll'	=> $qtyInputAll,
									'qtySaleAll'	=> $qtySaleAll,
									'qtyPROFITAll'	=> $qtyPROFITAll,

								]
							);

		return $pdf->stream();

	}

	public function chkHasSujo(){
		
		/*
		$emptySTOCK_SUJO = DB::table('STOCK_SUJO')
			->where("SUJO_NO", Request::Input("SUJO_NO"))
			->whereNull("PIS_MK")
			->whereNull("ORIGIN_CD")
			->where(DB::raw("ISNULL(QTY, 0)"), 0)
			->whereNull("SIZES")
			->count();
		*/	

		$cntSTOCK_SUJO = DB::table('STOCK_SUJO')
						->where("CORP_MK", $this->getCorpId())
						->where("SUJO_NO", Request::Input("SUJO_NO"))
						->where("PIS_MK", Request::Input("PIS_MK"))
						//->where("INPUT_DATE", Request::Input("INPUT_DATE"))
						->where("SIZES", Request::Input("SIZES"))
						->where("ORIGIN_CD", Request::Input("ORIGIN_CD"))->count();
		
		return $cntSTOCK_SUJO > 0  ? "1" : "0";
	}

	// 선택된 수조데이터 조회
	private function getOrginCD()
	{
		return $ORIGIN_CD = DB::table('ORIGIN_CD')->lists('ORIGIN_NM', 'ORIGIN_CD');
	}

	// 재고정보 최대 SEQ 값 조회
	private function getMaxSeqInSTOCKINFO($pSIZES, $pPIS_MK){

		return $max_seq = STOCK_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("PIS_MK", $pPIS_MK)
									->where ("SIZES", $pSIZES)
									->first()->SEQ;

	}
	// 미수 최대 SEQ값 조회
	private function getMaxSeqInUNPROVINFO($pWRITE_DATE){
		return $max_seq = UNPROV_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_GRP;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class AidListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listDataBeforeSum()
	{
		$aidlist = DB::table("CHIT_INFO")
						->select(DB::raw("SUM(CASE WHEN DE_CR_DIV = '0' THEN AMT END) - SUM(CASE WHEN DE_CR_DIV = '1' THEN AMT END)  AS BEFORE_SUM")
						)->where("WRITE_DATE",  "<", Request::Input('start_date'))
							
			->where("CORP_MK","=", $this->getCorpId())
			->first();

		return response()->json($aidlist);

	}

	public function listData()
	{
		$aidlist = DB::table(
								DB::raw("
									(SELECT 
										CHIT_INFO.CORP_MK,
										CHIT_INFO.SEQ,
										CHIT_INFO.WRITE_DATE,
										ACCOUNT_CD.account_mk,
										ACCOUNT_CD.ACCOUNT_NM,
										CUST_CD.FRNM,
										CHIT_INFO.OUTLINE, 
										CASE CHIT_INFO.DE_CR_DIV WHEN '1' THEN CHIT_INFO.AMT END AS AMTT1, 
										CASE CHIT_INFO.DE_CR_DIV WHEN '0' THEN CHIT_INFO.AMT END AS AMTT2  
									FROM 
										CHIT_INFO
											LEFT OUTER JOIN ACCOUNT_CD 
														 ON (CHIT_INFO.CORP_MK=ACCOUNT_CD.CORP_MK and CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK)
											LEFT OUTER JOIN CUST_CD 
														 ON (CHIT_INFO.CUST_MK = CUST_CD.CUST_MK and CHIT_INFO.CORP_MK=CUST_CD.CORP_MK)
									) A
								")
							)
			->select(
				'CORP_MK',
				'SEQ',
				DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" ),
				'ACCOUNT_MK',
				'ACCOUNT_NM',
				'FRNM',
				'OUTLINE',
				DB::raw("ISNULL(AMTT1,0) AS AMT1"), 
				DB::raw("ISNULL(AMTT2,0) AS AMT2")
			)
			->where("A.CORP_MK","=", $this->getCorpId());

		return Datatables::of($aidlist)
				 ->filter(function($query) {
					if( Request::Has('start_date') && Request::Has('end_date')){
						$query->where(function($q){
								$q->where("WRITE_DATE",  ">=", Request::Input('start_date'))
								->where("WRITE_DATE",  "<=", Request::Input('end_date'));
							});
					}
				
					if( Request::Has('ACCOUNT_MK') ){
						$query->where("ACCOUNT_MK","=", Request::Input('ACCOUNT_MK'));
					}
				}) 
				->make(true);
	}

	public function index($cust)
	{
		return view("aidlist.list",[ "cust" => $cust ] );
	}

	//회계계정코드 조회
	public function getAccountingCode()
	{	
		$AccountingCode = DB::table("ACCOUNT_CD")
			->select(
				'ACCOUNT_CD.CORP_MK',
				'ACCOUNT_CD.ACCOUNT_MK',
				'ACCOUNT_CD.ACCOUNT_GRP_CD',
				'ACCOUNT_GRP.ACCOUNT_GRP_NM',
				'ACCOUNT_CD.ACCOUNT_NM',
				DB::raw("CASE ACCOUNT_CD.DE_CR_DIV 
							WHEN 0 THEN '대변'
							WHEN 1 THEN '차변' 
						END AS DE_CR_DIV")
			)
			->join('ACCOUNT_GRP', function($join){
				$join->on("ACCOUNT_CD.CORP_MK", "=" ,"ACCOUNT_GRP.CORP_MK");
				$join->on("ACCOUNT_CD.ACCOUNT_GRP_CD", "=" ,"ACCOUNT_GRP.ACCOUNT_GRP_CD");
			})
			->where("ACCOUNT_CD.CORP_MK","=", $this->getCorpId());

		return Datatables::of($AccountingCode)->make(true);
	}

	public function PdfList(){

		$aidlist = DB::table(
								DB::raw("
									(SELECT 
										CHIT_INFO.CORP_MK,
										CHIT_INFO.SEQ,
										CHIT_INFO.WRITE_DATE,
										ACCOUNT_CD.ACCOUNT_MK,
										ACCOUNT_CD.ACCOUNT_NM,
										CUST_CD.FRNM,
										CHIT_INFO.OUTLINE, 
										CHIT_INFO.REMARK, 
										CASE CHIT_INFO.DE_CR_DIV WHEN '1' THEN AMT END AS AMTT1, 
										CASE CHIT_INFO.DE_CR_DIV WHEN '0' THEN AMT END AS AMTT2  
									FROM 
										CHIT_INFO
										left JOIN ACCOUNT_CD ON (CHIT_INFO.CORP_MK=ACCOUNT_CD.CORP_MK and CHIT_INFO.ACCOUNT_MK = ACCOUNT_CD.ACCOUNT_MK)
										left JOIN CUST_CD ON (CHIT_INFO.CUST_MK = CUST_CD.CUST_MK and CHIT_INFO.CORP_MK=CUST_CD.CORP_MK)
									) A
								")
							)
			->select(
				'A.CORP_MK',
				'A.SEQ',
				DB::raw( "CONVERT(CHAR(10), A.WRITE_DATE, 23) AS WRITE_DATE" ),
				'A.ACCOUNT_MK',
				'A.ACCOUNT_NM',
				'A.FRNM',
				'A.OUTLINE',
				'A.REMARK',
				DB::raw("ISNULL(A.AMTT1,0) AS AMT1"), 
				DB::raw("ISNULL(A.AMTT2,0) AS AMT2")
			)
			->where("A.CORP_MK", $this->getCorpId())
			->where("A.WRITE_DATE", ">=", Request::Input("start_date"))
			->where("A.WRITE_DATE", "<=", Request::Input("end_date"))
			->where(function($query){
					
				if( Request::has("ACCOUNT_MK") && Request::Input("ACCOUNT_MK") != ""){
					$query->where("A.ACCOUNT_MK", Request::Input("ACCOUNT_MK"));
				}
								
			})->orderBy("A.SEQ", "desc")
			->get();
			
			$sumAmt1 = 0;
			$sumAmt2 = 0;
			$injuryAmt	= 0;
			foreach($aidlist as $aid){
			
				$sumAmt1 = $sumAmt1 + $aid->AMT1;
				$sumAmt2 = $sumAmt2 + $aid->AMT2;
				$injuryAmt = $injuryAmt + ($aid->AMT1 - $aid->AMT2);
			}

			$pdf = PDF::loadView("aidlist.pdfList", 
								[
									'list'		=> $aidlist,
									'start_date'=> Request::Input("start_date"),
									'end_date'	=> Request::Input("end_date"),
									'sumAmt1'	=> $sumAmt1,
									'sumAmt2'	=> $sumAmt2,
									'injuryAmt'=> $injuryAmt,
								]
							);

		return $pdf->stream("보조부.pdf");
	
	}

}
<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use App\Http\Requests;

use App\CORP_INFO;
use App\User;
use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_CD;
use App\ACCOUNT_GRP;
use App\CHIT_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class MemberController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	//사원정보 리스트
	public function listData()
	{
		$MEM_INFO = DB::table('MEM_INFO AS MEM')
					->join("CORP_INFO AS CORP", function($join){
						$join->on("MEM.CORP_MK", "=", "CORP.CORP_MK");
					})->select(
								'MEM.id',
								'MEM.MEM_ID',
								'MEM.NAME',
								'MEM.PSTN',
								'MEM.GRP_CD',
								'MEM.PHONE_NO',
								'MEM.HP_NO',
								'MEM.STATE',
								'CORP.FRNM',
								'MEM.CORP_MK'
							)
					->where(function($query){
						if(!$this->chkSysAdmin()){
							$query->where("MEM.CORP_MK", $this->getCorpId());
						}
					}
				);

		return Datatables::of($MEM_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "MEM_ID" ){
							$query->where("MEM_ID",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "NAME" ){
							$query->where("NAME",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "STATE" ){
							$query->where("STATE",  "like", "%".Request::Input('textSearch')."%");
						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "CORP_NM" ){
							$query->where("CORP.FRNM",  "like", "%".Request::Input('textSearch')."%");
						}else{
							$query->where(function($q){
								$q->where("MEM_ID",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("NAME",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("CORP.FRNM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("STATE",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
	}

	//사원정보 조회 페이지
	public function index($member){
		return view("member.list",[ "member" => $member ] );
	}

	//사원정보 상세페이지
	public function detail($member, $corp_mk){
		$MEM_INFO = DB::table('MEM_INFO')
				->where("MEM_ID","=", $corp_mk)
				->first();

		return view("member.detail",
			[ 
				"member" => $member ,
				"corp_mk" => $corp_mk ,
				"mem_info" => $MEM_INFO
			] 
		);
	}

	//사원정보 수정
	public function update($corp, $mem_id){
		if( Request::isMethod('post') ){
			$validator = Validator::make( Request::Input(), [				
				'CORP_MK'			 => 'required|max:20,NOT_NULL',
				'MEM_ID'			 => 'required|max:20,NOT_NULL',
				'PWD_NO'			 => 'min:4|max:50',
				'PWD_NO_CONFIRMATION'=> 'same:PWD_NO',
				'NAME'				 => 'required|max:20, NOT_NULL',
				'JUMIN_NO'			 => 'required|min:7|max:8, NOT_NULL',	
				'EMAIL'				 => 'email',
				'GRP_CD'			 => 'required',
				
			]);

			if ($validator->fails()) {
				return redirect()->back()->withInput(Request::all())->withErrors($validator)->withInput()->send();
			}

			if( !Request::Has('STATE') ){
				$STATE = "0";
			}else{
				$STATE = "1";
			}

			$arrData = [
							"GRP_CD" =>	Request::input('GRP_CD'),
							"GRADE" =>	Request::input('GRADE'),
							"PSTN" =>	Request::input('PSTN'),
							"NAME" =>	Request::input('NAME'),
							"JUMIN_NO" =>	Request::input('JUMIN_NO'),
							"ADDR1" =>	Request::input('ADDR1'),
							"ADDR2" =>	Request::input('ADDR2'),
							"POST_NO" =>	Request::input('POST_NO'),
							"PHONE_NO" =>	Request::input('PHONE_NO'),
							"HP_NO" =>	Request::input('HP_NO'),
							"EMAIL" =>	Request::input('EMAIL'),
							"STATE" => $STATE,
						];
			
			if( Request::input('PWD_NO') != "" ){
				$arrData["password"] = bcrypt(Request::input('PWD_NO'));
			}

			//dd($arrData);

			DB::beginTransaction();
			try {
				DB::table("MEM_INFO")
					->where("MEM_ID", $mem_id )
					->update($arrData);

				// rback 사원 권한 활성화
				$user = User::where("MEM_ID", $mem_id)->first();
				if( $STATE == "1"){
					
					if( !$user->hasRole("AuthUser") ){
						$user->roles()->attach(10);
					}
					 
				}else{
					if( $user->hasRole("AuthUser") ){
						$user->roles()->detach(10);
					}
				}

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}

			DB::commit();
			//return response()->json(['result' => 'success']);
			//return view("corp.list",[ "corp" => $corp ] );
			return Redirect('/member/cust_grp_info/index');
		}else{
			$MEM_INFO = DB::table('MEM_INFO')
				->where("MEM_ID","=", $mem_id)
				->first();
			
			$model = DB::table("GRP_CD")
				->where("CORP_MK",'=', $MEM_INFO->CORP_MK)
				->get();
			
			return view("member.update",
				[ 
					"corp" => $corp ,
					"mem_id" => $mem_id ,
					"mem_info" => $MEM_INFO,
					"GRP_CD" => $model
				] 
			);
		}
	}

	//사원정보->사원등록
	public function insert($member, $corp_mk){
		
		if( Request::isMethod('post') ){
			//return $exception = DB::transaction(function(){
				$validator = Validator::make( Request::Input(), [
					'CORP_MK'			 => 'required|max:20,NOT_NULL',
					'MEM_ID'			 => 'required|max:20,NOT_NULL',
					'PWD_NO'			 => 'required|min:4|max:50,NOT_NULL',
					'PWD_NO_CONFIRMATION'=> 'required|same:PWD_NO',
					'NAME'				 => 'required|max:20, NOT_NULL',
					'JUMIN_NO'			 => 'required|min:7|max:8, NOT_NULL',	
					'EMAIL'				 => 'email',
					'GRP_CD'			 => 'required',
					
				]);

				

				if ($validator->fails()) {
					return redirect()->back()->withInput(Request::all())->withErrors($validator)->withInput()->send();
				}
				
				if( !Request::Has('STATE') ){
					$STATE = "0";
				}else{
					$STATE = "1";
				}
		
				try {
					
					// 동일한 키가 있는 경우 실패메시지
					$count = DB::table("MEM_INFO")
						//->where('CORP_MK', Request::Input("CORP_MK"))
						->where('MEM_ID', Request::Input("MEM_ID"))
						->count();
					
					// 동일한 키가 있는 경우 실패메시지
					$countJumin = DB::table("MEM_INFO")
						//->where('CORP_MK', Request::Input("CORP_MK"))
						->where('JUMIN_NO', Request::Input("JUMIN_NO"))
						->count();

					if( $count > 0){
						return back()->withErrors([
							'MEM_ID' => '입력한 아이디가 있습니다.',
						])->withInput();

					}else if( $countJumin  > 0){
						return back()->withErrors([
							'JUMIN_NO' => '동일한 주민번호가 있습니다.',
						])->withInput();

					} else{
						
						//dd( Request::input());
						DB::table("MEM_INFO")
							->insert(
								[
									"MEM_ID" =>	Request::input('MEM_ID'),
									//"PWD_NO" =>	DB::raw("PWDENCRYPT(".Request::input('PWD_NO').")"),
									"GRP_CD" =>	Request::input('GRP_CD'),
									"CORP_MK" => Request::input('CORP_MK'),
									"GRADE" =>	Request::input('GRADE'),
									"PSTN" =>	Request::input('PSTN'),
									"NAME" =>	Request::input('NAME'),
									"JUMIN_NO" =>	Request::input('JUMIN_NO'),
									"ADDR1" =>	Request::input('ADDR1'),
									"ADDR2" =>	Request::input('ADDR2'),
									"POST_NO" =>	Request::input('POST_NO'),
									"PHONE_NO" =>	Request::input('PHONE_NO'),
									"HP_NO" =>	Request::input('HP_NO'),
									"EMAIL" =>	Request::input('EMAIL'),
									"STATE" => $STATE,
									"REG_IP" => Request::ip(),
									"password" => bcrypt(Request::input('PWD_NO')),
									"remember_token" => Request::input('_token'),
								]
							);
						
						if( $STATE == "1"){
							// 회원가입시 권한 자동할당 구현
						}
						

						//DB::commit();
						return Redirect('/member/member/index');
					}
				}catch(\ValidationException $e){
					DB::rollback();

					$errors = $e->errors();
					$errors =  json_decode($errors); 
					return response()->json(['result' => 'fail', 'message' => $errors], 422);

				}catch(\Exception $e){
					DB::rollback();
					if (Request::ajax() || Request::wantsJson()) {
						$message = $e->getMessage();
						if (is_object($message)) { 
							$message = $message->toArray(); 
						}
						
						return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
					}else{
						return $e;
					}
				}
			//});
		}else{
			
			$model = DB::table("GRP_CD")
				//->where("CORP_MK",'=', Request::segment(4))
				->get();
			
			if( count($model) == 0){

				return view("member.insert",
					[ 
						"member" => $member,
						"corp_mk" => $corp_mk,
						"GRP_CD" => $model
					] 
				)->withErrors([
					'GRP_CD' => '그룹정보메뉴에서 그릅코드를를 먼저 등록하세요',
				]);;
			
			}else{
				return view("member.insert",
					[ 
						"member" => $member,
						"corp_mk" => $corp_mk,
						"GRP_CD" => $model
					] 
				);
			}
		}
	}

	//선택된 사원정보 삭제
	public function _delete(){

		$corp_id = Auth::user()->CORP_ID;

		if (Request::ajax()) {
			DB::table("MEM_INFO")
				->where("id", Request::get('cd'))
				->delete();

			return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
		}
		return Response::json(['result' => 'failed']);
	}

	// View : 급여정보 조회화면
	public function salary($member){
		
		return view("member.salaryList",[ "member" => $member ] );
	}
	
	// DataTable : 급여정보 조회데이터
	public function listSalary(){

		$SALARY_INFO = DB::table('SALARY_INFO AS S')
			->join("MEM_INFO AS M", function($join){
				$join->on("S.MEM_ID", "=", "M.MEM_ID");
			})->select(
						 'M.NAME'
						,'S.MEM_ID'
						, DB::raw( "CONVERT(CHAR(10), S.PROV_DATE, 23) AS PROV_DATE" )
						,'S.PROV_AMT'
						,'S.FOOD_CHG'
						,'S.PARTN_YN'
						,'S.DFDT_FMLY_CNT'	
						,'S.TROUBLE_CNT'
						,'S.OLD_TREAT_CNT'	
						,'S.MEDI_INSUR_RANK'	
						,'S.MEDI_INSUR_PAY'	
						,'S.NATN_PS_RANK'	
						,'S.NATN_PS_PAY'	
						,'S.EMPLY_INSUR_PAY'	
						,'S.JUMIN_TAX'	
						,'S.GABGUN_TAX'	
						,'S.DEFAULT_PAY'	
						,'S.ETC_CHECK_OFF'	
						,'S.ETC_ALLOWANCE'	
						,'S.PROV_AMT'
						,'S.FOOD_CHG'
						, DB::raw("( S.MEDI_INSUR_PAY + S.NATN_PS_PAY + S.EMPLY_INSUR_PAY + S.JUMIN_TAX + S.GABGUN_TAX + S.ETC_CHECK_OFF) AS DEDUCATION")
						, DB::raw("( S.PROV_AMT - (S.MEDI_INSUR_PAY + S.NATN_PS_PAY + S.EMPLY_INSUR_PAY + S.JUMIN_TAX + S.GABGUN_TAX + S.ETC_CHECK_OFF)) AS REAL_PAY")
					)
			//->where('CORP_MK', $this->getCorpId())
			->where("S.MEM_ID", Request::Input("MEM_ID"));
			//->where("S.MEM_ID", "PISCES");
						
		return Datatables::of($SALARY_INFO)
			->filter(function($query) {
					if( Request::Has('start_dt') && $this->fnValidateDate(Request::Input("start_dt"))){
						if( Request::Has('end_dt') && $this->fnValidateDate(Request::Input("end_dt"))){
							$query->whereBetween("S.PROV_DATE",  array(Request::Input("start_dt"), Request::Input("end_dt") ));
						}
					}
			})->make(true);
	}



	// 급여조회 
	public function getSalary(){
		
		$SALARY_INFO = DB::table('SALARY_INFO AS S')
			->join("MEM_INFO AS M", function($join){
				$join->on("S.MEM_ID", "=", "M.MEM_ID");
			})->select(
						 'M.NAME'
						,'S.MEM_ID'
						, DB::raw( "CONVERT(CHAR(10), S.PROV_DATE, 23) AS PROV_DATE" )
						,'S.PROV_AMT'
						,'S.FOOD_CHG'
						,'S.PARTN_YN'
						,'S.DFDT_FMLY_CNT'	
						,'S.TROUBLE_CNT'
						,'S.OLD_TREAT_CNT'	
						,'S.MEDI_INSUR_RANK'	
						,'S.MEDI_INSUR_PAY'	
						,'S.NATN_PS_RANK'	
						,'S.NATN_PS_PAY'	
						,'S.EMPLY_INSUR_PAY'	
						,'S.JUMIN_TAX'	
						,'S.GABGUN_TAX'	
						,'S.DEFAULT_PAY'	
						,'S.ETC_CHECK_OFF'	
						,'S.ETC_ALLOWANCE'	
						,'S.PROV_AMT'
						,'S.FOOD_CHG'
						, DB::raw("( S.MEDI_INSUR_PAY + S.NATN_PS_PAY + S.EMPLY_INSUR_PAY + S.JUMIN_TAX + S.GABGUN_TAX + S.ETC_CHECK_OFF) AS DEDUCATION")
						, DB::raw("( S.PROV_AMT - (S.MEDI_INSUR_PAY + S.NATN_PS_PAY + S.EMPLY_INSUR_PAY + S.JUMIN_TAX + S.GABGUN_TAX + S.ETC_CHECK_OFF)) AS REAL_PAY")
					)
			//->where('CORP_MK', $this->getCorpId())
			->where("S.MEM_ID", Request::Input("MEM_ID"))
			->where("S.PROV_DATE", Request::Input("PROV_DATE"))
			->first();

		return Response::json(['result' => 'success', 'data' =>$SALARY_INFO]);
	}
	
	//급여수정
	public function updateSalary(){
	
		$validator = Validator::make( Request::Input(), [
			'MEM_ID'		=> 'required|max:20,NOT_NULL',
			'PROV_DATE'		=> 'required|date_format:"Y-m-d',

			'PROV_AMT'		=> 'numeric',
			'FOOD_CHG'		=> 'numeric',
			'DFDT_FMLY_CNT'		=> 'numeric',
			'TROUBLE_CNT'=> 'numeric',
			'OLD_TREAT_CNT'	=> 'numeric',
			'MEDI_INSUR_RANK'	=> 'numeric',
			'MEDI_INSUR_PAY'	=> 'numeric',
			'NATN_PS_RANK'	=> 'numeric',
			'NATN_PS_PAY'	=> 'numeric',
			'EMPLY_INSUR_PAY'	=> 'numeric',
			'JUMIN_TAX'	=> 'numeric',
			'GABGUN_TAX'	=> 'numeric',
			'DEFAULT_PAY'	=> 'numeric',
			'ETC_CHECK_OFF'	=> 'numeric',
			'ETC_ALLOWANCE'	=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		return $exception = DB::transaction(function(){

			try {
				

				DB::table("SALARY_INFO")
					->where("MEM_ID", Request::Input("MEM_ID"))
					->where("PROV_DATE", Request::Input("PROV_DATE"))
					->update( 
					[
						  'MEM_ID'			=> Request::Input("MEM_ID")
						, 'PROV_DATE'		=> Request::Input("PROV_DATE")
						, 'PROV_AMT'		=> Request::Input("PROV_AMT")
						, 'FOOD_CHG'		=> Request::Input("FOOD_CHG")
						, 'PARTN_YN'		=> Request::Input("PARTN_YN")
						, 'DFDT_FMLY_CNT'	=> Request::Input("DFDT_FMLY_CNT")
						, 'TROUBLE_CNT'		=> Request::Input("TROUBLE_CNT")
						, 'OLD_TREAT_CNT'	=> Request::Input("OLD_TREAT_CNT")
						, 'MEDI_INSUR_RANK'	=> Request::Input("MEDI_INSUR_RANK")
						, 'MEDI_INSUR_PAY'	=> Request::Input("MEDI_INSUR_PAY")
						, 'NATN_PS_RANK'	=> Request::Input("NATN_PS_RANK")
						, 'NATN_PS_PAY'		=> Request::Input("NATN_PS_PAY")
						, 'EMPLY_INSUR_PAY'	=> Request::Input("EMPLY_INSUR_PAY")
						, 'JUMIN_TAX'		=> Request::Input("JUMIN_TAX")
						, 'GABGUN_TAX'		=> Request::Input("GABGUN_TAX")
						, 'DEFAULT_PAY'		=> Request::Input("DEFAULT_PAY")
						, 'ETC_CHECK_OFF'	=> Request::Input("ETC_CHECK_OFF")
						, 'ETC_ALLOWANCE'	=> Request::Input("ETC_ALLOWANCE")
					]
				);
				return response()->json(['result' => 'success']);

			}catch(\ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}
			}
		});
	}

	// 급여저장
	public function setSalary(){
		
		$validator = Validator::make( Request::Input(), [
			'MEM_ID'		=> 'required|max:20,NOT_NULL',
			'PROV_DATE'		=> 'required|date_format:"Y-m-d',

			'PROV_AMT'		=> 'numeric',
			'FOOD_CHG'		=> 'numeric',
			'DFDT_FMLY_CNT'		=> 'numeric',
			'TROUBLE_CNT'=> 'numeric',
			'OLD_TREAT_CNT'	=> 'numeric',
			'MEDI_INSUR_RANK'	=> 'numeric',
			'MEDI_INSUR_PAY'	=> 'numeric',
			'NATN_PS_RANK'	=> 'numeric',
			'NATN_PS_PAY'	=> 'numeric',
			'EMPLY_INSUR_PAY'	=> 'numeric',
			'JUMIN_TAX'	=> 'numeric',
			'GABGUN_TAX'	=> 'numeric',
			'DEFAULT_PAY'	=> 'numeric',
			'ETC_CHECK_OFF'	=> 'numeric',
			'ETC_ALLOWANCE'	=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		
		return $exception = DB::transaction(function(){

			try {
				
				if(  DB::table("SALARY_INFO")->where("MEM_ID", Request::Input("MEM_ID"))->where("PROV_DATE", Request::Input("PROV_DATE"))->count() > 0 ){
					return response()->json(['result' => 'DUPLICATION', 'message' => "같은 데이터가 존재합니다."], 422);	
				}

				DB::table("SALARY_INFO")->insert( 
					[
						  'MEM_ID'			=> Request::Input("MEM_ID")
						, 'PROV_DATE'		=> Request::Input("PROV_DATE")
						, 'PROV_AMT'		=> Request::Input("PROV_AMT")
						, 'FOOD_CHG'		=> Request::Input("FOOD_CHG")
						, 'PARTN_YN'		=> Request::Input("PARTN_YN")
						, 'DFDT_FMLY_CNT'	=> Request::Input("DFDT_FMLY_CNT")
						, 'TROUBLE_CNT'		=> Request::Input("TROUBLE_CNT")
						, 'OLD_TREAT_CNT'	=> Request::Input("OLD_TREAT_CNT")
						, 'MEDI_INSUR_RANK'	=> Request::Input("MEDI_INSUR_RANK")
						, 'MEDI_INSUR_PAY'	=> Request::Input("MEDI_INSUR_PAY")
						, 'NATN_PS_RANK'	=> Request::Input("NATN_PS_RANK")
						, 'NATN_PS_PAY'		=> Request::Input("NATN_PS_PAY")
						, 'EMPLY_INSUR_PAY'	=> Request::Input("EMPLY_INSUR_PAY")
						, 'JUMIN_TAX'		=> Request::Input("JUMIN_TAX")
						, 'GABGUN_TAX'		=> Request::Input("GABGUN_TAX")
						, 'DEFAULT_PAY'		=> Request::Input("DEFAULT_PAY")
						, 'ETC_CHECK_OFF'	=> Request::Input("ETC_CHECK_OFF")
						, 'ETC_ALLOWANCE'	=> Request::Input("ETC_ALLOWANCE")
					]
				);
				return response()->json(['result' => 'success']);

				

			}catch(\ValidationException $e){
				DB::rollback();

				$errors = $e->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);

			}catch(\Exception $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}
			}
		});
	}

	public function deleteSalary(){
		

		if (Request::ajax()) {
			DB::table("SALARY_INFO")
				->where("MEM_ID", Request::Input('MEM_ID'))
				->where(DB::raw("CONVERT(CHAR(10), PROV_DATE, 23)"), Request::Input('PROV_DATE'))
				->delete();

			return Response::json(['result' => 'success']);
		}
		return Response::json(['result' => 'failed'], 442);
	}

	// 급여 입력시 건강/주민/갑근/의료 계산
	public function GetTax(){
		
	
		// 의료보험 등급
		$MEDI_INS_TBL = DB::table("MEDI_INSUR_TABLE")
						->where("SALARY_MIN", "<=", (int)Request::Input("DEFAULT_PAY"))
						->where("SALARY_MAX", ">", (int)Request::Input("DEFAULT_PAY"))
						->first();

		$MEDI_RANK		= $MEDI_INS_TBL->MEDI_INSUR_RANK;
		$MEDI_INSU_AMT	= (int)$MEDI_INS_TBL->MEDI_INSUR_PAY;

		// 국민연금 등급
		$NATN_PS_TBL = DB::table("NATN_PS_TABLE")
						->where("SALARY_MIN", "<=", (int)Request::Input("DEFAULT_PAY"))
						->where("SALARY_MAX", ">", (int)Request::Input("DEFAULT_PAY"))
						->first();

		$NATN_RANK		= $NATN_PS_TBL->NATN_PS_RANK;
		$NATN_AMT		= (int)$NATN_PS_TBL->NATN_PS_AMT;

		// 세금
		$TAX_TBL	 = DB::table("TAX_TABLE")
						->where("SALARY_MIN", "<=", (int)Request::Input("DEFAULT_PAY"))
						->where("SALARY_MAX", ">", (int)Request::Input("DEFAULT_PAY"))
						->where("DFDT_FMLY_CNT", (int)Request::Input("FAMI_CNT"))
						->first();
		
		// 갑근세
		$GABG_TAX = 0;
		if( $TAX_TBL !== null){
			// 갑근세
			$GABG_TAX	 = (int)$TAX_TBL->GABGUN_TAX;			
		}
		
		//고용보험료(원청징수 0.45%)
		$EMP_ISUPAY	 = (int)floor(((float)Request::Input("DEFAULT_PAY") * (float)0.45) / 100);

		// 주민세
		$JUMIN_TAX	 = (int)floor((((float)$GABG_TAX * (float)(0.1)) / 10) * 10);

		return Response::json(
								[
									 'MEDI_RANK'		=> $MEDI_RANK
									,'MEDI_INSU_AMT'	=> $MEDI_INSU_AMT
									,'NATN_RANK'		=> $NATN_RANK
									,'NATN_AMT'			=> $NATN_AMT
									,'EMP_ISUPAY'		=> $EMP_ISUPAY
									,'JUMIN_TAX'		=> $JUMIN_TAX
									,'GABG_TAX'			=> $GABG_TAX
								]
							);
	}

	
}


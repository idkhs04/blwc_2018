<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Role;
use App\Permission;
use App\User;

class EntrustInitController extends Controller
{
    public function initRole(){
        
        $owner = new Role();
        $owner->name         = 'owner';
        $owner->display_name = '최고관리자'; // optional
        $owner->description  = '프로젝트 최고 관리자 권한'; // optional
        $owner->save();
        
        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = '시스템관리자'; // optional
        $admin->description  = '시스템 관리자 권한'; // optional
        $admin->save();
        
        $user = User::where('username', '=', 'admin')->first();
        $user->attachRole($owner);
        //$user->roles()->attach($owner->id);

        
        $user2 = User::where('username', '=', 'hyosik')->first();
        $user2->attachRole($admin);
        //$user2->roles()->attach($admin->id);

        
        $createPost = new Permission();
        $createPost->name         = 'create-post';
        $createPost->display_name = 'Create Posts'; // optional
        // Allow a user to...
        $createPost->description  = 'create new blog posts'; // optional
        $createPost->save();
        
        $editUser = new Permission();
        $editUser->name         = 'edit-user';
        $editUser->display_name = 'Edit Users'; // optional
        // Allow a user to...
        $editUser->description  = 'edit existing users'; // optional
        $editUser->save();
        
        $admin->attachPermission($createPost);
        // equivalent to $admin->perms()->sync(array($createPost->id));
        
        $owner->attachPermissions([$createPost, $editUser]);
        // equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));
    }
}

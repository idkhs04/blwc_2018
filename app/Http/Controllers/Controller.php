<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Datatables;
use App\Http\Requests;
use Session;
use App\User;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use role;
use permission;
use ability;
use Config;


class Controller extends BaseController 
{
	use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests, EntrustUserTrait;
	
	function __construct(){
		
		
		// Connection 정보를 세션으로 된다.
		/*
		if( Session::get('database.default') != null){
			Config::set('database.default', Session::get('database.default'));
		}else{
			Config::set('database.default', 'sqlsrv');
		}
		DB::reconnect( Config::get('database.default') );
		*/
		
		$this->middleware("chksession")->except([]);

		if(Session::has('MEM_ID')){

			$user = User::where("MEM_ID", Auth::user()->MEM_ID)->first();
			
			if (Request::ajax()) {
				return response()->json(['result' => 'Unauthorized'], 401); 
			}else{

				// 일반인증된 사용자 확인
				if( ( !$user->hasRole(['AuthUser']))) {
					redirect::to('/member/login')->send();
				}

				if( !Session::has("CORP_MK") || is_null(Session::get("CORP_MK")) || empty(Session::get("CORP_MK"))   ){
					redirect::to('/member/login')->send();
				}
			}
		}else{
			
			if (Request::ajax()) {
				return response()->json(['result' => 'Unauthorized'], 401); 
			}else{
				redirect::to('/member/login')->send();
			}
		}
	}

	// 데이터베이스 커넥션 변경
	public function changeConnection()
	{
		//DB::connection()->setDatabaseName('blwc');
		dd(DB::connection()->getDatabaseName());
		//$this->connection = $conn;
	}
	public function errList(){

		return view("config.list");
	
	}

	// 시스템 관리자용 업체별 세션 변경 (a/s용)
	public function setLoginByAdmin($CORP_MK){
		
		if($this->chkSysAdmin()){
			Session::put('CORP_MK', $CORP_MK);
			
			return redirect('/');
		}else{
			redirect::to('/member/login')->send();
		}
	}

	// 백업관리자용 업체별 세션 변경 (백업사이트 조회용)
	public function setLoginByBackup($CORP_MK){

		$CORP_INFO = DB::table("CORP_INFO")->where("CORP_MK", $CORP_MK)->first();

		if( $CORP_INFO != null ){
			return view("corp.backup_frame",  ['CORP_MK' => $CORP_MK, 'CORP_NM' => $CORP_INFO->FRNM]);
		}else{
			return view("corp.backup", ['CORP_MK' => $corp_mk]);
		}
	}
	
	public function setFeedBack(){
		
		$params = json_decode($_POST["data"]);
		DB::table("TB_FEEDBACK")->insert(
			[
				 'ISSUE'		=> urldecode($params[0]->Issue)
				,'ISSUE_URL'	=> urldecode($params[0]->issueurl)
				,'USER_SESSION'	=> Session::get('MEM_ID')
				,'IMAGE'		=> $params[1]
			]
		);

		return response()->json(['result' => 'success'], 200); 
	}

	// 메뉴얼 보기 페이지 
	public function indexMenual(){
		
		return view("config.menual");
	}

	// 개인정보 취급 방침
	public function notice(){

		/*
		$request = \Illuminate\Http\Request::create('http://opendata.busan.go.kr/openapi/service/GoodPriceStoreService/getGoodPriceStoreList', 'GET', 
					[
						'serviceKey' => 'hRqLic00bWcXWvQu5PjLP0wg2QlGxi20PQYi1CCsMIjeS0G7v90uwUnLeVlZnrU7hJI%2BDJ71jfCfgbRTU6Loiw%3D%3D', 
						'numOfRows' => '10',
						'pageNo' => '1'
					]
				);

		*/

		//$response = Route::dispatch($request)
		//dd($request);
		
		return view("config.notice");
	}


	public function getFeedBack(){
		
		// 오류개선 조회 화면 
		if( Request::Input("type") == "DT"){
			$feedback = DB::table("TB_FEEDBACK AS FB")
					//->whereIn("FB.IS_CMPLT", ["0", null])
					->select(
						 "FB.FEEDIDX"
						, DB::raw("CASE WHEN DATALENGTH(FB.ISSUE) > 20
										THEN SUBSTRING(FB.ISSUE,1,20)+'...' ELSE FB.ISSUE END AS ISSUE")
						, DB::raw("FB.ISSUE AS ORIGIN_ISSUE")
						,'FB.ISSUE_URL'
						,'FB.USER_SESSION'	
						,'FB.IMAGE'
						,'FB.IS_CMPLT'
						, DB::raw("CASE WHEN FB.IS_CMPLT  = '1' THEN '완료' ELSE '대기' END AS CMPLT_NM")
						,'MEM.NAME'
						,'CORP.FRNM'
						, DB::raw("DATEDIFF(day, GETDATE(), FB.WRITEDT) AS WRITE_DT")
						, DB::raw("CONVERT(CHAR(10), FB.WRITEDT, 23) AS INPUT_DATE" )
					)->join("MEM_INFO AS MEM", function($join){
						$join->on("FB.USER_SESSION", "=", "MEM.MEM_ID");
					})->leftjoin("CORP_INFO AS CORP", function($join){
						$join->on("CORP.CORP_MK", "=", "MEM.CORP_MK");
					});
			return Datatables::of($feedback)->make(true);
		}else{
		// 오류조회 갯수 확인
			$feedback = DB::table("TB_FEEDBACK AS FB")
					//->whereIn("FB.IS_CMPLT", ["0", null])
					->select(
						 "FB.FEEDIDX"
						, DB::raw("CASE WHEN DATALENGTH(FB.ISSUE) > 20
										THEN SUBSTRING(FB.ISSUE,1,20)+'...' ELSE FB.ISSUE END AS ISSUE")
						, DB::raw("FB.ISSUE AS ORIGIN_ISSUE")
						,'FB.ISSUE_URL'
						,'FB.USER_SESSION'	
						,'FB.IS_CMPLT'
						, DB::raw("CASE WHEN FB.IS_CMPLT  = '1' THEN '완료' ELSE '대기' END AS CMPLT_NM")
						,'MEM.NAME'
						,'CORP.FRNM'
						, DB::raw("DATEDIFF(day, GETDATE(), FB.WRITEDT) AS WRITE_DT")
						, DB::raw("CONVERT(CHAR(10), FB.WRITEDT, 23) AS INPUT_DATE" )
					)->join("MEM_INFO AS MEM", function($join){
						$join->on("FB.USER_SESSION", "=", "MEM.MEM_ID");
					})->leftjoin("CORP_INFO AS CORP", function($join){
						$join->on("CORP.CORP_MK", "=", "MEM.CORP_MK");
					});
			return response()->json(['result' => $feedback->take(10)->get(), 'cnt' =>$feedback->count() ]); 
		}

	}

	
	public function getFeedBackDetail(){
			
		$feedbackDetail = DB::table("TB_FEEDBACK AS FB")
						->select(
							 "FB.FEEDIDX"
							,"FB.ISSUE"
							,'FB.ISSUE_URL'
							,'FB.USER_SESSION'	
							,'FB.IMAGE'
							,'FB.IS_CMPLT'
							, DB::raw("CASE WHEN FB.IS_CMPLT  = '1' THEN '완료' ELSE '대기' END AS CMPLT_NM")
							, DB::raw("DATEDIFF(day, GETDATE(), FB.WRITEDT) AS WRITE_DT")
							, DB::raw("CONVERT(CHAR(10), FB.WRITEDT, 23) AS INPUT_DATE" )
						)->where("FB.FEEDIDX", Request("FEEDIDX"));

		return response()->json(['result' => $feedbackDetail->first()]);
	}


	public function setComplete(){
		
		$result =  DB::table("TB_FEEDBACK")
			->where("FEEDIDX", Request("FEEDIDX"))
			->update(
				[
					'IS_CMPLT' => Request::Input('IS_CMPLT')						  
				]	
			);

		return response()->json(['result' => 'success']);
		
	}

	public function downMenual1(){
		
		$file= public_path(). "/menual/blwc_menual_0_v_0.pdf";
		return response()->download($file);
	}

	public function downMenual2(){
		
		$file= public_path(). "/menual/blwc_menual_v0.1.pdf";
		return response()->download($file);
	}


	public function chkSysAdmin(){
		
		$user = User::where("MEM_ID", Auth::user()->MEM_ID)->first();
		// 일반인증된 사용자 확인
		if( ( $user->hasRole(['sysAdmin']))) {
			return true;
		}
		return false;
	
	}
	
	// 백업사용자 권한 부여
	private function SetBackupUser(){
		
		$user = User::where("MEM_ID", Auth::user()->MEM_ID)->first();
		
		$user->attachRole(4);
		return true;

		if($user->hasRole(['backup'])){
			return true;
		}else{
			$user->attachRole(['backup']);
			return true;
		}
	
	}

	public function getCorpId(){

		
		if( Session::has("CORP_MK") && !is_null(Session::get("CORP_MK")) && !empty(Session::get("CORP_MK"))   ){
			return Session::get('CORP_MK');
		}else{
			return redirect('/member/login')->send() ;
			die();
		}
	}

	// 수조 체크 표시
	public function setUseChkSujoDisplay(){
		$result =  DB::table("MEM_INFO")
			->where("CORP_MK", $this->getCorpId())	
			->update(
				[
					'CONF_CHK_SUJO' => Request::Input('CONF_CHK_SUJO')
				]
		);
		
		if( $result ){
			$model =  DB::table("MEM_INFO")->where("CORP_MK", $this->getCorpId())->first();
			return response()->json(['result' => 'success', 'model' => $model ]); 
		}else{
			return response()->json(['result' => 'fail'], 422); 
		}

	}

	// 수조의 거래처 표기 
	public function setUseConfigSujocust(){
		$result =  DB::table("MEM_INFO")
			->where("CORP_MK", $this->getCorpId())	
			->update(
				[
					'SUJO_CUST_YN' => Request::Input('SUJO_CUST_YN')
				]	
		);
		
		if( $result ){
			$model =  DB::table("MEM_INFO")->where("CORP_MK", $this->getCorpId())->first();
			return response()->json(['result' => 'success', 'model' => $model ]); 
		}else{
			return response()->json(['result' => 'fail'], 422); 
		}
	}

	// 수량단위(마리 사용)
	public function setConfigQty(){
	
		$result =  DB::table("MEM_INFO")
			->where("CORP_MK", $this->getCorpId())	
			->update(
				[
					'CONF_QTY' => Request::Input('CONFIG_QTY')
				]	
		);
		
		if( $result ){
			$model =  DB::table("MEM_INFO")->where("CORP_MK", $this->getCorpId())->first();
			return response()->json(['result' => 'success', 'model' => $model ]); 
		}else{
			return response()->json(['result' => 'fail'], 422); 
		}
	}

	public function setMenutoggle(){
		
		$result =  DB::table("MEM_INFO")
			->where("CORP_MK", $this->getCorpId())	
			->update(
				[
					'CONF_MENU_TOGGLE' => Request::input("CONFIG_TOGGLE")
				]	
		);
		
		if( $result ){
			$model =  DB::table("MEM_INFO")->where("CORP_MK", $this->getCorpId())->first();
			return response()->json(['result' => 'success', 'model' => $model ]); 
		}else{
			return response()->json(['result' => 'fail'], 422); 
		}
	
	}

	public function getConfig(){

		$model =  DB::table("MEM_INFO")
				->where("CORP_MK", $this->getCorpId())
				->select(
						   "id"
						  ,"MEM_ID"
						  ,"MEM_TYPE"
						  ,"GRP_CD"
						  ,"CORP_MK"
						  ,"GRADE"
						  ,"PSTN"
						  ,"NAME"
						  ,"STATE"
						  ,"MAIL_CHK"
						  ,"REG_DATE"
						  ,"PWD_DATE"
						  ,"PWD_CNT"
						  ,"CONF_QTY"
						  ,"CONF_SKIN"
						  ,"SUJO_CUST_YN"
						  ,"CONF_MENU_TOGGLE"
						  ,"CONF_CHK_SUJO"
				)->first();

		return response()->json(['result' => 'success', 'model' => $model ]); 
	}

	// 스킨 설정
	public function setConfigSkin(){
		
		
		$result =  DB::table("MEM_INFO")
			->where("CORP_MK", $this->getCorpId())	
			->update(
				[
					'CONF_SKIN' => Request::Input('CONFIG_SKIN')
				]	
		);
		
		if( $result ){
			$model =  DB::table("MEM_INFO")
						->where("CORP_MK", $this->getCorpId())
						->select(
								   "id"
								  ,"MEM_ID"
								  ,"MEM_TYPE"
								  ,"GRP_CD"
								  ,"CORP_MK"
								  ,"GRADE"
								  ,"PSTN"
								  ,"NAME"
								  ,"JUMIN_NO"
								  ,"ADDR1"
								  ,"ADDR2"
								  ,"POST_NO"
								  ,"PHONE_NO"
								  ,"HP_NO"
								  ,"EMAIL"
								  ,"STATE"
								  ,"MAIL_CHK"
								  ,"REG_DATE"
								  ,"REG_IP"
								  ,"PWD_DATE"
								  ,"PWD_CNT"
								  ,"CONF_QTY"
								  ,"CONF_SKIN"
						)->first();
			return response()->json(['result' => 'success', 'model' => $model ]); 
		}else{
			return response()->json(['result' => 'fail'], 422); 
		}
	}
	
	// MODEL(1) : 회사정보
	public function getCorpInfo(){
		return DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->first();
	}

	public function fnValidateDate($date){
		if( preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
			return true;
		else return false;
	}

	public function getCorpInfoJSON(){
		$model = DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->first();
		return response()->json($model); 
	}

}

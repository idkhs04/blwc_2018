<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\STOCK_SUJO;
use App\PIS_INFO;
use App\ORIGIN_CD;
use App\PIS_CLASS_CD;
use App\CUST_CD;
use App\STOCK_INFO;
use App\UNPROV_INFO;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use DateTime;
use Watson\Validating\ValidationException;

use App\User;


class StockSujoController extends Controller
{
	public function index($buy)
	{	
		return view("sujo.list", [ "buy" => $buy] );
	}
	// 수조(매입관리) 조회
	// 수정 : 2016-09-26 ( (일일판매) 매입처는 최신 업체 한군대만 조회 되도록 한다) : 주석처리(with View)
	public function listData()
	{
		
		// 매입처도 추가 
		// 재고테이블의 출고가 없는 업체가 입고거래처임 OUTPUT_DATE IS NULL 추가 
		// 2016-11-30
		$C_MK = $this->getCorpId();
		
		$STOCK_SUJO = STOCK_SUJO::select( 
					  'PIS_INFO.PIS_NM'
					, 'STOCK_SUJO.ORIGIN_NM'
					, 'STOCK_SUJO.CORP_MK'
					, 'STOCK_SUJO.SUJO_NO'
					, 'STOCK_SUJO.PIS_MK'
					, 'STOCK_SUJO.SIZES'
					, 'STOCK_SUJO.IS_CHECK'
					, DB::raw( "CONVERT(CHAR(10), STOCK_SUJO.INPUT_DATE, 23) AS INPUT_DATE" )
					, DB::raw( "ISNULL(ROUND(STOCK_SUJO.QTY, 2), 0) AS QTY")
					, DB::raw( "ISNULL(ROUND(STOCK_SUJO.SALE_UNCS, 2), 0) AS SALE_UNCS")
					, 'STOCK_SUJO.ORIGIN_CD'
					, DB::raw( "C.FRNM AS CUST_LIST")
					, "C.CUST_MK"
					, DB::raw("(SELECT TOP 1 CONF_CHK_SUJO FROM MEM_INFO WHERE CORP_MK='".$this->getCorpId()."') AS CONF_CHK_SUJO")
				)->leftjoin("PIS_INFO", function($join){
					$join->on("STOCK_SUJO.PIS_MK", "=", "PIS_INFO.PIS_MK");
					$join->on("STOCK_SUJO.CORP_MK", "=", "PIS_INFO.CORP_MK");
				})->leftjoin("CUST_CD AS C", function($join){
					$join->on("STOCK_SUJO.CUST_MK", "=", "C.CUST_MK");
					$join->on("STOCK_SUJO.CORP_MK", "=", "C.CORP_MK");
				})->where("STOCK_SUJO.CORP_MK", $this->getCorpId());
			
		return Datatables::of($STOCK_SUJO)
			
				->filter(function($query) {

					if( Request::Has('start_date') && $this->fnValidateDate(Request::Input("start_date"))){
						if( Request::Has('end_date') && $this->fnValidateDate(Request::Input("end_date"))){
							$query->whereBetween("INPUT_DATE",  array(Request::Input("start_date"), Request::Input("end_date") ));
						}
					}
					
					if ( !Request::Has("HAS_EMPTY") && Request::Input("HAS_EMPTY") != "N"){
						// 빈수조도 조회되도록 설정함
						$query->orwhere(function($q){
								$q->whereNull("INPUT_DATE")->where("STOCK_SUJO.CORP_MK", $this->getCorpId());
						});
						
					}else{
						// 빈수조는 조회안되도록 설정 (일일판매에서 수조 조회시)
						$query->where(function($q){
								$q->whereNotNull("INPUT_DATE");
						});
					}
					
					if( Request::Has('textSearch') ){
						if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SALE_UNCS"){
							$query->where("SALE_UNCS",  Request::Input('textSearch') );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "PIS_NM"){
							$query->where("PIS_NM",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SIZES"){
							$query->where("SIZES",  "like", "%".Request::Input('textSearch')."%");

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "QTY"){
							$query->where("QTY",  Request::Input('textSearch') );

						}else if( Request::Has('srtCondition') && Request::Input('srtCondition') == "SUJO_NO"){
							$query->where("SUJO_NO",  "like", "%".Request::Input('textSearch')."%");

						}else{
							$query->where(function($q){
								$q->where("SALE_UNCS", is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 )
								->orwhere("PIS_NM",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("SIZES",  "like", "%".Request::Input('textSearch')."%")
								->orwhere("QTY", is_numeric(Request::Input('textSearch')) ? Request::Input('textSearch') : -99999999 )
								->orwhere("SUJO_NO",  "like", "%".Request::Input('textSearch')."%");
							});
						}
					}
				}
		)->make(true);
		
	}
	
	// 선택된 수조데이터 조회
	public function getSujoData()
	{
		
		$STOCK_SUJO = STOCK_SUJO::select( 
				  'PIS_INFO.PIS_NM'
				, 'STOCK_SUJO.ORIGIN_NM'
				, 'STOCK_SUJO.ORIGIN_CD'
				, 'STOCK_SUJO.CORP_MK'
				, 'STOCK_SUJO.SUJO_NO'
				, 'STOCK_SUJO.PIS_MK'
				, 'STOCK_SUJO.SIZES'
				, DB::raw( "CONVERT(CHAR(10), STOCK_SUJO.INPUT_DATE, 23) AS INPUT_DATE")
				, DB::raw( "ROUND(STOCK_SUJO.QTY, 1) AS QTY")
				, 'STOCK_SUJO.SALE_UNCS'
				, 'STOCK_SUJO.ORIGIN_CD'
				)->leftjoin("PIS_INFO", function($join){
					$join->on("STOCK_SUJO.PIS_MK", "=", "PIS_INFO.PIS_MK");
					$join->on("STOCK_SUJO.CORP_MK", "=", "PIS_INFO.CORP_MK");
				})->where("STOCK_SUJO.CORP_MK", $this->getCorpId())
				->where("STOCK_SUJO.SUJO_NO", Request::Input("SUJO_NO"))->get();
			

		return Datatables::of($STOCK_SUJO)->make(true);
	}

	// 어종조회
	public function getPisCD()
	{	
		
		$PIS_INFO = PIS_INFO::select( 
									  'PIS_INFO.PIS_MK'
									, 'PIS_INFO.PIS_NM'
									, 'PIS_CLASS_CD.SIZES'
									, 'PIS_INFO.CORP_MK'
									)->join("PIS_CLASS_CD", function($join){
										$join->on("PIS_INFO.PIS_MK", "=", "PIS_CLASS_CD.PIS_MK");
										$join->on("PIS_INFO.CORP_MK", "=", "PIS_CLASS_CD.CORP_MK");
									})->where("PIS_CLASS_CD.SIZES", "!=", "null")
									->where("PIS_INFO.CORP_MK", $this->getCorpId())
									->where("SALE_TARGET",  True );

		return Datatables::of($PIS_INFO)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("PIS_INFO.PIS_NM",  "like", "%".Request::Input('textSearch')."%");
					}
		})->make(true);
	}

	// 거래처 조회
	public function getPisCust()
	{
		$CUST_CD = CUST_CD::select( 
					  "CUST_MK"
					, "CORP_MK"
					, "CUST_GRP_CD"
					, "CORP_DIV"
					, "FRNM"
					, "RPST"
					, "RANK"
					, "ETPR_NO"
					, "JUMIN_NO"
					, "PHONE_NO"
					, "FAX"
					, "ADDR1"
					, "ADDR2"
					, "POST_NO"
					, "UPJONG"
					, "UPTE"
					, "AREA_CD"
					, "REMARK" 
				)->where("CORP_MK", $this->getCorpId());
				

		return Datatables::of($CUST_CD)
				->filter(function($query) {
					if( Request::Has('textSearch') ){
						$query->where("FRNM",  "like", "%".Request::Input('textSearch')."%");
					}
					if ( Request::Has('corp_div')){
						$query->where("CORP_DIV",  Request::Input('corp_div'));
					}

					if ( Request::Has("cust_grp_cd") && Request::Input("cust_grp_cd") != "ALL"){
						$query->where("CUST_GRP_CD", Request::Input("cust_grp_cd"));
					}

					$user = User::where("MEM_ID", Auth::user()->MEM_ID)->first();

					if( !$user->hasRole(['CashBook'])){
						if ( Request::Has('notJ')) {
							$query->whereNotIn("CORP_DIV",  [Request::Input('notJ')]);		
						}
					}
					$query->where("CORP_MK", $this->getCorpId());
		})->make(true);
	}

	// 선택된 수조데이터 조회
	public function getOrginCD()
	{
		$ORIGIN_CD = DB::table("ORIGIN_CD")
				->select( 
				  'ORIGIN_CD'
				, 'ORIGIN_NM'
				)->orderBy('ORIGIN_SEQ', 'ASC')->get();
		
		
		return $ORIGIN_CD;
		
	}

	// 판매단가 수정
	public function setSujoDataUNCN()
	{
		$validator = Validator::make( Request::Input(), [
			'SUJO_NO'   => 'required|max:20,NOT_NULL',
			'SALE_UNCS' => 'required|numeric'
		]);

		if ($validator->fails()) {
			return response()->json(['result' => 'fail', 'reason' => 'validation_make']);
		}

		DB::beginTransaction();

		try {
			
			STOCK_SUJO::where("CORP_MK", $this->getCorpId())
				->where("SUJO_NO", Request::input('SUJO_NO'))
				->update(["SALE_UNCS" => Request::input('SALE_UNCS')]);

		} catch(ValidationException $e){
			DB::rollback();
			return response()->json(['result' => 'fail', 'reason' => 'validation_catch']);

		}catch(Exception $e){
			DB::rollback();
			throw $e;
		}

		DB::commit();
		return response()->json(['result' => 'success']);
	}

	public function _delete(){

		$validator = Validator::make( Request::Input(), [
			'SUJO_NO'   => 'required|max:6,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
			
			try {
				// 동일한 키가 있는 경우 실패메시지
				$count = STOCK_SUJO::where('CORP_MK', $this->getCorpId())->where('SUJO_NO',  Request::Input("SUJO_NO"))->first()->QTY;
				
				//dd($count);
				// 수량이 0이거나 0보다 작을 경우 삭제 가능하도록 체크
				if (  (int)$count > 0  && $count !== null ){
					return response()->json(['result' =>'DUPLICATION', 'message' => '해당 수조에는 입고정보가 있습니다.'], 422);
				}else{
					
					DB::table("STOCK_SUJO")
						->where('CORP_MK' , $this->getCorpId())
						->where('SUJO_NO' , Request::input('SUJO_NO'))
						->update([
									'PIS_MK'	=> DB::raw('null'),
									'SIZES'		=> DB::raw('null'),
									'CUST_MK'	=> DB::raw('null'),
								]
						);

					DB::table("STOCK_SUJO")
						->where('CORP_MK' , $this->getCorpId())
						->where('SUJO_NO' , Request::input('SUJO_NO'))
						->delete();
					
					return response()->json(['result' => 'success']);
				}

			} catch(ValidationException $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}
			return response()->json(['result' =>'DB_ERROR'], 422);
		});
		
	}

	// 임시 수조 생성
	public function setSujoInsert_Info(){
	
		
		return $exception = DB::transaction(function(){
		
			try {
				DB::table("STOCK_SUJO")
					->insert(
						[
							  "CORP_MK"		=> $this->getCorpId()
							, "SUJO_NO"		=> Request::Input("SUJO_NO")
							, "PIS_MK"		=> Request::Input("PIS_MK")
							, "SIZES"		=> Request::Input("SIZES")
							, "INPUT_DATE"	=> Request::Input("INPUT_DATE")
							, "QTY"			=> 0
							, "SALE_UNCS"	=> Request::Input("SALE_UNCS")
							, "ORIGIN_CD"	=> Request::Input("ORIGIN_CD")
							, "ORIGIN_NM"	=> Request::Input("ORIGIN_NM")
						]
				);
				return response()->json(['result' => 'success']);
			} catch(ValidationException $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}
		});
	}

	// 수조추가 
	public function setSujoInsert(){
		
		$validator = Validator::make( Request::Input(), [
			'SUJO_NO'   => 'required|alpha_dash|max:6,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		return $exception = DB::transaction(function(){
		
			try {

				// 동일한 키가 있는 경우 실패메시지
				$count = STOCK_SUJO::where('CORP_MK', $this->getCorpId())->where('SUJO_NO',  Request::Input("SUJO_NO"))->count();
				if ( $count > 0){
					return response()->json(['result' =>'DUPLICATION', 'message' => '동일한 수조가 존재합니다'], 422);
				}

				DB::table("STOCK_SUJO")
					->insert(
								[
									'CORP_MK' => $this->getCorpId(),
									'SUJO_NO' => Request::input('SUJO_NO'),
								]				
							);

				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){

				if (Request::ajax() || Request::wantsJson()) {
					$message = $e->getMessage();
					
					if (is_object($message)) { 
						$message = $message->toArray(); 
					}
					
					return response()->json(['result' =>'DB_ERROR', 'message' => $message], 422);
				}else{
					return $e;
				}

			}catch(Exception $e){
				DB::rollback();
				throw $e;
			}
		});
	}

	// 입고 추가
	public function setSujoDataIn(){
		
		// 넘어온값 Validation 체크 해야..
		$validator = Validator::make( Request::Input(), [
			'SUJO_NO'		=> 'required|max:6,NOT_NULL',
			'PIS_MK'		=> 'required|max:4,NOT_NULL',
			'SIZES'			=> 'required|max:20,NOT_NULL',
			'ORIGIN_CD'		=> 'required|max:3,NOT_NULL',
			'QTY'			=> 'required|numeric',
			'UNCS'			=> 'required|numeric',
			'CUST_MK'		=> 'required|max:20',
			'SALE_UNCN'		=> 'numeric',
			'INPUT_AMT'		=> 'required|numeric',
			'INPUT_DATE'	=> 'required|date',
			'INPUT_DISCOUNT'=> 'numeric',
			'INPUT_UNPROV'	=> 'numeric',
			'CURR_QTY'		=> 'numeric',
			'OUTLINE'		=> 'max:50',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$OLD_STOCK = STOCK_SUJO::where('CORP_MK', $this->getCorpId())
								->where('SUJO_NO',  Request::Input("SUJO_NO"))
								->select(
									  'CORP_MK'
									, 'SUJO_NO'
									, 'ORIGIN_NM'
									, 'ORIGIN_CD'
									, 'PIS_MK'
									, 'SIZES'
									, DB::raw( "CONVERT(CHAR(10), INPUT_DATE, 23) AS INPUT_DATE")
									, 'QTY'
									, 'SALE_UNCS'

								)->first();
		
		$arr_request= [
			'CORP_MK'		=> $this->getCorpId(),
			'PIS_MK'		=> Request::Input("PIS_MK"),
			'SIZES'			=> Request::Input("SIZES"),
			'SEQ'			=> $this->getMaxSeqInSTOCKINFO(Request::Input("SIZES") , Request::Input("PIS_MK") ) + 1,
			'CUST_MK'		=> Request::Input("CUST_MK"),
			'INPUT_DATE'	=> Request::Input("INPUT_DATE"),
			'OUTPUT_DATE'	=> null,
			'OUTLINE'		=> Request::Input("OUTLINE"),
			'DE_CR_DIV'		=> 1,
			'UNCS'			=> Request::Input("UNCS"),
			'INPUT_AMT'		=> Request::Input("INPUT_AMT"),
			'INPUT_DISCOUNT'=> Request::Input("INPUT_DISCOUNT"),
			'INPUT_UNPROV'	=> Request::Input("INPUT_AMT") - Request::Input("DISCOUNT") - Request::Input("CASH") ,
			'ORIGIN_CD'		=> Request::Input("ORIGIN_CD"),
			'ORIGIN_NM'		=> Request::Input("ORIGIN_NM"),
			'QTY'			=> Request::Input("QTY"),
			'CASH'			=> Request::Input("CASH"),
			'SUJO_NO'		=> Request::Input("SUJO_NO"),
			'SALE_UNCS'		=> Request::Input("SALE_UNCS"),

			'OPTION_FORCE'	=> Request::Input("OPTION_FORCE"),
		];

		// 수조에 어종이 없으면
		if( (int)$OLD_STOCK->QTY == 0 || $OLD_STOCK->QTY === null){
			
			try{
				// 재고 및 미지급금 처리
				
				$this->InStock_Unprov($arr_request);
				
				// 수조 수정
				$this->updateSujoInfo($arr_request);
				// 완료
				//dd($OLD_STOCK);
				return response()->json(['result' => 'success']);
				
			}catch(Exception $e){
				return $e;
			}
		
		// 수조에 기존에 고기가 있는 경우
		}else{
			
			if(	
					$OLD_STOCK->PIS_MK		!= $arr_request['PIS_MK']	||
					$OLD_STOCK->SIZES		!= $arr_request['SIZES']	||
					$OLD_STOCK->ORIGIN_NM	!= $arr_request['ORIGIN_NM']
				
			){
				// 어종 규격은 같으나 입고일이 다른 경우
				return response()->json(['result' => 'fail', 'type'=> 'error01', 'message' => '어종, 규격, 원산지가 다른 경우는 입고 할 수 없습니다']);
			}
			
			
			if( ( substr($OLD_STOCK->INPUT_DATE, 0, 10) != $arr_request['INPUT_DATE'] ) && ( $arr_request['OPTION_FORCE'] != "true" )){
				// 어종 규격은 같으나 입고일이 다른 경우
				return response()->json(
					[
						'result'	 => 'fail', 
						'type'		 => 'error02', 
						'recent_date'=> substr($OLD_STOCK->INPUT_DATE, 0, 10),
						'new_date'	 => $arr_request['INPUT_DATE'], 
						'message'	 => '어종 규격은 같으나 입고일이 다릅니다'
					]
				);
			
			}else{
				
				// 수조를 업데이트 (합치는 경우)	
				if( $arr_request['INPUT_DATE'] == substr($OLD_STOCK->INPUT_DATE, 0, 10)){
					
					try{
						// 수조의 날짜를 선택하는 경우
						// 입력날짜를 수조 날짜에 맞춰 수조를 업데이트
						$OLD_STOCK->INPUT_DATE =  $arr_request['INPUT_DATE'];
						$this->InStock_Unprov($arr_request);
						$this->updateSujoInfo($arr_request);
						
					} catch(Exception $e){

						return $e;
					}
				}else{
					try{
						$this->InStock_Unprov($arr_request);
						$this->updateSujoInfo($arr_request);
					}catch(Exception $e){
					
						return $e;
					}
				}
			}
		}
		return response()->json(['result' => 'success']);
	}


	// 입고
	private function InStock_Unprov($STOCK_INFO){
	
		DB::beginTransaction();
		try {
				STOCK_INFO::insert(
						[
							'CORP_MK'		=> $this->getCorpId(),
							'PIS_MK'		=> $STOCK_INFO['PIS_MK'],
							'SIZES'			=> $STOCK_INFO['SIZES'],
							'SEQ'			=> (int)($this->getMaxSeqInSTOCKINFO($STOCK_INFO['SIZES'],$STOCK_INFO['PIS_MK'] ) ) + 1,
							'CUST_MK'		=> $STOCK_INFO['CUST_MK'],
							'INPUT_DATE'	=> $STOCK_INFO['INPUT_DATE'],
							'OUTPUT_DATE'	=> null,
							'OUTLINE'		=> $STOCK_INFO['OUTLINE'],
							'DE_CR_DIV'		=> 1,
							'UNCS'			=> $STOCK_INFO['UNCS'],
							'INPUT_AMT'		=> $STOCK_INFO['INPUT_AMT'] ,
							'INPUT_DISCOUNT'=> $STOCK_INFO['INPUT_DISCOUNT'],
							'INPUT_UNPROV'	=> ($STOCK_INFO['INPUT_AMT'] - $STOCK_INFO['INPUT_DISCOUNT'] - $STOCK_INFO['CASH']),
							'ORIGIN_CD'		=> $STOCK_INFO['ORIGIN_CD'],
							'ORIGIN_NM'		=> $STOCK_INFO['ORIGIN_NM'],
							'QTY'			=> $STOCK_INFO['QTY'],
							'SUJO_NO'		=> $STOCK_INFO['SUJO_NO'],
						]
				);
				
				// 미수금 Insert
				if( (int)$STOCK_INFO['INPUT_UNPROV'] > 0){

					UNPROV_INFO::insert(
							[
								'CORP_MK'		=> $this->getCorpId(),
								'WRITE_DATE'	=> $STOCK_INFO['INPUT_DATE'],
								'SEQ'			=> ($this->getMaxSeqInUNPROVINFO($STOCK_INFO['INPUT_DATE']) ) + 1,
								'PROV_CUST_MK'	=> $STOCK_INFO['CUST_MK'],
								'PROV_DIV'		=> 1,
								'AMT'			=> $STOCK_INFO['INPUT_UNPROV'],
								'REMARK'		=> '입고 미수',
								'STOCK_PIS_MK'	=> $STOCK_INFO['PIS_MK'],
								'STOCK_SIZES'	=> $STOCK_INFO['SIZES'],
								'STOCK_SEQ'		=> $STOCK_INFO['SEQ'],
							]
					);
				}

		}
		catch(ValidationException $e){
			DB::rollback();
		}

		DB::commit();

	}

	//  수정 (빈수조 일때)
	private function updateSujoInfo($SUJO_INFO){

		$OLD_SUJO = DB::table("STOCK_SUJO")
					->where("CORP_MK", $SUJO_INFO['CORP_MK'])
					->where("SUJO_NO", $SUJO_INFO['SUJO_NO'])->first();
		
		if( $OLD_SUJO->QTY == null){
			$OLD_SUJO->QTY = 0;
		}

		STOCK_SUJO::where("CORP_MK", $SUJO_INFO['CORP_MK'])
			->where("SUJO_NO", $SUJO_INFO['SUJO_NO'])
			->update(
				[
					
					"PIS_MK"		=> $SUJO_INFO['PIS_MK'],
					"SIZES"			=> $SUJO_INFO['SIZES'],
					"INPUT_DATE"	=> $SUJO_INFO['INPUT_DATE'],
					"SALE_UNCS"		=> is_numeric($SUJO_INFO['SALE_UNCS']) ? $SUJO_INFO['SALE_UNCS'] : null ,
					"ORIGIN_CD"		=> $SUJO_INFO['ORIGIN_CD'],
					"ORIGIN_NM"		=> $SUJO_INFO['ORIGIN_NM'],
					"QTY"			=> $OLD_SUJO->QTY + $SUJO_INFO['QTY'],
					"CUST_MK"		=> $SUJO_INFO['CUST_MK'],
				]
		);
	}

	// 이고시 매입처가 다를경우 입고할수있도록 체크하는 함수
	private function chkCusk($SORCE, $DEST){
		
		$S_STOCK_INFO = DB::table("STOCK_INFO AS S")
						->join("STOCK_SUJO AS J", function($join){
							$join->on("S.CORP_MK", "=", "J.CORP_MK");
							$join->on("S.PIS_MK", "=", "J.PIS_MK");
							$join->on("S.SIZES", "=", "J.SIZES");
							$join->on("S.ORIGIN_CD", "=", "J.ORIGIN_CD");
								
						})->where("S.CORP_MK", $this->getCorpId())
						//->where("SUJO_NO", $SORCE->SUJO_NO)
						->where("S.ORIGIN_CD", $SORCE->ORIGIN_CD)
						->where("S.PIS_MK", $SORCE->PIS_MK)
						->where("S.SIZES", $SORCE->SIZES)
						->whereNotNull("S.CUST_MK")
						->whereNull("S.OUTPUT_DATE")
						->where(function($query){
							$query->where("S.OUTLINE", "")
								->orwhereNull("S.OUTLINE");
						})
						->where("S.CUST_MK", "<>", "")
						->get();

		
		$D_STOCK_INFO = DB::table("STOCK_INFO AS S")
						->join("STOCK_SUJO AS J", function($join){
							$join->on("S.CORP_MK", "=", "J.CORP_MK");
							$join->on("S.PIS_MK", "=", "J.PIS_MK");
							$join->on("S.SIZES", "=", "J.SIZES");
							$join->on("S.ORIGIN_CD", "=", "J.ORIGIN_CD");
								
						})->where("S.CORP_MK", $this->getCorpId())
						//->where("SUJO_NO", $SORCE->SUJO_NO)
						->where("S.ORIGIN_CD", $DEST->ORIGIN_CD)
						->where("S.PIS_MK", $DEST->PIS_MK)
						->where("S.SIZES", $DEST->SIZES)
						->whereNotNull("S.CUST_MK")
						->whereNull("S.OUTPUT_DATE")
						->where(function($query){
							$query->where("S.OUTLINE", "")
								->orwhereNull("S.OUTLINE");
						})
						->where("S.CUST_MK", "<>", "")
						->get();
		
		//dd($S_STOCK_INFO);
		
		// 이고시 이고대상 수조가 null 일 경우가 있으므로 체크
		// 2016-10-30 : 청수활어/해마수산
		if( $S_STOCK_INFO === null){
			return true;
		}
		
		if( count($D_STOCK_INFO) == 0){
			return true;
		}
		
		$isOk = true;
		foreach( $S_STOCK_INFO as $s){
			foreach( $D_STOCK_INFO as $d){
				if( $s->CUST_MK ==  $d->CUST_MK){
					$isOk = false;
					return true;
				}
			}
			if( $isOk == false){
				return false;
			}
		}
		
		return false;
	
	}

	// 이고 저장
	public function setMoveSujo(){
		

		// 넘어온값 Validation 체크 해야..
		$validator = Validator::make( Request::Input(), [
			'DST_SUJO_NO'		=> 'required|max:6,NOT_NULL',
			'SRC_SUJO_NO'		=> 'required|max:6,NOT_NULL',
			'QTY'				=> 'numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$SRC_SUJO_NO	= Request::Input("SRC_SUJO_NO");
		$DST_SUJO_NO	= Request::Input("DST_SUJO_NO");
		$INPUTE_DATE	= Request::Input("INPUT_DATE");
		$QTY			= Request::Input("QTY");

		$SRC_STOCK_SUJO = STOCK_SUJO::where('SUJO_NO', $SRC_SUJO_NO)
							->where('CORP_MK', $this->getCorpId())
							->select(
									 'CORP_MK'
									,'PIS_MK'
									,'SIZES'	
									,'ORIGIN_CD'	
									,'ORIGIN_NM'	
									, DB::raw("CONVERT(CHAR(10), INPUT_DATE, 23) AS INPUT_DATE")
									,'SALE_UNCS'
									,'QTY'		
									,'SUJO_NO'
									,'CUST_MK'
								)->first();
		$DST_STOCK_SUJO = STOCK_SUJO::where('SUJO_NO', $DST_SUJO_NO)
							->where('CORP_MK', $this->getCorpId())
							->select(
									 'CORP_MK'
									,'PIS_MK'
									,'SIZES'	
									,'ORIGIN_CD'	
									,'ORIGIN_NM'	
									, DB::raw("CONVERT(CHAR(10), INPUT_DATE, 23) AS INPUT_DATE")
									,'SALE_UNCS'
									,'QTY'		
									,'SUJO_NO'	
									,'CUST_MK'
								)
							->first();
		
		// 매입거래처 체크
		if( !$this->chkCusk($SRC_STOCK_SUJO, $DST_STOCK_SUJO) ){
			return response()->json(['result' => 'fail', 'type'=>'EQUAL_CUST', 'message' => '매입거래처가 다를 경우 이고할 수 없습니다'], 422);
		}


		if( $DST_STOCK_SUJO->INPUT_DATE == null){
			$DST_STOCK_SUJO->INPUT_DATE = $SRC_STOCK_SUJO->INPUT_DATE;
		}
		
		if( $SRC_STOCK_SUJO->SIZES != $DST_STOCK_SUJO->SIZES || $SRC_STOCK_SUJO->PIS_MK != $DST_STOCK_SUJO->PIS_MK ){
			
			if(		$DST_STOCK_SUJO->SIZES != null 
				&&	$DST_STOCK_SUJO->PIS_MK != null 
				&&	$DST_STOCK_SUJO->QTY != 0
				&&	$DST_STOCK_SUJO->INPUT_DATE != null
			){
				return response()->json(['result' => 'fail', 'type'=> 'EQUAL', 'message' => '어종 또는 규격이 다른 경우는 이고 할 수 없습니다'], 422);
			}
		}
		
		// 빈수조가 아닐때
		if( $DST_STOCK_SUJO->QTY > 0){

			if( ( $SRC_STOCK_SUJO->INPUT_DATE  != $DST_STOCK_SUJO->INPUT_DATE ) && ( Request::Input('OPTION_FORCE') != "true" ) ){
				// 날짜가 다를경우
				return response()->json(
					[
						'result'	 => 'fail', 
						'type'		 => 'error03', 
						'src_date'	 => substr( $SRC_STOCK_SUJO->INPUT_DATE, 0, 10),
						'dst_date'	 => substr( $DST_STOCK_SUJO->INPUT_DATE, 0, 10), 
						'message'	 => '이고할 수조에 입고날짜가 다른 재고가 존재합니다 통합할 날짜를 선택하여 주십시오'
					], 422
				);

			}else{
				// 이고시 날짜를 선택한 경우
				// 날짜가 다른쪽을 출고 / 입고 처리 시키고
				if( $INPUTE_DATE == substr( $SRC_STOCK_SUJO->INPUT_DATE, 0, 10) ){
					
					DB::beginTransaction();
					
					try {
						$this->OutInfo($DST_STOCK_SUJO, $SRC_SUJO_NO);
						$this->InInfo($DST_STOCK_SUJO, $DST_SUJO_NO);
							
						$DST_STOCK_SUJO->SALE_UNCS = $SRC_STOCK_SUJO->SALE_UNCS ;
						
					} catch(ValidationException $e){
						DB::rollback();

					} catch(Exception $e){
						DB::rollback();
						throw $e;
					}
					DB::commit();
					

				} else {
					
					DB::beginTransaction();
					
					try {
						$this->OutInfo($SRC_STOCK_SUJO, $SRC_SUJO_NO);
						$this->InInfo($SRC_STOCK_SUJO, $DST_SUJO_NO);
						
					} catch(ValidationException $e){
						DB::rollback();

					} catch(Exception $e){
						DB::rollback();
						throw $e;
					}

					DB::commit();
				}
			}
		
		}else{
			$DST_STOCK_SUJO->SALE_UNCS = $SRC_STOCK_SUJO->SALE_UNCS ;
		}

		$DST_STOCK_SUJO->SALE_UNCS	= $SRC_STOCK_SUJO->SALE_UNCS ;
		$DST_STOCK_SUJO->PIS_MK		= $SRC_STOCK_SUJO->PIS_MK;
		$DST_STOCK_SUJO->SIZES		= $SRC_STOCK_SUJO->SIZES;
		$DST_STOCK_SUJO->ORIGIN_CD	= $SRC_STOCK_SUJO->ORIGIN_CD;
		$DST_STOCK_SUJO->ORIGIN_NM	= $SRC_STOCK_SUJO->ORIGIN_NM;
		$DST_STOCK_SUJO->INPUT_DATE	= $INPUTE_DATE;

		// 2017-06-13 김현섭
		// 이고할 대상수조에 거래처코드가 비었을 경우만 이고 수조의 거래처 코드를 삽입한다
		if( $DST_STOCK_SUJO->CUST_MK == "" || $DST_STOCK_SUJO->CUST_MK == null){
			$DST_STOCK_SUJO->CUST_MK = $SRC_STOCK_SUJO->CUST_MK;
		}
		
		$DST_STOCK_SUJO->QTY		= $DST_STOCK_SUJO->QTY + $QTY;
		$DST_STOCK_SUJO->QTY		= number_format($DST_STOCK_SUJO->QTY, 2) ;
		
		STOCK_SUJO::where('SUJO_NO', $DST_SUJO_NO)
					->where('CORP_MK', $this->getCorpId())
					->update(
						[
							'PIS_MK'		=> $DST_STOCK_SUJO->PIS_MK,
							'SIZES'			=> $DST_STOCK_SUJO->SIZES,
							'ORIGIN_CD'		=> $DST_STOCK_SUJO->ORIGIN_CD,
							'ORIGIN_NM'		=> $DST_STOCK_SUJO->ORIGIN_NM,
							'SALE_UNCS'		=> $DST_STOCK_SUJO->SALE_UNCS,
							'INPUT_DATE'	=> $DST_STOCK_SUJO->INPUT_DATE,
							'QTY'			=> $DST_STOCK_SUJO->QTY,
							'CUST_MK'		=> $DST_STOCK_SUJO->CUST_MK,
		
						]
					);
		$SRC_STOCK_SUJO->QTY =  round(( $SRC_STOCK_SUJO->QTY - floatval($QTY)) * 100) / 100;
		$SRC_STOCK_SUJO->QTY = number_format($SRC_STOCK_SUJO->QTY ,2);

		if( $SRC_STOCK_SUJO->QTY == 0.00 || $SRC_STOCK_SUJO->QTY == 0){
			// 빈수조가 되는것을 방지한다 우선..
			STOCK_SUJO::where('SUJO_NO', $SRC_SUJO_NO)
						->where('CORP_MK', $this->getCorpId())
						->update(
							[
								'PIS_MK'		=> DB::raw("null") ,
								'SIZES'			=> DB::raw("null") ,
								'ORIGIN_CD'		=> DB::raw("null") ,
								'ORIGIN_NM'		=> DB::raw("null") ,
								'INPUT_DATE'	=> DB::raw("null") ,
								'QTY'			=> 0 ,
								'SALE_UNCS'		=> DB::raw("null") ,
								'CUST_MK'		=> DB::raw("null") ,
							]
			);
		
		}else{

			STOCK_SUJO::where('SUJO_NO', $SRC_SUJO_NO)
						->where('CORP_MK', $this->getCorpId())
						->update(
							[
								'QTY'			=> $SRC_STOCK_SUJO->QTY ,
							]
			);
		}

		return response()->json(['result'	 => 'success']);
	}

	private function OutInfo($pSTOCK_SUJO, $sSUJO_NO){
		
		
		STOCK_INFO::insert([
			'CORP_MK'		=> $this->getCorpId(),
			'PIS_MK'		=> $pSTOCK_SUJO->PIS_MK,
			'SIZES'			=> $pSTOCK_SUJO->SIZES,
			'SEQ'			=> $this->getMaxSeqInSTOCKINFO($pSTOCK_SUJO->SIZES, $pSTOCK_SUJO->PIS_MK) + 1,

			'ORIGIN_CD'		=> $pSTOCK_SUJO->ORIGIN_CD,
			'ORIGIN_NM'		=> $pSTOCK_SUJO->ORIGIN_NM,

			'CUST_MK'		=> DB::raw("null"),
			'INPUT_DATE'	=> $pSTOCK_SUJO->INPUT_DATE,
			'OUTPUT_DATE'	=> DB::raw("CONVERT(char(10), GetDate(),126)"),

			'DE_CR_DIV'		=> 0,
			'OUTLINE'		=> "이고시 출고된 내역",
			'QTY'			=> $pSTOCK_SUJO->QTY,
			'CURR_QTY'		=> 0,
			'SUJO_NO'		=> $sSUJO_NO,


		]);
		
	}

	private function InInfo($pSTOCK_SUJO, $dSUJO_NO){
		
		STOCK_INFO::insert([
			'CORP_MK'		=> $this->getCorpId(),
			'PIS_MK'		=> $pSTOCK_SUJO->PIS_MK,
			'SIZES'			=> $pSTOCK_SUJO->SIZES,
			'SEQ'			=> $this->getMaxSeqInSTOCKINFO($pSTOCK_SUJO->SIZES, $pSTOCK_SUJO->PIS_MK) + 1,

			'ORIGIN_CD'		=> $pSTOCK_SUJO->ORIGIN_CD,
			'ORIGIN_NM'		=> $pSTOCK_SUJO->ORIGIN_NM,

			'CUST_MK'		=> DB::raw("null"),
			'INPUT_DATE'	=> DB::raw("CONVERT(char(10), GetDate(),126)"),
			'OUTPUT_DATE'	=> DB::raw("null"),

			'DE_CR_DIV'		=> 1,
			'OUTLINE'		=> "이고시 입고된 내역",
			'QTY'			=> $pSTOCK_SUJO->QTY,
			'CURR_QTY'		=> 1,
			'SUJO_NO'		=> $dSUJO_NO,
		]);
	}

	public function chkHasSujo(){
	
		$STOCK_SUJO = DB::table("STOCK_SUJO")
					->where("CORP_MK", $this->getCorpId())
					->where("SUJO_NO", Request::Input("SUJO_NO"))
					->get();

		if( count($STOCK_SUJO) == 0){
			return response()->json(['result'	 => 0]);
		}else{
		}	return response()->json(['result'	 => count($STOCK_SUJO)]);
	}

	// 빈수조 테이블 가져오기
	public function getSujoEmpty(){

		$STOCK_SUJO = STOCK_SUJO::select(
				  'STOCK_SUJO.SUJO_NO'
			)->join( 
				DB::raw( " ( SELECT * FROM STOCK_SUJO WHERE [CORP_MK]='".$this->getCorpId()."' AND SUJO_NO='".Request::Input("SUJO_NO")."' ) AS B  "), function($join){
					$join ->on("STOCK_SUJO.PIS_MK", "=", "B.PIS_MK ")
						  ->on("STOCK_SUJO.SIZES", "=", "B.SIZES ")
						  ->orwhereNull("STOCK_SUJO.SIZES");
			})->where("STOCK_SUJO.CORP_MK", $this->getCorpId())
			->where("STOCK_SUJO.SUJO_NO", '<>', Request::Input("SUJO_NO"));
			
			

		return Datatables::of($STOCK_SUJO)->make(true);
	}

	public function getSujoSearch(){

		$STOCK_SUJO = STOCK_SUJO::select(
				  'STOCK_SUJO.SUJO_NO'
				, 'STOCK_SUJO.PIS_MK'
				, 'STOCK_SUJO.SIZES'
				, 'STOCK_SUJO.ORIGIN_CD'
				, 'STOCK_SUJO.ORIGIN_NM'
				, 'PIS.PIS_NM'
				, DB::raw( "CONVERT(CHAR(10), STOCK_SUJO.INPUT_DATE, 23) AS INPUT_DATE" )
			)->leftjoin("PIS_INFO AS PIS", function($join){
						$join->on("PIS.PIS_MK", "=", "STOCK_SUJO.PIS_MK");
						$join->on("PIS.CORP_MK", "=", "STOCK_SUJO.CORP_MK");
			})->where("STOCK_SUJO.CORP_MK", $this->getCorpId())
			->where("STOCK_SUJO.SUJO_NO", '<>', Request::Input("SUJO_NO"))
			->where(function($query){
				
				if( Request::Has("PIS_MK") ){
					$query->where("STOCK_SUJO.PIS_MK", Request::Input("PIS_MK") );
				}

				if( Request::Has("SIZES") ){
					$query->where("STOCK_SUJO.SIZES", Request::Input("SIZES") );
				}

				if( Request::Has("ORIGIN_CD") ){
					$query->where("STOCK_SUJO.ORIGIN_CD", Request::Input("ORIGIN_CD") );
				}
			});
			
		return Datatables::of($STOCK_SUJO)->make(true);
	}

	// 새로운 임시수조번호 랜덤 생성
	public function getSujoNewTemp(){

		$randSujoNo = "RE".substr(uniqid(''), -2);
		$cnt		= 0;
		
		do{
			$cnt = $this->chkSujoNoNew($randSujoNo);
			if( $cnt == 0){
				break;
			}
		} while( $cnt != 0);
		
		return response()->json(['NEW_SUJO_NO'=> $randSujoNo ]);
	}

	private function chkSujoNoNew($randSujoNo){

		$cntSUJO_INFO = DB::table("STOCK_SUJO")
						->where("CORP_MK", $this->getCorpId())
						->where("SUJO_NO", $randSujoNo)
						->count();

		return $cntSUJO_INFO;

	}

	// 로그 조회
	public function log($buy){
		return view("sujo.log",[ "buy" => $buy] );
	}

	// 재고조회
	public function listLog()
	{
		$STOCK_LOG = DB::table("STOCK_SUJO_LOG AS LG")
								->select(  "LG.CORP_MK"
										  ,"LG.IDX"
										  ,DB::raw("CASE WHEN LG.TYPE = 'C' THEN '추가'
													     WHEN LG.TYPE = 'D' THEN '삭제'
													     WHEN LG.TYPE = 'U' THEN '수정'
													ELSE '에러' END AS TYPE_NM")
										  ,"LG.SUJO_NO"
										  ,"LG.PIS_MK"
										  ,"LG.SIZES"
										  , DB::raw( "CONVERT(CHAR(10), LG.INPUT_DATE, 23) AS INPUT_DATE")
										  , DB::raw( "ROUND(LG.QTY, 2) AS QTY" )
										  ,"LG.SALE_UNCS"
										  ,"LG.ORIGIN_CD"
										  ,"LG.ORIGIN_NM"
										  , DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.WRITE_DT,101),121) AS WRITE_DT")
										  , DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.UPDATE_DT,101),121) AS UPDATE_DT")
										  , DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,LG.DELETE_DT,101),121) AS DELETE_DT")
										  ,"LG.REMARK"
										  ,"PIS.PIS_NM"
								)->leftjoin("PIS_INFO AS PIS", function($join){
									$join->on("PIS.PIS_MK", "=", "LG.PIS_MK");
									$join->on("PIS.CORP_MK", "=", "LG.CORP_MK");
								})->where("LG.CORP_MK", $this->getCorpId());
						
		return Datatables::of($STOCK_LOG)->make(true);
    }

	// 재고로그
	public function listLog1(){

		$STOCK_LOG = DB::table("STOCK_INFO_LOG AS S")
					->select(   DB::raw("S.*")
							  , "PIS.PIS_NM"
							  , DB::raw("CONVERT(CHAR(10), S.INPUT_DATE, 23) AS INPUT_DATE1")
							  , DB::raw("CONVERT(CHAR(19),CONVERT(DATETIME,S.WRITE_DATE,101),121) AS WRITE_DATE2")
							  , DB::raw("S.QTY * UNCS AS AMT")
							  , DB::raw("S.QTY * UNCS - INPUT_DISCOUNT AS REAL_AMT")
							  , DB::raw("S.QTY * UNCS - INPUT_DISCOUNT - UNPROV AS CASH")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS AS BEFORE_AMT")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS - BEFORE_INPUT_DISCOUNT AS BEFORE_REAL_AMT")
							  , DB::raw("S.BEFORE_QTY * BEFORE_UNCS - BEFORE_INPUT_DISCOUNT - BEFORE_UNPROV AS BEFORE_CASH")
							  , "C.FRNM"
							  , DB::raw("BC.FRNM AS BEFORE_FRNM")
							)
					->leftjoin("CUST_CD AS C", function($join){
						$join->on("S.CORP_MK", "=", "C.CORP_MK");
						$join->on("S.CUST_MK", "=", "C.CUST_MK");
							
					})->leftjoin("CUST_CD AS BC", function($join){
						$join->on("S.CORP_MK", "=", "BC.CORP_MK");
						$join->on("S.BEFORE_CUST_MK", "=", "BC.CUST_MK");
							
					})->leftjoin("PIS_INFO AS PIS", function($join){
						$join->on("PIS.PIS_MK", "=", "S.PIS_MK");
						$join->on("PIS.CORP_MK", "=", "S.CORP_MK");
							
					})->where("S.CORP_MK", $this->getCorpId());
	
		return Datatables::of($STOCK_LOG)
			
				->filter(function($query) {
					
					if( Request::Has('PIS_MK') ){
						$query->where("S.PIS_MK",  Request::Input('PIS_MK') );
					}

					if( Request::Has('SIZES') ){
						$query->where("S.SIZES",  Request::Input('SIZES') );
					}
				}
		)->make(true);
	
	}

	// 재고로그
	public function setCheckYN(){
		
		$uptCnt = DB::table("STOCK_SUJO")
			->where("CORP_MK", $this->getCorpId())
			->where("SUJO_NO", Request::Input("SUJO_NO"))
			->update([
				'IS_CHECK'	=> Request::Input("IS_CHECK_YN")
			]
		);

		return response()->json(['success'=> $uptCnt ]);

	}

	public function Pdf()
	{

		$pdf = PDF::loadHTML("<h1>Test</h1>");

		// $pdf->save('myfile.pdf');
		return $pdf->stream();

	}

	
	// 재고정보 최대 SEQ 값 조회
	private function getMaxSeqInSTOCKINFO($pSIZES, $pPIS_MK){
		
		return $max_seq = STOCK_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("PIS_MK", $pPIS_MK)
									->where ("SIZES", $pSIZES)
									->first()->SEQ;
				
	}
	
	// 미수 최대 SEQ값 조회
	private function getMaxSeqInUNPROVINFO($pWRITE_DATE){
		return $max_seq = UNPROV_INFO::select( DB::raw('ISNULL(max("SEQ"), 0) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\STOCK_SUJO;
use App\PIS_INFO;
use App\CUST_CD;
use App\SALE_INFO_M;
use App\SALE_INFO_D;
use App\STOCK_INFO;

use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class StcsController extends Controller
{
    //
	public function index($stsc)
	{
		
		$MAX_WRITE_DATE	= DB::table("SALE_INFO_M")
						->select(DB::raw('CONVERT(CHAR(10), ISNULL(MAX(WRITE_DATE), GETDATE()), 23) AS MAX_WRITE_DATE'))
						->where("CORP_MK", $this->getCorpId())
						->first();

		return view("stsc.list", [ "stsc" => $stsc, 'MAX_T' => $MAX_WRITE_DATE->MAX_WRITE_DATE] );
	}

	// 기간별 판매 목록 조회
	public function listData()
	{

		$stock_query = "(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS QTY, SUM(INPUT_AMT) AS AMT, AVG(UNCS) AS UNCS, CORP_MK
						   FROM STOCK_INFO
						   WHERE (CORP_MK = '".$this->getCorpId()."')
							 AND (INPUT_DATE >= '".Request::Input("start_date")."')
							 AND (INPUT_DATE <= '".Request::Input("end_date")."')
							 AND (DE_CR_DIV = 1)
							 AND QTY > 0
							 AND UNCS > 0
							 AND INPUT_AMT > 0
							 AND OUTPUT_DATE IS NULL ";

		if( ! empty(Request::Input("textSearch") ) && Request::Input("textSearch") != ""){
			$stock_query.= " AND CUST_MK = '".Request::Input('textSearch')."'";
		}
		$stock_query.=" GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, CORP_MK ) AS I";


		//DB::connection()->setDatabaseName('blwc');
		$SALE_INFO_DATE = DB::table(DB::raw($stock_query)
						)->select(  "P.PIS_NM"
								  , "I.PIS_MK"
								  , "I.SIZES"
								  , "I.ORIGIN_NM"
								  , DB::raw("CONVERT(CHAR(10), I.INPUT_DATE, 23) AS INPUT_DATE" )
								  , DB::raw("ROUND(I.QTY, 1) AS QTY")
								  , DB::raw("FLOOR(I.UNCS) AS UNCS")
								  , DB::raw("ROUND(I.AMT, 2) AS AMT")
								  , DB::raw("ROUND(O.S_QTY, 2) AS S_QTY")
								  , DB::raw("ROUND(O.S_AMT, 2) AS S_AMT")
								  , DB::raw("ROUND( (I.QTY - ISNULL(O.S_QTY, 0)), 3) AS REST")
								  , DB::raw("O.S_AMT - I.AMT AS BENEFIT")
								  , DB::raw("ISNULL(S.SALE_UNCS, 0) AS SALE_UNCS")
								  , DB::raw("ISNULL( ROUND(S.SALE_UNCS * (ROUND(I.QTY, 1) - ISNULL(O.S_QTY, 0)), -3, 0), 0) AS RES_AMT")
								  , DB::raw("
											 STUFF ( (SELECT DISTINCT ',' + (CUST.FRNM)
														FROM STOCK_INFO AS ST
															 LEFT OUTER JOIN CUST_CD AS CUST
																		  ON CUST.CUST_MK = ST.CUST_MK
																	     AND CUST.CORP_MK = ST.CORP_MK
														WHERE ST.CORP_MK = '".$this->getCorpId()."'
														  AND ST.PIS_MK     = I.PIS_MK
														  AND ST.CORP_MK    = I.CORP_MK
														  AND ST.SIZES      = I.SIZES
														  AND ST.ORIGIN_NM  = I.ORIGIN_NM
														  AND ST.INPUT_DATE = I.INPUT_DATE
														  AND ST.DE_CR_DIV  = 1
														  AND CUST.CORP_DIV = 'P' 
														  FOR XML PATH('')), 1, 1, '') AS CUST_FRNM ")
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS S_QTY, SUM(AMT) AS S_AMT, CORP_MK
										   FROM SALE_INFO_D
										   WHERE (CORP_MK = '".$this->getCorpId()."')
										    AND (INPUT_DATE >= '".Request::Input("start_date")."')
										    AND (INPUT_DATE <= '".Request::Input("end_date")."')
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, CORP_MK
										) AS O
									  "), function($leftjoin){
											$leftjoin->on("I.CORP_MK", "=", "O.CORP_MK");
											$leftjoin->on("I.PIS_MK", "=", "O.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "O.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "O.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "O.INPUT_DATE");
									  }
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, AVG(SALE_UNCS) AS SALE_UNCS
										   FROM STOCK_SUJO
										  WHERE (CORP_MK = '".$this->getCorpId()."')
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS S
									"), function($leftjoin){
											$leftjoin->on("I.PIS_MK", "=", "S.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "S.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "S.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "S.INPUT_DATE");
									  }
						)->join("PIS_INFO AS P", function($join){
							$join->on("I.PIS_MK", "=", "P.PIS_MK");
							$join->on("I.CORP_MK", "=", "P.CORP_MK");
						})->where("P.CORP_MK", $this->getCorpId())
						->where(function($query){

							if( !empty( Request::Input("textSearch")) && ( Request::Input("textSearch"))){
								
							}
						
						});

		return Datatables::of($SALE_INFO_DATE)->filter(function($query) {
					
					
		})->make(true);

	}
	// 기간별 판매현황 PDF 출력
	public function Pdf(){

		$START_DATE	= Request::segment(4);
		$END_DATE	= Request::segment(5);

		$SALE_INFO_DATE = DB::table(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS QTY, SUM(INPUT_AMT) AS AMT, AVG(UNCS) AS UNCS
										   FROM STOCK_INFO
										   WHERE (CORP_MK = '".$this->getCorpId()."')
											 AND (INPUT_DATE >= '".$START_DATE."')
											 AND (INPUT_DATE <= '".$END_DATE."')
											 AND (DE_CR_DIV = 1)
											 AND (OUTPUT_DATE IS NULL)
										   GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS I
									  ")
						)->select(  "P.PIS_NM"
								  , "I.PIS_MK"
								  , "I.SIZES"
								  , "I.ORIGIN_NM"
								  , DB::raw( "CONVERT(CHAR(10), I.INPUT_DATE, 23) AS INPUT_DATE" )
								  , DB::raw( "ROUND(ISNULL(I.QTY, 0), 1) AS QTY")
								  , DB::raw( "FLOOR(I.UNCS) AS UNCS")
								  , "I.AMT"
								  , DB::raw( "ROUND(ISNULL(O.S_QTY, 0),1) AS S_QTY")
								  , "O.S_AMT"
								  , DB::raw("I.QTY - ISNULL(O.S_QTY, 0) AS REST")
								  , DB::raw("O.S_AMT - I.AMT AS BENEFIT")
								  , DB::raw("ISNULL(S.SALE_UNCS, 0) AS SALE_UNCS")
								  , DB::raw("ISNULL( ROUND( S.SALE_UNCS* (I.QTY - ISNULL(O.S_QTY, 0)), 0), 0) AS RES_AMT")
								  , DB::raw("
											 STUFF ( (SELECT DISTINCT ',' + (CUST.FRNM)
														FROM STOCK_INFO AS ST
															 LEFT OUTER JOIN CUST_CD AS CUST
																		  ON CUST.CUST_MK = ST.CUST_MK
																		 AND CUST.CORP_MK = ST.CORP_MK
												WHERE ST.CORP_MK = '".$this->getCorpId()."'
												  AND ST.PIS_MK     = I.PIS_MK
												  AND ST.SIZES      = I.SIZES
												  AND ST.ORIGIN_NM  = I.ORIGIN_NM
												  AND ST.INPUT_DATE = I.INPUT_DATE
												  AND ST.DE_CR_DIV  = 1
												  AND ST.OUTPUT_DATE IS NULL
												  AND CUST.CORP_DIV = 'P'
												  FOR XML PATH('')), 1, 1, '') AS CUST_FRNM ")
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, SUM(QTY) AS S_QTY, SUM(AMT) AS S_AMT
										   FROM SALE_INFO_D
										   WHERE (CORP_MK = '".$this->getCorpId()."')
											AND (INPUT_DATE >= '".$START_DATE."')
											AND (INPUT_DATE <= '".$END_DATE."')
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS O
									  "), function($leftjoin){
											$leftjoin->on("I.PIS_MK", "=", "O.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "O.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "O.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "O.INPUT_DATE");
									  }
						)->leftjoin(DB::raw("
										(SELECT PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE, AVG(SALE_UNCS) AS SALE_UNCS
										   FROM STOCK_SUJO
										  WHERE (CORP_MK = '".$this->getCorpId()."')
										  GROUP BY PIS_MK, SIZES, ORIGIN_NM, INPUT_DATE
										) AS S
									"), function($leftjoin){
											$leftjoin->on("I.PIS_MK", "=", "S.PIS_MK");
											$leftjoin->on("I.SIZES", "=", "S.SIZES");
											$leftjoin->on("I.ORIGIN_NM", "=", "S.ORIGIN_NM");
											$leftjoin->on("I.INPUT_DATE", "=", "S.INPUT_DATE");
									  }
						)->join("PIS_INFO AS P", function($join){
							$join->on("I.PIS_MK", "=", "P.PIS_MK");
							//$join->on("I.CORP_MK", "=", "P.CORP_MK");
						})->where("I.UNCS", ">", "0")
						->where("P.CORP_MK", $this->getCorpId())->get();


		$qtyInputQty = 0;
		$qtyInputAmt  = 0;

		$qtySaleQty  = 0;
		$qtySaleAmt  = 0;

		$benefitSum = 0;
		$resAmtSum = 0;

		foreach($SALE_INFO_DATE as $info){
			$qtyInputQty += (int)$info->QTY;
			$qtyInputAmt += (int)$info->AMT;

			$qtySaleQty  += (int)$info->S_QTY;
			$qtySaleAmt  += (int)$info->S_AMT;

			$benefitSum  += (int)$info->BENEFIT;
			$resAmtSum   += (int)$info->RES_AMT;
		}

		$pdf = PDF::loadView("stsc.Pdf",
								[
									'model'			=> $SALE_INFO_DATE,
									'start'			=> $START_DATE,
									'end'			=> $END_DATE,
									'qtyInputQty'	=> $qtyInputQty,
									'qtyInputAmt'	=> $qtyInputAmt,
									'qtySaleQty'	=> $qtySaleQty,
									'qtySaleAmt'	=> $qtySaleAmt,
									'benefitSum'	=> $benefitSum,
									'resAmtSum'		=> $resAmtSum,
								]
							);

		return $pdf->stream();
	}

	public function getTotal(){

		$SALE_INFO = DB::table('SALE_INFO_M AS SM')
						->select( DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.UNCL_AMT) as money),1))-1) AS TUNCL")
								, DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.TOTAL_SALE_AMT) as money),1))-1) AS TSALE  ")
								, DB::raw("CONVERT(varchar(18),cast(SUM(SD.SUMQTY) as money),1) AS TQTY ")
								, DB::raw("SUBSTRING(CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1),1, charindex('.',CONVERT(varchar(18),cast(SUM(SM.CASH_AMT) as money),1))-1) AS TCASH ")
						)->join(DB::raw("(SELECT CORP_MK, WRITE_DATE, CUST_MK, SEQ, SUM(QTY) AS SUMQTY
											FROM SALE_INFO_D AS D
										   GROUP BY CORP_MK, WRITE_DATE, CUST_MK, SEQ) AS SD "), function($join){

								$join->on("SM.CORP_MK", "=", "SD.CORP_MK");
								$join->on("SM.WRITE_DATE", "=", "SD.WRITE_DATE");
								$join->on("SM.CUST_MK", "=", "SD.CUST_MK" );
								$join->on("SM.SEQ", "=", "SD.SEQ" );

						})->where ( 'SM.CORP_MK', $this->getCorpId())->first();

		return response()->json($SALE_INFO);
	}

	// 일일판매 상세조회(목록형)
	public function detailData()
	{
		$SALE_INFO = DB::table("SALE_INFO_D AS D")
						->select("P.PIS_NM"
								, "D.CORP_MK"
								, "D.WRITE_DATE"
								, "D.ORIGIN_NM"
								, "D.ORIGIN_CD"
								, "D.CUST_MK"
								, "D.SEQ"
								, "D.PIS_MK"
								, "D.SIZES"
								, "D.QTY"
								, "D.UNCS"
								, "D.AMT"
								, "D.SRL_NO"
								, "D.REMARK")
						->join("PIS_INFO AS P", function($join){
								$join->on("D.PIS_MK", "=", "P.PIS_MK");
								$join->on("D.CORP_MK", "=", "P.CORP_MK");
							}
						)->where( 'D.CORP_MK', $this->getCorpId())
						->where( 'D.CUST_MK', Request::Input("cust_mk"))
						->where( 'D.SEQ', Request::Input("seq"))
						->where( 'D.WRITE_DATE', Request::Input("write_date"));


		return Datatables::of($SALE_INFO)->make(true);
	}

	// 판매저장
	public function setSaleInfoInsert(){


		/*
		$validator = Validator::make( Request::Input(), [
			'CUST_MK' => 'required|max:2,NOT_NULL',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		*/

		// 동일한 키가 있는 경우 실패메시지
		/*
		$count = CUST_GRP_INFO::where('CORP_MK', $this->getCorpId())->where('CUST_GRP_CD',  Request::Input("CUST_GRP_CD"))->count();
		if( $count > 0){
			return response()->json(['result' => 'fail', 'reason' => 'PrimaryKey'], 422);
		}
		*/


		DB::beginTransaction();

		$seq = $this->getMaxSeqInSALEINFO_M(Request::input('WRITE_DATE'), Request::input('CUST_MK')) + 1 ;


		try {

			// 판매내역 입력
			DB::table("SALE_INFO_M")->insert(
				[
					  'CORP_MK'				=> $this->getCorpId()
					, 'WRITE_DATE'			=> Request::input('WRITE_DATE')
					, 'CUST_MK'				=> Request::input('CUST_MK')
					, 'SEQ'					=> $seq
					, 'CASH_AMT'			=> Request::input('CASH_AMT')
					, 'CHECK_AMT'			=> Request::input('CHECK_AMT')
					, 'CARD_AMT'			=> Request::input('CARD_AMT')
					, 'UNCL_AMT'			=> Request::input('UNCL_AMT')
					, 'TOTAL_SALE_AMT'		=> Request::input('TOTAL_SALE_AMT')
					, 'SALE_DISCOUNT'		=> Request::input('SALE_DISCOUNT')
					, 'SALE_DISCOUNT_REMARK'=> Request::input('SALE_DISCOUNT_REMARK')
				] );

			$i = 0;
			foreach (Request::Input("DETAIL") as $item) {

				$qty = (int) $item[7];

				$stock_seq = $this->getMaxSeqInSTOCKINFO($item[4] , $item[12] ) + 1;

				$data = [

						  'CORP_MK'			=> $this->getCorpId()
						, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
						, 'CUST_MK'			=> Request::input('CUST_MK')
						, 'SEQ'				=> $seq
						, 'SRL_NO'			=> ++$i
						, 'PIS_MK'			=> $item[12]
						, 'SIZES'			=> $item[4]
						, 'INPUT_DATE'		=> $item[3]
						, 'ORIGIN_CD'		=> $item[11]
						, 'ORIGIN_NM'		=> $item[5]
						, 'QTY'				=> $item[7]
						, 'UNCS'			=> $item[8]
						, 'AMT'				=> $item[9]
						, 'STOCK_INFO_SEQ'	=> $stock_seq

					];

				DB::table("SALE_INFO_D")->insert( $data );

				DB::table("STOCK_INFO")->insert(
					[

						  'CORP_MK'			=> $this->getCorpId()
						, 'PIS_MK'			=> $item[12]
						, 'SIZES'			=> $item[4]
						, 'SEQ'				=> $stock_seq
						, 'CUST_MK'			=> Request::input('CUST_MK')
						, 'ORIGIN_CD'		=> $item[11]
						, 'ORIGIN_NM'		=> $item[5]
						, 'INPUT_DATE'		=> $item[3]
						, 'OUTPUT_DATE'		=> Request::input('WRITE_DATE')
						, 'OUTLINE'			=> "현섭판매저장 테스트??".$item[12]."에게 판매"
						, 'DE_CR_DIV'		=> 0
						, 'QTY'				=> $qty
						, 'CURR_QTY'		=> $qty

					]
				);

				STOCK_SUJO::where("CORP_MK", $this->getCorpId())
					->where("SUJO_NO", $item[0])
					->where("PIS_MK", $item[12])
					->where("SIZES",  $item[4])
					->update(
						[
							'QTY' => DB::raw("QTY - ".$qty)
						]
					);

			}

			if( (int)Request::input('UNCL_AMT')  > 0) {
				// 미수정보
				DB::table("UNCL_INFO")->insert(
					[
						  'CORP_MK'			=> $this->getCorpId()
						, 'WRITE_DATE'		=> Request::input('WRITE_DATE')
						, 'SEQ'				=> $this->getMaxSeqInUNCLINFO(Request::input('WRITE_DATE')) + 1
						, 'SALE_SEQ'		=> $seq
						, 'UNCL_CUST_MK'	=> Request::input('CUST_MK')
						, 'PROV_DIV'		=> 0
						, 'CASH_AMT'		=> 0
						, 'CHECK_AMT'		=> 0
						, 'CARD_AMT'		=> 0
						, 'UNCL_AMT'		=> Request::input('UNCL_AMT')
						, 'AMT'				=> Request::input('UNCL_AMT')
						, 'REMARK'			=> '판매미수'
					]
				);
			}
		}
		catch(ValidationException $e){
			DB::rollback();

			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);

		}catch(Exception $e){
			$errors = $e->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		DB::commit();
		return response()->json(['result' => 'success']);

	}

	// 일일판매 수정(목록형)
	public function getUpdateSaleList(){




	}

	// 일일판매 거래처 조회(목록형)
	public function getCustList(){

		$CUST_CD = DB::table("CUST_CD AS CUST")
						->select( "GRP.CORP_MK"
								 , "GRP.CUST_GRP_NM"
								 , "CUST.CUST_MK"
								 , "CUST.FRNM"
								 , "CUST.RPST"
								 , "CUST.PHONE_NO"
								 , "CUST.RANK"
								 , "CUST.AREA_CD"
								 , "AREA.AREA_NM")
						->leftjoin("CUST_GRP_INFO AS GRP", function($join){
								$join->on("GRP.CUST_GRP_CD", "=", "CUST.CUST_GRP_CD");
								$join->on("GRP.CORP_MK", "=", "CUST.CORP_MK");
							}
						)->join( "AREA_CD AS AREA", function($join){
								$join->on("AREA.AREA_CD", "=", "CUST.AREA_CD");
							}
						)->where( 'GRP.CORP_MK', $this->getCorpId())
						->whereNotIn('CUST.CORP_DIV', ['J', 'A', 'P']);

		return Datatables::of($CUST_CD)

				->filter(function($query) {


					if( Request::Has('srtCondition') && Request::Input('srtCondition') != ''){
						$query->where("GRP.CUST_GRP_CD",  Request::Input('srtCondition') )
							->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
					}else{
						$query->where("CUST.FRNM",  "like", "%".Request::Input('textSearch')."%");
					}

				})->make(true);

	}

	// 총미수금 불러오기
	public function getTotalUncl(){

		$UNCL_INFO = DB::table( DB::raw("
										( SELECT  UNCL_CUST_MK
											  , CORP_MK
											  , ISNULL(CASE PROV_DIV WHEN '0' THEN SUM(AMT) END , 0) UNCL
											  , ISNULL(CASE PROV_DIV WHEN '1' THEN SUM(AMT) END , 0) UNUNCL
										   FROM UNCL_INFO
									    GROUP BY UNCL_CUST_MK, CORP_MK, PROV_DIV
										) AS A
									")
								)
								->select( DB::raw("SUM(A.UNCL) - SUM(A.UNUNCL) AS TOTAL_UNCL"))
								->where ( 'A.CORP_MK', $this->getCorpId())
								->where ( 'A.UNCL_CUST_MK', Request::Input("UNCL_CUST_MK"))
								->groupBy("A.UNCL_CUST_MK","A.CORP_MK")->first();

		return response()->json($UNCL_INFO);

	}

	// 업체별판매통계 거래처 목록
	public function listCustSale($stcs){

		$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")
					->where("CORP_MK", $this->getCorpId())->get();

		return view("stsc.listCustSale", [ "stcs" => $stcs, "CUST_GRP_INFO" => $CUST_GRP_INFO] );
	}

	// 업체별판매통계 월별 통계 상세
	public function listCustSaleDetail($stcs){

		return view("stsc.listCustSaleDetail", [ "stcs" => $stcs ] );
	}

	public function getDataCustInput($stcs){


		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
			'CUST_MK'		=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$SALE_INFO_D = DB::table(DB::raw("( SELECT CORP_MK, CUST_MK, PIS_MK, SIZES,  SUM(A.QTY) AS TQTY, SUM(QTY * UNCS - INPUT_DISCOUNT)/10000 AS TAMT
												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M

												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A12M

											FROM STOCK_INFO AS A
											WHERE CORP_MK='".$this->getCorpId()."'
											  AND CUST_MK='".urldecode(Request::Input("CUST_MK"))."'
											  AND YEAR(INPUT_DATE)='".Request::Input("YEAR")."'
											GROUP BY CORP_MK, CUST_MK, PIS_MK, SIZES
											) AS A"))
								->select( "A.*"
										 ,"P.PIS_NM"
										 ,"C.FRNM"
										)
								->join("CUST_CD AS C", function($join){
										$join->on("A.CORP_MK", "=", "C.CORP_MK");
										$join->on("A.CUST_MK", "=", "C.CUST_MK");
									}
								)->join("PIS_INFO AS P", function($join){
										$join->on("A.PIS_MK", "=", "P.PIS_MK");
										$join->on("A.CORP_MK", "=", "P.CORP_MK");
									}
								)->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES",Request::Input("SIZES"));
									}

									if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
										$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", Request::Input("PIS_MK"));
									}

									if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
										$query->where("A.CUST_MK", urldecode(Request::Input("CUST_MK")));
									}

								});

		return Datatables::of($SALE_INFO_D)->make(true);

	}

	// 업체별판매통계 데이터 조회
	public function getDataCustInputTotal($stcs){


		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
			'CUST_MK'		=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$SALE_INFO_D = DB::table("STOCK_INFO AS A")
								->select(	  DB::raw("ISNULL(ROUND(SUM(QTY), 2), 0) AS TQTY")
											, DB::raw("ISNULL(ROUND(SUM((QTY * UNCS - INPUT_DISCOUNT) /10000) , 2), 0)AS TAMT")

											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) , 2), 0) AS Q1M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) , 2), 0) AS Q2M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) , 2), 0) AS Q3M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) , 2), 0) AS Q4M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) , 2), 0) AS Q5M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) , 2), 0) AS Q6M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) , 2), 0) AS Q7M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) , 2), 0) AS Q8M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) , 2), 0) AS Q9M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) , 2), 0) AS Q10M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) , 2), 0) AS Q11M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) , 2), 0) AS Q12M")

											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A1M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A2M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A3M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A4M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A5M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A6M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A7M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A8M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A9M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A10M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A11M")
											, DB::raw("ISNULL(ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 , 2), 0) AS A12M")

										)
								->leftjoin("CUST_CD AS C", function($join){
										$join->on("A.CORP_MK", "=", "C.CORP_MK");
										$join->on("A.CUST_MK", "=", "C.CUST_MK");
									}
								)->leftjoin("PIS_INFO AS P", function($join){
										$join->on("A.PIS_MK", "=", "P.PIS_MK");
										$join->on("A.CORP_MK", "=", "P.CORP_MK");
									}
								)->where("A.CORP_MK", $this->getCorpId())
								 ->where(DB::raw("YEAR(A.INPUT_DATE)"), Request::Input("YEAR"))
								 ->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES",Request::Input("SIZES"));
									}

									if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
										$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", Request::Input("PIS_MK"));
									}

									if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
										$query->where("A.CUST_MK", Request::Input("CUST_MK"));
									}

								});

		return Datatables::of($SALE_INFO_D)->make(true);

	}


	public function getDataCustSale($stcs){


		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
			'CUST_MK'		=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		$SALE_INFO_D = DB::table(DB::raw("( SELECT A.CORP_MK
												 , A.PIS_MK
												 , A.SIZES
												 , A.CUST_MK
												 , ROUND( SUM(A.QTY), 0) AS TQTY
												 , ROUND(SUM(A.AMT)/10000, 0) AS TAMT
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0) AS Q2M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0) AS Q3M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0) AS Q4M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0) AS Q5M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0) AS Q6M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0) AS Q7M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0) AS Q8M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0) AS Q9M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0) AS Q10M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0) AS Q11M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0) AS Q12M

												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0) AS A1M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0) AS A2M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0) AS A3M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0) AS A4M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000, 0) AS A5M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0) AS A6M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0) AS A7M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0) AS A8M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0) AS A9M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0) AS A10M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0) AS A11M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0) AS A12M

											 FROM SALE_INFO_D A
												  LEFT OUTER JOIN CUST_CD B
												          ON A.CUST_MK = B.CUST_MK
												         AND B.CORP_DIV='S'
												         AND B.CORP_MK='".urldecode(Request::Input("CUST_MK"))."'
										    WHERE YEAR(A.WRITE_DATE) = ".Request::Input("YEAR")."
										    GROUP BY A.CORP_MK, A.PIS_MK, A.SIZES, A.CUST_MK
										   HAVING A.CORP_MK='".$this->getCorpId()."'
										      AND A.CUST_MK='".urldecode(Request::Input("CUST_MK"))."'

											) A "))
								->select( "A.*"
										 ,"B.PIS_NM"
										)
								->leftjoin("PIS_INFO AS B", function($join){
										$join->on("A.PIS_MK", "=", "B.PIS_MK");
										$join->on("A.CORP_MK", "=", "B.CORP_MK");
									}
								)->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES", Request::Input("SIZES"));
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", Request::Input("PIS_MK"));
									}

								});

		return Datatables::of($SALE_INFO_D)->make(true);

	}

	// 업체별판매통계 데이터 조회
	public function getDataCustSaleTotal($stcs){

		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
			'CUST_MK'		=> 'required',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$SALE_INFO_D = DB::table("SALE_INFO_D AS A")
								->select(	  DB::raw("ROUND(SUM(A.QTY), 0) AS TQTY")
											, DB::raw("ROUND(SUM(A.AMT)/10000, 0) AS TAMT")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0) AS Q2M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0) AS Q3M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0) AS Q4M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0) AS Q5M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0) AS Q6M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0) AS Q7M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0) AS Q8M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0) AS Q9M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0) AS Q10M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0) AS Q11M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0) AS Q12M")

											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0)AS A1M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0) AS A2M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0) AS A3M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0) AS A4M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000, 0) AS A5M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0) AS A6M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0) AS A7M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0) AS A8M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0) AS A9M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0) AS A10M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0) AS A11M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0) AS A12M")
										)
							->leftjoin("CUST_CD AS B", function($join){
									$join->on("A.CUST_MK", "=", "B.CUST_MK");
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin("PIS_INFO AS C", function($join){
									$join->on("A.PIS_MK", "=", "C.PIS_MK");
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->where("A.CORP_MK", $this->getCorpId())
							 ->where("B.CORP_MK", $this->getCorpId())
							 ->where("B.CORP_DIV", 'S')
							 ->where(function($query){

								if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
									$query->where("A.CUST_MK", urldecode(Request::Input("CUST_MK")) );
								}

								if( Request::has("YEAR") && Request::Input("YEAR") != ""){
									$query->where(DB::raw("YEAR(A.WRITE_DATE)"), Request::Input("YEAR"));
								}

								if( Request::has("SIZES") && Request::Input("SIZES") != ""){
									$query->where("A.SIZES", Request::Input("SIZES"));
								}

								if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
									$query->where("A.PIS_MK", Request::Input("PIS_MK"));
								}

							});

		return Datatables::of($SALE_INFO_D)->make(true);

	}



	// 업체별매입통계
	public function listCustInput($stcs){

		$CUST_GRP_INFO = DB::table("CUST_GRP_INFO")
					->where("CORP_MK", $this->getCorpId())->get();

		return view("stsc.listCustInput", [ "stcs" => $stcs, "CUST_GRP_INFO" => $CUST_GRP_INFO] );
	}

	// 업체별매입통계목록 상세
	public function listCustInputDetail($stcs){
		return view("stsc.listCustInputDetail", [ "stcs" => $stcs ] );
	}

	// 어종별판매통계
	public function listPisSale($stcs){

		return view("stsc.listPisSale", [ "stcs" => $stcs ] );
	}

	// 어종별판매통계 조회
	public function getDataPisSale($stcs){


		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$SALE_INFO_TABLE = DB::table(DB::raw(
										"( SELECT CORP_MK
												, PIS_MK
												, SIZES
												, ROUND(SUM(QTY), 0) AS TQTY
												, ROUND(SUM(AMT)/10000, 0) AS TAMT
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0)  AS Q2M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0)  AS Q3M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0)  AS Q4M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0)  AS Q5M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0)  AS Q6M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0)  AS Q7M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0)  AS Q8M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0)  AS Q9M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0)  AS Q10M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0)  AS Q11M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0)  AS Q12M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0)  AS A1M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0)  AS A2M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0)  AS A3M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0)  AS A4M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000 , 0) AS A5M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0)  AS A6M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0)  AS A7M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0)  AS A8M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0)  AS A9M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0)  AS A10M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0)  AS A11M
												, ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0)  AS A12M
											FROM SALE_INFO_D
										   WHERE YEAR(WRITE_DATE) = '".Request::Input("YEAR")."'
										   GROUP BY CORP_MK, PIS_MK, SIZES
										  HAVING CORP_MK= '".$this->getCorpId()."'
										) AS A")
									)
						->select("A.*", "B.PIS_NM")
						->leftjoin("PIS_INFO AS B", function($join){
								$join->on("A.PIS_MK", "=", "B.PIS_MK");
								$join->on("A.CORP_MK", "=", "B.CORP_MK");
							}
						)->where(function($query){

							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", "LIKE", Request::Input("SIZES")."%"  );
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("B.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}

						});

		return Datatables::of($SALE_INFO_TABLE)->make(true);

	}


	// 어종별판매합계
	public function getPisSaleTotal(){
		/* 합계  */

		$TOTAL_SALE = DB::table("SALE_INFO_D AS A")
					->select(	  "A.CORP_MK"
								, DB::raw("ROUND(SUM(QTY), 0) AS TQTY, ROUND(SUM(AMT)/10000, 0) AS TAMT")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0) AS Q2M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0) AS Q3M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0) AS Q4M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0) AS Q5M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0) AS Q6M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0) AS Q7M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0) AS Q8M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0) AS Q9M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0) AS Q10M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0) AS Q11M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0) AS Q12M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0) AS A1M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0) AS A2M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0) AS A3M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0) AS A4M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000, 0) AS A5M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0) AS A6M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0) AS A7M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0) AS A8M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0) AS A9M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0) AS A10M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0) AS A11M")
								, DB::raw("ROUND(SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0) AS A12M")
							)
					->leftjoin("PIS_INFO AS B", function($join){
							$join->on("A.PIS_MK", "=", "B.PIS_MK");
							$join->on("A.CORP_MK", "=", "B.CORP_MK");
					})->where(DB::raw("YEAR(WRITE_DATE)"), Request::Input("YEAR"))
					  ->where(function($query){
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("B.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
							}
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}
					})->groupBy("A.CORP_MK")
					->having("A.CORP_MK", "=", $this->getCorpId());

		return Datatables::of($TOTAL_SALE)->make(true);

	}

	// 어종별매입통계
	public function listPisInput($stcs){

		return view("stsc.listPisInput", [ "stcs" => $stcs ] );
	}

	// 어종별매입통계 조회
	public function getDataPisInput($stcs){


		$validator = Validator::make( Request::Input(), [
			'YEAR'			=> 'required|numeric',
		]);

		if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors);
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$STOCK_INFO_TABLE = DB::table(DB::raw(
										"( SELECT CORP_MK
												, PIS_MK
												, SIZES
												, ROUND(SUM(QTY), 2) AS TQTY
												, ROUND(SUM(QTY * UNCS - INPUT_DISCOUNT), 2) AS TAMT
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END), 2) AS Q1M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END), 2) AS Q2M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END), 2) AS Q3M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END), 2) AS Q4M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END), 2) AS Q5M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END), 2) AS Q6M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END), 2) AS Q7M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END), 2) AS Q8M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END), 2) AS Q9M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END), 2) AS Q10M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END), 2) AS Q11M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END), 2) AS Q12M

												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A1M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A2M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A3M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A4M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A5M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A6M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A7M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A8M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A9M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A10M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A11M
												, ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A12M
											FROM STOCK_INFO AS A
										   WHERE YEAR(INPUT_DATE) = '".Request::Input("YEAR")."'
										     AND DE_CR_DIV = '1'
										   GROUP BY CORP_MK, PIS_MK, SIZES
										  HAVING CORP_MK= '".$this->getCorpId()."' ) AS A_1")
									)
						->select(  "P.PIS_NM"
								 , "A_1.CORP_MK"
								 , "A_1.PIS_MK"
								 , "A_1.SIZES"
								 , DB::raw("ROUND(A_1.TQTY, 2) AS TQTY")
								 , DB::raw("A_1.TAMT / 10000 AS TAMT")
								 , "A_1.Q1M"
								 , "A_1.Q2M"
								 , "A_1.Q3M"
								 , "A_1.Q4M"
								 , "A_1.Q5M"
								 , "A_1.Q6M"
								 , "A_1.Q7M"
								 , "A_1.Q8M"
								 , "A_1.Q9M"
								 , "A_1.Q10M"
								 , "A_1.Q11M"
								 , "A_1.Q12M"
								 , "A_1.A1M"
								 , "A_1.A2M"
								 , "A_1.A3M"
								 , "A_1.A4M"
								 , "A_1.A5M"
								 , "A_1.A6M"
								 , "A_1.A7M"
								 , "A_1.A8M"
								 , "A_1.A9M"
								 , "A_1.A10M"
								 , "A_1.A11M"
								 , "A_1.A12M"
						)->leftjoin("PIS_INFO AS P", function($join){
								$join->on("A_1.PIS_MK", "=", "P.PIS_MK");
								$join->on("A_1.CORP_MK", "=", "P.CORP_MK");
							}
						)->where(function($query){
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A_1.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A_1.PIS_MK", Request::Input("PIS_MK"));
							}
						});

		return Datatables::of($STOCK_INFO_TABLE)->make(true);

	}

	// 어종별판매합계
	public function getPisInputTotal(){
		/* 합계*/
		/*
		 */

		$TOTAL_STOCK = DB::table("STOCK_INFO AS A")
						->select( DB::raw("ROUND(SUM(A.QTY), 2) AS TQTY")
								, DB::raw("ROUND(SUM((A.QTY * A.UNCS - A.INPUT_DISCOUNT) / 10000), 2) AS TAMT")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END), 2) AS Q1M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END), 2) AS Q2M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END), 2) AS Q3M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END), 2) AS Q4M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END), 2) AS Q5M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END), 2) AS Q6M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END), 2) AS Q7M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END), 2) AS Q8M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END), 2) AS Q9M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END), 2) AS Q10M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END), 2) AS Q11M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END), 2) AS Q12M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A1M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A2M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A3M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A4M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A5M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A6M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A7M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A8M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A9M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A10M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A11M")
								, DB::raw("ROUND(SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000, 2) AS A12M")
							)
					->join("PIS_INFO AS P", function($join){
							$join->on("P.PIS_MK", "=", "A.PIS_MK");
							$join->on("P.CORP_MK", "=", "A.CORP_MK");
					})->where(DB::raw("YEAR(A.INPUT_DATE)"), Request::Input("YEAR"))
					  ->where("A.DE_CR_DIV", "1")
					  ->where("A.CORP_MK", $this->getCorpId())
					  ->where(function($query){
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}
					});

		return Datatables::of($TOTAL_STOCK)->make(true);

	}

	// 업체별 매출통계 PDF
	public function PdflistCustSaleDetail(){

		$CUST_MK	= urldecode(Request::Input("CUST_MK"));
		$SIZES		= Request::Input("SIZES");
		$YEAR		= Request::Input("YEAR");
		$PIS_MK		= Request::Input("PIS_MK");

		// 동일한 키가 있는 경우 실패메시지
		$STOCK_INFO_TABLE = DB::table(DB::raw("( SELECT A.CORP_MK
												 , A.PIS_MK
												 , A.SIZES
												 , A.CUST_MK
												 , ROUND( SUM(A.QTY), 0) AS TQTY
												 , ROUND(SUM(A.AMT)/10000, 0) AS TAMT
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0) AS Q2M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0) AS Q3M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0) AS Q4M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0) AS Q5M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0) AS Q6M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0) AS Q7M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0) AS Q8M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0) AS Q9M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0) AS Q10M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0) AS Q11M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0) AS Q12M

												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0) AS A1M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0) AS A2M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0) AS A3M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0) AS A4M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000, 0) AS A5M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0) AS A6M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0) AS A7M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0) AS A8M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0) AS A9M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0) AS A10M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0) AS A11M
												 , ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0) AS A12M

											 FROM SALE_INFO_D A
												  LEFT OUTER JOIN CUST_CD B
												          ON A.CUST_MK = B.CUST_MK
												        -- AND B.CORP_DIV='S'
												         AND B.CORP_MK='".$CUST_MK."'
										    WHERE YEAR(A.WRITE_DATE) = ".$YEAR."
										    GROUP BY A.CORP_MK, A.PIS_MK, A.SIZES, A.CUST_MK
										   HAVING A.CORP_MK='".$this->getCorpId()."'
										      AND A.CUST_MK='".urldecode($CUST_MK)."'

											) A "))
								->select( "A.*"
										 ,"P.PIS_NM"
										)
								->leftjoin("PIS_INFO AS P", function($join){
										$join->on("A.PIS_MK", "=", "P.PIS_MK");
										$join->on("A.CORP_MK", "=", "P.CORP_MK");
									}
								)->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES", Request::Input("SIZES"));
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", urldecode(Request::Input("PIS_MK")));
									}

								})->get();

		$TOTAL_STOCK = DB::table("SALE_INFO_D AS A")
								->select(	  DB::raw("ROUND(SUM(A.QTY), 0) AS TQTY")
											, DB::raw("ROUND(SUM(A.AMT)/10000, 0) AS TAMT")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END), 0) AS Q1M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END), 0) AS Q2M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END), 0) AS Q3M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END), 0) AS Q4M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END), 0) AS Q5M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END), 0) AS Q6M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END), 0) AS Q7M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END), 0) AS Q8M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END), 0) AS Q9M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END), 0) AS Q10M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END), 0) AS Q11M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END), 0) AS Q12M")

											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000, 0)AS A1M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000, 0) AS A2M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000, 0) AS A3M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000, 0) AS A4M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000, 0) AS A5M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000, 0) AS A6M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000, 0) AS A7M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000, 0) AS A8M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000, 0) AS A9M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000, 0) AS A10M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000, 0) AS A11M")
											, DB::raw("ROUND(SUM(CASE MONTH(A.WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000, 0) AS A12M")
										)
							->leftjoin("CUST_CD AS B", function($join){
									$join->on("A.CUST_MK", "=", "B.CUST_MK");
									$join->on("A.CORP_MK", "=", "B.CORP_MK");
								}
							)->leftjoin("PIS_INFO AS C", function($join){
									$join->on("A.PIS_MK", "=", "C.PIS_MK");
									$join->on("A.CORP_MK", "=", "C.CORP_MK");
								}
							)->where("A.CORP_MK", $this->getCorpId())
							 ->where("B.CORP_MK", $this->getCorpId())
							 ->where("B.CORP_DIV", 'S')
							 ->where(function($query){

								if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
									$query->where("A.CUST_MK", urldecode(Request::Input("CUST_MK")) );
								}

								if( Request::has("YEAR") && Request::Input("YEAR") != ""){
									$query->where(DB::raw("YEAR(A.WRITE_DATE)"), Request::Input("YEAR"));
								}

								if( Request::has("SIZES") && Request::Input("SIZES") != ""){
									$query->where("A.SIZES", Request::Input("SIZES"));
								}

								if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
									$query->where("A.PIS_MK", Request::Input("PIS_MK"));
								}

							})->get();

		$CUST_FRNM = DB::table("CUST_CD")->where('CUST_MK', urldecode($CUST_MK))->where("CORP_MK", $this->getCorpId())->first();
		//dd($CUST_FRNM );
		
		
		$pdf = PDF::loadView("stsc.pdfListCustSaleDetail",
								[
									'list'		=> $STOCK_INFO_TABLE,
									'sum'		=> $TOTAL_STOCK,
									'year'		=> $YEAR,
									'custfrnm'	=> $CUST_FRNM->FRNM,
								]
							)->setPaper('a4', 'landscape');
		return $pdf->stream("판매현황.pdf");
	}

	// 어종별 판매 통계 PDF
	public function PdflistPisSale(){


		$SALE_INFO_TABLE = DB::table(DB::raw(
										"( SELECT CORP_MK
												, PIS_MK
												, SIZES
												, SUM(QTY) AS TQTY
												, SUM(AMT)/10000 AS TAMT
												, SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '01' THEN AMT ELSE 0 END)/10000 AS A1M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '02' THEN AMT ELSE 0 END)/10000 AS A2M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '03' THEN AMT ELSE 0 END)/10000 AS A3M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '04' THEN AMT ELSE 0 END)/10000 AS A4M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '05' THEN AMT ELSE 0 END)/10000 AS A5M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '06' THEN AMT ELSE 0 END)/10000 AS A6M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '07' THEN AMT ELSE 0 END)/10000 AS A7M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '08' THEN AMT ELSE 0 END)/10000 AS A8M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '09' THEN AMT ELSE 0 END)/10000 AS A9M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '10' THEN AMT ELSE 0 END)/10000 AS A10M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '11' THEN AMT ELSE 0 END)/10000 AS A11M
												, SUM(CASE MONTH(WRITE_DATE) WHEN '12' THEN AMT ELSE 0 END)/10000 AS A12M
											FROM SALE_INFO_D
										   WHERE YEAR(WRITE_DATE) = '".Request::Input("YEAR")."'
										   GROUP BY CORP_MK, PIS_MK, SIZES
										  HAVING CORP_MK= '".$this->getCorpId()."'
										) AS A")
									)
						->select("A.*", "B.PIS_NM")
						->leftjoin("PIS_INFO AS B", function($join){
								$join->on("A.PIS_MK", "=", "B.PIS_MK");
								$join->on("A.CORP_MK", "=", "B.CORP_MK");
							}
						)->where(function($query){

							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("B.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}


						})->get();


		$TOTAL_STOCK = DB::table("STOCK_INFO AS A")
						->select( DB::raw("SUM(A.QTY) AS TQTY")
								, DB::raw("SUM((A.QTY * A.UNCS - A.INPUT_DISCOUNT) / 10000) AS TAMT")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A1M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A2M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A3M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A4M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A5M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A6M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A7M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A8M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A9M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A10M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A11M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A12M")
							)
					->join("PIS_INFO AS P", function($join){
							$join->on("P.PIS_MK", "=", "A.PIS_MK");
							$join->on("P.CORP_MK", "=", "A.CORP_MK");
					})->where(DB::raw("YEAR(A.INPUT_DATE)"), Request::Input("YEAR"))
					  ->where("A.DE_CR_DIV", "1")
					  ->where("A.CORP_MK", $this->getCorpId())
					  ->where(function($query){
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
							}
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}


					})->get();

		$CORP_FRNM = DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->first();
		$pdf = PDF::loadView("stsc.PdflistPisSale",
								[
									'list'		=> $SALE_INFO_TABLE,
									'sum'		=> $TOTAL_STOCK,
									'year'		=> Request::Input("YEAR"),
									'custfrnm'	=> $CORP_FRNM->FRNM,
								]
							)->setPaper('a4', 'landscape');
		

		return $pdf->stream();
		
	}




	//업체별 매입 통계 PDF

	public function PdflistCustInput(){

		$SALE_INFO = DB::table(DB::raw("( SELECT CORP_MK, CUST_MK, PIS_MK, SIZES,  SUM(A.QTY) AS TQTY, SUM(QTY * UNCS - INPUT_DISCOUNT)/10000 AS TAMT
												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M

												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A12M

											FROM STOCK_INFO AS A
											WHERE CORP_MK='".$this->getCorpId()."'
											  AND CUST_MK='".Request::Input("CUST_MK")."'
											  AND YEAR(INPUT_DATE)='".Request::Input("YEAR")."'
											GROUP BY CORP_MK, CUST_MK, PIS_MK, SIZES
											) AS A"))
								->select( "A.*"
										 ,"P.PIS_NM"
										 ,"C.FRNM"
										)
								->join("CUST_CD AS C", function($join){
										$join->on("A.CORP_MK", "=", "C.CORP_MK");
										$join->on("A.CUST_MK", "=", "C.CUST_MK");
									}
								)->join("PIS_INFO AS P", function($join){
										$join->on("A.PIS_MK", "=", "P.PIS_MK");
										$join->on("A.CORP_MK", "=", "P.CORP_MK");
									}
								)->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES",Request::Input("SIZES"));
									}

									if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
										$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", Request::Input("PIS_MK"));
									}

									if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
										$query->where("A.CUST_MK", Request::Input("CUST_MK"));
									}

								})->get();

		// 동일한 키가 있는 경우 실패메시지
		$SALE_INFO_D = DB::table("STOCK_INFO AS A")
								->select(	  DB::raw("SUM(QTY) AS TQTY")
											, DB::raw("SUM((QTY * UNCS - INPUT_DISCOUNT) /10000) AS TAMT")

											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M")

											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A1M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A2M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A3M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A4M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A5M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A6M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A7M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A8M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A9M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A10M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A11M")
											, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END)/10000 AS A12M")

										)
								->leftjoin("CUST_CD AS C", function($join){
										$join->on("A.CORP_MK", "=", "C.CORP_MK");
										$join->on("A.CUST_MK", "=", "C.CUST_MK");
									}
								)->leftjoin("PIS_INFO AS P", function($join){
										$join->on("A.PIS_MK", "=", "P.PIS_MK");
										$join->on("A.CORP_MK", "=", "P.CORP_MK");
									}
								)->where("A.CORP_MK", $this->getCorpId())
								 ->where(DB::raw("YEAR(A.INPUT_DATE)"), Request::Input("YEAR"))
								 ->where(function($query){

									if( Request::has("SIZES") && Request::Input("SIZES") != ""){
										$query->where("A.SIZES",Request::Input("SIZES"));
									}

									if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
										$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%");
									}

									if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
										$query->where("A.PIS_MK", Request::Input("PIS_MK"));
									}

									if( Request::has("CUST_MK") && Request::Input("CUST_MK") != ""){
										$query->where("A.CUST_MK", Request::Input("CUST_MK"));
									}

								})->get();

		$CUST_FRNM = DB::table("CUST_CD")->where("CORP_MK", $this->getCorpId())->where('CUST_MK', Request::Input("CUST_MK") )->first();
		//dd($SALE_INFO_TABLE);
		
		$year =  Request::Input("YEAR");
		$frnm = $CUST_FRNM->FRNM;
		/*
		$pdf = Excel::create('New file', function($excel) use ( $SALE_INFO, $SALE_INFO_D,$year , $frnm){

			$excel->sheet('New sheet', function($sheet) use ( $SALE_INFO, $SALE_INFO_D,  $year ,  $frnm){

				$sheet->loadView('stsc.pdfListCustInput'		);

			});

		})->store('pdf');

		*/
		$pdf = PDF::loadView("stsc.pdfListCustInput",
								[
									'list'		=> $SALE_INFO ,
									'sum'		=> $SALE_INFO_D,
									'year'		=> Request::Input("YEAR"),
									'custfrnm'	=> $CUST_FRNM->FRNM,
								]
							)->setPaper('a4', 'landscape');
		return $pdf->stream("거래처별매입현황.pdf");
		
	}


	// 어종별 매입 통계 PDF
	public function PdflistPisInput(){


		// 동일한 키가 있는 경우 실패메시지
		$STOCK_INFO_TABLE = DB::table(DB::raw(
										"( SELECT CORP_MK
												, PIS_MK
												, SIZES
												, SUM(QTY) AS TQTY
												, SUM(QTY * UNCS - INPUT_DISCOUNT) AS TAMT
												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M

												, SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A1M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A2M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A3M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A4M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A5M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A6M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A7M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A8M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A9M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A10M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A11M
												, SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A12M
											FROM STOCK_INFO AS A
										   WHERE YEAR(INPUT_DATE) = '".Request::Input("YEAR")."'
										     AND DE_CR_DIV = '1'
										   GROUP BY CORP_MK, PIS_MK, SIZES
										  HAVING CORP_MK= '".$this->getCorpId()."' ) AS A_1")
									)
						->select(  "P.PIS_NM"
								 , "A_1.CORP_MK"
								 , "A_1.PIS_MK"
								 , "A_1.SIZES"
								 , "A_1.TQTY"
								 , DB::raw("A_1.TAMT / 10000 AS TAMT")
								 , "A_1.Q1M"
								 , "A_1.Q2M"
								 , "A_1.Q3M"
								 , "A_1.Q4M"
								 , "A_1.Q5M"
								 , "A_1.Q6M"
								 , "A_1.Q7M"
								 , "A_1.Q8M"
								 , "A_1.Q9M"
								 , "A_1.Q10M"
								 , "A_1.Q11M"
								 , "A_1.Q12M"
								 , "A_1.A1M"
								 , "A_1.A2M"
								 , "A_1.A3M"
								 , "A_1.A4M"
								 , "A_1.A5M"
								 , "A_1.A6M"
								 , "A_1.A7M"
								 , "A_1.A8M"
								 , "A_1.A9M"
								 , "A_1.A10M"
								 , "A_1.A11M"
								 , "A_1.A12M"
						)->leftjoin("PIS_INFO AS P", function($join){
								$join->on("A_1.PIS_MK", "=", "P.PIS_MK");
								$join->on("A_1.CORP_MK", "=", "P.CORP_MK");
							}
						)->where(function($query){
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A_1.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A_1.PIS_MK", Request::Input("PIS_MK"));
							}
						})->get();

		
		$TOTAL_STOCK = DB::table("STOCK_INFO AS A")
						->select( DB::raw("SUM(A.QTY) AS TQTY")
								, DB::raw("SUM((A.QTY * A.UNCS - A.INPUT_DISCOUNT) / 10000) AS TAMT")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY ELSE 0 END) AS Q1M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY ELSE 0 END) AS Q2M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY ELSE 0 END) AS Q3M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY ELSE 0 END) AS Q4M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY ELSE 0 END) AS Q5M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY ELSE 0 END) AS Q6M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY ELSE 0 END) AS Q7M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY ELSE 0 END) AS Q8M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY ELSE 0 END) AS Q9M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY ELSE 0 END) AS Q10M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY ELSE 0 END) AS Q11M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY ELSE 0 END) AS Q12M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '01' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A1M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '02' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A2M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '03' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A3M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '04' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A4M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '05' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A5M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '06' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A6M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '07' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A7M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '08' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A8M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '09' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A9M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '10' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A10M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '11' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A11M")
								, DB::raw("SUM(CASE MONTH(INPUT_DATE) WHEN '12' THEN QTY * UNCS - INPUT_DISCOUNT ELSE 0 END) / 10000 AS A12M")
							)
					->join("PIS_INFO AS P", function($join){
							$join->on("P.PIS_MK", "=", "A.PIS_MK");
							$join->on("P.CORP_MK", "=", "A.CORP_MK");
					})->where(DB::raw("YEAR(A.INPUT_DATE)"), Request::Input("YEAR"))
					  ->where("A.DE_CR_DIV", "1")
					  ->where("A.CORP_MK", $this->getCorpId())
					  ->where(function($query){
							if( Request::has("SIZES") && Request::Input("SIZES") != ""){
								$query->where("A.SIZES", Request::Input("SIZES"));
							}
							if( Request::has("PIS_NM") && Request::Input("PIS_NM") != ""){
								$query->where("P.PIS_NM", "LIKE", "%".Request::Input("PIS_NM")."%" );
							}
							if( Request::has("PIS_MK") && Request::Input("PIS_MK") != ""){
								$query->where("A.PIS_MK", Request::Input("PIS_MK"));
							}
					})->get();
		
		$CORP_FRNM = DB::table("CORP_INFO")->where("CORP_MK", $this->getCorpId())->first();
		$pdf = PDF::loadView("stsc.pdfListPisInput",
								[
									'list'		=> $STOCK_INFO_TABLE,
									'sum'		=> $TOTAL_STOCK,
									'year'		=> Request::Input("YEAR"),
									'custfrnm'	=> $CORP_FRNM->FRNM,
								]
							)->setPaper('a4', 'landscape');

		return $pdf->stream("어종별매입현황.pdf");


	}


	public function getCustGrp(){
		$CUST_GRP = CUST_GRP_INFO::where('CORP_MK', $this->getCorpId())->get();
		return response()->json($CUST_GRP);
	}


	public function getTblTempl(){
		return View('template.table', ['id' => Request::Input("id")]);
	}


	// 판매정보 최대 SEQ 값 조회
	private function getMaxSeqInSALEINFO_M($pWRITE_DATE, $pCUST_MK){

		return $max_seq = SALE_INFO_M::select( DB::raw('ISNULL(max("SEQ"), 1) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("CUST_MK", $pCUST_MK)
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;

	}


	// 재고정보 최대 SEQ 값 조회
	private function getMaxSeqInSTOCKINFO($pSIZES, $pPIS_MK){

		return $max_seq = STOCK_INFO::select( DB::raw('ISNULL(max("SEQ"), 1) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("PIS_MK", $pPIS_MK)
									->where ("SIZES", $pSIZES)
									->first()->SEQ;

	}

	// 미수금 최대 SEQ 값 조회
	private function getMaxSeqInUNCLINFO($pWRITE_DATE){

		return $max_seq = UNCL_INFO::select( DB::raw('ISNULL(max("SEQ"), 1) AS SEQ') )
									->where ("CORP_MK", $this->getCorpId())
									->where ("WRITE_DATE", $pWRITE_DATE)
									->first()->SEQ;

	}
}


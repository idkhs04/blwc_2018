<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;


use App\Http\Requests;

use App\CUST_GRP_INFO;
use App\CUST_CD;
use App\ACCOUNT_GRP;
use App\BRING_AMT_INFO;
use Mockery\CountValidator\Exception;
use Validator;
use DB;
use Storage;
use Lang;
use Response;
use Excel;
use Datatables;
use PDF;
use Watson\Validating\ValidationException;

class BringAmtGrantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function listData()
	{		
		$BRING_AMT_INFO = BRING_AMT_INFO::select(
				'BRING_AMT',
				DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" ),
				DB::raw( "'' AS REMARK")
			)
			->where("CORP_MK", $this->getCorpId());
						
		return Datatables::of($BRING_AMT_INFO)
				->filter(function($query) {
					if( Request::Has('start_date') && Request::Input('start_date') ){
						$query->where(function($q){
							$q->where("WRITE_DATE",  ">=", Request::Input('start_date'));
						});
					}
				}
		)->make(true);
	}

	public function index($bringamtgrant)
	{
		
		return view("bringamtgrant.list",[ "bringamtgrant" => $bringamtgrant ] );
	}

	public function getBringAmt(){
		

		$BRING_AMT_INFO = DB::table(DB::raw("
								(SELECT  CORP_MK
								        ,MAX(WRITE_DATE) AS WRITE_DATE 
								   FROM BRING_AMT_INFO 
								  WHERE CORP_MK		='".$this->getCorpId()."' 
								    AND WRITE_DATE	<= '".date("Y-m-d")."'
								  GROUP BY CORP_MK
								) AS A")
							)->join("BRING_AMT_INFO AS B", function($join){
								$join->on("A.CORP_MK", "=", "B.CORP_MK");
								$join->on("A.WRITE_DATE", "=", "B.WRITE_DATE");
								
							})->select( 
								  DB::raw("CONVERT(CHAR(10), A.WRITE_DATE, 23) AS WRITE_DATE")
								,"B.BRING_AMT"
							)->first();
		
		return response()->json([$BRING_AMT_INFO]);
	
	}


	// 이월하기
	public function setClose(){
		
		return $exception = DB::transaction(function(){
			$validator = Validator::make( Request::Input(), [
				'CLOSE_DATE' => 'required|date'
		
			]);
			
			if ($validator->fails()) {
				$errors = $validator->errors();
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}

			try{
				
				DB::statement(DB::raw("EXEC SP_BRING_AMT_INFO '".$this->getCorpId()."','".Request::Input("CLOSE_DATE")."'"));
				return response()->json(['result' => 'success']);

				/*
				$today		= strtotime(date("Y-m-d"));							// 오늘날짜
				$closeDate	= strtotime(Request::Input("CLOSE_DATE"));			// 마감일자
				$dateAdd1	= date('Y-m-d', strtotime("+1 day", $closeDate));	// 마감일자+1
				
				$diff = (int)ceil(abs($closeDate - $today) / 86400);
				
				// 2016-11-09 Stored Procedure를 이용하여 쿼리를 1-Transaction으로 작업요망 (아래참조)
				// DB::statement(DB::raw("EXEC SP_CLOSING '".$this->getCorpId()."','".Request::Input("CLOSING_DATE")."','".Request::Input("CLOSING_DATE_END")."'"));
				// dd($diff );
				for($i=0; $i<$diff ; $i++){
				
					
					$BRING_INFO	= DB::table("BRING_AMT_INFO")
										->where("CORP_MK", $this->getCorpId())
										->where("WRITE_DATE", Request::Input("CLOSE_DATE"))
										->select( DB::raw("ISNULL(BRING_AMT, 0) AS BRING_AMT"))
										->first();
					$BRING_INFO	= $BRING_INFO == null ? 0 :(int)$BRING_INFO->BRING_AMT;
					
					
					$BSUM_AMT1	= DB::table("CHIT_INFO")
										->where("CORP_MK", $this->getCorpId())
										->where("WRITE_DATE", $dateAdd1)
										->where("DE_CR_DIV", "1")
										->select( DB::raw("SUM(AMT) AS SUM"))
										->first();
					$BSUM_AMT1	= $BSUM_AMT1 == null ? 0 : (int)$BSUM_AMT1->SUM;


					$BSUM_AMT2	= DB::table("CHIT_INFO")
										->where("CORP_MK", $this->getCorpId())
										->where("WRITE_DATE", $dateAdd1)
										->where("DE_CR_DIV", "0")
										->select( DB::raw("SUM(AMT) AS SUM"))
										->first();
					$BSUM_AMT2	= $BSUM_AMT2 == null ? 0 : (int)$BSUM_AMT2->SUM;

					$BRING_AMT	= $BRING_INFO + $BSUM_AMT1 - $BSUM_AMT2;
					
					DB::table("BRING_AMT_INFO")
						->where("CORP_MK", $this->getCorpId())
						->where("WRITE_DATE", $dateAdd1)
						->delete();
					
					DB::table("BRING_AMT_INFO")
						->where("CORP_MK", $this->getCorpId())
						->where("WRITE_DATE", $dateAdd1)
						->insert(
							[
								 "CORP_MK"		=> $this->getCorpId()
								,"WRITE_DATE"	=> $dateAdd1
								,"BRING_AMT"	=> $BRING_AMT

							]
						);	
				}
				*/

				return response()->json(['result' => 'success']);

			} catch(ValidationException $e){
				$errors = $e->errors();
				
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			}catch(Exception $e){
				$errors = $e->errors();
				
				$errors =  json_decode($errors); 
				return response()->json(['result' => 'fail', 'message' => $errors], 422);
			
			}
		});

	
	}

	//이월금 정보 등록
    public function insert($bringamtgrant)
    {

        $validator = Validator::make( Request::Input(), [
            'BRING_AMT' => 'required|numeric',
            'WRITE_DATE' => 'required|date'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}

		// 동일한 키가 있는 경우 실패메시지
		$count = DB::table("BRING_AMT_INFO")
			->where('CORP_MK', $this->getCorpId())
			->where('WRITE_DATE',  Request::Input("WRITE_DATE"))
			->count();

		if( $count > 0){
			return response()->json(['result' => 'fail', 'reason' => 'PrimaryKey'], 422);
		}

        DB::beginTransaction();
        try {
            //dd($request);
            DB::table("BRING_AMT_INFO")->insert(
                [
                    'CORP_MK' => $this->getCorpId(),
                    'BRING_AMT' => Request::input('BRING_AMT'),
                    'WRITE_DATE' => Request::input('WRITE_DATE')
                ]
            );
        }
        catch(ValidationException $e){
            DB::rollback();

            return Redirect::to("/bringamtgrant/" . $bringamtgrant)
                    ->withErrors($e->getErrors())
                    ->withInput();

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }


	//선택된 이월금 정보
	public function edit($bringamtgrant, $id)
	{	
		$model = DB::table("BRING_AMT_INFO")
			->select(
				DB::raw( "CONVERT(CHAR(10), WRITE_DATE, 23) AS WRITE_DATE" ),
				'BRING_AMT'
			)
			->where("CORP_MK", $this->getCorpId())
            ->where("WRITE_DATE", $id)
			->first();

		return response()->json(
			[
				'BRING_AMT' => $model->BRING_AMT, 
				'WRITE_DATE' => $model->WRITE_DATE
			]);
	}

	//선택된 이월금 정보 수정
    public function update($bringamtgrant)
    {
        $validator = Validator::make( Request::Input(), [
            'BRING_AMT' => 'required'
        ]);

        if ($validator->fails()) {
			$errors = $validator->errors();
			$errors =  json_decode($errors); 
			return response()->json(['result' => 'fail', 'message' => $errors], 422);
		}
		

        DB::beginTransaction();
        try {
            DB::table("BRING_AMT_INFO")
				->where("CORP_MK", $this->getCorpId())
                ->where("WRITE_DATE", Request::input('WRITE_DATE'))
                ->update(
					[
						"BRING_AMT" => Request::input('BRING_AMT')
					]
				);

        }catch(Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();
		return response()->json(['result' => 'success']);
    }

	//선택된 비용계정그룹 정보 삭제
    public function _delete()
    {
        if (Request::ajax()) {
            DB::table("BRING_AMT_INFO")
				->where("CORP_MK", $this->getCorpId())
                ->whereIn("WRITE_DATE", Request::get('cd'))
                ->delete();

            return Response::json(['result' => 'success', 'code' => Request::get('cd')]);
        }
        return Response::json(['result' => 'failed']);
    }
/*
    public function Excel()
    {
        // 컬럼을 지정해줘야 에러가 안남
        $model = CUST_GRP_INFO::select('CUST_GRP_CD', 'CUST_GRP_NM', 'CORP_MK')
            ->where("CORP_MK", $this->getCorpId())
            ->get();

        Excel::create('거래처그룹', function ($excel) use ($model) {
            $excel->sheet('거래처그룹', function ($sheet) use ($model) {
                $sheet->fromArray($model);
            });
        })->export('xls');
    }

    public function Pdf()
    {

        $pdf = PDF::loadHTML("<h1>Test</h1>");

        // $pdf->save('myfile.pdf');
        return $pdf->stream();

    }
*/
}
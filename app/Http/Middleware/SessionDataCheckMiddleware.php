<?php
 
namespace App\Http\Middleware;
 
use Closure;
use Auth;
use Session;
 
class SessionDataCheckMiddleware {
 
	/**
	 * Check session data, if role is not valid logout the request
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		
		if(!Auth::User()){
			// if session is expired
			return response()->json(['message' => 'unauth'], 403);
		}

		return $next($request);

	}
 
}

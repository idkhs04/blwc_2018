<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('/root/init', 'EntrustInitController@initRole');

Route::group(['middleware'=>['web', 'entrust-gui.admin']], function(){
	$id = 0;

	route('entrust-gui::users.index');
	route('entrust-gui::users.create');
	route('entrust-gui::users.destroy', $id);
	route('entrust-gui::users.update', $id);
	route('entrust-gui::users.edit', $id);

	route('entrust-gui::roles.index');
	route('entrust-gui::roles.create');
	route('entrust-gui::roles.destroy', $id);
	route('entrust-gui::roles.update', $id);
	route('entrust-gui::roles.edit', $id);

	route('entrust-gui::permissions.index');
	route('entrust-gui::permissions.create');
	route('entrust-gui::permissions.destroy', $id);
	route('entrust-gui::permissions.update', $id);
	route('entrust-gui::permissions.edit', $id);

});

Route::auth();


//업체정보
Route::group(['prefix'=>'corp', 'middleware' => ['web'] ],function(){

	//업체정보 리스트
	$this->get('/{corp}/listData','CorpInfoController@listData');

	//업체정보 인덱스페이지
	$this->match(['get','post'],'/{corp}/index{page?}','CorpInfoController@index');

	//업체정보 상세페이지
	$this->match(['get','post'],'/{corp}/detail/{corp_mk}','CorpInfoController@detail');

	//업체정보 업데이트
	$this->match(['get','post'], '/{corp}/update/{corp_mk}','CorpInfoController@update');

	//업체정보 등록
	$this->match(['get','post'], '/{corp}/insert','CorpInfoController@insert');

	// 업체 사용여부
	$this->match(['get','post'], '/{corp}/setCorpActive','CorpInfoController@setCorpActive');

	// 전자세금계산서 사용여부
	$this->match(['get','post'], '/{corp}/setCorpTax','CorpInfoController@setCorpTax');
	

	// 회사 기본정보 복사(거래처그룹,거래처,비용계정그룹,비용계정,비용전표,보조부,계정그룹,규격, 회사정보 + 사용자 1명)
	$this->match(['get','post'], '/{corp}/setCorpCopy','CorpInfoController@setCorpCopy');

	$this->get('/{corp}/getCorpInfoJSON', 'CorpInfoController@getCorpInfoJSON');

	$this->post('/{corp}/setBackup', 'CorpInfoController@setBackup');

	$this->post('/{corp}/chkCorpBackup', 'CorpInfoController@chkCorpBackup');

	$this->match(['get','post'], '/{corp}/backIdex/{CORP_MK}', 'CorpInfoController@backIndex');
	$this->get('/{corp}/getBackList', 'CorpInfoController@getBackList');

	

});


Route::group(['prefix'=>'member', 'middleware' => ['web']],function(){
	//사원정보 리스트
	$this->get('/{member}/listData','MemberController@listData');

	//사원정보 인덱스페이지
	$this->match(['get','post'],'/{member}/index{page?}','MemberController@index');

	//사원정보 상세페이지
	$this->match(['get','post'],'/{member}/detail/{corp_mk}','MemberController@detail');

	//사원정보 업데이트
	$this->match(['get','post'], '/{member}/update/{corp_mk}','MemberController@update');

	//사원정보->사원등록
	$this->match(['get','post'], '/{member}/insert/{corp_mk}','MemberController@insert');

	//사원정보->사원등록
	$this->match(['get','post'], '/{member}/_delete','MemberController@_delete');

	//사원정보->급여정보
	$this->match(['get','post'], '/{member}/salary/{MEM_ID}','MemberController@salary');
	$this->match(['get','post'], '/{member}/listSalary','MemberController@listSalary');


	// 급여계산 서비스
	$this->match(['get','post'], '/{member}/getTax','MemberController@getTax');

	// 급여저장
	$this->match(['get','post'], '/{member}/setSalary','MemberController@setSalary');

	// 급여삭제
	$this->match(['get','post'], '/{member}/deleteSalary','MemberController@deleteSalary');

	// 급여조회
	$this->match(['get','post'], '/{member}/getSalary','MemberController@getSalary');
	$this->match(['get','post'], '/{member}/detailSalary','MemberController@detailSalary');

	// 급여수정
	$this->match(['get','post'], '/{member}/updateSalary','MemberController@updateSalary');


	//
	//로그인
	Route::get('login', 'Auth\AuthController@getLogin');
	Route::post('login', 'Auth\AuthController@postLogin');

	//로그아웃
	Route::get('logout', 'Auth\AuthController@logout');
});

//그룹정보
Route::group(['prefix'=>'group', 'middleware' => ['web']],function(){

	//그룹정보 리스트
	$this->get('/{corp}/listData','GroupController@listData');

	//그룹정보 인덱스페이지
	$this->match(['get','post'],'/{corp}/index{page?}','GroupController@index');

	//그룹정보 등록
	$this->match(['get','post'], '/{corp}/insert','GroupController@insert');

	//그룹정보 수정
	$this->get('/{cust}/Edit/{id}/{page?}','GroupController@edit');

	//그룹정보 업데이트
	$this->match(['get','post'], '/{cust}/Update','GroupController@update');

	//그룹정보 삭제
	$this->match(['get','post'], '/{cust}/Delete','GroupController@_delete');

});


Route::group(['prefix'=>'pis', 'middleware' => ['web']],function(){
	$this->match(['get','post'],'/{pis}/index{page?}','PisInfoController@index');
	$this->match(['get','post'],'/{pis}/detail/{PIS_MK}','PisInfoController@detail');
	$this->match(['get','post'],'/{pis}/getDetailData','PisInfoController@getDetailData');
	$this->match(['get','post'],'/{pis}/updpisData','PisInfoController@updpisData');
	$this->match(['get','post'],'/{pis}/_delete','PisInfoController@_delete');


	$this->get('/{pis}/listData','PisInfoController@listData');
	$this->get('/{pis}/detailList','PisInfoController@detailList');
	$this->get('/{pis}/detailListData','PisInfoController@detailListData');
	$this->match(['get','post'],'/{pis}/setpisData','PisInfoController@setpisData');

	$this->get('/{pis}/chkPIS_MK','PisInfoController@chkPIS_MK');
});

// 어종 코드관리
Route::group(['prefix'=>'piscls', 'middleware' => ['web']],function(){

	$this->match(['get','post'],'/{piscls}/index{page?}','PisClassController@index');
	$this->match(['get','post'],'/{piscls}/detail/{PIS_MK}','PisClassController@detail');

	$this->match(['get','post'],'/{piscls}/delete/{PIS_MK}','PisClassController@_delete');
	$this->match(['get','post'],'/{piscls}/_delete','PisClassController@_delete');

	$this->get('/{piscls}/listData','PisClassController@listData');
	$this->get('/{piscls}/getDataDetail','PisClassController@getDataDetail');
	$this->get('/{piscls}/detailList','PisClassController@detailList');
	$this->get('/{piscls}/detailListData','PisClassController@detailListData');

	$this->match(['get','post'],'/{piscls}/setpisClsData','PisClassController@setpisClsData');
	$this->match(['get','post'],'/{piscls}/updpisClsData','PisClassController@updpisClsData');


	$this->get('/{piscls}/chkPIS_MK','PisClassController@chkPIS_MK');
});

// 일일판매
Route::group(['prefix'=>'sale', 'middleware' => ['web']],function(){

	$this->match(['get','post'],'/{sale}/Pdf/{START_DATE}/{END_DATE}','SaleInfoMController@pdf');
	$this->match(['get','post'],'/{sale}/Pdf/{START_DATE}/{END_DATE}/{SRT_CONDITION}','SaleInfoMController@pdf');
	$this->match(['get','post'],'/{sale}/Pdf/{START_DATE}/{END_DATE}/{SRT_CONDITION}/{SEARCH}','SaleInfoMController@pdf');

	$this->match(['get','post'], '/{sale}/PdfDetail/{CUST_MK}/{SEQ}/{WRITE_DATE}','SaleInfoMController@PdfDetail');
	$this->match(['get','post'], '/{sale}/PdfDetailSelected/{CUST_MK}/{SEQ}/{WRITE_DATE}/{END_DATE}','SaleInfoMController@PdfDetailSelected');

	$this->get('/{sale}/Excel','SaleInfoMController@excel');

	$this->match(['get','post'],'/{sale}/index{page?}','SaleInfoMController@index');
	$this->get('/{sale}/listData','SaleInfoMController@listData');

	$this->match(['get','post'], '/{sale}/log','SaleInfoMController@log');
	$this->match(['get','post'], '/{sale}/listLogM','SaleInfoMController@listLogM');
	$this->match(['get','post'], '/{sale}/listLogD1','SaleInfoMController@listLogD1');
	$this->match(['get','post'], '/{sale}/listLogD2','SaleInfoMController@listLogD2');

	$this->get('/{sale}/getTotal','SaleInfoMController@getTotal');
	$this->get('/{sale}/detailList/{CUST_MK}/{SEQ}/{WRITE_DATE}','SaleInfoMController@detailList');
	$this->get('/{sale}/getUpdateSaleList','SaleInfoMController@getUpdateSaleList');
	$this->get('/{sale}/detailData','SaleInfoMController@detailData');
	$this->get('/{sale}/getCustList','SaleInfoMController@getCustList');
	$this->get('/{sale}/getCustGrp','SaleInfoMController@getCustGrp');
	$this->get('/{sale}/getTblTempl{id?}','SaleInfoMController@getTblTempl');
	$this->get('/{sale}/getTotalUncl','SaleInfoMController@getTotalUncl');
	$this->get('/{sale}/setSaleInfoInsert','SaleInfoMController@setSaleInfoInsert');

	$this->get('/{sale}/detailForm/{CUST_MK}/{SEQ}/{WRITE_DATE}','SaleInfoMController@detailForm');
	$this->get('/{sale}/detailReturn/{CUST_MK}/{SEQ}/{WRITE_DATE}','SaleInfoMController@detailReturn');
	$this->get('/{sale}/detailDelete/{CUST_MK}/{SEQ}/{WRITE_DATE}','SaleInfoMController@detailDelete');


	$this->get('/{sale}/getUpdateList','SaleInfoMController@getUpdateList');
	$this->get('/{sale}/getSujoNo','SaleInfoMController@getSujoNo');
	$this->get('/{sale}/getViewSaleSum','SaleInfoMController@getViewSaleSum');
	$this->get('/{sale}/setUpdateViewSaleSum','SaleInfoMController@setUpdateViewSaleSum');

	$this->match(['get','post'], '/{sale}/setUpdateSaleDetail','SaleInfoMController@setUpdateSaleDetail');
	$this->post('/{sale}/setReturnSaleDetail','SaleInfoMController@setUpdateSaleDetail');
	$this->match(['get','post'], '/{sale}/setDeleteSaleDetail','SaleInfoMController@setDeleteSaleDetail');




});


// 계산서 등록
Route::group(['prefix'=>'tax', 'middleware' => ['web']],function(){


	// 년단위 계산서 관리 작업
	$this->get('/{tax}/listTaxCust','TaxAccountController@listTaxCust');
	$this->get('/{tax}/listTaxCustData','TaxAccountController@listTaxCustData');
	$this->get('/{tax}/detailTaxCust/{CUST_MK}','TaxAccountController@detailTaxCust');
	$this->get('/{tax}/getCustSaleDetailData','TaxAccountController@getCustSaleDetailData');
	

    //$this->get('/{cust}/Pdf','CustCdController@pdf');
    //$this->get('/{cust}/Excel','CustCdController@excel');

	$this->get('/{tax}/indexBuy{page?}','TaxAccountController@indexBuy');
	$this->get('/{tax}/detail/{CUST_MK}','TaxAccountController@detailBuy');

	$this->get('/{tax}/getTaxAccountList','TaxAccountController@getTaxAccountList');
	$this->get('/{tax}/getTaxBuyGeneration','TaxAccountController@getTaxBuyGeneration');
	$this->get('/{tax}/setTaxBuy','TaxAccountController@setTaxBuy');

	$this->get('/{tax}/indexSale{page?}','TaxAccountController@indexSale');
	$this->get('/{tax}/getIndexSaleCust','TaxAccountController@getIndexSaleCust');
	$this->get('/{tax}/detailSale/{CUST_MK}','TaxAccountController@detailSale');
	$this->get('/{tax}/getTaxSaleAccountList','TaxAccountController@getTaxSaleAccountList');
	$this->get('/{tax}/setTaxSale','TaxAccountController@setTaxSale');
	$this->get('/{tax}/getTaxSaleGeneration','TaxAccountController@getTaxSaleGeneration');


	$this->get('/{tax}/uptTaxSale','TaxAccountController@uptTaxSale');
	$this->get('/{tax}/deleteTaxSale','TaxAccountController@deleteTaxSale');

	// 매입계산서 수정 불러오기
	$this->get('/{tax}/getTaxBuyItem','TaxAccountController@getTaxBuyItem');


	// 매출계산서 수정 불러오기
	$this->get('/{tax}/getTaxSaleItem','TaxAccountController@getTaxSaleItem');

	// 매출세금계산서 출력 PDF
	$this->match(['get','post'],'/{tax}/getPdfSaleTax/{WRITE_DATE}/{SEQ}/{CUST_MK}/{WHITE}','TaxAccountController@getPdfSaleTax');
	
	// 매출세금계산서 전체 출력(기간) PDF
	$this->match(['get','post'],'/{tax}/getPdfSaleTaxAll/{WRITE_DATE_START}/{WRITE_DATE_END}/{CUST_MK}','TaxAccountController@getPdfSaleTaxAll');
	$this->match(['get','post'],'/{tax}/getPdfSaleTaxAll/{WRITE_DATE_START}/{WRITE_DATE_END}/{CUST_MK}/{WRITE_DT}/{CODE}','TaxAccountController@getPdfSaleTaxAll');

	$this->match(['get','post'],'/{tax}/getPdfSaleTaxAll/{WRITE_DATE_START}/{WRITE_DATE_END}/{CUST_MK}?W={WHITE}','TaxAccountController@getPdfSaleTaxAll');
	$this->match(['get','post'],'/{tax}/getPdfSaleTaxAll/{WRITE_DATE_START}/{WRITE_DATE_END}/{CUST_MK}/{WRITE_DT}/{CODE}?W={WHITE}','TaxAccountController@getPdfSaleTaxAll');
	

	// 매출세금계산서 국세청 엑셀출력
	$this->get('/{tax}/ExcelTaxBill/{WRITE_DATE}/{SEQ}/{CUST_MK}','TaxAccountController@getExcelTaxBill');
	$this->match(['get','post'], '/{tax}/ExcelTaxBillDownload','TaxAccountController@ExcelTaxBillDownload');

	$this->match(['get','post'], '/{tax}/getPdfeTax/{START_DATE}/{END_DATE}/{CUST_GRP}/{CHKST_SA}/{STTUS}/{SEARCH}','TaxAccountController@getPdfeTax');
	$this->match(['get','post'], '/{tax}/getPdfeTax/{START_DATE}/{END_DATE}/{CUST_GRP}/{CHKST_SA}/{STTUS}','TaxAccountController@getPdfeTax');

	// 매입/매출계산서 합계
	// 거래업체별 합계
	$this->get('/{tax}/list','TaxAccountController@index');
	$this->get('/{tax}/getTaxCustSumList','TaxAccountController@getTaxCustSumList');

	// 매출별 합계
	$this->get('/{tax}/getTaxSaleSumList','TaxAccountController@getTaxSaleSumList');

	// 매입별 합계
	$this->get('/{tax}/getTaxInputSumList','TaxAccountController@getTaxInputSumList');

	// 연간합계
	$this->get('/{tax}/getTaxYearSumList','TaxAccountController@getTaxYearSumList');

	// 매출계산서 합계 출력
	$this->match(['get','post'],'/{tax}/PdfsaleSum/{START_DATE}/{END_DATE}/{CUST_GRP}','TaxAccountController@PdfsaleSum');

	// 매입계산서 합계 출력
	$this->match(['get','post'],'/{tax}/PdfinputSum/{START_DATE}/{END_DATE}/{CUST_GRP}','TaxAccountController@PdfbuySum');

	// 연간합계 출력(계산서 촐괄표)
	$this->match(['get','post'],'/{tax}/PdfyearSum/{START_DATE}/{END_DATE}','TaxAccountController@PdfyearSum');

	// 연간비율설정
	$this->get("/{tax}/setChangeDeclaration", 'TaxAccountController@setChangeDeclaration');

	// 국세청 세금계산서 조회화면
	$this->get("/{tax}/elist", 'TaxAccountController@elist');
	$this->get("/{tax}/elist/{CUST_MK}/{WRITE_DATE}/{AMT}/{SALE_BUY}", 'TaxAccountController@elist');
	$this->get("/{tax}/elist/{CUST_MK}/{SALE_BUY}", 'TaxAccountController@elist');
	
	// 국세청 세금계산서 데이터 조회/상세조회
	$this->get("/{tax}/elistData", 'TaxAccountController@elistData');
	$this->get("/{tax}/eDetailData", 'TaxAccountController@eDetailData');

	// 국세청 세금계산서 저장
	$this->post("/{tax}/eUpdateData", 'TaxAccountController@eUpdateData');

	// 국세청 세금계산서 정정 (2017/03/14)
	$this->get("/{tax}/eSetCopyNMod", 'TaxAccountController@eSetCopyNMod');

	// 국세청 세금계산서 송신
	$this->match(['get','post'],"/{tax}/eTaxSend", 'TaxAccountController@eTaxSend');

	// 국세청 세금계산서 수신
	$this->match(['get','post'],"/{tax}/eTaxReceive", 'TaxAccountController@eTaxReceive');
	
	
	// 세금계산서 통합 입력저장
	$this->get("/{tax}/setTaxIntergration", 'TaxAccountController@setTaxIntergration');
	// 세금계산서 통합 수정저장
	$this->get("/{tax}/updTaxIntergration", 'TaxAccountController@updTaxIntergration');
	
	
	
});

// 결산관리
Route::group(['prefix'=>'closing', 'middleware' => ['web']],function(){

	$this->match(['get','post'],'/{sale}/index{page?}','ClosingController@index');
	$this->get('/{closing}/listData','ClosingController@listData');
	$this->get('/{closing}/setClosing','ClosingController@setClosing');

	$this->get('/{closing}/detail/{START_DATE}/{END_DATE}','ClosingController@detail');

	//$this->match(['get','post'],'/{closing}/Pdf/{START_DATE}/{END_DATE}','ClosingController@pdf');
	$this->get('/{closing}/Pdf/{START_DATE}/{END_DATE}','ClosingController@pdf');
	$this->get('/{closing}/PdfArrange/{START_DATE}/{END_DATE}','ClosingController@PdfArrange');
	$this->get('/{closing}/detailList','ClosingController@detailList');

	$this->get('/{closing}/detailListArrange/{START_DATE}/{END_DATE}','ClosingController@detailListArrange');
	//$this->match(['get','post'],'/{closing}/listDetailArrange','ClosingController@listDetailArrange');


	$this->get('/{closing}/getClosingUnclList','ClosingController@getClosingUnclList');
	$this->get('/{closing}/getClosingUnprovList','ClosingController@getClosingUnprovList');



});

// 미수금관리
Route::group(['prefix'=>'uncl', 'middleware' => ['web']],function(){
	$this->match(['get','post'],'/{uncl}/index{page?}','UnclInfoController@index');
	$this->match(['get','post'],'/{uncl}/detail/{CUST_MK}/','UnclInfoController@detail');
	$this->match(['get','post'],'/{uncl}/detail2/{CUST_MK}','UnclInfoController@detail2');
	$this->match(['get','post'],'/{uncl}/detail2/{CUST_MK}/{MODE}','UnclInfoController@detail2');

	$this->match(['get','post'],'/{uncl}/detailListData','UnclInfoController@detailListData');
	$this->match(['get','post'],'/{uncl}/detailListData2','UnclInfoController@detailListData2');

	$this->match(['get','post'],'/{uncl}/delete','UnclInfoController@_delete');

	$this->get('/{uncl}/listData','UnclInfoController@listData');
	$this->get('/{uncl}/setUnclData','UnclInfoController@setUnclData');

	$this->match(['get','post'],'/{uncl}/pdfEmpty','UnclInfoController@pdfEmpty');
	$this->match(['get','post'],'/{uncl}/pdfEmpty/{CUST_GRP_CD}/{textSearch}','UnclInfoController@pdfEmpty');
	$this->match(['get','post'],'/{uncl}/pdfEmpty/{CUST_GRP_CD}','UnclInfoController@pdfEmpty');

	$this->match(['get','post'],'/{uncl}/pdfDetail','UnclInfoController@pdfDetail');
	$this->match(['get','post'],'/{uncl}/pdfDetail/{CUST_GRP_CD}/{textSearch}','UnclInfoController@pdfDetail');
	$this->match(['get','post'],'/{uncl}/pdfDetail/{CUST_GRP_CD}','UnclInfoController@pdfDetail');

	//$this->match(['get','post'],'/{uncl}/pdfDetailCust','UnclInfoController@pdfDetailCust');
	$this->get('/{uncl}/pdfDetailCust/{CUST_MK}/{start_date?}{end_date?}','UnclInfoController@pdfDetailCust');
	$this->get('/{uncl}/pdfDetailCustTest/{CUST_MK}/{start_date?}{end_date?}','UnclInfoController@pdfDetailCustTest');

});


// 미지급금관리
Route::group(['prefix'=>'unprov', 'middleware' => ['web']],function(){
	$this->match(['get','post'],'/{unprov}/index{page?}','UnprovInfoController@index');
	$this->match(['get','post'],'/{unprov}/detail/{CUST_MK}/','UnprovInfoController@detail');
	$this->match(['get','post'],'/{unprov}/detail2/{CUST_MK}/','UnprovInfoController@detail2');
	$this->match(['get','post'],'/{unprov}/detail2/{CUST_MK}/{MODE}','UnprovInfoController@detail2');

	$this->match(['get','post'],'/{unprov}/detailListData','UnprovInfoController@detailListData');
	$this->match(['get','post'],'/{unprov}/detailListData2','UnprovInfoController@detailListData2');

	$this->match(['get','post'],'/{unprov}/delete','UnprovInfoController@_delete');

	$this->match(['get','post'],'/{unprov}/listLog','UnprovInfoController@listLog');
	$this->match(['get','post'],'/{unprov}/log','UnprovInfoController@log');

	$this->match(['get','post'],'/{unprov}/getDataDetail','UnprovInfoController@getDataDetail');


	$this->get('/{unprov}/getTotalUnprov','UnprovInfoController@getTotalUnprov');

	$this->get('/{unprov}/listData','UnprovInfoController@listData');
	$this->get('/{unprov}/setUnprovData','UnprovInfoController@setUnprovData');
	$this->get('/{unprov}/updUnprovData','UnprovInfoController@updUnprovData');

	$this->get('/{unprov}/PdfDetail/{start_date}/{end_date}/{PROV_CUST_MK}','UnprovInfoController@PdfDetail');
	
	$this->match(['get','post'],'/{unprov}/pdfEmpty','UnprovInfoController@pdfEmpty');
	$this->match(['get','post'],'/{unprov}/pdfEmpty/{CUST_GRP_CD}/{textSearch}','UnprovInfoController@pdfEmpty');
	$this->match(['get','post'],'/{unprov}/pdfEmpty/{CUST_GRP_CD}','UnprovInfoController@pdfEmpty');

	$this->match(['get','post'],'/{unprov}/pdfListDetail','UnprovInfoController@pdfListDetail');
	$this->match(['get','post'],'/{unprov}/pdfListDetail/{CUST_GRP_CD}/{textSearch}','UnprovInfoController@pdfListDetail');
	$this->match(['get','post'],'/{unprov}/pdfListDetail/{CUST_GRP_CD}','UnprovInfoController@pdfListDetail');


});

// 기간별 판매, 통계관리
Route::group(['prefix'=>'stcs', 'middleware' => ['web']],function(){

	// 업체별 판매현황
	$this->match(['get','post'],'/{stcs}/listCustSale','StcsController@listCustSale');
	$this->get('/{stcs}/listCustSaleData','StcsController@listCustSaleData');
	// 업체별 판매현황 목록
	$this->match(['get','post'],'/{stcs}/listCustSaleDetail/{CUST_MK}','StcsController@listCustSaleDetail');
	$this->get('/{stcs}/getDataCustSale','StcsController@getDataCustSale');
	$this->get('/{stcs}/getDataCustSaleTotal','StcsController@getDataCustSaleTotal');

	$this->match(['get','post'],'/{stcs}/Pdf/{START_DATE}/{END_DATE}','StcsController@Pdf');

	// ============================== 통계 =============================
	// 업체별 매입현황
	$this->match(['get','post'],'/{stcs}/listCustInput','StcsController@listCustInput');
	$this->get('/{stcs}/listCustSaleData','StcsController@listCustSaleData');
	// 업체별 매입현황 목록
	$this->match(['get','post'],'/{stcs}/listCustInputDetail/{CUST_MK}','StcsController@listCustInputDetail');
	$this->get('/{stcs}/getDataCustInput','StcsController@getDataCustInput');
	$this->get('/{stcs}/getDataCustInputTotal','StcsController@getDataCustInputTotal');


	// 어종별 판매현황
	$this->match(['get','post'],'/{stcs}/listPisSale','StcsController@listPisSale');
	$this->match(['get','post'],'/{stcs}/getDataPisSale','StcsController@getDataPisSale');
	$this->match(['get','post'],'/{stcs}/getPisSaleTotal','StcsController@getPisSaleTotal');

	// 어종별 매입현황
	$this->match(['get','post'],'/{stcs}/listPisInput','StcsController@listPisInput');
	$this->match(['get','post'],'/{stcs}/getDataPisInput','StcsController@getDataPisInput');
	$this->match(['get','post'],'/{stcs}/getPisInputTotal','StcsController@getPisInputTotal');


	$this->match(['get','post'],'/{stsc}/index{page?}','StcsController@index');
	$this->get('/{stcs}/listData','StcsController@listData');

	$this->get('/{stcs}/PdflistCustSaleDetail/{CUST_MK?}{YEAR?}{SIZES?}{PIS_MK?}','StcsController@PdflistCustSaleDetail');

	$this->get('/{stcs}/PdflistPisSale/{YEAR?}{SIZES?}{PIS_MK?}','StcsController@PdflistPisSale');

	$this->get('/{stcs}/PdflistCustInput/{CUST_MK?}{YEAR?}{SIZES?}{PIS_MK?}','StcsController@PdflistCustInput');

	$this->get('/{stcs}/PdflistPisInput/{YEAR?}{SIZES?}{PIS_MK?}','StcsController@PdflistPisInput');



});

// 매입관리 수조관리
Route::group(['prefix'=>'buy', 'middleware' => ['web']],function(){
	$this->get('/{buy}/Pdf','StockSujoController@pdf');
    $this->get('/{buy}/Excel','StockSujoController@excel');

	$this->get('/{buy}/chkHasSujo', 'StockSujoController@chkHasSujo');
	$this->get('/{buy}/View/{id}/{page?}','StockSujoController@view');
    $this->get('/{buy}/Edit/{id}/{page?}','StockSujoController@edit');
    $this->match(['get','post'], '/{buy}/Insert','StockSujoController@insert');
    $this->match(['get','post'], '/{buy}/Update','StockSujoController@update');
    $this->match(['get','post'], '/{buy}/Delete','StockSujoController@_delete');
    $this->match(['get','post'], '/{buy}/Create','StockSujoController@create');

	$this->match(['get','post'], '/{buy}/log','StockSujoController@log');
	$this->match(['get','post'], '/{buy}/listLog','StockSujoController@listLog');
	$this->match(['get','post'], '/{buy}/listLog1','StockSujoController@listLog1');

    $this->match(['get','post'], '/{buy}/validate','StockSujoController@validatemodel');

	$this->match(['get','post'],'/{buy}/index{page?}','StockSujoController@index');

	$this->get('/{buy}/listData','StockSujoController@listData');
	$this->get('/{buy}/SujoData','StockSujoController@getSujoData');
	$this->match(['get','post'], '/{buy}/setDataUNCN','StockSujoController@setSujoDataUNCN');
	$this->match(['get','post'], '/{buy}/addDataSujo','StockSujoController@setSujoInsert');
	$this->match(['get','post'], '/{buy}/setSujoDataIn','StockSujoController@setSujoDataIn');
	$this->match(['get','post'], '/{buy}/getSujoEmpty','StockSujoController@getSujoEmpty');
	$this->match(['get','post'], '/{buy}/getSujoSearch','StockSujoController@getSujoSearch');
	$this->match(['get','post'], '/{buy}/setMoveSujo','StockSujoController@setMoveSujo');

	$this->match(['get','post'], '/{buy}/getOrginCD','StockSujoController@getOrginCD');
	$this->match(['get','post'], '/{buy}/getPisCD','StockSujoController@getPisCD');
	$this->match(['get','post'], '/{buy}/getPisCust','StockSujoController@getPisCust');

	$this->get('/{buy}/setSujoInsert_Info','StockSujoController@setSujoInsert_Info');
	$this->get('/{buy}/getSujoNewTemp','StockSujoController@getSujoNewTemp');
	
	$this->get('/{buy}/setCheckYN','StockSujoController@setCheckYN');
	
});


// 재고관리
Route::group(['prefix'=>'stock', 'middleware' => ['web']],function(){

	$this->match(['get','post'], '/{stock}/Pdf/{PIS_MK}/{SIZES}/{INPUT_DATE}/{OUTPUT_DATE}','StockInfoController@pdf');
	$this->match(['get','post'], '/{stock}/Pdf/{PIS_MK}/{SIZES}/{INPUT_DATE}/{OUTPUT_DATE}/{SRT}','StockInfoController@pdf');
	$this->match(['get','post'], '/{stock}/Pdf/{PIS_MK}/{SIZES}/{INPUT_DATE}/{OUTPUT_DATE}/{SRT}/{SEARCH}','StockInfoController@pdf');
	$this->match(['get','post'], '/{stock}/Pdf/{PIS_MK}/{SIZES}/{INPUT_DATE}/{OUTPUT_DATE}/{SRT}/{SEARCH}/{INOUT}','StockInfoController@pdf');

    $this->post('/{stock}/Excel','StockInfoController@excel');

	$this->match(['get','post'],'/{stock}/index{page?}','StockInfoController@index');
	
	$this->match(['get','post'],'/{stock}/detail/{PIS_MK}/{SIZES}','StockInfoController@detail');
	
	$this->get('/{stock}/listDataPisData','StockInfoController@listDataPisData');
	$this->match(['get','post'],'/{stock}/listPis{page?}','StockInfoController@listPis');
	$this->match(['get','post'],'/{stock}/listAllData{page?}','StockInfoController@listAllData');

	$this->match(['get','post'],'/{stock}/setUpdateStock','StockInfoController@setUpdateStock');
	$this->get('/{stock}/listData','StockInfoController@listData');
	$this->get('/{stock}/detailData','StockInfoController@detailData');
	$this->get('/{stock}/dataInputModList','StockInfoController@dataInputModList');
	$this->get('/{stock}/detailEdit/{PIS_MK}/{SIZES}/{SEQ}/{SUJO_NO}','StockInfoController@detailEdit');
	$this->get('/{stock}/detailEditArr/{PIS_MK}/{SIZES}/{SEQ}/{SUJO_NO}','StockInfoController@detailEditArr');

	$this->get('/{stock}/setSujoOut','StockInfoController@setSujoOut');
	$this->get('/{stock}/getQtySum','StockInfoController@getQtySum');
	$this->get('/{stock}/removeStockInSujo','StockInfoController@removeStockInSujo');
	$this->get('/{stock}/getStockDetailUpdateList','StockInfoController@getStockDetailUpdateList');

	$this->match(['get','post'],'/{stock}/listLog','StockInfoController@listLog');
	$this->match(['get','post'],'/{stock}/log/{PIS_MK}/{SIZE}','StockInfoController@log');

	$this->match(['get','post'],'/{stock}/detailLog','StockInfoController@detailLog');

	$this->get('/{stock}/chkHasSujo', 'StockInfoController@chkHasSujo');
	
	$this->get('/{stock}/getSaleListForInput', 'StockInfoController@getSaleListForInput');
	
});

// 거래처 그룹
Route::group(['prefix'=>'cust', 'middleware' => ['web']],function(){

	$this->get('/{cust}/listData','CustGrpInfoController@listData');

    $this->get('/{cust}/View/{id}/{page?}','CustGrpInfoController@view');
    $this->get('/{cust}/Edit/{id}/{page?}','CustGrpInfoController@edit');
    $this->match(['get','post'], '/{cust}/Insert','CustGrpInfoController@insert');
    $this->match(['get','post'], '/{cust}/Update','CustGrpInfoController@update');
    $this->match(['get','post'], '/{cust}/Delete','CustGrpInfoController@_delete');
    $this->match(['get','post'], '/{cust}/Create','CustGrpInfoController@create');

    $this->match(['get','post'], '/{cust}/validate','CustGrpInfoController@validatemodel');
    $this->match(['get','post'],'/{cust}/index{page?}','CustGrpInfoController@index');
	$this->match(['get','post'],'/{cust}','CustGrpInfoController@index');

	$this->match(['get','post'],'/{cust}/log{page?}','CustGrpInfoController@log');
	$this->match(['get','post'],'/{cust}/listLog','CustGrpInfoController@listLog');

	$this->match(['get','post'],'/{cust}/getCustGrp','CustGrpInfoController@getCustGrp');

});

// 거래처 관리 라우터
Route::group(['prefix'=>'custcd', 'middleware' => ['web']],function(){

    //$this->get('/{cust}/Pdf','CustCdController@pdf');
    //$this->get('/{cust}/Excel','CustCdController@excel');

    $this->get('/{cust}/listData','CustCdController@listData');

	//거래처 그룹리스트 들고오기
    $this->get('/{cust}/getCustGrp','CustCdController@getCustGrp');

	//거래처 지역리스트 들고오기
    $this->get('/{cust}/getAreaCd','CustCdController@getAreaCd');

	//거래처 수정정보 들고오기
    $this->get('/{cust}/Edit/{id}/{page?}','CustCdController@edit');

	//거래처 등록
    $this->match(['get','post'], '/{cust}/Insert','CustCdController@insert');

	//거래처 수정
	$this->match(['get','post'], '/{cust}/Update','CustCdController@update');

	//거래처 미지급금 목록 메모 수정
	$this->match(['get','post'], '/{cust}/updateMemo','CustCdController@updateMemo');

	//거래처 미수금 목록 메모 수정
	$this->match(['get','post'], '/{cust}/updateMemo1','CustCdController@updateMemo1');

	//거래처 삭제
	$this->match(['get','post'], '/{cust}/_delete','CustCdController@_delete');

    //$this->match(['get','post'], '/{cust}/validate','CustCdController@validatemodel');
    $this->match(['get','post'],'/{cust}/index{page?}','CustCdController@index');
	//$this->match(['get','post'],'/{cust}','CustCdController@index');

	$this->match(['get','post'],'/{cust}/log{page?}','CustCdController@log');
	$this->match(['get','post'],'/{cust}/listLog','CustCdController@listLog');

	Route::get('/{cust}/Excel', 'CustCdController@Excel');

});

// 비용계정그룹
Route::group(['prefix'=>'accountgrp', 'middleware' => ['web']],function(){

    //$this->get('/{cust}/Pdf','accountgrpController@pdf');
    //$this->get('/{cust}/Excel','accountgrpController@excel');

    $this->get('/{cust}/listData','accountgrpController@listData');
    /*
	*/
    $this->get('/{cust}/View/{id}/{page?}','accountgrpController@view');
    $this->get('/{cust}/Edit/{id}/{page?}','accountgrpController@edit');
    $this->match(['get','post'], '/{cust}/Insert','accountgrpController@insert');
    $this->match(['get','post'], '/{cust}/Update','accountgrpController@update');
    $this->match(['get','post'], '/{cust}/Delete','accountgrpController@_delete');
    $this->match(['get','post'], '/{cust}/Create','accountgrpController@create');

    //$this->match(['get','post'], '/{cust}/validate','accountgrpController@validatemodel');
    $this->match(['get','post'],'/{cust}/index{page?}','accountgrpController@index');
	//$this->match(['get','post'],'/{cust}','accountgrpController@index');
});

// 비용계정
Route::group(['prefix'=>'accountcd', 'middleware' => ['web']],function(){

    $this->get('/{cust}/listData','accountcdController@listData');

    $this->get('/{cust}/getAccountGrp','accountcdController@getAccountGrp');

    $this->get('/{cust}/Edit/{id}/{page?}','accountcdController@edit');

    $this->match(['get','post'],'/{cust}/index{page?}','accountcdController@index');

    $this->match(['get','post'], '/{cust}/Update','accountcdController@update');

    $this->match(['get','post'], '/{cust}/Insert','accountcdController@insert');

	$this->match(['get','post'], '/{cust}/Delete','accountcdController@_delete');

});

/* 비용관리탭 시작
 * 비용전표관리
*/
Route::group(['prefix'=>'chitinfo', 'middleware' => ['web']],function(){

    //$this->get('/{cust}/Pdf','ChitInfoController@pdf');
    //$this->get('/{cust}/Excel','ChitInfoController@excel');

	$this->get('/{chitinfo}/listData','ChitInfoController@listData');
	$this->get('/{chitinfo}/listData2','ChitInfoController@listData2');
	$this->get('/{chitinfo}/listData3','ChitInfoController@listData3');

	$this->get('/{chitinfo}/searchAccountList','ChitInfoController@searchAccountList');
	$this->get('/{chitinfo}/insert','ChitInfoController@insert');
	$this->get('/{chitinfo}/update','ChitInfoController@update');
	$this->get('/{chitinfo}/getData','ChitInfoController@getData');
	$this->get('/{chitinfo}/_delete','ChitInfoController@_delete');

	$this->get('/{chitinfo}/PdfDetail/{start_date?}{end_date?}','ChitInfoController@PdfDetail');
	$this->get('/{chitinfo}/PdfDetail2/{start_date?}{end_date?}','ChitInfoController@PdfDetail2');

	$this->match(['get','post'],'/{chitinfo}/index','ChitInfoController@index');
	$this->match(['get','post'],'/{chitinfo}/index2','ChitInfoController@index2');
	$this->match(['get','post'],'/{chitinfo}/index3','ChitInfoController@index3');

	$this->get('/{chitinfo}/getlistFee','ChitInfoController@getlistFee');
	$this->get('/{chitinfo}/getlistPump','ChitInfoController@getlistPump');

	$this->match(['get','post'],'/{chitinfo}/index3','ChitInfoController@index3');
	//$this->match(['get','post'],'/{cust}','ChitInfoController@index');
});

// 부조부 조회
Route::group(['prefix'=>'aidlist', 'middleware' => ['web']],function(){

    $this->get('/{cust}/listData','AidListController@listData');
	// 회계계정코드 리스트 들고오기
    $this->get('/{cust}/getAccountingCode','AidListController@getAccountingCode');

    $this->match(['get','post'],'/{cust}/index{page?}','AidListController@index');

	$this->get('/{aidlist}/PdfList/{start_date?}{end_date?}{ACCOUNT_MK?}','AidListController@PdfList');

	$this->get('/{aidlist}/listDataBeforeSum','AidListController@listDataBeforeSum');
	
});

// 계정그룹별 조회
Route::group(['prefix'=>'accountgrplist', 'middleware' => ['web']],function(){

    //$this->get('/{cust}/Pdf','AccountGrpListController@pdf');
    //$this->get('/{cust}/Excel','AccountGrpListController@excel');

    $this->get('/{cust}/listData','AccountGrpListController@listData');
	// 계정그룹 리스트 들고오기
    $this->get('/{cust}/getAccountGrp','AccountGrpListController@getAccountGrp');
	// 비용계정 추가
    $this->match(['get','post'], '/{cust}/Insert','AccountGrpListController@insert');
	// 비용계정 수정
    $this->get('/{cust}/Edit/{id}/{page?}','AccountGrpListController@edit');
	// 비용계정 업데이트
    $this->match(['get','post'], '/{cust}/Update','AccountGrpListController@update');
	// 비용계정 삭제
    $this->match(['get','post'], '/{cust}/Delete','AccountGrpListController@delete');
    /*
    $this->get('/{cust}/View/{id}/{page?}','AccountGrpListController@view');
    $this->match(['get','post'], '/{cust}/Create','AccountGrpListController@create'); */

    //$this->match(['get','post'], '/{cust}/validate','AccountGrpListController@validatemodel');
    $this->match(['get','post'],'/{cust}/index{page?}','AccountGrpListController@index');
	//$this->match(['get','post'],'/{cust}','AccountGrpListController@index');
});

// 이월금 정보
Route::group(['prefix'=>'bringamtgrant', 'middleware' => ['web']],function(){

    //$this->get('/{cust}/Pdf','BringAmtGrantController@pdf');
    //$this->get('/{cust}/Excel','BringAmtGrantController@excel');

    $this->get('/{bringamtgrant}/listData','BringAmtGrantController@listData');
	// 계정그룹 리스트 들고오기
    $this->get('/{bringamtgrant}/getAccountGrp','BringAmtGrantController@getAccountGrp');
	// 비용계정 추가
    $this->match(['get','post'], '/{bringamtgrant}/Insert','BringAmtGrantController@insert');
	// 비용계정 수정
    $this->get('/{bringamtgrant}/Edit/{id}/{page?}','BringAmtGrantController@edit');
	// 비용계정 업데이트
    $this->match(['get','post'], '/{bringamtgrant}/Update','BringAmtGrantController@update');
	// 비용계정 삭제
    $this->match(['get','post'], '/{bringamtgrant}/Delete','BringAmtGrantController@_delete');
    /*
    $this->get('/{cust}/View/{id}/{page?}','BringAmtGrantController@view');
    $this->match(['get','post'], '/{cust}/Create','BringAmtGrantController@create'); */

    //$this->match(['get','post'], '/{cust}/validate','BringAmtGrantController@validatemodel');
    $this->match(['get','post'],'/{bringamtgrant}/index{page?}','BringAmtGrantController@index');
	$this->match(['get','post'],'/{bringamtgrant}/setClose','BringAmtGrantController@setClose');

	$this->match(['get','post'],'/{bringamtgrant}/getBringAmt','BringAmtGrantController@getBringAmt');


});
/* 비용관리탭 끝 */

Route::get('/home', 'HomeController@index');


$this->match(['get','post'], '/config/changeConnection', 'Controller@changeConnection'); 

Route::get('/config/setUseChkSujoDisplay', 'Controller@setUseChkSujoDisplay');
Route::get('/config/setUseConfigSujocust', 'Controller@setUseConfigSujocust');
Route::get('/config/getConfig', 'Controller@getConfig');
Route::get('/config/setConfigSkin', 'Controller@setConfigSkin');
Route::get('/config/setConfigQty', 'Controller@setConfigQty');
Route::get('/config/setMenutoggle', 'Controller@setMenutoggle');
Route::get('/config/indexMenual', 'Controller@indexMenual');
Route::get('/config/notice', 'Controller@notice');




Route::get('/config/downMenual1', 'Controller@downMenual1');
Route::get('/config/downMenual2', 'Controller@downMenual2');

$this->match(['get','post'], '/config/setFeedBack', 'Controller@setFeedBack');
$this->match(['get','post'], '/config/getFeedBack', 'Controller@getFeedBack');
$this->match(['get','post'], '/config/getFeedBackDetail', 'Controller@getFeedBackDetail');
$this->match(['get','post'], '/config/setComplete', 'Controller@setComplete');

$this->match(['get','post'],'/config/index{page?}','Controller@errList');

$this->match(['get','post'], '/config/setLoginByAdmin/{CORP_MK}', 'Controller@setLoginByAdmin');
$this->match(['get','post'], '/config/setLoginByBackup/{CORP_MK}', 'Controller@setLoginByBackup');

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => ['entrust-gui']], function() {
    Route::get('/', 'AdminController@welcome');
    Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
});

Entrust::hasRole('role-name');
Entrust::can('permission-name');
Entrust::ability('admin,owner', 'create-post,edit-user');



<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class STOCK_INFO extends Model
{
    //
	protected $table ="STOCK_INFO";

    public $timestamps = false;

}

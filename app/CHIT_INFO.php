<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CHIT_INFO extends Model
{
    //
	protected $table ="CHIT_INFO";

    public $timestamps = false;

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ACCOUNT_GRP extends Model
{
    //
	protected $table ="ACCOUNT_GRP";
		
    public $timestamps = false;
	
}

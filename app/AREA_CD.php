<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AREA_CD extends Model
{
    //
	protected $table ="AREA_CD";

	protected $primaryKey = "AREA_CD";

    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CUST_GRP_INFO extends Model
{
    //
    protected $table ="CUST_GRP_INFO";

    public $timestamps = false;
	
	protected $CORP_DIV_NM;
}

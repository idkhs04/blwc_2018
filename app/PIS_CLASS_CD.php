<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PIS_CLASS_CD extends Model
{
    //

	protected $table ="PIS_CLASS_CD";

	//protected $primaryKey = "ORIGIN_CD";

	public $timestamps = false;
}

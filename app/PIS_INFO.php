<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PIS_INFO extends Model
{
    //
	protected $table ="PIS_INFO";

	public $timestamps = false;

}

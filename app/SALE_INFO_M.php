<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SALE_INFO_M extends Model
{
    //
	protected $table ="SALE_INFO_M";

	public $timestamps = false;
}

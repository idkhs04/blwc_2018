<?php

namespace App;
use Esensi\Model\Contracts\HashingModelInterface;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\HashingModelTrait;
use Esensi\Model\Traits\ValidatingModelTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use DB;
use Session;
use App\CORP_INFO;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, ValidatingModelInterface, HashingModelInterface
{

    use Authenticatable, CanResetPassword, ValidatingModelTrait, EntrustUserTrait, HashingModelTrait;

	public $timestamps = false; //updated_at 사용안함 주석처리하면 자동으로 updated_at값갱신
	protected $table = 'MEM_INFO';
	
	protected $_user;

	protected $username = 'MEM_ID';
	
	protected $fillable = [
		
		'MEM_ID', 
		'MEM_TYPE', 
		'PWD_NO',
		'CORP_MK',
		'GRADE',
		'PSTN',
		'JUMIN_NO',
		'ADDR1',
		'ADDR2',
		'POST_NO',
		'PHONE_NO',
		'HP_NO',
		'EMAIL',
		'STATE',
		'MAIL_CHK',
		'REG_DATE',
		'REG_IP',
		'PWD_DATE',
		'PWD_CNT',
		'password',
		'remember_token',
		'name',
	];

	public function getRole($roleid){
		
		$role = Role::findOrFail(10)->first();
		return $role;
	}
	
	public function corp_info(){
		

        return $this->belongsTo('App\CORP_INFO','CORP_MK','CORP_MK');
    }

	protected $hidden = [
        'password', 'remember_token',
    ];

	protected $rulesets = [

        'creating' => [
            //'email'      => 'required|email|unique:MEM_INFO',
            'password'   => 'required',
        ],

        'updating' => [
            'email'      => 'required|email|unique:MEM_INFO',
            'password'   => '',
        ],
    ];
	
    /** 현섭삭제 */
    public function entrustPasswordHash()
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }


	/*
	protected $table = 'Users';
	protected $fillable = [
		'id',
		'name',
		'email',
		'password',
		'remember_token',
		'created_at',
		'updated_at',
	];
	*/
}

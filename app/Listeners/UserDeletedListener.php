<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:20
 */
//UserDeletedListener

namespace App\Listeners;

use Acoustep\EntrustGui\Events\UserDeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class UserDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreatedEvent  $event
     * @return void
     */
    public function handle(UserDeletedEvent $event)
    {
        //dd($event);
        Log::info('Deleted: '.$event->user->email);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:02
 */
namespace App\Listeners;

use Acoustep\EntrustGui\Events\UserUpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class UserUpdatedListener
{
    /**
     * Update the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdatedEvent  $event
     * @return void
     */
    public function handle(UserUpdatedEvent $event)
    {
        Log::info('updated: '.$event->user->email);
    }
}
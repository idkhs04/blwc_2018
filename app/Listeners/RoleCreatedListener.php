<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:38
 */

namespace App\Listeners;

use Acoustep\EntrustGui\Events\RoleCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class RoleCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RoleCreatedEvent  $event
     * @return void
     */
    public function handle(RoleCreatedEvent $event)
    {

        Log::info('created: '.$event->role->name);
    }
}
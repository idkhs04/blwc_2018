<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:48
 */


namespace App\Listeners;

use Acoustep\EntrustGui\Events\PermissionUpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class PermissionUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PermissionUpdatedEvent  $event
     * @return void
     */
    public function handle(PermissionUpdatedEvent $event)
    {

        Log::info('created: '.$event->permission->name);
    }
}
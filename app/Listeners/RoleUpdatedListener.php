<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:38
 */


namespace App\Listeners;

use Acoustep\EntrustGui\Events\RoleUpdatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class RoleUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RoleUpdatedEvent  $event
     * @return void
     */
    public function handle(RoleUpdatedEvent $event)
    {

        Log::info('updated: '.$event->role->name);
    }
}
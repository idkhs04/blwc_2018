<?php
/**
 * Created by PhpStorm.
 * User: idkhs04
 * Date: 2016-07-22
 * Time: 오후 3:39
 */


namespace App\Listeners;

use Acoustep\EntrustGui\Events\RoleDeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class RoleDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RoleDeletedEvent  $event
     * @return void
     */
    public function handle(RoleDeletedEvent $event)
    {
        Log::info('Deleted: '.$event->role->name);
    }
}
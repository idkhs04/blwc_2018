<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SALE_INFO_D extends Model
{
    //
	protected $table ="SALE_INFO_D";

	public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ORIGIN_CD extends Model
{
    //
	protected $table ="ORIGIN_CD";

    public $timestamps = false;
}

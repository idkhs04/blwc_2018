<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BRING_AMT_INFO extends Model
{
    //
	protected $table ="BRING_AMT_INFO";

    public $timestamps = false;

}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

	//'default' => 'sqlsrv',
    'default' => env('DB_CONNECTION', 'sqlsrv'),
	//'backup' => env('DB_CONNECTION', 'backup_sqlsrv'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
        ],

        'sqlsrv' => array(
            'driver' => 'sqlsrv',
            'host' => '210.205.92.52',
			// 리눅스일 경우 위의 host 줄을 주석처리하고 아래의 주석 1줄만 풀어준다
			//'host' => 'BLWC', //'env('DB_HOST', 'BLWC'), // Provide IP address here
			// /etc/reetds/freetds.conf
            'database' => env('DB_DATABASE', 'BLWC_2018'), //blwc_20161130
            'username' => env('DB_USERNAME', 'sa'),
            'password' => env('DB_PASSWORD', '@ksric0196!'),
            'prefix' => '',
			'charset'   => 'utf8',
			'characterset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
        ),
		'backup_sqlsrv' => array(
            'driver' => 'sqlsrv',
            'host' => 'BLWC', //'env('DB_HOST', 'BLWC'), // Provide IP address here
			// /etc/reetds/freetds.conf
            'database' => env('DB_DATABASE', 'blwc_backup'), //
            'username' => env('DB_USERNAME', 'sa'),
            'password' => env('DB_PASSWORD', '@ksric0196!'),
            'prefix' => '',
			'charset'   => 'utf8',
			'characterset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
        ),
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];

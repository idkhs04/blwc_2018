<?php
return [
	"layout" => "entrust-gui::app",
	//"layout" => "app",
	"route-prefix" => "entrust-gui",
	"pagination" => [
		"users" => 10,
		"roles" => 10,
		"permissions" => 10,
	],
	"middleware" => ['web', 'entrust-gui.admin'],
	//"middleware" => null,
	"unauthorized-url" => '/login',
	"middleware-role" => ['sysAdmin'],
	"confirmable" => false,
	//"confirmable" => false,

	"users" => [
		'deletable' => true,
	],
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => ':attribute는 문자, 숫자, "_" 만 입력 가능합니다',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => ':attribute는(은) 날짜형식 입력입니다.',
    'date_format'          => ':attribute는(은) :format 형식입니다(YYYY-MM-DD).',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ':attribute는(은) 이메일형식 입력입니다.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => ':attribute 는(은) 최대 :max 자 내로 입력하셔야합니다.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => ':attribute는 최소 :min 자 이상입력하셔야 합니다.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ':attribute는(은) 숫자형식 입니다.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => ':attribute는(은) 필수입력 입니다.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => ':attribute는(은):other와(과) 동일하게 입력하셔야 합니다.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
		'CORP_MK'		=> '회사코드',
		'SUJO_NO'		=> '수조번호',
		'PIS_MK'		=> '어종코드',
		'PIS_NM'		=> '어종명',
		'SIZES'			=> '규격',
		'ORIGIN_CD'		=> '원산지코드',
		'ORIGIN_NM'		=> '원산지명',
		'QTY'			=> '수량',
		'UNCS'			=> '단가',
		'INPUT_DISCOUNT'=> '할인금액',
		'INPUT_DATE'	=> '입고일자',
		'CUST_MK'		=> '거래처코드',
		'FRNM'			=> '거래처상호',
		'CUST_GRP_CD'	=> '거래처그룹코드',
		'UNCL_CUST_MK'	=> '거래처그룹코드',
		'PROV_CUST_MK'	=> '거래처그룹코드',
		'CUST_GRP_NM'	=> '거래처그룹명',
		'UPTE'			=> '업태',
		'UPJONG'		=> '업종',
		'POST_NO'		=> '우편번호',
		'ADDR1'			=> '주소1',
		'chkPIS_MK'		=> '어종중복체크',

		'RPST'			=> '대표자명',
		'ETPR_NO'		=> '사업자등록번호',
		'PHONE_NO'		=> '전화번호',

		'MEM_ID'		=> '회원아이디',
		'PWD_NO'		=> '비밀번호',
		'NAME'			=> '성명',
		'JUMIN_NO'		=> '생년월일',
		'GRP_CD'		=> '그룹코드',
		'GRP_NM'		=> '그룹명',
		'AREA_CD'		=> '지역',
		'AMT'			=> '금액',
		'ACCOUNT_GRP_CD'=> '비용계정그룹' ,
		'ACCOUNT_GRP_NM'=> '비용계정그룹명' ,
		'ACCOUNT_MK'	=> '비용계정코드',
		'ACCOUNT_NM'	=> '비용계정명',
		'BRING_AMT'		=> '이월금',
		'WRITE_DATE'	=> '작성일',
		'UNCL_AMT'		=> '단가',
		'TOTAL_SALE_AMT'=> '합계',
		'CARD_AMT'		=> '카드금액',
		'CASH_AMT'		=> '현금금액',
		'UNCL_AMT'		=> '미수금액',
		'CLOSE_DATE'	=> '마감일자',
		'QTY_TOBE'		=> '중량',
		'START_DATE'	=> '시작일자',
		'END_DATE'		=> '종료일자',
		'DST_SUJO_NO'	=> '이고할 수조번호',
		'REASON'		=> '사유',
		'EMAIL'			=> '이메일 주소',
		'EMAIL_EDIT'	=> '이메일 주소',
		'RPST_EMAIL'	=> '이메일 주소',
		'ETPR_NO_EDIT'	=> '사업자번호',
		'GOODS'			=> '품목',
		'SUPPLY_AMT'	=> '공급가액',
		'JUMIN_NO_EDIT'	=> '생년월일',
		'JUMIN_NO'		=> '생년월일', 
		'PWD_NO_CONFIRMATION' => '비밀번호확인',
		'CORP_DIV'		=> '업체구분',
		'MEMO'			=> '메모',
	],

];

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Keywords" content="활어 활어시장 재고 재고관리 시스템 ERP 수산 수산시장 도매 재고관리시스템 부산 활어 조합 부산활어 부산활어조합" />
		<meta name="Description" content="부산활어조합의 활어 도소매 유통시스템 입니다. 재고관리, 판매관리, 계산서 관리, 통계 등 다양한 기능을 제공합니다." />
		<title>@yield('title') - 부산활어조합</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="Shortcut Icon" type="image/x-icon" href="/image/logo.png" />

		<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
		<link href="/static/css/normalize-css/normalize.css" />
		<link rel="stylesheet" href="/static/css/fontium.css">
		<link rel="stylesheet/less"  type="text/css" href="/static/less/bootstrap/bootstrap.less" />
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

		<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<!-- DataTables -->
		<link rel="stylesheet" href="/static/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="/static/css/datatable/jquery.dataTables.min.css">
		<link rel="stylesheet" href="/static/css/jquery-ui.min.css">
		
		<!-- Theme style -->
		<link rel="stylesheet" href="/bower_components/AdminLTE/dist/css/AdminLTE.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		   folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="/static/dist/css/skins/_all-skins.min.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="/static/plugins/iCheck/flat/blue.css">
		<!-- Morris chart -->
		<link rel="stylesheet" href="/static/plugins/morris/morris.css">
		<!-- jvectormap -->
		<link rel="stylesheet" href="/static/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
		<!-- Date Picker -->
		<link rel="stylesheet" href="/static/plugins/datepicker/datepicker3.css">
		<!-- Daterange picker -->
		<link rel="stylesheet" href="/static/plugins/daterangepicker/daterangepicker.css">
		<!-- Daterange picker -->
		<!--<link rel="stylesheet" href="/static/plugins/daterangepicker/daterangepicker-bs3.css">-->
		<link rel="stylesheet" href="/static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		<link rel="stylesheet" href="/static/css/list.css">
		<link rel="stylesheet" href="/static/css/config.css" />
		<link href="/static/js/IntroJs/introjs.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="/static/css/feedback/feedback.css"  type="text/css" media="screen" />
		<link rel="stylesheet" href="/static/css/common.css"  type="text/css" media="screen" />
		<style>
			#pupupzone {
				background-color:white;
				height: 50px;
				position: relative;
				width: 100%;
				color:blue;
				z-index:900;
				font-weight:bold;
				display:none;
			}
			#pupupzone .pop {
				margin:0;
				padding-top: 9px;
				position: relative;
				text-align: center;
			}

			#pupupzone .pop .closebt {
				right: 0;
				top: 12px;
				font-weight:bold;
				color:red;
			}

			.pop a {
				display:inline-block;
			}
		</style>
	</head>
	<body class="hold-transition sidebar-mini skin-blue">
		<div class="wrapper">
			<div id="pupupzone">
				<div class="pop">
					<img src="/image/chrom.jpg" height="35px"/>
					<span class="title"> 현재 사이트는 크롬 브라우저에 최적화 되어있습니다</span>
					<span class="closebt">
						<a href="https://www.google.co.kr/chrome/browser/desktop/" target="_blank" class="btnDownload">크롬 브라우저 설치 하기 (클릭!)</a>
					</span>
				</div>
			</div>
			@include('include.header')
			@include('include.sidemenu')
						
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<div class="is-visible main_alertMsg" style="display: none;"></div>
				@if ($alert = Session::get('alert-success'))
				<div class="alert alert-warning" style="margin:0px 7px 5px 9px;">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ $alert }}
				</div>
				@endif
				<section class="content-header">
					
					
					<!--
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">@yield('class')</li>
						<li class="active">@yield('title')</li>
					</ol>
					-->

					
					<!--
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
					-->
				</section>
				<section class="content">	
					<div class="row">
						@include('entrust-gui::partials.notifications')
						@yield('content')
					</div>
				</section>
			</div>
			@include('include.footer')
			@include('include.aside')
		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>

		<!-- jQuery -->
		<script src="/static/js/jquery/jquery.js"></script>
		<script src="/static/js/jquery/jquery.fileDownload.js" ></script>
		<!-- jQuery Validation-->
		<script src="/static/js/jquery/jquery.validation.min.js"></script>
		<!-- DataTables -->
		<script src="/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- App scripts -->
		<!-- AdminLTE App -->
		<script src="/static/dist/js/app.min.js"></script>
		<script src="/static/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script src="/static/plugins/daterangepicker/daterangepicker.js"></script>
		<!--<script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.js"></script>-->
		<script src="/static/js/common/list.js" ></script>
		<script src="/static/js/common/uiUtile.js" ></script>
		<script src="/static/js/common/config.js" ></script>
		<script src="/static/js/jquery/jquery.blockUI.js"></script>
		
		@yield('script')
		<script>
			// 검색 부 로컬 스토리지 초기화
			
			$(".main-sidebar li a").click(function(){
				localStorage["IS_BACK"]  = "N";
				localStorage["CHECK_ZERO"]		= "";
				localStorage["SEARCH_TEXT"]		= "";
				localStorage["SEARCH_CONDITION"]= "";
			});
			
			@role('sysAdmin')
			if( localStorage['ADMIN_AUTH_CORP_NM'] !== undefined ){
				$("span.adminAuthName").append(" " + localStorage['ADMIN_AUTH_CORP_NM']);
				$("span.adminAuthName").css("display", "inline");
			}else{
				$("span.adminAuthName").css("display", "none");
			}
			@endrole

			
		</script>
		<script type="text/javascript" src="/static/js/IntroJs/intro.js"></script>
		<script type="text/javascript" src="/static/js/common/ajax-datatables-expired-session.js"></script>
		<script type="text/javascript" src="/static/components/feedback/js/feedback.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				
				// 크롬인지 아닌지 체크..
				var filter = "win16|win32|win64|mac";

				if(navigator.platform){

					if(0 > filter.indexOf(navigator.platform.toLowerCase())){
						//alert("Mobile");
					}else{
						var agt = navigator.userAgent.toLowerCase();
						if (agt.indexOf("chrome") == -1){
							$("#pupupzone").css("display", "block");
							setTimeout(function(){ $("#pupupzone").slideUp(); }, 5000);
						}else{
							$("#pupupzone").css("display", "none");
						}
					}
				}
				
				// 사용자 환경 설정 : config.js 파일 참조
				getConfig();
				
				// 수조의 거래처 표기
				$("input[name='config_SujoCust']").click(setUseConfigSujocust);

				// 수조체크 표기
				$("input[name='config_ChkSujo']").click(setUseChkSujoDisplay);
				
				// 마리 수 사용
				$("input[name='config_Qty']").click(setUseConfigQty);

				// 스킨 사용
				$("#layout-skins-list td a").on("click", function(){
					setSkin( $(this).attr("data-skin") );
				});

				// 좌측메뉴 최소화 사용
				$("input[name='config_collapse']").on("click", function(){
					var isChecked = $(this).is(":checked") ? 1 : 0;
					setMenutoggle( isChecked );
				});

				$.getJSON('/config/getFeedBack', {
					_token			: '{{ csrf_token() }}'
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					var sumQty = data.cnt;
					var is_complt, _class ;

					$(".rqst_error_cnt").html(sumQty);
					$("ul.feedBacList > li.header").html(sumQty + "개의 피드백 알림이 있습니다")

					data.result.forEach(function(item){
						
						if(item.IS_CMPLT == 1 ){
							is_complt	= "<i class='icon fa fa-check'></i>완료";
							_class		= "alert-success";
						}else{
							is_complt = "<i class='icon fa fa-warning'></i>대기";
							_class	  = "alert-warning";
						}
						
						$("ul.feedBacList").prepend("<li class=' " + _class + "'>" +
														"<ul class='menu'>" +
															"<li>" + 
																"<a href='#'>" +
																	"<div class='pull-left'>" + is_complt + "</div>" + 
																	"<h4>" + item.FRNM + "<small><i class='fa fa-clock-o'></i>"+ item.WRITE_DT + "</small>" +
																	"</h4>" + 
																	"<p>"+ item.ISSUE +"</p>" + 
																"</a>" +
															"</li>" +
														"</ul>" +
													"</li>"
						);
					});

				}).error(function(xhr,status, response) {
					
				});

				// feedback
				Feedback({
					h2cPath:'/static/components/feedback/js/html2canvas.js'
					,url : '/config/setFeedBack?_token={{ csrf_token() }}'
				});
				
			});
		</script>
		
		@stack('scripts')
		
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>@yield('title')::부산활어조합</title>
		
		<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
		<link rel="Shortcut Icon" type="image/x-icon" href="/image/logo.png" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link href="/static/css/normalize-css/normalize.css" />

		<link rel="stylesheet" href="/static/css/fontium.css">

		<link rel="stylesheet/less"  type="text/css" href="/static/less/bootstrap/bootstrap.less" />
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		<!-- DataTables -->
		<link rel="stylesheet" href="/static/plugins/datatables/dataTables.bootstrap.css">
		
		<link rel="stylesheet" href="/static/css/datatable/jquery.dataTables.min.css">

		<link rel="stylesheet" href="/static/css/jquery-ui.min.css">
		

		<!-- Theme style -->
		<link rel="stylesheet" href="/static/css/AdminLTE/AdminLTE.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		   folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="/static/dist/css/skins/_all-skins.min.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="/static/plugins/iCheck/flat/blue.css">
		<!-- Morris chart -->
		<link rel="stylesheet" href="/static/plugins/morris/morris.css">
		<!-- jvectormap -->
		<link rel="stylesheet" href="/static/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
		<!-- Date Picker -->
		<link rel="stylesheet" href="/static/plugins/datepicker/datepicker3.css">
		<!-- Daterange picker -->
		<link rel="stylesheet" href="/static/plugins/daterangepicker/daterangepicker.css">
		<!-- Daterange picker -->
		<!--<link rel="stylesheet" href="/static/plugins/daterangepicker/daterangepicker-bs3.css">-->

		<link rel="stylesheet" href="/static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		<link rel="stylesheet" href="/static/css/list.css">

		<link rel="stylesheet" href="/static/css/config.css" />

		<script src="/static/js/less/less.js"></script>


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			th.check{
				max-width:20px;
			}

			.menu li {
				display: inline;
				position: relative;
			}
			.menu ul { padding:0; margin-bottom:0;}
			.menu ul a{ margin:0 0 5px 10px;}
			table.dataTable thead > tr > th {
				text-align: center;
			}

			.colCheck { width:80px;}
			.check{ text-align:center;}
			caption { display:none; }
			
			table.dataTable tbody th, table.dataTable tbody td {
				padding: 8px 10px;
				padding: 1px 7px 1px 7px;
				margin: 0;
				vertical-align: middle;
			}

			.menu ul a {
				margin: 1px 2px 1px 2px;
			}
			table.dataTable tbody .btn{
				padding:2px 12px;
			}
			.btn-app {
				border-radius: 3px;
				position: relative;
				padding: 8px 3px;
				margin: 0 0 10px 10px;
				min-width: 60px;
				height: 52px;
				text-align: center;
				color: #666;
				border: 1px solid #ddd;
				background-color: #f4f4f4;
				font-size: 12px;
			}
			.box {
				position: relative;
				border-radius: 3px;
				background: #ffffff;
				border-top: 3px solid #d2d6de;
				margin-bottom: 8px;
				width: 100%;
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
			}
			.form-group{margin-bottom:3px;}

			#layout-skins-list th{ text-align:center;}
			#layout-skins-list td{ padding:2px;}
			#layout-skins-list tbody tr > td:nth-child(2) {text-align:center;}
			
			.sizes{ color:#f39c12;}
			.bold{font-weight:bold;}
			
			.alert-warning {
				background-color: #fcf8e3;
				border-color: #faebcc;
				color: #8a6d3b;
			}
		</style>
		
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			@include('include.header')
			@include('include.sidemenu')
						
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1> @yield('title')</h1>
					@if ($alert = Session::get('alert-success'))
					  <div class="alert alert-warning" style="margin:5px 0 0 0;">
						 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  {{ $alert }}
					  </div>
					@endif
					<!--
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
					-->
				</section>
				<section class="content">	
					<div class="row">
					@include('entrust-gui::partials.notifications')
					@yield('content')
					</div>
				</section>
			</div>
			@include('include.footer')
			@include('include.aside')
		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- jQuery 2.2.3 -->
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- jQuery Validation-->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
		<!-- DataTables -->
		<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<!-- App scripts -->
		<!-- AdminLTE App -->
		<script src="/static/dist/js/app.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script src="/static/plugins/daterangepicker/daterangepicker.js"></script>
		<!-- 다음 우편번호 API-->
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		<script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.js"></script>
		<script src="/static/js/common/list.js" ></script>
		<script src="/static/js/common/config.js" ></script>
		<script src="/static/js/jquery/jquery.blockUI.js"></script>
		<script>
			// 사용자 환경 설정 : config.js 파일 참조
			getConfig();
				
			// 마리 수 사용
			$("input[name='config_Qty']").click(setUseConfigQty);

			// 스킨 사용
			$("#layout-skins-list td a").on("click", function(){
				setSkin($(this).attr("data-skin"));
			});

			// 스크롤 메뉴 생성기
			$(window).scroll(function() {
				
				var displayHeight = $("header").height() + $(".content-wrapper > .content-header").height() + ($(".nav_menu_wapper").height())*2 + $(".dvSearch").height() + 10;
				if( $(document).scrollTop() >= displayHeight){
					$(".dvMainBtnSet").css({
						'position': 'fixed'
						,'z-index': '1010'
						,'float': 'left'
						,'padding-left': '0'
						,'width': '100%'
						,'top': '1%'
						,'height': 'auto'
					
					});
				}else{
					$(".dvMainBtnSet").css({
						'position': 'relative'
						,'z-index': '10'
						,'padding-left': '15px'
						,'height': '85px'
					});
				}
			});

			
		</script>
		@stack('scripts')
	</body>
</html>

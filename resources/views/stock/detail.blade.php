@extends('layouts.main')
@section('class','매입관리')
@section('title','어종별 재고관리 상세')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ vertical-align:baseline; margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblCustList_filter, #tblStockUpdList_filter { display:none;}

	#modal_stock_arrange label{
		display:inline-block;
		width:22%;
		clear:both;
		padding-left:1%;
	}

	#modal_stock_arrange .reason label{
		width:auto;
		padding-left:2%;
	}


	#modal_stock_arrange .form-control{
		display:inline-block;
		width:22%;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust, #tblList_length{
		display:none;
	}

	#tblList_Sum{
		float:right;
		font-weight:bold;
	}

	#SUM_QTY{ padding-left:2px; border:none; color:blue; }

	.btnSelect{ margin-right:10px;}

	.input-group.dt-body-right{ text-align:right;}
	.dt-body-right{ text-align:right;}
	#pdf{display:none;}

	.red{color:red;}
	.blue{color:blue;}
	.green{color:green;}

	.sm-font{ font-size:11px;}
	/*.dataTables_length{ display:none;}*/

	#tblList tbody .btn {
		padding: 0px 4px;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="error_message"><ul></ul></div>

			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<!--<button type="button" class="btn btn-success" id="btnStockArranage" ><i class="fa fa-circle-o-notch"></i> 재고정리</button>
					<button type="button" class="btn btn-success btnPdf" id="btnPdf" ><i class="fa fa-edit"></i> 출력</button>
					
					<button type="button" class="btn btn-danger" id="btnDelete" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnUpdate" ><i class="fa fa-edit"></i> 재고수정</button>
					-->
					<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력</button>
					<button type="button" class="btn btn-success" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<label>
						<input type="radio" name="srtRadio" class="srtRadio" id="srt_input"  />
						입고
					</label>
					<label>
						<input type="radio" name="srtRadio" class="srtRadio" id="srt_output" checked />
						출고
					</label>
				</div>
				<div id="reportrange" class="btn-group" style="background: #fff; cursor: pointer; padding: 1px 5px; border: 1px solid #ccc; width:170px">
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="SUJO_NO">수조번호</option>
					<option value="FRMN">거래처명</option>
					<option value="PIS_NM">어종명</option>
					<option value="QTY">출고</option>
					<option value="UNCS">단가</option>
					<option value="OUTLINE">적요</option>
				</select>
			</div>

			<div class="col-md-2 col-xs-6">
				<div class="input-group dt-body-right">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<input type="hidden" id="PIS_MK"  name="PIS_MK" value="{{$PIS_MK}}" />
					<input type="hidden" id="SIZES"   name="SIZES" value="{{$SIZES}}"  />
					<input type="hidden" id="hSujo_no" name="hSujo_no" />
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="dataTables_length" id="tblList_Sum">
				<span class="glyphicon glyphicon-asterisk"></span>현재고<span type="text" id="SUM_QTY" ></span> Kg
			</div>
			<table id="tblList" class="responsive table table-bordered table-hover display nowrap" summary="상세재고 목록" width="100%">
				<caption>상세재고조회</caption>
				<thead>
					<tr>
						<th class="check" width="30px">관리</th>
						<th class="name" width="60px" >어종</th>
						<th class="name" width="30px">수조</th>
						<th class="name" width="100px">거래처</th>
						<th class="name" width="60px">입고일</th>
						<th class="name" width="60px">입고단가</th>
						<th class="name" width="60px">출고일</th>
						<th class="name" width="60px">재고정리</th>
						<th class="name" width="30px">입고</th>
						<th class="name" width="30px">출고</th>
						<th class="name" width="60px">판매단가</th>
						<th class="name" width="80px">판매이익</th>
						
						<th width="80px">입출고 합계</th>
						<th class="name" width="auto">적요</th>
						<!--<th class="name">SEQ</th>-->
					</tr>
				</thead>
				
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		var start = moment().subtract(6, 'days');
		var end = moment();

		$("#btnLog").attr("href", "javascript:;")
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}
		// 입고정보 : 입고일
		$("#modal_sujo_input_date, #modal_stock_output_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			opens:"left",
			cancelClass: "",
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);



		getSumQty();
		
		var origin_in_date	= '2000-01-01';
		var out_Qty			= 0.0;
		// 입고날짜를 기준으로 입력된 순번으로 => 출고날짜 순으로 보이도록 조회
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			order : [[4, "desc"], [6, "desc"], [0, "desc"]],
			//order : [[0, "desc"]],
			bInfo : false,
			ajax: {
				url: "/stock/stock/detailData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.pis_mk		= $("input[name='PIS_MK']").val();
					d.sizes			= $("input[name='SIZES']").val();
					d.inOutDate		= $("input[name='srtRadio']:checked").attr("id");
				}
			},
			columns: [
				{
					data: "SEQ",
					name: "ST.SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							
							// 입고된 재고중 반품된 입고는 제외
							if( row.OUTPUT_DATE === null && row.CUST_MK != null && row.REASON == null){
								return	"<button type='button' class='btn btn-success btnUpdate' data-val='" + data +"'> 수정</button>" +
										" <button type='button' class='btn btn-danger btnDelete' data-val='" + data +"'> 삭제</button>" ;
							}else{
								return '';
							}
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data:   "PIS_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.PIS_NM + " <span class='sizes'>" + row.SIZES + "</span><span class='bold'> [" + row.ORIGIN_NM  + "] </span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-center"},
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'INPUT_DATE', name: 'ST.INPUT_DATE', className: "dt-body-center" },
				{
					data: 'UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'OUTPUT_DATE', name: 'OUTPUT_DATE', className: "dt-body-center" },
				{ 
					data: 'REASON_NM', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== ""){
								return data;
							}else{
								if( row.OUTLINE != "" && row.OUTLINE !== null && ( row.OUTLINE.includes('이고') || row.OUTLINE.includes('출고') )){
									return "<span class='sm-font'>" + row.OUTLINE.substr(0, 6) + "</span>";
								}
							}
						}
						return data;
					},
					className: "dt-body-center" 
				},
				{
					data: 'IN_QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{
					data: 'OUT_QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{
					data: 'SALE_UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right green"
				},
				{
					data: 'PROFIT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return  ( row.SALE_UNCS - row.UNCS ) * (row.OUT_QTY );
					},
					className: "dt-body-right"
				},
				{
					"className":      'dt-body-left sumOutQty',
					"orderable":      false,
					
				},
				{
					data: 'OUTLINE',
					name: 'ST.OUTLINE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return  data;
					},
					className: "dt-body-left"
				},
				{ data: 'SEQ',	 name: 'ST.SEQ' , visible : false},
				{ data: 'ORIGIN_NM',	 name: 'ORIGIN_NM', visible : false },
			],
			fnRowCallback: function(nRow, nData){
				
				//console.log('loop');
				//초기값
				if( ( origin_in_date == '2000-01-01' || origin_in_date == nData.INPUT_DATE ) && ( nData.OUTPUT_DATE != null && nData.OUTPUT_DATE != "null" )){
					
					origin_in_date	=  nData.INPUT_DATE; 
					out_Qty			+= $.isNumeric(nData.OUT_QTY) ? parseFloat(nData.OUT_QTY) : 0; 
					
				}else if ( ( nData.OUTPUT_DATE == "null" || nData.OUTPUT_DATE == null ) && ( nData.OUT_QTY == 0 || nData.OUT_QTY == null) && nData.REASON == null ){
					
					$(nRow).find(".dt-body-left.sumOutQty").html("소합계 : " + "<span class='red'>" + nData.IN_QTY + "</span> / <span class='blue'>" +  out_Qty.toFixed(1) + "</span>") ;
					origin_in_date = '2000-01-01';
					out_Qty = 0;
				}else{
					out_Qty = 0;
				}
			},
			fnDrawCallback: function () {
				out_Qty = 0;
				origin_in_date = '2000-01-01';

			},
			footerCallback: function ( row, data, start, end, display ) {
				
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총입고
				input_stock= api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 8 ).footer() ).html(Number(input_stock.toFixed(2)).format());

				// 총출고
				output_stock = api.column( 9 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 9).footer() ).html(Number(output_stock.toFixed(2)).format());

				// 판매이익
				profit = api.column(11 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column(11).footer() ).html(Number(profit.toFixed(2)).format());
				

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ location.href="/buy/sujo/index?page=1"; });

		/** 그리드 선택  **/
		/*
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		*/

		$('#tblList tbody').on( 'click', 'button', function () {
			//var data = oTable.row( $(this).parents('tr') ).data();
			//location.href = "/stock/stock/detail?PIS_MK=" + data.PIS_MK + "&SIZES=" + data.SIZES;
		});

		// 입고일/출고일 선택 초기화
		function setConditionDefault(){
			if( $("#srt_input").is(":checked")) {
				//$("#reportrange > label").text("입고일");
			}else{
				//$("#reportrange > label").text("출고일");
			}
			oTable.draw() ;
		}
		$(".srtRadio").click(setConditionDefault);

	
		// 재고 수정/삭제 클릭
		$("body").on("click", ".btnUpdate, .btnDelete", function(){

			//if( chkInputTble("tblList", "재고수정") ){

				var selectedSEQ = $(this).attr("data-val"); 
				var rowData = oTable.row( $("#tblList tbody tr > td > button[data-val='" + selectedSEQ + "']").parents('tr') ).data();
				
				//console.log(rowData);
				
				//return false;
				
				// var cTable = $('#tblStockUpdList').DataTable();
				//cTable.destroy();
				// dataTable을 여러번 새롭게 호출하기위해서 임시로 생성후 삭제

				if ( $.fn.DataTable.isDataTable('#tblStockUpdList') ) {
					$('#tblStockUpdList').DataTable().destroy();
					$('#tblStockUpdList tbody').off( 'click', 'button');
				}
				$('#tblStockUpdList tbody').empty();

				var cTable = $('#tblStockUpdList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					destroy:true,
					bFilter: false,
					bInfo: false,
					bLengthChange: false,
					ajax: {
						url: "/stock/stock/getStockDetailUpdateList",
						data:function(d){
							d.pis_mk		= rowData.PIS_MK;
							d.sizes			= rowData.SIZES;
							d.origin_cd		= rowData.ORIGIN_CD;
							d.origin_nm		= rowData.ORIGIN_NM;
							d.input_date	= rowData.INPUT_DATE;
							d.sujo_no		= rowData.SUJO_NO;
							d.option		= "1";
							d.qty			= rowData.IN_QTY;
							d.seq			= rowData.SEQ;
							d.cust_mk		= rowData.CUST_MK;
							// 재고정리일때만..option발동
						}
					},
					columns: [
						{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-center red bold" },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'SIZES',  name: 'SIZES' },
						{ data: 'ORIGIN_NM',  name: 'ORIGIN_NM', className: "dt-body-center" },
						{ data: 'INPUT_DATE',  name: 'INPUT_DATE', className: "dt-body-center" },
						{
							data: 'QTY',
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return Number(data).format();
								}
								return data;
							},
							className: "dt-body-right"
						},
						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

				$("#modal_sujo_stock_update").modal("show");

				var isUpdate = $(this).hasClass("btnUpdate") ? true : false;

				$('#tblStockUpdList tbody').on( 'click', 'button', function () {

					var data = cTable.row( $(this).parents('tr') ).data();
					

					
					// 재고수정 이동
					if( isUpdate ){
						location.href = "/stock/stock/detailEdit/" + data.PIS_MK + "/" + data.SIZES + "/" + selectedSEQ + "/" + data.SUJO_NO ;
					}else{
						// 삭제
							
							if( confirm('선택된 재고를 삭제 하시겠습니까?')){
								var rowData = data;
								
								$.getJSON('/stock/stock/removeStockInSujo', {
									'SUJO_NO'		: data.SUJO_NO,
									'PIS_MK'		: rowData.PIS_MK ,
									'SIZES'			: rowData.SIZES ,
									'SEQ'			: selectedSEQ,
									_token			: '{{ csrf_token() }}'
								}).success(function(xhr){
									var data = jQuery.parseJSON(JSON.stringify(xhr));

									var info = $('#modal_sujo_stock_update .edit_alert');
									info.hide().find('ul').empty();
									info.slideDown();
									$("#modal_sujo_stock_update").modal("hide");
									oTable.draw() ;
									getSumQty();

								}).error(function(xhr,status, response) {
									var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
									var info = $('#modal_sujo_stock_update .edit_alert');
									info.hide().find('ul').empty();
										for(var k in error.message){
											if(error.message.hasOwnProperty(k)){
												error.message[k].forEach(function(val){
													info.find('ul').append('<li>' + val + '</li>');
												});
											}
										}
									info.slideDown();
								});
							}
						}
				});
			//}
		});

		// 거래처 검색
		$("#btnSearchCust").click(function(){
			$( ".slideSearchCust" ).show(function(){

				var cTable = $('#tblCustList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					bLengthChange: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='strSearchCust']").val();
						}
					},
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' },
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' },
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 추가</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();

					$("#modal_stock_cust_mk").val(data.CUST_MK);
					$("#modal_stock_cust_nm").val(data.FRNM);
					$("input[name='strSearchCust']").val('');
					$(".slideSearchCust" ).slideUp();
				});

				$("input[name='strSearchCust']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			});
		});

		// f : 총합계 불러오기
		function getSumQty(){
			$.getJSON('/stock/stock/getQtySum', {
				'PIS_MK'		: $("#PIS_MK").val() ,
				'SIZES'			: $("#SIZES").val() ,
				_token			: '{{ csrf_token() }}'
			}).success(function(xhr){

				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("#SUM_QTY").text(data.data.SUM_QTY);


			}).error(function(xhr,status, response) {
				$("#SUM_QTY").text("오류...");
			});
		}

		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$(".slideSearchCust").slideUp();
			$(".modal input[type='text']").val("");
			$(".modal input[type='radio']:nth-child(1)").prop("checked", true);
			$(".modal textarea").val("");
			$(".modal select option:eq(0)").prop("selected", "selected");

		}
		// 거래처 검색 닫기
		$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });

		$("#btnLog").click(function(){
			location.href = "/stock/stock/log/" + $("#PIS_MK").val() + "/" + $("#SIZES").val();
		});

		// Pdf 출력
		$("#btnPdf").click(function(){

			var width=740;
			var height=720;
			var pis_mk		= "{{Request::segment(4)}}";
			var sizes		= "{{Request::segment(5)}}";
			var input_date	= $("#reportrange input[name='start_date']").val();
			var output_date	= $("#reportrange input[name='end_date']").val();
			var condition	= $("select[name='srtCondition'] option:selected").val();
			
			var search_text	= $("input[name='textSearch']").val() == "" ? "none" : $("input[name='textSearch']").val();
			var srt_output	= $("input[name='srtRadio']:checked").attr("id");

			var url = "/stock/stock/Pdf/" + pis_mk + "/" + sizes + "/" + input_date + "/" + output_date + "/" + condition + "/" + search_text + "/" + srt_output;
				console.log(url);
			getPopUp(url , width, height);

		});
	});

</script>
<!-- 재고수정/삭제 -->
<div class="modal fade" id="modal_sujo_stock_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 재고 수정/삭제 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert">
					<ul>
						<li>재고수정의 경우 선택한 <span class='bold red'>수조번호</span>를 기준으로 재고량 변동이 있습니다.</li>
						<li>수조가 <span class='bold red'>없음</span>으로 표기된 경우는 현재 재고가 수조에 존재 하지 않는다는 표시이므로 <br/>수정 후 재고량은 변동 되지만 수조의 수량이 변동되는 현상이 없으므로 수정을 하셔도 됩니다. </li>
					</ul>
				</div>
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblStockUpdList" class="table table-bordered table-hover display nowrap" summary="재고 수정/삭제 목록">
								<caption>재고 수정/삭제 목록</caption>
								<thead>
									<tr>
										<th class="subject" width="60px">수조</th>
										<th class="name" width="60px">어종명</th>
										<th class="name" width="30px">규격</th>
										<th class="name" width="30px">원산지</th>
										<th class="name" width="50px">입고일</th>
										<th class="name" width="30px">중량</th>
										<th class="name" width="auto">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 입고수정-->
<div class="modal fade" id="modal_sujo_input_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 입고수정 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<input type="hidden" name="SEQ" id="SELECTED_SEQ" />
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblSujoUpdList" class="table table-bordered table-hover display nowrap" summary="거래처 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th class="subject" width="80px">업체명</th>
										<th class="name" width="50px">입고</th>
										<th class="name" width="50px">매입금액</th>
										<th class="name" width="50px">미지급</th>
										<th class="name" width="50px">적요</th>
										<th class="name" width="auto">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 재고정리-->
<div class="modal fade" id="modal_stock_arrange" role="dialog">
	<div class="modal-dialog dialog-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 재고정리 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<!--<form id="form_sujo">-->
					<div class="form-group">
						<label for="modal_stock_sujo_no"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
						<input type="text" class="form-control" id="modal_stock_sujo_no" readonly="readonly" />
					</div>

					<div class="form-group">
						<label for="modal_stock_pis_mk"><i class='fa fa-check-circle text-red'></i> 어종 / 규격</label>
						<input type="hidden" class="form-control" id="modal_stock_pis_mk" />
						<input type="text" class="form-control" id="modal_stock_pis_nm" readonly="readonly" />
						<input type="text" class="form-control" id="modal_stock_sizes" readonly="readonly" />
					</div>

					<div class="form-group">
						<label for="modal_stock_origin_nm"><i class='fa fa-check-circle text-red'></i> 원산지</label>
						<input type="hidden" class="form-control" id="modal_stock_origin_cd" readonly="readonly" />
						<input type="text" class="form-control" id="modal_stock_origin_nm" readonly="readonly" />
					</div>

					<div class="form-group">
						<label for="modal_stock_input_date"><i class='fa fa-check-circle text-red'></i> 입고일</label>
						<input type="text" class="form-control" id="modal_stock_input_date" readonly="readonly" />

						<label for="modal_stock_output_date"><i class='fa fa-check-circle text-red'></i> 재고출고일</label>
						<input type="text" class="form-control" id="modal_stock_output_date"  />
					</div>

					<div class="form-group">
						<label for="modal_stock_qty"><i class='fa fa-check-circle text-red'></i> 현 재고 중량</label>
						<input type="text" class="form-control" id="modal_stock_qty" readonly="readonly"  />

						<label for="modal_stock_qty_tobe"><i class='fa fa-check-circle text-red'></i> 재고정리 중량</label>
						<input type="text" class="form-control numeric" id="modal_stock_qty_tobe" />
					</div>


					<div class="form-group">
						<label for="modal_stock_cust_mk"><i class='fa fa-check-circle text-aqua'></i> 거래처</label>
						<input type="text" class="form-control" id="modal_stock_cust_mk" placeholder="거래처코드" readonly="readonly">
						<input type="text" class="form-control" id="modal_stock_cust_nm" placeholder="거래처명" readonly="readonly">
						<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
							<i class="fa fa-search"></i>
						</button>

						<div class="form-group slideSearchCust">
							<div class="box box-primary">
								<h5><i class='fa fa-check-circle text-aqua'></i> 거래처 선택</h5>

								<label for="strSearchCust"><i class='fa fa-check-circle text-aqua'></i> 거래처 상호</label>
								<input type="text" class="form-control" name="strSearchCust" id="strSearchCust" />

								<button type="button" class="btn btn-danger  pull-right btn_CustClose">
									<span class="glyphicon glyphicon-remove"></span> 닫기
								</button>
								<div class="box-body">
									<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
										<caption>어종 조회</caption>
										<thead>
											<tr>
												<th class="check">코드</th>
												<th class="subject">상호</th>
												<th class="name">대표자</th>
												<th class="name">전화번호</th>
												<th class="name">선택</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group reason">
						<label for="modal_stock_reason"><i class='fa fa-check-circle text-aqua'></i> 재고정리사유</label>
						<label class='sel'><input type="radio" name="modal_stock_reason" class="minimal" value="1" >감량</label>
						<label class='sel'><input type="radio" name="modal_stock_reason" class="minimal" value="2" >폐사</label>
						<label class='sel'><input type="radio" name="modal_stock_reason" class="minimal" value="3" > 증량</label>
						<label class='sel'><input type="radio" name="modal_stock_reason" class="minimal" value="99" checked> 기타</label>
					</div>

					<div class="form-group">
						<label for="modal_sujo_outline"><i class='fa fa-check-circle text-aqua'></i> 적요</label>
						<textarea  class="form-control textarea" style="width:70%;" id="modal_sujo_outline" placeholder="적요입력" ></textarea>
					</div>


				<!--</form>-->

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>

				<button type="button" class="btn btn-success  pull-right" id="btnSujoStockOut">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>

@stop
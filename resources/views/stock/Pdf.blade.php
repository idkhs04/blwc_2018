<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>상세재고조회</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}

			}

			@page {
				counter-increment: page;
				counter-reset: page 1;

			}
			footer{
				position: fixed; bottom:1px; text-align:right;

			}

			.page-number:before  {
				 content: "Page " counter(page) " of " counter(pages);
				content: counter(page);
			}

		</style>


	</head>
	<body>
		<script type="text/php">

		if ( isset($pdf) ) {

			$size = 10;
			$color = array(0,0,0);
			if (class_exists('Font_Metrics')) {
				$font = Font_Metrics::get_font("NanumGothic");
				$text_height = Font_Metrics::get_font_height($font, $size);
				$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
			} elseif (class_exists('Dompdf\\FontMetrics')) {
				$font = $fontMetrics->getFont("NanumGothic");
				$text_height = $fontMetrics->getFontHeight($font, $size);
				$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
			}

			$foot = $pdf->open_object();

			$w = $pdf->get_width();
			$h = $pdf->get_height();

			// Draw a line along the bottom
			$y = $h - $text_height - 24;
			$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

			$pdf->close_object();
			$pdf->add_object($foot, "all");

			$text = "{PAGE_COUNT} - {PAGE_NUM}";  

			// Center the text
			$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

		}
		</script>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<span class="title" style='text-align:center;font-size:23px;display:block;'>{{ $title }}</span>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
						</tr>
					</table>
					<table id="tblHeader" summary="기간별 어종별 판매조회" style="border:none;width:100%">
						<tr>
							<td style="text-align:left;width:49%;">거래기간 : {{$start}} ~ {{$end }}</td>
							<td style="text-align:right;width:49%;">현재고 : {{ number_format($qty, 2) }}  Kg</td>
						</tr>
					</table>

					<table id="tblList" summary="어종별 재고조회" width="100%" style="border-bottom:3px solid block;">
						<caption>재고조회 상세</caption>
						<thead style="border-left:none;border-right:none;border-top:3px solid block; border-bottom:3px solid block;padding-top:5px;">
							<tr>
								<td style='text-align:center;'>거래업체</td>
								<td style='text-align:center;' width="80px;">입고일</td>
								<td style='text-align:center;' width="80px;">출고일</th>
								<td style='text-align:center;' width="80px;">매입(Kg)</th>
								<td style='text-align:center;' width="80px;">매출(Kg)</th>
								<td style='text-align:center;' width="80px;">현재고(Kg)</th>
								
								<td style='text-align:center;' width="80px;">판매단가</th>
								<td style='text-align:center;' width="80px;">판매이익</th>

								<td style='text-align:center;' width="auto">비고</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($model as $key => $item)
							<tr>
								<td>{{ $item->FRNM }}</td>
								<td style='text-align:center;'>{{ $item->INPUT_DATE }}</td>
								<td style='text-align:center;'>{{ $item->OUTPUT_DATE }}</td>
								<td style='text-align:right;'>{{ number_format($item->IN_QTY, 2) }}</td>
								<td style='text-align:right;'>{{ number_format($item->OUT_QTY, 2) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->CURR_QTY, 2)}}</td>
								
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->SALE_UNCS, 0)}}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->PROFIT, 0)}}</td>

								<td style='text-align:left;padding-left:5px;'>{{ $item->OUTLINE}}</td>
							</tr>
							@endforeach
							<tr>
								<td colspan="3" style='text-align:center;'>합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계</td>
								<td style='text-align:right;'>{{ number_format($qtyInputAll, 2) }}</td>
								<td style='text-align:right;'>{{ number_format($qtySaleAll, 2) }}</td>

								
								<td colspan="2"></td>
								<td style='text-align:right;'> {{ number_format($qtyPROFITAll, 0)}}</td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>





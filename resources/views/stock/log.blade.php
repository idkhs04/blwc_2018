@extends('layouts.main')
@section('class','매입관리')
@section('title','재고 로그')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblListStock_filter{ display:none;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}

	.tdetailLog{
		width:45%;
		margin:2%;
		padding:3%;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-5 col-xs-12">
				<div class="btn-group-vertical">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
				</div>
			</div>
		</div>
	</div>
</div>
			
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblListStock" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="재고 목록 로그">
				<caption>재고관리 로그</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="60px">기록일</th>  
						<th width="50px">구분</th>  
						<th width="60px">이전수량</th>  
						<th width="100px">이전입고금액</th>  
						<th width="100px">이전미수금</th>  
						<th width="100px">현재수량</th>
						<th width="100px">현재입고금액</th>  
						<th width="100px">현재미수금</th>  
						<th width="auto"></th>  
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		
		$("#btnList").click(function(){
			location.href="/stock/stock/index";
		});

		var sTable = $('#tblListStock').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			ajax: {
				url: "/stock/stock/listLog",
				data:function(d){
					d.PIS_MK	= "{{$PIS_MK}}";
					d.SIZES		= "{{$SIZES}}";
				},
			},
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center" },
				{ data: 'LOG_DIV', name: 'LOG_DIV' , className: "dt-body-center" },
				{ 
					data: 'BEFORE_QTY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'BEFORE_REAL_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'BEFORE_UNPROV', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'QTY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'REAL_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'UNPROV', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					"className":      '',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblListStock tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			
			var row = sTable.row( tr );
			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var d = row.data();
				var listHtml = "";
				$.getJSON('/stock/stock/detailLog', {
					PIS_MK		: "{{$PIS_MK}}",
					SIZES		: "{{$SIZES}}",
					SRL_NO		: d.SRL_NO,
					SEQ			: d.SEQ,

					}).success(function(xhr){
						
						$(".detailLog td").text("");
						var data = jQuery.parseJSON(JSON.stringify(xhr.result));

						$(".tblBefore tr:eq(0) > td").text(data.BEFORE_INPUT_DATE);
						$(".tblBefore tr:eq(1) > td").text(data.BEFORE_OUTPUT_DATE);
						$(".tblBefore tr:eq(2) > td").text(data.BEFORE_ORIGIN_NM);
						$(".tblBefore tr:eq(3) > td").text(data.BEFORE_FRNM);
						$(".tblBefore tr:eq(4) > td").text(data.BEFORE_QTY);
						$(".tblBefore tr:eq(5) > td").text(data.BEFORE_UNCS);
						$(".tblBefore tr:eq(6) > td").text(data.BEFORE_INPUT_DISCOUNT);
						$(".tblBefore tr:eq(7) > td").text(data.BEFORE_INPUT_AMT);
						$(".tblBefore tr:eq(8) > td").text(data.BEFORE_REAL_AMT);
						$(".tblBefore tr:eq(9) > td").text(data.BEFORE_UNPROV);
						$(".tblBefore tr:eq(10) > td").text(data.CASH);
						$(".tblBefore tr:eq(11) > td").text(data.BEFORE_OUTLINE);

						$(".tblAfter tr:eq(0) > td").text(data.INPUT_DATE);
						$(".tblAfter tr:eq(1) > td").text(data.OUTPUT_DATE);
						$(".tblAfter tr:eq(2) > td").text(data.ORIGIN_NM);
						$(".tblAfter tr:eq(3) > td").text(data.FRNM);
						$(".tblAfter tr:eq(4) > td").text(data.QTY);
						$(".tblAfter tr:eq(5) > td").text(data.UNCS);
						$(".tblAfter tr:eq(6) > td").text(data.INPUT_DISCOUNT);
						$(".tblAfter tr:eq(7) > td").text(data.INPUT_AMT);
						$(".tblAfter tr:eq(8) > td").text(data.REAL_AMT);
						$(".tblAfter tr:eq(9) > td").text(data.UNPROV);
						$(".tblAfter tr:eq(10) > td").text(data.CASH);
						$(".tblAfter tr:eq(11) > td").text(data.OUTLINE);

						
						row.child($(".detailLog").html()).show();
						tr.addClass('shown');

				}).error(function(xhr){
					alert('error');
				});
			}
		});
	});

</script>
<div class="detailLog" style="display:none;">
	<table class="tblBefore tdetailLog" style="width:45%;float:left;">
		<colgroup>
			<col/>
			<col/>
		</colgroup>
		<tr>
			<th>입고일</th>
			<td class='bInputDate'>입고일</td>
		</tr>
		<tr>
			<th>출고일</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>원산지</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>거래업체</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>중량</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고단가</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>할인금액</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고금액</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고금액(할일적용)</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>미지급금</th>
			<td class='bUnprov'></td>
		</tr>
		<tr>
			<th>현금</th>
			<td class='bCash'></td>
		</tr>
		<tr>
			<th>비고</th>
			<td class='bRemark'>입고일</td>
		</tr>
	</table>
	
	<table class="tblAfter tdetailLog" style="width:45%">
		<colgroup>
			<col/>
			<col/>
		</colgroup>
		<tr>
			<th>입고일</th>
			<td class='bInputDate'>입고일</td>
		</tr>
		<tr>
			<th>출고일</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>원산지</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>거래업체</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>중량</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고단가</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>할인금액</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고금액</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>입고금액(할일적용)</th>
			<td class='bOutputDate'></td>
		</tr>
		<tr>
			<th>미지급금</th>
			<td class='bUnprov'></td>
		</tr>
		<tr>
			<th>현금</th>
			<td class='bCash'></td>
		</tr>
		<tr>
			<th>비고</th>
			<td class='bRemark'>입고일</td>
		</tr>
	</table>
</div>

@stop
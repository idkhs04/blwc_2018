@extends('layouts.main')
@section('class','매입관리')
@section('title','일일입고 관리')
@section('content')
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ vertical-align:baseline; margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblCustList_filter, #tblStockUpdList_filter { display:none;}

	#modal_stock_arrange label{
		display:inline-block;
		width:22%;
		clear:both;
		padding-left:1%;
	}

	#modal_stock_arrange .reason label{
		width:auto;
		padding-left:2%;
	}


	#modal_stock_arrange .form-control{
		display:inline-block;
		width:22%;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust, #tblList_length{
		display:none;
	}

	#tblList_Sum{
		float:right;
		font-weight:bold;
	}

	#SUM_QTY{ padding-left:2px; border:none; color:blue; }

	.btnSelect{ margin-right:10px;}

	.input-group.dt-body-right{ text-align:right;}
	.dt-body-right{ text-align:right;}
	#pdf{display:none;}

	.red{color:red;}
	.blue{color:blue;}
	.green{color:green;}

	/*.dataTables_length{ display:none;}*/
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="error_message"><ul></ul></div>

			<div class="col-md-1 col-xs-5">
				<div class="btn-group">
					<!--
					<button type="button" class="btn btn-danger" id="btnDelete" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnUpdate" ><i class="fa fa-edit"></i> 재고수정</button>
					-->
					<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력</button>
				</div>
			</div>

			<div class="col-md-3 col-xs-7">
				<div class="btn-group">
					<label>
						<input type="radio" name="srtRadio" class="srtRadio" id="srt_input" checked />
						입고일별
					</label>
				</div>
				<div id="reportrange" class="btn-group" style="background: #fff; cursor: pointer; padding: 1px 5px; border: 1px solid #ccc;">
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control cis-lang-ko"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			
			<table id="tblList" class="responsive table table-bordered table-hover display nowrap" summary="상세재고 목록" width="100%">
				<caption>상세재고조회</caption>
				<thead>
					<tr>
						<th class="check" width="60px">관리</th>
						<th class="name" width="60px" >어종</th>
						<th class="name" width="60px" >원산지</th>
						<th class="name" width="30px">수조</th>
						<th class="name" width="100px">거래처</th>
						<th class="name" width="60px">입고일</th>
						
						<th class="name" width="30px">입고</th>
						<th class="name" width="60px">입고단가</th>
						<th class="name" width="30px">합계</th>

						<th class="name" width="80px">적요</th>
						<th class="name" width="80px">수정일시</th>
						<th class="name" width="50px">바로가기</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		var start = moment();
		var end = moment();

		$("#btnLog").attr("href", "javascript:;")
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}
		// 입고정보 : 입고일
		$("#modal_sujo_input_date, #modal_stock_output_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			opens:"left",
			cancelClass: "",
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			order : [[5, "desc"], [4, "asc"]],
			bInfo : false,
			ajax: {
				url: "/stock/stock/listDataPisData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					data: "PIS_MK",
					name: "ST.PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {

							if( row.OUTPUT_DATE === null && row.CUST_MK != null){
							//return "<input type='checkbox' class='editor-active' value='" + data +"' >";
							return	"<button type='button' class='btn btn-success btnUpdate' data-val='" + row.SEQ +"'> 수정</button>" +
									" <button type='button' class='btn btn-danger btnDelete' data-val='" + row.SEQ +"'> 삭제</button>" ;
							}else{
								return '';
							}
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: "PIS_NM",
					name: "PIS_INFO.PIS_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.PIS_NM + " <span class='sizes'>" + row.SIZES + "</span>";
						}
						return data;
					},
					className: "dt-body-left blue bold"
				},
				{
					data:   "ORIGIN_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span class='bold'>" + data  + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'SUJO_NO', name: 'ST.SUJO_NO', className: "dt-body-center"},
				
				{ data: 'IN_FRNM', name: 'C.FRNM' },
				{ data: 'INPUT_DATE', name: 'ST.INPUT_DATE', className: "dt-body-center" },
				
				{
					data: 'IN_QTY',
					name: 'ST.QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{
					data: 'UNCS',
					name: 'ST.UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'INPUT_AMT',
					name: 'ST.INPUT_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{
					data: 'OUTLINE',
					name: 'ST.OUTLINE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					data: 'UPDATE_DATE',
					name: 'ST.UPDATE_DATE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					data:  "SUJO_NO",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return	"<button class='btnStock btn btn-success'><i class='fa fa-arrow-circle-right'></i> 상세재고 </button>" + 
									" <button class='btnUnprov btn btn-success'><i class='fa fa-arrow-circle-right'></i> 미지급금 </button>";
						}
						return data;
					},
					orderable:  false,
					className: "dt-body-left"
				},
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},

			],
			footerCallback: function ( row, data, start, end, display ) {
				
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총입고
				input_stock= api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 6 ).footer() ).html(Number(input_stock.toFixed(2)).format());
				// 합계
				input_sum= api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 8 ).footer() ).html(Number(input_sum.toFixed(2)).format());
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ location.href="/stock/stock/index?page=1"; });

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 상세재고 바로가기
		$('#tblList tbody').on( 'click', '.btnStock', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/stock/stock/detail/" + data.PIS_MK + "/" + data.SIZES;
		});

		// 미지급금 바로가기
		$('#tblList tbody').on( 'click', '.btnUnprov', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/unprov/unprov/detail2/" + data.CUST_MK;
		});

		// 입고일/출고일 선택 초기화
		function setConditionDefault(){
			if( $("#srt_input").is(":checked")) {
				//$("#reportrange > label").text("입고일");
			}else{
				//$("#reportrange > label").text("출고일");
			}
			oTable.draw() ;
		}
		
		// 재고 수정/삭제 클릭
		$("#tblList tbody").on("click", ".btnUpdate, .btnDelete", function(){

			//if( chkInputTble("tblList", "재고수정") ){

				var selectedSEQ = $(this).attr("data-val"); 
				var rowData = oTable.row( $("#tblList tbody tr > td > button[data-val='" + selectedSEQ + "']").parents('tr') ).data();

				
				//return false;
				
				// var cTable = $('#tblStockUpdList').DataTable();
				//cTable.destroy();
				// dataTable을 여러번 새롭게 호출하기위해서 임시로 생성후 삭제

				if ( $.fn.DataTable.isDataTable('#tblStockUpdList') ) {
					$('#tblStockUpdList').DataTable().destroy();
					$('#tblStockUpdList tbody').off( 'click', 'button');
				}
				$('#tblStockUpdList tbody').empty();

				var cTable = $('#tblStockUpdList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					destroy:true,
					bFilter: false,
					bInfo: false,
					bLengthChange: false,
					ajax: {
						url: "/stock/stock/getStockDetailUpdateList",
						data:function(d){
							d.pis_mk		= rowData.PIS_MK;
							d.sizes			= rowData.SIZES;
							d.origin_cd		= rowData.ORIGIN_CD;
							d.origin_nm		= rowData.ORIGIN_NM;
							d.input_date	= rowData.INPUT_DATE;
							d.sujo_no		= rowData.SUJO_NO;
							d.cust_mk		= rowData.CUST_MK;
							d.option		= "1";
							d.qty			= rowData.IN_QTY;
							d.seq			= rowData.SEQ;
							// 재고정리일때만..option발동
						}
					},
					columns: [
						{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-center red bold" },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'SIZES',  name: 'SIZES' },
						{ data: 'ORIGIN_NM',  name: 'ORIGIN_NM', className: "dt-body-center" },
						{ data: 'INPUT_DATE',  name: 'INPUT_DATE', className: "dt-body-center" },
						{
							data: 'QTY',
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return Number(data).format();
								}
								return data;
							},
							className: "dt-body-right"
						},
						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

				$("#modal_sujo_stock_update").modal("show");

				var isUpdate = $(this).hasClass("btnUpdate") ? true : false;

				$('#tblStockUpdList tbody').on( 'click', 'button', function () {

					var data = cTable.row( $(this).parents('tr') ).data();
					
					// 재고수정 이동
					if( isUpdate ){
						location.href = "/stock/stock/detailEdit/" + data.PIS_MK + "/" + data.SIZES + "/" + selectedSEQ + "/" + data.SUJO_NO ;
					}else{
						// 삭제
						if( confirm('선택된 재고를 삭제 하시겠습니까?')){
							var rowData = data;
							
							$.getJSON('/stock/stock/removeStockInSujo', {
								'SUJO_NO'		: data.SUJO_NO,
								'PIS_MK'		: rowData.PIS_MK ,
								'SIZES'			: rowData.SIZES ,
								'SEQ'			: selectedSEQ,
								_token			: '{{ csrf_token() }}'
							}).success(function(xhr){
								var data = jQuery.parseJSON(JSON.stringify(xhr));

								var info = $('#modal_sujo_stock_update .edit_alert');
								info.hide().find('ul').empty();
								info.slideDown();
								$("#modal_sujo_stock_update").modal("hide");
								oTable.draw() ;
								getSumQty();

							}).error(function(xhr,status, response) {
								var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
								var info = $('#modal_sujo_stock_update .edit_alert');
								info.hide().find('ul').empty();
									for(var k in error.message){
										if(error.message.hasOwnProperty(k)){
											error.message[k].forEach(function(val){
												info.find('ul').append('<li>' + val + '</li>');
											});
										}
									}
								info.slideDown();
							});
						}
					}
				});
			//}
		});

		$("#btnLog").click(function(){
			location.href = "/stock/stock/log/" + $("#PIS_MK").val() + "/" + $("#SIZES").val();
		});
	});


</script>
<!-- 재고수정/삭제 -->
<div class="modal fade" id="modal_sujo_stock_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 재고 수정/삭제 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert">
					<ul>
						<li>재고수정의 경우 선택한 <span class='bold red'>수조번호</span>를 기준으로 재고량 변동이 있습니다.</li>
						<li>수조가 <span class='bold red'>없음</span>으로 표기된 경우는 현재 재고가 수조에 존재 하지 않는다는 표시이므로 <br/>수정 후 재고량은 변동 되지만 수조의 수량이 변동되는 현상이 없으므로 수정을 하셔도 됩니다. </li>
					</ul>
				</div>
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblStockUpdList" class="table table-bordered table-hover display nowrap" summary="재고 수정/삭제 목록">
								<caption>재고 수정/삭제 목록</caption>
								<thead>
									<tr>
										<th class="subject" width="60px">수조</th>
										<th class="name" width="60px">어종명</th>
										<th class="name" width="30px">규격</th>
										<th class="name" width="30px">원산지</th>
										<th class="name" width="50px">입고일</th>
										<th class="name" width="30px">중량</th>
										<th class="name" width="auto">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

@stop
@extends('layouts.main')
@section('class','매입/매출관리')
@section('title','전체 입출고조회')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ vertical-align:baseline; margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblCustList_filter, #tblStockUpdList_filter { display:none;}

	#modal_stock_arrange label{
		display:inline-block;
		width:22%;
		clear:both;
		padding-left:1%;
	}

	#modal_stock_arrange .reason label{
		width:auto;
		padding-left:2%;
	}

	#modal_stock_arrange .form-control{
		display:inline-block;
		width:22%;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust, #tblList_length{
		display:none;
	}

	#tblList_Sum{
		float:right;
		font-weight:bold;
	}

	#SUM_QTY{ padding-left:2px; border:none; color:blue; }

	.btnSelect{ margin-right:10px;}

	.input-group.dt-body-right{ text-align:right;}
	.dt-body-right{ text-align:right;}
	#pdf{display:none;}

	.red{color:red;}
	.blue{color:blue;}
	.green{color:green;}

	.sm-font{ font-size:11px;}

	#tblList tbody .btn {
		padding: 0px 4px;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력</button>
					<button type="button" class="btn btn-success" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<label>
						<input type="radio" name="srtRadio" class="srtRadio" id="srt_input"  checked />
						입고
					</label>
					<label>
						<input type="radio" name="srtRadio" class="srtRadio" id="srt_output" />
						출고
					</label>
					
				</div>
				<div id="reportrange" class="btn-group" style="background: #fff; cursor: pointer; padding: 1px 5px; border: 1px solid #ccc; width:170px">
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="SUJO_NO">수조번호</option>
					<option value="FRMN">거래처명</option>
					<option value="PIS_NM">어종명</option>
					<option value="QTY">출고</option>
					<option value="UNCS">단가</option>
					<option value="OUTLINE">적요</option>
				</select>
			</div>

			<div class="col-md-2 col-xs-6">
				<div class="input-group dt-body-right">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<input type="hidden" id="hSujo_no" name="hSujo_no" />
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			
			<table id="tblList" class="responsive table table-bordered table-hover display nowrap" summary="상세재고 목록" width="100%">
				<caption>상세재고조회</caption>
				<thead>
					<tr>
						<th class="check" width="30px">관리</th>
						<th class="name" width="60px" >어종</th>
						<th class="name" width="60px" >규격</th>
						<th class="name" width="60px" >원산지</th>
						<th class="name" width="30px">수조</th>
						<th class="name" width="100px">거래처</th>
						<th class="name" width="60px">입고일</th>
						<th class="name" width="60px">입고단가</th>
						<th class="name" width="60px">출고일</th>
						<th class="name" width="60px">재고정리</th>
						<th class="name" width="30px">입고</th>
						<th class="name" width="30px">출고</th>
						<th class="name" width="60px">판매단가</th>
						<th class="name" width="80px">판매이익</th>
						<th width="80px">입출고 합계</th>
						<th class="name" width="auto">적요</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>	
				</tfoot>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		var start = moment().subtract(0, 'days');
		var end = moment();

		$("#btnLog").attr("href", "javascript:;")
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}
		// 입고정보 : 입고일
		$("#modal_sujo_input_date, #modal_stock_output_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			opens:"left",
			cancelClass: "",
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);


		var origin_in_date	= '2000-01-01';
		var out_Qty			= 0.0;

		// 입고날짜를 기준으로 입력된 순번으로 => 출고날짜 순으로 보이도록 조회
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			order : [[1, "asc"],[2, "desc"], [3, "desc"],[6, "desc"], [0, "desc"], [8, "desc"]],
			bInfo : false,
			ajax: {
				url: "/stock/stock/detailData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.pis_mk		= $("input[name='PIS_MK']").val();
					d.sizes			= $("input[name='SIZES']").val();
					d.inOutDate		= $("input[name='srtRadio']:checked").attr("id");
				}
			},
			columns: [
				{
					data: "SEQ",
					name: "ST.SEQ",
					render: function ( data, type, row ) {
					
						return	"<button type='button' class='btn btn-success btnGoDetail' data-val='" + data +"'> 상세</button>";
					},
					className: "dt-body-center"
				},
				{ data: 'PIS_NM', name: 'PIS_INFO.PIS_NM', className: "dt-body-center"},
				{ data: 'SIZES', name: 'ST.SIZES', className: "dt-body-center"},
				{ data: 'ORIGIN_NM', name: 'ST.ORIGIN_NM', className: "dt-body-center"},
				{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-center"},
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'INPUT_DATE', name: 'ST.INPUT_DATE', className: "dt-body-center" },
				{
					data: 'UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'OUTPUT_DATE', name: 'OUTPUT_DATE', className: "dt-body-center" },
				{ 
					data: 'REASON_NM', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== ""){
								return data;
							}else{
								if( row.OUTLINE != "" && row.OUTLINE !== null && ( row.OUTLINE.includes('이고') || row.OUTLINE.includes('출고') )){
									return "<span class='sm-font'>" + row.OUTLINE.substr(0, 6) + "</span>";
								}
							}
						}
						return data;
					},
					className: "dt-body-center" 
				},
				{
					data: 'IN_QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{
					data: 'OUT_QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{
					data: 'SALE_UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right green"
				},
				{
					data: 'PROFIT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return  ( row.SALE_UNCS - row.UNCS ) * (row.OUT_QTY );
					},
					className: "dt-body-right"
				},
				{
					"className":      'dt-body-left sumOutQty',
					"orderable":      false,
				},
				{
					data: 'OUTLINE',
					name: 'ST.OUTLINE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return  data;
					},
					className: "dt-body-left"
				},
				{ data: 'SEQ',	 name: 'ST.SEQ' , visible : false},
				{ data: 'ORIGIN_NM',	 name: 'ORIGIN_NM', visible : false },
			],
			fnRowCallback: function(nRow, nData){
				
				//console.log('loop');
				//초기값
				if( ( origin_in_date == '2000-01-01' || origin_in_date == nData.INPUT_DATE ) && ( nData.OUTPUT_DATE != null && nData.OUTPUT_DATE != "null" )){
					
					origin_in_date	=  nData.INPUT_DATE; 
					out_Qty			+= $.isNumeric(nData.OUT_QTY) ? parseFloat(nData.OUT_QTY) : 0; 
					
				}else if ( ( nData.OUTPUT_DATE == "null" || nData.OUTPUT_DATE == null ) && ( nData.OUT_QTY == 0 || nData.OUT_QTY == null) ){
					
					$(nRow).find(".dt-body-left.sumOutQty").html("소합계 : " + "<span class='red'>" + nData.IN_QTY + "</span> / <span class='blue'>" +  out_Qty.toFixed(1) + "</span>") ;
					origin_in_date = '2000-01-01';
					out_Qty = 0;
				}else{
					out_Qty = 0;
				}
			},
			fnDrawCallback: function () {
				out_Qty = 0;
				origin_in_date = '2000-01-01';

			},
			footerCallback: function ( row, data, start, end, display ) {
				
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총입고
				input_stock= api.column( 10 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 10 ).footer() ).html(Number(input_stock.toFixed(2)).format());

				// 총출고
				output_stock = api.column( 11 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 11).footer() ).html(Number(output_stock.toFixed(2)).format());

				// 판매이익
				profit = api.column(13 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column(13).footer() ).html(Number(profit.toFixed(2)).format());
				

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ location.href="/buy/sujo/index?page=1"; });

		// 입고일/출고일 선택 초기화
		function setConditionDefault(){
			if( $("#srt_input").is(":checked")) {
				//$("#reportrange > label").text("입고일");
			}else{
				//$("#reportrange > label").text("출고일");
			}
			oTable.draw() ;
		}
		$(".srtRadio").click(setConditionDefault);

	
		// 상세재고 이동
		$("body").on("click", ".btnGoDetail", function(){
			var rowData = oTable.row( $("#tblList tbody tr > td > button").parents('tr') ).data();
			var PIS_MK = rowData.PIS_MK;
			var SIZES = rowData.SIZES;

			location.href="/stock/stock/detail/" + PIS_MK + "/" + SIZES

		});

		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$(".slideSearchCust").slideUp();
			$(".modal input[type='text']").val("");
			$(".modal input[type='radio']:nth-child(1)").prop("checked", true);
			$(".modal textarea").val("");
			$(".modal select option:eq(0)").prop("selected", "selected");

		}

		$("#btnLog").click(function(){
			location.href = "/stock/stock/log/" + $("#PIS_MK").val() + "/" + $("#SIZES").val();
		});

		// Pdf 출력
		$("#btnPdf").click(function(){

			var width=740;
			var height=720;
			var pis_mk		= "{{Request::segment(4)}}";
			var sizes		= "{{Request::segment(5)}}";
			var input_date	= $("#reportrange input[name='start_date']").val();
			var output_date	= $("#reportrange input[name='end_date']").val();
			var condition	= $("select[name='srtCondition'] option:selected").val();
			
			var search_text	= $("input[name='textSearch']").val() == "" ? "none" : $("input[name='textSearch']").val();
			var srt_output	= $("input[name='srtRadio']:checked").attr("id");

			var url = "/stock/stock/Pdf/" + pis_mk + "/" + sizes + "/" + input_date + "/" + output_date + "/" + condition + "/" + search_text + "/" + srt_output;
				console.log(url);
			getPopUp(url , width, height);

		});
	});

</script>
@stop
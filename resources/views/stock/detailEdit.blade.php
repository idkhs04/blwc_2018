@extends('layouts.main')
@section('title','입고수정')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	#tblCustList_filter, #tblSujoList_filter { display:none;}
	.btn-info{
		vertical-align:baseline;
		margin-top:0px;
	}
	.sum_valid{ text-align:right;}

	#strSearchCust, #strSearchSujo { width:100px; padding-left:10px;margin-left:5px;}

	.modal label { margin-top:5px; margin-right:10px;}

	#strSearchCust , #strSearchSujo{ display:inline;}

</style>

<div class="col-sm-12">
	
	<div class="box box-info">
		<div class="modal-body"></div>
		<!--
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>경고! 데이터 오류입니다!</strong>
			<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		-->
		{{  Form::open(['url'=>'/stock/'.$stock."/". $mode = Request::segment(3) == "detailEdit" ? "setUpdateStock": "Insert", 'method' => 'POST', 'enctype' => "multipart/form-data", 'class'=>'form-horizontal', 'id'=>"frmEdit" , 'onsubmit' => 'return validateForm()' ]) }}
		<input type="hidden" name="new_sujo_no" />
		<!--<input type="hidden" name="OLD_SUJO_NO" value="{{$model->SUJO_NO}}"/>-->
		<input type="hidden" name="mode" value="{{ Request::segment(3)}}" />
		<input type="hidden" name="page" value="{{ app('request')->input('page') == '' ? 1 : app('request')->input('page') }}" />
		<input type="hidden" name="error" value="{{ count($errors)}}" />
		<input type="hidden" name="SEQ" value="{{ $model->SEQ }}" />
		<input type="hidden" name="PIS_MK" value="{{$model->PIS_MK}}" />
			<div class="box-body">
				<div class="form-group">
					<label for="SUJO_NO" class="col-sm-2 control-label"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="SUJO_NO" placeholder="" name="SUJO_NO" value="{{$sujo_no}}" readonly="readonly" />
					</div>
					<!--
					<div class="col-sm-2">
						<button type="button" name="search" id="btnSearchSujo" class="btn btn-info">수조검색</button>
					</div>
					-->
				</div>
				<div class="form-group">
					<label for="PIS_NM" class="col-sm-2 control-label"><i class='fa fa-check-circle text-red'></i> 어종</label>
					<div class="col-sm-2">
						
						<input type="text" class="form-control " id="CUST_GRP_NM" placeholder="" name="CUST_GRP_NM" value="{{$model->PIS_NM}}" readonly="readonly" />
						@if ($errors->has('PIS_NM')) 
							<p class="help-block">{{ $errors->first('PIS_NM') }}</p> 
						@endif
					</div>
					<div class="col-sm-2">
						
						<input type="text" class="form-control" id="SIZES" placeholder="" name="SIZES" value="{{$model->SIZES}}" readonly="readonly" >
						@if ($errors->has('SIZES')) <p class="help-block">{{ $errors->first('SIZES') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="ORIGIN_CD" class="col-sm-2 control-label"><i class='fa fa-check-circle text-red'></i> 원산지</label>
					<div class="col-sm-2">
						{!! Form::select('ORIGIN_CD',  (['' => '== 선 택 =='] + $modelOrgin ),  $model->ORIGIN_CD, ['class' => 'form-control', 'readonly'=> 'readonly', 'disabled'=>'disabled' ]) !!}
						<input type='hidden' name='ORIGIN_NM' value="{{$model->ORIGIN_NM}}" />
						@if ($errors->has('ORIGIN_NM')) <p class="help-block">{{ $errors->first('ORIGIN_NM') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="INPUT_DATE" class="col-sm-2 control-label"><i class='fa fa-check-circle text-red'></i> 입고일</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="INPUT_DATE" placeholder="" name="INPUT_DATE" value="{{$model->INPUT_DATE}}" readonly='readonly' />
						@if ($errors->has('INPUT_DATE')) <p class="help-block">{{ $errors->first('INPUT_DATE') }}</p> @endif
					</div>

					<label for="QTY" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 입고수량</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid" id="QTY" placeholder="" name="QTY" value="{{number_format($model->QTY, 1)}}">
						<input type="hidden" class="form-control" placeholder="" name="PRE_QTY" value="{{$model->QTY}}">
						@if ($errors->has('QTY')) <p class="help-block sum_valid">{{ $errors->first('QTY') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="UNCS" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 입고단가</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid" id="UNCS" placeholder="" name="UNCS" value="{{number_format($model->UNCS)}}">
						@if ($errors->has('UNCS')) <p class="help-block ">{{ $errors->first('UNCS') }}</p> @endif
					</div>

					<label for="AMT" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 입고합계</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid" id="AMT" placeholder="" name="AMT" value="{{ number_format( ($model->AMT) )}}" readonly="readonly" >
						@if ($errors->has('AMT')) <p class="help-block">{{ $errors->first('AMT') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="INPUT_DISCOUNT" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 할인금액</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid numeric" id="INPUT_DISCOUNT" placeholder="" name="INPUT_DISCOUNT" value="{{number_format($model->INPUT_DISCOUNT)}}">
						@if ($errors->has('INPUT_DISCOUNT')) <p class="help-block">{{ $errors->first('INPUT_DISCOUNT') }}</p> @endif
					</div>

					<label for="REAL_AMT" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 입고금액</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid" id="REAL_AMT" placeholder="" name="REAL_AMT" value="{{number_format(  ($model->REAL_AMT) )}}" readonly="readonly">
						@if ($errors->has('REAL_AMT')) <p class="help-block">{{ $errors->first('REAL_AMT') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="CASH" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 현금</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid numeric" id="CASH" placeholder="" name="CASH" value="{{number_format( $model->CASH  )}}">
						@if ($errors->has('CASH')) <p class="help-block">{{ $errors->first('CASH') }}</p> @endif
					</div>

					<label for="UNPROV" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 미지급금</label>
					<div class="col-sm-2">
						<input type="text" class="form-control sum_valid" id="UNPROV" placeholder="" name="UNPROV" value="{{number_format(  $model->UNPROV ) }}" readonly="readonly">
						@if ($errors->has('UNPROV')) <p class="help-block">{{ $errors->first('UNPROV') }}</p> @endif
					</div>
				</div>
				<div class="form-group">
					<label for="FRNM" class="col-sm-2 control-label"><i class='fa fa-check-circle text-red'></i> 거래업체부호</label>
					<div class="col-sm-2">
						<input type="text" style="display:inline;width:50%;" class="form-control" id="CUST_MK" placeholder="" name="CUST_MK" value="{{$model->CUST_MK}}" readonly="readonly" >
						@if ($errors->has('CUST_MK')) <p class="help-block">{{ $errors->first('CUST_MK') }}</p> @endif
						<input type='hidden' name='PRE_CUST_MK' value="{{$model->CUST_MK}}" />

						<button type="button" name="search" id="btnSearchCust" class="btn btn-info"><i class="fa fa-search"></i></button>
					</div>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="FRNM" placeholder="" name="FRNM" value="{{$model->FRNM}}" readonly="readonly" />
						@if ($errors->has('FRNM')) <p class="help-block">{{ $errors->first('FRNM') }}</p> @endif
					</div>
				</div>
				<!--
				<div class="form-group">
					<label for="REASON" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 재고정리 사유</label>
					<div class="col-sm-6" >
						<input type="radio" name="REASON" value="0" /> 감량
						<input type="radio" name="REASON" value="1" /> 폐사
						<input type="radio" name="REASON" value="2" /> 증량
						<input type="radio" name="REASON" value="3" /> 기타
						@if ($errors->has('REASON')) <p class="help-block">{{ $errors->first('REASON') }}</p> @endif
					</div>
				</div>
				-->
				<div class="form-group">
					<label for="OUTLINE" class="col-sm-2 control-label"><i class='fa fa-circle-o text-aqua'></i> 적요</label>
					<div class="col-sm-6">
						<textarea class="form-control" id="OUTLINE" placeholder="" name="OUTLINE" >{{$model->OUTLINE}}</textarea>
						@if ($errors->has('OUTLINE')) <p class="help-block">{{ $errors->first('OUTLINE') }}</p> @endif
					</div>
				</div>
			</div>
		  <!-- /.box-body -->
			<div class="box-footer text-center">
				<a class="btn btn-default" href="/stock/stock/detail/{{$model->PIS_MK}}/{{$model->SIZES}}" title="취소">취소</a>
				<button type="submit" class="btn btn-info center" alt="저장">저장</button>
			</div>
		  <!-- /.box-footer -->
		{{ Form::close() }}
	</div>
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	
	//alert('개발자 작업중입니다. 사용 하지 마십시오.');
	
	var chkSujo		= true;
	var ORGIN_CD	= "{{ $model->ORIGIN_CD }}";
	var INPUT_DATE	= "{{ $model->INPUT_DATE}}";

	function validateForm() {

		$("#UNPROV").val(removeCommas($("#UNPROV").val().trim()) );
		$("#CASH").val(removeCommas($("#CASH").val().trim()) );
		$("#REAL_AMT").val(removeCommas($("#REAL_AMT").val().trim()) );
		$("#INPUT_DISCOUNT").val(removeCommas($("#INPUT_DISCOUNT").val().trim()) );
		$("#AMT").val(removeCommas($("#AMT").val().trim()) );
		$("#UNCS").val(removeCommas($("#UNCS").val().trim()) );
		$("#QTY").val(removeCommas($("#QTY").val().trim()) );
		$("select[name='ORIGIN_CD']").prop("disabled", false);
		
		return true;
								
	}


	$(function(){
		
		// 현 재고의 수조가 있는지 없는지 체크
		// 수조를 삭제 및 이고했을 수도 있으므로..
		/*
		$.getJSON("/stock/stock/chkHasSujo", {
			  'SUJO_NO'		: "{{$model->SUJO_NO}}"
			, 'PIS_MK'		: "{{$model->PIS_MK}}"
			, 'SIZES'		: "{{$model->SIZES}}"
			, 'INPUT_DATE'	: INPUT_DATE
			, 'ORIGIN_CD'	: ORGIN_CD
			,_token : '{{ csrf_token() }}'
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			
			if( data != "0"){
				chkSujo = true;
				$(".modal-body > .alert").remove();
				$("button[type='submit']").prop("disabled", false);
				$("#btnSearchSujo").prop("disabled", true);
			}else{
				$(".box-info > .modal-body > .alert").remove();
				$(".box-info > .modal-body").prepend( getAlert('warning', '경고', "이고 및 수조변경으로 인해 [수조검색] 버튼을 선택하여 현재고의 수조를 지정하세요") ).show();
				$(".modal .modal-body").prepend( getAlert('warning', '경고', "해당재고는 이고 및 수조변경으로 인해 현재고의 수조를 지정해야 합니다.") ).show();
				chkSujo = false;

				$("button[type='submit']").prop("disabled", true);
				$("#btnSearchSujo").prop("disabled", false);

				$("#btnSearchSujo").trigger("click");

			}
		
		});
		*/

		var error	= $("input[name='error']").val();
		var mode	= $("input[name='mode']").val();

		if( mode == "Edit"){
			$("#CUST_GRP_CD").prop("readonly", true);
		}

		/*
		var today = moment();
		// 초기값 세팅
		// 입고등록 > 입고일 
		$("#INPUT_DATE").val(today.format('YYYY-MM-DD'));

		
		$("#INPUT_DATE").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10일', '11월', '12월'],
				}
		});
		*/
		
		

		$(".sum_valid").on("keyup", function(){
			
			var qty			= removeCommas($("#QTY").val());			// 입고중량
			var input_uncn	= removeCommas($("#UNCS").val());			// 입고단가
			var input_amt	= removeCommas($("#AMT").val());			// 입고합계
			var discount	= removeCommas($("#INPUT_DISCOUNT").val());	// 할인금액
			var real_amt	= removeCommas($("#REAL_AMT").val());		// 입고금액
			var cash		= removeCommas($("#CASH").val());			// 현금
			var unprov		= removeCommas($("#UNPROV").val());			// 미지급금

			var T = Number( '1e' + 1 );
			a = Math.round( ( qty * input_uncn ) * T ) / T;
			a += 500;
			a = Math.floor( a / 1000 ) * 1000;

			real_amt		= a - discount;
			unprov			= a - discount - cash;
			
			$("#AMT").val(Number(a).format());
			$("#REAL_AMT").val(Number(real_amt).format());
			$("#UNPROV").val(Number(unprov).format());
		}); 

		// 거래처 추가
		$("#btnSearchCust").click(function(){ 
			
			$("#tblCustList").dataTable().fnDestroy();
			var cTable = $('#tblCustList').DataTable({
				processing: true,
				serverSide: true,
				iDisplayLength: 10,		// 기본 100개 조회 설정
				bLengthChange: false,
				order : [[1, "desc"]],
				bInfo : false,
				retrieve	: true,
				search		: false,
				info		: false,
				ajax: {
					url: "/buy/sujo/getPisCust",
					data:function(d){
						d.textSearch	= $("input[name='strSearchCust']").val();
						d.corp_div		= "P";
					}
				},
				columns: [
					{ data: 'CUST_MK', name: 'CUST_MK' },
					{ data: 'FRNM', name: 'FRNM' },
					{ data: 'RPST',  name: 'RPST' },
					{ data: 'PHONE_NO',  name: 'PHONE_NO' },
					{
						data:   "CUST_MK",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
							}
							return data;
						},
						className: "dt-body-center"
					},
				],
				"searching": true,
				"paging": true,
				"autoWidth": true,
				"oLanguage": {
					"sLengthMenu": "조회수 _MENU_ ",
					"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
					"sProcessing": "현재 조회 중입니다",
					"sEmptyTable": "조회된 데이터가 없습니다",
					"sZeroRecords": "조회된 데이터가 없습니다",
					"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
					"oPaginate": {
						"sFirst": "처음",
						"sLast": "끝",
						"sNext": "다음",
						"sPrevious": "이전"
					}
				}
			});

			$("#modal_cust_search").modal({show:true});

			$('#tblCustList tbody').on( 'click', 'button', function () {
				var data = cTable.row( $(this).parents('tr') ).data();
				$("#CUST_MK").val(data.CUST_MK);
				$("#FRNM").val(data.FRNM);
				$("input[name='strSearchCust']").val('');
				$("#modal_cust_search").modal("hide");	
			});

			$("input[name='strSearchCust']").on("keyup", function(){
				cTable.search($(this).val()).draw() ;
			});
		});
		
		// 원산지 변경
		$("select[name='ORIGIN_CD']").change(function(){
			$("input[name='ORIGIN_NM']").val( $("select[name='ORIGIN_CD'] option:selected").text());
		});

		
		// 수조 선택 조회
		var sTable;
		/*
		$("#btnSearchSujo").click(function(){ 
			
			$("#tblSujoList").dataTable().fnDestroy();
			sTable = $('#tblSujoList').DataTable({
				processing: true,
				serverSide: true,
				iDisplayLength: 10,		// 기본 100개 조회 설정
				bLengthChange: false,
				order : [[0, "asc"]],
				bInfo : false,
				retrieve	: true,
				search		: false,
				info		: false,
				ajax: {
					url: "/buy/sujo/getSujoSearch",
					data:function(d){
						d.SUJO_NO		= "{{$model->SUJO_NO}}";
						d.PIS_MK		= "{{$model->PIS_MK}}";
						d.SIZES			= "{{$model->SIZES}}";
						d.ORIGIN_CD		= "{{$model->ORIGIN_CD}}";
					}
				},
				columns: [
					{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-center" },
					{ data: 'PIS_NM', name: 'PIS_NM' },
					{ data: 'SIZES',  name: 'SIZES' },
					{ data: 'ORIGIN_NM',  name: 'ORIGIN_NM' },
					{ data: 'INPUT_DATE',  name: 'INPUT_DATE', className: "dt-body-center" },
					{
						data:   "SUJO_NO",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								return "<button class='btn btn-success btn-block btnChoice' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
							}
							return data;
						},
						className: "dt-body-center"
					},
				],
				"searching": true,
				"paging": true,
				"autoWidth": true,
				"oLanguage": {
					"sLengthMenu": "조회수 _MENU_ ",
					"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
					"sProcessing": "현재 조회 중입니다",
					"sEmptyTable": "조회된 데이터가 없습니다",
					"sZeroRecords": "조회된 데이터가 없습니다",
					"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
					"oPaginate": {
						"sFirst": "처음",
						"sLast": "끝",
						"sNext": "다음",
						"sPrevious": "이전"
					}
				}
			});

			$("#modal_Sujo_search").modal({show:true});

			// 목록에서 수조를 선택
			$('#tblSujoList tbody').on( 'click', '.btnChoice', function () {
				var data = sTable.row( $(this).parents('tr') ).data();
				$("#SUJO_NO").val(data.SUJO_NO);
				$("#modal_Sujo_search").modal("hide");	
				$("button[type='submit']").prop("disabled", false);
			});
		});
		*/
		// 임시 수조번호 생성
		/*
		$(".btnAddSujoTemp").click(function(){
			$.getJSON('/buy/sujo/getSujoNewTemp', {
				
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("input[name='new_sujo_no']").val(data.NEW_SUJO_NO);
				$("#SUJO_NO").val(data.NEW_SUJO_NO);
				$("#modal_Sujo_search").modal("hide");
				$("button[type='submit']").prop("disabled", false);
			});
		});
		*/
	});
</script>

<div class="modal fade" id="modal_Sujo_search" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 수조조회 조회/선택 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				
				<div class="box box-primary">

					<div class="box-body">
						<!--
						<button type="button" class="btn btn-success pull-left btnAddSujoTemp" data-dismiss="modal">
							임시수조 생성
						</button>
						
						<input type="text" class="form-control" name="strTempSujo" id="strSearchSujo" readonly="readonly" />
						-->
						<table id="tblSujoList" class="table table-bordered table-hover" summary="거래처 목록">
							<caption>수조 조회</caption>
							<thead>
								<tr>
									<th scope="col" class="check">수조</th>
									<th scope="col" class="subject">어종</th>
									<th scope="col" class="name">사이즈</th>
									<th scope="col" class="name">원산지</th>
									<th scope="col" class="name">입고일</th>
									<th scope="col" class="name">선택</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_cust_search" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 거래처 조회/선택 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				
				<div class="box box-primary">

					<label for="strSearchCust"><i class='fa fa-check-circle text-aqua'></i> 거래처 상호</label>
					<input type="text" class="form-control" name="strSearchCust" id="strSearchCust" />
					<div class="box-body">
						<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
							<caption>거래처 조회</caption>
							<thead>
								<tr>
									<th scope="col" class="check">코드</th>
									<th scope="col" class="subject">상호</th>
									<th scope="col" class="name">대표자</th>
									<th scope="col" class="name">전화번호</th>
									<th scope="col" class="name">선택</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
		</div>
	</div>
</div>
@stop
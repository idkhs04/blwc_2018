@extends('layouts.main')
@section('class','매입관리')
@section('title','어종별 재고조회')
@section('content')

<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.btn-info{ vertical-align:baseline; margin-top:0px;}
</style>

<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			
			<div class='col-md-4 col-xs-12'>
				<input type="radio" name="chkSujoStock" value="1" checked="checked" /> 수조 재고
				<input type="radio" name="chkSujoStock" value="0" /> 이전 재고
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종별 재고 목록" width="100%">
				<caption>재고조회</caption>
				<thead>
					<tr>
						<th width="60px">어종</th>
						<th width="40px">규격</th>
						<th width="45px">중량</th>
						<th width="100px">상세</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	
	$(function () {
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/stock/stock/listData",
				data:function(d){
					d.chkStock		= $("input[name='chkSujoStock']:checked").val();
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			columns: [
			
				{ data: 'PIS_NM', name: 'PIS_NM' },
				{ data: "SIZES",  name: 'ST.SIZES'},
				{ 
					data: 'QTY', 
					//name: 'ST.QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: "PIS_NM",
					name: 'ST.PIS_NM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-success btnGoDetail' ><i class='fa fa-arrow-circle-right'></i> 상세보기</button>";
						}
						return data;
					},
					className: "dt-body-left",
					orderable:      false,
				},
				{
					"className":      '',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("input[name='chkSujoStock']").change(function(){
			oTable.draw() ;
		});
		
		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 선택 후 상세화면으로 이동
		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/stock/stock/detail/" + data.PIS_MK + "/" + data.SIZES;
		} );
	});

</script>
@stop
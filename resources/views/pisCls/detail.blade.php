@extends('layouts.main')
@section('class','코드관리')
@section('title','어종관리 상세')

@section('content')
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}
</style>
<div>
<!-- board_search-->
@include('include.search_header', ['pis' => $pis])
<!-- List_board-->
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<div class="col-md-12 col-xs-12">
					<table id="tblDetail" class="display" cellspacing="0" width="100%">
						<colgroup>
							<col />
							<col />
						</colgroup>
						<thead>
							<th></th>
							<th></th>
						</thead>
						<tbody>
							<tr><th>어종부호</th><td>{{$PIS_INFO->PIS_MK}}</td></tr>
							<tr><th>어종명</th><td>{{$PIS_INFO->PIS_NM}}</td></tr>
							<tr><th>학명</th><td>{{$PIS_INFO->HAKNM}}</td></tr>
							<tr><th>분류</th><td>{{$PIS_INFO->CLASS}}</td></tr>
							<tr><th>서식장소</th><td>{{$PIS_INFO->FORM_PLCE}}</td></tr>
							<tr><th>분포지역</th><td>{{$PIS_INFO->CLASS_AREA}}</td></tr>
							<tr><th>적정수온</th><td>{{$PIS_INFO->NORMAL_TEMP}}</td></tr>
							<tr><th>판매대상</th><td>{{$PIS_INFO->FILENAME}}</td></tr>
							<tr><th>파일명</th><td>{{$PIS_INFO->FILENAME}}</td></tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종부호 목록 목록" width="100%">
					<caption></caption>
					<thead>
						<tr>
							<th scope="col" class="check">선택</th>
							<th scope="col" class="subject">어종부호</th>
							<th scope="col" class="name">규격</th>
							<th scope="col" class="name">비고</th>

						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	<!-- 본문  -->
	</div>
	

	<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="/static/js/jquery/jquery.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="/static/js/jquery-ui/jquery-ui.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="/static/js/bootstrap/bootstrap.js"></script>
	<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script>
	
	$(function () {
	
		var vTable = $('#tblDetail').DataTable({
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false,
		});
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "/pis/pis/detailListData",
				data:function(d){
					d.PIS_MK		= "{{ Request::segment(4)}}";
					
				}
			},
			columns: [
				{
					data:   "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'PIS_MK', name: 'PIS_MK' },
				{ data: 'SIZES', name: 'SIZES' },
				{ data: "REMARK",  name: 'REMARK'},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		

		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/sale/sale/detailForm/" + data.CUST_MK + "/" + data.SEQ + "/" + data.WRITE_DATE;
		});

	});

</script>
<!-- 어종추가 팝업 -->
<div class="modal fade" id="modal_pis_insert" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content modal-sm">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 어종 추가 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="box box-primary">
					<div class="box-body">
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>

				<button type="button" class="btn btn-success btn-default pull-right" id="btnSavePis">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>
@stop
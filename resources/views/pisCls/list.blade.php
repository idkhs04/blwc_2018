@extends('layouts.main')
@section('class','코드관리')
@section('title','어종관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	#modal_piscls_insert label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_piscls_insert label.srt{
		width:45px;
	}

	#modal_piscls_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	.btn-info{
		vertical-align:baseline;
		margin-top:0;
	}
	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	#modal_pis_insert label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_pis_insert .form-control{
		display:inline-block;
		width:291px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{ display:none;}
	.btn-info{ vertical-align:baseline; margin-top:0 }

	div.file{ display:none;}

	.detail_detailList th{ text-align:center;}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnUpdate" ><i class="fa fa-edit"></i> 어종수정</button>
					<button type="button" class="btn btn-danger" id="btnDelete" ><i class="fa fa-remove"></i> 어종삭제</button>
					<button type="button" class="btn btn-info" id="btnAdd" ><i class="fa fa-plus"></i> 어종추가</button>
				</div>
			</div>
			
			<div class="col-md-2 col-xs-4">
				<select class="form-control" name="srtCondition" id="srtCondition">
					<option value="PIS_NM">어종명</option>
					<option value="PIS_MK">어종부호</option>
				</select>
			</div>
			<!--
			<div class="col-md-3 col-xs-8">
				<div class="form-control">
					<label><input type="radio" name="chkSizeOrQty" value="ALL" /> 전체</label>
					<label><input type="radio" name="chkSizeOrQty" value="SIZES" /> 수량</label>
					<label><input type="radio" name="chkSizeOrQty" value="QTYS" /> 마리</label>
				</div>
			</div>
			-->

			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종 정보조회" width="100%">
				<caption>어종 규격 조회</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th class="check" width="70px">사이즈 관리</th>
						<th class="name" width="50px">어종부호</th>
						<th value="name" width="60px">어종명</th>
						<th value="name" width="70px">사이즈(필수)</th>
						<th value="name" width="auto"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	var clPIS_MK = '';

	$(function () {
		
		var start = moment().subtract(6, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');
		
		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
			$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
		}
			
		// 어종정보 작성일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		
		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			ranges: {
				'오늘': [moment(), moment()],
				'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
				'이번 달': [moment().startOf('month'), moment().endOf('month')],
				'지난 달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);

		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},
		 
			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},
		 
			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/pis/pis/listData",
				data:function(d){

					d.chkSale		= "1";
					d.chkCommon		= "0";
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.chkSizeOrQty	= $("input[name='chkSizeOrQty']:checked").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			fnDrawCallback: function(oSettings ){
				setOpenRow();
			},
			aoColumnDefs: [
				{ "sType": "numeric-comma", "aTargets": [2] }
			],
			order: [[ 3, 'asc' ]],
			columns: [
				{
					data:   "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" +data+ "' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					"className":      'details-control dt-body-center',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'PIS_MK',		 name: 'PIS_MK' },
				{ data: 'PIS_NM',		 name: 'PIS_NM' },
				{
					orderable:  false,
					data:   "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button type='button' data-pis_mk='"+row.PIS_MK+"' data-pis_nm='"+row.PIS_NM+"' class='btn btn-success btnAddSizes'><i class='fa fa-plus'></i> 사이즈 추가</button>";
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='chkSizeOrQty']").on("change", function(){
			oTable.draw() ;
		});

		// 규격 수정/삭제 후 해당 어종칸이 열려있도록 한다.
		function setOpenRow(){
			//console.log(clPIS_MK);
			$("#tblList tbody > tr > td > input[type=checkbox][value='"+ clPIS_MK +"']").parents('tr').find('td.details-control').trigger('click');			
		}

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("#tblList tbody > tr > td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("#tblList tbody > tr > td > input[type=checkbox]").prop("checked", true);
			}
		});
		
		// 그리드 단추 클릭
		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format( row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		function format ( d ) {

			var xmlDoc = jQuery.parseXML(d.DD);
			var table = '<table class="detail_detailList table table-bordered table-hover display nowrap" cellpadding="5" cellspacing="0" border="0" width="60%">' +
							"<thead>" +
								"<tr>" +
									"<th width='30px'>사이즈</th>" +
									"<th width='auto' class='dt-body-left'>삭제</th>" +
								"</tr>" + 
							"</thead><tbody>";


			$(xmlDoc).find("PIS_SIZE").each(function (i,e){

				table += '<tr>' +
							//'<td> <input type="checkbox" class="dtPisMk" value=' + $(e).find('PIS_MK').text() +'/></td>'+
							'<td class="dt-body-center" >'+ $(e).find('SIZES').text() + "</td>" +
							"<td class='dt-body-left' data-pis_mk='" + $(e).find('PIS_MK').text() + "' data-pis_size='" + $(e).find('SIZES').text() + "'>" + 
							//	'<button type="button" class="btn btn-info btnUpdSizes"><i class="fa fa-edit"></i> 수정</button> ' + 
								'<button type="button" class="btn btn-danger btnRemoveSizes"><i class="fa fa-remove"></i> 삭제</button>' + 
							'</td>'+
						'</tr>';

			});
			table += "</tbody></table>";
			return table;
			
		}

		// 규격 추가 팝업
		$("body").on("click", "button.btnAddSizes",function(){

			var pis_mk =  $(this).attr('data-pis_mk');
			var pis_nm =  $(this).attr('data-pis_nm');

			defaltModalInsert();
			$("#mode").val("ins");
			$("#modal_piscls_insert input[name='SIZES']").prop("readonly", false);
			$("#modal_piscls_insert input[name='radSIZES']").prop("disabled", false);
			
			$("#btnPisInfoList").prop("disabled", false);

			$("#modal_piscls_mk").val(pis_mk);
			$("#modal_piscls_nm").val(pis_nm);

			$('#modal_piscls_insert').modal({show:true}); 

		});

		// 규격 수정 팝업
		$("body").on("click", "button.btnUpdSizes",function(){
			
			var pis_mk = $(this).parent('td').attr('data-pis_mk');
			var sizes  = $(this).parent('td').attr('data-pis_size');
			
			clPIS_MK = pis_mk;
			// 수정일 경우 입력 막음
			$("#modal_sizes").prop("diabled", true);
			//$("input[name='radSIZES']").prop('readonly', true);

			//console.log(pis_mk, sizes);

			if( sizes == "마리" ){
				sizes = "QTYS";
			}
			$.getJSON('/piscls/piscls/getDataDetail', {
				PIS_MK	: pis_mk ,
				SIZES	: sizes,
				_token		: '{{ csrf_token() }}'
			}).success(function(data){
				
				var item = data[0];

				$("#mode").val("upd");
				$("#btnPisInfoList").prop("disabled", true);
				$("#modal_piscls_insert input[name='radSIZES']").prop("disabled", true);

				$("#modal_piscls_insert").modal("show");
				$("#modal_piscls_insert input[name='SIZES']").prop("readonly", true);

				$("#modal_piscls_insert input[name='PIS_MK']").val(item.PIS_MK)
				$("#modal_piscls_insert input[name='PIS_NM']").val(item.PIS_NM)

				if( item.SIZES == "QTYS"){
					$("#modal_piscls_insert input[name='SIZES']").val("마리");
					$("#modal_piscls_insert input[name='radSIZES']:eq(0)").prop("checked", false);
					$("#modal_piscls_insert input[name='radSIZES']:eq(1)").prop("checked", true);
				}else{
					$("#modal_piscls_insert input[name='SIZES']").val(item.SIZES)
					$("#modal_piscls_insert input[name='radSIZES']:eq(0)").prop("checked", true);
					$("#modal_piscls_insert input[name='radSIZES']:eq(1)").prop("checked", false);
				}

				$("input[name='OLD_SIZES']").val(item.SIZES);
				$("#modal_piscls_insert input[name='SIZES']").val(item.SIZES)
				//$("#modal_piscls_insert input[name='REMARK']").val(item.REMARK)
				
			}).error(function(xhr,status, response) {
				
				var data = jQuery.parseJSON(JSON.stringify(xhr.responseJSON));
				$(".content > .alert").remove();
				$(".content").prepend( getAlert('danger', '경고', "오류입니다. 다시한번 시도하시고 안되면 관리자에게 문의하세요.") ).show();
			
			});	

		});

		// 규격 삭제 팝업
		$("body").on("click", "button.btnRemoveSizes",function(){
			
			var pis_mk = $(this).parent('td').attr('data-pis_mk');
			var sizes  = $(this).parent('td').attr('data-pis_size');

			if( confirm("삭제하시겠습니까?") ){
				if( sizes == "마리" ){
					sizes = "QTYS";
				}
				$.getJSON('/piscls/piscls/_delete', {
					PIS_MK	: pis_mk ,
					SIZES	: sizes,
					_token		: '{{ csrf_token() }}'
				}).success(function(data){
					clPIS_MK = pis_mk;
					oTable.draw();
				}).error(function(xhr,status, response) {
					var info = $('#modal_pis_insert .edit_alert');
					info.hide().find('ul').empty();

					try {
						var error = jQuery.parseJSON(xhr.responseText); 
						
						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li> 이미 동일한 데이터가 존재합니다.</li>');
						}else if( error.result == "DB_ERROR"){
							info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
						}else{
							for(var k in error.message){
								info.find('ul').append('<li>' + k + '</li>');
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
					} catch(err){
						
					}

					info.slideDown();
					$(".content > .alert").remove();
					$(".content").prepend( getAlert('danger', '경고', "현재 사용중이므로 삭제 불가능합니다.") ).show();
				
				});
			}
		});
		
		// 규격코드 입력 추가
		$("#btnAdd").click(function(){
			defaltModalInsert();
			$("#mode").val("ins");
			$("#modal_pis_insert input[name='SIZES']").prop("readonly", false);
			$("#modal_pis_insert input[name='radSIZES']").prop("disabled", false);
			
			$("#btnPisInfoList").prop("disabled", false);
			$('#modal_pis_insert').modal({show:true}); 
		});

		// 규격 입력시 검색
		$("#btnPisInfoList").click(function(){ 
			$( ".slideSearchCust" ).show(function(){
				var cTable = $('#tblPisInfoList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					bLengthChange: false,
					bInfo:false,
					ajax: {
						url: "/pis/pis/listData",
						data:function(d){
							d.textSearch	= $("input[name='strSearchPis']").val();
							d.chkSale		= $("input[name='chkSale']").is(":checked") ? 1 : 0;
							d.chkCommon		= $("input[name='chkCommon']").is(":checked") ? 1 : 0;
						}
					},
					
					columns: [
						{ data: 'PIS_MK', name: 'PIS_MK' },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'HAKNM',  name: 'HAKNM' },
						{ data: 'CLASS',  name: 'CLASS' },
						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span>선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
						
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblPisInfoList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();
					$("#modal_piscls_mk").val(data.PIS_MK);
					$("#modal_piscls_nm").val(data.PIS_NM);

					$("input[name='strSearchPis']").val('');
					$(".slideSearchCust" ).slideUp();	
				});

				$("input[name='strSearchPis']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
				$("input[name='chkSale'], input[name='chkCommon']").on('click', function(){
					cTable.draw();
				});
			
				// 거래처 검색 닫기
				$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });
			});
		});

		// 차대구분 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 어종 저장(입력/저장) 버튼
		$("#btnInsertSave").click(function(){

			var info = $('#modal_piscls_insert .edit_alert');

			clPIS_MK = $("input[name='PIS_MK']").val();

			var mode = $("#mode").val();
			var	url  = '/piscls/piscls/setpisClsData'; 
			var JsonData = {
								"PIS_MK"	: $("input[name='PIS_MK']").val(),
								"SIZES"		: ( $("input[name='radSIZES']:checked").val() == "SIZES" ? $("input[name='SIZES']").val() : $("#modal_qtys").val() ),
								"OLD_SIZES"	: $("input[name='OLD_SIZES']").val() == "마리" ? "QTYS" : $("input[name='OLD_SIZES']").val(), 
								//"REMARK"	: $("input[name='REMARK']").val(),
			};
			
			if( mode != "ins"){
				url = '/piscls/piscls/updpisClsData'
			}
			$.ajax( {
				url : url,
				dataType:'json',
				type : 'GET',
				data : JsonData,			
				success : function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					if (data.result == "success") {
						$('#modal_piscls_insert').modal("hide"); 
					}
					
					oTable.draw();

				},
				error : function(xhr){
					var error = jQuery.parseJSON(xhr.responseText); 
					info.hide().find('ul').empty();
					if( error.result == "DUPLICATION"){
						info.find('ul').append('<li> 이미 동일한 데이터가 존재합니다.</li>');
					}else if( error.result == "DB_ERROR"){
						info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
					}else{
						for(var k in error.message){
							info.find('ul').append('<li>' + k + '</li>');
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					}
					info.slideDown();
				}
			});
		});
		$('#modal_piscls_insert').on('hidden.bs.modal', defaltModalInsert);
		$('#modal_pis_insert').on('hidden.bs.modal', defaltModalInsert);

		// 어종 입력 초기화
		function defaltModalInsert(){
			
			$(".edit_alert ul").empty();
			$("section.content > div.alert").remove();
			$("#modal_piscls_insert input[type='text']").val("");

			$("#modal_piscls_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_piscls_insert input[name='modal_remark']:first").prop("checked", true);
			
			$("#modal_piscls_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_piscls_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			//$('.edit_alert').hide();
			//$(".slideSearchCust" ).slideUp();	
			$("#modal_remark_text").focus();
			$("span.msg").hide();


			$("#modal_pis_insert input[type='text']").val("");
			$("#modal_pis_insert input[type='file']").val("");
			$("#modal_write_dt").val(today);

			$("#modal_pis_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_pis_insert input[name='modal_remark']:first").prop("checked", true);
			
			$("#modal_pis_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_pis_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			
		}

		// 어종부호 중복체크
		/*
		$("#btnChkPrimary").click(function(){
			
			$.getJSON('/pis/pis/chkPIS_MK', {
				_token		: '{{ csrf_token() }}',
				PIS_MK		: $("#modal_piscls_mk").val(),
				chkPIS_MK	: $("input[type='chkPIS_MK']").val(),
			}).success (function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if( data.result == "success"){
					$("span.msg").text("사용 가능합니다").show()
					$("input[name='chkPIS_MK']").val(true);
				}
				
			}).error(function(xhr){
				$("span.msg").text("사용 불가능합니다").show()
			});
		
		});
		*/
		
		setInitSizeOrQty();
		$("input[name='radSIZES']").click(setInitSizeOrQty);
		
		function setInitSizeOrQty(){
			if( $("input[name='radSIZES']:checked").val() == "SIZES" ) {
				$("#modal_sizes").prop("readonly", false);
				$("#modal_sizes").val("");
			}else{
				$("#modal_sizes").prop("readonly", true);
				$("#modal_sizes").val("마리");
			}
		}

		// 어종추가
		$("#btnAdd").click(function(){
			$("#mode").val("ins");
			defaltModalInsert();

			$('#modal_pis_insert').modal({show:true}); 
			
			$("#modal_pis_mk, #modal_pis_nm").prop("readonly", false);
			$("#btnChkPrimary").prop("disabled", false)
		});

		// 입력창 닫기
		// $('#modal_pis_insert').on('hidden.bs.modal', CbSetInitModalClose);
		
		// 차대구분 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 어종 저장 버튼
		$("#btnInsertSavePisMk").click(function(){
			
			//var SALE_TARGET	= $("#modal_pis_insert input[name='sale_target']:checked").val();
			
			var form = document.forms.namedItem("attach_form"); // high importance!, here you need change "yourformname" with the name of your form
			var formdata = new FormData(form); // high importance!
			var mode = $("#mode").val();
			//alert(mode);
			if( mode == "ins"){
				$.ajax( {
						url : '/pis/pis/setpisData',
						dataType:'json',
						type : 'POST',
						enctype: 'multipart/form-data',
						data : formdata,
						contentType: false,
						processData: false, // high importance!
					
						success : function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							if (data.result == "success") {
								$('#modal_pis_insert').modal("hide"); 
							}

							clPIS_MK = $("#modal_pis_mk").val();
							oTable.draw();

						},
						error : function(xhr){
							var error = jQuery.parseJSON(xhr.responseText); 
							var info = $('#modal_pis_insert .edit_alert');
							info.hide().find('ul').empty();

							if( error.result == 'primary_key'){
								info.find('ul').append('<li>' + error.message + '</li>');
							}else{
								
								console.log(error.message);
								for(var k in error.message){
									if(error.message.hasOwnProperty(k)){
										error.message[k].forEach(function(val){
											info.find('ul').append('<li>' + val + '</li>');
										});
									}
								}
							}
							info.slideDown();
						}
				});

			}else{

				$.ajax( {
						url : '/pis/pis/updpisData',
						dataType:'json',
						type : 'POST',
						enctype: 'multipart/form-data',
						data : formdata,
						contentType: false,
						processData: false, // high importance!
					
						success : function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							if (data.result == "success") {
								$('#modal_pis_insert').modal("hide"); 
							}
							clPIS_MK = $("#modal_pis_mk").val();
							oTable.draw();

						},
						error : function(xhr){
							var error = jQuery.parseJSON(xhr.responseText); 
							var info = $('#modal_pis_insert .edit_alert');
							info.hide().find('ul').empty();

							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
							info.slideDown();
						}
				});
			}
		});

		// 어종 수정
		$("#btnUpdate").click(function(){
			defaltModalInsert();
			$("#mode").val("upd");

			$("#modal_pis_mk").prop("readonly", true);
			//$("#btnChkPrimary").prop("disabled", true);

			var arr_code = [];
			//var row = null;

			$('#tblList > tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();
			
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정은 한개만 선택해주세요") ).show();
				return false;
			}
			
			// console.log(arr_code);

			if( arr_code.length == 1){
				$('#modal_pis_insert').modal({show:true}); 
				$.getJSON('/pis/pis/getDetailData', {
					PIS_MK : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					data = data.result;

					$("#modal_pis_mk").val(data.PIS_MK);
					$("#modal_pis_nm").val(data.PIS_NM);
					$("#modal_haknm").val(data.HAKNM);
					$("#modal_class").val(data.CLASS);
					$("#modal_form_plce").val(data.FORM_PLCE);
					$("#modal_class_area").val(data.CLASS_AREA);
					$("#modal_normal_temp").val(data.NORMAL_TEMP);

					/*
					// 판매어종 구분 하지 않음...
					if( data.SALE_TARGET == "1" ){
						$("#SALE_TARGET1").prop("checked", true); 
						$("#SALE_TARGET0").prop("checked", false);
					}else{
						$("#SALE_TARGET0").prop("checked", true); 
						$("#SALE_TARGET1").prop("checked", false);
					}
					*/

					if( data.FILENAME !== null ){
						$("div.file").css("display", "block");
						$("div.file").append("<img src='/uploads/" + data.FILENAME + "' width='70%'/>");
					}else{
						$("div.file").css("display", "none").empty();
					}
				});
			}
		});
		
		
		// 어종부호 중복체크
		/*
		$("#btnChkPrimary").click(function(){
			
			$.getJSON('/pis/pis/chkPIS_MK', {
				_token		: '{{ csrf_token() }}',
				PIS_MK		: $("#modal_pis_mk").val(),
				chkPIS_MK	: $("input[type='chkPIS_MK']").val(),
			}).success (function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if( data.result == "success"){
					$("input[name='chkPIS_MK']").val(true);
					$(".edit_valert").html( getAlert('success', '확인', "사용가능합니다") ).show();
					$(".msg").empty();
				}
				
			}).error(function(xhr){
				$("input[name='chkPIS_MK']").val(false);
				$(".edit_valert").html( getAlert('warning', '경고', "해당 어종코드는 사용 불가능합니다") ).show();
			});
		});
		*/
		// 어종삭제
		$("#btnDelete").click(function(){
			var arr_code = [];
			$('#tblList > tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();

			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				if(confirm("선택된 어종을 삭제하시겠습니까?")){
					$.getJSON("/pis/pis/_delete", {
						PIS_MK: arr_code ,
						_token : '{{ csrf_token() }}'
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						
						if (data.result == "success") {

							clPIS_MK = arr_code;
							oTable.draw();
						}
					}).error(function(xhr,status, response) {
						
						$(".content > .alert").remove();
						$(".content").prepend( getAlert('danger', '경고', "현 어종은 사용중이거나 규격코드가 존재 합니다.") ).show();
						return;

						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.dvAlert');
						info.append("<ul></ul>");
						info.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li>' + error.message + '</li>');
						}else{
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						
						info.slideDown();
						oTable.draw();
					});
				}
			}
		});
	});


</script>
<!-- 규격코드 추가 팝업 -->
<div class="modal fade" id="modal_piscls_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 어종 사이즈 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<input type="hidden" name="chkPIS_MK" value="" />
				<input type="hidden" id="mode" value="" />
				<div class="form-group">
					<label for="modal_piscls_mk"><i class='fa fa-check-circle text-red'></i> 어종부호</label>
					<input type="text" class="form-control" id="modal_piscls_mk" name="PIS_MK" placeholder="어종부호" readonly="readonly" />
					<button type="button" name="search" id="btnPisInfoList" class="btn btn-info"><i class="fa fa-search"></i></button>
					<input type="text" class="form-control" id="modal_piscls_nm" name="PIS_NM" placeholder="어종명" readonly="readonly" />
					<span class="msg"></span>
				</div>
				<div class="form-group slideSearchCust">
					<div class="box box-primary">
						<div class="box-body">
							<label for="strSearchPis"><i class='fa fa-check-circle text-aqua'></i> 어종명</label>
							<input type="text" class="form-control cis-lang-ko" name="strSearchPis" id="strSearchPis" />

							<label for="chkSale" class='srt'><i class='fa fa-check-circle text-aqua'></i> 판매</label>
							<input type="checkbox" class="" name="chkSale" id="chkSale" />

							<label for="chkSale" class='srt'><i class='fa fa-check-circle text-aqua'></i> 일반</label>
							<input type="checkbox" class="" name="chkCommon" id="chkCommon" />

							<table id="tblPisInfoList" class="table table-bordered table-hover display nowrap" summary="어종코드 조회">
								<caption>어종코드 입력</caption>
								<thead>
									<tr>
										<th scope="col" class="name">어종부호</th>
										<th scope="col" class="name">어종명</th>
										<th scope="col" class="name">학명</th>
										<th scope="col" class="name">분류</th>
										<th scope="col" class="name"></th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_sizes"><i class='fa fa-check-circle text-red'></i> 사이즈</label>
					<input type="hidden" class="form-control" id="modal_qtys" value="QTYS" />
					<input type="text" class="form-control" id="modal_sizes" name="SIZES" placeholder="사이즈" />
					<input type="radio"  name="radSIZES" value="SIZES" checked="checked" /> Kg

					<!--<input type="radio"  name="radSIZES" value="QTYS" /> 마리-->

					<input type="hidden" id="OLD_SIZES" name="OLD_SIZES" value="" />
				</div>
				<!--
				<div class="form-group">
					<label for="modal_piscls_remark"><i class='fa fa-check-circle text-aqua'></i> 비고</label>
					<input type="text" class="form-control cis-lang-ko" id="modal_piscls_remark" name="REMARK"  placeholder="비고"  style="width:70%"/>
				</div>
				-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
			
		</div>
	</div>
</div>
<!-- 어종 추가 팝업 -->
<div class="modal fade" id="modal_pis_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<form name="attach_form" method="POST" enctype="multipart/form-data" action="">
				<input type="hidden" name="mode" id="mode" value="" />
				<div class="modal-header" style="padding:15px 35px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><span class="glyphicon glyphicon-th-large"></span> 어종 정보 입력 </h4>
				</div>
				<div class="modal-body" style="padding:20px 20px;">
					<p class='red'> 어종추가 후 사이즈(규격)을 필수로 입력하셔야 합니다. </p>
					<div class="edit_valert"></div>
					<div class="edit_alert">
						<ul>
							
						</ul>
					</div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<!--<input type="hidden" name="chkPIS_MK" value="" />-->
					<div class="form-group">
						
						
						<input type="hidden"  id="modal_pis_mk" name="PIS_MK" style="width:80px;" placeholder="어종부호" />
						
						<!--
						<button type="button" name="search" id="btnChkPrimary" class="btn btn-info">중복체크</button>
						-->
						<label for="modal_pis_mk"><i class='fa fa-check-circle text-red'></i> 어종명</label>
						<input type="text" class="form-control cis-lang-ko" id="modal_pis_nm" name="PIS_NM" placeholder="어종명" style="width:22%;"/>
						<span class="msg red"></span>
					</div>
					<!--
					<div class="form-group">
						<label for="modal_sale_target"><i class='fa fa-check-circle text-red'></i> 판매대상</label>
						<input type="radio"  name="SALE_TARGET" value="1" id="SALE_TARGET1" /> 판매어종
						<input type="radio"  name="SALE_TARGET" value="0" id="SALE_TARGET0" /> 일반어종
					</div>
					-->
					@role('sysAdmin')
					<div class="form-group">
						<label for="modal_haknm"><i class='fa fa-circle-o text-aqua'></i> 학명</label>
						<input type="text" class="form-control cis-lang-ko" id="modal_haknm" name="HAKNM" placeholder="학명" />
						
					</div>
					<div class="form-group">
						<label for="modal_class"><i class='fa fa-circle-o text-aqua'></i> 분류</label>
						<input type="text" class="form-control cis-lang-ko" id="modal_class" name="CLASS" placeholder="분류" />

					</div>
					<div class="form-group">
						<label for="modal_form_plce"><i class='fa fa-circle-o text-aqua'></i> 서식장소</label>
						<input type="text" class="form-control cis-lang-ko" id="modal_form_plce" name="FORM_PLCE"  placeholder="서식장소" />
					</div>
					<div class="form-group">
						<label for="modal_class_area"><i class='fa fa-circle-o text-aqua'></i> 분포지역</label>
						<input type="text" class="form-control cis-lang-ko" id="modal_class_area" name="CLASS_AREA" placeholder="분포지역">
					</div>
					<div class="form-group">
						<label for="modal_normal_temp"><i class='fa fa-circle-o text-aqua'></i> 적정수온</label>
						<input type="text" class="form-control" id="modal_normal_temp" name="NORMAL_TEMP" placeholder="적정수온">
					</div>
					
					<div class="form-group">
						<label for="modal_filename"><i class='fa fa-circle-o text-aqua'></i> 파일명</label>
						<input type="file" class="form-control" id="modal_filename" name="FILENAME" />
						<div class='file'> </div>
					</div>
					@endrole
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success pull-right" id="btnInsertSavePisMk">
						<span class="glyphicon glyphicon-off"></span> 저장
					</button>
					<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
						<span class="glyphicon glyphicon-remove"></span> 닫기 
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop
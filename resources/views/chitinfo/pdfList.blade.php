<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>금전출납부</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:11px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
		<style>
			@page { margin: 50px 20px; }
			footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px; }
			footer .page a:after {  text-align:right;content: counter(pages) " / " counter(page); }

		</style>
	</head>
	<body>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>금&nbsp;&nbsp;전&nbsp;&nbsp;출&nbsp;&nbsp;납&nbsp;&nbsp;부</span>

					<table id="tblHeader" border="0" cellpadding="0" cellspacing="0" summary="미지급내역 합계" width="100%" style="border:none;margin-bottom:3px;">
						<tr>
							<td style="text-align:left;">조회일자 : {{ $start_date }} ~ {{ $end_date}}</td>
							<td style="text-align:right;">상호 : {{ $list[0]->CORP_FRNM}}</td>
						</tr>
					</table>

					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미지급내역 조회" width="100%" style="border-bottom:1px solid block;margin-top:2px;border-left:none;border-right:none;">
						<caption>미수장부</caption>
						
						<thead>
							<tr>
								<td style="text-align:center;border-bottom:3px double black;" >NO</td>
								<td style="text-align:center;border-bottom:3px double black;" >일 자 </td>
								<td style="text-align:center;border-bottom:3px double black;" >계정명</td>
								<td style="text-align:center;border-bottom:3px double black;" >상호</td>
								<td style="text-align:center;border-bottom:3px double black;" >지출금액</td>
								<td style="text-align:center;border-bottom:3px double black;" >수입금액</td>
								<td style="text-align:center;border-bottom:3px double black;" >적요</td>
								<td style="text-align:center;border-bottom:3px double black;" >비 고</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
							<tr>
								<td style="text-align:center;border-top:1px solid black;border-bottom:1px solid black;border-left:1px solid black" height="30px">{{$key++}}</td>
								<td style="text-align:center;border-left:none;border-right:none;border-top:1px solid black;border-bottom:1px solid black" height="30px">{{$item->WRITE_DATE}}</td>
								<td style="padding-left:5px;text-align:left;border-top:1px solid black;border-bottom:1px solid black" >{{$item->ACCOUNT_NM}}</td>
								<td style="padding-left:5px;text-align:left;border-bottom:1px solid black" >{{$item->FRNM}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($item->AMT1)}} 원</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($item->AMT2)}} 원</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black">{{$item->OUTLINE}}</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black">{{$item->REMARK}}</td>
							</tr> 
							
							@endforeach
							<tr>
								<td style="text-align:center;border-bottom:1px solid black;border-left:1px solid black" height="30px" colspan="4">합 계</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($sumAmt1) }} 원</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($sumAmt2) }} 원</td>
								<td colspan="2" ></td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>

</html>

	



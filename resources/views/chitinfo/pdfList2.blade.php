<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>금전출납부</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:12px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
		<style>
			@page {margin-top: 30px; margin-bottom: 30px; margin-left:15px; margin-right:15px;}
			footer {bottom: 0px; position: fixed;}

			.page span { display:inline-block; width:20%;}
		</style>
	</head>
	<body>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>금&nbsp;&nbsp;전&nbsp;&nbsp;출&nbsp;&nbsp;납&nbsp;&nbsp;부</span>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
						</tr>
					</table>
					<table id="tblHeader" border="0" cellpadding="0" cellspacing="0" summary="미지급내역 합계" width="100%" style="border:none;margin-bottom:3px;">
						<tr>
							<td style="text-align:left;">조회일자 : {{ $start_date }} ~ {{ $end_date}}</td>
							<td style="text-align:right;">상호 : {{ $corp->FRNM}}</td>
						</tr>
					</table>

					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미지급내역 조회" width="100%" style="border-bottom:1px solid block;margin-top:2px;border-left:none;border-right:none;">
						<caption>미수장부</caption>
						
						<thead>
							<tr>
								<td style="text-align:center;border-left:none;border-right:none;" height="30px" width="80px">일&nbsp;&nbsp;&nbsp;자</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">계&nbsp;정&nbsp;명</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">적&nbsp;&nbsp;&nbsp;&nbsp;요</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">입&nbsp;&nbsp;&nbsp;&nbsp;금</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">출&nbsp;&nbsp;&nbsp;&nbsp;금</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">기&nbsp;간&nbsp;잔&nbsp;액</td>
								<td style="text-align:center;border-left:none;border-right:none;" width="100px">전&nbsp;체&nbsp;잔&nbsp;액</td>
								<td style="text-align:center;border-left:none;border-right:none;">비&nbsp;&nbsp;&nbsp;고</td>
							</tr>
						</thead>
						<tbody>
							{{--*/ $sumAmt  = 0 /*--}}
							@foreach ($list as $key => $item)
							<tr>
								<td style='text-align:center;border-left:none;border-right:none; padding-left:5px;border-bottom:none;border-top:none;' >{{ $item->WRITE_DATE }}</td>
								<td style='text-align:left;border-left:none;border-right:none; padding-left:7px;border-bottom:none;border-top:none;' >{{ $item->ACCOUNT_NM }}</td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >
									{{ $item->FRNM }} {{$item->OUTLINE}}</td>
								@if( $item->DE_CR_DIV == '0')
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >
								{{ number_format($item->AMT, 2) }}</td>
								
								{{--*/ $sumAmt += $item->AMT /*--}}

								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' ></td>
								@else 
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >{{ number_format($item->AMT, 2) }}</td>
								{{--*/ $sumAmt -= $item->AMT /*--}}
								@endif
								
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >{{ number_format($sumAmt) }}</td>

								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >{{ number_format($item->TOT_AMT) }}</td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' >{{ $item->REMARK }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
	<footer>
		<div class='page'>
			<span>{{$corp->FRNM}}</span>
			<span style="width:40%">{{$corp->ADDR1}} {{$corp->ADDR2}}</span>
			<span>Tel : {{$corp->PHONE_NO}}</span>
			<span>Fax : {{$corp->FAX}}</span>
		</div>
	</footer>
</html>

	



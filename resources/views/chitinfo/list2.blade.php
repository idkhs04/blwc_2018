@extends('layouts.main')
@section('class','비용관리')
@section('title','회비관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	
	.btnSearch{ margin-top:0;}
	.dataTables_filter{ display:none;}
	
	.btn.btn-info {
		vertical-align:baseline;
		margin-top:0;
	}

	.ui-datepicker-calendar {
		display: none;
	 }

	.pRow{
		display:block;
		position:relative;
		text-align:right;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<label>년도</label>
					<input id="input_date" name="input_date" type="text"  class="form-control" style="width:60px;"/>
				</div>
			</div>
			
			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<label>
						<input type="checkbox" name="srtCheck" class="srtCheck" id="srtCheck" />
						금액 표시
					</label>
				</div>
			</div>
			
			<div class="col-md-3 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="거래처 검색" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="회비관리" width="100%">
				<caption>회비관리</caption>
				<thead>
					<tr>
						<th width="30px">순번</th>
						<th width="80px">상호</th>
						<th width="65px">1월</th>
						<th width="65px">2월</th>
						<th width="65px">3월</th>
						<th width="65px">4월</th>
						<th width="65px">5월</th>
						<th width="65px">6월</th>
						<th width="65px">7월</th>
						<th width="65px">8월</th>
						<th width="65px">9월</th>
						<th width="65px">10월</th>
						<th width="65px">11월</th>
						<th width="65px">12월</th>
						<th width="65px">합계</th>
						<th width="auto"></th>
						
					</tr>
				</thead>
				<tfoot>
					<tr>
						
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>

						
					</tr>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		var rowSum = 0.0;
		var AllrowSum = 0.0;
		var start = moment();
		var end = moment();
		
		$("#input_date").val(  moment().format('YYYY') );
		$('#input_date').datepicker({
			changeYear: true,
			showButtonPanel: true,
			closeText: "확인",
			currentText: '올해', 
			dateFormat: 'yy',
			onClose: function(dateText, inst) {
				var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(year, 1));

				$("input[name='input_date']").trigger('change');
			}
		});

		$("#input_date").focus(function () {
			$(".ui-datepicker-month").hide();
		});
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/chitinfo/chitinfo/getlistFee",
				data:function(d){
					d.year			= $("#input_date").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.ACCOUNT_MK	= '01';
				}
			},
			order: [[ 0, "ASC" ]],
			fnRowCallback: function(nRow, nData){
				

				$(nRow).find("span.pRow").each(function(){	
					rowSum += $.isNumeric( removeCommas( $(this).html())) ? parseFloat(removeCommas($(this).html())) : 0.0;
				})
				$(nRow).find("td:nth-child(15)").html(Number(rowSum).format());
				AllrowSum += rowSum;
				rowSum = 0.0;
				
			},
			fnDrawCallback: function () {
				AllrowSum = 0.0;

				if( $("#srtCheck").is(":checked") ){
					$("span.pRow").css("display", "block");
				}else{
					$("span.pRow").css("display", "none");
				}
			},
				
			columns: [
				
				{ data: 'IDX', name: 'IDX', className: "dt-body-right"},
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-left"},
				
				{
					data: 'A1',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M1 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A2',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M2 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A3',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M3 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A4',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M4 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A5',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M5 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A6',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M6 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A7',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M7 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A8',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M8 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A9',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M9 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A10',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M10 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A11',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M11 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'A12',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return row.M12 + "<span class='pRow'>" + Number(data).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'IDX',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					className: "dt-body-right red"
				},
				
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			footerCallback: function ( row, data, start, end, display ) {

				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
			
				// 1월별 총합계
				M1 = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(M1.format()+"원").css({'text-align':'right'});
				
				M2 = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(M2.format()+"원").css({'text-align':'right'});

				M3 = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(M3.format()+"원").css({'text-align':'right'});

				M4 = api.column( 5 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 5 ).footer() ).html(M4.format()+"원").css({'text-align':'right'});

				M5 = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 6 ).footer() ).html(M5.format()+"원").css({'text-align':'right'});

				M6 = api.column( 7 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 7 ).footer() ).html(M6.format()+"원").css({'text-align':'right'});

				M7 = api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 8 ).footer() ).html(M7.format()+"원").css({'text-align':'right'});

				M8 = api.column( 9 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 9 ).footer() ).html(M8.format()+"원").css({'text-align':'right'});

				M9 = api.column( 10 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 10 ).footer() ).html(M9.format()+"원").css({'text-align':'right'});

				M10 = api.column( 11 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 11 ).footer() ).html(M10.format()+"원").css({'text-align':'right'});

				M11 = api.column( 12 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 12 ).footer() ).html(M11.format()+"원").css({'text-align':'right'});

				M12 = api.column( 13 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 13 ).footer() ).html(M12.format()+"원").css({'text-align':'right'});


				SUM = api.column( 14 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 14 ).footer() ).html("총합계 : " + AllrowSum.format()+"원").css({'text-align':'right'});
				

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});
		
		$("#srtCheck").on("click", function(){
			if( $(this).is(":checked") ){
				$("span.pRow").css("display", "block");
			}else{
				$("span.pRow").css("display", "none");
			}
		});

		$("#input_date").change(function(){
			oTable.draw() ;
			
		});
		//검색하기
		$("#search-btn").on("click",function(){
			oTable.draw();
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 비용계정그룹 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#tblList tbody').on('click', '.btnDelete',function () {

			var rowData = oTable.row( $(this).parents('tr') ).data();
			$(".content > .alert").remove();
			
			//console.log(rowData);
			var seq	= rowData.SEQ;
			var wt	= rowData.WRITE_DATE;
			
			if( seq == null || wt == null){
				$(".content").prepend( getAlert('warning', '경고', "해당 금전출납내역은 비어 있습니다.") ).show();
				return false;
			}

			if(confirm("해당 금전출납내역을 삭제하시겠습니까?")){
				$.getJSON("/chitinfo/chitinfo/_delete", {
					SEQ : rowData.SEQ ,
					WRITE_DATE : rowData.WRITE_DATE ,
					_token : '{{ csrf_token() }}'
				}, function (xhr) {
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if (data.result == "success") {
						oTable.draw();
					}
				}, function(xhr){

				});
			}
			
		});

	});

</script>

@stop
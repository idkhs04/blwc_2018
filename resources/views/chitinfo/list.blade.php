@extends('layouts.main')
@section('class','비용관리')
@section('title','금전출납부관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ vertical-align:baseline;}
	.btnSearch{ margin-top:0;}
	.dataTables_filter{ display:none;}
	#modal_insert label, #modal_update label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_insert .form-control, #modal_update .form-control{
		display:inline-block;
		width:150px;
		clear:both;
	}
	.slideSearchAccount, .slideSearchCust{ display:none; margin-top:5px; }
	.btn.btn-info {
		vertical-align:baseline;
		margin-top:0;
	}

	tr.IN { color:blue;}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnUpdateAccountGrp" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDeleteAccountGrp" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnAddAccountGrp" ><i class="fa fa-plus-circle"></i> 추가</button>
					<button type="button" class="btn btn-success btnPdf" ><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>

			<div class="col-md-3 col-xs-12">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<label>작성일</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="비용계정그룹" width="100%">
				<caption>비용계정그룹</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th width="45px">
							@role('CashBook') 
							납부일자
							@else
							작성일자
							@endrole
						</th>
						<th width="80px">계정명</th>
						<th width="80px">적요</th>
						<th width="80px">입금</th>
						<th width="80px">출금</th>
						<th width="80px">기간잔액</th>
						<th width="60px">총잔액</th>
						<th width="80px">
							@role('CashBook') 
							수금일자
							@else
							비고
							@endrole
						</th>
						<th width="auto"></th>
					</tr>
				</thead>
				
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		var start = moment();
		var end = moment();
		
		var sumAmt	 = 0;
		// 초기값 세팅 ★★★★ 초기값 아직 지정X ★★★★
		// 입고등록 > 입고일
		$("#modal_insert_date").val(end.format('YYYY-MM-DD'));

		function cbSetDate(start, end) {
			sumAmt	 = 0;
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 입고일
		$("#modal_insert_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/chitinfo/chitinfo/listData2",
				data:function(d){
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.accountGrp	= $("select[name='accountGrp'] option:selected").val();
					d.custGrp		= $("select[name='custGrp'] option:selected").val();
					 
				}
			},
			order: [[ 0, "desc" ], [ 1, "desc" ]],
			fnRowCallback: function(nRow, nData){

				if( nData.DE_CR_DIV == 1){
					$(nRow).addClass('OUTPUT');
					sumAmt -= parseInt(nData.AMT);
				}else{
					$(nRow).addClass('IN');
					sumAmt += parseInt(nData.AMT);
				}
				$(nRow).find("td:nth-child(7)").html(Number(sumAmt).format()+ "원");
				
			},
			initComplete : function(settings, json){
				sumAmt = 0;
			},
			columns: [
				{
					data:   "SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE', className: "dt-body-center"},
				{
					data: 'ACCOUNT_NM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					data: 'FRNM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data + " " + row.OUTLINE 
						}
						return data;
					},
					className: "dt-body-left"
				},

				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.DE_CR_DIV == "0"){
								return Number(data).format() +"원";
							}
							
						}
						return '';
					},
					className: "dt-body-right"
				},
				
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.DE_CR_DIV == "1"){
								return Number(data).format() +"원";
							}
							
						}
						return '';
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(sumAmt).format() +"원";
						}
						return '';
					},
					className: "dt-body-right"
				},
				{
					data: 'TOT_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() +"원";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'REMARK', name: 'REMARK', className: "dt-body-left" },
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총입금금액
				UnclAmt1 = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(UnclAmt1.format()+"원").css({'text-align':'center'});

				// 총지출금액
				UnclAmt2 = api.column( 5 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				$( api.column( 6 ).footer() ).html(UnclAmt2.format()+"원").css({'text-align':'center'});

				// 수입금액
				//UnclAmt3 = (UnclAmt1-UnclAmt2);
				//$( api.column( 6 ).footer() ).html("총 : "+UnclAmt3.format()+"원").css({'text-align':'center'});
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		//검색하기
		$("#search-btn").on("click",function(){
			oTable.draw();
		});

		//검색하기[값변경시]
		$("input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw();
			sumAmt = 0;
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw();
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 금전출납부 추가버튼
		$("#btnAddAccountGrp").click(function(){ 
			$("#mode").val("ins"); 
			$("#modal_insert_date").val(end.format('YYYY-MM-DD'));
			$('#modal_insert').modal({show:true});
			
			setTimeout(function(){
				$("input[name='ACCOUNT_NM']").focus();
			},800);
		});


		// 금전출납부 수정모달
		$("#btnUpdateAccountGrp").click(function(){

			var rowData = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();
			console.log(rowData);
			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend(  getAlert('warning', '경고', "수정할 전표를 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".content").prepend(  getAlert('warning', '경고', "수정할 전표는 한개만 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length == 1){
				$.getJSON("/chitinfo/chitinfo/getData", {
					_token : '{{ csrf_token() }}',
					'WRITE_DATE' : rowData.WRITE_DATE,
					'SEQ'		: rowData.SEQ,
					'CUST_MK'	: rowData.CUST_MK

				},function(data){
					var data = data[0];
					$("#mode").val("upd");
					//$('#modal_insert h4').text("비용계정 수정")

					$('#modal_insert').modal({show:true});

					$("#modal_insert input[name='OLD_WRITE_DATE']").val(data.WRITE_DATE);
					$("#modal_insert input[name='WRITE_DATE']").val(data.WRITE_DATE);
					$("#modal_insert input[name='ACCOUNT_MK']").val(data.ACCOUNT_MK);
					$("#modal_insert input[name='ACCOUNT_NM']").val(data.ACCOUNT_NM);
					$("#modal_insert input[name='CUST_MK']").val(data.CUST_MK);
					$("#modal_insert input[name='CUST_NM']").val(data.FRNM);
					$("#modal_insert input[name='OUTLINE'][value='" + data.OUTLINE + "']").prop("checked", true);
					$("#modal_insert input[name='DE_CR_DIV'][value='" + data.DE_CR_DIV + "']").prop("checked", true);
					$("#modal_insert input[name='AMT']").val(Number(data.AMT).format());
					$("#modal_insert textarea[name='REMARK']").val(data.REMARK);
					$("#modal_insert input[name='SEQ']").val(data.SEQ);

					
					setTimeout(function(){
						$("input[name='ACCOUNT_NM']").focus();
					},800);
				});
			}
		});

		// 비용계정그룹 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#btnDeleteAccountGrp').click( function () {
			var rowData = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();
			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 금전출납내역을 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 금전출납내역은 한개만 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length == 1){
				if(confirm("선택된 금전출납내역을 삭제하시겠습니까?")){
					$.getJSON("/chitinfo/chitinfo/_delete", {
						SEQ : rowData.SEQ ,
						WRITE_DATE : rowData.WRITE_DATE ,
						_token : '{{ csrf_token() }}'
					}, function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							oTable.draw();
						}
					}, function(xhr){

					});
				}
			}
		});

		// 거래처 조회
		$("#btnSearchCust").click(function(){
			$( ".slideSearchCust" ).show(function(){
				var cTable = $('#tblCustList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					bLengthChange: false,
					bInfo:false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='CUST_NM']").val();
							//d.notJ			= 'J';
							d.corp_div		= $("select[name='CORP_DIV'] option:selected").val();
							d.cust_grp_cd	= $("#CUST_GRP").val();
						}
					},
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' , className: "dt-body-center"},
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' ,className: "dt-body-center"},
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();

					$("#modal_insert_cust_mk").val(data.CUST_MK);
					$("#modal_insert_cust_nm").val(data.FRNM);
					//$("input[name='strSearchCust']").val('');
					$(".slideSearchCust" ).slideUp();
				});
				
				// 거래처 구분 변경시 : 2017-07-04 (김현섭)
				$('#CORP_DIV').on('change', function () {
					cTable.draw() ;
				});

				// 거래처 그룹 선택 변경시 : 2017-07-04(김현섭)
				$('#CUST_GRP').on('change', function () {
					cTable.draw() ;
				});
	
				$("input[name='CUST_NM']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			});

			setTimeout(function(){
				$(".slideSearchCust input[name='CUST_NM']").focus();
			},500);

		});

		// 거래처 검색 닫기
		$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });

		// 회사계정코드 조회
		$("#btnSearchAccount").click(function(){

			$( ".slideSearchAccount" ).show(function(){
				var aTable = $('#tblAccountList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					bLengthChange: false,
					bInfo:false,
					ajax: {
						url: "/chitinfo/chitinfo/searchAccountList",
						data:function(d){
							d.textSearch	= $("input[name='ACCOUNT_NM']").val();
						}
					},

					columns: [
						{ data: 'ACCOUNT_GRP_CD', name: 'A.ACCOUNT_GRP_CD' , className: "dt-body-center"},
						{ data: '.ACCOUNT_GRP_NM', name: 'B.ACCOUNT_GRP_NM' , className: "dt-body-center"},
						{ data: 'ACCOUNT_NM', name: 'ACCOUNT_NM' },
						{ data: 'DC_NM',  name: 'DC_NM', className: "dt-body-center" },
						{
							data: "ACCOUNT_GRP_CD",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblAccountList tbody').on( 'click', 'button', function () {
					var data = aTable.row( $(this).parents('tr') ).data();
					
					$("#modal_insert input[name='ACCOUNT_MK']").val(data.ACCOUNT_MK);
					$("#modal_insert input[name='ACCOUNT_NM']").val(data.ACCOUNT_NM);
					//$("input[name='ACCOUNT_NM']").val('');
					
					// 계정코드의 차변(수입)/대변(지출)에 따라 자동 선택
					$("#modal_insert input[name='DE_CR_DIV']").prop("checked", false);
					if( data.DE_CR_DIV == "1"){
						$("#modal_insert input[value='0']").prop("checked", true);
					}else{
						$("#modal_insert input[value='1']").prop("checked", true);
					}
					
					$(".slideSearchAccount" ).slideUp();
				});

				$("input[name='ACCOUNT_NM']").on("keyup", function(){
					aTable.search($(this).val()).draw() ;
				});
			});
			
			setTimeout(function(){
				$("input[name='ACCOUNT_NM']").focus();
			},800);
			
		});

		$('#modal_insert').on('hidden.bs.modal', setInitModalClose);

		// 비용전표 추가 모달 닫기
		$(".btn_AccountClose").click(function(){ $("div.slideSearchAccount").hide(); });

		// 비용전표 저장 버튼 클릭
		$("#btnSaveChitInfo").click(function(){
			
			@role('CashBook') 
			
			if( $("#modal_insert textarea[name='REMARK']").val() == ""){
				$("#modal_insert textarea[name='REMARK']").val(moment().format('MM/DD'));
			}

			@endrole
			$.blockUI({
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() {

					var mode = $("#mode").val();
					//console.log( $("#modal_insert input[name='DE_CR_DIV']:checked").val());
					var paramJSON = {
						_token		: '{{ csrf_token() }}'
						, OLD_WRITE_DATE: $("#modal_insert input[name='OLD_WRITE_DATE']").val()
						, WRITE_DATE: $("#modal_insert input[name='WRITE_DATE']").val()
						, DE_CR_DIV	: $("#modal_insert input[name='DE_CR_DIV']:checked").val()
						, CUST_MK	: $("#modal_insert input[name='CUST_MK']").val()
						, ACCOUNT_MK: $("#modal_insert input[name='ACCOUNT_MK']").val()
						, OUTLINE	: $("#modal_insert input[name='OUTLINE']:checked").val()
						, AMT		: removeCommas($("#modal_insert input[name='AMT']").val())
						, REMARK	: $("#modal_insert textarea[name='REMARK']").val()
					};


					var url = "/chitinfo/chitinfo/insert";

					if( mode == "upd"){
						url = "/chitinfo/chitinfo/update";
						paramJSON.SEQ = $("#SEQ").val();
					}


					$.getJSON(url, paramJSON ).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						if( data.result == "success" ){
							$('#modal_insert').modal("hide");
							oTable.draw() ;
							$.unblockUI();
						}
					}).error(function(xhr){
						$.unblockUI();
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li>' + error.message + '</li>');
						}else{
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						$.unblockUI();
						info.slideDown();
					});

				}
			});

		});

		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$(".modal input[type='text']").val("");
			$(".modal input[name='OUTLINE'][type='radio']:eq(0)").prop('checked', true);
			$(".modal input[name='DE_CR_DIV'][type='radio']:eq(0)").prop('checked', true);
			$(".modal textarea").val("");
			$("div.slideSearchAccount").hide();
			$("div.slideSearchCust").hide();
			
			$("select[name='CORP_DIV'] option:eq(0)").prop("selected", true);
			$("select[name='CUST_GRP'] option:eq(0)").prop("selected", true);

		}

		$(".btnPdf").click(function(){

			var width=740;
			var height=720;

			var start_date		= $("#reportrange input[name='start_date']").val();
			var end_date		= $("#reportrange input[name='end_date']").val();

			var url = "/chitinfo/chitinfo/PdfDetail2?start_date=" + start_date + "&end_date=" + end_date;
			getPopUp(url , width, height);
		});
	});

</script>

<!-- 비용전표 정보관리-->
<div class="modal fade" id="modal_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 비용전표 작성</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<input type='hidden' id="mode" value="" />
				<input type='hidden' id="SEQ"  name="SEQ" value="" />
				<div class="form-group">
					<label for="modal_insert_date" class='mleft'><i class='fa fa-check-circle text-red'></i>
						@role('CashBook') 
						납부일자
						@else
						작성일자
						@endrole
					</label>
					<input type="text" class="form-control" id="modal_insert_date" name='WRITE_DATE' readonly="readonly" />
					<input type="hidden" id="modal_insert_date_old" name='OLD_WRITE_DATE' readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="ACCOUNT_MK"><i class='fa fa-check-circle text-red'></i> 계정코드</label>
					<input type="hidden" class="form-control" id="ACCOUNT_MK" placeholder="계정코드" name="ACCOUNT_MK" />
					<input type="text" class="form-control" id="ACCOUNT_NM" placeholder="계정코드명" name="ACCOUNT_NM" />
					<button type="button" name="searchCust" id="btnSearchAccount" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>

					<div class="form-group slideSearchAccount">
						<div class="box box-primary">
							<h5>
								<span class="glyphicon glyphicon-th-large"></span> 회사계정코드 조회
								<button type="button" class="btn btn-danger pull-right btn_AccountClose">
									<span class="glyphicon glyphicon-remove"></span> 닫기
								</button>
							</h5>

							<div class="box-body">
								<table id="tblAccountList" class="table table-bordered table-hover" summary="계정 목록">
									<caption>계정목록 조회</caption>
									<thead>
										<tr>
											<th width="70px" >그룹코드</th>
											<th width="120px" >그룹명</th>
											<th width="140px">계정명</th>
											<th width="70px">차/대구분</th>
											<th width="auto">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_insert_cust_mk"><i class='fa fa-check-circle text-red'></i> 거래처</label>
					<input type="hidden" class="form-control" id="modal_insert_cust_mk" name="CUST_MK" placeholder="거래처코드" readonly="readonly" />
					<input type="text" class="form-control" id="modal_insert_cust_nm" name="CUST_NM" placeholder="거래처명" />
					<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info" />
						<i class="fa fa-search"></i>
					</button>

					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5>
								<span class="glyphicon glyphicon-th-large"></span> 거래처 선택
								<button type="button" class="btn btn-danger pull-right btn_CustClose">
									<span class="glyphicon glyphicon-remove"></span> 닫기
								</button>
							</h5>
							<div class="box-header">
								<div class="form-group">
									
									<label for="CORP_DIV"><i class='fa fa-check-circle text-aqua'></i> 거래처 구분</label>
									<select class="form-control form-input-md" name="CORP_DIV" id="CORP_DIV">
										<option value="T">비용계정</option>
										<option value="J">활어조합</option>
										<option value="P">활어매입처</option>
										<option value="S">활어매출처</option>
									</select>

									<label for="CUST_GRP_CD"><i class='fa fa-check-circle text-aqua'></i> 거래처그룹</label>
									<select class="form-control form-input-md" name="CUST_GRP" id="CUST_GRP">
										<option value="ALL"> == 전체 ==</option>
										@foreach ($custGrp as $custgrp)
											<option value="{{ $custgrp->CUST_GRP_CD }}">{{ $custgrp->CUST_GRP_NM }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>거래처 조회</caption>
									<thead>
										<tr>
											<th width="30px">코드</th>
											<th width="100px">상호</th>
											<th width="60px">대표자</th>
											<th width="90px">전화번호</th>
											<th width="auto">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="OUTLINE_CASH"><i class='fa fa-circle-o text-aqua'></i> 적요</label>
					<input type="radio" id="OUTLINE_CASH"  name="OUTLINE" value="현금" checked='checked' /> 현금
					<input type="radio" id="OUTLINE_CARD"  name="OUTLINE" value="카드" /> 카드
					<input type="radio" id="OUTLINE_TONG"  name="OUTLINE" value="통장" /> 통장
				</div>

				<div class="form-group">
					<label for="DE_CR_DIV"><i class='fa fa-circle-o text-aqua'></i> 차대구분</label>
					<input type="radio" id="DE_CR_DIV1"  name="DE_CR_DIV" value="1" checked='checked' /> 지출금액
					<input type="radio" id="DE_CR_DIV0"  name="DE_CR_DIV" value="0" /> 수입금액
				</div>


				<div class="form-group">
					<label for="modal_insert_amt"><i class='fa fa-check-circle text-red'></i> 금액</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_insert_amt" name="AMT" placeholder="금액 입력" />원
				</div>

				<div class="form-group">
					<label for="modal_insert_remark"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					@role('CashBook') 
					<textarea  class="form-control cis-lang-ko" id="modal_insert_remark" name="REMARK" placeholder="수금월일 예) 01/11 : 생략시 오늘 날짜로 자동 저장됨" style="width:70%" ></textarea>
					
					@else
					<textarea  class="form-control cis-lang-ko" id="modal_insert_remark" name="REMARK" placeholder="적요입력" style="width:70%" ></textarea>
					@endrole
					
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>

				<button type="button" class="btn btn-success pull-right" id="btnSaveChitInfo">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>

@stop
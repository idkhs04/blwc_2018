@extends('layouts.main')
@section('class','비용관리')
@section('title','이월금 정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ vertical-align: baseline;margin-top: 0; margin-left:5px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
	#reportrange input[type='text'] {
		width:100px;
	}

	.dvClose{ vertical-align:baseline; padding-top:4px;}
</style>
<div>
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<div class="col-md-4 col-xs-6">
					<label>조회시작일자</label> 
					<input type='text' name="start_date" id="WRITE_DATE_ST" placeholder="조회시작일자" readonly="readonly" />
				</div>
				<div class="col-md-4 col-xs-6">					
					<label>기준일</label> 
					
					<div class="btn-group">
						<input type='text' name="CLOSE_DATE" id="CLOSE_DATE" class='form-control' placeholder="이월기준일" style="width:100px;float:left;"/>
						<button type="button" name="search" id="btnClose" class="btn btn-info btnClose"><i class="fa fa-lock"></i> 이월</button>
						<button type="button" class="btn btn-success btnAddBringamtgrant" ><i class="fa fa-plus"></i> 추가</button>
					
					</div>
					
				</div>
				<div class="col-md-4 col-xs-6 dvClose">					
					<label>마지막 마감 : </label> 
					<span class="textClose"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<div class="edit_alert_main"><ul></ul></div>
				<table id="tblList" class="table table-bordered table-hover display nowrap" summary="이월금 정보" width="100%">
					<caption>이월금 정보</caption>
					<thead>
						<tr>
							<th class="check" width="15px"></th>
							<th width="50px">작성일자</th>
							<th width="60px">이월금액</th>
							<th width="auto"></th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		var start = moment().subtract(6, 'days');
		var end = moment();
		
		// 초기값 세팅 ★★★★ 초기값 아직 지정X ★★★★
		// 입고등록 > 입고일 
		
		//$("#WRITE_DATE_ST").val(end.format('YYYY-MM-DD'));
		
		function cbSetDate(start, end) {
			//$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			//$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
//			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		$("#WRITE_DATE_ST, #CLOSE_DATE").daterangepicker({
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			singleDatePicker: true,
			changeMonth: true,
			changeYear: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});


		//이월금 정보 추가 : 작성일자
		$("#WRITE_DATE_IN").daterangepicker({
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			singleDatePicker: true,
			changeMonth: true,
			changeYear: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		// 이월금 추가 팝업 초기값(오늘날짜)
		$("#WRITE_DATE_IN").val(end.format('YYYY-MM-DD'));
		$("#CLOSE_DATE").val(end.format('YYYY-MM-DD'));
		getBrignAmt();
		function getBrignAmt(){
			$.getJSON("/bringamtgrant/bringamtgrant/getBringAmt", []).success(
				function(data){

					console.log(data);
					item = data[0];
					
					$(".textClose").html(item.WRITE_DATE + " (" + Number(item.BRING_AMT).format() + " 원)");

				}
			).fail(function(xhr){
				$(".main_alertMsg").html( getAlert('danger', '경고', "마지막 이월일 불러오기에 실패했습니다.") ).show();
			});		
		}
		
		
		//이월금 정보 저장버튼
		$("#btnInsertAccountGrpOK").click(function(){
			$.getJSON('/bringamtgrant/cust_grp_info/Insert', {
				_token			: '{{ csrf_token() }}'
				, BRING_AMT		: removeCommas($("#BRING_AMT_IN").val())
				, WRITE_DATE	: $("#WRITE_DATE_IN").val()
			}).success(function(xhr){
				getBrignAmt();
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$('#modal_Bringamtgrant_IN').modal("hide");
				$("#modal_Bringamtgrant_IN .form-group > input").val("");
				$("#modal_Bringamtgrant_IN ul").empty();
				oTable.draw();
				
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_Bringamtgrant_IN .edit_alert');
				info.hide().find('ul').empty();
				
				if(error.reason == "PrimaryKey"){
					info.find('ul').append('<li>' + "같은일자에 이월금정보가 있습니다." + '</li>');
				}else{					
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		
		
		// 이월금 정보 수정
		$("#btnUpdateOKBringamtgrant").click(function(){
			$.getJSON('/bringamtgrant/cust_grp_info/Update', {
				_token		: '{{ csrf_token() }}'
				, BRING_AMT	: removeCommas($("#BRING_AMT").val())
				, WRITE_DATE	: $("#WRITE_DATE").val()
			}).success(function(xhr){
				getBrignAmt();
				$('#modal_Bringamtgrant').modal("hide");
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from 
				if(error.reason == "PrimaryKey"){
					alert("계정그룹코드가 있습니다.");
					return false;
				}else{
					var info = $('#modal_Bringamtgrant .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				}
			});
		});

		// 이월금 정보 리스트
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 10,		// 기본 10개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/bringamtgrant/custcd/listData",
				data:function(d){
					d.start_date	= $("input[name='start_date']").val();
				}
			},
			order: [[ 1, 'desc' ]],
			columns: [
				{
					data:   "WRITE_DATE",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' ,className: "dt-body-center"},
				{
					data: 'BRING_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format()+"원";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'WRITE_DATE', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return	"<button type='button' class='btn btn-info btnUpdateBringamtgrant'>수정</button>" +
									" <button type='button' class='btn btn-danger btnDeleteBringamtgrant'>삭제</button>";
						}
						return data;
					},
					className: "dt-body-left"
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		$("select[name='srtCondition'], input[name='start_date']").on("change", function(){
			oTable.draw() ;
		});
	
		
		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		
		//이월금 정보 추가버튼
		$("body").on("click", ".btnAddBringamtgrant", function(){ 
			// 이월금 추가 팝업 초기값(오늘날짜)
			$("#WRITE_DATE_IN").val(end.format('YYYY-MM-DD'));
			$('#modal_Bringamtgrant_IN').modal({show:true}); 
		});

		//이월금추가 닫기 버튼
		$("#modal_Bringamtgrant_IN .btn-danger").click(function(){ 
			$("#modal_Bringamtgrant_IN .form-group > input").val("");
			$("#modal_Bringamtgrant_IN ul").empty();
		});


		// 이월금 정보 수정모달
		$("#tblList tbody").on("click", ".btnUpdateBringamtgrant", function(){
			
			var arr_code = [];
			var row = oTable.row( $(this).parents('tr') ).data();

			arr_code.push(row.WRITE_DATE);

			if( arr_code.length == 1){
				var page = $("input[name='page']").val()	
				$.getJSON("/bringamtgrant/cust_grp_info/Edit/" + arr_code[0] + "/page=" + page+"", {
					_token : '{{ csrf_token() }}'
				},function(data){
					$("#BRING_AMT").val(Number(data.BRING_AMT).format());
					$("#WRITE_DATE").val(data.WRITE_DATE);
					$('#modal_Bringamtgrant').modal({show:true});
				});
			}
		});

		
		// 비용계정그룹 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$("#tblList tbody").on("click", '.btnDeleteBringamtgrant', function () {

			var arr_code = [];
			var row = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(row.WRITE_DATE);

			if( arr_code.length == 1){

				if(confirm("선택된 내용을 삭제하시겠습니까?")){

					$.getJSON("/bringamtgrant/cust_grp_info/Delete", {
						cd : arr_code ,
						_token : '{{ csrf_token() }}'
					}, function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							for(var code in data.code){
								oTable.row('.data-code').remove().draw(false);
							}
							
						}
					}, function(xhr){
						console.log(xhr)
					});
				}
			}
		});

		// 마감버튼 클릭
		$(".btnClose").click(function(){
			
			var DE_CR_DIV	= $("#modal_unprov_insert input[name='DE_CR_DIV']:checked").val();
			var mode = $("#mode").val();
			var url = "/bringamtgrant/bringamtgrant/setClose";

			var JsonData =  {
				_token			: '{{ csrf_token() }}',
				CLOSE_DATE		: $("input[name='CLOSE_DATE']").val() ,
			};

			$.getJSON(	 url
					   , JsonData
			
			).success(function(xhr){
				getBrignAmt();

				var data = jQuery.parseJSON(JSON.stringify(xhr));
				oTable.draw() ;
				
				$(".content > .alert").remove();
				$(".content").prepend( getAlert('info', '알림', "이월이 처리되었습니다.") ).show();
	

			}).fail(function(xhr){
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert_main');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();
				
			});
		});
	
	});

</script>

<!-- 이월금 정보 추가-->
<div class="modal fade" id="modal_Bringamtgrant_IN" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 이월금 추가</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="WRITE_DATE_IN"><i class='fa fa-check-circle text-red'></i> 작성일자</label>
					<input type="text" class="form-control" id="WRITE_DATE_IN" placeholder="" name="WRITE_DATE_IN" value=""/>
				</div>
				<div class="form-group">
					<label for="BRING_AMT_IN"><i class='fa fa-check-circle text-red'></i> 이월금액</label>
					<input type="text" class="form-control numeric" id="BRING_AMT_IN" placeholder="" name="BRING_AMT_IN" value="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="submit" class="btn btn-success" id="btnInsertAccountGrpOK">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 이월금 정보 수정-->
<div class="modal fade" id="modal_Bringamtgrant" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 이월금 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="WRITE_DATE"><i class='fa fa-check-circle text-red'></i> 작성일자</label>
					<input type="text" class="form-control" id="WRITE_DATE" placeholder="" name="WRITE_DATE" value="" readonly="readonly"/>
				</div>
				<div class="form-group">
					<label for="BRING_AMT"><i class='fa fa-check-circle text-red'></i> 이월금액</label>
					<input type="text" class="form-control numeric" id="BRING_AMT" placeholder="" name="BRING_AMT" value="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="submit" class="btn btn-success" id="btnUpdateOKBringamtgrant">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.main')
@section('class','매입관리')
@section('title','수조 로그')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	.dataTables_filter{ display:none;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-2 col-xs-6">

				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<ul class="nav nav-tabs" id="tabContent">
				<li class="active firstTab"><a href="#first" data-toggle="tab">수조로그</a></li>
				<li class=""><a href="#second" data-toggle="tab">입고로그</a></li>
				<li class=""><a href="#third" data-toggle="tab">미지급로그</a></li>
				<li class=""><a href="#fourth" data-toggle="tab">판매로그</a></li>
			</ul>
			<div class="tab-content" id="tab_area">
				<div class="tab-pane firstTab active" id="first">
					<table id="tblList" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="수조 목록 로그" width="100%">
						<caption>수조 관리</caption>
						<thead>
							<tr>
								<th class="name" width="20px">순번</th>  
								<th class="name">구분</th>  
								<th class="subject">수조</th>
								<th class="name">어종</th>  
								<th class="name">규격</th>  
								<th class="name">입고일</th>  
								<th class="name">수량</th>  
								<th class="name">단가</th>  
								<!--<th class="name">원산지코드</th> -->
								<th class="name">원산지</th>  
								<th class="name">비고</th>  
								<th class="name">입력</th>  
								<th class="name">수정</th>  
								<th class="name">삭제</th>  
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="second">
					<table id="tblListStockSujo" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="재고 목록 로그" width="100%">
						<caption>입고관리 로그</caption>
						<thead>
							<tr>
								<th></th>
								<th class="name">어종</th>  
								<th class="name">규격</th>  
								<th class="name">입고일</th>  
								<th class="name">원산지</th>  

								<th class="name">기록일</th>  
								<!--<th class="subject">log_div</th>-->
								<th class="name">이전수량</th>  
								<th class="name">이전입고</th>  
								<th class="name">이전미수</th>  
								<th class="name">현재수량</th>  
								<th class="name">현재입고</th>  
								<th class="name">현재미수</th>  

							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="third">
					<table id="tblListUnprov" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="미지급금 목록 로그" width="100%">
						<caption>미지급 로그</caption>
						<thead>
							<tr>
								<th class="name" width="20px">순번</th>  
								<th class="name">구분</th>  
								<th class="subject">작성일</th>
								<th class="name">SEQ</th>  
								<!--<th class="name">PROV_DIV</th>  -->
								<th class="name">거래처</th>  
								<th class="name">어종</th>  
								<th class="name">수량</th>  
								<th class="name">규격</th>  
								<th class="name">STOCK_SEQ</th>  
								<th class="name">비고</th>  
								<th class="name">입력</th>  
								<th class="name">수정</th>  
								<th class="name">삭제</th>  
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="fourth">
					<table id="tblListStockL" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="재고 목록 로그" width="100%">
						<caption>판매 로그</caption>
						<thead>
							<tr>
								<th></th>
								<th class="name">기록일자</th>  
								<th class="name">구분</th>  
								<th class="subject">기록자ID</th>
								<th class="name">거래업체</th>  
								<th class="name">판매일자</th>  
								<th class="name">이전금액</th>  
								<th class="name">이후금액</th>  
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		

		$("#btnList").click(function(){
			location.href="/buy/sujo/index";
		});
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 1000,		// 기본 100개 조회 설정
			ajax: {
				url: "/buy/sujo/listLog",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			order: [[ 0, "desc" ]],
			columns: [
				{ data: 'IDX', name: 'IDX' , className: "dt-body-center" },
				{ data: 'TYPE_NM', name: 'TYPE_NM' , className: "dt-body-center" },
				{ data: 'SUJO_NO', name: 'SUJO_NO' , className: "dt-body-center"},
				{ data: 'PIS_NM', name: 'PIS_NM' },
				{ data: 'SIZES', name: 'SIZES' },
				{ data: 'INPUT_DATE', name: 'INPUT_DATE' , className: "dt-body-center"},
				{ 
					data: 'QTY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'SALE_UNCS', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				//{ data: 'ORIGIN_CD', name: 'ORIGIN_CD' },
				{ data: 'ORIGIN_NM', name: 'ORIGIN_NM' , className: "dt-body-center"},
				{ data: 'REMARK', name: 'REMARK' },
				{ data: 'WRITE_DT', name: 'WRITE_DT' , className: "dt-body-center" },
				{ data: 'UPDATE_DT', name: 'UPDATE_DT' , className: "dt-body-center" },
				{ data: 'DELETE_DT', name: 'DELETE_DT' , className: "dt-body-center" },
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		
		// 입고(재고)로그
		var sTable = $('#tblListStockSujo').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 1000,		// 기본 100개 조회 설정
			ajax: {
				url: "/buy/sujo/listLog1",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			order: [[ 5, "desc" ]],
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'PIS_NM', name: 'PIS_NM' , className: "dt-body-center" },
				{ data: 'SIZES', name: 'SIZES' , className: "dt-body-center" },
				{ data: 'INPUT_DATE1', name: 'INPUT_DATE1' , className: "dt-body-center" },
				{ data: 'ORIGIN_NM', name: 'ORIGIN_NM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE2', name: 'WRITE_DATE2' , className: "dt-body-center" },
				//{ data: 'LOG_DIV', name: 'LOG_DIV' , className: "dt-body-center" },
				{ 
					data: 'BEFORE_QTY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},

				{ 
					data: 'BEFORE_REAL_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},

				{ 
					data: 'BEFORE_UNPROV', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},

				{ 
					data: 'QTY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'REAL_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'UNPROV', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
			
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		 $('#tblListStockSujo tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = sTable.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		function format ( d ) {
			// `d` is the original data object for the row
			return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:50%" >'+
					'<tr>'+
						'<th>입고일:</th>'+
						'<td>'+d.BEFORE_INPUT_DATE+'</td>'+
						'<th>출고일:</th>'+
						'<td>'+d.BEFORE_OUTPUT_DATE+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>원산지:</th>'+
						'<td>'+d.BEFORE_ORIGIN_NM+'</td>'+
						'<th>거래업체:</th>'+
						'<td>'+d.BEFORE_FRNM+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>중량:</th>'+
						'<td>'+d.BEFORE_QTY+'</td>'+
						'<th>입고단가:</th>'+
						'<td>'+d.BEFORE_UNCS+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>할인금액:</th>'+
						'<td>'+d.BEFORE_INPUT_DISCOUNT+'</td>'+
						'<th>입고금액:</th>'+
						'<td>'+d.BEFORE_AMT+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>입고(할인적용):</th>'+
						'<td>'+d.BEFORE_REAL_AMT+'</td>'+
						'<th>미지금급:</th>'+
						'<td>'+d.BEFORE_UNPROV+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>현금:</th>'+
						'<td>'+d.BEFORE_CASH+'</td>'+
						'<th>비고:</th>'+
						'<td>'+d.BEFORE_OUTLINE+'</td>'+
					'</tr>'+

				'</table>';
		}

		// 미수금 로그
		var uTable = $('#tblListUnprov').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 1000,		// 기본 100개 조회 설정
			ajax: {
				url: "/unprov/unprov/listLog",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{ data: 'IDX', name: 'IDX' , className: "dt-body-center" },
				{ data: 'TYPE_NM', name: 'TYPE_NM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center"},
				{ data: 'SEQ', name: 'SEQ' },
				//{ data: 'PROV_DIV', name: 'PROV_DIV' },
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'PIS_NM', name: 'PIS_NM' },
				{ 
					data: 'AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'STOCK_SIZES', name: 'STOCK_SIZES' , className: "dt-body-center"},
				{ data: 'STOCK_SEQ', name: 'STOCK_SEQ' , className: "dt-body-center"},
				{ data: 'REMARK', name: 'REMARK' },
				{ data: 'WRITE_DT', name: 'WRITE_DT' , className: "dt-body-center" },
				{ data: 'UPDATE_DT', name: 'UPDATE_DT' , className: "dt-body-center" },
				{ data: 'DELETE_DT', name: 'DELETE_DT' , className: "dt-body-center" },
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 판매로그
		var stTable = $('#tblListStockL').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 1000,		// 기본 100개 조회 설정
			ajax: {
				url: "/sale/sale/listLogM",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'LOG_DATE', name: 'LOG_DATE' , className: "dt-body-center" },
				{ data: 'LOG_DIV', name: 'LOG_DIV' , className: "dt-body-center" },
				{ data: 'MEM_ID', name: 'MEM_ID' , className: "dt-body-center" },
				{ data: 'FRNM', name: 'FRNM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center" },
				{ 
					data: 'BEFORE_TOTAL_SALE_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'TOTAL_SALE_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
			
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblListStockL tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			
			var row = stTable.row( tr );
			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var d = row.data();
				var listHtml = "";
				$.getJSON('/sale/sale/listLogD1', {
					_token		: '{{ csrf_token() }}'
					, LOG_DATE	: d.LOG_DATE
					, LOG_SEQ	: d.LOG_SEQ
					}).success(function(xhr){
						var list = jQuery.parseJSON(JSON.stringify(xhr.data));
						listHtml += "" +
							'<h4>이전내역</h4>' + 
							'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:100%" >' +
								'<thead><tr><th>어종명</th><th>규격</th><th>입고일</th><th>원산지</th><th>중량</th><th>단가</th><th>금액</th><th>비고</th></tr></thead>' + 
								'<tbody>';

						$.each(list, function(i){
							
							console.log( this.PIS_NM);
							listHtml +=  
							'<tr>'+
								'<td>'+list[i].PIS_NM+'</td>'+
								'<td>'+list[i].SIZES+'</td>'+
								'<td>'+list[i].WRITE_DATE+'</td>'+
								'<td>'+list[i].ORIGIN_NM+'</td>'+
								'<td>'+list[i].QTY+'</td>'+
								'<td>'+list[i].UNCS+'</td>'+
								'<td>'+list[i].AMT+'</td>'+
								'<td>'+list[i].REMARK+'</td>'+
							'</tr>';
						
						});
						listHtml += "</tbody></table>";

						row.child(listHtml).show();
						tr.addClass('shown');

				}).error(function(xhr){
					alert('error');
				});
				
				$.getJSON('/sale/sale/listLogD2', {
					_token		: '{{ csrf_token() }}'
					, LOG_DATE	: d.LOG_DATE
					, LOG_SEQ	: d.LOG_SEQ
				}).success(function(xhr){
					var list = jQuery.parseJSON(JSON.stringify(xhr).data);
					listHtml +='<h4>이후내역</h4>' + 
							'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:100%" >' +
								'<thead><tr><th>어종명</th><th>규격</th><th>입고일</th><th>원산지</th><th>중량</th><th>단가</th><th>금액</th><th>비고</th></tr></thead>' + 
								'<tbody>';

						$.each(list, function(i){
							listHtml +=  
							'<tr>'+
								'<td>'+ list[i].PIS_NM+'</td>'+
								'<td>'+list[i].SIZES+'</td>'+
								'<td>'+list[i].WRITE_DATE+'</td>'+
								'<td>'+list[i].ORIGIN_NM+'</td>'+
								'<td>'+list[i].QTY+'</td>'+
								'<td>'+list[i].UNCS+'</td>'+
								'<td>'+list[i].AMT+'</td>'+
								'<td>'+list[i].REMARK+'</td>'+
							'</tr>';
						
						});
						listHtml += "</tbody></table>";
						row.child(listHtml).show();
						tr.addClass('shown');;

				}).error(function(xhr){
					alert('error');
				});


			}
		} );


	});

</script>

@stop
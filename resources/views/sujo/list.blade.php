@extends('layouts.main')
@section('class','매입관리')
@section('title','수조별 입고등록')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	.btn-flat { vertical-align:top;}
	.mleft{margin-left:22px;}
	#tblList{ width:100%;}
	.dataTables_filter { display:none;}

	#modal_sujo_in label, #modal_sujo_move label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_sujo_move label:nth-child(3){
		padding-left:20px;
	}

	#modal_sujo_in .form-control, #modal_sujo_move .form-control{
		display:inline-block;
		width:130px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{ display:none; }
	.slideSearchCust {margin-top:5px;}
	#modal_sujo_in #modal_sujo_no_pis_mk{ width:78px; }
	#modal_sujo_in #modal_sujo_outline{ width:70%;}

	input.dt-body-right{ text-align:right;}
	.btn-info{ vertical-align:baseline; margin-top:0;}

	.dvAlert{ display:none; cursor:pointer;}

	.dataTables_length{ display:none;}

	.alert-warning {
		background-color: #fcf8e3;
		border-color: #faebcc;
		color: #8a6d3b;
	}

	#tblList td .btn{
		vertical-align:baseline;
		margin-right:5px;
	}

	table.dataTable tbody .btn{ padding:0px 5px;}
	.btn-sm{ margin:3px 5px 0 5px;}
	#tblList .th-body-left{ text-align:left; padding-left:7px;}
	.bold { display:inline-block; color:#357ca5; float:right;}
	.force-right label#force_right { margin-left: 256px; width: 80px;}
	.blue { font-weight:bold;}

	#tblList tbody tr.cselected {
		background-color: #99ffcc;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class='dvAlert alert alert-warning'></div>
			<table id="tblList" class="table table-bordered table-hover display nowrap markdown SujoList" summary="수조조회" width="100%">
				<thead>
					<tr>
						<th width="25px" id="step1">수조</th>
						<th width="160px">어종명</th>
						<th width="80px">매입처</th>
						<th width="45px">입고일</th>
						<th width="40px">수량</th>
						<th width="45px">판매단가</th>
						<th width="auto" class='th-body-left'>
							<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력보기</button>
							<button type="button" class="btn bg-orange-active color-palette" id="btnAddSujo" ><i class="fa fa-plus-circle"></i> 수조추가</button>
						</th>
						<th width="150px">체크</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	var oTable;
	$(function () {
		
		var start = moment().startOf('month');
		var end = moment();

		// 초기값 세팅
		// 입고등록 > 입고일
		$("#modal_sujo_input_date").val(end.format('YYYY-MM-DD'));

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 입고일
		$("#modal_sujo_input_date, #modal_stock_output_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
					fromLabel: "시작일",
					toLabel: "종료일",
					applyLabel: "확인",
					cancelLabel: "취소",
				}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			opens: "left",
			drops: "down",
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "",
			ranges: {
				'오늘': [moment(), moment()],
				'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
				'이번 달': [moment().startOf('month'), moment().endOf('month')],
				'지난 달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		oTable = $('#tblList').DataTable({
			processing	: true,
			serverSide	: true,
			iDisplayLength: 150,		// 기본 100개 조회 설정
			responsive: true,
			lengthChange: false,
			bInfo:false,
			ajax: {
				url: "/buy/sujo/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			fnRowCallback: function(nRow, nData){
				
				if( nData.IS_CHECK == "Y" && nData.CONF_CHK_SUJO == "Y"){
					$(nRow).addClass("cselected");
				}
				
			}
			,columns: [
				{ data: 'SUJO_NO', name: 'SUJO_NO', className: "dt-body-left"},
				{
					data:   "PIS_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== null){
								return "<a class='blue' href='/stock/stock/detail/" + row.PIS_MK + "/" + row.SIZES + "'>" + data + " <span class='sizes'>" + row.SIZES + "</span><span class='bold'>[" + row.ORIGIN_NM + "]</span></a>";
							}else{
								return "";
							}
						}
						return data;
					},
					className: "dt-body-left "
				},
				{
					data:   "CUST_LIST",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== null){

								return data.cut(14);
							}else{
								return "";
							}
						}
						return data;
					},
					className: "dt-body-left SUJO_CUST_YN"
				},

				{ data: 'INPUT_DATE', name: 'INPUT_DATE', className: "dt-body-center" },
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'SALE_UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data:  "SUJO_NO",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {

							var strBtnGroup  = 
								"<button type='button' class='btn bg-orange-active color-palette btnInSujo'>입고</button>" +
								"<button type='button' class='btn btn-info btnUpdateUncn'>단가수정</button>" +
								"<button type='button' class='btn bg-purple btnStockArranage'>재고정리</button>" +
								"<button type='button' class='btn btn-success btnMoveSujo'>이고</button>" +
								"<button type='button' class='btn bg-purple btnUpdate'>입고수정</button>";

							if( row.PIS_MK !== null && row.SIZES !== null){
								strBtnGroup += "<a href='/stock/stock/detail/" + row.PIS_MK + "/" + row.SIZES + "'>" +
									"<button class='btn bg-light-blue-active'>상세재고</button></a>" ;
							}else{
								strBtnGroup += "<a href='#'>" +
									"<button class='btn bg-light-blue-active'>상세재고</button></a>" ;
							}
							strBtnGroup	+= "<button type='button' class='btn btn-danger btnDelete'>삭제</button>";
							return strBtnGroup;
						}
						return data;
					},
					orderable:  false,
					className: "dt-body-left"
				},
				{
					data:  "IS_CHECK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.IS_CHECK == "Y"){
								return "<input type='checkbox' class='" + row.SUJO_NO + "' name='is_check' checked />"
							}else{
								return "<input type='checkbox' class='" + row.SUJO_NO + "' name='is_check' />"
							}
						}
						return data;
					},
					orderable:  false,
					visible:  false,
					className: "dt-body-center is_check"
				},

			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "[수조 추가] 버튼을 클릭하여 수조를 등록하세요",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		/*.on('xhr.dt', function ( e, settings, json, xhr ) {
			chkSession(xhr.responseText);
		});
		*/

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});



		// 판매단가 수정
		$("body").on("click", ".btnUpdateUncn", function(){

			var arr_code = [];
			var row = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(row.SUJO_NO);

			if( arr_code.length == 1){

				$.getJSON('/buy/sujo/SujoData', {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					var info = $(".dvAlert");
					info.empty();

					if (data.recordsTotal == 1) {

						if( data.data[0].INPUT_DATE === null || data.data[0].PIS_NM === null || data.data[0].PIS_NM == "null"){
							
							$(".content > .alert").remove();
							$(".content").prepend( getAlert('warning', '경고', "입고내역이 없으므로 단가를 수정할 수 없습니다") ).show();
							return;
						}

						$("#modal_sujo_no").val(data.data[0].SUJO_NO);
						$("#modal_pis_nm").val(data.data[0].PIS_NM + " [ "+  data.data[0].ORIGIN_NM + "] " + data.data[0].SIZES);
						$("#modal_input").val(data.data[0].INPUT_DATE);
						$("#modal_pis_uncn").val(Number(data.data[0].SALE_UNCS).format());
						$('#modal_Uncn').modal({show:true});

						setTimeout(function(){
							$("#modal_pis_uncn").focus();
							if( $("#modal_pis_uncn").val() == "0"){
								$("#modal_pis_uncn").val('');
							}
						},500);
					}
				});
			}
		});

		// 판매단가수정 확인
		$("#btnUpdateUNCN").click(function(){
			//if( confirm("판매단가를 수정하시겠습니까?")){
				$.getJSON('/buy/sujo/setDataUNCN', {
						  _token	: '{{ csrf_token() }}'
						, SUJO_NO	: $("#modal_sujo_no").val()
						, SALE_UNCS	: removeCommas( $("#modal_pis_uncn").val())
					}, function(data){
						$('#modal_Uncn').modal("hide");
						oTable.draw() ;
					}
				);
			//}
		});

		// 입고 버튼 클릭
		$("body").on("click", ".btnInSujo", function(){

			var arr_code = [];
			var row = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(row.SUJO_NO);

			if( arr_code.length == 1){

				$(".slideSearchPisMk, .slideSearchCust").hide();
				$.getJSON('/buy/sujo/getOrginCD', {
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					var $stations = $("select[name='origin_cd']");
					$stations.empty();
					//$stations.append("<option value='' >선택</option>");

					$.each(data, function(){
						$stations.append('<option  value="' + this.ORIGIN_CD +'">' + this.ORIGIN_NM + '</option>');
					});

					$stations.find("option[value='KR']").attr('selected', true);
				});

				$.getJSON('/buy/sujo/SujoData', {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if (data.recordsTotal >= 1) {

						// 입고는 수조입고날짜를 묻는 경우 생길때 데이터를 유지해야하므로
						// 팝업창이 뜰때 화면 데이터를 클리어 한다
						setInitModalClose();

						$("#modal_sujo_in_sujo_no").val(data.data[0].SUJO_NO);
						$("#modal_sujo_no_pis_mk").val(data.data[0].PIS_MK);
						$("#modal_sujo_in_pis_nm").val(data.data[0].PIS_NM);
						$("#modal_sujo_in_pis_sizes").val(data.data[0].SIZES);
						
						// 기존 수조에 원산지코드가 있는 경우 해당 원산지코드를 바인딩
						if( data.data[0].ORIGIN_CD != "" && data.data[0].ORIGIN_CD !== null && data.data[0].ORIGIN_CD != "null"){
							$("#origin_cd option[value='" + data.data[0].ORIGIN_CD + "']").attr('selected', true);
							
						}

						$("#modal_sujo_input_date").val(data.data[0].INPUT_DATE);

						if( data.data[0].PIS_MK == "null" || data.data[0].PIS_MK === null ){
							$("#btnSearchPisMk").removeAttr("disabled");
							$("#origin_cd").removeAttr("disabled");
						}else{
							$("#btnSearchPisMk").attr("disabled", "disabled");
							$("#origin_cd").attr("disabled", "disabled");
						}

						$("input[name='modal_stock_reason']:eq(0)").prop('checked', true);

			
						$('#modal_sujo_in').modal({show:true});
						$("#modal_pis_nm").val(data.data[0].PIS_NM + " [ "+  data.data[0].ORIGIN_NM + "] " + data.data[0].SIZES);
						
						// 입고등록 > 입고일
						$("#modal_sujo_input_date").val(end.format('YYYY-MM-DD'));
						//setValidation();

						setTimeout(function(){
							$("#modal_sujo_qty").focus();
						},500);

					}
				});
			}
		});

		// 입고정보 팝업 닫기
		//$('#modal_sujo_in').on('hidden.bs.modal', setInitModalClose);

		// 어종/규격 검색 버튼(입고 등록시 팝업)
		$("#btnSearchPisMk").click(function(){
			$( ".slideSearchPisMk" ).show(function(){
			
				setTimeout(function(){
					$("#strSearchPisMk").focus();
				},500);
				
				var pTable = $('#tblPisList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					bInfo:false,
					
					ajax: {
						url: "/buy/sujo/getPisCD",
						data:function(d){
							d.textSearch	= $("input[name='strSearchPisMk']").val();
						}
					},
					columns: [
						{ data: 'PIS_MK', name: 'PIS_INFO.PIS_MK' },
						{ data: 'PIS_NM', name: 'PIS_INFO.PIS_NM' },
						{ data: 'SIZES',  name: 'PIS_CLASS_CD.SIZES' },
						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><i class='fa fa-location-arrow'></i> 선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "좌측메뉴 > 코드관리 > 어종관리로 이동하여 어종을 추가하세요",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});
				/*.on('xhr.dt', function ( e, settings, json, xhr ) {
					chkSession(xhr.responseText);
				});
				*/

				$('#tblPisList tbody').on( 'click', 'button', function () {
					var data = pTable.row( $(this).parents('tr') ).data();

					$("#modal_sujo_no_pis_mk").val(data.PIS_MK);
					$("#modal_sujo_in_pis_nm").val(data.PIS_NM);
					$("#modal_sujo_in_pis_sizes").val(data.SIZES);
					$("input[name='strSearchPisMk']").val('');

					$(".slideSearchPisMk" ).hide();
					setTimeout(function(){
						$("#modal_sujo_qty").focus();
					},500);
				});

				$("input[name='strSearchPisMk']").on("keyup", function(){
					pTable.search($(this).val()).draw() ;
				});

			});
		});
		//어종검색 닫기
		$(".btn_PisClose").click(function(){ 
			$("div.slideSearchPisMk").hide(); 

			setTimeout(function(){
				$("#modal_sujo_qty").focus();
			},500);
		});

		// 거래처 조회
		$("#btnSearchCust").click(function(){

			// 거래처조회 창 보이기
			$( ".slideSearchCust" ).show(function(){
				
				setTimeout(function(){
					$("#strSearchCust").focus();
				},500);

				var cTable = $('#tblCustList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='strSearchCust']").val();
							d.corp_div		= 'P'
						}
					},
					order: [[ 1, 'asc' ]],
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' , className: "dt-body-center"},
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' ,className: "dt-body-center"},
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "좌측메뉴 > 코드관리 > 거래초코드로 이동하여 거래처를 먼저 추가하세요",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();

					$("#modal_cust_cust_mk").val(data.CUST_MK);
					$("#modal_cust_cust_nm").val(data.FRNM);
					$("input[name='strSearchCust']").val('');
					$(".slideSearchCust" ).slideUp();

					setTimeout(function(){
						$("#modal_sujo_outline").focus();
					},500);
				});

				$("input[name='strSearchCust']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			});
		});

		// 거래처 검색 닫기
		$(".btn_CustClose").click(function(){ 
			$("div.slideSearchCust").hide(); 
			setTimeout(function(){
				$("#modal_sujo_outline").focus();
			},500);
		});


		// 입고 저장 버튼
		$("#btnSaveSujoStock, #btnPickDateSujoStock").click(function(){

			$.blockUI({
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() {

					// 만약에 입고날짜가 다를 경우
					var input_date = $("#modal_sujo_in_date input[name='input_date']:checked").val() ;
					input_date = ( input_date == "" || input_date === undefined ) ? $("#modal_sujo_input_date").val() : input_date ;
					$("#origin_cd").removeAttr("disabled");

					$.getJSON('/buy/sujo/setSujoDataIn', {
						_token			: '{{ csrf_token() }}',
						SUJO_NO			: $("#modal_sujo_in_sujo_no").val() ,
						PIS_MK			: $("#modal_sujo_no_pis_mk").val() ,
						SIZES			: $("#modal_sujo_in_pis_sizes").val() ,
						ORIGIN_CD		: $("#origin_cd option:selected").val(),
						ORIGIN_NM		: $("#origin_cd option:selected").text(),
						INPUT_DATE		: input_date ,
						QTY				: removeCommas($("#modal_sujo_qty").val()),
						UNCS			: removeCommas($("#modal_sujo_uncn").val()),
						INPUT_DISCOUNT	: removeCommas($("#modal_sujo_discount").val()),
						CASH			: removeCommas($("#modal_sujo_cash").val()),
						INPUT_UNPROV	: removeCommas($("#modal_sujo_unprov").val()),
						SALE_UNCS		: removeCommas($("#modal_sujo_sale_uncn").val()),
						INPUT_AMT		: removeCommas($("#modal_sujo_input_amt").val()),
						CUST_MK			: $("#modal_cust_cust_mk").val(),
						CUST_NM			: $("#modal_cust_cust_nm").val(),
						OUTLINE			: $("#modal_sujo_outline").val(),

						OPTION_FORCE	: $("#force_date").val(),


					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if( data.result == 'fail'){
							if (data.type == 'error01'){
								$("#modal_sujo_in .modal-body").prepend("<p style='color:red'>" + data.message + "</p>");
								$.unblockUI();
							}else if (data.type == "error02" ){
								$("#recent_date").val( data.recent_date);
								$("label[for='recent_date'] span").text(data.recent_date);
								$("#new_date").val( data.new_date);
								$("label[for='new_date'] span").text(data.new_date);

								$.unblockUI();
								$("#modal_sujo_in").modal("hide");
								$("#modal_sujo_in_date" ).modal("show");
								$("#force_date").val(true);
							}
							$.unblockUI();
						}

						else if (data.result == "success") {
							$("#modal_sujo_in").modal("hide");
							$("#modal_sujo_in_date" ).modal("hide");
							oTable.draw() ;
							$.unblockUI();
						}

					}).error(function(xhr,status, response) {
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('#modal_sujo_in .edit_alert');
						info.hide().find('ul').empty();
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						info.slideDown();
						$.unblockUI();
					});
				}
			});
		});

		// 입고정보 계산식
		$(".sum_valid").on("focusout, focusin, keyup", function(){
			
			

			var qty			= $("#modal_sujo_qty").val().replace(/,/ig, '');		// 입고수량
			var input_uncn	= $("#modal_sujo_uncn").val().replace(/,/ig, '');		// 입고단가
			var input_amt	= $("#modal_sujo_input_amt").val().replace(/,/ig, '');	// 입고합계
			var discount	= $("#modal_sujo_discount").val().replace(/,/ig, '');	// 할인금액
			var real_amt	= $("#modal_sujo_real_amt").val().replace(/,/ig, '');	// 입고금액
			var cash		= $("#modal_sujo_cash").val().replace(/,/ig, '');		// 현금
			var unprov		= $("#modal_sujo_unprov").val().replace(/,/ig, '');		// 미지급금
			var sale_uncn	= $("#modal_sujo_sale_uncn").val().replace(/,/ig, '');	// 판매단가
			

			var T = Number( '1e' + 1 );
			a = Math.round( ( qty * input_uncn ) * T ) / T;
			a += 500;
			a = Math.floor( a / 1000 ) * 1000;

			input_amt		= a;
			real_amt		= input_amt - discount;
			unprov			= input_amt - discount - cash;

			if( discount > real_amt){
				$(".modal-body > .alert").remove();
				$(".modal-body").prepend( getAlert('warning', '경고', "할인금액은 입고금액보다 클수 없습니다") ).show();
				//$("#modal_sujo_discount").val(0);
				return false;
			}

			if( cash > qty * input_uncn - discount) {
				$(".modal-body > .alert").remove();
				$(".modal-body").prepend( getAlert('warning', '경고', "현금은 입고금액에서 할인금액을 뺀 값을 초과 할 수 없습니다") ).show();
				$("#modal_sujo_cash").val(0);
				return false;
			}

			$("#modal_sujo_input_amt").val(Number(input_amt).format());
			$("#modal_sujo_real_amt").val(Number(real_amt).format());
			$("#modal_sujo_unprov").val(Number(unprov).format());
		});


		// 수조추가 버튼
		$("#btnAddSujo").click(function(){ 
			$('#modal_Sujo').modal({show:true}); 
			
			setTimeout(function(){
				$("#modal_Sujo_sujo_no").focus();
			},500);
		});

		// 수조추가 닫기
		$('#modal_Sujo').on('hidden.bs.modal', setInitModalClose);

		// 수조추가 저장 버튼
		$("#btnInsertSujoOK").click(function(){

			$.blockUI({
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() {
					$.getJSON('/buy/sujo/addDataSujo', {
						_token		: '{{ csrf_token() }}'
						, SUJO_NO	: $("#modal_Sujo_sujo_no").val()
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						if( data.result == "success" ){
							$('#modal_Sujo').modal("hide");
							oTable.draw() ;
							$.unblockUI();
						}
					}).error(function(xhr){
						$.unblockUI();
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li>' + error.message + '</li>');
						}else{
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						$.unblockUI();
						info.slideDown();
					});
				}
			});
		});

		// 이고 버튼 클릭
		$("body").on("click", "#btnMoveSujo, .btnMoveSujo", function(){

			var arr_code = [];
			var row = null;

			if( $(this).attr("id") == "btnMoveSujo"){

				$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
					if ($(this).is(":checked")) {
						arr_code.push($(this).val());
					}
				});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "이고할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "이고는 한개만 선택해주세요") ).show();
				return false;
			}

			}else{
				var row = oTable.row( $(this).parents('tr') ).data();
				arr_code.push(row.SUJO_NO);
			}
			if( arr_code.length == 1){
				
				$.getJSON('/buy/sujo/SujoData', {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					var info = $(".dvAlert");
					info.empty();

					if( data.result == 'fail'){
						if (data.type == 'error03'){
							$("#modal_sujo_move_date .modal-body").prepend("<p style='color:red'>" + data.message + "</p>");

							$("#src_date").val( data.src_date);
							$("label[for='src_date'] span").text(data.src_date);
							$("#dst_date").val( data.dst_date);
							$("label[for='dst_date'] span").text(data.dst_date);

							$("#modal_sujo_move").modal("hide");
							$("#modal_sujo_in_date" ).modal("show");
							$("#force_date_03").val(true);
						}
					}

					if (data.recordsTotal == 1) {

						setInitModalClose();
						if( data.data[0].INPUT_DATE === null || data.data[0].PIS_NM === null){


							info.append("<div class='alert alert-warning fade in'>" +
											"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
											"<strong>Warning!</strong> 입고내역이 없으므로 이고를 할 수 없습니다" +
										"<div>");
							$("#modal_sujo_move").modal("hidden");
							return false;
						}

						$("#modal_sujo_move_no").val(data.data[0].SUJO_NO);
						$("#modal_sujo_move_pis_nm").val(data.data[0].PIS_NM + "  " + data.data[0].SIZES);
						$("#modal_sujo_move_orgin_nm").val(data.data[0].ORIGIN_NM);
						$("#modal_sujo_move_input").val(data.data[0].INPUT_DATE);
						$("#modal_sujo_move_qty").val(Number(data.data[0].QTY).format(1));
						$("#modal_pis_uncn").val(data.data[0].SALE_UNCS);

						$("#modal_sujo_move").modal("show");
						$("#modal_sujo_cust_mk").val(row.CUST_MK);

						setTimeout(function(){
							$("#modal_sujo_move_qty_minus").focus();
						},500);
					}
				});

				$.getJSON('/buy/sujo/getSujoEmpty', {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					var $move_selection = $("select[name='modal_sujo_move_select']");
					$move_selection.empty();
					$move_selection.append("<option value='' >선택</option>");

					$.each(data.data, function(){
						$move_selection.append('<option value="' + this.SUJO_NO +'">' + this.SUJO_NO + '</option>');
					});
				});
			}
		});

		// 이고 저장
		$("#btnMoveSujoSave, #btnSujoMoveForce").click(function(){

			var input_date = $("#modal_sujo_move_input").val();

			var QtyAll		= parseInt(removeCommas($("#modal_sujo_move_qty").val()));
			var QtyInput	= parseInt(removeCommas($("#modal_sujo_move_qty_minus").val()));

			//숫자인지 판별
			if( !$.isNumeric(QtyAll) || !$.isNumeric(QtyInput) ){
				$("#modal_sujo_move .modal-body p.msg").remove();
				$("#modal_sujo_move .modal-body").prepend("<p class='msg' style='color:red;'>" + "이고중량과 이고중량은 숫자형식이어야 합니다" +  "</p>");
				return false;
			}
			// 총중량보다 이고중량이 클경우 저장 제한
			if( QtyAll < QtyInput){
				$("#modal_sujo_move .modal-body p.msg").remove();
				$("#modal_sujo_move .modal-body").prepend("<p class='msg' style='color:red;'>" + "이고중량이 총중량보다 크면 안됩니다" + "</p>");
				return false;
			}

			// 만약에 이고시 입고날짜가 다를 경우
			if( $(this).attr("id") == "btnSujoMoveForce"){
				input_date = $("input[name='move_input_date']:checked").val() ;
			}

			input_date = ( input_date == "" || input_date === undefined ) ? $("#modal_sujo_input_date").val() : input_date ;
			var info = $('#modal_sujo_move .edit_alert');
			info.hide().find('ul').empty();

			$.getJSON('/buy/sujo/setMoveSujo', {
				SRC_SUJO_NO : $("#modal_sujo_move_no").val() ,
				DST_SUJO_NO : $("#modal_sujo_move_select option:selected").val(),
				OPTION_FORCE: $("#force_date_03").val(),
				INPUT_DATE	: input_date,
				QTY			: $("#modal_sujo_move_qty_minus").val(),
				CUST_MK		: $("#modal_sujo_cust_mk").val(),
				_token		: '{{ csrf_token() }}'
			}).success(function(xhr){
				$("#modal_sujo_move").modal("hide");
				$("#modal_sujo_move_date").modal("hide");
				oTable.draw() ;

			}).error(function(xhr,status, response) {

				var data = jQuery.parseJSON(JSON.stringify(xhr.responseJSON));

				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.

				var info = $('#modal_sujo_move .dvAlert');
				info.append("<ul></ul>");
				info.hide().find('ul').empty();

				if( data.result == 'fail'){
					if (data.type == 'EQUAL'){
						$("#modal_sujo_move .modal-body .msg").remove();
						$("#modal_sujo_move .modal-body").prepend("<p class='msg' style='color:red'>" + data.message + "</p>");
					}

					if (data.type == 'EQUAL_CUST'){
						$("#modal_sujo_move .modal-body .msg").remove();
						$("#modal_sujo_move .modal-body").prepend("<p class='msg' style='color:red'>" + data.message + "</p>");
					}

					if (data.type == 'error03'){
						$("#modal_sujo_move_date .modal-body").prepend("<p style='color:red'>" + data.message + "</p>");

						$("#src_date").val( data.src_date);
						$("label[for='src_date'] span").text(data.src_date);
						$("#dst_date").val( data.dst_date);
						$("label[for='dst_date'] span").text(data.dst_date);

						$("#modal_sujo_move").modal("hide");
						$("#modal_sujo_move_date" ).modal("show");
						$("#force_date_03").val(true);
					}else{
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.

						var info1 = $('#modal_sujo_move .edit_alert');
						info1.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info1.find('ul').append('<li>' + error.message + '</li>');
						}else{

							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info1.find('ul').append('<li>' + val + '</li>');
										return;
									});
								}
							}
						}
						
						info1.slideDown();
					}

				}else{
					$("#modal_sujo_move").modal("hide");
					oTable.row('.data-code').remove().draw(false);
				}

			});
		});

		// 수조체크 이벤트/이벤트 핸들러

		$("#tblList tbody").on("click", "input[type='checkbox']", function(){

			var row = $(this).parents('tr');

			var IS_CHECK = "Y";
			if( $(this).is(":checked")){
				if( !$(this).hasClass("cselected")){
					$(row).addClass("cselected");
				}
			}else{
				IS_CHECK = "N";
				$(row).removeClass("cselected");
			}
			
			$.getJSON('/buy/sujo/setCheckYN', {
				_token			: '{{ csrf_token() }}',
				"SUJO_NO"		: $(this).attr("class"),
				"IS_CHECK_YN"	: IS_CHECK
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
			});
		});


		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$("body").on("click", ".btnDelete", function(){

			var arr_code = [];
			var row = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(row.SUJO_NO);

			if(confirm("선택된 수조를 삭제하시겠습니까?")){

				$.getJSON("/buy/sujo/Delete", {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if (data.result == "success") {
						oTable.draw();
					}
				}).error(function(xhr,status, response) {

					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.

					var info = $('.dvAlert');
					info.append("<ul></ul>");
					info.hide().find('ul').empty();

					if( error.result == "DUPLICATION"){
						info.find('ul').append('<li>' + error.message + '</li>');
					}else{
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					}

					info.slideDown();
				});
			}
		});

		// 재고 수정/삭제 클릭
		var cTable;
		$("body").on("click", ".btnUpdate", function(){

			var row		= oTable.row( $(this).parents('tr') ).data();
			var eSu_no	= row.SUJO_NO;
			$("#tblStockUpdList").dataTable().fnDestroy();
			cTable = $('#tblStockUpdList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					bDestroy: true,
					bFilter: false,
					bInfo: false,
					ajax: {
						url: "/stock/stock/getStockDetailUpdateList",
						data:function(d){
							d.pis_mk		= row.PIS_MK;
							d.sizes			= row.SIZES;
							d.origin_cd		= row.ORIGIN_CD;
							d.origin_nm		= row.ORIGIN_NM;
							d.input_date	= row.INPUT_DATE;
							d.sujo_no		= row.SUJO_NO;
							d.cust_mk		= row.CUST_MK;
						}
					},
					columns: [
						{
							data: 'SUJO_NO',
							name: 'SUJO.SUJO_NO',
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									if( data == eSu_no){
										return "<span class='red'>" + data + "</span>";
									}else{
										return data;
									}
								}
								return data;
							},
							className: "dt-body-center"
						},
						{ data: 'FRNM', name: 'FRNM', className: "dt-body-left" },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'SIZES',  name: 'SIZES' },
						{ data: 'ORIGIN_NM',  name: 'ORIGIN_NM', className: "dt-body-center" },
						{ data: 'INPUT_DATE',  name: 'INPUT_DATE', className: "dt-body-center" },
						{
							data: 'QTY',
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return Number(data).format();
								}
								return data;
							},
							className: "dt-body-right"
						},
						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btnUpdateStock' ><span class='glyphicon glyphicon-on'></span>수정</button>" +
											" <button class='btn btn-warning btnRemoveStock' ><span class='glyphicon glyphicon-on'></span>삭제</button>" ;
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "<span class='red'>" + row.INPUT_DATE + "</span> 일자의 입고내역은 이고 후 수조번호가 변경되었으므로 상세재고조회에서 재고수정을 해주세요",
						"sZeroRecords": "<span class='red'>" + row.INPUT_DATE + "</span> 일자의 입고내역은 이고 후 수조번호가 변경되었으므로 상세재고조회에서 재고수정을 해주세요",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

			$("#modal_sujo_stock_update").modal("show");


		});

		// 수정
		$('#tblStockUpdList tbody').on( 'click', 'button', function () {

			var data = cTable.row( $(this).parents('tr') ).data();
			//console.log(data);

			// 재고수정 이동
			if( $(this).hasClass("btnUpdateStock")){
				location.href = "/stock/stock/detailEdit/" + data.PIS_MK + "/" + data.SIZES + "/" + data.SEQ + "/" + data.SUJO_NO ;

			}else{
			// 삭제
				$.getJSON('/stock/stock/removeStockInSujo', {
					'SUJO_NO'		: data.SUJO_NO,
					'PIS_MK'		: data.PIS_MK ,
					'SIZES'			: data.SIZES ,
					'SEQ'			: data.SEQ,
					_token			: '{{ csrf_token() }}'
				}).success(function(xhr){

					var info = $('#modal_sujo_stock_update .edit_alert');
					info.hide().find('ul').empty();
					info.slideDown();
					$("#modal_sujo_stock_update").modal("hide");
					oTable.draw() ;
					getSumQty();

				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_sujo_stock_update .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				});
			}
		});

		// 재고정리 클릭
		$('body').on( 'click', '.btnStockArranage', function () {

			//if( chkInputTble("tblList", "재고정리") ){
				var data = oTable.row( $(this).parents('tr') ).data();
				//console.log(data)
				//var selectedSEQ = data.SEQ;
				$.getJSON('/buy/sujo/SujoData', {
					SUJO_NO : data.SUJO_NO ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if (data.recordsTotal == 1) {
						setInitModalClose();
						$("#modal_stock_output_date").val(end.format('YYYY-MM-DD'));
						$("#modal_stock_sujo_no").val(data.data[0].SUJO_NO);
						$("#modal_stock_pis_mk").val(data.data[0].PIS_MK);
						$("#modal_stock_pis_nm").val(data.data[0].PIS_NM);
						$("#modal_stock_sizes").val(data.data[0].SIZES);
						$("#modal_stock_origin_cd").val(data.data[0].ORIGIN_CD);

						$("#modal_stock_origin_nm").val(data.data[0].ORIGIN_NM);
						$("#modal_stock_input_date").val(data.data[0].INPUT_DATE);
						$("#modal_stock_qty").val(data.data[0].QTY);
						$("#modal_stock_qty_tobe").val(data.data[0].QTY);
						$("input[name='modal_stock_reason']:eq(0)").prop('checked', true);
	
						$("#modal_stock_arrange").modal("show");
						setTimeout(function(){
							$("#modal_stock_qty_tobe").focus();
						},500);

					}
				});
			//}

		});
		// 재고정리 저장
		$("#btnSujoStockOut").click(function(){

			var data = oTable.row( $(this).parents('tr') ).data();
			var reason = $("input[name=modal_stock_reason]:checked").val();

			$.getJSON('/stock/stock/setSujoOut', {
				'SUJO_NO'		: $("#modal_stock_sujo_no").val() ,
				'PIS_MK'		: $("#modal_stock_pis_mk").val() ,
				'PIS_NM'		: $("#modal_stock_pis_nm").val() ,
				'SIZES'			: $("#modal_stock_sizes").val() ,
				'ORGIN_CD'		: $("#modal_stock_origin_cd").val() ,
				'ORGIN_NM'		: $("#modal_stock_origin_nm").val() ,
				'INPUT_DATE'	: $("#modal_stock_input_date").val() ,
				'OUTPUT_DATE'	: $("#modal_stock_output_date").val() ,
				'QTY'			: $("#modal_stock_qty").val() ,
				'QTY_TOBE'		: removeCommas($("#modal_stock_qty_tobe").val()) ,
				'REASON'		: reason,
				'CUST_CD'		: $("#modal_stock_cust_mk").val() ,
				'CUST_NM'		: $("#modal_stock_cust_nm").val() ,
				'OUTLINE'		: $("#modal_sujo_outline").val(),
				_token			: '{{ csrf_token() }}'
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if( data.result == "success"){
					$('#modal_stock_arrange').modal("hide");
					oTable.draw() ;
				}

			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();
			});
		});

		$("#btnLog").click(function(){
			location.href="/buy/buy/log";
		});
		
		$.getJSON('/config/getConfig', {
			_token			: '{{ csrf_token() }}'
		}).success(function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			if( data.result == "success"){
				
				if( data.model.SUJO_CUST_YN == "N"){
					oTable.column(2).visible(false);
				}else{
					oTable.column(2).visible(true);
				}

				if( data.model.CONF_CHK_SUJO == "Y"){
					oTable.column(7).visible(true);
				}else{
					oTable.column(7).visible(false);
				}
			}
			
		}).error(function(xhr,status, response) {
		});

		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$(".modal input[type='text']").val("");
			$(".modal input[type='radio']:nth-child(1)").prop("checked", true);
			$(".modal textarea").val("");
			$(".modal select option:eq(0)").prop("selected", true);
			$(".modal-body > .alert").remove();
			$("#origin_cd").removeAttr("disabled");
		}
	});


</script>

<script type="text/javascript">
	function startIntro(){

		var intro = introJs();
		intro.setOptions({
			steps: [
				{
					element: '#btnAddSujo',
					intro: "클릭하면 수조를 추가합니다."
				},
				{
					element: '.btnInSujo',
					intro: "입고를 할 경우 해당 버튼을 클릭하세요",
					position: 'right'
				},
				{
					element: '.btnUpdateUncn',
					intro: '판매단가를 수정 해야 할 경우 해당 버튼을 클릭하세요',
					position: 'left'
				},
				{
					element: '#step4',
					intro: "<span style='font-family: Tahoma'>Another step with new font!</span>",
					position: 'bottom'
				},
				{
					element: '#step5',
					intro: '<strong>Get</strong> it, <strong>use</strong> it.'
				}
			]
		});

		intro.start();
	}
</script>

<!-- 수조 추가-->
<div class="modal fade" id="modal_Sujo" role="dialog">
	<div class="modal-dialog modal-sm ">

		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 수조추가</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_Sujo_sujo_no"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
					<input type="text" class="form-control cis-lang-ko" id="modal_Sujo_sujo_no" lang="ko" placeholder=""/>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="button" class="btn btn-success" id="btnInsertSujoOK">
					<span class="glyphicon glyphicon-off"></span> 추가
				</button>
			</div>
		</div>
	</div>
</div>
<!-- 입고-->
<div class="modal fade" id="modal_sujo_in" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 입고정보 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_sujo_in_sujo_no"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
					<input type="text" class="form-control" id="modal_sujo_in_sujo_no" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_sujo_no_pis_mk"><i class='fa fa-check-circle text-red'></i> 어종 / 규격</label>
					<input type="text" class="form-control" id="modal_sujo_no_pis_mk" readonly="readonly" />

					<button type="button" name="searchPisMk" id="btnSearchPisMk" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>

					<input type="text" class="form-control" id="modal_sujo_in_pis_nm" readonly="readonly" />
					<input type="text" class="form-control" id="modal_sujo_in_pis_sizes" readonly="readonly" />
				</div>

				<div class="form-group slideSearchPisMk">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 어종 조회 선택</h5>

						<label for="modal_pis_nm"><i class='fa fa-check-circle text-red'></i> 어종명</label>
						<input type="text" class="form-control cis-lang-ko" name="strSearchPisMk" id="strSearchPisMk" />

						<button type="button" class="btn btn-danger  pull-right btn_PisClose">
							<span class="glyphicon glyphicon-remove"></span> 닫기
						</button>

						<div class="box-body">
							<table id="tblPisList" class="table table-bordered table-hover display nowrap" summary="게시물 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th scope="col" class="check">어종부호</th>
										<th scope="col" class="subject">어종명</th>
										<th scope="col" class="name">규격</th>
										<th scope="col" class="name">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="modal_pis_nm"><i class='fa fa-check-circle text-red'></i> 원산지</label>
					<select class="form-control" name="origin_cd" id="origin_cd">
					</select>
					<label for="modal_sujo_input_date" class='mleft'><i class='fa fa-check-circle text-red'></i> 입고일</label>
					<input type="text" class="form-control" id="modal_sujo_input_date" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_sujo_qty"><i class='fa fa-check-circle text-red'></i> 입고수량</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_qty" placeholder="" />
				</div>
				<div class="form-group">
					<label for="modal_sujo_uncn"><i class='fa fa-check-circle text-red'></i> 입고단가</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_uncn" placeholder="">

					<label for="modal_sujo_input_am" class='mleft'><i class='fa fa-circle-o text-aqua'></i> 입고합계</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_input_amt" readonly= "readonly" placeholder="입고합계 입력">
				</div>
				<div class="form-group">
					<label for="modal_sujo_discount"><i class='fa fa-circle-o text-aqua'></i> 할인금액</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_discount" placeholder="">

					<label for="modal_sujo_real_amt" class='mleft'><i class='fa fa-circle-o text-aqua'></i> 입고금액</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_real_amt" readonly= "readonly" placeholder="0">
				</div>

				<div class="form-group">
					<label for="modal_sujo_cash"><i class='fa fa-circle-o text-aqua'></i> 현금</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_cash" placeholder="">

					<label for="modal_sujo_unprov" class='mleft'><i class='fa fa-circle-o text-aqua'></i> 미지급금</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_unprov" readonly= "readonly" placeholder="미지급금 입력">
				</div>

				<div class="form-group">
					<label for="modal_sujo_sale_uncn"><i class='fa fa-circle-o text-aqua'></i> 판매단가</label>
					<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_sujo_sale_uncn" placeholder="">
				</div>

				<div class="form-group">
					<label for="modal_cust_cust_mk"><i class='fa fa-check-circle text-red'></i> 거래처</label>
					<input type="text" class="form-control" id="modal_cust_cust_mk" placeholder="" readonly="readonly">
					<input type="text" class="form-control" id="modal_cust_cust_nm" placeholder="" readonly="readonly">
					<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>

					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5><span class="glyphicon glyphicon-th-large"></span> 거래처 선택</h5>

							<label for="strSearchCust"><i class='fa fa-circle-o text-aqua'></i> 거래처 상호</label>
							<input type="text" class="form-control cis-lang-ko" name="strSearchCust" id="strSearchCust" />

							<button type="button" class="btn btn-danger  pull-right btn_CustClose">
								<span class="glyphicon glyphicon-remove"></span> 닫기
							</button>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>어종 조회</caption>
									<thead>
										<tr>
											<th scope="col" class="check">코드</th>
											<th scope="col" class="subject">상호</th>
											<th scope="col" class="name">대표자</th>
											<th scope="col" class="name">전화번호</th>
											<th scope="col" class="name">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_sujo_outline"><i class='fa fa-circle-o text-aqua'></i> 적요</label>
					<textarea  class="form-control cis-lang-ko" id="modal_sujo_outline" placeholder="" width="80%" ></textarea>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>

				<button type="button" class="btn btn-success pull-right" id="btnSaveSujoStock">
					<span class="glyphicon glyphicon-off"></span> 입고 저장
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 단가수정 -->
<div class="modal fade" id="modal_Uncn" role="dialog">
	<div class="modal-dialog modal-sm" >
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 판매단가 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_sujo_no"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
					<input type="text" class="form-control" id="modal_sujo_no" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_pis_nm"><i class='fa fa-check-circle text-red'></i> 어종명</label>
					<input type="text" class="form-control cis-lang-ko" id="modal_pis_nm" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_input"><i class='fa fa-check-circle text-red'></i> 입고일</label>
					<input type="text" class="form-control" id="modal_input" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_pis_uncn"><i class='fa fa-check-circle text-red'></i> 판매단가</label>
					<input type="text" class="form-control numeric" id="modal_pis_uncn" placeholder="판매단가 입력">
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기 </button>
				<button type="button" class="btn btn-success " id="btnUpdateUNCN">
					<span class="glyphicon glyphicon-off"></span> 수정
				</button>
			</div>
		</div>
	</div>
</div>


<!-- 수조이고 통합  -->
<div class="modal fade" id="modal_sujo_in_date" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 입고정보 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="form-group">
					<input type="hidden" id="force_date" name="force_date" />
					<p>
						수조에 입고날짜가 다른 재고가 존재 합니다.
						통합할 날짜를 선택하여 주십시오.
					</p>

					<input type="radio" name="input_date"  id="new_date" />
					<label for="new_date"><i class='fa fa-check-circle text-red'></i><span></span></label>

					<input type="radio" name="input_date"  id="recent_date" />
					<label for="recent_date"><i class='fa fa-check-circle text-red'></i><span></span></label>
					<p>
						날짜를 통합하면 비선택 날짜의 재고가
						선택 날짜의 재고로 통합되며,
						기존 입고 단가는 새 입고 단가로 변경 됩니다
					</p>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 취소 </button>

				<button type="button" class="btn btn-success  pull-right" id="btnPickDateSujoStock">
					<span class="glyphicon glyphicon-off"></span> 이고 저장
				</button>
			</div>

			</div>
		</div>
	</div>
</div>

<!-- 이고 -->
<div class="modal fade" id="modal_sujo_move" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 이고</h4>
			</div>
			<div class="modal-body" style="padding:10px 20px 5px 20px;">
				<div class='edit_alert' ><ul></ul></div>


				<div class="form-group">
					<input type="hidden" id="modal_sujo_cust_mk" name="modal_sujo_cust_mk"  />
					<label for="modal_sujo_move_no"><i class='fa fa-check-circle text-red'></i>수조</label>
					<input type="text" class="form-control" id="modal_sujo_move_no" readonly="readonly" />

					<label for="modal_sujo_move_pis_nm"><i class='fa fa-check-circle text-red'></i> 어종/규격</label>
					<input type="text" class="form-control" id="modal_sujo_move_pis_nm" readonly="readonly" />

				</div>
				<div class="form-group">
					<label for="modal_sujo_move_orgin_nm"><i class='fa fa-check-circle text-red'></i> 원산지</label>
					<input type="text" class="form-control" id="modal_sujo_move_orgin_nm" readonly="readonly" />

					<label for="modal_sujo_move_input"><i class='fa fa-check-circle text-red'></i> 입고일 </label>
					<input type="text" class="form-control" id="modal_sujo_move_input" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_sujo_move_qty"><i class='fa fa-check-circle text-red'></i> 총중량</label>
					<input type="text" class="form-control numeric" id="modal_sujo_move_qty" readonly="readonly" />

					<label for="modal_sujo_move_select"><i class='fa fa-check-circle text-red'></i> 이고수조</label>
					<select class="form-control" name="modal_sujo_move_select" id="modal_sujo_move_select">
						<option>선택</option>
					</select>
				</div>
				<div class="form-group force-right">
					<label for="modal_sujo_move_qty_minus" id="force_right"><i class='fa fa-check-circle text-red'></i> 이고중량</label>
					<input type="text" class="form-control numeric" id="modal_sujo_move_qty_minus" placeholder="0">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnMoveSujoSave">
					<span class="glyphicon glyphicon-off"></span> 이고
				</button>
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기 </button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_sujo_move_date" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 이고정보 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<input type="hidden" id="force_date_03" name="force_date_03" />
					<p>
					이고할 수조에 입고날짜가 다른 재고가 존재 합니다.
					통합할 날짜를 선택하여 주십시오.
					</p>
					<input type="radio" name="move_input_date"  id="src_date" />
					<label for="src_date"><i class='fa fa-check-circle text-red'></i> <span></span></label>

					<input type="radio" name="move_input_date"  id="dst_date" />
					<label for="dst_date"><i class='fa fa-check-circle text-red'></i> <span></span></label>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 취소 </button>

				<button type="button" class="btn btn-success  pull-right" id="btnSujoMoveForce">
					<span class="glyphicon glyphicon-off"></span> 확인
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 재고수정/삭제 -->
<div class="modal fade" id="modal_sujo_stock_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 입고수정 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert">
					<ul>
						<li class='red'> 빨간색 수조번호는 현재 선택한 수조</li>
					</ul>
				</div>
				
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblStockUpdList" class="table table-bordered table-hover display nowrap table-condensed" summary="재고 수정/삭제 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th class="subject" width="15px">수조</th>
										<th width="60px">업체명</th>
										<th class="name" width="80px">어종명</th>
										<th class="name" width="15px">규격</th>
										<th class="name" width="20px">원산지</th>
										<th class="name" width="45px">입고일</th>
										<th class="name" width="40px">중량</th>
										<th class="name" width="auto">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>


<!-- 재고정리-->
<div class="modal fade" id="modal_stock_arrange" role="dialog">
	<div class="modal-dialog dialog-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 재고정리 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_stock_sujo_no"><i class='fa fa-check-circle text-red'></i> 수조번호</label>
					<input type="text" class="form-control" id="modal_stock_sujo_no" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_stock_pis_mk"><i class='fa fa-check-circle text-red'></i> 어종 / 규격</label>
					<input type="hidden" class="form-control" id="modal_stock_pis_mk" />
					<input type="text" class="form-control" id="modal_stock_pis_nm" readonly="readonly" />
					<input type="text" class="form-control" id="modal_stock_sizes" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_stock_origin_nm"><i class='fa fa-check-circle text-red'></i> 원산지</label>
					<input type="hidden" class="form-control" id="modal_stock_origin_cd" readonly="readonly" />
					<input type="text" class="form-control" id="modal_stock_origin_nm" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_stock_input_date"><i class='fa fa-check-circle text-red'></i> 입고일</label>
					<input type="text" class="form-control" id="modal_stock_input_date" readonly="readonly" />

					<label for="modal_stock_output_date"><i class='fa fa-check-circle text-red'></i> 재고출고일</label>
					<input type="text" class="form-control" id="modal_stock_output_date"  />
				</div>

				<div class="form-group">
					<label for="modal_stock_qty"><i class='fa fa-check-circle text-red'></i> 현 재고 중량</label>
					<input type="text" class="form-control" id="modal_stock_qty" readonly="readonly"  />

					<label for="modal_stock_qty_tobe"><i class='fa fa-check-circle text-red'></i> 재고정리 중량</label>
					<input type="text" class="form-control numeric" id="modal_stock_qty_tobe" />
				</div>


				<div class="form-group">
					<label for="modal_stock_cust_mk"><i class='fa fa-check-circle text-aqua'></i> 거래처</label>
					<input type="text" class="form-control" id="modal_stock_cust_mk" placeholder="거래처코드" readonly="readonly">
					<input type="text" class="form-control" id="modal_stock_cust_nm" placeholder="거래처명" readonly="readonly">
					<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>

					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5><i class='fa fa-check-circle text-aqua'></i> 거래처 선택</h5>

							<label for="strSearchCust"><i class='fa fa-check-circle text-aqua'></i> 거래처 상호</label>
							<input type="text" class="form-control cis-lang-ko" name="strSearchCust" id="strSearchCust" />

							<button type="button" class="btn btn-danger  pull-right btn_CustClose">
								<span class="glyphicon glyphicon-remove"></span> 닫기
							</button>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>거래처 조회</caption>
									<thead>
										<tr>
											<th class="check">코드</th>
											<th class="subject">상호</th>
											<th class="name">대표자</th>
											<th class="name">전화번호</th>
											<th class="name">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group reason">
					<label for="modal_stock_reason"><i class='fa fa-check-circle text-aqua'></i> 재고정리사유</label>
					<label class='sel'><input type="radio" name="modal_stock_reason" value="1" class="minimal"  checked="checked" />감량</label>
					<label class='sel'><input type="radio" name="modal_stock_reason" value="2"  class="minimal" />폐사</label>
					<label class='sel'><input type="radio" name="modal_stock_reason" value="3" class="minimal"  /> 증량</label>
					<label class='sel'><input type="radio" name="modal_stock_reason" value="99" class="minimal"  /> 기타</label>
				</div>

				<div class="form-group">
					<label for="modal_sujo_outline"><i class='fa fa-check-circle text-aqua'></i> 적요</label>
					<textarea  class="form-control textarea cis-lang-ko" style="width:70%;" id="modal_sujo_outline" placeholder="적요입력" ></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>

				<button type="button" class="btn btn-success  pull-right" id="btnSujoStockOut">
					<span class="glyphicon glyphicon-off"></span> 재고정리
				</button>
			</div>
		</div>
	</div>
</div>
@stop

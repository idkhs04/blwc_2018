@extends('layouts.main')
@section('class','회원관리')
@section('title','급여정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}

	#modal_salary input.form-control{ width:70%;}
	#modal_salary label{ float:left; width:105px;}

	.dt-body-right{text-align:right;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	.detailTbl th, .detailTbl td{ text-align:right;}

	.detailTbl th { background-color:khaki;}
</style>


<div class="col-md-12">
	<div class="box box-primary">
		
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			
			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-info" id="btnAddMember" ><i class="fa fa-user-plus"></i> 추가</button>
					<button type="button" class="btn btn-success" id="btnUpdateMember"><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDeleteMember"><i class="fa fa-user-times"></i> 삭제</button>
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<label>지급일</label> 
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			
		</div>
		
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="사원급여 목록" width="100%">
				<caption>사원 급여정보</caption>
				<thead>
					<tr>
						<th width="35px">상세보기</th>
						<th width="15px"></th>
						<th width="40px">회원명</th>
						<th width="60px">지급일</th>
						<th width="80px">지급금액</th>
						<th width="80px">공제금액</th>
						<th width="80px">실지급액</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		var start = moment().subtract(1, 'months');
		var end = moment();
		
		$('#modal_salary_prov_date').val(end.format('YYYY-MM-DD'));

		// 입고정보 : 입고일 
		$("#modal_salary_prov_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			opens: "right",
			drops: "down",
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			ranges: {
				'오늘': [moment(), moment()],
				'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
				'이번 달': [moment().startOf('month'), moment().endOf('month')],
				'지난 달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);


		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 10개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/member/member/listSalary",
				data:function(d){
					d.MEM_ID	= "{{Request::segment(4)}}";
					d.start_dt	= $("input[name='start_date']").val();
					d.end_dt	= $("input[name='end_date']").val();
				}
			},
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{
					data:   "MEM_ID",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				
				{ data: 'NAME', name: 'NAME' ,className: "dt-body-left"},
				{ data: 'PROV_DATE', name: 'PROV_DATE' ,className: "dt-body-center"},
				{ 
					data: 'PROV_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'DEDUCATION', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'REAL_PAY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'REAL_PAY', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return '';
						}
						return '';
					},
					className: "dt-body-right"
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		$("input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		 $('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		function format ( d ) {
			// `d` is the original data object for the row
			return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:50%" class="detailTbl table table-bordered table-hover display nowrap">'+
					'<tr>'+
						'<th>지급일:</th>'+
						'<td>'+d.PROV_DATE+'</td>'+
						'<th>배우자 유무:</th>'+
						'<td><input type="checkbox" '+ (d.PARTN_YN == "1" ? 'checked' : '') + ' disabled /> '+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>기본급여:</th>'+
						'<td>'+Number(d.DEFAULT_PAY).format()+'</td>'+
						'<th>장애인수:</th>'+
						'<td>'+Number(d.TROUBLE_CNT).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>식대:</th>'+
						'<td>'+Number(d.FOOD_CHG).format()+'</td>'+
						'<th>부양가족수:</th>'+
						'<td>'+Number(d.DFDT_FMLY_CNT).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>지긊합계:</th>'+
						'<td>'+Number(d.DEDUCATION).format()+'</td>'+
						'<th>경로우대자수:</th>'+
						'<td>'+Number(d.OLD_TREAT_CNT).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>의료보험:</th>'+
						'<td>'+Number(d.MEDI_INSUR_PAY).format()+'</td>'+
						'<th>갑근세:</th>'+
						'<td>'+Number(d.GABGUN_TAX).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>국민연금:</th>'+
						'<td>'+Number(d.NATN_PS_PAY).format()+'</td>'+
						'<th>주민세:</th>'+
						'<td>'+Number(d.JUMIN_TAX).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>고용보험:</th>'+
						'<td>'+Number(d.EMPLY_INSUR_PAY).format()+'</td>'+
						'<th>공제합계:</th>'+
						'<td>'+Number(d.DEDUCATION).format()+'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>기타공제:</th>'+
						'<td>'+Number(d.ETC_ALLOWANCE).format()+'</td>'+
						'<th>실지급액:</th>'+
						'<td>'+Number(d.PROV_AMT).format()+'</td>'+
					'</tr>'+

				'</table>';
		}

		//목록버튼

		$("#btnList").click(function(){
			location.href="/member/member/index?page=1";
		});
		//수정버튼
		$("#btnUpdateMember").click(function(){
			
			$("input[name='mode']").val("u");

			var arr_code = [];
			var prov_date = "";
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
					var data = oTable.row( $(this).parents('tr') ).data();
					prov_date = data.PROV_DATE;
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정은 한개만 선택해주세요") ).show();
				return false;
			}

			$("#modal_salary").modal({show:true});
			
			$.getJSON("/member/member/getSalary", {
				_token				: '{{ csrf_token() }}'
				, MEM_ID			: "{{Request::segment(4)}}"
				, PROV_DATE			: prov_date
			}, function (xhr) {
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if (data.result == "success") {
					
					var item = data.data;
					
					$("#modal_salary_prov_date").val(item.PROV_DATE);
					$("#modal_salary_REAL").val(Number(item.PROV_AMT).format());
					$("#modal_salary_FOOD_CHG").val(Number(item.FOOD_CHG).format());
					
					console.log(item.PARTN_YN);
					item.PARTN_YN == "1" ? $("#modal_salary_PARTNER_YN").prop("checked", true) : $("#modal_salary_PARTNER_YN").prop("checked", false);

					$("#modal_salary_DFDT_FMLY_CNT").val(Number(item.DFDT_FMLY_CNT).format());
					$("#modal_salary_TRUBLE_CNT").val(Number(item.TROUBLE_CNT).format())
					$("#modal_salary_OLD_TREAT_CNT").val(Number(item.OLD_TREAT_CNT).format())

					$("#modal_salary_medi_rank").val(Number(item.MEDI_INSUR_RANK).format())
					$("#modal_salary_MEDI_INSUR_RANK").val(Number(item.MEDI_INSUR_PAY).format())
					$("#modal_salary_natnal_rank").val(Number(item.NATN_PS_RANK).format())
					$("#modal_salary_NATN_PS_RANK").val(Number(item.NATN_PS_PAY).format())

					$("#modal_salary_EMPLY_INSUR_PAY").val(Number(item.EMPLY_INSUR_PAY).format())
					$("#modal_salary_JUMIN_TAX").val(Number(item.JUMIN_TAX).format())
					$("#modal_salary_GABGUN_TAX").val(Number(item.GABGUN_TAX).format())
					$("#modal_salary_DEFAULT_PAY").val(Number(item.DEFAULT_PAY).format())
					$("#modal_salary_ETC_CHECK_OFF").val(Number(item.ETC_CHECK_OFF).format())
					$("#modal_salary_ALLOWANCE").val(Number(item.ETC_ALLOWANCE).format())

					$("#modal_salary_PROV_AMT").val( Number(parseInt(item.DEFAULT_PAY) + parseInt(item.FOOD_CHG) + parseInt(item.ETC_ALLOWANCE)).format());
					sumTaxCHECK = parseInt(item.MEDI_INSUR_PAY) + parseInt(item.NATN_PS_PAY) + parseInt(item.EMPLY_INSUR_PAY) + parseInt(item.JUMIN_TAX) + parseInt(item.GABGUN_TAX) + parseInt(item.ETC_CHECK_OFF);

					$("#modal_salary_CHECK").val(Number(sumTaxCHECK).format()) ;
				}
			}, function(xhr){
				console.log(xhr)
			});
			
		});
		
		$('#modal_salary').on('hidden.bs.modal', CbSetInitModalClose);

		//추가버튼
		$('#btnAddMember').click(function () {
			$("input[name='mode']").val("i");
			$("#modal_salary").modal({show:true});
		});

		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#btnDeleteMember').click( function () {
			var prov_date = '';
			var arr_code = [];
				$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
					if ($(this).is(":checked")) {
						arr_code.push($(this).val());
						var data = oTable.row( $(this).parents('tr') ).data();
						prov_date = data.PROV_DATE;
					}
				});
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				var data = oTable.row( $(this).parents('tr') ).data();
				if(confirm("선택된 내용을 삭제하시겠습니까?")){

					$.getJSON("/member/member/deleteSalary", {
						_token				: '{{ csrf_token() }}'
						, MEM_ID			: "{{Request::segment(4)}}"
						, PROV_DATE			: prov_date
					}, function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						if (data.result == "success") {
							oTable.draw() ;
						}
					}, function(xhr){
						console.log(xhr)
					});
				}
			}
		});

		// 급여 저장
		$("#btnSaveSalary").click(function(){

			$.blockUI({ 
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() { 
						
						var url = '/member/member/setSalary';
						if(  $("input[name='mode']").val() != "i" ){
							url = '/member/member/updateSalary';
						}

						$.getJSON(url, {
							_token				: '{{ csrf_token() }}'
							, MEM_ID			: "{{Request::segment(4)}}"
							, PROV_DATE			: $("#modal_salary_prov_date").val()
							, PROV_AMT			: removeCommas($("#modal_salary_REAL").val())
							, FOOD_CHG			: removeCommas($("#modal_salary_FOOD_CHG").val())
							, PARTN_YN			: $("#modal_salary_PARTNER_YN").is(":checked") ? "1" : "0"
							, DFDT_FMLY_CNT		: removeCommas($("#modal_salary_DFDT_FMLY_CNT").val())
							, TROUBLE_CNT		: removeCommas($("#modal_salary_TRUBLE_CNT").val())
							, OLD_TREAT_CNT		: removeCommas($("#modal_salary_OLD_TREAT_CNT").val())

							, MEDI_INSUR_RANK	: removeCommas($("#modal_salary_medi_rank").val())
							, MEDI_INSUR_PAY	: removeCommas($("#modal_salary_MEDI_INSUR_RANK").val())
							, NATN_PS_RANK		: removeCommas($("#modal_salary_natnal_rank").val())
							, NATN_PS_PAY		: removeCommas($("#modal_salary_NATN_PS_RANK").val())

							, EMPLY_INSUR_PAY	: removeCommas($("#modal_salary_EMPLY_INSUR_PAY").val())
							, JUMIN_TAX			: removeCommas($("#modal_salary_JUMIN_TAX").val())
							, GABGUN_TAX		: removeCommas($("#modal_salary_GABGUN_TAX").val())
							, DEFAULT_PAY		: removeCommas($("#modal_salary_DEFAULT_PAY").val())
							, ETC_CHECK_OFF		: removeCommas($("#modal_salary_ETC_CHECK_OFF").val())
							, ETC_ALLOWANCE		: removeCommas($("#modal_salary_ALLOWANCE").val())
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						$("#modal_salary").modal("hide");
						oTable.draw() ;
						$.unblockUI();

					}).error(function(xhr,status, response) {
						

						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('#modal_salary .edit_alert');

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li> 동일날짜에 입력한 데이터가 존재합니다.</li>');

						}else{
							info.hide().find('ul').empty();
							for(var k in error.message){
								info.find('ul').append('<li>' + k + '</li>');
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						info.slideDown();
						$.unblockUI();
					});
					$.unblockUI();
				}
			});
		});


		// 자동계산 이벤트
		$(".sum_valid").on("focusout, focusin", function(){
			
			var defaultPay	= removeCommas( $("#modal_salary_DEFAULT_PAY").val() );		// 기본급여
			var foodPay		= removeCommas( $("#modal_salary_FOOD_CHG").val() );		// 식대
			var etcPay		= removeCommas( $("#modal_salary_ALLOWANCE").val() );		// 기타수당
			var sumPay		= removeCommas( $("#modal_salary_PROV_AMT").val() );		// 지급합계

			var mediInsPay	= removeCommas( $("#modal_salary_MEDI_INSUR_RANK").val() );	// 의료보험 
			var natInsPay	= removeCommas( $("#modal_salary_NATN_PS_RANK").val() );	// 국민연금 
			var empInsPay	= removeCommas( $("#modal_salary_EMPLY_INSUR_PAY").val() );	// 고용보험
			var etcInsPay	= removeCommas( $("#modal_salary_ETC_CHECK_OFF").val() );	// 기타공제 

			var isCheck		= $("#modal_salary_PARTNER_YN").is("checked") ? true : false;// 배우자 유무
			var trubCnt		= $("#modal_salary_TRUBLE_CNT").val();						// 장애인수
			var fmlyCnt		= $("#modal_salary_DFDT_FMLY_CNT").val();					// 부양가족수
			var oldCnt		= $("#modal_salary_OLD_TREAT_CNT").val();					// 경로우대자수

			var taxGabg		= removeCommas( $("#modal_salary_GABGUN_TAX").val() );		// 갑근세 
			var taxJumin	= removeCommas( $("#modal_salary_JUMIN_TAX").val() );		// 주민세 
			var sumTaxCHECK	= removeCommas( $("#modal_salary_CHECK").val() );			// 공제합계 
			var realPay		= removeCommas( $("#modal_salary_REAL").val() );			// 실지급액 

			var rankNatinl	= $("#modal_salary_natnal_rank").val();						// 국민연금 등급
			var rankMedi	= $("#modal_salary_medi_rank").val();						// 의료보험 등급

			defaultPay	= $.isNumeric( defaultPay ) ? defaultPay : 0;
			foodPay		= $.isNumeric( foodPay ) ? foodPay : 0;
			etcPay		= $.isNumeric( etcPay ) ? etcPay : 0;
			
			$.getJSON("/member/member/getTax", {
					DEFAULT_PAY : defaultPay ,
					FAMI_CNT	: getFamilyCount() ,
					_token : '{{ csrf_token() }}'
				}, function (xhr) {
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					
					// 의료보험 등급
					$("#modal_salary_natnal_rank").val(Number(data.NATN_RANK).format());
					// 의료보험료
					$("#modal_salary_MEDI_INSUR_RANK").val(Number(data.MEDI_INSU_AMT).format());
					
					// 국민연급 등급
					$("#modal_salary_medi_rank").val(Number(data.MEDI_RANK).format());	
					// 국민연금표
					 $("#modal_salary_NATN_PS_RANK").val(Number(data.NATN_AMT).format());
					
					// 고용보험
					$("#modal_salary_EMPLY_INSUR_PAY").val(Number(data.EMP_ISUPAY).format());

					// 주민세
					$("#modal_salary_JUMIN_TAX").val(Number(data.JUMIN_TAX).format());
					// 갑근세
					$("#modal_salary_GABGUN_TAX").val(Number(data.GABG_TAX).format());
					
					// 기타공제
					if( etcInsPay != null && etcInsPay != ""){
						$("#modal_salary_ETC_CHECK_OFF").val(Number(etcInsPay).format());
					}else{
						$("#modal_salary_ETC_CHECK_OFF").val(0)
						etcInsPay = 0;
					}

					// 공제합계
					sumTaxCHECK = parseInt(data.MEDI_INSU_AMT) + parseInt(data.NATN_AMT) + parseInt(data.EMP_ISUPAY) + parseInt(data.JUMIN_TAX) + parseInt(data.GABG_TAX) + parseInt(etcInsPay);

					$("#modal_salary_CHECK").val(Number(sumTaxCHECK).format()) ;
			
					// 실지급금액
					realPay = sumPay - sumTaxCHECK;
					var realPay		= $("#modal_salary_REAL").val(Number(realPay).format()) ;


				}, function(xhr){
					console.log(xhr)
				});
		
			sumPay = parseInt(defaultPay) + parseInt(foodPay) + parseInt(etcPay);
			$("#modal_salary_PROV_AMT").val(Number(sumPay).format()) ;
			
		});


		
		// 가족수 모두 가져오기
		function getFamilyCount(){
			
			var count		= 1;
			
			var isCheck		= $("#modal_salary_PARTNER_YN").is(":checked") ? true : false;	// 배우자 유무
			console.log(isCheck);

			var trubCnt		= parseInt($("#modal_salary_TRUBLE_CNT").val());				// 장애인수
			var fmlyCnt		= parseInt($("#modal_salary_DFDT_FMLY_CNT").val());				// 부양가족수
			var oldCnt		= parseInt($("#modal_salary_OLD_TREAT_CNT").val());				// 경로우대자수

			trubCnt	= $.isNumeric(trubCnt) ? trubCnt : 0;
			fmlyCnt	= $.isNumeric(fmlyCnt) ? fmlyCnt : 0;
			oldCnt	= $.isNumeric(oldCnt) ? oldCnt : 0;

			if(isCheck){
				count += 1;
			}

			return count + trubCnt + fmlyCnt + oldCnt;

			

		}
	});
</script>
<!-- 급여정보 입력-->
<div class="modal fade" id="modal_salary" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 급여정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="row">
					<input type="hidden" name="mode" id="mode" />
					<div class="col-md-6 col-xs-6">
						<div class="form-group">
							<label for="modal_salary_prov_date"><i class='fa fa-circle-o text-red'></i> 지급일</label>
							<input type="text" class="form-control" id="modal_salary_prov_date" readonly="readonly" />
						</div>

						<div class="form-group">
							<label for="modal_salary_DEFAULT_PAY"><i class='fa fa-circle-o text-aqua'></i> 기본급여</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_DEFAULT_PAY" placeholder="기본급여" />
						</div>

						<div class="form-group">
							<label for="modal_salary_FOOD_CHG"><i class='fa fa-circle-o text-aqua'></i> 식대</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_FOOD_CHG" placeholder="식대" />
						</div>

						<div class="form-group">
							<label for="modal_salary_ALLOWANCE"><i class='fa fa-circle-o text-aqua'></i>기타수당</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_ALLOWANCE"  placeholder="기타수당"   />
						</div>

						<div class="form-group">
							<label for="modal_salary_PROV_AMT"><i class='fa fa-circle-o text-aqua'></i>지급합계</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_PROV_AMT" placeholder="지급합계" readonly="readonly" />
						</div>
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="form-group">
							<label for="modal_salary_PARTNER_YN"><i class='fa fa-circle-o text-aqua'></i> 배우자유무</label>
							<input type="checkbox" id="modal_salary_PARTNER_YN" class="sum_valid" /> 배우자있음
						</div>
						<div class="form-group">
							<label for="modal_salary_TRUBLE_CNT"><i class='fa fa-circle-o text-aqua'></i> 장애인수</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_TRUBLE_CNT" placeholder="장애인수" />
						</div>
						<div class="form-group">
							<label for="modal_salary_DFDT_FMLY_CNT" class='mleft'><i class='fa fa-circle-o text-aqua'></i>부양가족수</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_DFDT_FMLY_CNT" placeholder="부양가족 수" />
						</div>

						<div class="form-group">
							<label for="modal_salary_OLD_TREAT_CNT"><i class='fa fa-circle-o text-aqua'></i> 경로우대자수</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_OLD_TREAT_CNT" placeholder="경로우대자수">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6 col-xs-6">
					
						<div class="form-group">
							<label for="modal_salary_MEDI_INSUR_RANK"><i class='fa fa-circle-o text-aqua'></i> 의료보험</label>

							<input type="text" id="modal_salary_medi_rank" readonly="readonly" style="width:50px;float:left;" /> <span style="float:left;">등급</span>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_MEDI_INSUR_RANK" style="width:50%;" placeholder="의료보험" />
						</div>
						<div class="form-group">
							<label for="modal_salary_NATN_PS_RANK"><i class='fa fa-circle-o text-aqua'></i> 국민연금</label>

							<input type="text" id="modal_salary_natnal_rank" readonly="readonly" style="width:50px;float:left;"  /> <span style="float:left;">등급</span>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_NATN_PS_RANK" style="width:50%;" placeholder="국민연금" />
						</div>
						<div class="form-group">
							<label for="modal_salary_EMPLY_INSUR_PAY"><i class='fa fa-circle-o text-aqua'></i> 고용보험</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_EMPLY_INSUR_PAY" placeholder="고용보험" />
						</div>
						<div class="form-group">
							<label for="modal_salary_ETC_CHECK_OFF"><i class='fa fa-circle-o text-aqua'></i> 기타공제</label>
							<input type="text" class="form-control sum_valid numeric glyphicon-user dt-body-right" id="modal_salary_ETC_CHECK_OFF" placeholder="기타공제" />
						</div>
					</div>
					<div class="col-md-6 col-xs-6">
					
						<div class="form-group">
							<label for="modal_salary_GABGUN_TAX"><i class='fa fa-circle-o text-aqua'></i> 갑근세</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_GABGUN_TAX" placeholder="갑근세" readonly="readonly" />
						</div>
						<div class="form-group">
							<label for="modal_salary_JUMIN_TAX"><i class='fa fa-circle-o text-aqua'></i> 주민세</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_JUMIN_TAX" placeholder="주민세" readonly="readonly" />
						</div>
						<div class="form-group">
							<label for="modal_salary_CHECK"><i class='fa fa-circle-o text-aqua'></i> 공제합계</label>
							<input type="text" class="form-control sum_valid numeric dt-body-right" id="modal_salary_CHECK" placeholder="공제합계" readonly="readonly" />
						</div>
						<div class="form-group">
							<label for="modal_salary_REAL"><i class='fa fa-circle-o text-aqua'></i> 실지급액</label>
							<input type="text" class="form-control sum_valid dt-body-right" id="modal_salary_REAL" placeholder="실지급액" readonly="readonly" />
						</div>
					
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
						<span class="glyphicon glyphicon-remove"></span> 닫기 
					</button>

					<button type="button" class="btn btn-success  pull-right" id="btnSaveSalary">
						<span class="glyphicon glyphicon-off"></span> 저장
					</button>
				</div>
		</div>
	</div>
</div>
@stop
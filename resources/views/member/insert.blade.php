@extends('layouts.main')
@section('class','회원관리')
@section('title','사원정보')

@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}

	#tblDetail > tbody > tr > td{
		width:25%;
	}
	#tblDetail > tbody > tr > th{
		width:30px;
		background-color: #eaeaea;
		text-align:right;
	}

	#tblDetail > tbody > tr > td > textarea{
		width:100%;
		height:100px;
	}

	#tblDetail input[type='text'], #tblDetail input[type='password']{
		padding:0;
		height:23px;
		margin:5px;
	}

	.form-control{ height:28px;padding-top:3px;}

	.center{ text-align:center;}

	table.dataTable tbody .btn {
		padding: 3px 12px;
		vertical-align:baseline;
		margin-top:4px;
		margin-left:2px;
	}

	#tblDetail > tbody > tr > th{
		width:10%;
		background-color:khaki;
		text-align:right;
	}

	.form-control { height:28px;padding-top:3px;}
	input.form-control { width:200px;}
	select.form-control { width:200px; margin-left:5px;}

	table.dataTable { width:70%; float:left;}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-2 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<form action="/member/cust_grp_info/insert/{{$corp_mk}}" method="post" name="CorpUpdateForm" id="CorpUpdateForm">
				{{csrf_field()}}
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<div class="col-md-12 col-xs-12">
					<table id="tblDetail" class="table table-bordered table-hover display nowrap dataTable" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<th>회원아이디</th>
								<td colspan="3">
									<div class="col-xs-6">
										<input type="text" class="form-control" id="MEM_ID" name="MEM_ID" value="{{ old('MEM_ID') }}"/>
										@if ($errors->has('MEM_ID'))
											<div class="help-block">
												<strong>{{ $errors->first('MEM_ID') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>비밀번호</th>
								<td>
									<div class="col-xs-12">
										<input type="password" class="form-control" id="PWD_NO" name="PWD_NO" value="" />
										@if ($errors->has('PWD_NO'))
											<div class="help-block">
												<strong>{{ $errors->first('PWD_NO') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>비밀번호확인</th>
								<td>
									<div class="col-xs-12">
										<input type="password" class="form-control" id="PWD_NO_CONFIRMATION" name="PWD_NO_CONFIRMATION" value="" />
										@if ($errors->has('PWD_NO_CONFIRMATION'))
											<div class="help-block">
												<strong>{{ $errors->first('PWD_NO_CONFIRMATION') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>그룹코드</th>
								<td>
									<div class="col-xs-12">
										<select class="form-control" id="GRP_CD" name="GRP_CD" value="{{ old('GRP_CD') }}">
										@foreach ($GRP_CD as $list)
											<option value="{{$list->GRP_CD}}">{{$list->GRP_NM}}({{$list->GRP_CD}})</option>
										@endforeach
										</select>
									</div>
								</td>
								<th>업체부호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="CORP_MK" name="CORP_MK" value="{{$corp_mk}}" readonly="readonly" />
										@if ($errors->has('CORP_MK'))
											<div class="help-block">
												<strong>{{ $errors->first('CORP_MK') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>급호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="GRADE" name="GRADE" value="{{ old('GRADE') }}" />
									</div>
								</td>
								<th>직위</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="PSTN" name="PSTN" value="{{ old('PSTN') }}" />
									</div>
								</td>
							</tr>
							<tr>
								<th>성명</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="NAME" name="NAME" value="{{ old('NAME') }}" />
										@if ($errors->has('NAME'))
											<div class="help-block">
												<strong>{{ $errors->first('NAME') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>주민번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="JUMIN_NO" name="JUMIN_NO" value="{{ old('JUMIN_NO') }}" />
										@if ($errors->has('JUMIN_NO'))
											<div class="help-block">
												<strong>{{ $errors->first('JUMIN_NO') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>우편번호</th>
								<td colspan="3">
									<div class="col-xs-6">
										<input type="text" class="form-control" id="POST_NO" name="POST_NO" value="{{ old('POST_NO') }}" readonly="readonly"  style="float:left;width:50%"/>
										<button type="button" class="btn btn-success btn-info" name="searchPisMk" id="btnSearchPisMk" onclick="sample6_execDaumPostcode()" ">
											<span class="glyphicon glyphicon-on"></span> 우
										</button>
									</div>
								</td>
							</tr>
							<tr>
								<th>주소</th>
								<td colspan="3">
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ADDR1" name="ADDR1" value="{{ old('ADDR1') }}" readonly="readonly" />
									</div>
								</td>
							</tr>
							<tr>
								<th>상세주소</th>
								<td colspan="3">
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ADDR2" name="ADDR2" value="{{ old('ADDR2') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>전화번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="PHONE_NO" name="PHONE_NO" value="{{ old('PHONE_NO') }}"/>
									</div>
								</td>
								<th>핸드폰번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="HP_NO" name="HP_NO" value="{{ old('HP_NO') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>이메일</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="EMAIL" name="EMAIL" value="{{ old('EMAIL') }}"/>
										@if ($errors->has('EMAIL'))
											<div class="help-block">
												<strong>{{ $errors->first('EMAIL') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>회원상태</th>
								<td>
									<div class="col-xs-12">
										<input type="checkbox" class='chkQuery' id="STATE" name="STATE" value="1" checked />활성화
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 center">
						
						<button type="button" class="btn btn-danger" id="cancel">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>

						<button type="submit" class="btn btn-success " id="btnSave">
							<span class="glyphicon glyphicon-off"></span> 저장
						</button>
					
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	$(function () {

		// 활성화 및 비조합원 구분 
		$(".chkQuery").click(function(){
			var value = $(this).is(":checked") ? "1" : "0";
			$(this).val(value);
		});

		//지역검색 팝업
		$("#btnSearchAreaCd").click(function(){ 
			$('#modal_corpEdit').modal({show:true}); 
				var pTable = $('#tableAreaCdList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					ajax: {
						url: "/custcd/cust_grp_info/getAreaCd",
						data:function(d){
							d.textSearch = $("input[name='textSearchAreaCdEdit']").val();
						}
					},
					columns: [
						{ data: 'AREA_CD', name: 'AREA_CD' },
						{ data: 'AREA_NM',  name: 'AREA_NM' },
						{
							data:   "AREA_CD",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 추가</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

			$('#tableAreaCdList tbody').on( 'click', 'button', function () {
				var data = pTable.row( $(this).parents('tr') ).data(); 
				$("#AREA_NM").val(data.AREA_NM);
				$("#AREA_CD").val(data.AREA_CD);
				$("input[name='textSearchAreaCdEdit']").val('');
				$('#modal_corpEdit_close').click();
			});
		});

		$("input[name='textSearchAreaCdEdit']").on("keyup", function(){
			pTable.search($(this).val()).draw();
		});
		//지역검색 팝업 끝
		
		// 목록바로가기
		$("#btnList").click(function(){
			location.href="/member/member/index?page=1";
		});


		//취소버튼
		$("#cancel").on("click",function(){
			window.location.href="/corp/cust_grp_info/index";
		});
	});
</script>

<script>
	function sample6_execDaumPostcode() {
		new daum.Postcode({
			oncomplete: function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 각 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullAddr = ''; // 최종 주소 변수
				var extraAddr = ''; // 조합형 주소 변수

				// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
				if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
					fullAddr = data.roadAddress;

				} else { // 사용자가 지번 주소를 선택했을 경우(J)
					fullAddr = data.jibunAddress;
				}

				// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
				if(data.userSelectedType === 'R'){
					//법정동명이 있을 경우 추가한다.
					if(data.bname !== ''){
						extraAddr += data.bname;
					}
					// 건물명이 있을 경우 추가한다.
					if(data.buildingName !== ''){
						extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
					fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
				}
				
				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('POST_NO').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('ADDR1').value = fullAddr;
				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('ADDR2').focus();
			}
		}).open();
	}
</script>

<div class="modal fade" id="modal_corpEdit" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 지역코드 조회</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="form-group SearchAreaCdEdit">
					<div class="box box-primary">
						<label for="textSearchAreaCdEdit"><span class="glyphicon glyphicon-user"></span> 지역명</label>
						<input type="text" class="form-control" name="textSearchAreaCdEdit" id="textSearchAreaCdEdit" />
						<div class="box-body">
							<table id="tableAreaCdList" class="table table-bordered table-hover" summary="게시물 목록">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th scope="col" class="name">지역코드</th>
										<th scope="col" class="name">지역명</th>
										<th scope="col" class="name">선택</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal" id="modal_corpEdit_close">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>

@stop
@extends('layouts.main')
@section('class','회원관리')
@section('title','사원정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-info" id="btnAddMember" ><i class="fa fa-user-plus"></i> 추가</button>
					<button type="button" class="btn btn-success" id="btnUpdateMember"><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDeleteMember"><i class="fa fa-user-times"></i> 삭제</button>
				</div>
			</div>
			<!--
			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="MEM_ID">회원아이디</option>
					<option value="CORP_NM">회사명</option>
					<option value="NAME">성명</option>
					<option value="STATE">회원상태</option>
				</select>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력(회원 아이디, 성명 ..)" value="{{ Request::Input('textSearch') }}">
					
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			-->
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="사원정보 목록" width="100%">
				<caption>사원정보</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="60px">회원아이디</th>
						<th width="50px">회사명</th>
						<th width="50px">성명</th>
						<th width="50px">직위</th>
						<th width="50px">그룹코드</th>
						<th width="50px">전화번호</th>
						<th width="50px">핸드폰번호</th>
						<th width="50px">상태</th>
						<th width="auto">급여</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 10개 조회 설정
			bLengthChange: false,
			ajax: {
				url: "/member/member/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},

			columns: [
				{
					data:   "MEM_ID",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center chk"
				},
				{
					data:   "MEM_ID",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<a href='/member/cust_grp_info/detail/"+data+"'>"+data+"</a>";
						}
						return data;
					},
					className: "dt-body-left"
				},
				{ data: 'FRNM', name: 'FRNM' ,className: "dt-body-left"},
				{ data: 'NAME', name: 'NAME' ,className: "dt-body-left"},
				{ data: 'PSTN', name: 'PSTN' ,className: "dt-body-center"},
				{ data: 'GRP_CD', name: 'GRP_CD' ,className: "dt-body-center"},
				{ data: 'PHONE_NO', name: 'PHONE_NO' ,className: "dt-body-center"},
				{ data: 'HP_NO', name: 'HP_NO' ,className: "dt-body-center"},
				{
					data:   "STATE",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data == "1"){
								return "<input type='checkbox' checked disabled />";
							}else{
								return "<input type='checkbox' disabled />";
							}
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data:   "MEM_ID",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-info btnGoSalary'><i class='fa fa-won'></i> 급여관리</botton>";
						}
						return data;
					},
					className: "dt-body-left"
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		
		$("#btnAddMember").click(function(){
			location.href = "/member/cust_grp_info/insert/{{Session::get('CORP_MK')}}";
		});
		
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$('#tblList tbody').on('click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			
			if( data.MEM_ID === null || data.MEM_ID === undefined || data.MEM_ID === ""){

				$(".content > .alert").remove();
				$(".content").prepend( getAlert('danger', '오류', "사용자 계정에 문제가 있습니다. 관리자에게 문의하세요.") ).show();
				return false;
			}
			
			location.href = "/member/member/salary/" + data.MEM_ID ;
		} );
			

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td.chk > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td.chk > input[type=checkbox]").prop("checked", true);
			}
		});

		//수정버튼
		$("#btnUpdateMember").click(function(){

			var arr_code = [];
			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				window.location.href="/member/cust_grp_info/update/"+arr_code[0]+"";
			}
		});

		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#btnDeleteMember').click( function () {

			var arr_code = [];
				$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
					if ($(this).is(":checked")) {
						arr_code.push($(this).val());
					}
				});
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				if(confirm("선택된 내용을 삭제하시겠습니까?")){
					
					$.getJSON("/member/cust_grp_info/_delete", {
						cd : arr_code[0] ,
						_token : '{{ csrf_token() }}'
					}, function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							for(var code in data.code){
								oTable.draw();
							}
						}
					}, function(xhr){
						console.log(xhr)
					});
				}
			}
		});
	});
</script>
@stop
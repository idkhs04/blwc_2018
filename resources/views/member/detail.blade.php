@extends('layouts.main')
@section('class','회원관리')
@section('title','사원정보')
@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}

	#tblDetail > tbody > tr > td{
		width:25%;
	}
	#tblDetail > tbody > tr > th{
		width:10%;
		background-color: darkgray;
		text-align:right;
	}
	#tblDetail > tbody > tr > td > textarea{
		width:100%;
		height:100px;
	}

	#tblDetail > tbody > tr > th{
		width:10%;
		background-color:khaki;
		text-align:right;
		height:25px;
	}
	
	.form-control { height:28px;padding-top:3px;}
	input.form-control { width:200px;}
	select.form-control { width:200px;}

	table.dataTable { width:70%; float:left;}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					@role('sysAdmin')
					<button type="button" class="btn btn-success" id="btnAddMember" ><i class="fa fa-plus"></i> 추가</button>
					@endrole
					<button type="button" class="btn btn-success" id="btnUpdateMember" ><i class="fa fa-edit"></i> 수정</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" value="{{$mem_info->GRP_CD}}" name="GRP_CD_HIDDEN" id="GRP_CD_HIDDEN"/>
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-12 col-xs-12">
				<table id="tblDetail" class="table table-bordered table-hover display nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<th>회원아이디</th>
							<td colspan="3">
								<div class="col-xs-12">
									{{ $mem_info->MEM_ID }}
								</div>
							</td>
						</tr>
						<tr>
							<th>그룹코드</th>
							<td>
								<div class="col-xs-12">
									<select class="form-control" id="GRP_CD" name="GRP_CD" disabled>
										<option value="CON">테스트 시스템담당(CON)</option>
										<option value="EMP">사원(EMP)</option>
										<option value="GEN">테스트 일반사원(GEN)</option>
									</select>
								</div>
							</td>
							<th>업체부호</th>
							<td>
								<div class="col-xs-12">
								{{$mem_info->CORP_MK}}
								</div>
							</td>
						</tr>
						<tr>
							<th>급호</th>
							<td>
								<div class="col-xs-12">
									{{ $mem_info->GRADE }}
								</div>
							</td>
							<th>직위</th>
							<td>
								<div class="col-xs-12">
									{{ $mem_info->PSTN }}
								</div>
							</td>
						</tr>
						<tr>
							<th>성명</th>
							<td>
								<div class="col-xs-12">
								{{ $mem_info->NAME }}
								</div>
							</td>
							<th>주민번호</th>
							<td>
								<div class="col-xs-12">
								{{ $mem_info->JUMIN_NO }}
								</div>
							</td>
						</tr>
						<tr>
							<th>우편번호</th>
							<td colspan="3">
								<div class="col-xs-12">
								{{ $mem_info->POST_NO }}
								</div>
							</td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3">
								<div class="col-xs-12">
								{{ $mem_info->ADDR1 }}
								</div>
							</td>
						</tr>
						<tr>
							<th>상세주소</th>
							<td colspan="3">
								<div class="col-xs-12">
								{{ $mem_info->ADDR2 }}
								</div>
							</td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td>
								<div class="col-xs-12">
								{{ $mem_info->PHONE_NO }}
								</div>
							</td>
							<th>핸드폰번호</th>
							<td>
								<div class="col-xs-12">
								{{ $mem_info->HP_NO }}
								</div>
							</td>
						</tr>
						<tr>
							<th>이메일</th>
							<td>
								<div class="col-xs-12">
								{{ $mem_info->EMAIL }}
								</div>
							</td>
							<th>회원상태</th>
							<td>
								<div class="col-xs-12">
								@if($mem_info->STATE)
									활성화
								@else
									비활성화
								@endif
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		// 목록바로가기
		$("#btnList").click(function(){
			location.href="/member/member/index?page=1";
		});

		// 추가 바로가기
		$("#btnAddMember").click(function(){
			location.href="/member/member/insert/{{$mem_info->CORP_MK}}";
		});

		//수정버튼
		$("#btnUpdateMember").click(function(){
			window.location.href="/member/cust_grp_info/update/{{$corp_mk}}";
		});


		var GRP_CD = $("#GRP_CD_HIDDEN").val();
		$("#GRP_CD > option[value='" + GRP_CD + "']").prop("selected", true);
		
		var vTable = $('#tblDetail').DataTable({
			"paging":   false,
			"ordering": false,
			"info":     false,
			"searching": false
		});

		

	});
</script>

<!-- 지역 검색 -->
<div class="modal fade" id="modal_corpEdit" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 지역코드 조회</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="form-group SearchAreaCdEdit">
					<div class="box box-primary">
						<label for="textSearchAreaCdEdit"><span class="glyphicon glyphicon-user"></span> 지역명</label>
						<input type="text" class="form-control" name="textSearchAreaCdEdit" id="textSearchAreaCdEdit" />
						<div class="box-body">
							<table id="tableAreaCdList" class="table table-bordered table-hover" summary="게시물 목록">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th scope="col" class="name">지역코드</th>
										<th scope="col" class="name">지역명</th>
										<th scope="col" class="name">선택</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal" id="modal_corpEdit_close">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
<!-- 지역 검색 끝 -->
@stop
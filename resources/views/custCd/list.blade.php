@extends('layouts.main')
@section('class','코드관리')
@section('title','거래처 등록')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	#modal_custCd label, #modal_custGrpEdit label{
		display:inline-block;
		width:24%;
		clear:both;
	}
	#modal_custCd .form-input-sm, #modal_custGrpEdit .form-input-sm{
		display:inline-block;
		width:24%;
		clear:both;
	}

	#modal_custCd .form-input-md, #modal_custGrpEdit .form-input-md{
		display:inline-block;
		width:24%;
		clear:both;
	}

	#modal_custCd .form-input-lg, #modal_custGrpEdit .form-input-lg{
		display:inline-block;
		width:74%;
		clear:both;
	}

	.SearchCustGrp , .SearchAreaCd, .SearchCustGrpEdit , .SearchAreaCdEdit{
		display:none;
	}

	#tableCustGrpList_filter, #tableAreaCdList_filter, #tableCustGrpEditList_filter, #tableAreaCdEditList_filter{
		display:none;
	}

	.btn.btn-info {
		vertical-align:baseline;
	}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	label[for='textSearchAreaCd'] { float:left;}

	#textSearchAreaCd{ width:150px;}

	.code{ 40px;}
	.code_nm{ 40px;}

	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}

	.table .table th{ background-color:khaki; text-align:right;}

	#modal_custCd  #POST_NO, #modal_custCd  #AREA_CD , #modal_custCd  #CUST_GRP_CD , #modal_custGrpEdit #CUST_GRP_CD_EDIT, #modal_custGrpEdit #AREA_CD_EDIT,  #modal_custGrpEdit #POST_NO_EDIT{ width:100px;}

	.modal label.rightLabel{
		padding-left:43px;
	}

</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-2 col-xs-6">
				<div class="btn-group">
					<button type="button" class="btn btn-info" id="btnAddCustCd" ><i class="fa fa-plus-circle"></i> 추가</button>
					<button type="button" class="btn btn-success" id="btnExcel" ><i class="fa fa-file-excel-o"></i> 엑셀</button>
					<!--
					<button type="button" class="btn btn-danger" id="btnDeleteCust" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-success btnLog" ><i class="fa fa-code"></i> 이력보기</button>
					-->
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition1">
					<option value='ALL' /> 그룹</option>
					@foreach( $custGrp as $c )
					<option value="{{$c->CUST_GRP_CD}}" >{{$c->CUST_GRP_NM}}</option>
					@endforeach
				</select>
			</div>

			<!--
			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					
					<option value="FRNM">상호</option>
					<option value="RPST">대표자</option>
					<option value="CUST_GRP_NM">그룹명</option>
					
				</select>
			</div>
			-->

			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control cis-lang-ko"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="거래처 목록" width="100%">
				<caption>거래처 관리</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th width="30px">상세</th>
						<th width="30px">관리</th>
						<th class="subject" width="80px">그룹명</th>
						<th class="subject" width="40px">코드</th>
						<th class="subject" width="40px">구분</th>
						<th class="subject" width="80px">상호</th>
						<th class="subject" width="60px">대표자</th>
						<th class="subject" width="50px">사업자번호</th>
						<th class="subject" width="50px">전화번호</th>
						<th class="subject" width="50px">H/P</th>
						<th class="subject" width="100px">E-mail</th>
						<th class="subject" width="auto">미수입력</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>

	$(function () {

		// 일일판매는 기본적으로 해당 달의 첫번째 달을 기준으로 조회
		var start = moment().startOf('month');
		var end = moment();
		start = end;
		// 초기값 세팅


		// 입고정보 : 입고일 
		$("input[name='WRITE_DATE'], #JUMIN_NO, #JUMIN_NO_EDIT").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});


		// 거래처추가 버튼
		$("#btnAddCustCd").click(function(){ 

			$('#modal_custCd').modal({show:true}); 
			$("#WRITE_DATE").val(end.format('YYYY-MM-DD'));
		});
		
		//거래처추가 닫기 버튼
		$("#modal_custCd .btn-danger").click(function(){ 
			$("#modal_custCd .form-group > input").val("");
			$("#modal_custCd ul").empty();
			
		});

		// 거래처추가 저장 버튼
		$("#btnInsertCustCdOK").click(function(){
			$.getJSON('/custcd/cust_grp_info/Insert', {
				_token		: '{{ csrf_token() }}'
				//, CUST_MK	: $("#CUST_MK").val()
				, CUST_GRP_CD	: $("#CUST_GRP_CD").val()
				, CUST_GRP_NM	: $("#CUST_GRP_NM").val()
				, CORP_DIV		: $("#CORP_DIV").val()
				, FRNM			: $("#FRNM").val()
				, RPST			: $("#RPST").val()
				, ETPR_NO		: $("#ETPR_NO").val()
				, JUMIN_NO		: $("#JUMIN_NO").val()
				, PHONE_NO		: $("#PHONE_NO").val()
				, UPTE			: $("#UPTE").val()
				, FAX			: $("#FAX").val()
				, UPJONG		: $("#UPJONG").val()
				, AREA_NM		: $("#AREA_NM").val()
				, AREA_CD		: $("#AREA_CD").val()
				, RANK			: $("#RANK").val()
				, POST_NO		: $("#POST_NO").val()
				, ADDR1			: $("#ADDR1").val()
				, ADDR2			: $("#ADDR2").val()
				, REMARK		: $("#REMARK").val()
				, WRITE_DATE	: $("#WRITE_DATE").val()
				, EMAIL			: $("#EMAIL").val()
				, INTERFACE_YN	: $("input[name='INTERFACE_YN']:checked").val()
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$('#modal_custCd').modal("hide");
				$("#modal_custCd .form-group > input").val("");
				$("#modal_custCd ul").empty();
				oTable.draw();
				//getSumQty();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_custCd .edit_alert');
				info.hide().find('ul').empty();

				for(var k in error.message){
					if(error.message.hasOwnProperty(k)){
						error.message[k].forEach(function(val){
							info.find('ul').append('<li>' + val + '</li>');
						});
					}
				}
				info.slideDown();
				
			});
		});

		// 거래처등록->거래처그룹 검색 팝업
		$("#btnSearchCustGrp, #btnSearchCustGrpEdit").click(function(){ 
			if( $(this).attr('id') == 'btnSearchCustGrp'){
				$( ".SearchCustGrp" ).show(function(){
					var pTable = $('#tableCustGrpList').DataTable({
						processing: true,
						serverSide: true,
						retrieve: true,
						iDisplayLength: 10,		// 기본 10개 조회 설정
						bLengthChange: false,
						bInfo : false,
						ajax: {
							url: "/custcd/cust_grp_info/getCustGrp",
							data:function(d){
								d.textSearch	= $("input[name='textSearchCustGrpCd']").val();
							}
						},
						columns: [
							{ data: 'CUST_GRP_CD', name: 'CUST_GRP_CD' },
							{ data: 'CUST_GRP_NM',  name: 'CUST_GRP_NM' },
							{
								data:   "CUST_GRP_CD",
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return "<button class='btn btn-success btn-block' ><i class='fa fa-arrow-circle-right'> 선택</button>";
									}
									return data;
								},
								className: "dt-body-center"
							},
						],
						"searching": true,
						"paging": true,
						"autoWidth": true,
						"oLanguage": {
							"sLengthMenu": "조회수 _MENU_ ",
							"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
							"sProcessing": "현재 조회 중입니다",
							"sEmptyTable": "조회된 데이터가 없습니다",
							"sZeroRecords": "조회된 데이터가 없습니다",
							"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
							"oPaginate": {
								"sFirst": "처음",
								"sLast": "끝",
								"sNext": "다음",
								"sPrevious": "이전"
							}
						}
					});

					$('#tableCustGrpList tbody').on( 'click', 'button', function () {
						var data = pTable.row( $(this).parents('tr') ).data();
	 
						$("#CUST_GRP_CD").val(data.CUST_GRP_CD);
						$("#CUST_GRP_NM").val(data.CUST_GRP_NM);
						
						$("input[name='textSearchCustGrpCd']").val('');
						$(".SearchCustGrp" ).hide();	
					});

					$("input[name='textSearchCustGrpCd']").on("keyup", function(){
						pTable.search($(this).val()).draw() ;
					});
				});
			}else if( $(this).attr('id') == 'btnSearchCustGrpEdit'){
				$( ".SearchCustGrpEdit" ).show(function(){
					var pTable = $('#tableCustGrpEditList').DataTable({
						processing: true,
						serverSide: true,
						retrieve: true,
						
						ajax: {
							url: "/custcd/cust_grp_info/getCustGrp",
							data:function(d){
								d.textSearch	= $("input[name='textSearchCustGrpCdEdit']").val();
							}
						},
						columns: [
							{ data: 'CUST_GRP_CD', name: 'CUST_GRP_CD' },
							{ data: 'CUST_GRP_NM',  name: 'CUST_GRP_NM' },
							{
								data:   "CUST_GRP_CD",
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return "<button class='btn btn-success btn-block' ><i class='fa fa-arrow-circle-right'> 선택</button>";
									}
									return data;
								},
								className: "dt-body-center"
							},
						],
						"searching": true,
						"paging": true,
						"autoWidth": true,
						"oLanguage": {
							"sLengthMenu": "조회수 _MENU_ ",
							"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
							"sProcessing": "현재 조회 중입니다",
							"sEmptyTable": "조회된 데이터가 없습니다",
							"sZeroRecords": "조회된 데이터가 없습니다",
							"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
							"oPaginate": {
								"sFirst": "처음",
								"sLast": "끝",
								"sNext": "다음",
								"sPrevious": "이전"
							}
						}
					});

					$('#tableCustGrpEditList tbody').on( 'click', 'button', function () {
						var data = pTable.row( $(this).parents('tr') ).data();
	 
						$("#CUST_GRP_CD_EDIT").val(data.CUST_GRP_CD);
						$("#CUST_GRP_NM_EDIT").val(data.CUST_GRP_NM);
						$("input[name='textSearchCustGrpCdEdit']").val('');
						$(".SearchCustGrpEdit" ).hide();	
					});

					$("input[name='textSearchCustGrpCdEdit']").on("keyup", function(){
						pTable.search($(this).val()).draw() ;
					});
				});
			}
		});
		//거래처등록->거래처그룹 검색 팝업 닫기
		$(".btn-CustCd-Close").click(function(){ 
			$("div.SearchCustGrpEdit, .SearchCustGrp").hide(); 
		});
		
		
		//거래처등록->지역검색 팝업
		$("#btnSearchAreaCd ,#btnSearchAreaCd_Edit").click(function(){ 
			if( $(this).attr('id') == 'btnSearchAreaCd'){
				$( ".SearchAreaCd" ).show(function(){
					var pTable = $('#tableAreaCdList').DataTable({
						processing: true,
						serverSide: true,
						retrieve: true,
						iDisplayLength: 100,		// 기본 10개 조회 설정
						bLengthChange: false,
						bInfo:false,
						ajax: {
							url: "/custcd/cust_grp_info/getAreaCd",
							data:function(d){
								d.textSearch	= $("input[name='textSearchAreaCd']").val();
							}
						},
						columns: [
							{ data: 'AREA_CD', name: 'AREA_CD' , className: "dt-body-center"},
							{ data: 'AREA_NM',  name: 'AREA_NM' },
							{
								data:   "AREA_CD",
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return "<button class='btn btn-success btn-block' ><i class='fa fa-arrow-circle-right'></span> 선택</button>";
									}
									return data;
								},
								className: "dt-body-center"
							},
						],
						"searching": true,
						"paging": true,
						"autoWidth": true,
						"oLanguage": {
							"sLengthMenu": "조회수 _MENU_ ",
							"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
							"sProcessing": "현재 조회 중입니다",
							"sEmptyTable": "조회된 데이터가 없습니다",
							"sZeroRecords": "조회된 데이터가 없습니다",
							"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
							"oPaginate": {
								"sFirst": "처음",
								"sLast": "끝",
								"sNext": "다음",
								"sPrevious": "이전"
							}
						}
					});

					$('#tableAreaCdList tbody').on( 'click', 'button', function () {
						var data = pTable.row( $(this).parents('tr') ).data();
	 
						$("#AREA_NM").val(data.AREA_NM);
						$("#AREA_CD").val(data.AREA_CD);
						$("input[name='textSearchAreaCd']").val('');
						$(".SearchAreaCd" ).hide();	
					});
	 
					$("input[name='textSearchAreaCd']").on("keyup", function(){
						pTable.search($(this).val()).draw() ;
					});
				});
			}
			else if( $(this).attr('id') == 'btnSearchAreaCd_Edit' ){
				$( ".SearchAreaCdEdit" ).show(function(){
					var pTable = $('#tableAreaCdEditList').DataTable({
						processing: true,
						serverSide: true,
						retrieve: true,
						iDisplayLength: 100,		// 기본 10개 조회 설정
						bLengthChange: false,
						bInfo:false,
						ajax: {
							url: "/custcd/cust_grp_info/getAreaCd",
							data:function(d){
								d.textSearch	= $("input[name='textSearchAreaCdEdit']").val();
							}
						},
						columns: [
							{ data: 'AREA_CD', name: 'AREA_CD' , className: "dt-body-center"},
							{ data: 'AREA_NM',  name: 'AREA_NM' },
							{
								data:   "AREA_CD",
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return "<button class='btn btn-success btn-block' ><i class='fa fa-arrow-circle-right'></i> 선택</button>";
									}
									return data;
								},
								className: "dt-body-center"
							},
						],
						"searching": true,
						"paging": true,
						"autoWidth": true,
						"oLanguage": {
							"sLengthMenu": "조회수 _MENU_ ",
							"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
							"sProcessing": "현재 조회 중입니다",
							"sEmptyTable": "조회된 데이터가 없습니다",
							"sZeroRecords": "조회된 데이터가 없습니다",
							"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
							"oPaginate": {
								"sFirst": "처음",
								"sLast": "끝",
								"sNext": "다음",
								"sPrevious": "이전"
							}
						}
					});

					$('#tableAreaCdEditList tbody').on( 'click', 'button', function () {
						var data = pTable.row( $(this).parents('tr') ).data();
						$("#AREA_NM_EDIT").val(data.AREA_NM);
						$("#AREA_CD_EDIT").val(data.AREA_CD);
						$("input[name='textSearchAreaCdEdit']").val('');
						$(".SearchAreaCdEdit" ).hide();	
					});
					$("input[name='textSearchAreaCdEdit']").on("keyup", function(){
						pTable.search($(this).val()).draw() ;
					});
				});
			}
		});
		//거래처등록->지역검색 팝업 닫기
		$(".btn-AreaCd-Close").click(function(){ $("div.SearchAreaCd").hide(); $("div.SearchAreaCdEdit").hide(); });

		// 모달닫기
		$('.modal').on('hidden.bs.modal', setInitModalClose);
		
		// 미수입력
		$("body").on("click", ".btnAddUncl", function(){
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href="/uncl/uncl/detail2/" + data.CUST_MK;
		});
		
		// 거래처수정 모달
		$("body").on("click", ".btnUpdate", function(){

			var arr_code = [];
		
			var data = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(data.CUST_MK);
		
			if( arr_code.length == 1){
				var page = $("input[name='page']").val()	
				$.getJSON("/custcd/cust_grp_info/Edit/" + arr_code[0] + "/page=" + page+"", {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(data){

					// 거래처그룹 팝업 금지
					//$("#btnSearchCustGrpEdit").prop("disabled", true);
					
					$("#CUST_MK_EDIT").val(data.CUST_MK);
					$("#CUST_GRP_NM_EDIT").val(data.CUST_GRP_NM);
					$("#CUST_GRP_CD_EDIT").val(data.CUST_GRP_CD);
					$("input[name='CUST_GRP_CD_OLD']").val(data.CUST_GRP_CD);

					$("#CORP_DIV_EDIT option[value='" + data.CORP_DIV + "']").prop("selected", true);
				
					$("#FRNM_EDIT").val(data.FRNM);
					$("#RPST_EDIT").val(data.RPST);
					$("#RANK_EDIT > option").each(function(i , d){
						if( $(d).val() == data.RANK_EDIT){
							$(d).attr('selected','selected');
						}
					});
					$("#ETPR_NO_EDIT").val(data.ETPR_NO);
					$("#JUMIN_NO_EDIT").val(data.JUMIN_NO);
					$("#PHONE_NO_EDIT").val(data.PHONE_NO);
					$("#FAX_EDIT").val(data.FAX);
					$("#ADDR1_EDIT").val(data.ADDR1);
					$("#ADDR2_EDIT").val(data.ADDR2);
					$("#POST_NO_EDIT").val(data.POST_NO);
					$("#UPJONG_EDIT").val(data.UPJONG);
					$("#UPTE_EDIT").val(data.UPTE);
					$("#AREA_CD_EDIT").val(data.AREA_CD);
					$("#AREA_NM_EDIT").val(data.AREA_NM);
					$("#REMARK_EDIT").val(data.REMARK);
					$('#modal_custGrpEdit').modal({show:true});

					$("#WRITE_DATE_EDIT").val(data.WRITE_DATE);
					$("#EMAIL_EDIT").val(data.EMAIL);
					
					if( data.INTERFACE_YN !== null){
						$("input[name='INTERFACE_YN_EDIT']").prop('checked', false);
						$("input[name='INTERFACE_YN_EDIT'][value='" + data.INTERFACE_YN + "']").prop('checked', true);
					}else{
						$("input[name='INTERFACE_YN_EDIT'][value='Y']").prop('checked', false);
						$("input[name='INTERFACE_YN_EDIT'][value='N']").prop('checked', true);
					}
				});
			}
		});
		
		// 거래처수정 
		$("#btnUpdateCustCdOK").click(function(){
			//$.getJSON('/buy/sujo/addDataSujo', {
			$.getJSON('/custcd/cust_grp_info/Update', {
				_token		: '{{ csrf_token() }}'
				, CUST_GRP_CD_EDIT	: $("#CUST_GRP_CD_EDIT").val()
				, CUST_MK_EDIT		: $("#CUST_MK_EDIT").val()
				, CORP_DIV_EDIT		: $("#CORP_DIV_EDIT").val()
				, FRNM_EDIT			: $("#FRNM_EDIT").val()
				, RPST_EDIT			: $("#RPST_EDIT").val()
				, ETPR_NO_EDIT		: $("#ETPR_NO_EDIT").val()
				, JUMIN_NO_EDIT		: $("#JUMIN_NO_EDIT").val()
				, PHONE_NO_EDIT		: $("#PHONE_NO_EDIT").val()
				, FAX_EDIT			: $("#FAX_EDIT").val()
				, ADDR1_EDIT		: $("#ADDR1_EDIT").val()
				, ADDR2_EDIT		: $("#ADDR2_EDIT").val()
				, POST_NO_EDIT		: $("#POST_NO_EDIT").val()
				, UPJONG_EDIT		: $("#UPJONG_EDIT").val()
				, UPTE_EDIT			: $("#UPTE_EDIT").val()
				, AREA_CD_EDIT		: $("#AREA_CD_EDIT").val()
				, REMARK_EDIT		: $("#REMARK_EDIT").val()
				, WRITE_DATE_EDIT	: $("#WRITE_DATE_EDIT").val()
				, EMAIL_EDIT		: $("#EMAIL_EDIT").val()
				, INTERFACE_YN_EDIT	: $("input[name='INTERFACE_YN_EDIT']:checked").val()
			}).success(function(xhr){
				$('#modal_custGrpEdit').modal("hide");
				//수정후 목록이 재갱신됨을 차단 요청 : 2017-04-24(한려수산)
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				if(error.reason == "PrimaryKey"){
					alert("거래처 그룹코드가 있습니다.");
					return false;
				}else{
					var info = $('#modal_custGrpEdit .edit_alert');
					info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
					info.slideDown();
				}
			});
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 10개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/custcd/custcd/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.srtCondition1	= $("select[name='srtCondition1'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					
				}
			},
			order: [[ 5, 'asc' ]],
			columns: [
				{
					data:   "CUST_MK",
					//name: "GRP.CUST_GRP_CD",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{
					data:  "CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return	"<button type='button' class='btn btn-success btnUpdate'><i class='fa fa-edit'></i> 수정</button>" +
									" <button type='button' class='btn btn-danger btnDeleteCust'><i class='fa fa-remove'></i> 삭제</button>" ;
						}
						return data;
					},
					orderable:  false,
					className: "dt-body-left"
				},

				{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM' },
				{ data: 'CUST_MK', name: 'CUST_MK' },
				{ 
					data:   "CORP_DIV",
					render: function ( data, type, row ) {
						if (data !== null && data == "S"){
							return "<span class='red'>매출처</span>";
						}else if (data !== null && data == "P"){
							return "<span class='blue'>매입처</span>";
						}else if (data !== null && data == "T" ){	
							return "<span class='green'>비용계정</span>";
						}else{
							return "<span class='bold'>활어조합</span>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ 
					data:   "FRNM",
					render: function ( data, type, row ) {
						return "<span class='bold'>" + data + "</span>";
					},
					className: "dt-body-left"
				},
				{ data: 'RPST', name: 'RPST' },
				{
					data:   "ETPR_NO",
					render: function ( data, type, row ) {
						if (data !== null && data.length == 10)
						{
							firstNo		= data.substr(0, 3);
							secondNo	= data.substr(3, 2);
							thirdNo		= data.substr(5, 5);
							
							return firstNo + "-" + secondNo + "-" + thirdNo
						}
						return data;
					},
					className: "dt-body-center"
				},
				
				{ data: 'PHONE_NO', name: 'PHONE_NO' , className: "dt-body-left" },
				{ data: 'FAX', name: 'FAX' , className: "dt-body-left" },
				{ data: 'EMAIL', name: 'EMAIL' , className: "dt-body-left" },
				{
					data:   "CUST_MK",
					render: function ( data, type, row ) {
						return "<button type='button' class='btn btn-info btnAddUncl'><i class='fa fa-edit'></i>미수입력</button>";
					},
					className: "dt-body-left"
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});

		$("select[name='srtCondition1']").on("change", function(){
			oTable.draw() ;
		});

		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		
		//상세보기 버튼 클릭
		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});

		function format ( d ) {
			// `d` is the original data object for the row
			return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:50%" class="table table-bordered table-hover display nowrap">'+
					'<tr>'+
						'<th>전화번호:</th>'+
						'<td>'+ ( d.PHONE_NO === null ? "" : d.PHONE_NO ) +'</td>'+
						'<th>생년월일:</th>'+
						'<td>'+ ( d.JUMIN_NO === null ? "" : d.JUMIN_NO ) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>업태:</th>'+
						'<td>'+ ( d.UPTE === null ? "" : d.UPTE )  +'</td>'+
						'<th>종목:</th>'+
						'<td>'+ ( d.UPJONG === null ? "" : d.UPJONG ) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>지역코드:</th>'+
						'<td>'+ ( d.AREA_CD === null ? "" : d.AREA_CD ) +'</td>'+
						'<th>지역명:</th>'+
						'<td>'+ ( d.AREA_NM === null ? "" : d.AREA_NM ) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>우편번호:</th>'+
						'<td colspan="3">'+ ( d.POST_NO === null ? "" : d.POST_NO ) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>주소:</th>'+
						'<td>'+ ( d.ADDR1 === null ? "" : d.ADDR1 )+'</td>'+
						'<th>상세주소:</th>'+
						'<td>'+ ( d.ADDR2 === null ? "" : d.ADDR2 ) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<th>첫거래일자:</th>'+
						'<td>'+ ( d.WRITE_DATE === null ? "" : d.WRITE_DATE )   +'</td>'+
						'<th>이메일주소:</th>'+
						'<td>'+ ( d.EMAIL === null ? "" : d.EMAIL ) +'</td>'+
					'</tr>'+
				'</table>';
		}

		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$("body").on("click", ".btnDeleteCust", function(){

			var arr_code = [];
		
			var data = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(data.CUST_MK);
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 거래처를 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 거래처는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				if(confirm("선택된 거래처를 삭제하시겠습니까?")){

					$.getJSON("/custcd/cust_grp_info/_delete", {
						cd : arr_code ,
						_token : '{{ csrf_token() }}'
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							for(var code in data.code){
								oTable.row('.data-code').remove().draw(false);
							}
						}
					}).error(function(xhr,status, response) {
						$(".content > .alert").remove();
						$(".content").prepend( getAlert('warning', '경고', "해당 거래처는 사용중이므로 삭제할 수 없습니다.") ).show();
					});
				}
			}
		});

		// 상세보기 버튼 클릭
		$('#tblListStockSujo tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = sTable.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});

		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$(".modal input[type='text']").val("");
			$(".modal textarea").val("");
			$(".modal select option:eq(0)").prop("selected", "selected");
			$("div.tableCustGrpList_wrapper").hide(); 
			$("div.SearchAreaCdEdit").hide();
			$("div.SearchCustGrpEdit, .SearchCustGrp").hide(); 
			$(".SearchAreaCd").hide();
		}

		$(".btnLog").click(function(){
			location.href = '/custcd/custcd/log';
		});

		$("#btnExcel").click(function(){

			var arr_data = [];

			arr_data.push(
							{
								'srtCondition'		: $("select[name='srtCondition'] option:selected").val()
								, 'srtCondition1'	: $("select[name='srtCondition1'] option:selected").val()
								, 'textSearch'		: $("input[name='textSearch']").val()
							}
						);

			var url		= '/custcd/custcd/Excel';
			$.fileDownload( url , {
				httpMethod: "GET",
				//_token: '{{ csrf_token() }}',
				data: {query : arr_data},
				successCallback: function (url) {
					//$preparingFileModal.dialog('close');
				},
			});
		});
	});
</script>

<script>
    function sample6_execDaumPostcode(mode) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }
				
				if( mode == 'insert'){
					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('POST_NO').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('ADDR1').value = fullAddr;
					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('ADDR2').focus();

				}else if( mode == 'edit'){
					// 우편번호와 주소 정보를 해당 필드에 넣는다.
					document.getElementById('POST_NO_EDIT').value = data.zonecode; //5자리 새우편번호 사용
					document.getElementById('ADDR1_EDIT').value = fullAddr;
					// 커서를 상세주소 필드로 이동한다.
					document.getElementById('ADDR2_EDIT').focus();
				} 
            }
        }).open();
    }
</script>

<!-- 거래처 등록-->
<div class="modal fade" id="modal_custCd" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 등록</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="CUST_GRP_CD"><i class='fa fa-check-circle text-red'></i> 거래처그룹</label>
					<input type="text" class="form-control form-input-sm code" id="CUST_GRP_CD" placeholder="" name="CUST_GRP_CD" value="" readonly="readonly"/>
					<button type="button" name="btnSearchCustGrp" id="btnSearchCustGrp" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md code_nm" id="CUST_GRP_NM" placeholder="" name="CUST_GRP_NM" value="" readonly="readonly"/>
				</div>
				<!-- 거래처그룹 검색 -->
				<div class="form-group SearchCustGrp">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 거래처그룹 선택</h5>					
						<label for="textSearchCustGrpCd"><i class='fa fa-circle-o text-aqua'></i>  거래처 부호</label>
						<input type="text" name="textSearchCustGrpCd" id="textSearchCustGrpCd" />
					
						<button type="button" class="btn btn-danger pull-right btn-CustCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>
				
						<div class="box-body">
							<table id="tableCustGrpList" class="table table-bordered table-hover display nowrap" summary="거래처그룹" width="100%">
								<caption>거래처그룹 선택</caption>
								<thead>
									<tr>
										<th class="name">그룹코드</th>
										<th class="name">그룹명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- 거래처그룹 검색 끝 -->
				<div class="form-group">
					<label for="CORP_DIV"><i class='fa fa-check-circle text-red'></i> 업체구분</label>
					<select class="form-control form-input-md" name="CORP_DIV" id="CORP_DIV">
						<option value="">== 선 택 ==</option>
						<option value="J">활어조합</option>
						<option value="P">활어매입처</option>
						<option value="S">활어매출처</option>
						<option value="T">비용계정</option>
					</select>
					<label for="WRITE_DATE" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 첫거래일자</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="WRITE_DATE" placeholder="" name="WRITE_DATE" value="">
				</div>
				<div class="form-group">
					<label for="FRNM"><i class='fa fa-check-circle text-red'></i> 거래처상호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="FRNM" placeholder="" name="FRNM" value="">
					<label for="RPST" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 대표자</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="RPST" placeholder="" name="RPST" value="">
				</div>
				<div class="form-group">
					<label for="ETPR_NO"><i class='fa fa-circle-o text-aqua'></i> 사업자등록번호</label>
					
					<input type="text" class="form-control form-input-md cis-lang-ko" id="ETPR_NO" placeholder="" name="ETPR_NO" value="">
					<label for="JUMIN_NO" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 생년월일</label>
					<input type="text" class="form-control form-input-md" id="JUMIN_NO" placeholder="" name="JUMIN_NO" value="">
				</div>
				<div class="form-group">
					<label for="FAX"><i class='fa fa-circle-o text-aqua'></i> H/P</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="FAX" placeholder="" name="FAX" value="">
					<label for="UPTE" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 업태</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="UPTE" placeholder="" name="UPTE" value="">
				</div>
				<div class="form-group">

					<label for="PHONE_NO"><i class='fa fa-circle-o text-aqua'></i> 전화번호</label>
					<input type="text" class="form-control form-input-md" id="PHONE_NO" placeholder="" name="PHONE_NO" value="">
					<label for="UPJONG" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 종목</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="UPJONG" placeholder="" name="UPJONG" value="">
				</div>
				<div class="form-group">
					<label for="AREA_CD"><i class='fa fa-check-circle text-red'></i> 지역</label>
					<input type="text" class="form-control form-input-sm" id="AREA_CD" placeholder="" name="AREA_CD" value="" readonly="readonly"/>
					<button type="button" name="btnSearchAreaCd" id="btnSearchAreaCd" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md" id="AREA_NM" placeholder="" name="AREA_NM" value="" readonly="readonly"/>
				</div>
				<!-- 지역 검색 -->
				<div class="form-group SearchAreaCd">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 지역코드 조회</h5>					
						<label for="textSearchAreaCd"><i class='fa fa-circle-o text-aqua'></i> 지역명</label>
						<input type="text" class="form-control cis-lang-ko" name="textSearchAreaCd" id="textSearchAreaCd" />
						
						<button type="button" class="btn btn-danger pull-right btn-AreaCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>
				
						<div class="box-body">
							<table id="tableAreaCdList" class="table table-bordered table-hover display nowrap" summary="지역코드 선택 목록">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th class="name">지역코드</th>
										<th class="name">지역명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- 지역 검색 끝 -->
				<!--
				<div class="form-group">
					<label for="RANK"><i class='fa fa-circle-o text-aqua'></i>  등급</label>
					<select class="form-control form-input-md" name="RANK" id="RANK">
						<option value="SU">우수</option>
						<option value="GE">일반</option>
						<option value="BD">불량</option>
					</select>
				</div>
				-->
				<div class="form-group">
					<label for="POST_NO"><i class='fa fa-circle-o text-aqua'></i> 우편번호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="POST_NO" placeholder="" name="POST_NO" value="" />
					<button type="button" name="searchPisMk" id="btnSearchPisMk" class="btn btn-info" onclick="sample6_execDaumPostcode('insert')" value="우편번호 찾기">우</button>

					<label for="이메일주소" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 이메일주소 </label>
					<input type="text" class="form-control form-input-sm cis-lang-en" id="EMAIL" placeholder="" name="EMAIL" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR1"><i class='fa fa-circle-o text-aqua'></i> 주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR1" placeholder="" name="ADDR1" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR2"><i class='fa fa-circle-o text-aqua'></i> 상세주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR2" placeholder="" name="ADDR2" value="" />
				</div>
				<div class="form-group">
					<label for="REMARK"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="REMARK" placeholder="" name="REMARK" value="" />
				</div>
				<div class="form-group" style="display:none;">
					<label for="INTERFACE_Y" style="width:135px;"><i class='fa fa-circle-o text-aqua'></i> 미수/미지급 출력</label>
					<input type="radio" id="INTERFACE_Y" name="INTERFACE_YN" value="Y" checked="checked" /> 예
					<input type="radio" id="INTERFACE_N" name="INTERFACE_YN" value="N" /> 아니오
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success pull-right" id="btnInsertCustCdOK">
					<span class="glyphicon glyphicon-off"></span> 등록
				</button>

				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 거래처 수정-->
<div class="modal fade" id="modal_custGrpEdit" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="CUST_GRP_CD"><i class='fa fa-circle-o text-red'></i>  거래처그룹</label>
					<input type="text" class="form-control form-input-sm" id="CUST_GRP_CD_EDIT" placeholder="" name="CUST_GRP_CD" value="" readonly="readonly"/>
					<input type="hidden" placeholder="" name="CUST_GRP_CD_OLD" value="" />
					<button type="button" name="btnSearchCustGrpEdit" id="btnSearchCustGrpEdit" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md" id="CUST_GRP_NM_EDIT" placeholder="" name="CUST_GRP_NM" value="" readonly="readonly"/>
				</div>
				<!-- 거래처그룹 검색 -->
				<div class="form-group SearchCustGrpEdit">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 거래처그룹 선택</h5>					
						<label for="textSearchCustGrpCdEdit"><i class='fa fa-circle-o text-aqua'></i>  거래처 부호</label>
						<input type="text" class="" name="textSearchCustGrpCdEdit" id="textSearchCustGrpCdEdit" />
						
						<button type="button" class="btn btn-danger pull-right btn-CustCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>
				
						<div class="box-body">
							<table id="tableCustGrpEditList" class="table table-bordered table-hover display nowrap" summary="거래처그룹 목록">
								<caption>거래처그룹 선택</caption>
								<thead>
									<tr>
										<th class="name">그룹코드</th>
										<th class="name">그룹명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- 거래처그룹 검색 끝 -->
				<div class="form-group">
					<label for="CUST_MK"><i class='fa fa-circle-o text-red'></i>  거래처부호</label>
					<input type="text" class="form-control form-input-md" id="CUST_MK_EDIT" placeholder="" name="CUST_MK" value="" readonly="readonly">
					
					<div style="display:none;">
						<label for="INTERFACE_YN_EDIT" class='rightLabel' style="width:175px;"><i class='fa fa-circle-o text-aqua'></i> 미수/미지급 출력</label>
						<input type="radio" id="INTEFACE_Y_EDIT" placeholder="" name="INTERFACE_YN_EDIT" value="Y" checked="checked"> 예
						<input type="radio" id="INTEFACE_N_EDIT" placeholder="" name="INTERFACE_YN_EDIT" value="N"> 아니오
					</div>
				</div>
				<div class="form-group">
					<label for="CORP_DIV"><i class='fa fa-circle-o text-aqua'></i>  업체구분</label>
					<select class="form-control form-input-md" name="CORP_DIV" id="CORP_DIV_EDIT">
						<option value="J">활어조합</option>
						<option value="P">활어매입처</option>
						<option value="S">활어매출처</option>
						<option value="T">비용계정</option>
					</select>

					<label for="WRITE_DATE_EDIT" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 첫거래일자</label>
					<input type="text" class="form-control form-input-md" id="WRITE_DATE_EDIT" placeholder="" name="WRITE_DATE" value="" />
				</div>
				<div class="form-group">
					<label for="FRNM"><i class='fa fa-check-circle text-red'></i>  거래처상호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="FRNM_EDIT" placeholder="" name="FRNM" value="">
					<label for="RPST" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  대표자</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="RPST_EDIT" placeholder="" name="RPST" value="">
				</div>
				<div class="form-group">
					<label for="ETPR_NO_EDIT"><i class='fa fa-circle-o text-aqua'></i> 사업자등록번호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="ETPR_NO_EDIT" data-inputmask="'mask': '999-99-99999'" placeholder="" name="ETPR_NO" value="">
					<label for="JUMIN_NO" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 생년월일</label>
					<input type="text" class="form-control form-input-md" id="JUMIN_NO_EDIT" placeholder="" name="JUMIN_NO" value="">
				</div>
				<div class="form-group">
					<label for="FAX"><i class='fa fa-circle-o text-aqua'></i> H/P</label>
					<input type="text" class="form-control form-input-md" id="FAX_EDIT" placeholder="" name="FAX" value="">
					<label for="UPTE" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  업태</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="UPTE_EDIT" placeholder="" name="UPTE" value="">
				</div>
				<div class="form-group">
					<label for="PHONE_NO"><i class='fa fa-circle-o text-aqua'></i>  전화번호</label>
					<input type="text" class="form-control form-input-md" id="PHONE_NO_EDIT" placeholder="" name="PHONE_NO" value="">
					
					<label for="UPJONG" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  종목</label>
					<input type="text" class="form-control form-input-md" id="UPJONG_EDIT" placeholder="" name="UPJONG" value="">
				</div>
				<div class="form-group">
					<label for="AREA_CD"><i class='fa fa-circle-o text-red'></i>  지역</label>
					<input type="text" class="form-control form-input-sm cis-lang-ko" id="AREA_CD_EDIT" placeholder="" name="AREA_CD" value="" readonly="readonly"/>
					<button type="button" name="btnSearchAreaCd_Edit" id="btnSearchAreaCd_Edit" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md" id="AREA_NM_EDIT" placeholder="" name="AREA_NM" value="" readonly="readonly"/>
				</div>
				<!-- 지역 검색 -->
				<div class="form-group SearchAreaCdEdit">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 지역코드 조회</h5>					
						<label for="textSearchAreaCdEdit"><i class='fa fa-circle-o text-aqua'></i>  지역명</label>
						<input type="text" class="" name="textSearchAreaCdEdit" id="textSearchAreaCdEdit" />
						
						<button type="button" class="btn btn-danger pull-right btn-AreaCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>				
						<div class="box-body">
							<table id="tableAreaCdEditList" class="table table-bordered table-hover" summary="게시물 목록">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th class="name">지역코드</th>
										<th class="name">지역명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="POST_NO"><i class='fa fa-circle-o text-aqua'></i>  우편번호</label>
					<input type="text" class="form-control form-input-md" id="POST_NO_EDIT" placeholder="" name="POST_NO" value="" />
					<button type="button" name="searchPisMk" id="btnSearchPisMk" class="btn btn-info" onclick="sample6_execDaumPostcode('edit')" value="우편번호 찾기">우</button>

					<label for="EMAIL_EDIT" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 이메일주소 </label>
					<input type="text" class="form-control form-input-sm cis-lang-en" id="EMAIL_EDIT" placeholder="" name="EMAIL" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR1"><i class='fa fa-circle-o text-aqua'></i>  주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR1_EDIT" placeholder="" name="ADDR1" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR2"><i class='fa fa-circle-o text-aqua'></i>  상세주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR2_EDIT" placeholder="" name="ADDR2" value="">
				</div>
				<div class="form-group">
					<label for="REMARK"><i class='fa fa-circle-o text-aqua'></i>  비고</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="REMARK_EDIT" placeholder="" name="REMARK" value="">
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success pull-right" id="btnUpdateCustCdOK">
					<span class="glyphicon glyphicon-off"></span> 등록
				</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
@stop
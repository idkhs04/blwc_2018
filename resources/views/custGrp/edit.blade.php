@extends('blwc_new.resources.views.layouts.main')

@section('title','거래처 그룹')

@section('content')

<div>
	<!-- board_search-->	
	<section >
		@include('blwc_new.resources.views.include.search_header', ['cust' => $cust])
	</section>
	<!-- List_boardType01 -->
	<section >
		<div class="col-sm-12">
			<div class="box box-info">
	            <div class="box-header with-border">
					<h3 class="box-title">@yield('title') 등록</h3>
	            </div>

				@if (count($errors) > 0)

					<!-- Form Error List -->
						<div class="alert alert-danger">
							<strong>Whoops! Something went wrong!</strong>

							<br><br>

							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
				@endif

			    <!-- /.box-header -->
	            <!-- form start -->
				{{  Form::open(['url'=>'/cust/'.$cust."/". $mode = Request::segment(3) == "Edit" ? "Update": "Insert", 'method' => 'POST', 'enctype' => "multipart/form-data", 'class'=>'form-horizontal', 'id'=>"frmEdit" ]) }}
				<input type="hidden" name="mode" value="{{ Request::segment(3)}}" />
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<input type="hidden" name="error" value="{{ count($errors)}}" />

					<div class="box-body">
	                	<div class="form-group">
	                  		<label for="CUST_GRP_CD" class="col-sm-2 control-label">거래처 그룹코드</label>
	                  		<div class="col-sm-2">
	                    		<input type="text" class="form-control" id="CUST_GRP_CD" placeholder="" name="CUST_GRP_CD" value="{{$model->CUST_GRP_CD}}" />
								@if ($errors->has('CUST_GRP_CD')) <p class="help-block">{{ $errors->first('CUST_GRP_CD') }}</p> @endif
	                  		</div>
	                	</div>
						<div class="form-group">
							<label for="CUST_GRP_NM" class="col-sm-2 control-label">거래처 그룹명</label>
							<div class="col-sm-5">
						 		<input type="text" class="form-control" id="CUST_GRP_NM" placeholder="" name="CUST_GRP_NM" value="{{$model->CUST_GRP_NM}}">
								@if ($errors->has('CUST_GRP_NM')) <p class="help-block">{{ $errors->first('CUST_GRP_NM') }}</p> @endif
							</div>
						</div>
					</div>
	              <!-- /.box-body -->
					<div class="box-footer text-center">
						<a class="btn btn-default" href="/cust/cust_grp_info/View/{{$model->CUST_GRP_CD}}/page={{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" title="취소">취소</a>
						<button type="submit" class="btn btn-info center" alt="저장">저장</button>
					</div>
	              <!-- /.box-footer -->
				{{ Form::close() }}
			</div>
		</div>

	</section>
	
</div>
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function(){

		var error	= $("input[name='error']").val();
		var mode	= $("input[name='mode']").val();

		if( mode == "Edit"){
			$("#CUST_GRP_CD").prop("readonly", true);
		}

	});

</script>
@stop
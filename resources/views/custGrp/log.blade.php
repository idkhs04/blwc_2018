@extends('layouts.main')

@section('title','거래처 그룹 로그')

@section('content')

<div>
	<style>
		input.cis-text {
			height: 34px;
			margin: 0px;
			vertical-align: top;
			padding-top: 0;
			margin-top: 0px;
		}
		.btn-info{ margin-top:4px;}
		.btnSearch{ margin-top:0;}
		#tblList_filter{ display:none;}
	</style>

	@include('include.search_header', ['cust' => $cust])

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">

				<table id="tblList" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="게시물 목록">
					<caption>거래처 그룹관리</caption>
					<colgroup>
						<col class="colCheck" />
						<col />
						<col />	
					</colgroup>
					<thead>
						<tr>
							<th class="name" width="20px">순번</th>  
							<th class="name">작업구분</th>  
							<th class="subject">거래체 부호</th>
							<th class="name">거래처 그룹명</th>  
							<th class="name">입력일시</th>  
							<th class="name">수정일시</th>  
							<th class="name">삭제일시</th>  
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(function () {
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: "/cust/custGrp/listLog",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				
				{ data: 'IDX', name: 'IDX' , className: "dt-body-center" },
				{ data: 'TYPE_NM', name: 'TYPE_NM' , className: "dt-body-center" },
				{ data: 'CUST_GRP_CD', name: 'CUST_GRP_CD' , className: "dt-body-center" },
				{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM' },
				{ data: 'WRITE_DT', name: 'WRITE_DT' , className: "dt-body-center" },
				{ data: 'UPDATE_DT', name: 'UPDATE_DT' , className: "dt-body-center" },
				{ data: 'DELETE_DT', name: 'DELETE_DT' , className: "dt-body-center" },
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

	});

</script>

@stop
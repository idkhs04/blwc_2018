@extends('blwc_new.resources.views.layouts.main')

@section('title','거래처 그룹')

@section('content')

<div>
	<!-- board_search-->	
	<section >
		@include('blwc_new.resources.views.include.search_header')
	</section>
	<!-- List_boardType01 -->
	<section >

		<div class="col-sm-12">
			<div class="box box-info">
	            <div class="box-header with-border">
					<h3 class="box-title">@yield('title') 등록</h3>
	            </div>
	            <!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label  class="col-sm-4 control-label">거래처 그룹코드</label>
						<div class="col-sm-2">
							{{$model->CUST_GRP_CD}}
						</div>
						<label class="col-sm-4 control-label">거래처 그룹명</label>
						<div class="col-sm-2">
							{{$model->CUST_GRP_NM}}
						</div>
					</div>
				</div>
			  <!-- /.box-body -->
				<div class="box-footer text-center">
					<a class="btn btn-default" id="btnCancel" href="/cust/cust_grp_info/List?page={{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" title="취소">취소</a>
					<a class="btn btn-info center" href="/cust/{{ $cust }}/Edit/{{$model->CUST_GRP_CD}}/{{ app('request')->input('page') }} " title="수정">수정</a>
				</div>
			  <!-- /.box-footer -->
			</div>
		</div>

	</section>
	
</div>

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function(){
		$("#btnUpdate").click(function(){
			$("#btnCancel").trigger("click");
		})

	});
</script>
@stop
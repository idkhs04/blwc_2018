@extends('layouts.main')
@section('class','코드관리')
@section('title','거래처그룹')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnUpdateCust" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDeleteCust" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnAddCust" ><i class="fa fa-plus-circle"></i> 추가</button>
				</div>
			</div>
			<!--
			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="CUST_GRP_CD">거래처그룹코드</option>
					<option value="CUST_GRP_NM">거래처그룹명</option>
				</select>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			-->

		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="거래처그룹 목록" width="100%">
				<caption>거래처 그룹관리</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th width="20px">거래체 부호</th>
						<th width="75px">거래처 그룹명</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		// 거래처추가 버튼
		$("#btnAddCust").click(function(){ $('#modal_custGrp').modal({show:true}); });
		// 거래처추가 닫기
		$('#modal_custGrp').on('hidden.bs.modal', CbSetInitModalClose);
		// 거래처추가 저장 버튼
		$("#btnInsertCustGrpOK").click(function(){
			$.getJSON('/cust/cust_grp_info/Insert', {
				_token		: '{{ csrf_token() }}'
				, CUST_GRP_CD	: $("#CUST_GRP_CD").val()
				, CUST_GRP_NM	: $("#CUST_GRP_NM").val()
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$('#modal_custGrp').modal("hide");
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_custGrp .edit_alert');
				info.hide().find('ul').empty();

				if( error.result == "DUPLICATION"){
					info.find('ul').append('<li>' + error.message + '</li>');
				}else{
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		// 거래처수정 닫기
		$('#modal_custGrpEdit').on('hidden.bs.modal', CbSetInitModalClose);
		// 거래처 그룹 수정 모달
		$("#btnUpdateCust").click(function(){

			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { arr_code.push($(this).val()); }
			});
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 거래처그룹을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정할 거래처그룹은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				var page = $("input[name='page']").val()
				$.getJSON("/cust/cust_grp_info/Edit/" + arr_code[0] + "/page=" + page+"", {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(data){
					$("#CUSTEDIT_GRP_CD").val(data.CUST_GRP_CD);
					$("#CUSTEDIT_GRP_NM").val(data.CUST_GRP_NM);
					$('#modal_custGrpEdit').modal({show:true});
				});
			}
		});

		// 거래처수정 저장 클릭
		$("#btnUpdateCustGrpOK").click(function(){
			$.getJSON('/cust/cust_grp_info/Update', {
				_token		: '{{ csrf_token() }}'
				, CUST_GRP_CD	: $("#CUSTEDIT_GRP_CD").val()
				, CUST_GRP_NM	: $("#CUSTEDIT_GRP_NM").val()
			}).success(function(xhr){
				$('#modal_custGrpEdit').modal("hide");
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_custGrpEdit .edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();

			});
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/cust/custGrp/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					data:   "CUST_GRP_CD",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'CUST_GRP_CD', name: 'CUST_GRP_CD' , className: "dt-body-center"},
				{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM' },
				{
					"className":      '',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});
		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});


		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#btnDeleteCust').click( function () {
			var arr_code = [];
				$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
					if ($(this).is(":checked")) {
						arr_code.push($(this).val());
					}
				});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 거래처그룹을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 거래처그룹은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				if(confirm("선택된 거래처그룹을 삭제하시겠습니까?")){

					$.getJSON("/cust/cust_grp_info/Delete", {
						cd : arr_code ,
						_token : '{{ csrf_token() }}'
					}, function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							for(var code in data.code){
								oTable.draw();
							}
						}else if(data.result == "failed"){

							$(".content > .alert").remove();
							$(".content").prepend( getAlert('warning', '경고', data.message) ).show();
						}
					}, function(xhr){
						console.log(xhr)
					});
				}
			}
		});
	});

</script>

<!-- 거래처 그룹 추가-->
<div class="modal fade" id="modal_custGrp" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 그룹 등록</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="CUST_GRP_CD"><i class='fa fa-check-circle text-red'></i> 거래처 그룹코드</label>
					<input type="text" class="form-control cis-lang-en" id="CUST_GRP_CD" placeholder="" name="CUST_GRP_CD" value="" />
				</div>
				<div class="form-group">
					<label for="modal_custGrp_no"><i class='fa fa-check-circle text-red'></i>  거래처 그룹명</label>
					<input type="text" class="form-control cis-lang-ko" id="CUST_GRP_NM" placeholder="" name="CUST_GRP_NM" value="">
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
				<button type="submit" class="btn btn-success" id="btnInsertCustGrpOK">
					<span class="glyphicon glyphicon-off"></span> 추가
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 거래처 수정-->
<div class="modal fade" id="modal_custGrpEdit" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 그룹 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="CUST_GRP_CD"><i class='fa fa-check-circle text-red'></i> 거래처 그룹코드</label>
					<input type="text" class="form-control cis-lang-en" id="CUSTEDIT_GRP_CD" placeholder="" name="CUSTEDIT_GRP_CD" value="" readonly="readonly"/>
				</div>
				<div class="form-group">
					<label for="modal_custGrp_no"><i class='fa fa-check-circle text-red'></i> 거래처 그룹명</label>
					<input type="text" class="form-control cis-lang-ko" id="CUSTEDIT_GRP_NM" placeholder="" name="CUSTEDIT_GRP_NM" value="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="submit" class="btn btn-success" id="btnUpdateCustGrpOK">
					<span class="glyphicon glyphicon-off"></span> 수정
				</button>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.main')
@section('class','코드관리')
@section('title','비용계정')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px; vertical-align:middle;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnUpdateAccountCd" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDeleteAccountCd" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnAddAccountCd" ><i class="fa fa-plus-circle"></i> 추가</button>
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="ACCOUNT_MK">계정부호</option>
					<option value="ACCOUNT_NM">계정명</option>
				</select>
			</div>

			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
					
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="비용계정 조회" width="100%">
				<caption>비용계정</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th class="subject" width="80px">계정그룹명</th>
						<th class="subject" width="30px">계정코드</th>
						<th class="subject" width="80px">계정명</th>
						<th width="50px">차대구분</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		// 비용계정추가 버튼
		$("#btnAddAccountCd").click(function(){ 
			$.getJSON("/accountcd/cust_grp_info/getAccountGrp", {
				_token : '{{ csrf_token() }}'
			},function(data){
				$("#ACCOUNT_GRP_CD").empty();
				$.each(data, function(i,item){
					for(var j=0 ; j < item.length ; j++ ){
						$("#ACCOUNT_GRP_CD").append("<option value="+ item[j].ACCOUNT_GRP_CD +">"+ item[j].ACCOUNT_GRP_NM +"</option>");
					}
				});
			});
			$('#modal_AccountCD').modal({show:true});
			setTimeout(function(){
				$("#ACCOUNT_MK").focus();
			},500);
		});

		//비용계정추가 닫기 버튼
		$("#modal_AccountCD .btn-danger").click(function(){ 
			$("#modal_AccountCD .form-group > input").val("");
			$("#modal_AccountCD ul").empty();
		});
		
		// 비용계정추가 저장버튼
		$("#btnInsertAccountCdOK").click(function(){
			$.getJSON('/accountcd/cust_grp_info/Insert', {
				_token		: '{{ csrf_token() }}'
				, ACCOUNT_MK	: $("#ACCOUNT_MK").val()
				, ACCOUNT_NM	: $("#ACCOUNT_NM").val()
				, DE_CR_DIV	: $("input[name='DE_CR_DIV']:checked").val()
				, ACCOUNT_GRP_CD	: $("#ACCOUNT_GRP_CD").val()
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$('#modal_AccountCD').modal("hide");
				$("#modal_AccountCD .form-group > input").val("");
				$("#modal_AccountCD ul").empty();
				oTable.draw();
				getSumQty();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_AccountCD .edit_alert');
				info.hide().find('ul').empty();

				if(error.reason == "PrimaryKey"){
					info.find('ul').append('<li>' + error.message + '</li>');
				}else{
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		// 비용계정 수정모달
		$("#btnUpdateAccountCd").click(function(){

			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 비용계정을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정할 비용계정은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				$.getJSON("/accountcd/cust_grp_info/getAccountGrp", {
					_token : '{{ csrf_token() }}'
				},function(data){
					$("#ACCOUNT_GRP_CD_EDIT").empty();
					$.each(data, function(i,item){
						for(var j=0 ; j < item.length ; j++ ){
							$("#ACCOUNT_GRP_CD_EDIT").append("<option value="+ item[j].ACCOUNT_GRP_CD +">"+ item[j].ACCOUNT_GRP_NM +"</option>");
						}
					});
				});

				var page = $("input[name='page']").val()
				setTimeout(function(){
					$.getJSON("/accountcd/cust_grp_info/Edit/" + arr_code[0] + "/page=" + page+"", {
						_token : '{{ csrf_token() }}'
					},function(data){
						$("#ACCOUNT_MK_EDIT").val(data.ACCOUNT_MK);
						$("#ACCOUNT_NM_EDIT").val(data.ACCOUNT_NM);
						$("#ACCOUNT_GRP_CD_EDIT").children("option").each(function(i, d){
							
							if($(d).val() == data.ACCOUNT_GRP_CD){
								$(d).attr("selected",true);
							}else{
								$(d).attr("selected",false);
							}
						});

						$("input[name='DE_CR_DIV_EDIT']").each(function(i, d){
							if( data.DE_CR_DIV == $(d).val() ){
								$(d).prop("checked",true);
							}else{
								$(d).prop("checked",false);
							}
						});
						$('#modal_AccountCdEdit').modal({show:true});
						setTimeout(function(){
							$("#ACCOUNT_NM_EDIT").focus();
						},500);

					});
				},500);
			}
		});
		
		// 비용계정 수정 
		$("#btnUpdateAccountCdOK").click(function(){

			$.getJSON('/accountcd/cust_grp_info/Update', {
				_token		: '{{ csrf_token() }}'
				, ACCOUNT_MK		: $("#ACCOUNT_MK_EDIT").val()
				, ACCOUNT_NM		: $("#ACCOUNT_NM_EDIT").val()
				, ACCOUNT_GRP_CD	: $("#ACCOUNT_GRP_CD_EDIT").val()
				, DE_CR_DIV			: $("input[name='DE_CR_DIV_EDIT']:checked").val()
			}).success(function(xhr){
				$('#modal_AccountCdEdit').modal("hide");
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText); 
				var info = $('#modal_AccountCdEdit .edit_alert');
				info.hide().find('ul').empty();

				if(error.reason == "PrimaryKey"){
					info.find('ul').append('<li>' + error.message + '</li>');
				}else{
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/accountcd/custcd/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					data:   "ACCOUNT_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'ACCOUNT_GRP_NM', name: 'ACCOUNT_GRP_NM' },
				{ data: 'ACCOUNT_MK', name: 'ACCOUNT_MK', className: "dt-body-center" },
				{ data: 'ACCOUNT_NM', name: 'ACCOUNT_NM' },
				{
					data:   "DE_CR_DIV",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data == "1"){
								return "<span class='red'>입금</span>";
							}else{
								return "<span class='blue'>출금</span>";
							}
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					"className":      '',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 삭제
		// 현재 2개 이상 삭제시 콜백이 죽는 관계로 화면상에서 table row가 안지워짐 => 향후 수정
		$('#btnDeleteAccountCd').click( function () {
			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 비용계정을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 비용계정을 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				if(confirm("선택된 비용계정을 삭제하시겠습니까?")){

					$.getJSON("/accountcd/cust_grp_info/Delete", {
						cd : arr_code[0] ,
						_token : '{{ csrf_token() }}'
					}).success(function (xhr) {
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							for(var code in data.code){
								oTable.row('.data-code').remove().draw(false);
							}
						}
					}).error(function(xhr){
						$(".content > .alert").remove();
						$(".content").prepend( getAlert('danger', '경고', "선택한 비용계정코드는 금전출납부에서 사용중이므로 삭제 할 수 없습니다.") ).show();
						return;
					});
				}
			}
		});
	});

</script>
<!-- 비용계정 추가-->
<div class="modal fade" id="modal_AccountCD" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 비용계정 등록</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="ACCOUNT_MK"><i class='fa fa-check-circle text-red'></i> 계정부호 </label>
					<input type="text" class="form-control cis-lang-en" id="ACCOUNT_MK" placeholder="" name="ACCOUNT_MK" value="" />
				</div>
				<div class="form-group">
					<label for="ACCOUNT_GRP_CD"><i class='fa fa-check-circle text-red'></i> 계정그룹 </label>
					<select class="form-control form-input-md" name="ACCOUNT_GRP_CD" id="ACCOUNT_GRP_CD"></select>
				</div>
				<div class="form-group">
					<label for="ACCOUNT_NM"><i class='fa fa-check-circle text-red'></i> 계정명 </label>
					<input type="text" class="form-control cis-lang-ko" id="ACCOUNT_NM" placeholder="" name="ACCOUNT_NM" value="">
				</div>
				<div class="form-group">
					<label for="DE_CR_DIV"><i class='fa fa-check-circle text-red'></i> 차대구분 </label>
					<input type="radio" class="" id="" name="DE_CR_DIV" value="1" checked="checked">입금
					<input type="radio" class="" id="" name="DE_CR_DIV" value="0">출금
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="submit" class="btn btn-success" id="btnInsertAccountCdOK">
					<span class="glyphicon glyphicon-off"></span> 추가
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 비용계정 수정-->
<div class="modal fade" id="modal_AccountCdEdit" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 비용계정 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="ACCOUNT_MK_EDIT"><i class='fa fa-check-circle text-red'></i> 계정부호</label>
					<input type="text" class="form-control cis-lang-en" id="ACCOUNT_MK_EDIT" placeholder="" name="ACCOUNT_MK_EDIT" value="" readonly="readonly"/>
				</div>
				<div class="form-group">
					<label for="ACCOUNT_GRP_CD_EDIT"><i class='fa fa-check-circle text-red'></i> 계정그룹</label>
					<select class="form-control form-input-md" name="ACCOUNT_GRP_CD_EDIT" id="ACCOUNT_GRP_CD_EDIT"></select>
				</div>
				<div class="form-group">
					<label for="ACCOUNT_NM_EDIT"><i class='fa fa-check-circle text-red'></i> 계정명</label>
					<input type="text" class="form-control cis-lang-ko" id="ACCOUNT_NM_EDIT" placeholder="" name="ACCOUNT_NM_EDIT" value="">
				</div>
				<div class="form-group">
					<label for="DE_CR_DIV_EDIT"><i class='fa fa-check-circle text-red'></i> 차대구분</label>
					<input type="radio" class="" id="" name="DE_CR_DIV_EDIT" value="1">입금
					<input type="radio" class="" id="" name="DE_CR_DIV_EDIT" value="0">출금
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
				<button type="submit" class="btn btn-success pull-right" id="btnUpdateAccountCdOK">
					<span class="glyphicon glyphicon-off"></span> 수정
				</button>
			</div>
		</div>
	</div>
</div>
@stop
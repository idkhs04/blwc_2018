@extends('layouts.main')

@section('title','오류/개선 목록')

@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	#modal_uncl_insert label{
		display:inline-block;
		width:85px;
		clear:both;
	}

	#modal_uncl_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	.btn-info{ margin-top:0px;}

	
</style>
<!--
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-3 col-xs-12">
				<select class="form-control" name="srtCondition" id="srtCondition">
				</select>
			</div>
			<div class="col-md-5 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
-->
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="edit_alert"><ul></ul></div>
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="오류신고 정보조회" width="100%">
				<caption>오류신고 조회</caption>
				<thead>
					<tr>
						<th class="name">확인</th>
						<th class="check" width="15px">순번</th>
						<th class="name">구분</th>
						<th class="name">이슈명</th>
						<th class="name">업체명</th>
						<th class="name">입력자</th>
						<th class="name">날짜</th>
						<th class="">처리</th>
						
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		
		var start = moment().subtract(1500, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');

		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
			$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
		}
			
		// 미수금정보 작성일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10일', '11월', '12월'],
				}
		});
		
		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			opens: "right",
			drops: "down",
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			ranges: {
				'오늘': [moment(), moment()],
				'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
				'이번 달': [moment().startOf('month'), moment().endOf('month')],
				'지난 달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
				//daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				//monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10일', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);
	
	
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			bInfo:false,
			//iDisplayLength: 100,		// 기본 100개 조회 설정
			ajax: {
				url: "/config/getFeedBack",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.type			= "DT";
					//d.start_date	= $("input[name='start_date']").val();
					//d.end_date		= $("input[name='end_date']").val();
				}
			},
			order: [[ 1, 'desc' ]],
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: "FEEDIDX",	name: 'FEEDIDX', className: "dt-body-center"},
				{ data: "CMPLT_NM",	name: 'CMPLT_NM', className: "dt-body-center"},
				{ data: 'ISSUE', name: 'ISSUE' },
				{ data: 'FRNM',	 name: 'FRNM' },
				{ data: 'NAME', name : 'NAME'},
				{ data: 'INPUT_DATE', name : 'INPUT_DATE', className: "dt-body-center"},
				{ 
					data: "FEEDIDX",
					render: function ( data, type, row ) {
						@role('sysAdmin')
						if ( type === 'display' ) {
							if( row.IS_CMPLT == "1"){
								return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span>완료 [완료취소]</button>";
							}else{
								return "<button class='btn btn-warning btn-block' ><span class='glyphicon glyphicon-on'></span>대기 [완료하기]</button>";
							}
						}
						@endrole
						@role('AuthUser')
							if( row.IS_CMPLT == "1"){
								return "완료";
							}else{
								return "대기";
							}
						@endrole
					},
					className: "dt-body-center"
				},
			],
		
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			
			var row = oTable.row( tr );
			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var d = row.data();
				$.getJSON('/config/getFeedBackDetail', {
					_token			: '{{ csrf_token() }}',
					FEEDIDX			: d.FEEDIDX,

				}).success(function(xhr){
					
					var data = jQuery.parseJSON(JSON.stringify(xhr.result));

					$(".detailViewTbl tr:eq(0) > td").html(data.ISSUE);
					$(".detailViewTbl tr:eq(1) > td").html("<img src='" + data.IMAGE + "' width='100%' />");
					
					row.child($("div.detailView").html()).show();
					tr.addClass('shown');

				}).error(function(xhr){
					alert('error');
				});
			}
		});



		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 상세 버튼 클릭
		$('#tblList tbody').on( 'click', 'button', function () {

			var row		= oTable.row( $(this).parents('tr') ).data();
			var chCplte	= $(this).hasClass("btn-success") ? 0 : 1;

			//console.log($(this).hasClass("btn-success"));

			$.blockUI({ 
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() { 
					//alert('Page is now blocked; fadeIn complete'); 
					
					$.getJSON('/config/setComplete', {
						_token			: '{{ csrf_token() }}',
						FEEDIDX			: row.FEEDIDX,
						IS_CMPLT		: chCplte

					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						if (data.result == "success") {
							oTable.draw() ;
						}
						$.unblockUI();

					}).fail(function(xhr){
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						info.slideDown();
						$.unblockUI();
						
					});
				} 
			});  
		});
		
		
	
	});

</script>
<div class="detailView" style="display:none;">
	<table class="detailViewTbl" style="width:80%;float:left;">
		<tr>
			<th>오류내용</th>
			<td class='bInputDate'></td>
		</tr>
		<tr>
			<th>오류화면</th>
			<td class='bOutputDate'></td>
		</tr>
	</table>
</div>
@stop
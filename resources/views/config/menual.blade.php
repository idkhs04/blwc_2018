@extends('layouts.main')
@section('title','업무메뉴얼')
@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	#modal_uncl_insert label{
		display:inline-block;
		width:85px;
		clear:both;
	}

	#modal_uncl_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	.btn-info{ margin-top:0px;}

	
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<iframe style="width:100%;height:680px;"  src="/menual/blwc_menual_v0.2.pdf">
			</iframe>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>

@stop
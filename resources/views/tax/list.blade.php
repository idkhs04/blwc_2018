@extends('layouts.main')
@section('class','매출관리')
@section('title','년단위 계산서 관리')
@section('content')
<script src='/static/plugins/jQuery/jquery-2.2.3.min.js'></script>
<script src='/static/js/jquery/jquery.js'></script>
<script src='/static/js/jquery-ui/jquery-ui.js'></script>
<script src='/static/js/bootstrap/bootstrap.js'></script>
<script src='/static/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='/static/plugins/datatables/dataTables.bootstrap.min.js'></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	#modal_uncl_insert label{
		display:inline-block;
		width:85px;
		clear:both;
	}

	#modal_uncl_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	.btn-info{
		vertical-align:baseline;
		margin-top:0;
	}

	.waring-message { padding-top:4px;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	.dataTables_filter{ display:none;}
	.ui-datepicker-calendar {
		display: none;
	}
	.ui-datepicker-month{ display:none;}

	.dataTable .dataTable thead th { background-color:khaki;}
	.dataTable .dataTable tbody td { padding-right:2px; text-align:right; vertical-align:middle; width:8%;}
	.dataTable .dataTable tbody tr > td:nth-child(1) { background-color:khaki;}
	
	.dataTable table.dataTable tbody .btn { padding:0px 5px;}

	input.numeric { width:65px;text-align:right;padding-right:2px;}
	.control-label.waring-message {font-size:12px;}

	#modal_tax input.numeric { width:100%;}

	.dvAlert.alert-info{ padding:2px; text-align:center;}
	#tblList .btnInTaxData { float:left; padding:0px 0px;}

	th.dt-body-right {text-align:right;}
</style>

<div class='col-md-12'>

	<div class='box box-primary'>
		<div class='box-body'>
			<input type='hidden' name='page' value='{{ app('request')->input('page') == '' ? 1 : app('request')->input('page') }}' />
			
			<div class='col-md-2 col-xs-6'>
				<input type="radio" name="chkSaleStock" value="1" checked="checked" /> 매출
				<input type="radio" name="chkSaleStock" value="0" /> 매입
			</div>
			<div class="col-md-2 col-xs-6">
				<div class="btn-group">
					<label>데이터 유/무 : </label> 
					<input type="radio" name="chkZero" value="1"  /> 전체
					<input type="radio" name="chkZero" value="0" checked="checked" /> 유
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 2.5px 10px; border: 1px solid #ccc; width: 100%">
					<label>판매일</label> 
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<!--
			<div class="col-md-5 col-xs-12">
				<div id="reportrange1" class="pull-left" style="background: #fff; cursor: pointer; padding: 0px 10px; border: 0px solid #ccc; width: 120px">
					<label>년도 </label> 
					<span> </span>
					<input id="reportrange" type="text" readonly="readonly" style="width:60px;" />

					<input type='hidden' name="txtMonth" />
					<input type='hidden' name="txtYear" />
					<input type='hidden' name="txtday" />
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />

					<input type='hidden' name="start_date_year" />
					<input type='hidden' name="end_date_year" />
					
				</div>
				<div class='input-group'>
					<button type="button" name="searchAll" id="search-All" class="btn btn-info">전체</button>
					<button type="button" name="searchAll" id="search-1half" class="btn btn-info">2016상반기</button>
					<button type="button" name="searchAll" id="search-2half" class="btn btn-info">2016하반기</button>
				</div>
				
			</div>
			-->
			<div class='col-md-2 col-xs-6'>
				<div class='input-group'>
					<input type='text' name='textSearch' class='form-control'  placeholder='검색어 입력' value='{{ Request::Input('textSearch') }}'>
					<span class='input-group-btn'>
						<button type='button' name='search' id='search-btn' class='btn btn-info'><i class='fa fa-search'></i></button>
					</span>
				</div>
			</div>
			<div class='col-md-2 col-xs-6'>
				<select class='form-control' name='srtCondition' id='srtCondition'>
				</select>
			</div>
		</div>
	</div>
</div>

<div class='col-md-12'>
	<div class='box box-primary'>
		<div class='box-body'>
			<table id='tblList' class='table table-bordered table-hover display nowrap' summary='미수금 정보조회' width='100%'>
				<caption>계산서 정보관리 조회</caption>
				<thead>
					<tr>
						<th class='check' width='15px'></th>
						<th width='30px'>상세</th>
						<th class='name' width='70px'>업체그룹</th>
						<th class='name' width='70px'>업체명</th>
						<th class='name' width='70px'>총매출금</th>
						<th class='name' width='70px'>사업자번호</th>
						<th class='name' width='auto'></th>
						
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th class='dt-body-right'>총합계 : </th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src='/static/js/bootstrap/bootstrap.js'></script>
<script>
	var year, month, day;
	var sCustMK, sTaxAmt, tax_amt2;
	$(function () {
		
		var thisyear = moment().startOf('year').format("YYYY");
		var Gubun	 = "ALL";
		
		var start = moment().startOf('month');
		var end = moment();

		/*
		year = moment().format("YYYY");
		month = moment().startOf('month').format("MM");
		day = moment().startOf('day').format("DD");
		
		console.log(year, month, day);
		$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
		$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
		
		$("#reportrange").val(start.format('YYYY'));
		
		var start_year = moment().startOf('year').format('YYYY-MM-DD');
		var end_year = moment().endOf('year').format('YYYY-MM-DD');

		$("input[name='start_date_year']").val( start_year);
		$("input[name='end_date_year']").val( end_year);
		

		$("#search-All").html(thisyear + "전체");
		$("#search-1half").html(thisyear + "상반기");
		$("#search-2half").html(thisyear + "하반기");
		$("#tblList thead th:eq(4)").html( thisyear + " 총매출금");
		*/
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}
		
		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			
			cancelClass: "btn-default",			
			/*
			ranges: {
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
			},
			*/
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

			
		// 미수금정보 작성일자
		// 거래일 검색
		// 거래일 검색
		/*
		$("#reportrange").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'yy',
			prevText: '이전 달',
			nextText: '다음 달',
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNames: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			showMonthAfterYear: true,
			yearSuffix: ' 년',
			closeText : "확인",
			currentText: '이번달',
			locale: {

					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
					monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
			}).focus(function() {
				var thisCalendar = $(this);
				$('.ui-datepicker-calendar').detach();
				$('.ui-datepicker-close').click(function() {

					
					month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					
					thisyear = year;

					thisCalendar.datepicker('setDate', new Date(year, month, 1));
					
					$("input[name='start_date']").val( moment(year+'-01-01').format('YYYY-MM-DD')).trigger('change');
					$("input[name='end_date']").val( moment(year+'-12-31').format('YYYY-MM-DD')).trigger('change');
					$("input[name='start_date_year']").val( moment().year(year).startOf('year').format('YYYY-MM-DD')).trigger('change');
					$("input[name='end_date_year']").val( moment().year(year).endOf('year').format('YYYY-MM-DD')).trigger('change');
					
										
					$("#tblList thead th:eq(4)").html( thisyear + " 총매출금");
					$("#search-All").html(thisyear + "전체");
					$("#search-1half").html(thisyear + "상반기");
					$("#search-2half").html(thisyear + "하반기");

				});
			});

		*/

		$.getJSON('/sale/sale/getCustGrp', {
				_token	: '{{ csrf_token() }}'
			}, function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				var $srtCondition = $("select[name='srtCondition']");
				$srtCondition.empty();
				$srtCondition.append("<option value='ALL' >전체</option>");

				$.each(data, function(){
					$srtCondition.append("<option value='" + this.CUST_GRP_CD +"'>" + this.CUST_GRP_NM + "</option>");
				});
			}
		);

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: '/tax/tax/listTaxCustData',
				data:function(d){
					d.chkSaleStock	= $("input[name='chkSaleStock']:checked").val();
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.START_DATE	= $("input[name='start_date']").val();
					d.END_DATE		= $("input[name='end_date']").val();
					d.chkZero		= $("input[name='chkZero']:checked").val();
				}
			},
			order: [[ 3, 'ASC']],
			
			columns: [
				{
					data:   'CUST_MK',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='' >";
						}
						return data;
					},
					className: 'dt-body-center'
				},
				{
					'className':      'details-control dt-body-left',
					'orderable':      false,
					'data':           null,
					'defaultContent': ''
				},
				{ data: 'CUST_GRP_NM',	 name: 'CUST_GRP_NM' },
				{ data: 'FRNM', name : 'FRNM' },
				{ 
					data: 'SALE_SUM', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: 'dt-body-right'
				},
				{ 
					data:   "ETPR_NO",
					render: function ( data, type, row ) {
						if (data !== null && data.length == 10)
						{
							firstNo		= data.substr(0, 3);
							secondNo	= data.substr(3, 2);
							thirdNo		= data.substr(5, 5);
							
							return firstNo + "-" + secondNo + "-" + thirdNo
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ 
					data: null,
					orderable:false,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							//return " <button class='btn btn-info btnDetailGo' ><i class='fa fa-plus-circle'></i> 계산서 바로 가기 </button>";
							return '';
						}
						return '';
					},
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};

				SUM = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html( SUM.format()+"원").css({'text-align':'right'});

			},
			'searching': true,
			'paging': true,
			'autoWidth': true,
			'oLanguage': {
				'sLengthMenu': '조회수 _MENU_ ',
				'sLoadingRecords' : '조회 중...잠시만 기다려주세요',
				'sProcessing': '현재 조회 중입니다',
				'sEmptyTable': '조회된 데이터가 없습니다',
				'sZeroRecords': '조회된 데이터가 없습니다',
				'sInfo': '전체 _TOTAL_ 건 ( _START_ ~ _END_ )',
				'oPaginate': {
					'sFirst': '처음',
					'sLast': '끝',
					'sNext': '다음',
					'sPrevious': '이전'
				}
			}
		});

		$("input[name='textSearch']").on('keyup', function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date_year'], input[name='end_date_year'], input[name='start_date'], input[name='end_date']").on('change', function(){
			oTable.draw() ;
		});

		$("input[name='start_date']").on("change", function(){
			oTable.draw();
		});

		$("input[name='end_date']").on("change", function(){
			oTable.draw();
		});
		// 전체 클릭
		$("#search-All").on('click', function(){
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		// 데이터 유무 선택
		$("input[name='chkZero']").change(function(){
			oTable.draw() ;
		});

		$("input[name='chkSaleStock']").change(function(){
			if( $(this).val() == "1"){
				$("#tblList > thead tr > th:eq(4)").html("총매출금");
			}else{
				$("#tblList > thead tr > th:eq(4)").html("총매입금");
			}
			oTable.draw() ;
		});


		// 전체조회
		$("#search-All").click(function(){
			
			Gubun = "ALL"
			$("input[name='start_date']").val( moment(thisyear+"-01-01").format('YYYY-MM-DD'));
			$("input[name='end_date']").val( moment(thisyear+"-12-31").format('YYYY-MM-DD'));
			// 트리거 걸려있음..	

			$("input[name='start_date_year']").val( moment(thisyear+"-01-01").format('YYYY-MM-DD'));
			$("input[name='end_date_year']").val(  moment(thisyear+"-12-31").format('YYYY-MM-DD'));

			$("#tblList thead th:eq(4)").html( moment(thisyear).format("YYYY") + " 총매출금");

			oTable.draw() ;
		});

		// 상반기
		$("#search-1half").click(function(){
			
			Gubun = "1";
			$("input[name='start_date']").val( moment(thisyear+"-01-01").format('YYYY-MM-DD'));
			$("input[name='end_date']").val( moment(thisyear+"-06-30").format('YYYY-MM-DD'));

			$("input[name='start_date_year']").val( moment(thisyear+"-01-01").format('YYYY-MM-DD'));
			$("input[name='end_date_year']").val(  moment(thisyear+"-06-30").format('YYYY-MM-DD'));
			
			$("#tblList thead th:eq(4)").html(moment(thisyear).format("YYYY") + "상반기 총매출금");
			
			oTable.draw() ;
			// 트리거 걸려있음..	
		});

		// 하반기
		$("#search-2half").click(function(){
			
			Gubun = "2";
			$("input[name='start_date']").val(moment(thisyear+"-07-01").format('YYYY-MM-DD'));
			$("input[name='end_date']").val( moment(thisyear+"-12-31").format('YYYY-MM-DD'));

			$("input[name='start_date_year']").val( moment(thisyear+"-07-01").format('YYYY-MM-DD'));
			$("input[name='end_date_year']").val(  moment(thisyear+"-12-31").format('YYYY-MM-DD'));

			$("#tblList thead th:eq(4)").html( moment(thisyear).format("YYYY") + "하반기 총매출금");

			oTable.draw() ;
			
		});



		// 입고 버튼 클릭
		$("body").on("click", ".btnDetailGo", function(){
			
			var row		= oTable.row( $(this).parents('tr') ).data();
			var sCustMK	= row.CUST_MK;
			var saleBuy	= $("input[name='chkSaleStock']:checked").val();
			if( sCustMK != "" && sCustMK !== undefined ){	
				location.href="/tax/tax/elist/" + sCustMK + "/" + saleBuy;
			}
		});

		

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find('td > input[type=checkbox]').prop('checked', false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find('td > input[type=checkbox]').prop('checked', true);
			}
		});

		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			
			var row = oTable.row( tr );
			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var d = row.data();
				var listHtml = '';
			
				var list = new Array(2);
				var zeroList = {};

				for(var i=1; i < 14; i++){
					zeroList["T" + i + "M"] = 0;
				}

				$.getJSON('/tax/tax/getCustSaleDetailData', {
					_token		: '{{ csrf_token() }}'
					, chkSaleStock	: $("input[name='chkSaleStock']:checked").val()
					, CUST_MK	: d.CUST_MK
					, WRITE_DATE: $("input[name='start_date']").val()
					, END_DATE: $("input[name='end_date']").val()
					}).success(function(data){
						
						list[0] = data.TAX_SALE[0] === undefined ? zeroList : data.TAX_SALE[0];
						list[1] = data.TAX_SALE[1] === undefined ? zeroList : data.TAX_SALE[1];
						
						//console.log(list[0]);
						list[1].GUBUN = '계산서';
						
						if( $("input[name='chkSaleStock']:checked").val() == "1"){
							list[0].GUBUN = '매출';
						}else{
							list[0].GUBUN = '매입';
						}
						
						listHtml = "<table class='dataTable' width='90%' border='1' cellspacing='0' cellpadding='0'><thead><tr><th>구분</th>"

						var sloop = 1;
						var eloop = 12;

						if( Gubun == "1" ){
							sloop = 1;
							eloop = 6;
						}else if( Gubun == "2") {
							sloop = 7;
							eloop = 12;
						}

						for(var i=sloop; i<=eloop; i++){

							listHtml += "<th>" + i + " 월</th>";
						}
						listHtml += "<th> 합 계 </th>";
						listHtml += "</thead><tbody>";
					

					listHtml +=  
					'<tr class="sale">'+
						'<td width="60px">' + list[0].GUBUN + '</td>';

					if( Gubun == "1" || Gubun == "ALL"){

						listHtml += 
							'<td>'+ Number(list[0].T1M).format() + '</td>'+
							'<td>'+ Number(list[0].T2M).format()  +'</td>'+
							'<td>'+ Number(list[0].T3M).format()  +'</td>'+
							'<td>'+ Number(list[0].T4M).format()  +'</td>'+
							'<td>'+ Number(list[0].T5M).format()  +'</td>'+
							'<td>'+ Number(list[0].T6M).format()  +'</td>';
					}if( Gubun == "2" || Gubun == "ALL" ){

						listHtml += 
							'<td>'+ Number(list[0].T7M).format()  +'</td>'+
							'<td>'+ Number(list[0].T8M).format()  +'</td>'+
							'<td>'+ Number(list[0].T9M).format()  +'</td>'+
							'<td>'+ Number(list[0].T10M).format() +'</td>'+
							'<td>'+ Number(list[0].T11M).format() +'</td>'+
							'<td>'+ Number(list[0].T12M).format() +'</td>';
							
					}
					listHtml +=	'	<td>'+ Number(list[0].TAMT).format() + '</td>';
					listHtml +=  '</tr>';
						
					listHtml +=  "<tr class='tax' data-cust-mk='" + d.CUST_MK + "'>"+
									'<td width="60px" height="25px">' + list[1].GUBUN + '</td>';
					
					if( Gubun == "1" || Gubun == "ALL"){
						listHtml += 
							'<td>'+ setButton(1, list[1].T1M) + '</td>'+
							'<td>'+ setButton(2, list[1].T2M) + '</td>'+
							'<td>'+ setButton(3, list[1].T3M) + '</td>'+
							'<td>'+ setButton(4, list[1].T4M) + '</td>'+
							'<td>'+ setButton(5, list[1].T5M) + '</td>'+
							'<td>'+ setButton(6, list[1].T6M) + '</td>';
					}if( Gubun == "2" || Gubun == "ALL"){
						listHtml += 
							'<td>'+ setButton(7, list[1].T7M) + '</td>'+
							'<td>'+ setButton(8, list[1].T8M) + '</td>'+
							'<td>'+ setButton(9, list[1].T9M) + '</td>'+
							'<td>'+ setButton(10, list[1].T10M) + '</td>'+
							'<td>'+ setButton(11, list[1].T11M) + '</td>'+
							'<td>'+ setButton(12, list[1].T12M) + '</td>';
					}
					
					listHtml += '<td>'+ Number(list[1].TAMT).format() + '</td>';
					listHtml += '</tr>';
					listHtml += '</tbody></table>';
					row.child(listHtml).show();
					tr.addClass('shown');


				}).error(function(xhr){
					alert('error');
				});
			}
		} );

		function setButton(i, tax_amt){
			
			var month	= moment().startOf('month').format("MM");
			var btn		= "<button class='btnInTaxData btn btn-danger' data-month='" + month + "'><i class='fa fa-send'></i></button>"
			//console.log(tax_amt);
			if( i == month ){
				//return btn + " <input type='text' class='numeric' data-month='" + month + "' value='"+ Number(parseInt(tax_amt)).format() + "' readonly='readonly' />"
				return btn + " " + Number(parseInt(tax_amt)).format() ;
			}else{
				return Number(tax_amt).format();
			}
			
		}
		
		/** 세금계산서 발행 닫기 콜백(초기화) **/ 
		$('#modal_tax').on('hidden.bs.modal', function(){
			$(".modal input[type='text']").val("");
		});

		/** 세금계산서 발행 클릭 **/
		$('body').on( 'click', '.btnInTaxData', function () {
			
			// 거래처 코드
			var cust_mk		= $(this).parent('td').parent('tr').attr('data-cust-mk');

			tax_amt2		= removeCommas($(this).parent('td').text());
			var month		= $(this).attr('data-month');

			var saleBuy_amt	= removeCommas($(this).parent('td').parent('tr').parent('tbody').find("tr.sale td:eq(" + parseInt(month) + ")").text());


			var input_amt	 = parseFloat(saleBuy_amt) - parseFloat(tax_amt2);
			
			sCustMK	= cust_mk;
			sTaxAmt	= input_amt;
			
			
			$(".modal .gubun").text( $("input[name='chkSaleStock']:checked").val() == "1" ? "매출" : "매입");
			// 년-월
			$("#modal_tax_YM").val(moment().format('YYYY-MM'));
			// 매/출입금액
			$("#modal_tax_AMT").val(Number(saleBuy_amt).format());
			// 등록된 계산서 매출/입 금액
			$("#modal_tax_AMT_BEFORE").val(Number(tax_amt2).format());
			// 계산서 매출(매입)금액
			$("#modal_tax_input_amt").val(Number(input_amt).format());
			
			$("#modal_tax" ).modal("show");
			
		});

		$("#btnAddTax").click(function(){
			var saleBuy		= $("input[name='chkSaleStock']:checked").val();
			var input_user_amt	= removeCommas($("#modal_tax_input_amt").val());
			location.href="/tax/tax/elist/" + sCustMK + "/" + moment().format('YYYY-MM') + "/" + input_user_amt + "/" + saleBuy;
		});

		/*
		if( cust_mk !== null && cust_mk != 'null'){
			if( $.isNumeric(tax_amt)){
				// 계산서 등록

				$.getJSON('/tax/tax/setTaxSale', {
					_token			: '{{ csrf_token() }}',
					BUY_SALE_DIV	: $("input[name='chkSaleStock']:checked").val() == "1" ? "O" : "I", 
					WRITE_DATE		: moment().format('YYYY-MM-DD') ,
					GOODS			: '활어' ,
					SUPPLY_AMT		: tax_amt ,
					RECPT_CLAIM_DIV	: 1 ,
					REMARK			: '' ,
					CUST_MK			: cust_mk,
					MONTHS			: moment().format('MM') ,
					DAYS			: moment().format('DD'),

				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					console.log(data);

					if( data.result == "success"){
						alert('세금계서서 등록 완료: 거래처 아이디 : ' + cust_mk + ' 금액:' + tax_amt + ' 월' + month);		
						location.href="/tax/tax/elist/" + cust_mk + "/" + moment().format('YYYY-MM-DD') + "/" + data.SEQ;
					}

				}).error(function(xhr,status, response) {
					alert('세금계서서 등록 오류 : 거래처 아이디 : ' + cust_mk + ' 금액:' + tax_amt + ' 월' + month);		
				});
				
			}
		}
		*/
	});

</script>

<div class="modal fade" id="modal_tax" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> <span class="gubun"></span> 세금계산서 등록</h4>
				
			</div>
			<div class="modal-body" style="padding:10px 20px 5px 20px;">
				@if ( $corp_info->TAX_USE_YN == "Y")
				<div class='dvAlert alert alert-info'>전자 세금계산서 사용중</div>
				@endif
				<div class="form-group">
					<label for="modal_tax_YM"><i class='fa fa-check-circle text-red'></i> 년/월</label>
					<input type="text" class="form-control" id="modal_tax_YM" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_tax_AMT"><i class='fa fa-check-circle text-red'></i> 거래 총금액</label>
					<input type="text" class="form-control numeric" id="modal_tax_AMT" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_tax_AMT_BEFORE"><i class='fa fa-check-circle text-red'></i> 계산서 총금액</label>
					<input type="text" class="form-control numeric" id="modal_tax_AMT_BEFORE" readonly="readonly" />
				</div>

				<div class="form-group">
					<label for="modal_tax_input_amt"><i class='fa fa-check-circle text-red'></i> 발행예정 금액</label>
					<input type="text" class="form-control numeric" id="modal_tax_input_amt" />

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnAddTax">
					<span class="glyphicon glyphicon-off"></span> 등록 이동
				</button>
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 취소 
				</button>
			</div>
		</div>
	</div>
</div>
@stop


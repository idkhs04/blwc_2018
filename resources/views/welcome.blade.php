@extends('layouts.main')
@section('class','홈')
@section('title','환영합니다')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	ul.info { margin:0; padding:0 0 0 10px;}
</style>
@role('sysAdmin')
<div class="col-md-12">
	<!--
	<div class="col-md-6">
		<div id="chartContainer" style="height: 400px; width: 100%;"></div>
	</div>
	-->
	<!--결산표 -->
	<div class="col-md-12">
		<div class="box">
			<div id="chartContainer3" style="height: 500px; width: 100%;"></div>
		</div>
	</div>

	<!--매입매출현황 -->
	<div class="col-md-12">
		<div class="box">
			<div id="chartContainer2" style="height: 400px; width: 100%;"></div>
		</div>
	</div>

	<!--매입매출현황 -->
	<div class="col-md-12">
		<div class="box">
			<div id="chartContainer4" style="height: 1500px; width: 100%;"></div>
		</div>
	</div>
</div>
@endrole
<div class="col-md-12">
	<div class="col-md-12 col-md-offset">
		<div class="panel panel-default">
			<div class="panel-heading"> 
				<ul class="info">
					<li>AS접수시간 : 평일 09:00 ~ 18:00 / 주말 휴무 </li>
					<li>(주)이씨스 A/S번호 : 051-966-7000</li>
				</ul>
			</div>
			<div class="panel-body" id="step1">
				<img src="/image/main.jpg" width="100%" />
			</div>
		</div>
	</div>

</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/js/chart/canvasjs.min.js"></script>
<script type="text/javascript">
	
	$(function(){
		
		//$('.modal-info').modal({show:true}); 

		@role('sysAdmin')
		var dataPoints1 = new Array();

		// 2. Bar 형태 (일일판매)
		$.getJSON('/sale/sale/listData', {
			start_date	: moment('2017-01-08').format("YYYY-MM-DD"),
			end_date	: moment().format("YYYY-MM-DD"),
			_token : '{{ csrf_token() }}'
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			item = data.data;
		
			for(var i=0; i < item.length; i++){
				
				var tmpObj = new Object();

				tmpObj.label = item[i].PIS;
				tmpObj.y = parseInt(item[i].SUMQTY);
				
				dataPoints1.push(tmpObj);
			};

			var jsonData = JSON.stringify(dataPoints1) ;
			 
			//alert(jsonData) ;
			
			/*
			var chart1 = new CanvasJS.Chart("chartContainer1",
				{
					animationEnabled: true, // change to false
					
					title:{
						text: moment('2017-01-08').format("YYYY-MM-DD") + " 매출 현황"
					},
					axisX:{
						labelFontSize: 14,
						interval: 3,
					},
					data: [
						{	
							type: "bar",
							dataPoints:dataPoints1
						}
					],
				}
			);
			chart1.render();
			*/
			

		});

		// 3. 월별 좌표
		var dataPoints2_1 = new Array();
		var dataPoints2_2 = new Array();
		var dataPoints2_3 = new Array();
		var item;
		$.getJSON('/stcs/stcs/listData', {
			start_date	: moment().subtract(0, 'days').format("YYYY-MM-DD"),
			end_date	: moment().subtract(0, 'days').format("YYYY-MM-DD"),
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			item = data.data;
		
			for(var i=0; i < item.length; i++){
				
				tmpObj = new Object();
				tmpObj.label = item[i].CUST_FRNM + " " + item[i].PIS_NM + " " + item[i].SIZES;
				tmpObj.y = parseInt(item[i].QTY);
				dataPoints2_1.push(tmpObj);

				tmpObj1 = new Object();
				tmpObj1.label = item[i].CUST_FRNM  + " " + item[i].PIS_NM+ " " + item[i].SIZES;
				tmpObj1.y = parseInt(item[i].S_QTY);
				dataPoints2_2.push(tmpObj1);

				tmpObj2 = new Object();
				tmpObj2.label = item[i].CUST_FRNM  + " " + item[i].PIS_NM+ " " + item[i].SIZES;
				tmpObj2.y = parseInt(item[i].REST);
				dataPoints2_3.push(tmpObj2);
				
			};

			var jsonData = JSON.stringify(dataPoints1) ;
			 
			var chart2 = new CanvasJS.Chart("chartContainer2",
				{
					animationEnabled: true, // change to false
					theme: "theme1",
					title:{
						text: moment().subtract(0, 'days').format("YYYY-MM-DD") + " 입고 출고 현황",
						fontSize: 20
					},
					axisY:[{
							lineColor: "#4F81BC",
							tickColor: "#4F81BC",
							labelFontColor: "#4F81BC",
							titleFontColor: "#4F81BC",
							lineThickness: 2,
						},
						{
							lineColor: "#C0504E",
							tickColor: "#C0504E",
							labelFontColor: "#C0504E",
							titleFontColor: "#C0504E",
							lineThickness: 2,
					}],
					axisX:{
						labelFontSize: 14,
						interval: 1,
					},
					data: [
						{	
							type: "stackedColumn",
							showInLegend: true,
							legendText: "입고 수량",
							//color: "blue",
							dataPoints:dataPoints2_1
						},
						{	
							type: "stackedColumn",
							showInLegend: true,
							legendText: "출고 수량",
							//color: "red",
							axisYIndex: 0,
							dataPoints:dataPoints2_2
						},
						
						{	
							type: "spline",
							showInLegend: true,
							legendText: "현재고",
							
							//color: "green",
							dataPoints:dataPoints2_3
						}
						
					],
					legend: {
						cursor: "pointer",
						itemclick: function (e) {
							if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
								e.dataSeries.visible = false;
							} else {
								e.dataSeries.visible = true;
						}
						chart.render();
						}
					}
					
				}
			);
			chart2.render();
		});
		
		// 3. 10일간 결산표

		var start3			= moment().subtract(10, 'days').format('YYYY-MM-DD');
		var end3			= moment().format('YYYY-MM-DD');
		var before_start	= moment().subtract(11, 'days').format('YYYY-MM-DD');
		
		function sortJsonName(a,b){
			return a.CLOSING_DATE.toLowerCase() > b.CLOSING_DATE.toLowerCase() ? 1 : -1;
		};

		$.getJSON('/closing/closing/listData', {
			start_date	: start3,
			end_date	: end3,
			before_start:before_start,
			sort		: 'force',
		},function(xhr){

			var item;
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			item = data.data;
			item = $(item).sort(sortJsonName);

			var dataPoints3_1 = new Array();
			var dataPoints3_2 = new Array();
			var dataPoints3_3 = new Array();
			var dataPoints3_4 = new Array();

			for(var i=0; i < item.length; i++){
				

				// 총매입
				tmpObj = new Object();
				tmpObj.label = item[i].CLOSING_DATE;
				tmpObj.y = parseInt(item[i].BUY_AMT) /10000;
				dataPoints3_1.push(tmpObj);
				
				// 총매출
				tmpObj1 = new Object();
				tmpObj1.label = item[i].CLOSING_DATE;
				tmpObj1.y = parseInt(item[i].SALE_AMT)/10000;
				dataPoints3_2.push(tmpObj1);

				// 총지출금
				tmpObj3 = new Object();
				tmpObj3.label = item[i].CLOSING_DATE;
				tmpObj3.y = parseInt(item[i].COST)/10000;
				dataPoints3_3.push(tmpObj3);

				// 총수익금
				tmpObj4 = new Object();
				tmpObj4.label = item[i].CLOSING_DATE;
				tmpObj4.y = parseInt(item[i].TOTAL_GAIN)/10000;
				dataPoints3_4.push(tmpObj4);

			};
		
			var chart3 = new CanvasJS.Chart("chartContainer3", {
				title:{
					text:"최근 10일간 결산표",
					fontSize: 20
				},
				animationEnabled: true,
				axisX:{
					interval: 1,
					labelFontSize: 15,
					lineThickness: 0
				},
				axisY2:{
					valueFormatString: "#,##0.##만원",
					lineThickness: 0,
					labelFontSize: 15,
				},
				toolTip: {
					shared: true
				},
				legend:{
					verticalAlign: "top",
					horizontalAlign: "center",
					fontSize: 17

				},

				data: [
						{     
							type: "stackedBar",
							showInLegend: true,
							name: "총매입금",
							axisYType: "secondary",
							color: "#7E8F74",
							indexLabelFontColor:'#ffffff',
							indexLabelFontSize : 15,
							indexLabel: "{y}",
							dataPoints: dataPoints3_1
						},
						{     
							type: "stackedBar",
							showInLegend: true,
							name: "총매출금",
							axisYType: "secondary",
							color: "#F0E6A7",
							indexLabelFontSize : 15,
							indexLabel: "{y}",
							dataPoints: dataPoints3_2
						},
						{     
							type: "stackedBar",
							showInLegend: true,
							name: "총지출금",
							axisYType: "secondary",
							color: "#EBB88A",
							indexLabelFontSize : 15,
							indexLabel: "{y}",
							dataPoints: dataPoints3_3
						},
						{
							type: "stackedBar",
							showInLegend: true,
							name: "총수익금",
							axisYType: "secondary",
							color:"#DB9079",
							indexLabelFontColor:'#ffffff',
							indexLabelFontSize : 15,
							indexLabel: "{y}",
							dataPoints: dataPoints3_4
						}
					]
				});
			chart3.render();
			}
		);

		


		// 4. 미수업체현황
		var start3			= moment().subtract(10, 'days').format('YYYY-MM-DD');
		var end3			= moment().format('YYYY-MM-DD');
		var before_start	= moment().subtract(11, 'days').format('YYYY-MM-DD');

		$.getJSON('/uncl/uncl/listData', {
			chkZero		: "0",
		},function(xhr){
			var item;
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			item = data.data;
			
			var dataPoints4_1 = new Array();
			
			for(var i=0; i < item.length; i++){
				
				// 총매입
				tmpObj = new Object();
				tmpObj.label = item[i].FRNM;
				tmpObj.y = parseInt(item[i].TOTAL_UNCL_AMT) / 10000;
				dataPoints4_1.push(tmpObj);
				
			};
		
			var chart4 = new CanvasJS.Chart("chartContainer4", {
				title:{
					text:"미수업체목록",
					fontSize: 20
				},
				animationEnabled: true,
				axisX:{
					interval: 1,
					gridThickness: 1,
					labelFontSize: 13,
					labelFontStyle: "normal",
					labelFontWeight: "normal",
					labelFontFamily: "Lucida Sans Unicode"
				},
				axisY2:{
					labelFontSize: 13,
					valueFormatString: "#,##0.##만원",
					//interlacedColor: "rgba(1,77,101,.2)",
					//gridColor: "rgba(1,77,101,.1)"
				},
				toolTip: {
					shared: true
				},
				legend:{
					verticalAlign: "top",
					horizontalAlign: "center",
					fontSize: 13
				},

				data: [
						{     
							type: "bar",
							showInLegend: true,
							name: "총 미수금",
							axisYType: "secondary",
							color: "#7E8F74",
							indexLabelFontSize : 15,
							indexLabelFontColor:'#ffffff',
							dataPoints: dataPoints4_1
						}
					]
				});
			chart4.render();
			}
		);
		
		@endrole
		
	});
	
</script>

<div class="modal modal-info" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
			<h4 class="modal-title">알립니다.</h4>
			</div>
			<div class="modal-body">
				<p>서버 점검 및 이전 안내드립니다</p>
				<ul>
					<li> 일시 : 2017년 5월 11일(목) 오후 6시 30분 ~ 오후 9시 30분</li>
					<li> 내용 : 서버 업그레이드 및 속도 개선</li>
					<li> 해당 일시 서비스가 일시적으로 제한되오니 양해 바라오며</li>
					<li> 업무시간 이용에는 불편함 없도록 하겠습니다.</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-right" data-dismiss="modal"> 닫기</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection
<script type="text/javascript">
	function startIntro(){};
</script>


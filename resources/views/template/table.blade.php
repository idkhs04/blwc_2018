
<div class="control-group">
	<div class='dvAlert'></div>
	
	<!--
	<div class="tblCust{{$id}}Sale">
		<div class="unlc_area">
			<label><span class="glyphicon glyphicon-inbox"></span> 미수금</label><span class="TOTAL_UNCL" ></span> 원
			<div class="control-group pull-right tblCust{{$id}}_footer">
				<label> 금액합계 </label>
				<input type="text" class="tblCust{{$id}} dt-body-right" readonly="readonly" /> 원
			</div>
		</div>
		
	</div>
	-->

	<table id="tblCust{{$id}}" class="table table-hover display nowrap custList"  width="100%">
		<caption>일일판매</caption>
		<thead>
			<tr>
				<th width="15px;"></th>
				<th width="30px">수조</th>
				<th width="60px">매입처</th>
				<th width="50px">원산지</th>
				<th width="60px">입고일</th>
				<th width="50px">규격</th>
				<th width="60px">어종</th>
				<th width="50px">현재고</th>
				<th width="60px">중량</th>
				<th width="60px">판매단가</th>
				<th width="80px">금액</th>
				<th width="auto"></th>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>

	<div class="control-group pull-right tblCust{{$id}}_footer">
		<label> 금액합계 </label>
		<input type="text" class="tblCust{{$id}} dt-body-right" readonly="readonly" /> 원
		<button class="btn btn-success btn-block btnSale"><i class="fa  fa-gavel"></i>판매</button>
	</div>
	
	<div id="tblCust{{$id}}Sale">
		<div class="unlc_area">
			<label><span class="glyphicon glyphicon-inbox"></span> 미수금</label><span class="TOTAL_UNCL" ></span> 원
		</div>

		<div class="tbl_area"></div>
	</div>

	<div class="box box-info resultSale" id="resultSale{{$id}}">
		
		<div class="box-body">
			<div class="form-group">
				<div class="col-md-6 col-xs-6">
					<label for="txtSumSale"><i class='fa fa-circle-o text-aqua'></i> 판매금액합계</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtSumSale" placeholder="합계" name="txtSumSale" />
					
					<label for="txtDicount"><i class='fa fa-circle-o text-aqua'></i> 할인액</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtDicount" placeholder="할인금액" name="txtDicount" >

					<label for="txtSumSaleAll"><i class='fa fa-circle-o text-aqua'></i> 합계</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtSumSaleAll" placeholder="합계" name="txtSumSaleAll" >				

				</div>

				<div class="col-md-6 col-xs-6">
					<label for="txtCach"><i class='fa fa-circle-o text-aqua'></i> 현금</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtCach" placeholder="현금" name="txtCach" />

					<label for="txtCard"><i class='fa fa-circle-o text-aqua'></i> 카드</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtCard" placeholder="카드" name="txtCard" / >
				</div>

			</div>
			<div class="form-group">
				
				<div class="col-md-6 col-xs-6">
					<label for="txtUnprov"><i class='fa fa-circle-o text-aqua'></i> 미수금</label>
					<input type="text" class="form-control form-input-sm numeric dt-body-right" id="txtUnprov" placeholder="미수금" name="txtUnprov" >
					
					<label for="txtDicountText"><i class='fa fa-circle-o text-aqua'></i> 할인내역</label>
					<input type="text" class="form-control form-input-sm cis-lang-ko" id="txtDicountText" placeholder="할인내역" name="txtDicountText" style="width:70%;">
				</div>
			</div>
			
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-info pull-right" id="btnSaveSale">저장</button> 
			<button type="button" class="btn btn-warning pull-right" id="btnCancelBack">이전 </button>
		</div>
	</div>

	<div class="box box-info endResultSale" id="endResultSale{{$id}}">
		<div class="box-body">
			<p style="text-align:center;">
				<div class="gvSuccess">
				
				</div>
				<br />
				<input type="hidden" name="saled-seq"  />
				<input type="hidden" name="saled-write_dt" />
				
				<label><span class="glyphicon glyphicon-inbox"></span> 판매미수</label>
				<input type="text" readonly="readonly" name="UnprovResult" style="text-align:right;"/>원

				<button class="btn btn-info btnGoSale pull-right"><i class="fa fa-opencart"></i> 판매계속하기</button>
				<button class="btn btn-warning btnPdfSale2 pull-right" ><i class="fa fa-file-pdf-o"></i> 거래명세서 출력</button>
			</p>
		<div>
	</div>
</div>


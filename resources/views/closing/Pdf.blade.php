<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>결산표</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:11px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-align:right; border-top:1px solid block;border-bottom:1px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:1px solid block;
				margin-bottom:20px;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
			
		</head>
		<body>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<span class="title" style='text-align:center;font-size:23px;display:block;margin-bottom:20px;'>{{ $title }}</span> 
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
							</tr>
						</table>
						<span class="title" style='text-align:left;font-size:13px;display:block;'>1.미수금</span> 
						<table width="100%" border="1" cellpadding="0" cellspacing="0">
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'>결산전 총미수</td>
								<td style='text-align:center;'>결산 미수</td>
								<td style='text-align:center;'>결산 수금</td>
								<td style='text-align:center;'>미수금 총액</td>
							
								<td style='text-align:center;'>현금</td>
								<td style='text-align:center;'>통장</td>
								<td style='text-align:center;'>카드</td>
								<td style='text-align:center;'>기타</td>
							</tr>
							<tr>
								<td style="text-align:right;padding-right:3px;">{{ number_format( $model->UNCL_ALL - $model->ACCRUED + $model->COLLECTION_AMT )  }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->ACCRUED)  }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COLLECTION_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->UNCL_ALL) }}</td>
						
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COLLECTION_CASH) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COLLECTION_BANK) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COLLECTION_CARD) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COLLECTION_ETC) }}</td>
							</tr>
						</table>

						<span class="title" style='text-align:left;font-size:13px;display:block;'>2.미지급금</span> 
						<table id="tblList" summary="어종별 재고조회" width="100%" border="1" cellpadding="0" cellspacing="0" >
							<caption>미지급금</caption>
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'>결산전 총미지급금</td>
								<td style='text-align:center;'>결산 미지급금</td>
								<td style='text-align:center;'>결산일 지급금</td>
								<td style='text-align:center;'>미지급금 총액</td>
								
								<td style='text-align:center;'>현금</td>
								<td style='text-align:center;'>통장</td>
								<td style='text-align:center;'>카드</td>
								<td style='text-align:center;'>기타</td>
							</tr>
						
							<tr>
								<td style='text-align:right;padding-right:3px;'>{{ number_format( (double)$model->UNPROV_ALL -  (double)$model->CREDIT + (double)$model->PAYMENT)}}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->CREDIT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->PAYMENT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->UNPROV_ALL) }}</td>
								
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->PAYMENT_CASH) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->PAYMENT_BANK) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->PAYMENT_CARD) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->PAYMENT_ETC) }}</td>
							</tr>
							
						</table>
						
						<span class="title" style='text-align:left;font-size:13px;display:block;'>3.매출(판매)내역</span> 
						<table id="tblList" summary="매출(판매)내역 결산표"  width="100%" border="1" cellpadding="0" cellspacing="0" >
							<caption>매출(판매)내역 결산표</caption>
							
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'></td>
								<td style='text-align:center;'>판매량(Kg)</td>
								<td style='text-align:center;'>판매금액</td>
								<td style='text-align:center;'>입금액</td>
								<td style='text-align:center;'>미수액</td>
							</tr>
						
							<tr>
								<td style='text-align:center;'>결산 전일</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_SALE_WEIGHT, 1) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_SALE_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_SALE_COLLECTION) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_SALE_ACCRUED) }}</td>
							</tr>
							<tr>
								<td style='text-align:center;'>결산일</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_WEIGHT, 1) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_COLLECTION) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_ACCRUED) }}</td>
							</tr>
							
						</table>
						
						<span class="title" style='text-align:left;font-size:13px;display:block;'>4.매입(구매)내역</span> 
						<table id="tblList" summary="매입(구매)내역 결산표"  width="100%" border="1" cellpadding="0" cellspacing="0">
							<caption>매입(구매)내역 결산표</caption>
							
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'></td>
								<td style='text-align:center;'>구매량(Kg)</td>
								<td style='text-align:center;'>구매금액</td>
								<td style='text-align:center;'>지급금액</td>
								<td style='text-align:center;'>미지급금액</td>
							</tr>
						
							<tr>
								<td style='text-align:center;'>결산 전일</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_BUY_WEIGHT, 1) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_BUY_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'> {{ number_format($model->BEFORE_BUY_PAYMENT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BEFORE_BUY_CREDIT) }}</td>
							</tr>
							<tr>
								<td style='text-align:center;'>결산일</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BUY_WEIGHT, 1) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BUY_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BUY_PAYMENT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BUY_CREDIT) }}</td>
							</tr>
							
						</table>

						<span class="title" style='text-align:left;font-size:13px;display:block;'>5.지출내역</span> 
						<table id="tblList" summary="지출내역"  width="100%" border="1" cellpadding="0" cellspacing="0">
							<caption>지출내역</caption>
							
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'>업체명</td>
								<td style='text-align:center;'>구분</td>
								<td style='text-align:center;'>지출항목</td>
								<td style='text-align:center;'>현금</td>
								<td style='text-align:center;'>카드</td>
								<td style='text-align:center;'>통장</td>
							</tr>
						
							@foreach ($model1 as $key => $item)
							<tr>
								<td style='text-align:center;'>{{ $item->FRNM}}</td>
								<td style='text-align:center;'>{{ $item->DIV_NM}}</td>
								<td style='text-align:center;'>{{ $item->ACCOUNT_NM}}</td>
								@if( $item->DE_CR_DIV == "0")
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->CASH*(-1))}}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->CARD*(-1))}}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->ONLINE*(-1))}}</td>	
								@else
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->CASH)}}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->CARD)}}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($item->ONLINE)}}</td>
								@endif
							</tr>
							@endforeach
							<tr>
								<td colspan="2" style='text-align:center;'>합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계</td>
								<td style='text-align:center;'></td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($sumCASH) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($sumCARD) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($sumONLINE) }}</td>
							</tr>
							
						</table>

						<table id="tblList" summary="총 결산표"  width="100%" border="1" cellpadding="0" cellspacing="0">
							<caption>총 결산표</caption>
							
							<tr style="background-color:#b4b4b4;">
								<td style='text-align:center;'>총매입금액</td>
								<td style='text-align:center;'>총 판매금액</td>
								<td style='text-align:center;'>총 지출액</td>
								<td style='text-align:center;'>수익금</td>
							</tr>
						
							<tr>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->BUY_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_AMT) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->COST) }}</td>
								<td style='text-align:right;padding-right:3px;'>{{ number_format($model->SALE_AMT - $model->BUY_AMT - $model->COST) }}</td>
							</tr>
							
						</table>
					</div>
				</div>
			<!-- 본문  -->
			</div>
		</body>
</html>

	



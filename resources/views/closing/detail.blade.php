@extends('layouts.main')
@section('class','매출관리')
@section('title','결산표')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info, .btn-danger {
		margin-top: 0px;
		padding: 0;
		margin-left: 10px;
		vertical-align: baseline;
		margin: 0;
		border: 0;
		width: 20px;
		margin-left: 10px;
	}
	.btnSearch{ margin-top:0;}
	#tblListOut_filter { display:none;}

	#modal_stock_arrange label{
		display:inline-block;
		width:22%;
		clear:both;
		padding-left:1%;
	}

	#modal_stock_arrange .reason label{
		width:auto;
		padding-left:2%;
	}
	

	#modal_stock_arrange .form-control{
		display:inline-block;
		width:22%;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	#tblList_Sum{
		float:right;
	}

	#SUM_QTY{ padding-left:2px;}
	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}
	
	th.dt-body-right, .form-group.dt-body-right{ text-align:right;}
	th { text-align:center; padding:5px;}
	table.dataTable thead > tr > th {
		padding: 5px;
		background-color:khaki;
	}
	table.dataTable tr > td {
		text-align:right;
	}
	.box-header{padding:0 10px 0 10px;}
	.box-header h4{ margin:3px 3px 2px 0; float:left;}

	h5 { float:left; margin:3px 3px 2px 0;}
	
	.box-body{
		padding:2px 10px;
	}
	.dvMainBtnSet{ height:70px;}
	.col-md-6{ padding-left:0; padding-top:5px;}

	.col-md-6 > .box-body {padding-left:0;}
	.dataTables_length{ display:none;}

	table th, table td{ padding:2px;}

	.slideCloseUncl, .slideCloseUnProv{ display:none;}


</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-6 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 결산표 출력</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header">
			@if ( $START_DATE == $END_DATE )
			<h3 style="text-align:center;">{{ $START_DATE}} 결산표</h3>
			@else
			<h3 style="text-align:center;">{{ $START_DATE}} ~ {{ $END_DATE}} 결산표</h3>
			@endif
		</div>
		<div class="box-body">
			<div class="col-md-6">
				<div class="box-header">
					<h4>1. 미수금 
						<button type="button" class="btn btn-info pull-right btn_UnclOpen">
							<i class="fa fa-plus-circle"></i>
						</button>
					</h4>
					
				</div>
				<div class="box-body">
					<table id="tblListUncl" class="table dataTable table-bordered table-hover display nowrap" summary="미수금결산표" width="100%">
						<caption>미수금 결산표</caption>
						<thead>
							<tr>
								<th class="name">결산전 총미수</th>
								<th class="name">결산 미수</th>
								<th class="name">결산 수금</th>
								<th class="name">미수 총액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							
								<td>{{ number_format( $model->UNCL_ALL - $model->ACCRUED + $model->COLLECTION_AMT ) }}</td>
								<td>{{ number_format($model->ACCRUED)  }}</td>
								<td>{{ number_format($model->COLLECTION_AMT) }}</td>
								<td>{{ number_format($model->UNCL_ALL) }}</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th class="name">현금</th>
								<th class="name">통장</th>
								<th class="name">카드</th>
								<th class="name" colspan="2">기타</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format($model->COLLECTION_CASH) }}</td>
								<td>{{ number_format($model->COLLECTION_BANK) }}</td>
								<td>{{ number_format($model->COLLECTION_CARD) }}</td>
								<td>{{ number_format($model->COLLECTION_ETC) }}</td>
							</tr>
						</tbody>
					</table>
					
					<div class="form-group slideCloseUncl">
						<div class="box box-primary">
							<h5>
								<span class="glyphicon glyphicon-th-large"></span> 결산 미수금 상세내역
								<button type="button" class="btn btn-danger btn-default pull-right btn_UnclClose">
									<i class="fa fa-minus-circle"></i>
								</button>
							</h5>								
							<div class="box-body">
								<table id="tblUnclList" class="table table-bordered table-hover display nowrap" summary="미수목록">
									<caption>미수목록</caption>
									<thead>
										<tr>
											<th scope="col" class="subject">거래처명</th>
											<th scope="col" class="check">일자</th>
											<th scope="col" class="subject">현금</th>
											<th scope="col" class="name">카드</th>
											<th scope="col" class="name">통장</th>
											<th scope="col" class="name">기타</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>
						2. 미지급금
						<button type="button" class="btn btn-info pull-right btn_UnProvOpen">
							<i class="fa fa-plus-circle"></i>
						</button>
					</h4>
				</div>
				<div class="box-body">
					<table id="tblListUnProv" class="table dataTable  table-bordered table-hover display nowrap" summary="미지급금결산표" width="100%">
						<caption>미지급금 결산표</caption>
						<thead>
							<tr>
								<th class="name">결산전 총미지급</th>
								<th class="name">결산 미지급</th>
								<th class="name">결산일 지급</th>
								<th class="name">미지급 총액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format( (double)$model->UNPROV_ALL -  (double)$model->CREDIT + (double)$model->PAYMENT) }}</td>
								<td>{{ number_format($model->CREDIT) }}</td>
								<td>{{ number_format($model->PAYMENT) }}</td>
								<td>{{ number_format( (double)$model->UNPROV_ALL ) }}</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th class="name">현금</th>
								<th class="name">통장</th>
								<th class="name">카드</th>
								<th class="name" colspan="2">기타</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format($model->PAYMENT_CASH) }}</td>
								<td>{{ number_format($model->PAYMENT_BANK) }}</td>
								<td>{{ number_format($model->PAYMENT_CARD) }}</td>
								<td>{{ number_format($model->PAYMENT_ETC) }}</td>
							</tr>
						</tbody>
					</table>
					<div class="form-group slideCloseUnProv">
						<div class="box box-primary">
							<h5>
								<span class="glyphicon glyphicon-th-large"></span> 결산 미지급금 상세내역
								<button type="button" class="btn btn-danger btn-default pull-right btn_UnprovClose">
									<i class="fa fa-minus-circle"></i>
								</button>
							</h5>								
							<div class="box-body">
								<table id="tblUnProvList" class="table table-bordered table-hover display nowrap" summary="미지급목록">
									<caption>미수목록</caption>
									<thead>
										<tr>
											<th scope="col" class="subject">거래처명</th>
											<th scope="col" class="check">일자</th>
											<th scope="col" class="subject">현금</th>
											<th scope="col" class="name">카드</th>
											<th scope="col" class="name">통장</th>
											<th scope="col" class="name">기타</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="col-md-6">
				<div class="box-header">
					<h4>3.매출(판매)내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListSale" class="table dataTable table-bordered table-hover display nowrap" summary="매출(판매)내역" width="50%">
						<caption>매출(판매)내역 결산표</caption>
						<thead>
							<tr>
								<th class="name"></th>
								<th class="name">판매량(Kg)</th>
								<th class="name">판매금액</th>
								<th class="name">입금액</th>
								<th class="name">미수액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>결산 전일</th>
								<td>{{ number_format($model->BEFORE_SALE_WEIGHT, 1) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_AMT) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_COLLECTION) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_ACCRUED) }}</td>
							</tr>
							<tr>
								<th>결산일</th>
								<td>{{ number_format($model->SALE_WEIGHT, 1) }}</td>
								<td>{{ number_format($model->SALE_AMT) }}</td>
								<td>{{ number_format($model->SALE_COLLECTION) }}</td>
								<td>{{ number_format($model->SALE_ACCRUED) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>4.매입(구매)내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListSale" class="table dataTable  table-bordered table-hover display nowrap" summary="매입(구매)내역" width="50%">
						<caption>매입(구매)내역 결산표</caption>
						<thead>
							<tr>
								<th class="name"></th>
								<th class="name">구매량(Kg)</th>
								<th class="name">구매금액</th>
								<th class="name">지급금액</th>
								<th class="name">미지급금액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>결산 전일</th>
								<td>{{ number_format($model->BEFORE_BUY_WEIGHT, 1) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_AMT) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_PAYMENT) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_CREDIT) }}</td>
							</tr>
							<tr>
								<th>결산일</th>
								<td>{{ number_format($model->BUY_WEIGHT, 1) }}</td>
								<td>{{ number_format($model->BUY_AMT) }}</td>
								<td>{{ number_format($model->BUY_PAYMENT) }}</td>
								<td>{{ number_format($model->BUY_CREDIT) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>5.지출내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListOut" class="table dataTable table-bordered table-hover display nowrap" summary="지출내역" width="100%">
						<caption>지출내역</caption>
						<thead>
							<tr>
								<th class="name">업체명</th>
								<th class="name">구분</th>
								<th class="name">지출항목</th>
								<th class="name">현금</th>
								<th class="name">카드</th>
								<th class="name">통장</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th> 합계 </th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>총 결산표</h4>
				</div>
				<div class="box-body">
					<table id="tblListTotal" class="table dataTable table-bordered table-hover display nowrap" summary="총 결산표" width="100%">
						<caption>총 결산표</caption>
						<thead>
							<tr>
								<th class="name">총매입금액</th>
								<th class="name">총 판매금액</th>
								<th class="name">총 지출액</th>
								<th class="name">수익금</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format($model->BUY_AMT) }}</td>
								<td>{{ number_format($model->SALE_AMT) }}</td>
								<td>{{ number_format($model->COST) }}</td>
								<td>{{ number_format($model->SALE_AMT - $model->BUY_AMT - $model->COST) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<!-- 본문  -->
</div>
	
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		
		var oTable = $('#tblListOut').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 500,		// 기본 100개 조회 설정
			bInfo:false,
			ajax: {
				url: "/closing/closing/detailList",
				data:function(d){
					d.start_date	= "{{Request::segment(4)}}";
					d.end_date		= "{{Request::segment(5)}}";
				}
			},
			order: [[ 0, 'desc' ]],
			columns: [
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-center" },
				{ 
					data: 'DE_CR_DIV',
					name : 'I.DE_CR_DIV',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data == "0"){
								return "<span class='red'>" + row.DIV_NM + "</span>";
							}else{
								return "<span class='blue'>" + row.DIV_NM + "</span>";
							}
						}
						return data;
					},
					className: "dt-body-center" 
				},
				{  data: 'ACCOUNT_NM', name : 'ACCOUNT_NM', className: "dt-body-center" },				
				{ 
					data: 'CASH', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'CARD', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'ONLINE', 
					render: function ( data, type, row ) {
						if ( type === 'display') {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
			],
			fnRowCallback: function(nRow, nData){
				
				if( nData.DE_CR_DIV == "0" ){
					$(nRow).find("td:eq(3)").html( Number(nData.CASH *=(-1)).format() )
					$(nRow).find("td:eq(4)").html( Number(nData.CARD *=(-1)).format() )
					$(nRow).find("td:eq(5)").html( Number(nData.ONLINE *=(-1)).format() )
				}
			},
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var qtys = 0;
				var j = 0;
				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;
				};
				// 총매입수량
				
				/*
				Amt = api.column( 5 ).data().reduce( function (a, b) {
					size = api.column( 3 ).data()[j++];
					if( size == "QTYS") {
						qtys += intVal(b);
						return intVal(a);
					}else{
						return intVal(a) + intVal(b);
					}
					
				}, 0 );

				$( api.column( 5 ).footer() ).html("Kg : " + Number(Amt).format() + " / 마리:" + qtys);
				*/

				// 현금
				cashAmt = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(cashAmt).format() + " 원");

				// 카드
				cardAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(Number(cardAmt).format() + " 원");

				// 통장
				tongAmt = api.column( 5 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 5).footer() ).html(Number(tongAmt).format() + " 원");

				// 합계
				Amt = parseInt(tongAmt) + parseInt(cardAmt) + parseInt(cashAmt);
				$( api.column(1).footer() ).html(Number(Amt).format() + " 원");

				
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 목록 바로가기
		$("#btnList").click(function(){
			localStorage["IS_BACK"]		= "Y";
			location.href="/closing/closing/index?page=1";
		});

		// 결산표 출력
		$("#btnPdf").click(function(){
			
			var width=740;
			var height=720;
			
			var start_date	= "{{$START_DATE}}"  ;
			var end_date	= "{{$END_DATE}}" ;
			
			var url = "/closing/closing/Pdf/" + start_date + "/" + end_date;
			getPopUp(url , width, height);
		});
		
		// 미수상세보기
		$(".btn_UnclOpen").click(function(){ $( ".slideCloseUncl" ).show(); });
		// 미수상세닫기
		$(".btn_UnclClose").click(function(){ $( ".slideCloseUncl" ).hide(); });
		
		var uTable = $('#tblUnclList').DataTable({
			processing: true,
			serverSide: true,
			retrieve: true,
			bInfo	: false,
			ajax: {
				url: "/closing/closing/getClosingUnclList",
				data:function(d){
					d.WRITE_DATE_START	=  "{{$START_DATE}}";
					d.WRITE_DATE_END	=  "{{$END_DATE}}";
				}
			},
			order: [[ 0, 'asc' ], [ 6, 'asc' ]], // order by를 위해서 SEQ를 넣어서 unvisible로 수행 : 2017-09-19
			columns: [
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-left" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE', className: "dt-body-center" },
				{
					data:   "CASH",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data:   "CARD",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},

				{
					data:   "CHECK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data:   "REC",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							
							uncl	= $.isNumeric(parseInt(row.UNCL)) ? row.UNCL : 0;
							rec		= $.isNumeric(parseInt(data)) ? data : 0;
							return Number( ( parseInt(uncl) + parseInt(rec) )).format();
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data:   "SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					visible: false,
					className: "dt-body-right"
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
	
		});

		// 미지급상세보기
		$(".btn_UnProvOpen").click(function(){ $( ".slideCloseUnProv" ).show(); });
		// 미지급상세닫기
		$(".btn_UnprovClose").click(function(){ $( ".slideCloseUnProv" ).hide(); });
		
		var uTable = $('#tblUnProvList').DataTable({
			processing: true,
			serverSide: true,
			retrieve: true,
			bInfo	: false,
			ajax: {
				url: "/closing/closing/getClosingUnprovList",
				data:function(d){
					d.WRITE_DATE_START	=  "{{$START_DATE}}";
					d.WRITE_DATE_END	=  "{{$END_DATE}}";
				}
			},
			columns: [
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-left" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE', className: "dt-body-center" },
				{
					data:   "CASH",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data:   "CARD",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},

				{
					data:   "CHECK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data:   "ETC",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							
							uncl	= $.isNumeric(parseInt(row.UNCL)) ? row.UNCL : 0;
							rec		= $.isNumeric(parseInt(data)) ? data : 0;

							return Number( ( uncl + rec )).format();
						}
						return data;
					},
					className: "dt-body-center"
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
	
		});

	});

</script>
@stop
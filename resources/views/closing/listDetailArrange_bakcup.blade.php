@extends('layouts.main')
@section('class','매출관리')
@section('title','기간별결산표')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
 <style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblListOut_filter { display:none;}

	#modal_stock_arrange label{
		display:inline-block;
		width:22%;
		clear:both;
		padding-left:1%;
	}

	#modal_stock_arrange .reason label{
		width:auto;
		padding-left:2%;
	}


	#modal_stock_arrange .form-control{
		display:inline-block;
		width:22%;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	#tblList_Sum{
		float:right;
	}

	#SUM_QTY{ padding-left:2px;}
	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}
	th{ background-color:skyblue;}
	th.dt-body-right, .form-group.dt-body-right{ text-align:right; }
	th { text-align:center; padding:5px;}
	table.dataTable thead > tr > th {
		padding: 5px;
	}
	table.dataTable tr > td {
		text-align:right;
	}
	.box-header{padding:0 10px 0 10px;}
	.box-header h4{ margin:5px 0 5px 0;}

	.box-body .col-md-6{
		padding-left:5px;
		padding-right:5px;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-6 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success btnPdf" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 결산표 출력</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header">
			<h3 style="text-align:center;">{{ $START_DATE}} ~ {{ $END_DATE}} 결산표</h3>
		</div>

		<div class="box-body">
			<div class="col-md-6">
				<div class="box-header">
					<h4>1.미수금
					<!--
						<button type="button" class="btn btn-info pull-right btn_UnclOpen">
							<i class="fa fa-plus-circle"></i>
						</button>
					-->
					</h4>

				</div>
				<div class="box-body">
					<table id="tblListUncl" class="table dataTable table-bordered table-hover display nowrap" summary="미수금결산표" width="50%">
						<caption>미수금 결산표</caption>
						<thead>
							<tr>
								<th class="name">결산전 총미수</th>
								<th class="name">결산 미수</th>
								<th class="name">결산 수금</th>
								<th class="name">미수 총액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format( $model->ALL_ACCRUED - ( $model->ACCRUED - $model->COLLECTION_AMT )) }}</td>
								<td>{{ number_format($model->ACCRUED)  }}</td>
								<td>{{ number_format($model->COLLECTION_AMT) }}</td>
								<td>{{ number_format($model->ALL_ACCRUED) }}</td>
							</tr>
						
							<tr>
								<th class="name">현금</th>
								<th class="name">통장</th>
								<th class="name">카드</th>
								<th class="name">기타</th>
							</tr>
							<tr>
								<td>{{ number_format($model->COLLECTION_CASH) }}</td>
								<td>{{ number_format($model->COLLECTION_BANK) }}</td>
								<td>{{ number_format($model->COLLECTION_CARD) }}</td>
								<td>{{ number_format($model->COLLECTION_ETC) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>2.미지급금</h4>
				</div>
				<div class="box-body">
					<table id="tblListUnProv" class="table dataTable  table-bordered table-hover display nowrap" summary="미지급금결산표" width="100%">
						<caption>미지급금 결산표</caption>
						<thead>
							<tr>
								<th class="name">결산전 총미지급금</th>
								<th class="name">결산 미지급금</th>
								<th class="name">결산일 지급금</th>
								<th class="name">미지급 총액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format($model->ALL_CREDIT - ( $model->CREDIT - $model->PAYMENT )) }}</td>
								<td>{{ number_format($model->CREDIT) }}</td>
								<td>{{ number_format($model->PAYMENT) }}</td>
								<td>{{ number_format($model->ALL_CREDIT) }}</td>
							</tr>
							<tr>
								<th class="name">현금</th>
								<th class="name">통장</th>
								<th class="name">카드</th>
								<th class="name">기타</th>
							</tr>
							<tr>
								<td>{{ number_format($model->PAYMENT_CASH) }}</td>
								<td>{{ number_format($model->PAYMENT_BANK) }}</td>
								<td>{{ number_format($model->PAYMENT_CARD) }}</td>
								<td>{{ number_format($model->PAYMENT_ETC) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-md-6">
				<div class="box-header">
					<h4>3.매출(판매)내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListSale" class="table dataTable table-bordered table-hover display nowrap" summary="매출(판매)내역" width="100%">
						<caption>매출(판매)내역 결산표</caption>
						<thead>
							<tr>
								<th class="name"></th>
								<th class="name">판매량(Kg)</th>
								<th class="name">판매금액</th>
								<th class="name">입금액</th>
								<th class="name">미수액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>결산 전일</th>
								<td>{{ number_format($model->BEFORE_SALE_WEIGHT) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_AMT) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_COLLECTION) }}</td>
								<td>{{ number_format($model->BEFORE_SALE_ACCRUED) }}</td>
							</tr>
							<tr>
								<th>결산일</th>
								<td>{{ number_format($model->SALE_WEIGHT) }}</td>
								<td>{{ number_format($model->SALE_AMT) }}</td>
								<td>{{ number_format($model->SALE_COLLECTION) }}</td>
								<td>{{ number_format($model->SALE_ACCRUED) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-md-6">
				<div class="box-header">
					<h4>4.매입(구매)내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListSale" class="table dataTable  table-bordered table-hover display nowrap" summary="매입(구매)내역" width="100%">
						<caption>매입(구매)내역 결산표</caption>
						<thead>
							<tr>
								<th class="name"></th>
								<th class="name">구매량(Kg)</th>
								<th class="name">구매금액</th>
								<th class="name">지급금액</th>
								<th class="name">미지급금액</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>결산 전일</th>
								<td>{{ number_format($model->BEFORE_BUY_WEIGHT) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_AMT) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_PAYMENT) }}</td>
								<td>{{ number_format($model->BEFORE_BUY_CREDIT) }}</td>
							</tr>
							<tr>
								<th>결산일</th>
								<td>{{ number_format($model->BUY_WEIGHT) }}</td>
								<td>{{ number_format($model->BUY_AMT) }}</td>
								<td>{{ number_format($model->BUY_PAYMENT) }}</td>
								<td>{{ number_format($model->BUY_CREDIT) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>5.지출내역</h4>
				</div>
				<div class="box-body">
					<table id="tblListOut" class="table dataTable table-bordered table-hover display nowrap" summary="지출내역" width="100%">
						<caption>지출내역</caption>
						<thead>
							<tr>
								<th class="name">업체명</th>
								<th class="name">지출항목</th>
								<th class="name">현금</th>
								<th class="name">카드</th>
								<th class="name">통장</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box-header">
					<h4>총 결산표</h4>
				</div>
				<div class="box-body">
					<table id="tblListTotal" class="table dataTable table-bordered table-hover display nowrap" summary="총 결산표" width="100%">
						<caption>총 결산표</caption>
						<thead>
							<tr>
								<th class="name">총매입금액</th>
								<th class="name">총 판매금액</th>
								<th class="name">총 지출액</th>
								<th class="name">수익금</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ number_format($model->BUY_AMT) }}</td>
								<td>{{ number_format($model->SALE_AMT) }}</td>
								<td>{{ number_format($model->COST) }}</td>
								<td>{{ number_format($model->SALE_AMT - $model->BUY_AMT - $model->COST) }}</td>

							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		$("form[name='getPdf'] > input[name='start_date']").val( "{{ $START_DATE}}" );
		$("form[name='getPdf'] > input[name='end_date']").val( "{{ $END_DATE}}" );

		// 목록 바로가기
		$("#btnList").click(function(){
			location.href="/closing/closing/index?page=1";
		});

		var oTable = $('#tblListOut').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/closing/closing/detailList",
				data:function(d){
					d.start_date	= "{{Request::segment(4)}}";
					d.end_date		= "{{Request::segment(5)}}";
				}
			},
			order: [[ 0, 'desc' ]],
			columns: [
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-center" },
				{ data: 'ACCOUNT_NM', name: 'ACCOUNT_NM', className: "dt-body-center" },
				{
					data: 'CASH',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'CARD',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'ONLINE',
					render: function ( data, type, row ) {
						if ( type === 'display') {
							return data;
						}
						return data;
					},
					className: "dt-body-right"
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var qtys = 0;
				var j = 0;
				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;
				};
				// 총매입수량

				/*
				Amt = api.column( 5 ).data().reduce( function (a, b) {
					size = api.column( 3 ).data()[j++];
					if( size == "QTYS") {
						qtys += intVal(b);
						return intVal(a);
					}else{
						return intVal(a) + intVal(b);
					}

				}, 0 );

				$( api.column( 5 ).footer() ).html("Kg : " + Number(Amt).format() + " / 마리:" + qtys);
				*/

				// 현금
				cashAmt = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(Number(cashAmt).format() + " 원");

				// 카드
				cardAmt = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(cardAmt).format() + " 원");

				// 통장
				tongAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4).footer() ).html(Number(tongAmt).format() + " 원");


			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("#btnPdf").click(function(){

			var width=740;
			var height=720;
			var start_date	= "{{Request::segment(4)}}";
			var end_date	= "{{Request::segment(5)}}";

			var url = "/closing/closing/Pdf/" + start_date + "/" + end_date;
			getPopUp(url , width, height);
		});

	});

</script>
<!--
<div id="pdf">
	<form name="getPdf" method="post" action="/closing/closing/Pdf" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="start_date" />
		<input type="hidden" name="end_date" />

	</form>
</div>
-->
@stop
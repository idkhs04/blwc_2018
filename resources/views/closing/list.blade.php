@extends('layouts.main')
@section('class','매출관리')
@section('title','결산관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale, .edit_alert{ display:none; }
	th { white-space: nowrap; }

	.btn-info{ margin-top:0;}
	tfoot th{ text-align:right;}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-2 col-xs-6">
				<div class="btn-group">
					<!--<button type="button" class="btn btn-success" id="btnSaveClosing" ><i class="fa fa-save"></i> 결산저장</button>-->
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list-alt"></i> 기간별 결산표</button>
				</div>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="input-group">
					<div id="reportrange" class="pull-right" style="cursor: pointer; padding:3px 5px 0 5px; border: 1px solid #ccc; width: 100%">
						<label>기간</label>
						<span></span> <b class="caret"></b>
						<input type='hidden' name="start_date" />
						<input type='hidden' name="end_date" />
					</div>
				</div>
			</div>

			<!--
			<div class="col-md-5 col-xs-12">
				<div class="pull-right simpleSetDtArea">
					<button type="button" class="btn btn btn-info today">오늘</button>
					<button type="button" class="btn btn btn-info week">최근1주일</button>
					<button type="button" class="btn btn btn-info month">최근1달</button>
					<button type="button" class="btn btn btn-info month6">최근6개월</button>
					<button type="button" class="btn btn btn-info year">최근1년</button>
				</div>
			</div>
			-->
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="edit_alert"><ul></ul></div>
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="기간별 판매 목록" width="100%">
				<caption>결산관리</caption>
				<thead>
					<tr>
						<th class="check" width="60px">결산일</th>
						<th class="name" width="60px">최종수정일</th>
						<th class="name" width="60px">총매입수량</th>
						<th class="name" width="60px">총매출수량</th>
						<th class="name" width="60px">총매입금</th>
						<th class="name" width="60px">총판매금</th>
						<th class="name" width="60px">총지출금</th>
						<th class="name" width="60px">수익금</th>
						<th class="name" width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		// 일일판매는 기본적으로 해당 달의 첫번째 달을 기준으로 조회
		var start = moment().startOf('month');
		var end = moment();
		start = end;

		var before_start = moment().subtract(1, 'days').format('YYYY-MM-DD');
		
		var isBack		= localStorage["IS_BACK"];

		if( isBack == "Y"){
			start		= moment(localStorage["START_DATE"]);
			end			= moment(localStorage["END_DATE"]);
			before_start= moment(localStorage["BEFORE_DATE"]);
			localStorage["IS_BACK"] = 'N';
		}


		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD'))//.trigger('change');;
			$("input[name='end_date']").val(end.format('YYYY-MM-DD'))//.trigger('change');;

			before_start = start.subtract(1, 'days').format('YYYY-MM-DD');

			localStorage["START_DATE"]	= start.format('YYYY-MM-DD'); 
			localStorage["END_DATE"]	= end.format('YYYY-MM-DD');
			localStorage["BEFORE_DATE"]	= before_start;
			localStorage["IS_BACK"]		= "N";
		}

		// 날짜 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);


		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},

			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},

			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/closing/closing/listData",
				data:function(d){
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.before_start	= before_start;
				}
			},
			order: [[ 0, 'desc' ]],
			aoColumnDefs: [
				{ "sType": "numeric-comma", "aTargets": [3,4] }
			],
			columns: [

				{ data: 'CLOSING_DATE',			name: 'CLOSING_DATE', className: "dt-body-center" },
				{ data: 'CLOSING_UPDATE_DATE', name : 'CLOSING_UPDATE_DATE', className: "dt-body-center"},
				{
					data: 'BUY_WEIGHT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(Math.floor(data)).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'SALE_WEIGHT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(Math.floor(data)).format(2);
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'BUY_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'SALE_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'COST',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TOTAL_GAIN',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: null,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-success' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
						}
						return data;
					},
					className: "dt-body-left",
					orderable:  false,

				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총매입수량
				buyQty = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				// 총매출수량
				saleQty = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				// 총매입금
				buyAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );


				//console.log(api.column( 3 ).data());
				// 총매출금
				saleAmt = api.column( 5 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				// 총지출금
				outAmt = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 총수익금
				cost	= api.column( 7 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );
				
				$( api.column( 2 ).footer() ).html(Number(Math.floor(buyQty)).format());
				$( api.column( 3 ).footer() ).html(Number(Math.floor(saleQty)).format());
				$( api.column( 4 ).footer() ).html(Number(buyAmt).format());
				$( api.column( 5 ).footer() ).html(Number(saleAmt).format());
				$( api.column( 6 ).footer() ).html(Number(outAmt).format());
				$( api.column( 7 ).footer() ).html(Number(cost).format());
				//$( api.column( 6 ).footer() ).html(Number(total_gain).format());

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		
		$('#reportrange span').on("change", function(){
			//oTable.draw() ;
		});
		/*
		$("body").on('DOMSubtreeModified', "#reportrange span", function () {
			console.log('ddddd');
			setTimeout(function(){ oTable.draw() ; }, 200);
		});
		*/
		$("body").on('click', ".applyBtn", function () {
			//console.log('ddddd');
			setTimeout(function(){ oTable.draw() ; }, 200);
		});

		
		$("input[name='start_date']").change(function(){
			//oTable.draw() ;
		});

		$("input[name='end_date']").change(function(){
			//oTable.draw() ;
		});
		
		$("#search-btn").click(function(){
			oTable.draw() ;
		});
		


		$(".simpleSetDtArea > button").on("click", function(e){

			end = moment();
			if( $(this).hasClass("today") ){
				start = end;
			}else if( $(this).hasClass("week")) {
				start = end.subtract('days', 7);

			}else if( $(this).hasClass("month")) {
				start = end.subtract('months', 1);

			}else if( $(this).hasClass("month6") ) {
				start = end.subtract('months', 6);

			}else if( $(this).hasClass("year")) {
				start = end.subtract('months', 12);
			}
			end = moment();

			$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
			$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

			oTable.draw() ;
		});



		// 상세페이지 이동?
		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/closing/closing/detail/" + data.CLOSING_DATE + "/" + data.CLOSING_DATE;
		});

		// 결산저장
		$("#btnSaveClosing").click(function(){
			if(confirm('결산을 저장하시겠습니까?')){

				$.getJSON('/closing/closing/setClosing', {
					_token			: '{{ csrf_token() }}'
					, CLOSING_DATE		: $("input[name='start_date']").val()
					, CLOSING_DATE_END	: $("input[name='end_date']").val()
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					if( data.result == "success" ){
						$(".content").prepend( getAlert('success', '성공', "결산이 저장되었습니다") ).show();
						oTable.draw() ;
					}
				}).error(function(xhr){
					var info = $('.edit_alert');
					info.hide().find('ul').empty();

					var error = jQuery.parseJSON(xhr.responseText);
					if( error.result == "DB_ERROR"){
						info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
					}else{
						for(var k in error.message){
							info.find('ul').append('<li>' + k + '</li>');
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					}
					info.slideDown();
				});
			}
		});

		// 기간별 정산표
		$("#btnList").click(function(){
			location.href = "/closing/closing/detailListArrange/" + $("input[name='start_date']").val() + "/" + $("input[name='end_date']").val();
		});
	});
</script>
@stop
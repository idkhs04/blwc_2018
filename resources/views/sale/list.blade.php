@extends('layouts.main')
@section('class','매출관리')
@section('title','일일판매관리')
@section('content')
<link rel="stylesheet" type="text/css" href="/static/css/sale/sale.css" />
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == '' ? 1 : app('request')->input('page') }}" />

			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력</button>
					<button type="button" class="btn btn-success" id="btnPdf"><i class="fa fa-file-pdf-o"></i> 판매일보</button>
					<button type="button" class="btn btn-success" id="btnSelectedPdf"><i class="fa fa-file-pdf-o"></i> 명세서선택출력</button>
					<button type="button" class="btn btn-info" id="btnAddSale" ><i class="fa fa-plus-circle"></i> 추가</button>
				</div>
			</div>
			
			<div class="col-md-2 col-xs-6 config_qty">
				<div class="form-control" >
					<label>전체</label>
					<input type='radio' name='srtQty' value='' checked="checked" />
					<label>kg</label>
					<input type='radio' name='srtQty' value='kg'  />
					<label>마리</label>
					<input type='radio' name='srtQty' value='qty' />
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 2.5px 10px; border: 1px solid #ccc; width: 100%">
					<label>판매일</label> 
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="custGrp">
					<option value='ALL' /> 그룹</option>
					@foreach( $custGrp as $c )
					<option value="{{$c->CUST_GRP_CD}}" >{{$c->CUST_GRP_NM}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body listMain">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="판매 목록" width="100%">
				<caption>일일판매조회</caption>
				<tfoot>
					<tr>
						<th width="15px"></th>
						<th width="40px"></th>
						<th width="80px"></th>
						<th width="80px"></th>
						<th width="110px"></th>
						<th width="20px"></th>
						<th width="55px"></th>
						<th width="55px"></th>
						<th width="55px"></th>
						<th width="55px"></th>
						<th width="55px"></th>
						<th width="246px"></th>
						<th width="auto"></th>
						<th width="auto"></th>
					</tr>
				</tfoot>
				<thead>
					<tr>
						<th class="check" width="15px"><input type="checkbox" name="ALL" value='0' id='chkALL'/></th>
						<th class="name">판매일</th>
						<th class="name">그룹명</th>
						<th class="name">거래업체</th>
						<th class="name">판매어종</th>
						<th class="name">수량</th>
						<th class="name">판매액</th>
						<th class="name">현금</th>
						<th class="name">미입금</th>
						<th class="name">카드</th>
						<th class="name">할인액</th>
						<th class="name">상세관리</th>
						<th class="name">할인내역</th>
						<th class="name">미수 수금</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<input type="hidden" name="hid_word" value="" />
</div>
<!-- 일일판매 입력 팝업 -->
<div class="modal fade" id="modal_sale_insert" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:10px 25px 0px 25px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>

				<h4><span class="glyphicon glyphicon-th-large"></span> 일일판매 입력 </h4>
				<div class="form-group text-center top_right_date">

					<label for="modal_sujo_sale_date" class="col-sm-4 control-label"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i> 판매일</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="modal_sujo_sale_date" placeholder="" name="modal_sujo_sale_date" >	
					</div>
				</div>

				<div class="form-group topBtnArea">
					<button type="button" class="btn btn-success pull-right btnGoCustTab" >
						<i class="fa fa-plus"></i> 업체추가 
					</button>

					<button type="button" class="btn btn-danger pull-right tab_close" >
						<span class="glyphicon glyphicon-remove"></span> 취소 
					</button>
					
				</div>
			</div>
			<div class="modal-body" style="padding:0px 25px;">
				<div class="box box-primary">
					<div class="box-body">
						<ul class="nav nav-tabs"id="tabContent">
							<li class="active firstTab"><a href="#details" data-toggle="tab">업체추가</a></li>
							<!--<li class="active"><a href="#details" data-toggle="tab">업체추가</a><i class='fa fa-close text-red'></i></li>-->
						</ul>
						<div class="tab-content" id="tab_area">
							<div class="tab-pane firstTab active" id="details">
								<div class="control-group">  
									<div class="box-header">
										<div class="col-md-3 col-xs-6">
											<label for="modal_sujo_sale_date" class="modal_control" ><i class='fa fa-circle-o text-aqua'></i> 그룹명</label>
											<select name="strSearchCust" id="strSearchCust">
											</select>
										</div>
										<!--
										<div class="col-md-3 col-xs-6">
											<label for="modal_sujo_sale_chk" class="modal_control" ><i class='fa fa-circle-o text-aqua'></i> 모두조회</label>
											<input type="checkbox" name="chkSearchCust"  id="modal_sujo_sale_chk"/>
										</div>
										-->
										<div class="col-md-3 col-xs-6">
											<div class="input-group">
												<label for="modal_sujo_sale_date" class="modal_control"><i class='fa fa-circle-o text-aqua'></i> 거래처</label>
												<input type="text" name="textSearchCust" placeholder="검색어 입력" class='cis-lang-ko' />
											</div>
										</div>

									</div>
									<table id="tblCustList" class="table table-bordered table-hover display nowrap" summary="일일판매 거래처 조회">
										<caption>일일판매 입력</caption>
										<thead>
											<tr>
												<th width="60px">그룹</th>
												<th width="60px">업체명</th>
												<th width="40px">대표자명</th>
												<th width="40px">전화번호</th>
												<th width="60px">사업자번호</th>
												<th width="30px">지역명</th>
												<th width="auto">선택</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div> 
						</div>
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 전체닫기 
				</button>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
		return this.flatten().reduce( function ( a, b ) {
			if ( typeof a === 'string' ) {
				a = a.replace(/[^\d.-]/g, '') * 1;
			}
			if ( typeof b === 'string' ) {
				b = b.replace(/[^\d.-]/g, '') * 1;
			}

			return a + b;
		}, 0 );
	});

	$(function () {
			
		var data_text = '';

		var arrTempUseCancel	= new Array();
		var arrTempUseUncl		= new Array();
		var tempSum = 0;
		
		var start = moment().startOf('month');
		var end = moment();
		start = end;
			
		var isBack		= localStorage["IS_BACK"];
		var sText		= '';
		var sCondition	= 'ALL';
		var saled_seq	= '';
		if( isBack == "Y"){
			start		= moment(localStorage["START_DATE"]);
			end			= moment(localStorage["END_DATE"]);

			$("select[name='custGrp'] option[value='" + localStorage["SEARCH_CONDITION"] + "']").prop("selected",true);
			localStorage["IS_BACK"] = 'N';
		}

		// 일일판매는 기본적으로 해당 달의 첫번째 달을 기준으로 조회
		// 초기값 세팅
		function cbSetDate(start, end) {
			
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');

			localStorage["START_DATE"]	= start.format('YYYY-MM-DD'); 
			localStorage["END_DATE"]	= end.format('YYYY-MM-DD');
			localStorage["IS_BACK"]		= "N";
		}

		// 탭닫기
		function closeTab(CUST_MK){
			$("#tab_area > #" + CUST_MK).empty();
			$("#tabContent > li > a[href='#" + CUST_MK + "']").parents('li').remove();
			$(".tab-pane#" + CUST_MK).remove();
			$("#tabContent > li:first > a").trigger("click");
			
		}

		// 입고정보 : 입고일 
		$("#modal_sujo_sale_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		});
		
		function getTotal(){
			$.getJSON('/sale/sale/getTotal', {
				_token			: '{{ csrf_token() }}',
				START_DATE		: $("input[name='start_date']").val(),
				END_DATE		: $("input[name='end_date']").val(),
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				var sumQty = "";
				
				if( isConfigQty() ){
					sumQty = data.TQTY + "(KG : " + data.KG_SUM + " / 마리 : " + data.QTY_SUM + ")"
				}else{
					sumQty = data.TQTY + "(KG : " + data.KG_SUM + ")"
				}
				$(".headerSumQty ul li:nth-child(1) span.sum").html(sumQty);
				$(".headerSumQty ul li:nth-child(2) span.sum").html(data.TSALE);
				$(".headerSumQty ul li:nth-child(3) span.sum").html(data.TCASH);
				$(".headerSumQty ul li:nth-child(4) span.sum").html(data.TUNCL);
				
			}).error(function(xhr,status, response) {
				
			});
		}

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",			
			/*
			ranges: {
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
			},
			*/
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 300,		// 기본 300개 조회 설정
			bLengthChange: false,
			bInfo : false,
			fixedHeader : { footer:false, header:false},
			ajax: {
				url: "/sale/sale/listData",
				data:function(d){
					d.srtCondition	= isBack == "Y" ? localStorage["SEARCH_CONDITION"] : $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.srtQty		= $("input[name='srtQty']:checked").val();
					d.custGrp		= $("select[name='custGrp'] option:selected").val()
				}
			},
			order: [[ 1, 'DESC' ], [ 11, 'DESC' ]],
			columns: [
				{
					data: "SEQ",
					name: "SM.SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					orderable:      false,
					className: "dt-body-center"
				},
				{ data: 'WRITE_DATE', name: 'SM.WRITE_DATE',className: "dt-body-center" },
				{ data: 'CUST_GRP_NM', name: 'CUST_GRP.CUST_GRP_NM',className: "dt-body-left" },
					
				{
					data:  "FRNM",
					name: "C.FRNM", 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== null){

								return data.cut(14);
							}else{
								return "";
							}
						}
						return data;
					},
					className: "dt-body-left"
				},
				{ data: "PIS",  name: 'P.PIS_NM', className: "details-control"},
				{ 
					data: 'SUMQTY', 
					name: 'SD.SUMQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'TOTAL_SALE_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'CASH_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'UNCL_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'CARD_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'SALE_DISCOUNT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: "SEQ",
					name: "SM.SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-success btnGoDetail' ><i class='fa fa-arrow-circle-right'></i> 상세관리 </button>" + 
								  " <button class='btn btn-success btnPdfDetail' ><i class='fa fa-file-pdf-o'></i> 거래명세서 </button>" + 
								  " <button class='btn btn-success btnGoUncl' ><i class='fa fa-arrow-circle-right'></i> 미수 </button>";
						}
						return data;
					},
					className: "dt-body-left",
					/*orderable:  false,*/
				},
				{ 
					data: 'SALE_DISCOUNT_REMARK', 
					name: 'SM.SALE_DISCOUNT_REMARK', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return data;
						}
						return data;
					},
					className: "dt-body-left"
				},
				{ 
					data: 'UNCL_LIST', 
					name: 'UNCL_LIST', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== null){
								if( data.length > 1){
									return data;
								}else{
									return '';
								}
								
							}
						}
						return data;
					},
					className: "dt-body-left"
				}
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var qtys = 0;
				var j = 0;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총수량
				qutySum = api.column( 5 ).data().reduce( function (a, b) {
						
					size = 0;
					if( size == "마리" && isConfigQty() ) {
						qtys += intVal(b);
						return intVal(a);
					}else{
						return intVal(a) + intVal(b);
					}
					
					return intVal(a) + intVal(b);}, 0 );
				
				qutySum = qutySum.toFixed(2);
				if( isConfigQty()){
					$( api.column( 5 ).footer() ).html("Kg : " + Number(qutySum).format() + " / 마리:" + Number(qtys).format() );
				}else{
					$( api.column( 5).footer() ).html( Number(qutySum).format() );
				}

				// 총판매금액
				saleSum = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 6 ).footer() ).html(Number(saleSum).format() );
				
				// 총 입금금액
				inputSum = api.column( 7).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 7 ).footer() ).html(Number(inputSum).format() );

				// 총 미입금액
				UnclSum = api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 8 ).footer() ).html(Number(UnclSum).format() );

				// 총 카드금액
				carSum = api.column( 9 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 9 ).footer() ).html(Number(carSum).format() );

				// 총 할인금액
				disCSum = api.column( 10 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 10 ).footer() ).html(Number(disCSum).format() );

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblList tbody').on('click', 'td.details-control, .details-control2', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format(row.data()) ).show();
				tr.addClass('shown');
			}
		});

		// 전체 체크 
		$("#chkALL").on("click", function(){
			if( $(this).is(":checked")){
				$("#tblList tbody td > input[type=checkbox]").prop("checked", true);
			}else{
				$("#tblList tbody td > input[type=checkbox]").prop("checked", false);
			}
		});

		function format ( d ) {
			
			var arrPisList = d.PIS_LIST.split('/');			
			var strhtml = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:35%" class="detailSaleData table table-bordered table-hover display nowrap">'+
					'<thead><tr>'+
						'<th width="50px">판매어종</th>' +
						'<th width="50px">규격</th>' +
						'<th width="50px">수량</th>' +
						'<th width="50px">단가</th></tr></thead><tbody>';
						
					
			if( arrPisList.length > 0){
				for(var i=0; i<arrPisList.length; i++){

					var expArrPisList = arrPisList[i].split(" ");
					strhtml += '<tr>';
					strhtml += '<td class="blue dt-body-center">'+expArrPisList[0]+'</td>';
					strhtml += '<td class="blue dt-body-center">'+expArrPisList[1]+'</td>';
					strhtml += '<td class="dt-body-right">'+expArrPisList[2]+'</td>';
					strhtml += '<td class="dt-body-right">'+expArrPisList[3]+'</td>';
					strhtml += '</tr>';
				}
			}

			strhtml += '</tbody></table>';
			return strhtml;
		}
		

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
			localStorage["SEARCH_CONDITION"] = $("select[name='custGrp'] option:selected").val();
		});

		$("select[name='srtCondition']").on("change", function(){
			oTable.draw();
		});

		$("input[name='start_date']").on("change", function(){
			oTable.draw();
		});

		$("input[name='end_date']").on("change", function(){
			oTable.draw();
		});
		$("input[name='srtQty']").on("click", function(){
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw();
			localStorage["SEARCH_CONDITION"] = $("select[name='custGrp'] option:selected").val();
		});

		$("select[name='custGrp']").click(function(){
			oTable.draw();
			localStorage["SEARCH_CONDITION"] = $("select[name='custGrp'] option:selected").val();
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 상세페이지 이동
		$('#tblList tbody').on( 'click', 'button.btnGoDetail', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/sale/sale/detailList/" + data.CUST_MK + "/" + data.SEQ + "/" + data.WRITE_DATE;
		});

		// 미수페이지 이동
		$('#tblList tbody').on( 'click', 'button.btnGoUncl', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/uncl/uncl/detail2/" + data.CUST_MK ;
		});
		

		// 탭닫기(현재 보여지는 탭 삭제)
		$("#modal_sale_insert").on("click", ".tab_close", function(){
			var tab_id = $("div.tab-pane.active").attr("id");
			//console.log(tab_id);
			if( tab_id != 'details'){
				closeTab(tab_id);	
			}
		});

		// 전체닫기
		// 기존열린 탭들(거래처선택 탭제외)은 모두 삭제
		$('#modal_sale_insert').on('hidden.bs.modal', function () {
			$("#tabContent > li:first > a").trigger("click");
			$("#tabContent > li:not(.firstTab)").remove();
			$("#tab_area > div:not(.firstTab)").remove();
			
			var sale_date = $("#modal_sujo_sale_date").val();

			$("#reportrange input[name='end_date']").val(sale_date);
			oTable.draw();
		})

		// 일일판매 입력
		$("#btnAddSale").click(function(){
			
			$("#modal_sujo_sale_date").val(moment().format('YYYY-MM-DD'));
			$("#modal_sale_insert").modal({show:true, backdrop:'static'}).draggable({
				handle: ".modal-header",
				cursor: 'move',
			}); 
			$('.modal >.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');

			$.getJSON('/sale/sale/getCustGrp', {
					_token	: '{{ csrf_token() }}'
					
				}, function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					var $custGrp = $("select[name='strSearchCust']");
					$custGrp.empty();
					$custGrp.append("<option value='' >전체</option>");

					$.each(data, function(){
						$custGrp.append('<option value="' + this.CUST_GRP_CD +'">' + this.CUST_GRP_NM + '</option>');
					});
				}
			);

			var cTable = $('#tblCustList').DataTable({
				processing: true,
				serverSide: true,
				retrieve: true,
				search:false,
				iDisplayLength: 100,		// 기본 100개 조회 설정
				bLengthChange: false,
				bInfo:false,
				fnDrawCallback : function(){
					
					setTimeout(function(){
						$("input[name='textSearchCust']").focus();
					},800);
				},
				ajax: {
					url: "/sale/sale/getCustList",
					data:function(d){
						d.textSearch	= $("input[name='textSearchCust']").val();
						d.CHK_ALL		= $("input[name='chkSearchCust']").is(":checked") ? "1" : "0";
						d.srtCondition	= $("select[name='strSearchCust'] option:selected").val();
					}
				},
				order: [[ 1, 'asc' ]],
				columns: [
					{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM', className: "dt-body-center"  },
					{
						data:   "FRNM",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								if( data !== null){

									return data.cut(14);
								}else{
									return "";
								}
							}
							return data;
						},
						className: "dt-body-left"
					},

					{ data: 'RPST',  name: 'RPST' },
					{ data: 'PHONE_NO',  name: 'PHONE_NO' , className: "dt-body-center" },
					
					{
						data:   "ETPR_NO",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								if( data !== null && data != "null" && data.length == 10 ) {
									return data.substr(0, 3) + '-' + data.substr(3, 2) + '-' + data.substr(5, 5);
								}else{
									return data;
								}
							}
							return data;
						},
						className: "dt-body-left"
					},

					{ data: 'AREA_NM',  name: 'AREA_NM', className: "dt-body-center" },
					{
						data:   "CUST_GRP_NM",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								return "<button class='btn btn-success' ><i class='fa fa-location-arrow'></i> 선택</button>";
							}
							return data;
						},
						className: "dt-body-left"
					},
					{ data: 'CUST_MK',  name: 'CUST_MK', visible:false },
				],
				"searching": true,
				"paging": true,
				"autoWidth": true,
				"oLanguage": {
					"sLengthMenu": "조회수 _MENU_ ",
					"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
					"sProcessing": "현재 조회 중입니다",
					"sEmptyTable": "조회된 데이터가 없습니다. 거래처코드로 이동하여 판매할 거래처를 추가하세요 ",
					"sZeroRecords": "조회된 데이터가 없습니다. 거래처코드로 이동하여 판매할 거래처를 추가하세요",
					"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
					"oPaginate": {
						"sFirst": "처음",
						"sLast": "끝",
						"sNext": "다음",
						"sPrevious": "이전"
					}
				}
			});

			// 업체 선택 클릭
			$('#tblCustList tbody').on( 'click', 'button', function () {
				var data = cTable.row( $(this).parents('tr') ).data();
				
				$("#tabContent li > a[href='#" + data.CUST_MK+ "']").tab('show');
				

				if( $("#tabContent li > a[href='#" + data.CUST_MK +"']").length == 0){
					$("#tabContent").append("<li><a href='#" + data.CUST_MK + "' data-toggle='tab'>" + data.FRNM+ "</a></li>");
					$("#tab_area").append("<div class='tab-pane' id='" + data.CUST_MK + "'>" + data.FRNM + "</div>");
					
					$("select[name='strSearchCust'] option:eq(0)").prop("selected", true);
					$("input[name='textSearchCust']").val("");
		
					// 수조목록을 가져온다.
					getTable(data.CUST_MK);
					cTable.draw();
				}
				$("#tabContent li > a[href='#" + data.CUST_MK+ "']").tab('show');
				setButtonVisible();
			});

			$("select[name='strSearchCust']").on("change", function(){
				cTable.draw() ;
			});
				
			$("input[name='textSearchCust']").on("keyup", function(){
				cTable.search($(this).val()).draw() ;
			});

			$("input[name='chkSearchCust']").on("click", function(){
				cTable.search($(this).val()).draw() ;
			});
		});

		// 업체선택을 하게 되면 수조목록+재고정보 목록을 가져옴
		function getTable(CUST_MK){
			
			$("#tab_area > div#" + CUST_MK + "").load("getTblTempl", "id=" + CUST_MK , function(){
				
				var eachTable = $("#tblCust" + CUST_MK).DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					iDisplayLength: 100,		// 기본 100개 조회 설정
					bLengthChange: false,
					bInfo : false,
					fnDrawCallback: function () {

						$("#tblCust" + CUST_MK + " > tbody > tr:nth-child(1) > td > input.user_qty").focus();

						// 엔터키 => 판매
						$("#tblCust" + CUST_MK).keydown(function(e){
							if( e.which == 13){
								$("#" + CUST_MK + " .btnSale").trigger("click");
							}
						});
						
						// 중량 입력 이벤트
						$("#tblCust" + CUST_MK + " > tbody > tr > td > input.user_qty").keydown(function(e){
							
							switch (e.which){
								case 37:    //left arrow key
									break;
								case 38:    //up arrow key
									$(this).parents("tr").prev().find("input.user_qty").focus();
									break;
								case 39:    //right arrow key
									$(this).parents("td").next().find("input").focus();
									break;
								case 40:    //bottom arrow key
									$(this).parents("tr").next().find("input.user_qty").focus();
									break;
							}
						});


						// 판매단가
						$("#tblCust" + CUST_MK + " > tbody > tr > td > input.user_uncn").keydown(function(e){
							
							switch (e.which){
								case 37:    //left arrow key
									$(this).parents("td").prev().find("input.user_qty").focus();
									break;
								case 38:    //up arrow key
									$(this).parents("tr").prev().find("input.user_uncn").focus();
									break;
								case 39:    //right arrow key
									$(this).parents("td").next().next().find(".btn.minus").focus();
									break;
								case 40:    //bottom arrow key
									$(this).parents("tr").next().find("input.user_uncn").focus();
									break;
							}

						});

						// - + 버튼
						$("#tblCust" + CUST_MK + " > tbody > tr > td > .btn.btnCalc").keydown(function(e){
							
							switch (e.which){
								case 13:	// enter key
									e.preventDefault();
									break;
								case 32:	// space key
									$(e).trigger("click");
									break;
								case 37:    //left arrow key
									if( $(this).hasClass("minus") ){
										$(this).parents("tr").find("input.user_uncn").focus();
									}else{
										$(this).prev(".btn").focus();
									}
									break;
								case 38:    //up arrow key
									e.preventDefault();
									$(this).parents("tr").prev().find(".btn.minus").focus();
									break;
								case 39:    //right arrow key
									
									if( $(this).hasClass("plus2") ){
										$(this).parents("tr").next().find("input.user_qty").focus();
									}else{
										$(this).next(".btn").focus();
									}
									break;

									$(this).next(".btn").focus();
									break;
								case 40:    //bottom arrow key
									e.preventDefault();
									$(this).parents("tr").next().find(".btn.minus").focus();
									break;
							}
						});

					},
					initComplete : function(settings, json){
					
						$(".tblCust" + CUST_MK + "_footer input[type='text']").val( Number(tempSum).format());
						// 저녁 변수 초기화 시켜줌
						arrTempUseCancel	= new Array();
						arrTempUseUncl		= new Array();
						tempSum = 0;
						
					},
					fnRowCallback: function(nRow, nData){

						if( arrTempUseCancel.length != 0){
							
							for(var i=0; i<arrTempUseCancel.length; i++){
								
								if( nData.SUJO_NO == arrTempUseCancel[i][0]){
									$(nRow).find('td input.user_qty').val(arrTempUseCancel[i][1]);
									$(nRow).find('td input.user_uncn').val(arrTempUseUncl[i][1]);
									
									// 500단위로 절사함(2016-10-31) 
									var T = Number( '1e' + 1 );
									
									a = Math.round( removeCommas(arrTempUseCancel[i][1]) * removeCommas(arrTempUseUncl[i][1] ) * T ) / T;
									a += 500;
									a = Math.floor( a / 1000 ) * 1000;

									$(nRow).find('td input.sum ').val( Number(a).format() );

									// 합계 증감
									tempSum += parseFloat(a);
								}								
							}	
						}
						if( nData.IS_CHECK == "Y" && nData.CONF_CHK_SUJO == "Y"){
							
							$(nRow).addClass("cselected");
						}
						
					},
					ajax: {
						url: "/buy/sujo/listData",
						data:function(d){
							d.HAS_EMPTY = "N";
						}
					},
					
					columns: [
						{
							data:   "SUJO_NO",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<a href='#' data-toggle='dropdown' aria-expanded='true' class='dropdown-toggle btn btn-info btn-md btnMemo btn" + data+ "' id='s" +data+ "'><span class='glyphicon glyphicon-pencil'></span></a>" ;
								}
								return data;
							},
							className: "dt-body-center"
						},
						{ data: 'SUJO_NO', name: 'SUJO_NO' , className: "dt-body-center"},
						{
							data:   "CUST_LIST",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									if( data !== null ){
										return data.cut(14);
									}
								}else{
									return data;
								}
							},
							defaultContent :  "-",
							className: "dt-body-left"
						},
						{ 
							data: 'ORIGIN_NM',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return data;
								}
								return data;
							},
							className: "dt-body-center"
						},
						
						{ 
							data: 'INPUT_DATE',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return data;
								}
								return data;
							},
						},

						{ 
							data: 'SIZES',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return data;
								}
								return data;
							},
							className: "dt-body-center red"
						},

						{ 
							data: 'PIS_NM',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return data;
								}
								return data;
							},
							className: "dt-body-center blue"
						},
						{ 
							data: 'QTY',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return Number(data).format();
								}
								return data;
							},
							className: "dt-body-right green"
						},
						{ 
							data: 'ORIGIN_CD',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<input type='text' class='editor-active user_qty numeric' value='' data-orgincd='" + row.ORIGIN_CD+"' data-pismk='" + row.PIS_MK+ "' />";
								}
								return data;
							},
						},
						{ 
							data: 'SALE_UNCS',		
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<input type='text' class='editor-active ucnc numeric user_uncn' value='" + Number(data).format() +"' >";
								}
								return data;
							},
						},
						{ 
							data:   null,
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<input type='text' class='editor-active sum numeric' value='0' >";
								}
								return data;
							},
							className: "dt-body-center qtySum"
						},
						{ 
							data:   null,
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return  "<button class='btn minus btn-info btnCalc'>-500</button>" +
											"<button class='btn minus2 btn-info btnCalc'>-1,000</button>" + 
											"<button type='button' class='btn plus btn-danger btnCalc'>+500</button>" + 
											"<button type='button' class='btn plus2 btn-danger btnCalc'>+1,000</button>" ;
								}
								return data;
							},
							className: "dt-body-left"
						},
						
					],
					"searching": false,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$.getJSON('/sale/sale/getTotalUncl', {
					UNCL_CUST_MK	: CUST_MK,

				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					
					if( $.isNumeric(parseInt(data.TOTAL_UNCL))){
						$("#tblCust" + CUST_MK + "Sale .unlc_area span.TOTAL_UNCL").html(Number(data.TOTAL_UNCL).format());
					}else{
						$("#tblCust" + CUST_MK + "Sale .unlc_area span.TOTAL_UNCL").html("0");
						
					}
				}).error(function(xhr,status, response) {
					$("#tblCust" + CUST_MK + "Sale .unlc_area span.TOTAL_UNCL").html("0");
				});

				// 판매시 선택 Row 색표시
				eachTable.on( 'click', 'tr', function () {
					
					if ( $(this).hasClass('selected') ) {
						$(this).removeClass('selected');						
					}
					else {
						eachTable.$('tr.selected').removeClass('selected');
						$(this).addClass('selected');
					}
				});

				// (+, -) 버튼 클릭
				eachTable.on("click", "tbody td button.btn", function(e){
					
					var ucnc = parseInt( removeCommas($(this).parents('tr').find("td> input.ucnc").val()));

					if( $(this).hasClass("plus")) {	
						$(this).parents('tr').find("td> input.ucnc").val( Number(ucnc + 500).format() );
						$(this).parents('tr').find("td> input.ucnc").trigger("keyup");
					}else if( $(this).hasClass("minus")) {
						$(this).parents('tr').find("td> input.ucnc").val( Number(ucnc - 500).format() );
						$(this).parents('tr').find("td> input.ucnc").trigger("keyup");
					}

					if( $(this).hasClass("plus2")) {	
						$(this).parents('tr').find("td> input.ucnc").val( Number(ucnc + 1000).format() );
						$(this).parents('tr').find("td> input.ucnc").trigger("keyup");
					} else if( $(this).hasClass("minus2")) {	
						$(this).parents('tr').find("td> input.ucnc").val( Number(ucnc - 1000).format() );
						$(this).parents('tr').find("td> input.ucnc").trigger("keyup");
					}
				});

				function Floor(n, pos) { var digits = Math.pow(10, pos); var num = Math.floor(n * digits) / digits; return num.toFixed(pos); }
				
				// 값 변경 => 합계 계산
				eachTable.on("keyup", "tbody td input", function(e){
					
					// 빈수조 체크(1차적으로 어종/규격 존재만 체크)
					var pis_mk	= $(this).parents('tr').find("td:nth-child(7)").text();
					var sizes	= $(this).parents('tr').find("td:nth-child(6)").text();

					if( pis_mk == "" || sizes == ""){
						$(this).parents('tr').find("td > input.user_qty").val(0);
						$(this).parents('tr').find("td input.sum").val(0);
					}

					var user_qty	= removeCommas( $(this).parents('tr').find("td > input.user_qty").val());
					var ucnc		= removeCommas( $(this).parents('tr').find("td > input.ucnc").val());
					var sum			= ucnc * user_qty;

					// 500단위로 절사함(2016-10-31) 
					var T = Number( '1e' + 1 );
					a = Math.round( ( user_qty * ucnc ) * T ) / T;
					a += 500;
					a = Math.floor( a / 1000 ) * 1000;
					
					// 해당 Row 합계
					$(this).parents('tr').find("td:nth-child(11) > input").val( Number(a).format());

					var totalSum = 0;
					$("#tblCust" + CUST_MK + " tbody tr" ).each(function(){
						totalSum += parseInt( removeCommas($(this).find("td input.sum").val()) );
					});
					
					// 해당 테이블 전체합계
					$("input.tblCust" + CUST_MK).val(Number(totalSum).format());

				});
				
				// 판매 클릭

				$("#" + CUST_MK + " .btnSale").on("click", function(){
					$(".tblCust" + CUST_MK + "Sale").hide();

					var anSelected = fnGetSelected(CUST_MK);
					var info = $(".dvAlert");
					info.empty();

					if( anSelected === null){
						info.append("<div class='alert alert-warning fade in'>" +
										"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
										"<strong>Warning!</strong> 입력한 판매내역이 없습니다." +
									"<div>");
						return;
					}else{
						$("#tblCust" + CUST_MK + "_wrapper").slideUp();
						$(".tblCust" + CUST_MK + "_footer").slideUp();
						
						$("#resultSale" + CUST_MK).slideDown();
						$("#tblCust" + CUST_MK + "Sale > div.tbl_area ").empty();
						
						// 선택된 데이터 테이블이 로드 된 후 합계 계산
						$("#tblCust" + CUST_MK + "Sale > div.tbl_area ").html(anSelected);

						//console.log(anSelected);
						$.getJSON('/sale/sale/getTotalUncl', {
							UNCL_CUST_MK	: CUST_MK,
						}).success(function(xhr){

							var data = jQuery.parseJSON(JSON.stringify(xhr));
							if( data !== null && $.isNumeric(parseInt(data.TOTAL_UNCL))){
								$("#tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html(Number(data.TOTAL_UNCL).format());
								$(".tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html(Number(data.TOTAL_UNCL).format());
							}else{
								$("#tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html(0);
								$(".tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html(0);	
							}
							
						}).error(function(xhr,status, response) {
							$("#tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html("0");
							$(".tblCust" + CUST_MK + "Sale > div.unlc_area span.TOTAL_UNCL").html("0");
						});

						var qtySum = 0;
						
						$("#tblCust" + CUST_MK + "Sale > div.tbl_area > table#tblCust" + CUST_MK).find("tbody tr").each(function(){
							qtySum += parseInt( removeCommas( $(this).find("td > input.sum").val()));
						});
						
						$("#resultSale" + CUST_MK + " input[name='txtSumSale']").val(Number(qtySum).format());
						//$("#resultSale" + CUST_MK + " input[name='txtCach']").val(0);
						$("#resultSale" + CUST_MK + " input[name='txtUnprov']").val(Number(qtySum).format());
						
						$("#resultSale" + CUST_MK + " input[name='txtSumSaleAll']").val(Number(qtySum).format());
					}
				});

				// 취소 클릭
				$("#" + CUST_MK + " #btnCancelBack").on("click", function(){
					
					var FRNM = $("#tabContent > li.active > a").text();
					$(".tab-pane.active .tbl_area #tblCust" + CUST_MK + " tbody tr").each(function(i){
						arrTempUseCancel.push( [$(this).find("td:eq(0)").text(), $(this).find("td:eq(7) > input").val()] );
						arrTempUseUncl.push( [$(this).find("td:eq(0)").text(), $(this).find("td:eq(8) > input").val()] );
					});
					
					$("#tabContent li.active").remove();
					$(".tab-pane#" + CUST_MK).remove();
			
					if( $("#tabContent li > a[href='#" + CUST_MK +"']").length == 0){
						$("#tabContent").append("<li><a href='#" + CUST_MK + "' data-toggle='tab'>" + FRNM+ "</a></li>");
						$("#tab_area").append("<div class='tab-pane' id='" + CUST_MK + "'>" + FRNM + "</div>");
						
						// 수조목록을 가져온다.
						getTable(CUST_MK);
					}
					$("#tabContent li > a[href='#" + CUST_MK+ "']").tab('show');
					

				});

				// 판매화면으로 이동 (판매계속)
				$("#endResultSale" + CUST_MK + " .btnGoSale").click(function(){
					var FRNM = $("#tabContent > li.active > a").text();

					$("#tabContent li.active").remove();
					$(".tab-pane#" + CUST_MK).remove();

					if( $("#tabContent li > a[href='#" + CUST_MK +"']").length == 0){
						$("#tabContent").append("<li><a href='#" + CUST_MK + "' data-toggle='tab'>" + FRNM+ "</a></li>");
						$("#tab_area").append("<div class='tab-pane' id='" + CUST_MK + "'>" + FRNM + "</div>");
						
						// 수조목록을 가져온다.
						getTable(CUST_MK);
					}
					$("#tabContent li > a[href='#" + CUST_MK+ "']").tab('show');

				});

				
				// 판매저장 클릭
				$("#" + CUST_MK + " #btnSaveSale").on("click", function(){
					
					//console.log(CUST_MK + 'sale_Click');
					
					$.blockUI({ 
						baseZ: 999999,
						message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
						css : {
							backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
							color: '#000000', border: '0px solid #a00' //테두리 없앰
						},
						onBlock: function() { 

							var data = fnSetTableToArray( "#tblCust" + CUST_MK + "Sale > div.tbl_area > table#tblCust" + CUST_MK );
							
							$.getJSON('/sale/sale/setSaleInfoInsert', {
								_token					: '{{ csrf_token() }}'
								, 'WRITE_DATE'			: $("#modal_sujo_sale_date").val()
								, 'CUST_MK'				: CUST_MK
								, 'CASH_AMT'			: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtCach']").val())
								, 'CHECK_AMT'			: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtSumSale']").val())
								, 'CARD_AMT'			: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtCard']").val())
								, 'UNCL_AMT'			: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtUnprov']").val())
								, 'TOTAL_SALE_AMT'		: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtSumSaleAll']").val())
								, 'SALE_DISCOUNT'		: removeCommas( $("#resultSale" + CUST_MK + " input[name='txtDicount']").val())
								, 'SALE_DISCOUNT_REMARK': $("#resultSale" + CUST_MK + " input[name='txtDicountText']").val()
								, 'DETAIL'				: data,

							}).success(function(xhr){

								// 판매완료 메시지
								var data = jQuery.parseJSON(JSON.stringify(xhr));
								// 지금 판매한 seq 가져오기
								
								$("#endResultSale" + CUST_MK + " input[name='saled-seq']").val(data.seq);
								$("#endResultSale" + CUST_MK + " input[name='saled-write_dt']").val(data.write_date);
								//console.log(data.data);
								
								var info = $(".gvSuccess");
								info.empty();

								info.append("<div class='alert alert-success fade in'>" +
												"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
												"<strong>Success!</strong> 판매 완료되었습니다." +
											"<div>");

								$("#tblCust" + CUST_MK + "Sale").slideUp();
								$("#resultSale" + CUST_MK).slideUp();
								$("#endResultSale" + CUST_MK).show();

								// 판매완료되면 탭 색깔 변경
								$("ul#tabContent > li > a[href='#"+CUST_MK+"'] ").css({
									"background-color"	: "#00a65a",
									"color"				: "white",
								});

								$.getJSON('/sale/sale/getTotalUncl', {
									UNCL_CUST_MK	: CUST_MK,
								}).success(function(xhr){
									
									var data = jQuery.parseJSON(JSON.stringify(xhr));
									if( data !== null && $.isNumeric(parseInt(data.TOTAL_UNCL))){
										$("#endResultSale" + CUST_MK + " input[name='UnprovResult']").val(Number(data.TOTAL_UNCL).format());
									}else{
										$("#endResultSale" + CUST_MK + " input[name='UnprovResult']").val("0");
									}
									
								}).error(function(xhr,status, response) {
									$("#endResultSale" + CUST_MK + " input[name='UnprovResult']").val("0");
								});
								$.unblockUI();

							}).error(function(xhr,status, response) {
								
								var info = $(".dvAlert");
								info.empty();

								info.append("<div class='alert alert-warning fade in'>" +
												"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
												"<strong>Warning!</strong> 판매내역 저장이 실패되었습니다" +
											"<div>");
								$.unblockUI();
							});
						}
					});
				});

				// 판매완료 후 거래명세서 출력 버튼
				$("#endResultSale" + CUST_MK + " .btnPdfSale2").click(function(){
					var width=740;
					var height=720;
					var saled_seq	= $("#endResultSale" + CUST_MK + " input[name='saled-seq']").val();
					var write_date	= $("#endResultSale" + CUST_MK + " input[name='saled-write_dt']").val();
					var cust_mk		= CUST_MK;
					var url = "/sale/sale/PdfDetail/" + cust_mk + "/" + saled_seq + "/" + write_date;
					getPopUp(url , width, height);
				});

				$("#resultSale" + CUST_MK + " input[type='text']").keyup(function(){fnSetCalculationSale(CUST_MK)});

				// 판매저장 전 현금/미수금/카드정보 수정
				function fnSetCalculationSale(CUST_MK){
					var txtSumSale		= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtSumSale]").val() );		// 판매금액합계
					var txtCach			= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtCach]").val() );		// 현금입금
					var txtCard			= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtCard]").val() );		// 카드입금
					var txtDicount		= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtDicount]").val() );		// 할인금액
					var txtUnprov		= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtUnprov]").val() );		// 미수금
					var txtDicountText	= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtDicountText]").val() );	// 할일내역
					var txtSumSaleAll	= removeCommas( $("#resultSale" + CUST_MK + " input[name=txtSumSaleAll]").val() );	// 전체합계
				
					txtSumSaleAll	= txtSumSale - txtDicount;
					txtUnprov		= txtSumSaleAll - txtCach - txtCard;
					
					$("#resultSale" + CUST_MK + " input[name=txtSumSaleAll]").val(Number(txtSumSaleAll).format());
					$("#resultSale" + CUST_MK + " input[name=txtUnprov]").val(Number(txtUnprov).format());
				}
				
				// 일일판매했떤 항목만 복사해서 다시 테이블형태로 가져오기
				function fnGetSelected( CUST_MK)
				{	
					var tempTable = $( "#tblCust" + CUST_MK).clone();
					
					$(tempTable).find("tbody tr").each(function(){
						if($(this).find("input.user_qty").val() <= 0){
							$(this).remove();
						}
					});
					
					if( $(tempTable).find("tbody tr").length == 0 ){
						return null;
					}
					
					$(tempTable).find("td:nth-child(1), th:nth-child(1), td:nth-child(12), th:nth-child(12)").remove();

					$(tempTable).find("th").css("width", "auto");

					$(tempTable).find("td > input[type='text']").attr("readonly", 'readonly');
					$(tempTable).find("td > input[type='text']").attr("readonly", 'readonly');
				
					// 수정 : 2016-11-23
					// 100개 선택되도록
					return tempTable.dataTable({iDisplayLength: 100});
				}
		
				// 판매했던 항목들에 대해서 Ajax 처리를 위해 Array로 변경
				// 원산지코드와, 어종코드도 추가로 Array에 삽입
				function fnSetTableToArray(table){
					
					var $rows= $(table + " tbody tr");
					var data = [];
					var orgin_cd, pismk;

					$rows.each(function(row, v) {
						data[row] = [];

						$(this).find("td").each(function(cell, v) {
							
							if( $(this).find("input[type='text']").length > 0 ){

								if( $(this).find( $(this).find("input[type='text']") ).hasClass("numeric") ){
									data[row][cell] = removeCommas( $(this).find("input[type='text']").val());
								}else{
									data[row][cell] = $(this).find("input[type='text']").val() ;
								}

								//console.log($(this).find("input[type='text']").attr("data-orgincd"));
								if( $(this).find("input[type='text']").attr("data-orgincd") !== undefined ){
									orgin_cd = $(this).find("input[type='text']").attr("data-orgincd");
								}
								if( $(this).find("input[type='text']").attr("data-pismk") !== undefined ){
									pismk = $(this).find("input[type='text']").attr("data-pismk");
								}
							}else{
								data[row][cell] = $(this).text();
							}
						});
						data[row][11] = orgin_cd;
						data[row][12] = pismk;
					});
					return data;
				}
			});
		}
		
		// 업체추가 탭으로 바로 이동
		$("#modal_sale_insert").on("click", ".btnGoCustTab", function(){
			$("#tabContent .firstTab > a").trigger("click");
			$("input[name='textSearchCust']").focus();
			$("button.tab_close").css("display", "none");
		});

		$("#modal_sale_insert").on("click", "#tabContent > li", function(){
			
			if( $(this).hasClass("firstTab") ){
				$("input[name='textSearchCust']").focus();	
				$("button.tab_close").css("display", "none");
			}else{
				$("button.tab_close").css("display", "block");
			}
			
		});
		
		function setButtonVisible(){
			if( $("#tabContent > li.active").hasClass("firstTab")){
				$("button.tab_close").css("display", "none");
				$("input[name='textSearchCust']").focus();
			}else{
				$("button.tab_close").css("display", "block");
			}
		}

		// 로그조회 이동
		$("#btnLog").click(function(){
			location.href = '/sale/sale/log';
		});
		
		// 판매일보 pdf 출력
		$("#btnPdf").click(function(){
			
			var width=740;
			var height=720;
			
			var start_date	= $("#reportrange input[name='start_date']").val() ;
			var end_date	= $("#reportrange input[name='end_date']").val() ;
			
			var search	= $("input[name='textSearch']").val();
			var custGrp	= $("select[name='custGrp'] option:selected").val();
			console.log(search, custGrp);
			var url = "/sale/sale/Pdf/" + start_date + "/" + end_date + "/" + custGrp + "/" + search;
			
			getPopUp(url , width, height);

		});

		// 거래명세서 출력
		$('#tblList tbody').on( 'click', ".btnPdfDetail", function(){
			
			var width=740;
			var height=720;

			var data = oTable.row( $(this).parents('tr') ).data();
			var cust_mk		= data.CUST_MK;
			var seq			= data.SEQ;
			var write_date	= data.WRITE_DATE;
			
			var url = "/sale/sale/PdfDetail/" + cust_mk + "/" + seq + "/" + write_date;
			getPopUp(url , width, height);

		});
		// 거래명세서 선택출력 한꺼번에 출력
		// 조아수산 요청( 2017-03-13)
		$("#btnSelectedPdf").click(function(){

			var arr_code = [];
			var cust_code = [];

			var data = oTable.rows( $("#tblList tbody > tr > td > input[type=checkbox]:checked").parents('tr') ).data();
			
			if( data.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "출력할 항목을 선택해주세요") ).show();
				return false;
			}
	
			for(var i=0; i<data.length; i++){
				arr_code.push( data[i].SEQ );
				cust_code.push( data[i].CUST_MK);
			}
			
			if(data.length > 0 ){

				var width=740;
				var height=720;
				
				var WRITE_DATE	= $("input[name='start_date']").val();
				var END_DATE	= $("input[name='end_date']").val();				
								
				var url = "/sale/sale/PdfDetailSelected/" + cust_code + "/" + arr_code + "/" + WRITE_DATE + "/" + END_DATE ;
				getPopUp(url , width, height);
			}
		});
	});
</script>
@stop
@extends('layouts.main')
@section('class','매출관리')
@section('title','일일판매관리 수정')
@section('content')
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}
	.edit_alert {display:none;}
	#tblList th {text-align:center;}
	.right{text-align:right; padding-right:1.5px;}

	.header_sumary label { width:60px; text-align:right;}
	.header_sumary input[type='text'] { width:80px;}
	.numeric{ text-align:right;}

	#tblList .numeric{ width:55px;}
	input[name='new_sujo']{ width:70px;}

	#tblList td{ padding-top:1px; padding-bottom:1px; }
	.btn-info{ margin-top:0px;}

	input[name='uncs'] {
		width: 80px;
	}

	input[name='uncs'].numeric {
		width: 100px;
	}

	table.dataTable thead > tr > th {
		padding-right: 0px;
	}

	#tblList input[name='uncs'].numeric.wide {
		width:110px;
	}
	.checkbox label{ width:25%;padding-left:0; text-align:left;}
</style>
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-fixed-300 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 판매목록</button>
					<button type="button" class="btn btn-success" id="btnPrev" ><i class="fa fa-step-backward"></i> 취소</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-footer header_sumary">
			<div class="edit_alert"><ul></ul></div>
			<div class="col-md-4 col-xs-12 ">
				<div class="form-group">
					<label>거래일자</label> 
					<input type="text" id="custWriteDt"/>
					<input type="hidden" id="oldcustWriteDt"/>

					<label>현금</label> 
					<input type="text" id="cashSum" class="right numeric"/>
				</div>
				<div class="form-group" >
					
					<label>카드</label> 
					<input type="text" id="cardSum" class="right numeric" />
				
					<label>미수</label> 
					<input type="text" id="unclSum" class="right numeric" />
				</div>
			</div>


			<div class="col-md-4 col-xs-12">
				
				<div class="form-group">
					<label>할인금액</label> 
					<input type="text" id="saleDiscount" class="right numeric" />

					<label>할인내역</label> 
					<input type="text" id="saleDiscountTEXT" class="left" placeholder="할인내역" />
				</div>

				<div class="form-group">
					<label>정상합계</label> 
					<input type="text" id="totalSum" class="right numeric"  />

					<label>합계</label> 
					<input type="text" id="totalSumA" class="right numeric" />
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="form-group">
					<button type="button" class="btn btn-info" id="btnSaveSaleInfoM"><i class="fa fa-edit"></i> 수정저장</button>
				</div>
				<div class="form-group">
					<div class="checkbox navbar" data-placement="top" data-toggle="tooltip" data-original-title="Tooltip on top">
						<label >
							<input type="checkbox" id="chkOnlyUncl" class="left" name="chkOnlyUncl" />
							단가만 수정	
						</label>
						<span class='red'>수량변동은 없고 금액만 변경된 경우 체크</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="box-body">
			<div class="dataTables_wrapper">
				<table id="tblList" class="table dataTable table-bordered table-hover display nowrap" summary="일일판매 목록" width="100%">
					<caption>재고조회</caption>
					<thead>
						<tr>
							<th class="check" width="15px"></th>
							<th class="subject" width="50px;">어종</th>
							<th class="name" width="45px">규격</th>
							<th class="name" width="60px">원산지</th>
							<th class="name" width="60px">입고일</th>
							<th class="name" width="90px">중량</th>
							<th class="name" width="60px">단가</th>
							<th class="subject" width="80px">금액</th>
							<th class="name" width="80px">비고</th>
							<th class="name" width="100px">수조</th>
							<th class="name" width="auto">새수조</th> <!-- 10 -->
							<th class="name" style="display:none;">SRL_NO</th>
							<th class="name" style="display:none;">판매일</th>
							<th class="name" style="display:none;">어종코드</th>
							<th class="name" style="display:none;">원산지 코드</th>
							<th class="name" style="display:none;">중량</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>금액합계</th>
							<th class="sumAmt"><input type="text" style="text-align:right;"/></th>
							<th></th>
							<th></th>
							<th></th>
							<th style="display:none;"></th>
							<th style="display:none;"></th>
							<th style="display:none;"></th>
							<th style="display:none;"></th>
							<th style="display:none;"></th>
						</tr>
					</tfoot>
					<tbody>
						@foreach($model as $data)
						<tr>
							<td>
								@if ($data->ISMOD == "1")
								<input type="checkbox" data-pis_mk="{{$data->PIS_MK}}" data-sizes="{{$data->SIZES}}" data-input_date="{{$data->INPUT_DATE}}" 
									data-origin_cd="{{$data->ORIGIN_CD}}" data-origin_nm ="{{$data->ORIGIN_NM}}" />
								@endif
							</td>
							<td>{{$data->PIS_NM}}</td>
							<td>{{$data->SIZES}}</td>
							<td>{{$data->ORIGIN_NM}}</td>
							<td>{{$data->INPUT_DATE}}</td>
							<td>{{number_format($data->QTY,1)}} <input type="text" name="qty" value="{{number_format($data->QTY, 1)}}" class="numeric" /></td>
							<td><input type="text" name="uncs" value="{{ number_format($data->SALE_UNCS, 0) }}" class="numeric" /></td>
							<td><input type="text" name="uncs" value="{{ number_format($data->AMT, 0) }}" class="numeric wide" /></td>
							<td><input type="text" name="uncs" value="{{ $data->REMARK }}" /></td>
							<td class='sujo_list'>
								{{$data->SUJO_NO}}
							</td>
							<td>
								@if ($data->ISMOD == "1")
								<input type="text" name="new_sujo" id="" />
								@endif
							</td>
							<td style="display:none;">
								@if ($data->ISMOD == "1")
								{{$data->SRL_NO}}
								@endif
							</td>
							<td style="display:none;">
								@if ($data->ISMOD == "1")
								{{$data->WRITE_DATE}}
								@endif
							</td>
							<td style="display:none;">
								{{$data->PIS_MK}}
								
							</td>
							<td style="display:none;">
								
								{{$data->ORIGIN_CD}}
								
							</td>
							<td style="display:none;">
								{{$data->QTY}}
							</td>

						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	
	$(function () {
		
		var start = moment().subtract(1500, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');
		
		// 목록 바로가기
		$("#btnList").click(function(){
			location.href = "/sale/sale/index?page=1";
		});

		// 취소 
		$("#btnPrev").click(function(){
			location.href = "/sale/sale/detailList/{{ Request::segment(4)}}/{{ Request::segment(5)}}/{{ Request::segment(6)}}";
		});
			
		// 미수금정보 작성일자
		$("#custWriteDt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		
		
		$.getJSON('/sale/sale/getViewSaleSum', {
			_token			: '{{ csrf_token() }}',
			'CUST_MK'		: "{{ Request::segment(4)}}",
			'SEQ'			: "{{ Request::segment(5)}}",
			'WRITE_DATE'	: "{!! urldecode(Request::segment(6)) !!}",

		}).success(function(xhr){

			var data = jQuery.parseJSON(JSON.stringify(xhr));
			$("#custWriteDt").val(data.WRITE_DATE);
			$("#oldcustWriteDt").val(data.WRITE_DATE); // 거래일자 고정
			$("#cashSum").val(Number(data.CASH_AMT).format());
			$("#cardSum").val(Number(data.CARD_AMT).format());
			
			$("#unclSum").val(Number(data.UNCL_AMT).format());
			$("#saleDiscount").val(Number(data.SALE_DISCOUNT).format());
			$("#saleDiscountTEXT").val(data.SALE_DISCOUNT_REMARK);
			$("#totalSum").val(Number(data.TOTAL_SALE_AMT).format());
			$("#totalSumA").val(Number(data.TOTAL_SALE_AMT).format());
			//removeCommas
			
		}).error(function(xhr,status, response) {
			
		});
		
		getSumQty();
		var arrData = [];
		var i = 0;
		$("#tblList tbody tr > td > input[type='checkbox']").each(function(i){
			
			var pis_mk = $(this).attr("data-pis_mk");
			var sizes = $(this).attr("data-sizes");
			var input_date = $(this).attr("data-input_date");
			var origin_cd = $(this).attr("data-origin_cd");
			var origin_nm = $(this).attr("data-origin_nm");
			arrData.push( {'PIS_MK' : pis_mk,  'SIZES' : sizes,  'INPUT_DATE' : input_date, 'ORIGIN_CD' : origin_cd, 'ORIGIN_NM' : origin_nm} );
		});
		
		function getSumQty(){
			var sumQty = 0;
			$("#tblList tbody tr").each(function(){
				if( $.isNumeric( parseFloat( removeCommas($(this).find("td:eq(7) input").val())))){
					sumQty += parseFloat(removeCommas($(this).find("td:eq(7) input").val()));
				}
			});
			$(".sumAmt > input").val(Number(sumQty).format());
			$("#totalSum").val(Number(sumQty).format());
			$(".header_sumary input[type='text']").trigger("keyup");
		}

		$("#tblList tbody tr").each(function(){ 
			$(this).find("td:eq(7) > input").keyup(getSumQty); 
			$(this).find("td:eq(7) > input").change(getSumQty).trigger('change'); 
			$(".header_sumary input[type='text']").trigger("keyup");
		});

		

		$("#tblList tbody tr td > input").keyup(function(){

			var qty		= removeCommas($(this).parents("tr").find("td:eq(5) > input").val());
			var uncs	= removeCommas($(this).parents("tr").find("td:eq(6) > input").val());
			
			// keyup 이벤트
			// 수조의 중량과 단가가 비었을때 오류 방지를 위해 빈값일때는 0으로 초기화
			if( qty == "" ) {
				qty = 0;
			}
			if( uncs == ""){
				uncs = 0;
			}

			var T = Number( '1e' + 1 );
			a = Math.round( ( qty * uncs ) * T ) / T;
			a += 500;
			a = Math.floor( a / 1000 ) * 1000;

			if( $.isNumeric(parseFloat(qty)) && $.isNumeric(parseFloat(uncs))){
				if( a >= 0){
					$(this).parents("tr").find("td:eq(7) > input").val(a);
				}
			}
			getSumQty(); 
		});
		
		// 헤더안의 금액이 변동되었을때 이벤트
		$(".header_sumary input[type='text']").keyup(function(){
			
			var sumQty = 0;

			var cash		= parseInt( removeCommas($("#cashSum").val()) );	// 현금
			var cardSum		= parseInt(removeCommas($("#cardSum").val()) );		// 카드
			var unclSum		= parseInt(removeCommas($("#unclSum").val()) );		// 합계
			var totalSum	= parseInt(removeCommas($("#totalSum").val()) );	// 정상합계
			var saleDiscount= parseInt(removeCommas($("#saleDiscount").val()) );// 할인금액
			var totalSumA	= parseInt(removeCommas($("#totalSumA").val()) );	// 전체합계
				
			cash			= $.isNumeric(cash) ? cash : 0;
			cardSum			= $.isNumeric(cardSum) ? cardSum : 0;
			unclSum			= $.isNumeric(unclSum) ? unclSum : 0;
			saleDiscount	= $.isNumeric(saleDiscount) ? saleDiscount : 0;
			
			totalSumA		= totalSum - saleDiscount;
			unclSum			= totalSumA - cash - cardSum;
			
			$("#totalSum").val(Number(totalSum).format());
			$("#totalSumA").val(Number(totalSumA).format());
			
			$("#unclSum").val(Number(unclSum).format());
			$("#cardSum").val(Number(cardSum).format());
			$("#cashSum").val(Number(cash).format());
		});
		
		// 수조번호를 가져옴 
		$.getJSON('/sale/sale/getSujoNo', {
			_token			: '{{ csrf_token() }}',
			arrData			: arrData
		}).success(function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			var re_sujo_count	= 0;

			for(var i=0; i<data.length; i++){
				var select = "<select>";
				for(var j=0; j<data[i].length; j++){
					select += "<option value=" + data[i][j].SUJO_NO +">" + data[i][j].SUJO_NO+ "</option>";

					if( data[i][j].SUJO_NO == "RE"){
						re_sujo_count++;
						$("#tblList tbody tr > td > input[type='checkbox']").parents("tr:nth(" + (i)+ ")").find("input[name='new_sujo']").val("RE0" + i);
					}
				}
				select += "</select>";

				$("#tblList tbody tr > td > input[type='checkbox']").parents("tr:nth(" + (i)+ ")").find(".sujo_list").html(select);
			}

			// RE 수조가 존재할때 
			// - 반품하려는 재고(어종/사이즈/원산지)의 수조가 존재 하지 않는경우
			// -- 즉, 반품했을때 반품된 재고를 담을 수조가 이미 삭제 된 경우가 있을 경우
			if(re_sujo_count > 0){
				$(".content > .alert").remove();
				$(".content").prepend( getAlert('warning', '경고', "수량을 수정할 경우 부분적으로 반품된 재고를 담을 수조가 없을수도 있으므로 새로 생성될 새수조 번호를 정해주세요") ).show();				
				$("#tblList tbody tr > td > input[name='new_sujo']").each(function(){ 
					if( $(this).val() != "" ){
						$(this).css("background", "#ffcccc");
					}
				});
			}
			
		}).error(function(xhr,status, response) {
			
		});

		function fnSetTableToArray(table){
			
			$("#tblList tbody tr").each(function(){ 
				if( $(this).find("td:eq(5) > input").val() == "" || $(this).find("td:eq(5) > input").val() == undefined){
					$(this).find("td:eq(5) > input").val(0);
				}
			});

			var $rows= $(table + " tbody tr");
			var data = [];
			
			$rows.each(function(row, v) {
				data[row] = [];

				$(this).find("td").each(function(cell, v) {
					
					if( $(this).find("input[type='checkbox']").length > 0 ){
						data[row][cell] = $(this).find("input[type='checkbox']").is(":checked") ? "1" : "0";
					}else if( $(this).find("input[type='text']").length > 0 ){
						data[row][cell] = removeCommas( $(this).find("input[type='text']").val().trim() );
					}else if($(this).find("select").length > 0 ){
						if( $(this).find("option:selected").val() !== undefined ){
							data[row][cell] = $(this).find("option:selected").val().trim();
						}
					}else{
						data[row][cell] = $(this).text().trim();
					}
				});
			});
			return data;
		}
		
		// 저장
		$("#btnSaveSaleInfoM").click(function(){
			
			$(".header_sumary input[type='text']").trigger("keyup");
			$("#tblList tbody tr td:eq(0) input[type='checkbox']").prop('checked', false);
			
			$("#tblList tbody > tr > td:eq(10) > input").each(function(){
				
				var newSujoNo = $(this).val();
				
				// 새수조가 존재 할때(수조가 있는지 없는지 체크 한 후 저장하도록 한다)
				// 2016-12-12 : 김현섭 수정
				$.ajax({
						url : '/buy/buy/chkHasSujo', 
						type: "GET",
						data : {
							'SUJO_NO' : newSujoNo,
						}

					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						
						if(parseInt(data.result) != 0){
							$(".content > .alert").remove();
							$(".content").prepend( getAlert('warning', '경고', newSujoNo + "번 수조는 이미 존재합니다. 새로운 수조번호를 입력하세요") ).show();
							
							return false;
						}else{
							setTimeout(function(){ 
				
								if( confirm('합계를 확인하세요. 저장하시겠습니까?')){
									var data = fnSetTableToArray( "#tblList");
									var type = "UPDATE" ;
									
									$.ajax({
											url : '/sale/sale/setUpdateSaleDetail', 
											type: "POST",
											data : {
													_token					: '{{ csrf_token() }}',
													'CUST_MK'				: "{{ Request::segment(4)}}",
													'SEQ'					: "{{ Request::segment(5)}}",
													'WRITE_DATE'			: $("#custWriteDt").val(),
													'OLD_WRITE_DATE'		: "{!! urldecode(Request::segment(6)) !!}",
													'TYPE'					: type,
													'WRITE_DATE1'			: $("#custWriteDt").val(),
													'CASH_AMT'				: removeCommas($("#cashSum").val()),
													'CARD_AMT'				: removeCommas($("#cardSum").val()),
													'UNCL_AMT'				: removeCommas($("#unclSum").val()),
													'TOTAL_SALE_AMT'		: removeCommas($("#totalSum").val()),
													'SALE_DISCOUNT_REMARK'	: $("#saleDiscountTEXT").val(),
													'SALE_DISCOUNT'			: removeCommas($("#saleDiscount").val()),
													'list'					: data,
													'UNCL_ONLY_YN'			: $("#chkOnlyUncl").is(":checked") ? "Y" : "N",
											}

									}).success(function(xhr){
										var data = jQuery.parseJSON(JSON.stringify(xhr));

										if( data.result == "success"){
											alert('수정 되었습니다.');
											location.href = "/sale/sale/index";
										}

									}).error(function(xhr,status, response) {
										var error = jQuery.parseJSON(xhr.responseText);  
										var info = $('.edit_alert');
										info.hide().find('ul').empty();
										if( error.result == "DB_ERROR"){
											info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
										}else{
											for(var k in error.message){
												info.find('ul').append('<li>' + k + '</li>');
												if(error.message.hasOwnProperty(k)){
													error.message[k].forEach(function(val){
														info.find('ul').append('<li>' + val + '</li>');
													});
												}
											}
										}
										info.slideDown();
									});
								}
							}, 1000);
										
						}
					});
				
			});
			//return;
			
		});
		
		// 취소 뒤로가기
		$(".btnCancel").click(function(){
			location.href = "/sale/sale/detailList/{{Request::segment(4)}}/{{Request::segment(5)}}/{{Request::segment(6)}}";
		});
	});

</script>
<!-- 일일판매 수정 팝업 -->
@stop
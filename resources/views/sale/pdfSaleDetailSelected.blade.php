<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>거래명세서</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
				font-size:11px;
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:9px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table{
				border: 1px solid black;
				
				
			}

			th, td {
				/*border: 1px solid black;*/
				
				border:none;
			}

			span.title { text-align:center;}
			@page { margin:50px 45px 20px 30px;}
			.page-break { page-break-after:always; }
			
			.page-break:last-child{ page-break-after:avoid; }

		</style>
			
	</head>
	<body style="font-size:9px;">
		@for($k=0; $k < count($models); $k++)
		<div class="page-break">
			<div class="box box-primary">
				<div class="box-body">
					<table summary="거래명세서 공급자" width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;margin-top:40px;">
						<tr>
							<td colspan="6" style='border-top:1px solid black;border-left:1px solid black;text-align:center;height:40px;vertical-align:top;padding-left:75px;'>
								<span style="font-size:20px;text-decoration: underline;">&nbsp;거&nbsp;&nbsp;래&nbsp;&nbsp;명&nbsp;&nbsp;세&nbsp;&nbsp;서&nbsp;</span>
							</td>
							<td style='border-top:1px solid black;vertical-align:top;text-align:right;'>(공급자용)&nbsp;&nbsp;</td>
							<td style='border-left:1px solid black; border-right:2px dashed black; '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style='border-right:1px solid black'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td colspan="5" style=';padding-left:180px;border-top:1px solid black;vertical-align:top;text-align:center;font-size:20px;text-decoration: underline;'><span>&nbsp;거&nbsp;&nbsp;래&nbsp;&nbsp;명&nbsp;&nbsp;세&nbsp;&nbsp;서&nbsp;&nbsp;</span></td>
							<td colspan="2" style='border-right:1px solid black;border-top:1px solid black;vertical-align:top;text-align:right;'>(고객 보관용)&nbsp;&nbsp;</td>
						</tr>

						<tr>
							<td style='border-left:1px solid black;border-right:1px solid black;font-size:14px;' colspan="7" style='text-align:left;'>&nbsp;&nbsp;공&nbsp;&nbsp;급&nbsp;&nbsp;자 : {{ $corp->FRNM }}</td>
							<td style='border-top:none;border-right:2px dashed black; '>&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="7" style='text-align:left;border-left:1px solid black;border-right:1px solid black;font-size:14px;'>&nbsp;&nbsp;&nbsp;&nbsp;고&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;객 : {{ $custs[$k]->FRNM }}</td>
						</tr>

						<tr>
							<td colspan="4" style='text-align:left;border-left:1px solid black;border-top:1px solid black;border-right:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;거래일자 </div>: {{ $sums[$k]->WRITE_DATE }} </td>
							<td colspan="3" style='text-align:center;border-top:1px solid black;border-right:1px solid black;'>[결제내역] </td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-top:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;거래일자 </div>: {{ $sums[$k]->WRITE_DATE }} </td>
							<td colspan="3" style='text-align:center;border-top:1px solid black;border-right:1px solid black;border-left:1px solid black;'>[결제내역] </td>
						</tr>
						<tr>
							<td colspan="4" style='text-align:left;border-left:1px solid black;border-right:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;상호 </div>: {{ $custs[$k]->FRNM }}</td>
							<td colspan="3" style='text-align:left;border-left:1px solid black;border-right:1px solid black;'>&nbsp;&nbsp;이전미수금 &nbsp;: {{ number_format( $preUncls[$k]) }} </td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;상호 </div>: {{ $corp->FRNM }} </td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'>&nbsp;&nbsp;이전미수금 &nbsp;: {{ number_format( $preUncls[$k]) }} </td>
						</tr>
						<tr>
							<td colspan="4" style='text-align:left;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;대표자 </div>: {{ $custs[$k]->RPST}}</td>
							<td colspan="3" style='text-align:left;border-left:1px solid black;border-right:1px solid black;'></td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;대표자 </div>: {{ $corp->RPST}}</td>
							<td colspan="3" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'></td>
						</tr>
						<tr>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;주소 </div>: {{ $custs[$k]->ADDR1}}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;거래액 </div>: {{ number_format($sums[$k]->TOTAL_SALE_AMT) }}</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;주소 </div>: {{ $corp->ADDR1}}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;거래액 </div>: {{ number_format($sums[$k]->TOTAL_SALE_AMT) }}</td>
						</tr>
						<tr>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'> </td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;입금액 </div>: {{ number_format($sums[$k]->CASH_AMT + $sums[$k]->CARD_AMT) }}</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;계좌번호 </div>: {{ $corp->BANK_NM }} {{ $corp->ACCOUNT_NO  }}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'>
							<div style="width:75px; display:inline-block;">&nbsp;&nbsp;입금액 </div>: {{ number_format($sums[$k]->CASH_AMT + $sums[$k]->CARD_AMT) }}</td>
						</tr>

						<tr>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'> </td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;미입액 </div>: {{ number_format( $sums[$k]->UNCL_AMT) }}</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'></td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;미입액 </div>: {{ number_format( $sums[$k]->UNCL_AMT) }}</td>
						</tr>

						<tr>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;전화번호 </div>: {{ $custs[$k]->PHONE_NO}}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;전체미수액</div>: {{ number_format($totalUncls[$k]) }}</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;전화번호 </div>: {{ $corp->PHONE_NO}}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;전체미수액</div>: {{ number_format($totalUncls[$k]) }}</td>

						</tr>
						<tr>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block;">&nbsp;&nbsp;팩스번호 </div>: {{ $custs[$k]->FAX}}</td>

							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'>
								<div style="width:75px; display:inline-block;">&nbsp;&nbsp;할인내역 </div>: {{ number_format($sums[$k]->SALE_DISCOUNT) }}&nbsp;&nbsp;</td>
							</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'><div style="width:75px; display:inline-block">&nbsp;&nbsp;팩스번호 </div>: {{ $corp->FAX}}</td>
							<td colspan="3" style='text-align:left;border-right:1px solid black;border-left:1px solid black;'>
								<div style="width:75px; display:inline-block;">&nbsp;&nbsp;할인내역 </div>: {{ number_format($sums[$k]->SALE_DISCOUNT) }}&nbsp;&nbsp;</td>
							</td>
						</tr>
						<tr>
							<td colspan="4" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'></td>
							<td colspan="3" style='text-align:right;border-right:1px solid black;'>({{ date("Y-m-d") }})&nbsp;&nbsp;</td>
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="4" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'></td>
							<td colspan="3" style='text-align:right;border-right:1px solid black;'>({{ date("Y-m-d") }})&nbsp;&nbsp;</td>
						</tr>
							
						<tr>
							<td style='text-align:center;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;'>수조</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>어종</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>규격</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>원산지</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>수량</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>단가</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black;'>금액(원)</td>
							
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>

							<td style='text-align:center;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;'>수조</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>어종</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>규격</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>원산지</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>수량</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;'>단가</td>
							<td style='text-align:center;border-top:1px solid black;border-bottom:1px solid black;border-right:1px solid black;'>금액(원)</td>

						</tr>
						
						{{--*/ $j  = 0 /*--}}
						@for($j=0; $j < count($models[$k]); $j++)

						<tr>
							<td style='text-align:center;border-left:1px solid black;' width="50px">{{ $models[$k][$j]->SUJO_NO }}</td>
							<td style='text-align:center;' width="80px">{{ $models[$k][$j]->PIS_NM }}</td>
							<td style='text-align:center;' width="60px">{{ $models[$k][$j]->SIZES}}</td>
							<td style='text-align:center;' width="100px">{{ $models[$k][$j]->ORIGIN_NM}}</td>
							<td style='text-align:right;' width="60px">{{ number_format( $models[$k][$j]->QTY, 1 ) }}</td>
							<td style='text-align:right;' width="80px">{{ number_format( $models[$k][$j]->UNCS ) }}</td>
							<td style='text-align:right;border-right:1px solid black;padding-right:6px;'>{{ number_format( $models[$k][$j]->AMT ) }}</td>
							
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>

							<td style='text-align:center;border-left:1px solid black;' width="50px">{{ $models[$k][$j]->SUJO_NO  }}</td>
							<td style='text-align:center;' width="80px">{{ $models[$k][$j]->PIS_NM}}</td>
							<td style='text-align:center;' width="60px">{{ $models[$k][$j]->SIZES}}</td>
							<td style='text-align:center;' width="100px">{{ $models[$k][$j]->ORIGIN_NM}}</td>
							<td style='text-align:right;' width="60px">{{ number_format( $models[$k][$j]->QTY, 1 )}}</td>
							<td style='text-align:right;' width="80px">{{ number_format( $models[$k][$j]->UNCS ) }}</td>
							<td style='text-align:right;border-right:1px solid black;padding-right:6px;'>{{ number_format( $models[$k][$j]->AMT ) }}</td>
						</tr>
						
						@endfor
						
					
						<tr>
							<td style='text-align:center;border-left:1px solid black;border-top:1px solid dashed black;' width="25px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="80px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="60px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="100px">합 계 : </td>
							<td style='text-align:right;border-top:1px solid dashed black;' width="60px">{{ number_format( $sum_qtys[$k], 1 ) }}</td>
							<td style='text-align:right;border-top:1px solid dashed black;' width="80px"></td>
							<td style='text-align:right;border-top:1px solid dashed black;border-right:1px solid black;padding-right:6px;'>{{ number_format($sums[$k]->TOTAL_SALE_AMT) }}</td>
							
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>

							<td style='text-align:center;border-top:1px solid dashed black;border-left:1px solid black;' width="25px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="80px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="60px"></td>
							<td style='text-align:center;border-top:1px solid dashed black;' width="100px">합 계 : </td>
							<td style='text-align:right;border-top:1px solid dashed black;' width="60px">{{ number_format( $sum_qtys[$k], 1 ) }}</td>
							<td style='text-align:right;border-top:1px solid dashed black;' width="80px"></td>
							<td style='text-align:right;border-top:1px solid dashed black;border-right:1px solid black;padding-right:6px;'>{{ number_format( $sums[$k]->TOTAL_SALE_AMT ) }}</td>

						</tr>
						<tr>
							<td colspan="7" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'>  === 이하 여백 ===</td>
									
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="7" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'>  === 이하 여백 ===</td>
						</tr>
						@for ($i = 0; $i < 16 - count($models[$k]); $i++)
						<tr>
							<td colspan="7" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'> </td>
									
							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="7" style='text-align:center;border-left:1px solid black;border-right:1px solid black;'> </td>
						</tr>

						@endfor
						<tr>
							<td colspan="7" style='text-align:center;border:1px solid black;'></td>

							<td style="border-right:2px dashed black; ">&nbsp;&nbsp;</td>
							<td>&nbsp;&nbsp;</td>
							<td colspan="7" style='text-align:left;border:1px solid black;'>&nbsp;저희 {{$corp->FRNM}}을(를) 이용해주셔서 감사합니다</td>
						</tr>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
		@endfor
	</body>
</html>

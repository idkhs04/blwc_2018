@extends('layouts.main')
@section('class','매출관리')
@section('title','판매 로그')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblListUnprov_filter, #tblListStockSujo_filter{ display:none;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-2 col-xs-6">

				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblListStockL" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="재고 목록 로그">
				<caption>판매 로그</caption>
				<thead>
					<tr>
						<th width="30px"></th>
						<th width="80px">기록일자</th>  
						<th width="50px">구분</th>  
						<th width="60px">기록자ID</th>
						<th width="80px">거래업체</th>  
						<th width="80px">판매일자</th>  
						<th width="100px">이전금액</th>  
						<th width="100px">이후금액</th>  
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		
		// 목록 이동
		$("#btnList").click(function(){
			location.href="/sale/sale/index";
		});

		// 판매로그
		var stTable = $('#tblListStockL').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/sale/sale/listLogM",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'LOG_DATE', name: 'LOG_DATE' , className: "dt-body-center" },
				{ data: 'LOG_DIV_NM', name: 'LOG_DIV_NM' , className: "dt-body-center" },
				{ data: 'MEM_ID', name: 'MEM_ID' , className: "dt-body-center" },
				{ data: 'FRNM', name: 'FRNM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center" },
				{ 
					data: 'BEFORE_TOTAL_SALE_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'TOTAL_SALE_AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblListStockL tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			
			var row = stTable.row( tr );
			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				var d = row.data();
				var listHtml = "";
				$.getJSON('/sale/sale/listLogD1', {
					_token		: '{{ csrf_token() }}'
					, LOG_DATE	: d.LOG_DATE
					, LOG_SEQ	: d.LOG_SEQ
					}).success(function(xhr){
						var list = jQuery.parseJSON(JSON.stringify(xhr.data));
						listHtml += "" +
							'<h4>이전내역</h4>' + 
							'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:100%" >' +
								'<thead><tr><th>어종명</th><th>규격</th><th>입고일</th><th>원산지</th><th>중량</th><th>단가</th><th>금액</th><th>비고</th></tr></thead>' + 
								'<tbody>';

						$.each(list, function(i){
							
							console.log( this.PIS_NM);
							listHtml +=  
							'<tr>'+
								'<td>'+list[i].PIS_NM+'</td>'+
								'<td>'+list[i].SIZES+'</td>'+
								'<td>'+list[i].WRITE_DATE+'</td>'+
								'<td>'+list[i].ORIGIN_NM+'</td>'+
								'<td>'+list[i].QTY+'</td>'+
								'<td>'+list[i].UNCS+'</td>'+
								'<td>'+list[i].AMT+'</td>'+
								'<td>'+list[i].REMARK+'</td>'+
							'</tr>';
						
						});
						listHtml += "</tbody></table>";

						row.child(listHtml).show();
						tr.addClass('shown');

				}).error(function(xhr){
					alert('error');
				});
				
				$.getJSON('/sale/sale/listLogD2', {
					_token		: '{{ csrf_token() }}'
					, LOG_DATE	: d.LOG_DATE
					, LOG_SEQ	: d.LOG_SEQ
				}).success(function(xhr){
					var list = jQuery.parseJSON(JSON.stringify(xhr).data);
					listHtml +='<h4>이후내역</h4>' + 
							'<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:100%" >' +
								'<thead><tr><th>어종명</th><th>규격</th><th>입고일</th><th>원산지</th><th>중량</th><th>단가</th><th>금액</th><th>비고</th></tr></thead>' + 
								'<tbody>';

						$.each(list, function(i){
							listHtml +=  
							'<tr>'+
								'<td>'+ list[i].PIS_NM+'</td>'+
								'<td>'+list[i].SIZES+'</td>'+
								'<td>'+list[i].WRITE_DATE+'</td>'+
								'<td>'+list[i].ORIGIN_NM+'</td>'+
								'<td>'+list[i].QTY+'</td>'+
								'<td>'+list[i].UNCS+'</td>'+
								'<td>'+list[i].AMT+'</td>'+
								'<td>'+list[i].REMARK+'</td>'+
							'</tr>';
						
						});
						listHtml += "</tbody></table>";
						row.child(listHtml).show();
						tr.addClass('shown');;

				}).error(function(xhr){
					alert('error');
				});


			}
		} );

	});

</script>

@stop
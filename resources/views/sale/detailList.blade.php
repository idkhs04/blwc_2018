@extends('layouts.main')
@section('class','매출관리')
@section('title','일일판매관리 상세')
@section('content')
<style>
	.headerSumQty ul { margin-left:0; padding-left:0; margin-bottom: 0; padding-top: 7px;}
	.headerSumQty{text-align:right;}
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}
	.btn-footer .btn{ margin-right:10px;}
	.btn-footer{ text-align:center;}
	@media (min-width:768px) {
		.col-fixed-300{ width:300px; padding-right:0px;}
		.col-fixed-725{ width:725px; padding-right:0px; text-align:right;}
	}
	
	.dt-body-right {text-align:right;}

</style>
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList" ><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 거래명세서</button>
					<button type="button" class="btn btn-success" id="btnLog"><i class="fa fa-code"></i> 이력보기</button>
				</div>
			</div>
			<div class="headerSumQty col-md-9 col-xs-12">
				<ul>
					<li><label><span class="glyphicon glyphicon-user"></span> 거래업체명 : </label> <span id="custFrnm" ></span></li>
					<li><label><span class="glyphicon glyphicon-tag"></span> 거래일자 : </label> <span id="custWriteDt" ></span></li>
					<li><label><span class="glyphicon glyphicon-inbox"></span> 현금 : </label> <span id="cashSum" ></span></li>
					<li><label><span class="glyphicon glyphicon-inbox"></span> 미수 : </label> <span id="unclSum" ></span></li>
					<li><label><span class="glyphicon glyphicon-inbox"></span> 합계 : </label> <span id="totalSum" ></span></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="일일판매 상세 목록" width="100%">
				<caption>일일판매 상세조회</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="50px">어종</th>
						<th width="30px">규격</th>
						<th width="30px">원산지</th>
						<th width="50px">수량</th>
						<th width="50px">단가(원)</th>
						<th width="80px">금액</th>
						<th width="auto">기타</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>


		</div>
		<div class="box-footer btn-footer">
			<button class='btn btn-success btnGoUpdate' ><i class='fa fa-edit'></i> 수정</button>
			<button class='btn btn-warning  btnGoReturn' ><i class='fa  fa-reply-all'></i> 반품</button>
			<button class='btn btn-danger  btnGoDelete' ><i class='fa  fa-remove'></i> 삭제</button>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		$.getJSON('/sale/sale/getViewSaleSum', {
			_token			: '{{ csrf_token() }}',
			'CUST_MK'		: "{{ Request::segment(4)}}",
			'SEQ'			: "{{ Request::segment(5)}}",
			'WRITE_DATE'	: "{!! urldecode(Request::segment(6)) !!}",

		}).success(function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			$("#custFrnm").text(data.FRNM);
			$("#custWriteDt").text(data.WRITE_DATE);
			$("#cashSum").text(Number(data.CASH_AMT).format());
			$("#unclSum").text(Number(data.UNCL_AMT).format());
			$("#totalSum").text(Number(data.TOTAL_SALE_AMT).format());

		}).error(function(xhr,status, response) {
			alert('오류입니다. 관리자에게 문의바랍니다.')
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/sale/sale/detailData",
				data:function(d){
					d.cust_mk		= "{{ Request::segment(4)}}";
					d.seq			= "{{ Request::segment(5)}}";
					d.write_date	= "{!! urldecode(Request::segment(6)) !!}";
				}
			},
			columns: [
				{
					data:   "PIS_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'PIS_NM', name: 'PIS_NM' },
				{ data: 'SIZES',		name: 'SIZES'  , className: "dt-body-center"},
				{ data: "ORIGIN_NM",  name: 'ORIGIN_NM', className: "dt-body-center"},
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'REMARK', name: 'REMARK'},


			],

			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var qtys = 0;
				var j = 0;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총수량
				qutySum = api.column( 4 ).data().reduce( function (a, b) {

					size = api.column( 7 ).data()[j++];
					console.log(isConfigQty());
					if( size == "마리" && isConfigQty() ) {
						qtys += intVal(b);
						return intVal(a);
					}else{
						return intVal(a) + intVal(b);
					}

					return intVal(a) + intVal(b);}, 0 );

				if( isConfigQty()){
					$( api.column( 4 ).footer() ).html("Kg : " + Number(qutySum).format() + " / 마리:" + Number(qtys).format() );
				}else{
					$( api.column( 4 ).footer() ).html("Kg : " + Number(qutySum).format() );
				}

				// 총판매금액
				saleSum = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				$( api.column( 6 ).footer() ).html(Number(saleSum).format() );


			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			}else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 목록 바로가기
		$("#btnList").click(function(){
			localStorage["IS_BACK"]		= "Y";
			location.href = "/sale/sale/index?page=1";
		});

		// 로그조회 바로가기
		$("#btnLog").click(function(){
			location.href = "/sale/sale/log";
		});

		// 수정
		$('button.btnGoUpdate').click(function () {
			location.href = "/sale/sale/detailForm/{{Request::segment(4)}}/{{Request::segment(5)}}/{{Request::segment(6)}}";
		});

		// 반품
		$('button.btnGoReturn').click(function () {
			location.href = "/sale/sale/detailReturn/{{Request::segment(4)}}/{{Request::segment(5)}}/{{Request::segment(6)}}";
		});

		// 삭제
		$('button.btnGoDelete').click(function () {

			$.getJSON('/sale/sale/setDeleteSaleDetail', {
				_token		: '{{ csrf_token() }}',
				CUST_MK		: "{{Request::segment(4)}}",
				SEQ			: "{{Request::segment(5)}}",
				WRITE_DATE	: "{{Request::segment(6)}}",

			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				alert('삭제되었습니다.');
				location.href = "/sale/sale/index";

			}).error(function(xhr,status, response) {
				$(".main_alertMsg").html( getAlert('danger', '오류', "삭제도중 오류가 발생하였습니다. 관리자에게 문의하세요.") ).show();
			});

		});

		// 거래명세서 출력
		$("#btnPdf").click(function(){
			
			var width=740;
			var height=720;
			
			var cust_mk		= "{{Request::segment(4)}}";
			var seq			= "{{Request::segment(5)}}";
			var write_date	= "{{Request::segment(6)}}";
			
			var url = "/sale/sale/PdfDetail/" + cust_mk + "/" + seq + "/" + write_date;
			getPopUp(url , width, height);

		});
	});

</script>
@stop
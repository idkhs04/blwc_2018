<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>업체별 판매현황</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:10px; }
			thead{
				width:100%;
				position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>

		</head>
		<body>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<span class="title" style='text-align:center;font-size:23px;display:block;'>판매현황</span>

						<table id="tblHeader" summary="업체별 판매현황" style="border:none;width:100%">
							<tr>
								<td style="text-align:left;width:49%;"> {{$end }} 년도</td>
								<td style="text-align:right;width:49%;">(단위 : Kg/만원 )</td>
							</tr>
						</table>

						<table id="tblList" summary="업체별 판매현황" width="100%" style="border-bottom:3px solid block;">
							<caption>기간별 어종판매 목록</caption>
							<thead style="border-top:3px solid block; border-bottom:1px solid block;padding-top:5px;">
								<td class="name">NO</td>
								<td class="name">어종/규격</td>
								<td class="name">1월</td>
								<td class="name">2월</td>
								<td class="name">3월</td>
								<td class="name">4월</td>
								<td class="name">5월</td>
								<td class="name">6월</td>
								<td class="name">7월</td>
								<td class="name">8월</td>
								<td class="name">9월</td>
								<td class="name">10월</td>
								<td class="name">11월</td>
								<td class="name">12월</td>
								<td class="name">합계</td>
							</thead>
							<tbody>
								@foreach ($model as $key => $item)
								<tr>
									<td style='text-align:center;'>{{ $item->INPUT_DATE}}</td>
									<td style='text-align:center;'>{{ $item->PIS_NM}}</td>
									<td style='text-align:center;'>{{ $item->SIZES}}</td>
									<td style='text-align:center;'>{{ $item->ORIGIN_NM}}</td>
									<td style='text-align:right;'>{{ number_format($item->QTY, 2) }}</td>
									<td style='text-align:right;'>{{ number_format($item->UNCS) }}</td>
									<td style='text-align:right;'>{{ number_format($item->AMT) }}</td>

									<td style='text-align:right;'>{{ number_format($item->S_QTY, 2) }}</td>
									<td style='text-align:right;'>{{ number_format($item->S_AMT) }}</td>
									<td style='text-align:right;'>{{ number_format($item->BENEFIT )}}</td>
									<td style='text-align:right;'>{{ number_format($item->REST) }}</td>
									<td style='text-align:right;'>{{ number_format($item->SALE_UNCS) }}</td>
									<td style='text-align:right;'>{{ number_format($item->RES_AMT) }}</td>
								</tr>
								@endforeach 

								<tr>
									<td colspan="4" style='text-align:center;'> 합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계 </td>
									<td> {{number_format($qtyInputQty, 2)}}Kg</td>
									<td> </td>
									<td> {{number_format($qtyInputAmt)}}원</td>
									<td> {{number_format($qtySaleQty, 2)}}Kg</td>
									<td> {{number_format($qtySaleAmt)}}원</td>
									<td> {{number_format($benefitSum)}}원</td>
									<td> </td>
									<td> </td>
									<td>{{number_format($resAmtSum)}}원</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<!-- 본문  -->
			</div>
		</body>
</html>

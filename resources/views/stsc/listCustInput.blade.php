@extends('layouts.main')
@section('class','통계관리')
@section('title','업체별 매입현황 통계')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	#modal_custCd label, #modal_custGrpEdit label{
		display:inline-block;
		width:24%;
		clear:both;
	}
	#modal_custCd .form-input-sm, #modal_custGrpEdit .form-input-sm{
		display:inline-block;
		width:10%;
		clear:both;
	}

	#modal_custCd .form-input-md, #modal_custGrpEdit .form-input-md{
		display:inline-block;
		width:24%;
		clear:both;
	}

	#modal_custCd .form-input-lg, #modal_custGrpEdit .form-input-lg{
		display:inline-block;
		width:74%;
		clear:both;
	}

	.SearchCustGrp , .SearchAreaCd, .SearchCustGrpEdit , .SearchAreaCdEdit{
		display:none;
	}

	#tableCustGrpList_filter, #tableAreaCdList_filter, #tableCustGrpEditList_filter, #tableAreaCdEditList_filter{
		display:none;
	}
</style>


	<style>
		input.cis-text {
			height: 34px;
			margin: 0px;
			vertical-align: top;
			padding-top: 0;
			margin-top: 0px;
		}
		.btn-info{ margin-top:0px;}
		.btnSearch{ margin-top:0;}
		#tblList_filter{ display:none;}
	</style>
	<div class="col-md-12">
		<div class="box box-primary">
		<!--
			<div class="box-header">
				<h3 class="box-title"><i class="fonti um-search-minus"></i> 검색</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
		-->
			<div class="box-body">
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<div class="col-md-2 col-xs-4">
					<select class="form-control" name="srtCondition">
						<!--<option value="ALL">전체</option>
						<option value="CUST_GRP_NM">거래처그룹명</option>
						<option value="FRNM">거래처 상호명</option>-->
						<option value="ALL">전체</option>
					@foreach( $CUST_GRP_INFO as $GRP)
					<option value="{{$GRP->CUST_GRP_CD}}">{{$GRP->CUST_GRP_NM}}</option>
					@endforeach
					</select>
				</div>
				<div class="col-md-2 col-xs-8">
					<div class="input-group">
						<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
						<span class="input-group-btn">
							<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">

				<table id="tblList" class="table table-bordered table-hover display nowrap" summary="거래처 목록" width="100%">
					<caption>거래처 목록(매입처)</caption>
					<thead>
						<tr>
							<th class="check" width="15px"></th>
							<th class="subject" width="60px">그룹</th>
							<th class="subject" width="80px">상호</th>
							<th class="subject" width="50px">대표자</th>
							<th class="subject" width="30px">지역</th>
							<th class="subject" width="60px">Tel</th>
							<!--<th class="subject">등급</th>-->
							<th class="subject" width="auto">선택</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	
	<!-- //List_boardType01 -->
	<!-- paging -->
	</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
	
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/custcd/custcd/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.corp_div	= 'P';
					
				}
			},
			order : [2, 'asc'],
			columns: [
				{
					data: "CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM' },
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'RPST', name: 'RPST' },
				{ data: 'AREA_NM', name: 'AREA_NM' , className: "dt-body-center" },
				{ data: 'PHONE_NO', name: 'PHONE_NO' , className: "dt-body-center" },
				//{ data: 'RANK_NM', name: 'RANK_NM' , className: "dt-body-center" },
				{
					data:   "CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-success btnGoDetail' ><span class='glyphicon glyphicon-on'></span>선택</button>";
						}
						return data;
					},
					className: "dt-body-left",
				}
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/stcs/stcs/listCustInputDetail/" + data.CUST_MK;
		});

	});
</script>


@stop
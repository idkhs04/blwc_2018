<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>판매현황</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic'; font-size:11px; }
			thead{
				width:100%;
				height:109px;
			}
			table > tr > td { text-align:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>

	</head>
	<body>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'> 판매현황 </span>

					
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="text-align:right"> {{$year}} 년도</td>
						</tr>
						<tr>
							<td style="text-align:right">(단위 : Kg/만원)</td>
						</tr>
					</table>

					<table id="tblList" summary="어종별 재고조회" width="100%" style="border-bottom:3px solid block;" border="1" >
						<caption>판매일보 상세</caption>
						<thead style="border-left:none;border-right:none;border-top:3px solid block; border-bottom:3px solid block;padding-top:5px;">
							<tr>
								<td style='text-align:center;' >No</td>
								<td style='text-align:center;' >어종/규격</td>
								<td style='text-align:center;'>1월</td>
								<td style='text-align:center;'>2월</td>
								<td style='text-align:center;'>3월</td>
								<td style='text-align:center;'>4월</td>
								<td style='text-align:center;'>5월</td>
								<td style='text-align:center;'>6월</td>
								<td style='text-align:center;'>7월</td>
								<td style='text-align:center;'>8월</td>
								<td style='text-align:center;'>9월</td>
								<td style='text-align:center;'>10월</td>
								<td style='text-align:center;'>11월</td>
								<td style='text-align:center;'>12월</td>
								<td style='text-align:center;'>합계</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
							<tr>
								<td style='text-align:center;border-right:1px solid black;'> {{ ++$key }}</td>
								<td style='text-align:center;border-right:1px solid black;'> {{ $item->PIS_NM}} / {{ $item->SIZES}}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q1M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q2M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q3M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q4M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q5M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q6M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q7M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q8M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q9M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q10M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q11M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q12M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TQTY)  }} 원</td>
							</tr>
							<tr>
								<td style='text-align:center;border-right:1px solid black;'> </td>
								<td style='text-align:center;border-right:1px solid black;'> </td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A1M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A2M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A3M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A4M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A5M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A6M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A7M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A8M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A9M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A10M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A11M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A12M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TAMT)  }} KG</td>
							</tr>
							@endforeach
							
							@foreach ($sum as $key => $item)
							<tr>
								<td style='text-align:center;border-right:1px solid black;'> </td>
								<td style='text-align:center;border-right:1px solid black;'> 합 계</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q1M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q2M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q3M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q4M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q5M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q6M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q7M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q8M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q9M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q10M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q11M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q12M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TQTY)  }} 원</td>
							</tr>
							<tr>
								<td style='text-align:center;border-right:1px solid black;'> </td>
								<td style='text-align:center;border-right:1px solid black;'> </td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A1M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A2M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A3M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A4M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A5M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A6M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A7M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A8M) }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A9M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A10M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A11M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A12M)  }}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TAMT)  }} KG </td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
</html>



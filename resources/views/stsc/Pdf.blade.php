<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>기간별 어종 판매현황</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:10px; }
			thead{
				width:100%;
				position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>

		</head>
		<body>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<span class="title" style='text-align:center;font-size:23px;display:block;'>기간별 어종 판매현황</span>

						<table id="tblHeader" summary="기간별 어종별 판매조회" style="border:none;width:100%">
							<tr>
								<td style="text-align:left;width:49%;">판매일자 : {{$start}} ~ {{$end }}</td>
								<td style="text-align:right;width:49%;">(단위 : Kg)<br/> (단위 : 원)</td>
							</tr>
						</table>

						<table id="tblList" summary="기간별 어종별 판매조회" width="100%" style="border-bottom:3px solid block;">
							<caption>기간별 어종판매 목록</caption>
							<thead style="border-top:1px solid block; border-bottom:1px solid block;padding-top:5px;">
								<tr>
									<td style='border-left:1px solid black; text-align:center;' rowspan="2">입고일</td>
									<td style='text-align:center;' rowspan="2">어종</td>
									<td style='text-align:center;' rowspan="2">규격</td>
									<td style='text-align:center;' rowspan="2">원산지</td>
									<td style='border-left:1px solid black;text-align:center;' colspan="3">매입</td>
									<td style='border-left:1px solid black;text-align:center;' colspan="2">매출</td>
									<td style='border-left:1px solid black;text-align:center;' rowspan="2">판매이익</td>
									<td style='border-left:1px solid black;border-right:1px solid black;text-align:center;' colspan="3">재고(판매예정)</td>
								</tr>
								<tr>
									<td  style='border-left:1px solid black;text-align:center;'>수량</td>
									<td style='text-align:right;'>단가</td>
									<td style='border-right:1px solid black;text-align:center;'>매입액</td>

									<td style='text-align:right;'>수량</td>
									<td style='border-right:1px solid black;text-align:center;'>매출액</td>

									<td style='border-left:1px solid black;text-align:center;'>현재고</td>
									<td style='text-align:right;'>판매단가</td>
									<td style='border-right:1px solid black;text-align:center;'>판매금액</td>
								</tr>
							</thead>
							<tbody>
								@foreach ($model as $key => $item)
								<tr>
									<td style='text-align:center; width:60px;'>{{ $item->INPUT_DATE}}</td>
									<td style='text-align:center; width:50px'>{{ $item->PIS_NM}}</td>
									<td style='text-align:center; width:50px'>{{ $item->SIZES}}</td>
									<td style='text-align:center;'>{{ $item->ORIGIN_NM}}</td>
									<td style='text-align:right;'>{{ number_format($item->QTY, 2) }}</td>
									<td style='text-align:right;'>{{ number_format($item->UNCS) }}</td>
									<td style='text-align:right;'>{{ number_format($item->AMT) }}</td>

									<td style='text-align:right;'>{{ number_format($item->S_QTY, 2) }}</td>
									<td style='text-align:right;'>{{ number_format($item->S_AMT) }}</td>
									<td style='text-align:right;'>{{ number_format($item->BENEFIT )}}</td>
									<td style='text-align:right;'>{{ number_format($item->REST) }}</td>
									<td style='text-align:right;'>{{ number_format($item->SALE_UNCS) }}</td>
									<td style='text-align:right;'>{{ number_format($item->RES_AMT) }}</td>
								</tr>
								@endforeach

								<tr>
									<td colspan="4" style='text-align:center;'> 합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계 </td>
									<td> {{number_format($qtyInputQty, 2)}}Kg</td>
									<td> </td>
									<td> {{number_format($qtyInputAmt)}}원</td>
									<td> {{number_format($qtySaleQty, 2)}}Kg</td>
									<td> {{number_format($qtySaleAmt)}}원</td>
									<td> {{number_format($benefitSum)}}원</td>
									<td> </td>
									<td> </td>
									<td>{{number_format($resAmtSum)}}원</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<!-- 본문  -->
			</div>
		</body>
</html>

@extends('layouts.main')
@section('class','통계관리')
@section('title','어종별 매입 통계')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }
	.totalSum{width:100%;}
	.ui-datepicker-calendar {
		display: none;
	}



	.seach_area input[type='text'] { width:30%; margin-left:5px;}
	.input-group-btn{float:left;}

	table tr td:first-child{ width:50px;}

	table td span:first-child{display:block;}

	table td span{display:block;}
	table td span:nth-child(2){text-align:right;}

	.input-group-btn{float:left;}

	#modal_piscls label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_piscls .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}

	.input-group .form-control{ width:80%;}
	.input-group label { float:left; padding:8px 5px 0 0;}

	.input-group-btn{float:left;}

	.input-group-btn:after{
		clear:left;
	}
	.btn-info{
		margin-top:0;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<label>년도</label>
					<input id="input_date" name="input_date" type="text"  class="form-control" style="width:60px;"/>
				</div>
			</div>

			<div class="col-md-3 col-xs-6">
				<div class="input-group seach_area">
					<input type="text" name="textPisNm"  class="form-control" placeholder="어종" value="{{ Request::Input('textSearch') }}">
					<input type="hidden" name="textSizes"  placeholder="규격" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="sea2016-11-24rch" id="search-pop-btn" class="btn btn-info"><i class="fa fa-search"></i>어종검색</button>
						<button type="button" name="search" id='btnPdf' class="btn btn-success btnPdf"><i class="fa fa-file-pdf-o"></i> 출력</button>
					</span>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종별 매입통계" width="100%">
				<caption>어종별 매입통계</caption>
				<thead>
					<tr>
						<th class="name">어종/규격</th>
						<th class="name">1월</th>
						<th class="name">2월</th>
						<th class="name">3월</th>
						<th class="name">4월</th>
						<th class="name">5월</th>
						<th class="name">6월</th>
						<th class="name">7월</th>
						<th class="name">8월</th>
						<th class="name">9월</th>
						<th class="name">10월</th>
						<th class="name">11월</th>
						<th class="name">12월</th>
						<th class="name">합계</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>

			<table id="tblListSum" class="table table-bordered table-hover display nowrap" summary="어종별 매입통계 합계" width="100%">
				<caption>어종매입통계합계</caption>
				<thead>
					<tr>
						<th class="name">합계</th>
						<th class="name">1월</th>
						<th class="name">2월</th>
						<th class="name">3월</th>
						<th class="name">4월</th>
						<th class="name">5월</th>
						<th class="name">6월</th>
						<th class="name">7월</th>
						<th class="name">8월</th>
						<th class="name">9월</th>
						<th class="name">10월</th>
						<th class="name">11월</th>
						<th class="name">12월</th>
						<th class="name">합계</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		var start = moment().subtract(1500, 'days');
		var end = moment();

		// 초기값 세팅
		$("#input_date").val(  moment().format('YYYY') );
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		$('#input_date').datepicker({
			changeYear: true,
			showButtonPanel: true,
			closeText: "확인",
			currentText: '올해', 
			dateFormat: 'yy',
			onClose: function(dateText, inst) {
				var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(year, 1));

				$("input[name='input_date']").trigger('change');
			}
		});

		$("#input_date").focus(function () {
			$(".ui-datepicker-month").hide();
		});

		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},

			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},

			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		$.fn.dataTable.render.ellipsis = function () {
			return function ( data, type, row ) {
				//return type === 'display' && data.length > 10 ? data.substr( 0, 10 ) +'…' : data;
			}
		};

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			sDom: 'rt',
			columnDefs: [
				{ "width": "100px", "targets": 0 },
			],
			fixedColumns: true,
			ajax: {
				url: "/stcs/stcs/getDataPisInput",
				data:function(d){

					d.SIZES			= $("input[name='textSizes']").val();
					d.YEAR			= $("input[name='input_date']").val();
					d.PIS_MK		= $("input[name='textPisNm']").val();
				}
			},
			columns: [

				{
					data: 'PIS_NM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + data + "</span><span>" + row.SIZES + "</span>";
						}
						return data;
					},
				},
				{
					data: 'Q1M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A1M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q2M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A2M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q3M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A3M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q4M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A4M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q5M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A5M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q6M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A6M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q7M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A7M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q8M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A8M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q9M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A9M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q10M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A10M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q11M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A11M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q12M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A12M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							tot = parseFloat(row.TAMT).toFixed(2);
							
							return "<span>" + Number(data).format() +  " Kg</span><span>" + Number(tot).format() + " 만원</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
			],

			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		var sTable = $('#tblListSum').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			sDom: 'rt',
			columnDefs: [
				{ "width": "100px", "targets": 0 },
			],
			fixedColumns: true,
			ajax: {
				url: "/stcs/stcs/getPisInputTotal",
				data:function(d){
					d.SIZES			= $("input[name='textSizes']").val();
					d.YEAR			= $("input[name='input_date']").val();
					d.PIS_MK		= $("input[name='textPisNm']").val();
				}
			},
			columns: [

				{
					data: 'TQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "합    계";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q1M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A1M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q2M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A2M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q3M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A3M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q4M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A4M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q5M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A5M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q6M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A6M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q7M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A7M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q8M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A8M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q9M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A9M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q10M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A10M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q11M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A11M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q12M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A12M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format(2) +  " Kg</span><span>" + Number(row.TAMT).format(2) + " 만원</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},

			],
			drawCallback: function() {
				$("#tblListSum thead").remove();

				$("#tblList thead > tr th").each(function(i){
					console.log(i);
					$("#tblListSum td:eq(" + i + ")").css("width", $(this).css("width"));
				});
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 어종/규격 검색코드
		$("#search-pop-btn").click(function(){
			$('#modal_piscls').modal({show:true});
		});

		$('#modal_piscls').on('hidden.bs.modal', defaltModalInsert);

		// 어종 입력 초기화
		function defaltModalInsert(){
			$("#modal_piscls input[type='text']").val("");
			$('.edit_alert').hide().find('ul').empty();
			$(".slideSearchCust" ).slideUp();

			oTable.draw() ;
			sTable.draw() ;
		}

		// 어종 조회 버튼
		$("#btnPisInfoList").click(function(){
			$( ".slideSearchCust" ).show(function(){
				var cTable = $('#tblPisInfoList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					bLengthChange: false,
					ajax: {
						url: "/piscls/piscls/listData",
						data:function(d){
							d.textSearch	= $("input[name='strSearchPis']").val();
						}
					},
					columns: [
						{ data: 'PIS_MK', name: 'PIS.PIS_MK' },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'SIZES',  name: 'SIZES' },
						{ data: 'REMARK',  name: 'REMARK' },

						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span>선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},

					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblPisInfoList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();
					$("#modal_piscls_mk").val(data.PIS_MK);
					$("#modal_piscls_nm").val(data.SIZES);

					$("input[name='textPisNm']").val(data.PIS_MK);
					$("input[name='textSizes']").val(data.SIZES);

					$("input[name='strSearchPis']").val('');
					$(".slideSearchCust" ).slideUp();
				});

				$("input[name='strSearchPis']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});

				// 거래처 검색 닫기
				$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });


			});
		});

		$("#search-btn").on("click", function(){
			oTable.search($(this).val()).draw() ;
			sTable.search($(this).val()).draw() ;
		});
		$("input[name='input_date']").on("change", function(){
			oTable.draw() ;
			sTable.draw() ;
		});

		// 전체검색
		$("#search").click(function(){
			oTable.draw() ;
			sTable.draw() ;
		});

		/*
		// 업체별 판매현황 출력
		$(".btnPdf").click(function(){

			$("form[name='getPdf'] > input[name='YEAR']").val( $(".input-group input[name='input_date']").val() );
			$("form[name='getPdf'] > input[name='PIS_MK']").val( $(".input-group input[name='textPisNm']").val() );
			$("form[name='getPdf'] > input[name='SIZES']").val( $(".input-group input[name='textSizes']").val() );
			$("form[name='getPdf']").submit();
		});
		*/

		// 업체별 판매현황 출력
		$(".btnPdf").click(function(){

			var width=740;
			var height=720;
			var SIZES		= $(".input-group input[name='textSizes']").val();
			var YEAR		= $(".input-group input[name='input_date']").val();
			var PIS_MK		= $(".input-group input[name='textPisNm']").val();

			var url = "/stcs/stcs/PdflistPisInput?YEAR=" + YEAR + "&SIZES=" + SIZES + "&PIS_MK=" + PIS_MK;
			getPopUp(url , width, height);
		});
	});

</script>
<!--
<div id="pdf">
	<form name="getPdf" method="post" action="/stcs/stcs/PdflistPisInput" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="YEAR"  />
		<input type="hidden" name="PIS_MK"  />
		<input type="hidden" name="SIZES"  />
	</form>
</div>
 -->
<!-- 어종조회 팝업 -->
<div class="modal fade" id="modal_piscls" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">

			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 어종조회 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<input type="hidden" name="chkPIS_MK" value="" />
				<input type="hidden" id="mode" value="" />
				<div class="form-group">
					<label for="modal_piscls_mk"><span class="glyphicon glyphicon-ok" ></span> 어종부호</label>
					<input type="text" class="form-control" id="modal_piscls_mk" name="PIS_MK" placeholder="어종부호" readonly="readonly" />
					<button type="button" name="search" id="btnPisInfoList" class="btn btn-info"><i class="fa fa-search"></i></button>
					<input type="text" class="form-control" id="modal_piscls_nm" name="PIS_NM" placeholder="어종명" readonly="readonly" />
					<span class="msg"></span>
				</div>
				<div class="form-group slideSearchCust">
					<div class="box box-primary">
						<div class="box-body">
							<label for="strSearchPis"><span class="glyphicon glyphicon-ok"></span> 어종명</label>
							<input type="text" class="form-control" name="strSearchPis" id="strSearchPis" />
							<table id="tblPisInfoList" class="table table-bordered table-hover display nowrap" summary="어종코드 조회">
								<caption>어종코드 입력</caption>
								<thead>
									<tr>
										<th scope="col" class="name">어종부호</th>
										<th scope="col" class="name">어종명</th>
										<th scope="col" class="name">규격</th>
										<th scope="col" class="name">비고</th>
										<th scope="col" class="name"></th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>

		</div>
	</div>
</div>
@stop
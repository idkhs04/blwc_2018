@extends('layouts.main')
@section('class','매출관리')
@section('title','기간별 판매현황')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active , .totalSum{ width:80%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}

	input.dt-body-right{text-align:right;}

	tfoot th.dt-body-right {text-align:right; }
	table.dataTable thead > tr > th {
		padding-right:10px;
	}

	.btn-info{
		margin-top:0;
	}

	#modal_cust label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_cust .form-control {
		display:inline-block;
		width:130px;
		clear:both;
	}


</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-1 col-xs-6">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnPdf" ><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>
			<div class="col-md-4 col-xs-6">
				<div id="reportrange" class="pull-right" style="cursor: pointer; padding: 2px 5px; border: 1px solid #ccc; width: 100%">
					<label>입고일</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="input-group">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-cust" class="btn btn-info"><i class="fa fa-search"></i>거래처 검색</button>
					</span>
					<input type="text" name="textSearch" class="form-control"  placeholder="거래처 입력" >
					
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i>현황조회</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="기간별 판매 목록" width="100%">
				<caption>재고조회</caption>
				<thead>
					<tr>
						<th width="20px"></th>
						<th width="60px">입고일</th>
						<th width="80px">매입처</th>
						<th width="80px">어종</th>
						<th class="name">매입수량</th>
						<th class="name">매입단가</th>
						<th class="name">매입액</th>
						<th class="name">매출수량</th>
						<th class="name">매출액</th>
						<th class="name">판매이익</th>
						<th class="name">현재고</th>
						<th class="name">예상단가</th>
						<th class="name">예상금액</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th class="dt-body-right">Total:</th>
						<th class='dt-body-right'></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>
<!-- 재고정리-->
<div class="modal fade" id="modal_cust" role="dialog">
	<div class="modal-dialog dialog-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처조회 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="box-body">
					<label for="strSearchCust"><i class='fa fa-circle-o text-aqua'></i> 거래처 상호</label>
					<input type="text" class="form-control cis-lang-ko" name="strSearchCust" id="strSearchCust" />

					<button type="button" class="btn btn-info btnSearchCust"><i class="fa fa-search"></i>검색</button>

					<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
						<caption>거래처 조회</caption>
						<thead>
							<tr>
								<th scope="col" class="check">코드</th>
								<th scope="col" class="subject">상호</th>
								<th scope="col" class="name">대표자</th>
								<th scope="col" class="name">전화번호</th>
								<th scope="col" class="name">선택</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		/*
		var start = moment().startOf('months');
		var end = moment().endOf("months");
		*/
		var start = moment().subtract(6, 'days');
		var end = moment( );

		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);
		/*
		$.getJSON('/sale/sale/getTotal', {
			_token			: '{{ csrf_token() }}'
		}).success(function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			$(".headerSumQty ul li:nth-child(1) input").val(data.TCASH);
			$(".headerSumQty ul li:nth-child(2) input").val(data.TQTY);
			$(".headerSumQty ul li:nth-child(3) input").val(data.TSALE);
			$(".headerSumQty ul li:nth-child(4) input").val(data.TUNCL);

		}).error(function(xhr,status, response) {

		});
		*/

		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},

			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},

			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		$.fn.dataTable.render.ellipsis = function () {
			return function ( data, type, row ) {
				//return type === 'display' && data.length > 10 ? data.substr( 0, 10 ) +'…' : data;
			}
		};

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 1000,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/stcs/stcs/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			order: [[ 1, 'asc' ], [ 3, 'asc' ]],
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": '',
					name: 'INPUT_DATE'
				},
				/*
				{
					data:   "PIS_NM",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				*/
				{ data: 'INPUT_DATE', name: 'I.INPUT_DATE' , className: "dt-body-center"},
				{
					data: 'CUST_FRNM',
					render: function ( data, type, row ) {
						if( data !== null && data.length > 6){
							return data.substr( 0, 6 ) + '…'  ;
						}else { return data;}
					},
					className : "details-control2"
				},
				{
					data: 'PIS_NM',
					render: function ( data, type, row ) {
						
						return data +  " <span class='sizes'>" + row.SIZES + "</span>";
						
					},
					className: "dt-body-left bold blue"
				},
			
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'S_QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'S_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'BENEFIT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format() ;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'REST',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format(2) ;
						}
						return data;
					},
					className: "dt-body-right"
				},

				{
					data: 'SALE_UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='text' class='editor-active uncs dt-body-right' value='" + Number(data).format() +"' />";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'RES_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='text' class='editor-active sum dt-body-right' value='" + Number(data).format() +"' />";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'SIZES', name: 'SIZES', visible:false },
				{ data: 'ORIGIN_NM', name: '', visible:false },

			],

			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 매입수량 합계
				buyQty = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );

				// 매입액 합계
				buyAmt = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 매출수량 합계
				saleQty = api.column( 7 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 매출액 합계
				saleAmt = api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 판매이익 합계
				saleMargin = api.column( 9 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 전체페이지 합계
				total = api.column( 12 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				// 현재페이지 합계
				pageTotal = api.column( 12, { page: 'current'} ).data() .reduce( function (a, b) {
						return intVal(a) + intVal(b); }, 0 );

				$( api.column( 4 ).footer() ).html(Number(buyQty.toFixed(2)).format());
				$( api.column( 6 ).footer() ).html(Number(buyAmt).format(0));
				$( api.column( 7 ).footer() ).html(Number(parseInt(saleQty)).format());
				$( api.column( 8 ).footer() ).html(Number(saleAmt).format(0));
				$( api.column( 9 ).footer() ).html(Number(saleMargin).format());
				// 전체예상금액
				$( api.column( 12 ).footer() ).html("<input type='text' class='totalSum' value='" + Number(pageTotal).format() + "' />" );
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		
		$("#search-btn").click(function(){
			
			oTable.draw();

		});

		$('#tblList tbody').on('click', 'td.details-control, .details-control2', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );
			var oData = row.data();
			
			var input_uncn	= oData.UNCS;	// 선택된 매입단가
			console.log(input_uncn);

			//console.log(oData);
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {

				$.getJSON('/stock/stock/getSaleListForInput', {
						_token			: '{{ csrf_token() }}',
						PIS_MK			: oData.PIS_MK ,
						SIZES			: oData.SIZES ,
						ORIGIN_NM		: oData.ORIGIN_NM,
						INPUT_DATE		: oData.INPUT_DATE,
						FRNM			: oData.CUST_FRNM
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						//$("label[for='new_date'] span").text(data.new_date);
						
						row.child( format(data, input_uncn) ).show();
						tr.addClass('shown');
					}).error(function(xhr,status, response) {
						
					});
				
			}
		});

		function format ( data, input_uncn ) {
			
			// `d` is the original data object for the row
			$header = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;width:80%" class="detailSaleData table table-bordered table-hover display nowrap"><thead>'+
					'<tr>'+
						'<th width="50px">매입처</th>'+
						'<th width="50px">판매일</th>'+
						'<th width="50px">매출처</th>'+
						'<th width="50px">어종</th>'+
						'<th width="50px">사이즈</th>'+
						'<th width="50px">원산지</th>'+
						'<th width="50px">입고단가</th>'+
						'<th width="50px">입고합계</th>'+
						'<th width="50px">판매수량</th>'+
						'<th width="50px">판매단가</th>'+
						'<th width="50px">판매합계</th>'+
						'<th width="100px">판매이익</th>'+
					'</tr></thead>';

			$body = "<tbody>";
			$footer = "<tfoot>";
			
			var sumQty = 0.0;
			var sumAmt = 0.0;
			var sumFit = 0.0;
			var rows = data;
			$.each(rows, function(key, value){
				
				sumQty += parseFloat(rows[key].QTY);
				sumAmt += parseFloat(rows[key].AMT);
				sumFit += rows[key].AMT - (input_uncn * parseFloat(rows[key].QTY));
				$body += "<tr>" + 
							"<td>" + rows[key].P_FRNM + "</td>"+
							"<td>" + rows[key].WRITE_DATE + "</td>"+	
							"<td>" + rows[key].S_FRNM + "</td>"+
							"<td>" + rows[key].PIS_NM + "</td>"+
							"<td class='dt-body-center'>" + rows[key].SIZES + "</td>"+
							"<td class='dt-body-center'>" + rows[key].ORIGIN_NM + "</td>"+
							"<td class='dt-body-right'>" + Number(input_uncn).format() + "</td>"+
							"<td class='dt-body-right blue'>" + Number(input_uncn * parseFloat(rows[key].QTY) ).format() + "</td>"+
							"<td class='dt-body-right'>" + Number(rows[key].QTY).format() + "</td>"+
							"<td class='dt-body-right'>" + Number(rows[key].UNCS).format() + "</td>"+
							"<td class='dt-body-right red'>" + Number(rows[key].AMT).format() + "</td>"+
							"<td class='dt-body-right green'>" + Number( rows[key].AMT - (input_uncn * parseFloat(rows[key].QTY)) ).format() + "</td>" +
						"</tr>";
			});

			$footer ="<tfoot>" + 
						"<tr>" + 
							"<td>합계</td>"+
							"<td></td>"+	
							"<td></td>"+
							"<td></td>"+
							"<td></td>"+
							"<td></td>"+
							"<td></td>"+
							"<td></td>"+
							"<td class='dt-body-right'>" + Number(sumQty).format() + "</td>"+
							"<td class='dt-body-right'></td>"+
							"<td class='dt-body-right'>" + Number(sumAmt).format() + "</td>"+
							"<td class='dt-body-right'>" + Number(sumFit).format() + "</td>"+
						"</tr>" + 
					"</tfoot>";
				;
			
			$body += "</tbody>" + $footer + "</table>";
			return $header + $body;
		}

		oTable.on("keyup", "tbody td input", function(e){

			if( !$.isNumeric( $(this).val()) ){
				$(this).parents("td").find("span").remove();
				$(this).parents("td").append("<span class='red'>숫자만 입력 가능합니다.</span>");
				$(this).next("span").fadeOut("3000");
				return false;
			}

			var ucnc = $(this).parents('tr').find("td > input.ucnc").val();
			var qty = $(this).parents('tr').find("td:nth(11)").text();
			var sum = ucnc * qty;
			// 해당 Row 합계
			$(this).parents('tr').find("td.sum").text(sum);

			var totalSum = 0;
			$("#tblList tbody tr" ).each(function(){
				totalSum += parseInt($(this).find("td:last input.sum").val());
			});

			// 해당 테이블 전체합계
			$("#tblList tfoot .totalSum ").val(totalSum);

		});


		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/sale/sale/detailList/" + data.CUST_MK + "/" + data.SEQ + "/" + data.WRITE_DATE;
		});


		$('#search-cust').click(function(){

			
			var cTable = $('#tblCustList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					lengthChange: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='strSearchCust']").val();
							d.corp_div		= 'P'
						}
					},
					order: [[ 1, 'asc' ]],
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' , className: "dt-body-center"},
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' ,className: "dt-body-center"},
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "좌측메뉴 > 코드관리 > 거래초코드로 이동하여 거래처를 먼저 추가하세요",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

			$("#modal_cust").modal({show:true});

			$('#tblCustList tbody').on( 'click', 'button', function () {
				var data = cTable.row( $(this).parents('tr') ).data();
				$("input[name='textSearch']").val(data.CUST_MK);

				$("#modal_cust").modal("hide");
			});	

			$(".btnSearchCust").click(function(){
				cTable.draw();
			});
		});

		$("#btnPdf").click(function(){

			var width=740;
			var height=720;
			
			var start_date	= $("#reportrange input[name='start_date']").val() ;
			var end_date	= $("#reportrange input[name='end_date']").val()  ;
			
			var url = "/stcs/stcs/Pdf/" + start_date + "/" + end_date;
			getPopUp(url , width, height);
		});

	});

</script>
@stop
@extends('layouts.main')
@section('class','통계관리')
@section('title','업체별 매출 통계')

@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.btn-info{
		margin-top:0;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }
	.totalSum{width:100%;}
	.ui-datepicker-calendar {
		display: none;
	 }

	.seach_area input[type='text'] { width:30%; margin-left:5px;}
	.input-group-btn{float:left;}

	.input-group-btn:after{
		clear:left;
	}

	table tr td:first-child{ width:50px;}

	table td span{display:block;}
	table td span:nth-child(2){text-align:right;}

	#modal_piscls label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_piscls .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}

	.input-group .form-control{ width:80%;}
	.input-group label { float:left; padding:8px 5px 0 0;}


	#modal_cust .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">

		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<input id="input_cust" name="input_cust" type="text"  class="form-control" placeholder="거래처" />
					<input id="input_cust_mk" name="input_cust_mk" type="hidden" />
					<span class="input-group-btn">
						<button type="button" name="searchCust" id="search-pop-btnCust" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<label>년도</label>
					<input id="input_date" name="input_date" type="text"  class="form-control"  style="width:60px;"/>
				</div>
			</div>

			<div class="col-md-4 col-xs-6">
				<div class="input-group seach_area">
					<label>어종</label>
					<input type="text" name="textPisNm"  class="form-control" placeholder="어종" value="{{ Request::Input('textSearch') }}">
					<input type="text" name="textSizes"  class="form-control" placeholder="규격" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-pop-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<button type="button" name="search" id="search" class="btn btn-info"><i class="fa fa-search"></i> 검색</button>
				<button type="button" name="search" id='btnPdf' class="btn btn-success btnPdf"><i class="fa fa-file-pdf-o"></i> 출력</button>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="기간별 판매 목록" width="100%">
				<caption>월간어종판매목록</caption>
				<thead>
					<tr>
						<th class="name">어종/규격</th>
						<th class="name">1월</th>
						<th class="name">2월</th>
						<th class="name">3월</th>
						<th class="name">4월</th>
						<th class="name">5월</th>
						<th class="name">6월</th>
						<th class="name">7월</th>
						<th class="name">8월</th>
						<th class="name">9월</th>
						<th class="name">10월</th>
						<th class="name">11월</th>
						<th class="name">12월</th>
						<th class="name">합계</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>

			<table id="tblListSum" class="table table-bordered table-hover display nowrap" summary="기간별 판매 목록" width="100%">
				<caption>어종판매합계</caption>
				<thead>
					<tr>
						<th class="name">어종/규격</th>
						<th class="name">1월</th>
						<th class="name">2월</th>
						<th class="name">3월</th>
						<th class="name">4월</th>
						<th class="name">5월</th>
						<th class="name">6월</th>
						<th class="name">7월</th>
						<th class="name">8월</th>
						<th class="name">9월</th>
						<th class="name">10월</th>
						<th class="name">11월</th>
						<th class="name">12월</th>
						<th class="name">합계</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		var start = moment().subtract(1500, 'days');
		var end = moment();

		// 초기값 세팅

		$("#input_date").val( moment().format('YYYY') );
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');	
		}

		$('#input_date').datepicker({
			changeYear: true,
			showButtonPanel: true,
			closeText: "확인",
			currentText: '올해', 
			dateFormat: 'yy',
			onClose: function(dateText, inst) {
				var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(year, 1));

				$("input[name='input_date']").trigger('change');
			}
		});

		$("#input_date").focus(function () {
			$(".ui-datepicker-month").hide();
		});


		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},

			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},

			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		$.fn.dataTable.render.ellipsis = function () {
			return function ( data, type, row ) {
				//return type === 'display' && data.length > 10 ? data.substr( 0, 10 ) +'…' : data;
			}
		};

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			sDom: 'rt',
			columnDefs: [
				{ "width": "100px", "targets": 0 },
			],
			fixedColumns: true,
			ajax: {
				url: "/stcs/stcs/getDataCustSale",
				data:function(d){
					d.CUST_MK		= $("input[name='input_cust_mk']").val() == "" ? "{{Request::segment(4)}}" : $("input[name='input_cust_mk']").val();;
					d.SIZES			= $("input[name='textSizes']").val();
					d.YEAR			= $("input[name='input_date']").val();
					d.PIS_MK		= $("input[name='textPisNm']").val();
				}
			},
			columns: [

				{
					data: 'PIS_NM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + data + "</span><span>" + row.SIZES + "</span>";
						}
						return data;
					},
				},
				{
					data: 'Q1M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A1M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q2M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A2M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q3M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A3M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q4M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A4M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q5M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A5M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q6M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A6M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q7M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A7M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q8M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A8M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q9M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A9M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q10M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A10M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q11M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A11M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q12M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A12M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() +  "Kg </span><span>" + Number(row.TAMT).format() + " 만원</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},

			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		oTable.on("keyup", "tbody td input", function(e){

			if( !$.isNumeric( $(this).val()) ){
				$(this).parents("td").find("span").remove();
				$(this).parents("td").append("<span class='red'>숫자만 입력 가능합니다.</span>");
				$(this).next("span").fadeOut("3000");
				return false;
			}

			var ucnc = $(this).parents('tr').find("td > input.ucnc").val();
			var qty = $(this).parents('tr').find("td:nth(11)").text();
			var sum = ucnc * qty;
			// 해당 Row 합계
			$(this).parents('tr').find("td.sum").text(sum);

			var totalSum = 0;
			$("#tblList tbody tr" ).each(function(){
				totalSum += parseInt($(this).find("td:last input.sum").val());
			});

			// 해당 테이블 전체합계
			$("#tblList tfoot .totalSum ").val(totalSum);

		});

		
		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/sale/sale/detailList/" + data.CUST_MK + "/" + data.SEQ + "/" + data.WRITE_DATE;
		});



		var sTable = $('#tblListSum').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			sDom: 'rt',
			fixedColumns: true,
			ajax: {
				url: "/stcs/stcs/getDataCustSaleTotal",
				data:function(d){

					d.CUST_MK		= $("input[name='input_cust_mk']").val() == "" ? "{{Request::segment(4)}}" : $("input[name='input_cust_mk']").val();;
					d.SIZES			= $("input[name='textSizes']").val();
					d.YEAR			= $("input[name='input_date']").val();
					d.PIS_MK		= $("input[name='textPisNm']").val();
				}
			},
			columns: [

				{
					data: "Q1M",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "합계</span>";
						}
						return data;
					},
				},
				{
					data: 'Q1M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A1M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q2M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A2M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q3M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A3M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q4M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A4M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q5M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A5M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q6M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A6M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q7M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A7M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q8M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A8M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q9M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A9M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q10M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A10M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q11M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A11M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'Q12M',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() + "</span><span>" + Number(row.A12M).format() + "</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TQTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<span>" + Number(data).format() +  " Kg</span><span>" + Number(row.TAMT).format() + " 만원</span>";
						}
						return data;
					},
					className: "dt-body-right"
				},

			],
			fnDrawCallback: function() {
				$("#tblListSum thead").remove();

				$("#tblList thead > tr th").each(function(i){
					$("#tblListSum td:eq(" + i + ")").css("width", $(this).css("width"));
				});


			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
			sTable.draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
			sTable.draw() ;
		});

		// 전체검색
		$("input[name='input_date']").on("change", function(){
			oTable.draw() ;
			sTable.draw() ;
		});

		// 어종/규격 검색코드
		$("#search-pop-btn").click(function(){
			$('#modal_piscls').modal({show:true});
		});

		$('#modal_piscls').on('hidden.bs.modal', defaltModalInsert);

		// 어종 입력 초기화
		function defaltModalInsert(){
			$("#modal_piscls input[type='text']").val("");
			$('.edit_alert').hide().find('ul').empty();
			$(".slideSearchCust" ).slideUp();

			oTable.draw() ;
			sTable.draw() ;
		}

		// 어종 조회 버튼
		$("#btnPisInfoList").click(function(){
			$( ".slideSearchCust" ).show(function(){
				var cTable = $('#tblPisInfoList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					ajax: {
						url: "/piscls/piscls/listData",
						data:function(d){
							d.textSearch	= $("input[name='strSearchPis']").val();
						}
					},
					columns: [
						{ data: 'PIS_MK', name: 'PIS.PIS_MK' },
						{ data: 'PIS_NM', name: 'PIS_NM' },
						{ data: 'SIZES',  name: 'SIZES' },
						{ data: 'REMARK',  name: 'REMARK' },

						{
							data:   "PIS_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span>선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},

					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblPisInfoList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();
					$("#modal_piscls_mk").val(data.PIS_MK);
					$("#modal_piscls_nm").val(data.SIZES);

					$("input[name='textPisNm']").val(data.PIS_MK);
					$("input[name='textSizes']").val(data.SIZES);

					$("input[name='strSearchPis']").val('');
					$(".slideSearchCust" ).slideUp();
				});

				$("input[name='strSearchPis']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});

				// 거래처 검색 닫기
				$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });


			});
		});

		// 전체검색
		$("#search").click(function(){
			oTable.draw() ;
			sTable.draw() ;
		});


		// 거래처 조회
		$("#btnSearchCust").click(function(){
			$( ".slideSearchCust" ).show(function(){

				var cTable = $('#tblCustList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					bInfo		: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='strSearchCust']").val();
							d.corp_div		= 'S'
						}
					},
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' , className: "dt-body-center"},
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' ,className: "dt-body-center"},
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}

				});

				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();

					$("#modal_cust_cust_mk").val(data.CUST_MK);
					$("#modal_cust_cust_nm").val(data.FRNM);

					$("input[name='input_cust_mk']").val(data.CUST_MK);
					$("input[name='input_cust']").val(data.FRNM);
					$("input[name='strSearchCust']").val('');
					$(".slideSearchCust" ).slideUp();
				});

				$("input[name='strSearchCust']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			});
		});

		// 거래처 검색 닫기
		$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });

		$("#search-pop-btnCust").click(function(){
			$('#modal_cust').modal({show:true});
		});

		$('#modal_cust').on('hidden.bs.modal', defaltModalInsert);

		/*
		// 업체별 판매현황 출력
		$(".btnPdf").click(function(){


			$("form[name='getPdf'] > input[name='YEAR']").val( $(".input-group input[name='input_date']").val() );

			if( $(".input-group input[name='input_cust_mk']").val() == ""){
				$("form[name='getPdf'] > input[name='CUST_MK']").val( "{{Request::segment(4)}}" );
			}else{
				$("form[name='getPdf'] > input[name='CUST_MK']").val( $(".input-group input[name='input_cust_mk']").val() );
			}
			$("form[name='getPdf'] > input[name='PIS_MK']").val( $(".input-group input[name='textPisNm']").val() );
			$("form[name='getPdf'] > input[name='SIZES']").val( $(".input-group input[name='textSizes']").val() );


			$("form[name='getPdf']").submit();
		});
		*/

		// 업체별 판매현황 출력
		$(".btnPdf").click(function(){

			var width=740;
			var height=720;
			var CUST_MK		= "{{Request::segment(4)}}";
			var SIZES		= $(".input-group input[name='textSizes']").val();
			var YEAR		= $(".input-group input[name='input_date']").val();
			var PIS_MK		= $(".input-group input[name='textPisNm']").val();

			var url = "/stcs/stcs/PdflistCustSaleDetail?CUST_MK=" + CUST_MK + "&YEAR=" + YEAR + "&SIZES=" + SIZES + "&PIS_MK=" + PIS_MK;
			getPopUp(url , width, height);

		});

	});

</script>
<!--
<div id="pdf">
	<form name="getPdf" method="post" action="/stcs/stcs/PdflistCustSaleDetail" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="CUST_MK" value="{{Request::segment(4)}}"/>
		<input type="hidden" name="SIZES"  />
		<input type="hidden" name="YEAR"  />
		<input type="hidden" name="PIS_MK"  />
		<input type="hidden" name="input_date"  />
	</form>
</div>
 -->
<!-- 어종조회 팝업 -->
<div class="modal fade" id="modal_piscls" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">

			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 어종조회 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<input type="hidden" name="chkPIS_MK" value="" />
				<input type="hidden" id="mode" value="" />
				<div class="form-group">
					<label for="modal_piscls_mk"><span class="glyphicon glyphicon-ok" ></span> 어종부호</label>
					<input type="text" class="form-control" id="modal_piscls_mk" name="PIS_MK" placeholder="어종부호" readonly="readonly" />
					<button type="button" name="search" id="btnPisInfoList" class="btn btn-info"><i class="fa fa-search"></i></button>
					<input type="text" class="form-control" id="modal_piscls_nm" name="PIS_NM" placeholder="어종명" readonly="readonly" />
					<span class="msg"></span>
				</div>
				<div class="form-group slideSearchCust">
					<div class="box box-primary">
						<div class="box-body">
							<label for="strSearchPis"><span class="glyphicon glyphicon-ok"></span> 어종명</label>
							<input type="text" class="form-control" name="strSearchPis" id="strSearchPis" />
							<table id="tblPisInfoList" class="table table-bordered table-hover display nowrap" summary="어종코드 조회">
								<caption>어종코드 입력</caption>
								<thead>
									<tr>
										<th scope="col" class="name">어종부호</th>
										<th scope="col" class="name">어종명</th>
										<th scope="col" class="name">규격</th>
										<th scope="col" class="name">비고</th>
										<th scope="col" class="name"></th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-default pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="modal_cust" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 15px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 검색 </h4>
			</div>
			<div class="modal-body" style="padding:10px 15px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_cust_cust_mk"><i class='fa fa-circle-o text-aqua'></i> 거래처</label>
					<input type="text" class="form-control" id="modal_cust_cust_mk" placeholder="거래처코드" readonly="readonly">
					<input type="text" class="form-control" id="modal_cust_cust_nm" placeholder="거래처명" readonly="readonly">
					<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>

					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5><span class="glyphicon glyphicon-th-large"></span> 거래처 선택</h5>

							<label for="strSearchCust"><i class='fa fa-circle-o text-aqua'></i> 거래처 상호</label>
							<input type="text" class="form-control" name="strSearchCust" id="strSearchCust" />

							<button type="button" class="btn btn-danger btn-default pull-right btn_CustClose">
								<span class="glyphicon glyphicon-remove"></span> 닫기
							</button>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>어종 조회</caption>
									<thead>
										<tr>
											<th scope="col" class="check">코드</th>
											<th scope="col" class="subject">상호</th>
											<th scope="col" class="name">대표자</th>
											<th scope="col" class="name">전화번호</th>
											<th scope="col" class="name">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

@stop
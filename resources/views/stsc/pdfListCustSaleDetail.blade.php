<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>판매현황</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13.5px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>

	</head>
	<body>
		<script type="text/php">

			if ( isset($pdf) ) {

				$size = 10;
				$color = array(0,0,0);
				if (class_exists('Font_Metrics')) {
					$font = Font_Metrics::get_font("NanumGothic");
					$text_height = Font_Metrics::get_font_height($font, $size);
					$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
				} elseif (class_exists('Dompdf\\FontMetrics')) {
					$font = $fontMetrics->getFont("NanumGothic");
					$text_height = $fontMetrics->getFontHeight($font, $size);
					$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
				}

				$foot = $pdf->open_object();

				$w = $pdf->get_width();
				$h = $pdf->get_height();

				// Draw a line along the bottom
				$y = $h - $text_height - 24;
				$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

				$pdf->close_object();
				$pdf->add_object($foot, "all");

				$text = "{PAGE_COUNT} - {PAGE_NUM}";  

				// Center the text
				$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

			}
		</script>
		<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'> {{$custfrnm}} 판매현황</span>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="text-align:left"> {{$year}} 년도</td>
				<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
			</tr>
			
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			
			<tr>
				<td style="text-align:right">(단위 : Kg/만원)</td>
			</tr>
		</table>
		<table id="tblList" summary="어종별 재고조회" width="100%" style="border-bottom:3px solid block;" border="1" >
			<caption>판매일보 상세</caption>
			<thead style="border-left:none;border-right:none;border-top:3px solid block; border-bottom:3px solid block;padding-top:5px;">
				<tr>
					<td style='text-align:center;' >No</td>
					<td style='text-align:center;' >어종/규격</td>
					<td style='text-align:center;'>1월</td>
					<td style='text-align:center;'>2월</td>
					<td style='text-align:center;'>3월</td>
					<td style='text-align:center;'>4월</td>
					<td style='text-align:center;'>5월</td>
					<td style='text-align:center;'>6월</td>
					<td style='text-align:center;'>7월</td>
					<td style='text-align:center;'>8월</td>
					<td style='text-align:center;'>9월</td>
					<td style='text-align:center;'>10월</td>
					<td style='text-align:center;'>11월</td>
					<td style='text-align:center;'>12월</td>
					<td style='text-align:center;'>합계</td>
				</tr>
			</thead>
			<tbody>
				@foreach ($list as $key => $item)
				<tr>
					<td style='text-align:center;border-right:1px solid black;border-bottom:none;'> {{ ++$key }}</td>
					<td style='text-align:center;border-right:1px solid black;border-bottom:none;'> {{ $item->PIS_NM}}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q1M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q2M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q3M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q4M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q5M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q6M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q7M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q8M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q9M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q10M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q11M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->Q12M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TQTY)  }} KG</td>
				</tr>
				<tr>
					<td style='text-align:center;border-right:1px solid black;border-top:none;'> </td>
					<td style='text-align:center;border-right:1px solid black;border-top:none;'> [{{ $item->SIZES}}]</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A1M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A2M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A3M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A4M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A5M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A6M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A7M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A8M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A9M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A10M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A11M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A12M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TAMT)  }} 만원</td>
				</tr>
				@endforeach
				@foreach ($sum as $key => $item)
				<tr>
					<td style='text-align:center;border-right:1px solid black;border-bottom:none;border-top:3px double black;'></td>
					<td style='text-align:center;border-right:1px solid black;border-top:3px double black;'>수 량 합 계</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q1M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q2M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q3M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q4M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q5M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q6M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q7M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q8M) }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q9M)  }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q10M)  }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q11M)  }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->Q12M)  }}</td>
					<td style='text-align:right;padding-right:5px;border-top:3px double black;'>{{ number_format($item->TQTY)  }} KG</td>
				</tr>
				<tr>
					<td style='text-align:center;border-right:1px solid black;border-top:none;'></td>
					<td style='text-align:center;border-right:1px solid black;'>금 액 합 계</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A1M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A2M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A3M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A4M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A5M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A6M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A7M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A8M) }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A9M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A10M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A11M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->A12M)  }}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format($item->TAMT)  }} 만원</td>
				</tr>
				@endforeach
			</tbody>
		</table>	
	</body>
</html>
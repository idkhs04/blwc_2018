<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}
			thead{
				width:100%;
				height:109px;
				font-family:NanumGothic;  
			}
			table > tr > td { text-align:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}
			table, th, td {
				border-bottom: 1px solid black;
			}
			span.title { text-align:center;}
		</style>
	</head>
	<body>
		<div class="col-md-12">
			<div class="box box-primary"> 
				<div class="box-body">
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>미 수 장 부</span> 
					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="100%" style="border-bottom:1px solid block;margin-top:15px;">
						<caption>미수금</caption>
						<thead style="border-bottom:3px double block;padding-top:5px;">
							<tr>
								<th style='text-align:center;' height="30px" >상&nbsp;&nbsp;&nbsp;&nbsp;호</th>
								<th style='text-align:center;'>총금액</th>
								<th style='text-align:center;'>입금액</th>
								<th style='text-align:center;'>잔&nbsp;&nbsp;&nbsp;&nbsp;액</th>
								<th style='text-align:center;'>입금액</th>
								<th style='text-align:center;'>잔&nbsp;&nbsp;&nbsp;&nbsp;액</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
							<tr>
								<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->FRNM}}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format( $item->TOTAL_UNCL_AMT)}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							@endforeach
							<tr>
								<td style='text-align:center;' height="30px">총&nbsp;&nbsp;&nbsp;&nbsp;계</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format( $sum)}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
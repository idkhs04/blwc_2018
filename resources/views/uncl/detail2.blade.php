@extends('layouts.main')
@section('class','비용관리')
@section('title','매출 미수정보 상세조회')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>

	.detail_detailList th{
		width:50px;
	}
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblSaleList_filter { display:none;}

	.slideSearchPisMk, .slideSearchSale{
		display:none;
	}

	#tblList_Sum{
		float:right;
	}

	#SUM_QTY{ padding-left:2px;}

	span.details-control{
		display:inline-block;
		width:20px;
		height:20px;
	}
	
	tr.shown td.details-control, span.details-control-close {
		background: url('/image/details_close.png') no-repeat center left;
		width:20px;
		height:20px;
		display:inline-block;
	}

	td.details-control, span.details-control {
		background: url('/image/details_open.png') no-repeat center left;
		width:20px;
		height:20px;
		cursor: pointer;
		display:inline-block;
	}
	
	td th {
		width: 30px;
		text-align: right;
	}

	.red {color:red;}

	.form-group.dt-body-right{text-align:right;}
	
	.dt-body-right{text-align:right;}
	.headerSumQty {
		text-align: right;
	}

	.headerSumQty > ul{ padding-left:0; margin-bottom:0;}
	.headerSumQty label{ margin-right:5px;}
	.headerSumQty{ text-align:right;}
	.headerSumQty li {
		display: inline-block;
		padding: 0 10px;
	}

	.SumQtyHeader {
		padding-top:6px;
		padding-bottom:0px;
	}

	table.dataTable thead > tr > th { padding-right:0; padding:2px 3px;}
	.GROUP_CHILD{ display:none;}


	.modal .form-group > label:first-child {
		width: 130px;
	}
	.modal input.form-control {
		clear:both;
		width:130px;
		display:inline-block;
	}

	.padding-130{
		padding-left:130px;
	}

	.inl-blk{
		display:inline-block;
	}
	
	.slideSearchSale {margin-top:5px; display:none; }
	
	.font12{
		font-size:12px;
	}

</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-12 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					<!--<button type="button" class="btn btn-success" id="btnDatail"><i class="fa fa-list"></i> 기본형식</button>-->
					<button type="button" class="btn btn-danger" id="btnDeleteUncl" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info" id="btnAdd" ><i class="fa fa-plus"></i> 추가</button>
					<button type="button" class="btn btn-success btnPdfDetail"><i class="fa fa-file-pdf-o"></i> 미수장부출력</button>
					<!--<button type="button" class="btn btn-success btnPdfDetailTest"><i class="fa fa-file-pdf-o"></i> 개발자테스트중</button>-->
				</div>
				<div id="reportrange" class="btn-group" style="cursor: pointer; padding: 4px 10px; border: 1px solid #ccc;">
					<label>일자</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<!--
			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">

					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>

				</div>
			</div>
			-->
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header SumQtyHeader">
			<div class="form-group dt-body-right headerSumQty">
				<ul class="">
					<li>
						<label for="txtDicountText"><span class="glyphicon glyphicon-leaf"></span> 그룹</label>
						{{$CUST->CUST_GRP_NM}}</li>
					<li>
						<label for="txtDicountText"><span class="glyphicon glyphicon-leaf"></span> 거래처명</label>
						{{$CUST->FRNM}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-user"></span> 대표자명</label>
						{{$CUST->RPST}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-grain"></span> 사업자번호</label>
						{{$CUST->ETPR_NO}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-inbox"></span> 총 미수액</label>
						<span class="red TotalRecord">총미수액 로딩중..</span></li>
				</ul>
			</div>

		</div>
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="미수금 목록" width="100%">
				<caption>미수금 상세조회</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="50px">일자</th>
						<th width="65px">품목</th>
						<th width="40px">수량</th>
						<th width="40px">단가</th>
						<th width="60px">판매금액</th>
						<th width="60px">입금</th>
						<th width="60px">미입금</th>
						<th width="60px">총잔액</th>
						<th width="30px">상세</th>
						<th width="60px">할인금액</th>
						<th width="90px">메모</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="2" class='dt-body-right'></th>
						<th class='dt-body-right'></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th class='dt-body-right'></th>
						<th class='dt-body-right'></th>
						<th class='dt-body-right'></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		var mode = "{{$MODE}}";
		
		var inputAmt		= 0;
		var noInputAmt		= 0;
		var ArrangeSumAmt	= 0;

		var seq_index = -999;
		
		var start = moment().subtract(6, 'days');
		var end = moment( );

		var today = end.format('YYYY-MM-DD');
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');

			
		}

		// 미수금정보 입력 거래일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 미수금정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			opens:"left",
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		$("#modal_stock_output_date").val(end.format('YYYY-MM-DD'))

		// 해당 거래처 총 미수금
		getTotalUncl();
		function getTotalUncl(){
			$.getJSON('/sale/sale/getTotalUncl', {
				_token : '{{ csrf_token() }}',
				UNCL_CUST_MK : "{{Request::segment(4)}}"
			},function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("span.TotalRecord").text(Number(data.TOTAL_UNCL).format() + "원");
			});
		}

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			paging:   false,
			ordering: false,
			info:     false,
			bLengthChange: false,
			bInfo : false,
			bPaginate:false,
			ajax: {
				url: "/uncl/uncl/detailListData2",
				data:function(d){
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.cust_mk		= "{{Request::segment(4)}}";
				}
			},
			fnRowCallback: function(nRow, nData){
				
				if( nData.SEQ !== seq_index && ( nData.QTY == '' || nData.QTY === null)){
					$(nRow).addClass('GROUP_PARENT').attr('data-SEQ', nData.SEQ).attr('data-WT', nData.WRITE_DATE);
					
					
				}else{
					$(nRow).addClass('GROUP_CHILD').attr('data-SEQ', nData.SEQ).attr('data-WT', nData.WRITE_DATE);;
				}
				
				if( nData.REMARK !== null && ( !(nData.REMARK.indexOf("판매 미수") != -1 || nData.REMARK.indexOf("판매미수") != -1)) ){ 
					
					if( !$.isNumeric(nData.AMT)){
						$(nRow).hide();
						return $(nRow);
					}
					//ArrangeSumAmt	= inputAmt - noInputAmt;
				}

				//console.log(nData);
				if( nData.PROV_DIV == "0" && nData.QTY === null){		
					noInputAmt		+= $.isNumeric(parseInt(nData.AMT)) ? parseInt(nData.AMT) : 0;
				}

				if( nData.PROV_DIV == "1" && nData.QTY === null){

					//console.log(nData.PROV_DIV);
					inputAmt		+= $.isNumeric(parseInt(nData.AMT)) ? parseInt(nData.AMT) : 0;
					$(nRow).addClass('red')
				}

			},
			fnDrawCallback: function () {
				var api = this.api();
				// 기간별 소계
				//$( api.column( 2 ).footer() ).html(Number(ArrangeSumAmt).format() );

				$( api.column( 6 ).footer() ).html(Number(inputAmt).format() );
				$( api.column( 7 ).footer() ).html(Number(noInputAmt).format() );

				//$( api.column( 2 ).footer() ).html(Number((parseInt(inputAmt) - parseInt(noInputAmt))).format() );
				
				// 마지막엔 초기화
				inputAmt		= 0;
				noInputAmt		= 0;
				ArrangeSumAmt	= 0;

			},
			columns: [
				{
					data: "SEQ",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'WRITE_DATE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.QTY !== null && row.QTY != "null"){
								return '';
							}else{
								return data;
							}
						}
						return data;
					},
					className: "dt-body-center"
				},
				
				{
					data: 'REMARK',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.QTY !== null && row.QTY != "null"){
								//return "&nbsp;&nbsp;&nbsp;<i class='fa fa-fw fa-angle-right'></i>" + data;
								return data;
							}else{
								return "<b>" + data + "</b>";
							}
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data !== null){
								return Number(data).format();
							}else{
								return '';
							}
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'UNCS',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'TOTAL_SALE_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.QTY == null || row.QTY == "null"){
								return Number(data).format();
							}else{
								return Number(row.AMT).format();
							}
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.PROV_DIV== "1" && ( row.QTY == '' || row.QTY === null) ) {
								return Number(data).format();
							}else{
								return '';
							}					
						}else{
							return '';
						}
						//return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.PROV_DIV== "0"){
								return Number(row.AMT).format();
							}else{
								return '';
							}
							
						}
						return '';
					},
					className: "dt-body-right"
				},
					/*
				{
					data: 'TOT_AMT',
					render: function ( data, type, row ) {
						if( row.QTY == null || row.QTY == "null"){
							return Number(row.AMT).format();
						}
						return '';
					},
					className: "dt-body-right"
				},
				*/
				{
					data: 'SEQ',
					render: function ( data, type, row ) {
						if( row.QTY == null || row.QTY == "null"){
							return Number(row.TOT_AMT).format();
						}
						return '';
					},
					className: "dt-body-right"
				},
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if( data == null || data == "null"){
							return "<span class='details-control dt-body-center' data-target='.group_" + row.SEQ + "'></span>";
						}
						return '';
					},
					orderable:      false,
				},
				{
					data: 'SALE_DISCOUNT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.QTY == null || row.QTY == "null"){
								return Number(data).format();
							}
						}
						return '';
					},
					className: "dt-body-right",
					orderable:      false,
				},
				{
					data: 'MEMO',
					render: function ( data, type, row ) {
						if( type === 'display'){
							return data;
						}
						return '';
					},
					
				},
				{
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		
		$('#tblList tbody').on('click', 'span.details-control', function () {
			
			var seq = $(this).parent('td').parent('tr').attr('data-SEQ');
			var wt	= $(this).parent('td').parent('tr').attr('data-WT');
			var hasP = $(this).parent('td').parent('tr').hasClass('GROUP_PARENT');

			$("tr.GROUP_CHILD[data-seq='" +seq+"'][data-WT='" + wt + "']").css('display', 'table-row');
			$(this).attr('class', 'details-control-close')
		});

		$('#tblList tbody').on('click', 'span.details-control-close', function () {
			
			var seq = $(this).parent('td').parent('tr').attr('data-SEQ');
			var wt	= $(this).parent('td').parent('tr').attr('data-WT');
			var hasP = $(this).parent('td').parent('tr').hasClass('GROUP_PARENT');
			$("tr.GROUP_CHILD[data-seq='" +seq+"'][data-WT='" + wt + "']").css('display', 'none');
			$(this).attr('class', 'details-control')
		});
			

		$('#tblList tbody').on('click', 'td.details-control', function () {

			var tr = $(this).closest('tr');
			var row = oTable.row( tr );

			
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format( row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});

		$("input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ 
			localStorage["IS_BACK"]		= "Y";
			location.href="/uncl/uncl/index"; 
		});

		
		$("#btnDatail").click(function(){ location.href="/uncl/uncl/detail/{{Request::segment(4)}}"; });

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});


		
		// 미수금 입력 팝업
		$("#btnAdd").click(function(){
			defaltModalInsert();
			
			$('#modal_uncl_insert')
			/*
			.modal({
				backdrop: "static"
			})
			*/
			.draggable({
				handle: ".modal-header",
				cursor: 'move',
			})
			.modal({
				backdrop: "static"
			});

			$('.modal >.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');

			setTimeout(function(){
				$("#modal_amt").focus();
			},500);
		});

		// 비고 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 미수금 저장 버튼
		$("#btnInsertSave").click(function(){
			
			if( $("#modal_sale_yn").is(":checked") ){
				
				if( $("#modal_sale_dt").val() != "" ) {
					if( ! $.isNumeric( $("#modal_sale_yn").val()) ){
						$(".modal-body > .alert").remove();
						$(".modal-body").prepend( getAlert('warning', '경고', "판매정보 연동이 체크될때는 반드시 판매정보 하나를 선택해주세요") ).show();
						return false;
					}
				}
			}
			var DE_CR_DIV	= $("#modal_uncl_insert input[name='DE_CR_DIV']:checked").val();

			$.getJSON('/uncl/uncl/setUnclData', {
				_token			: '{{ csrf_token() }}',
				WRITE_DATE		: $("#modal_write_dt").val() ,
				UNCL_CUST_MK	: "{{Request::segment(4)}}" ,
				PROV_DIV		: DE_CR_DIV ,
				AMT				: removeCommas($("#modal_amt").val()),
				REMARK			: $("#modal_remark_text").val(),
				UNCL_SALE_SEQ	: $("#modal_sale_yn").is(":checked") ? $("#modal_sale_yn").val() : '',
				SALE_WRITE_DATE : $("#modal_sale_yn").is(":checked") ? $("#modal_sale_dt").val() : '',
				MEMO			: $("#modal_memo").val()
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if (data.result == "success") {
					$('#modal_uncl_insert').modal("hide");
					oTable.draw() ;
					getTotalUncl();
				}

			}).fail(function(xhr){
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();

			});
		});

		// 미수금 입력 초기화
		function defaltModalInsert(){
			
			$("#modal_uncl_insert > div.modal-dialog").attr("class", "modal-dialog modal-md");
			$("#modal_sale_yn").prop("checked", false);
			$("#modal_sale_yn").val("")
			$("div.slideSearchSale").hide();

			$(".edit_alert ul").empty();
			$("#modal_uncl_insert input[type='text']").val("");
			$("#modal_write_dt").val(today);


			$("#modal_uncl_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_uncl_insert input[name='modal_remark']:first").prop("checked", true);

			$("#modal_uncl_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_uncl_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$("#modal_remark_text").val( $("input[name='modal_remark']:checked").val());
			
			$("#modal_remark_text").focus();
		}

		// 미수금 정보 삭제
		$("#btnDeleteUncl").click(function(){

			var rowData = oTable.row( $("#tblList tbody tr > td > input[type=checkbox]:checked").parents('tr') ).data();
			var selectedSEQ = rowData.SEQ;
			
			// 판매미수내역은 삭제 할 수없도록 막음 : 2017-01-03
			if( rowData.PIS_NM !== null && rowData.PROV_DIV == "0" && rowData.QTY === null){
				$(".content > .alert").remove();
				$(".content").prepend( getAlert('warning', '경고', "판매미수 내역은 삭제할 수 없습니다") ).show();
				return false;
			}

			if( chkInputTble("tblList", "미수금 삭제") ){

				$.getJSON('/uncl/uncl/delete', {
					_token			: '{{ csrf_token() }}',
					WRITE_DATE		: rowData.WRITE_DATE,
					SEQ				: rowData.SEQ ,
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					if (data.result == "success") {
						$('#modal_uncl_insert').modal("hide");
						oTable.draw() ;
						getTotalUncl();
					}

				}).fail(function(xhr){
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('.edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();

				});
			}
		});
		
		$(".btnPdfDetailTest").click(function(){

			var width=740;
			var height=720;
			var CUST_MK			= "{{Request::segment(4)}}";
			var start_date		= $("#reportrange input[name='start_date']").val();
			var end_date		= $("#reportrange input[name='end_date']").val();

			var url = "/uncl/uncl/pdfDetailCustTest/" + CUST_MK + "?start_date=" + start_date + "&end_date=" + end_date;
			getPopUp(url , width, height);

		});
		
		$(".btnPdfDetail").click(function(){
			
			var width=740;
			var height=720;
			var CUST_MK			= "{{Request::segment(4)}}";
			var start_date		= $("#reportrange input[name='start_date']").val();
			var end_date		= $("#reportrange input[name='end_date']").val();

			var url = "/uncl/uncl/pdfDetailCust/" + CUST_MK + "?start_date=" + start_date + "&end_date=" + end_date;
			getPopUp(url , width, height);

		});

		// 리스트에서 입력모드일때 이벤트
		if( "{{Request::segment(5)}}" == "INSERT"){
			defaltModalInsert();
			$('#modal_uncl_insert').modal({show:true});

			setTimeout(function(){
				$("#modal_amt").focus();
			},500);
			
		}

		// 일일판매 조회
		$("#modal_sale_yn").click(function(){
			
			if( $(this).is(":checked") ) {
				// 일일판매조회 창 보이기
				$("#modal_uncl_insert > div.modal-dialog").attr("class", "modal-dialog modal-lg");
				$( ".slideSearchSale" ).show(function(){
					
					setTimeout(function(){
						$("#strSearchCust").focus();
					},500);
					
					$("#modal_sale_dt").val(end.format('YYYY-MM-DD'));

					var cTable = $('#tblSaleList').DataTable({
						processing	: true,
						serverSide	: true,
						iDisplayLength: 150,		// 기본 100개 조회 설정
						responsive: true,
						lengthChange: false,
						bInfo:false,
						ajax: {
							url: "/sale/sale/listData",
							data:function(d){
								d.srtCondition	= "ALL";
								d.start_date	= $("input[id='modal_sale_dt']").val();
								d.end_date		= $("input[id='modal_sale_dt']").val();
								d.CUST_MK		= "{{request::segment(4)}}";
								d.UNCL_SALE_SEQ	= $("#modal_sale_yn").val();
							}
						},
						order: [[ 1, 'asc' ]],
						columns: [
							{
								data: "SEQ",
								name: "SM.SEQ",
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return "<input type='checkbox' class='editor-active' value='" + data +"' >";
									}
									return data;
								},
								orderable:      false,
								className: "dt-body-center"
							},
							{ data: 'WRITE_DATE', name: 'SM.WRITE_DATE',className: "dt-body-center" },
							
							{ data: "PIS",  name: 'P.PIS_NM', className: "dt-body-left"},
							{ 
								data: 'SUMQTY', 
								name: 'SD.SUMQTY',
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							{ 
								data: 'TOTAL_SALE_AMT', 
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							{ 
								data: 'CASH_AMT', 
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							{ 
								data: 'UNCL_AMT', 
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							{ 
								data: 'CARD_AMT', 
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							{ 
								data: 'SALE_DISCOUNT', 
								render: function ( data, type, row ) {
									if ( type === 'display' ) {
										return Number(data).format() ;
									}
									return data;
								},
								className: "dt-body-right"
							},
							
						],
						"searching": true,
						"paging": true,
						"autoWidth": true,
						"oLanguage": {
							"sLengthMenu": "조회수 _MENU_ ",
							"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
							"sProcessing": "현재 조회 중입니다",
							"sEmptyTable": "조회된 데이터가 없습니다",
							"sZeroRecords": "조회된 데이터가 없습니다",
							"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
							"oPaginate": {
								"sFirst": "처음",
								"sLast": "끝",
								"sNext": "다음",
								"sPrevious": "이전"
							}
						}

					});

					$("input[name='modal_sale_dt']").on("change", function(){
						cTable.draw() ;
					});

					/** 그리드 선택  **/
					$('#tblSaleList tbody').on( 'click', 'tr', function () {
						
						$(this).parents("tbody").find("tr td input[type=checkbox]").prop("checked", false);

						$(this).find("td > input[type=checkbox]").prop("checked", true);
						
						var data = cTable.row($(this)).data();
						$("#modal_sale_yn").val( data.SEQ );
						$("#modal_amt").val( Number(data.UNCL_AMT).format());
						
					});

				
				});
			}else{
				$("#modal_uncl_insert > div.modal-dialog").attr("class", "modal-dialog modal-md");
				$("div.slideSearchSale").hide(); 
				setTimeout(function(){
					$("#modal_sujo_outline").focus();
				},500);
			}
		});

		
		
		// 거래처 검색 닫기
		$(".btn_SaleClose").click(function(){
			$("#modal_uncl_insert > div.modal-dialog").attr("class", "modal-dialog modal-md");
			$("div.slideSearchSale").hide(); 
			setTimeout(function(){
				$("#modal_sujo_outline").focus();
			},500);
		});
		
		$("#modal_sale_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

	});

</script>

<!-- 미수금 추가 팝업 -->
<div class="modal fade" id="modal_uncl_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 미수금 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_write_dt"><i class='fa fa-check-circle text-red'></i> 거래일자</label>
					<input type="text" class="form-control" id="modal_write_dt"  />
				</div>
				<div class="form-group">
					<label for="modal_remark_text"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					<input type="text" class="form-control cis-lang-ko" id="modal_remark_text" value='현금'/>
					<div class='padding-130'>
						<label>
							<input type="radio" name="modal_remark" class="minimal" checked value="현금" />현금
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="통장(입금)" />통장(입금)
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="CARD" />CARD
						</label>
						
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="" />직접입력
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="판매미수" />판매미수
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_div"><i class='fa fa-circle-o text-aqua'></i> 구분</label>
					<div class='inl-blk'>
						<label for="DE_CR_DIV_1">
							<input type="radio" name="DE_CR_DIV" class="minimal" value="1" id="DE_CR_DIV_1"/>입금
						</label>

						<label for="DE_CR_DIV_0">
							<input type="radio" name="DE_CR_DIV" class="minimal" checked value="0" id="DE_CR_DIV_0"/>미입금
						</label>

					</div>
				</div>
				<div class="form-group">
					<label for="modal_amt"><i class='fa fa-check-circle text-red'></i> 금액</label>
					<input type="text" class="form-control numeric" id="modal_amt" placeholder="금액 입력">
				</div>

				<div class="form-group">
					<label for="modal_memo"><i class='fa fa-check-circle text-aqua'></i> 메모</label>
					<input type="text" class="form-control" id="modal_memo" placeholder="메모 입력">
				</div>
				
				
				<div class="form-group">
					<label for="modal_sale_yn"><i class='fa fa-circle-o text-aqua'></i> 판매정보 연동</label>
					<label for="modal_sale_yn">
						<input type="checkbox" name="modal_sale_yn" class="minimal" value="" id="modal_sale_yn"/>
						<span class='font12'>체크한 경우 연동</span>
					</label>
				</div>
				
				<div class="form-group slideSearchSale">
					<div class="box box-primary">
						<h5><i class='fa fa-check-circle text-aqua'></i> 연동할 판매정보 선택 ☞ 일일판매조회에서 미수금 수금내역을 확인 가능합니다</h5>

						<label for="modal_sale_dt"><i class='fa fa-check-circle text-aqua'></i> 판매일</label>
						<input type="text" class="form-control" name="modal_sale_dt" id="modal_sale_dt" />

						<button type="button" class="btn btn-danger  pull-right btn_SaleClose">
							<span class="glyphicon glyphicon-remove"></span> 닫기
						</button>
						<div class="box-body">
							<table id="tblSaleList" class="table table-bordered table-hover" summary="거래처 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th class="check">선택</th>
										<th class="check" width="80px">판매일</th>
										<th class="subject">판매어종</th>
										<th class="name">수량</th>
										<th class="name">판매액</th>
										<th class="name">현금</th>
										<th class="name">미입금</th>
										<th class="name">카드</th>
										<th class="name">할인액</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>
@stop
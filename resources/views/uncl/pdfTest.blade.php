<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미수장부</title>
	</head>
	<body>

		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					{{--*/ $sum  = $sumUncl->TOTAL_UNCL /*--}}
					{{--*/ $_SEQ  = -9999 /*--}}
					<span>{{ $list[0]->FRNM }} 미수금</span>
					
					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="100%" >
						<caption>미수장부</caption>
						
						<thead>
							<tr>
								<td  height="30px" width="100px">일&nbsp;&nbsp;&nbsp;자</td>
								<td >품&nbsp;&nbsp;&nbsp;명</td>
								<td >규&nbsp;&nbsp;&nbsp;격</td>
								<td >원 산 지</td>
								<td >수&nbsp;&nbsp;&nbsp;량</td>
								<td >단&nbsp;&nbsp;&nbsp;가</td>
								<td >금&nbsp;&nbsp;&nbsp;액</td>
								<td >소&nbsp;&nbsp;&nbsp;계</td>
								<td >총&nbsp;잔&nbsp;액</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
								@if( $_SEQ == -9999 || $_SEQ != $item->NEW_SEQ)
									<tr>
										<td  >{{ $item->WRITE_DATE}}   </td>
										
										
										<td  >{{ $item->REMARK}}</td>
										<td  ></td>
										<td  ></td>

										<td  ></td>
										<td  ></td>
										<td  ></td>
										<td  >{{ number_format($item->AMT) }}</td>
										<td  >{{ number_format($item->TOT_AMT) }}</td>
									</tr> 
								@else
									<tr>
										<td  ></td>
										
										<td  >{{ $item->PIS_NM }}</td>
										<td  >{{ $item->SIZES }}</td>
										<td  >{{ $item->ORIGIN_NM }}</td>

										<td  >{{ number_format($item->QTY, 1) }}</td>
										<td  >{{ number_format($item->UNCS) }}</td>
										<td  >{{ number_format($item->AMT) }}</td>
										<td  ></td>
										<td  ></td>
									</tr> 
								@endif
								{{--*/ $_SEQ  = $item->NEW_SEQ /*--}}
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
	<footer>
		<div class='page'>
			<span>{{$corp->FRNM}}</span>
			<span style="width:40%">{{$corp->ADDR1}} {{$corp->ADDR2}}</span>
			<span>Tel : {{$corp->PHONE_NO}}</span>
			<span>Fax : {{$corp->FAX}}</span>
		</div>
	</footer>
</html>

	



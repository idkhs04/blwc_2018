<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미수장부</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13.5px; }
			thead{
				width:100%;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
		<style>
			@page {margin-top: 30px; margin-bottom: 70px;}
			footer {bottom: -40px; position: absolute; font-size:14px;}

			.page span { display:inline-block; width:20%;}
		</style>
	</head>
	<body>
	
	<script type="text/php">

	if ( isset($pdf) ) {

		$size = 10;
		$color = array(0,0,0);
		if (class_exists('Font_Metrics')) {
			$font = Font_Metrics::get_font("NanumGothic");
			$text_height = Font_Metrics::get_font_height($font, $size);
			$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
		} elseif (class_exists('Dompdf\\FontMetrics')) {
			$font = $fontMetrics->getFont("NanumGothic");
			$text_height = $fontMetrics->getFontHeight($font, $size);
			$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
		}

		$foot = $pdf->open_object();

		$w = $pdf->get_width();
		$h = $pdf->get_height();

		// Draw a line along the bottom
		$y = $h - $text_height - 24;
		$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

		$pdf->close_object();
		$pdf->add_object($foot, "all");

		$text = "{PAGE_COUNT} - {PAGE_NUM}";  

		// Center the text
		$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

	}
	</script>
		<!-- {{$corp->ADDR1}} {{$corp->ADDR2}} {{$corp->PHONE_NO}} {{$corp->FAX}} -->
		
		<div class="box-body">
			{{--*/ $sum  = $sumUncl->TOTAL_UNCL /*--}}
			{{--*/ $_SEQ  = -9999 /*--}}
			{{--*/ $amt  = 0 /*--}}
			{{--*/ $tot_amt  = 0 /*--}}
			<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>{{ $list[0]->FRNM }} 미수금</span>
			<!--<span class="title" style='text-align:right;font-size:12px;display:block;'> 총미수금 : {{ number_format($sumUncl->TOTAL_UNCL) }} </span>-->
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="text-align:left"> 거래처그룹: {{ $cust->CUST_GRP_NM }} </td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="text-align:left"> 사업자번호: {{ $cust->ETPR_NO }} </td>
					<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
				</tr>
			</table>
			<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="100%" style="border-bottom:3px solid block;margin-top:15px;border-left:none;border-right:none;">
				<caption>미수장부</caption>
				
				<thead>
					<tr>
						<td style="text-align:center;border-left:none;border-right:none;" height="30px" width="100px">일&nbsp;&nbsp;&nbsp;자</td>
						<td style="text-align:center;border-left:none;border-right:none;">품&nbsp;&nbsp;&nbsp;명</td>
						<td style="text-align:center;border-left:none;border-right:none;">규&nbsp;&nbsp;&nbsp;격</td>
						<td style="text-align:center;border-left:none;border-right:none;">원 산 지</td>
						<td style="text-align:center;border-left:none;border-right:none;">수&nbsp;&nbsp;&nbsp;량</td>
						<td style="text-align:center;border-left:none;border-right:none;">단&nbsp;&nbsp;&nbsp;가</td>
						<td style="text-align:center;border-left:none;border-right:none;">금&nbsp;&nbsp;&nbsp;액</td>
						<td style="text-align:center;border-left:none;border-right:none;">소&nbsp;&nbsp;&nbsp;계</td>
						<td style="text-align:center;border-left:none;border-right:none;">총&nbsp;잔&nbsp;액</td>
					</tr>
				</thead>
				<tbody>
					@foreach ($list as $key => $item)
						@if( $_SEQ == -9999 || $_SEQ != $item->NEW_SEQ)
							<tr>
								<td style='text-align:center;border-left:none;border-right:none; padding-left:5px;border-bottom:none;' >{{ $item->WRITE_DATE}}   </td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ $item->REMARK}}</td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' ></td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ number_format($item->AMT) }}</td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ number_format($item->TOT_AMT) }}</td>
							</tr> 
						@else
							@if( $item->PROV_DIV == "0")
							<tr>
								<td style='text-align:left;border-left:none;border-right:none; padding-left:5px;border-bottom:none;border-top:none;' ></td>
								
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ $item->PIS_NM }}</td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ $item->SIZES }}</td>
								<td style='text-align:left;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ $item->ORIGIN_NM }}</td>

								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ number_format($item->QTY, 1) }}</td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ number_format($item->UNCS) }}</td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;' >{{ number_format($item->AMT) }}</td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' ></td>
								<td style='text-align:right;border-left:none;border-right:none; padding-right:5px;border-bottom:none;border-top:none;' ></td>
							</tr> 
							@endif
						@endif
						{{--*/ $_SEQ  = $item->NEW_SEQ /*--}}
					@endforeach
				</tbody>
				
			</table>
				
		<!-- 본문  -->
		</div>
	</body>
	<footer>
		<div class='page'>
			<span>{{$corp->FRNM}}</span>
			<span style="width:40%">{{$corp->ADDR1}} {{$corp->ADDR2}}</span>
			<span>Tel : {{$corp->PHONE_NO}}</span>
			<span>Fax : {{$corp->FAX}}</span>
		</div>
	</footer>
</html>

	



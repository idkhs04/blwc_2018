<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미수금상세보기</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:11px; }
			thead{
				width:100%;
				height:109px;
			}
			table > tr > td { text-align:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
			
		</style>
		<style>
			@page { margin: 50px 20px 20px 20px; }
		</style>
	</head>
	<body>
		
		<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>미 수 금 상 세</span> 

		<div id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="100%" style="border-bottom:3px solid block;margin-top:15px;border-left:none;border-right:none;">
			@foreach ($list as $key => $item)
			<div style="display:block; width:100%; position:relative; height:20px;border-top:1px solid black;padding-top:7px;">
				
				<div style="width:50%;display:inline-block;">{{ $item->FRNM}}  {{ $item->PHONE_NO}}</div>
				<div style='width:50%; text-align:right;display:inline-block;padding-right:10px' >{{ number_format( $item->TOTAL_UNCL_AMT)}}</div>
			</div>
			
			@if( count($detail) > 0  && ($item->UNCL_CUST_MK == $detail[0]->CUST_MK ))
			<table id="tblListDetail" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="90%" style="border-bottom:1px solid block;margin-top:5px;margin-left:40px;border-left:none;border-right:none;margin-bottom:10px;">
				<thead style="border-bottom:3px double block;padding-top:5px;">
					<tr>
						<td style="text-align:center;border-left:none;border-right:none;">어종명</td>
						<td style="text-align:center;border-left:none;border-right:none;">규격</td>
						<td style="text-align:center;border-left:none;border-right:none;">원산지</td>
						<td style="text-align:center;border-left:none;border-right:none;">중량</td>
						<td style="text-align:center;border-left:none;border-right:none;">단가</td>
						<td style="text-align:center;border-left:none;border-right:none;">금액</td>
						<td style="text-align:center;border-left:none;border-right:none;">비고</td>
					</tr>
				</thead>
				<tbody>
				@foreach($detail as $k => $data)
					@if($item->UNCL_CUST_MK == $data->CUST_MK)
						<tr>
						<td style="text-align:left;padding-left:3px;border-left:none;border-right:none;">{{ $data->PIS_NM }}</td>
						<td style="text-align:left;padding-left:3px;border-left:none;border-right:none;">{{ $data->SIZES }}</td>
						<td style="text-align:left;padding-left:3px;border-left:none;border-right:none;">{{ $data->ORIGIN_NM }}</td>
						<td style="text-align:right;padding-right:3px;border-left:none;border-right:none;">{{ number_format($data->QTY) }}</td>
						<td style="text-align:right;padding-right:3px;border-left:none;border-right:none;">{{ number_format($data->UNCS) }}</td>
						<td style="text-align:right;padding-right:3px;border-left:none;border-right:none;">{{ number_format($data->AMT) }}</td>
						<td style="text-align:right;padding-right:3px;border-left:none;border-right:none;">{{ $data->REMARK}}</td>
						</tr>
					@endif
				@endforeach
				</tbody>
			</table>
			@endif
			@endforeach
		</div>
	
	</body>
	<footer>
		<div class="page">
			<div style="text-align:left; display:block;float:right;">{{$today}}</div>
		</div>
	</footer>
</html>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미수장부</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}
			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13.5px; }
			thead{
				width:100%;
				height:109px;
			}
			table > tr > td { text-align:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}
			table, th, td {
				border-bottom: 1px solid black;
			}
			span.title { text-align:center;}
		</style>
	</head>
	<body>
	<script type="text/php">
	if ( isset($pdf) ) {

		$size = 10;
		$color = array(0,0,0);
		if (class_exists('Font_Metrics')) {
			$font = Font_Metrics::get_font("NanumGothic");
			$text_height = Font_Metrics::get_font_height($font, $size);
			$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
		} elseif (class_exists('Dompdf\\FontMetrics')) {
			$font = $fontMetrics->getFont("NanumGothic");
			$text_height = $fontMetrics->getFontHeight($font, $size);
			$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
		}

		$foot = $pdf->open_object();

		$w = $pdf->get_width();
		$h = $pdf->get_height();

		// Draw a line along the bottom
		$y = $h - $text_height - 24;
		$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

		$pdf->close_object();
		$pdf->add_object($foot, "all");

		$text = "{PAGE_COUNT} - {PAGE_NUM}";  

		// Center the text
		$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

	}
	</script>
		<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>미 수 장 부</span> 
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
			</tr>
		</table>
		<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미수금 조회" width="100%" style="border-bottom:1px solid block;margin-top:15px;">
			<thead style="border-bottom:3px double black;padding-top:5px;">	
				<tr>
					<td style='text-align:center;' height="30px" >업&nbsp;체&nbsp;그&nbsp;룹</td>
					<td style='text-align:center;' >상&nbsp;&nbsp;&nbsp;&nbsp;호</td>
					<td style='text-align:center;' width="100px">사업자번호</td>
					<td style='text-align:center;' >총금액</td>
					<td style='text-align:center;' >입금액</td>
					<td style='text-align:center;' width="50px">잔&nbsp;&nbsp;&nbsp;&nbsp;액</td>
					<td style='text-align:center;' >입금액</td>
					<td style='text-align:center;' width="50px">잔&nbsp;&nbsp;&nbsp;&nbsp;액</td>
				</tr>
			</thead>
			<tbody>
				@foreach ($list as $key => $item)
				<tr>
					<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->CUST_GRP_NM}}</td>
					<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->FRNM}}</td>
					<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->ETPR_NO}}</td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format( $item->TOTAL_UNCL_AMT)}}</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				@endforeach
				<tr>
					<td style='text-align:center;' height="30px">총&nbsp;&nbsp;&nbsp;&nbsp;계</td>
					<td></td>
					<td></td>
					<td style='text-align:right;padding-right:5px;'>{{ number_format( $sum)}}</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					
				</tr>
			</tbody>
		</table>
				
		
	</body>
</html>
@extends('layouts.main')
@section('class','비용관리')
@section('title','매출 미수정보 상세조회')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>

	.detail_detailList th{
		width:50px;
	}
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblCustList_filter { display:none;}

	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	#tblList_Sum{
		float:right;
	}

	#SUM_QTY{ padding-left:2px;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center left;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center left;
	}
	td th {
		width: 30px;
		text-align: right;
	}

	.red {color:red;}

	.form-group.dt-body-right{text-align:right;}

	.headerSumQty {
		text-align: right;
	}

	.headerSumQty > ul{ padding-left:0; margin-bottom:0;}
	.headerSumQty label{ margin-right:5px;}
	.headerSumQty{ text-align:right;}
	.headerSumQty li {
		display: inline-block;
		padding: 0 10px;
	}

	.SumQtyHeader {
		padding-top:6px;
		padding-bottom:0px;
	}

	table.detail_detailList thead > tr > th { padding-right:0; padding:2px 3px;}

</style>


<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-5 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success" id="btnDatail2"><i class="fa fa-list"></i> 장부형식</button>
					<button type="button" class="btn btn-success" id="btnAdd" ><i class="fa fa-plus"></i> 추가</button>
					<button type="button" class="btn btn-danger" id="btnDeleteUncl" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-success btnPdfDetail"><i class="fa fa-file-pdf-o"></i> 미수장부출력</button>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div id="reportrange" class="pull-right" style="cursor: pointer; padding: 4px 10px; border: 1px solid #ccc; width: 100%">
					<label>일자</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			<!--
			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">

					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>

				</div>
			</div>
			-->
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header SumQtyHeader">
			<div class="form-group dt-body-right headerSumQty">
				<ul class="">
					<li>
						<label for="txtDicountText"><span class="glyphicon glyphicon-leaf"></span> 거래처명</label>
						{{$CUST->FRNM}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-user"></span> 대표자명</label>
						{{$CUST->RPST}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-grain"></span> 사업자번호</label>
						{{$CUST->ETPR_NO}}</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-inbox"></span> 총 미수액</label>
						<span class="red TotalRecord">총미수액 로딩중..</span></li>
				</ul>
			</div>

		</div>
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="미수금 목록" width="100%">
				<caption>미수금 상세조회</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="50px">일자</th>
						<th width="80px">구분</th>
						<th width="80px">입금</th>
						<th width="80px">미입금</th>
						<th width="80px">판매내역</th>
						<th width="80px">판매금액</th>
						<th width="80px">판매입금</th>
						<th width="80px">할인</th>
						<th width="auto">상세확인</th>
					</tr>
				</thead>
				<tfoot>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {

		var start = moment().subtract(6, 'days');
		var end = moment();

		var today = end.format('YYYY-MM-DD');
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 미수금정보 입력 거래일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 미수금정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		$("#modal_stock_output_date").val(end.format('YYYY-MM-DD'))

		// 해당 거래처 총 미수금
		$.getJSON('/sale/sale/getTotalUncl', {
			_token : '{{ csrf_token() }}',
			UNCL_CUST_MK : "{{Request::segment(4)}}"
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			$("span.TotalRecord").text(Number(data.TOTAL_UNCL).format() + "원");
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/uncl/uncl/detailListData",
				data:function(d){
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.cust_mk		= "{{Request::segment(4)}}";
				}
			},
			columns: [
				{
					data: "CORP_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' ,className: "dt-body-center"},
				{ data: 'REMARK', name: 'REMARK' },
				{
					data: 'ACCRUED_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-center"
				},
				{
					data: 'COLLECTION_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'PIS', name: 'PIS' },
				{
					data: 'TOTAL_SALE_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'CASH_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'SALE_DISCOUNT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					"className":      'details-control dt-body-left',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},

			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총입금
				Amt = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(Amt).format());

				// 총입금
				UnclAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(Number(UnclAmt).format());

				// 판매금액
				saleAmt = api.column( 6 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 6 ).footer() ).html(Number(saleAmt).format());

				// 판매입금
				saleInput = api.column( 7 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 7 ).footer() ).html(Number(saleInput).format());


			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format( row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ location.href="/uncl/uncl/index"; });
		
		$("#btnDatail2").click(function(){
			location.href="/uncl/uncl/detail2/{{Request::segment(4)}}"; 
		})
		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		function format ( d ) {

			var xmlDoc = jQuery.parseXML(d.DD);

			var table = '<table class="detail_detailList table table-bordered table-hover display nowrap" cellpadding="5" cellspacing="0" border="0" width="60%">' +
							"<thead>" +
								"<tr>" +
									"<th width='40px'>출고일</th>" +
									"<th width='60px'>어종</th>" +
									"<th width='30px'>규격</th>" +
									"<th width='30px'>수량</th>" +
									"<th width='60px'>원가</th>" +
									"<th width='60px'>금액</th>" +
									"<th width='30px'>원산지</th>" +
									"<th width='auto'>비고</th>" +
							"</thead><tbody>";


			$(xmlDoc).find("SALE").each(function (i,e){

				table += '<tr>' +
							'<td class="dt-body-center" >'+ $(e).find('WRITE_DATE').text() + "</td>" +
							'<td class="dt-body-center" >'+ $(e).find('PIS_NM').text() + "</td>" +
							'<td class="dt-body-center" >'+ $(e).find('SIZES').text() + "</td>" +
							'<td class="dt-body-right">' + Number($(e).find('QTY').text()).format() + '</td>'+
							'<td class="dt-body-right">'+ Number($(e).find('UNCS').text()).format()  + "</td>" +
							'<td class="dt-body-right">' + Number($(e).find('AMT').text()).format()  + '</td>'+
							'<td class="dt-body-center">' + $(e).find('ORIGIN_NM').text() + '</td>'+
							'<td>' + $(e).find('REMARK').text() + '</td>'+
						'</tr>';

			});
			table += "</tbody></table>";



			// `d` is the original data object for the row
			return table;
		}

		// 미수금 입력 팝업
		$("#btnAdd").click(function(){
			defaltModalInsert();
			$('#modal_uncl_insert').modal({show:true});
		});

		// 비고 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 미수금 저장 버튼
		$("#btnInsertSave").click(function(){

			var DE_CR_DIV	= $("#modal_uncl_insert input[name='DE_CR_DIV']:checked").val();

			$.getJSON('/uncl/uncl/setUnclData', {
				_token			: '{{ csrf_token() }}',
				WRITE_DATE		: $("#modal_write_dt").val() ,
				UNCL_CUST_MK	: "{{Request::segment(4)}}" ,
				PROV_DIV		: DE_CR_DIV ,
				AMT				: removeCommas($("#modal_amt").val()),
				REMARK			: $("#modal_remark_text").val(),
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if (data.result == "success") {
					$('#modal_uncl_insert').modal("hide");
					oTable.draw() ;
				}

			}).fail(function(xhr){
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();

			});
		});

		// 미수금 입력 초기화
		function defaltModalInsert(){

			$("#modal_uncl_insert input[type='text']").val("");
			$("#modal_write_dt").val(today);

			$("#modal_uncl_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_uncl_insert input[name='modal_remark']:first").prop("checked", true);

			$("#modal_uncl_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_uncl_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$("#modal_remark_text").val( $("input[name='modal_remark']:checked").val());

			$("#modal_remark_text").focus();
		}

		// 미수금 정보 삭테
		$("#btnDeleteUncl").click(function(){

			var rowData = oTable.row( $("#tblList tbody tr > td > input[type=checkbox]:checked").parents('tr') ).data();
			var selectedSEQ = rowData.SEQ;

			if( chkInputTble("tblList", "미수금 삭제") ){

				$.getJSON('/uncl/uncl/delete', {
					_token			: '{{ csrf_token() }}',
					WRITE_DATE		: rowData.WRITE_DATE,
					SEQ				: rowData.SEQ ,
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					if (data.result == "success") {
						$('#modal_uncl_insert').modal("hide");
						oTable.draw() ;
					}

				}).fail(function(xhr){
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('.edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();

				});
			}
		});
		/*
		$(".btnPdfDetail").click(function(){

			$("form[name='getPdf2'] > input[name='start_date']").val( $("#reportrange input[name='start_date']").val() );
			$("form[name='getPdf2'] > input[name='end_date']").val( $("#reportrange input[name='end_date']").val() );
			$("form[name='getPdf2']").submit();

		});
		*/

		$(".btnPdfDetail").click(function(){
			
			var width=740;
			var height=720;
			var CUST_MK			= "{{Request::segment(4)}}";
			var start_date		= $("#reportrange input[name='start_date']").val();
			var end_date		= $("#reportrange input[name='end_date']").val();

			var url = "/uncl/uncl/pdfDetailCust/" + CUST_MK + "?start_date=" + start_date + "&end_date=" + end_date;
			getPopUp(url , width, height);

		});
	});

</script>
<!--
<div id="pdf2">
	<form name="getPdf2" method="post" action="/uncl/uncl/pdfDetailCust" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type='hidden' name='start_date' />
		<input type='hidden' name='end_date' />
		<input type='hidden' name='CUST_MK' value="{{Request::segment(4)}}" />
	</form>
</div>
-->
<!-- 재고수정/삭제 -->
<div class="modal fade" id="modal_sujo_stock_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 재고 수정/삭제 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblStockUpdList" class="table table-bordered table-hover" summary="재고 수정/삭제 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th class="subject">수조번호</th>
										<th class="name">어종명</th>
										<th class="name">규격</th>
										<th class="name">원산지</th>
										<th class="name">입고일</th>
										<th class="name">중량</th>
										<th class="name">선택</th>
									</tr>
								</thead>

								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 입고수정-->
<div class="modal fade" id="modal_sujo_input_update" role="dialog" >
	<div class="modal-dialog  modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 입고수정 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<input type="hidden" name="SEQ" id="SELECTED_SEQ" />
				<div class="form-group slideStockUpdList">
					<div class="box box-primary">
						<div class="box-body">
							<table id="tblSujoUpdList" class="table table-bordered table-hover" summary="거래처 목록">
								<caption>어종 조회</caption>
								<thead>
									<tr>
										<th scope="col" class="subject">거래업체명</th>
										<th scope="col" class="name">입고</th>
										<th scope="col" class="name">매입금액</th>
										<th scope="col" class="name">미지급</th>
										<th scope="col" class="name">적요</th>
										<th scope="col" class="name">선택</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 미수금 추가 팝업 -->
<div class="modal fade" id="modal_uncl_insert" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 미수금 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_write_dt"><i class='fa fa-check-circle text-red'></i> 거래일자</label>
					<input type="text" class="form-control" id="modal_write_dt"  />
				</div>
				<div class="form-group">
					<label for="modal_remark_text"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					<input type="text" class="form-control" id="modal_remark_text" value='현금'/>
					<div>
						<label>
							<input type="radio" name="modal_remark" class="minimal" checked value="현금" />현금
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="통장(입금)" />통장(입금)
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="CARD" />CARD
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="판매미수" />판매미수
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="" />직접입력
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_div"><i class='fa fa-circle-o text-aqua'></i> 구분</label>
					<div>
						<label for="DE_CR_DIV_1">
							<input type="radio" name="DE_CR_DIV" class="minimal" value="1" id="DE_CR_DIV_1"/>입금
						</label>

						<label for="DE_CR_DIV_0">
							<input type="radio" name="DE_CR_DIV" class="minimal" checked value="0" id="DE_CR_DIV_0"/>미입금
						</label>

					</div>
				</div>
				<div class="form-group">
					<label for="modal_amt"><i class='fa fa-check-circle text-red'></i> 금액</label>
					<input type="text" class="form-control numeric" id="modal_amt" placeholder="금액 입력">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>
@stop
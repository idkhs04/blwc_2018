@extends('layouts.main')
@section('class','비용관리')
@section('title','미지급금 로그')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblListUnprov_filter, #tblListStockSujo_filter{ display:none;}

	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblListUnprov" class="table table-bordered table-hover display nowrap dataTable no-footer" summary="미지급금 목록 로그" width="100%">
				<caption>미지급 로그</caption>
				<thead>
					<tr>
						<th class="name" width="20px">순번</th>  
						<th class="name">구분</th>  
						<th class="subject">작성일</th>
						<th class="name">SEQ</th>  
						<th class="name">P.V</th>  
						<th class="name">거래처</th>  
						<th class="name">어종</th>  
						<th class="name">수량</th>  
						<th class="name">규격</th>  
						<th class="name">S.SEQ</th>  
						<th class="name">비고</th>  
						<th class="name">입력</th>  
						<th class="name">수정</th>  
						<th class="name">삭제</th>  
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		
		$("#btnList").click(function(){ location.href="/unprov/unprov/index"; });

		// 미지급금 로그
		var uTable = $('#tblListUnprov').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			order	: [[ 0, "desc" ]],
			ajax: {
				url: "/unprov/unprov/listLog",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{ data: 'IDX', name: 'IDX' , className: "dt-body-center" },
				{ data: 'TYPE_NM', name: 'TYPE_NM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center"},
				{ data: 'SEQ', name: 'SEQ' },
				{ data: 'PROV_DIV', name: 'PROV_DIV' },
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'PIS_NM', name: 'PIS_NM' },
				{ 
					data: 'AMT', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'STOCK_SIZES', name: 'STOCK_SIZES' , className: "dt-body-center"},
				{ data: 'STOCK_SEQ', name: 'STOCK_SEQ' , className: "dt-body-center"},
				{ data: 'REMARK', name: 'REMARK' },
				{ data: 'WRITE_DT', name: 'WRITE_DT' , className: "dt-body-center" },
				{ data: 'UPDATE_DT', name: 'UPDATE_DT' , className: "dt-body-center" },
				{ data: 'DELETE_DT', name: 'DELETE_DT' , className: "dt-body-center" },
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 재고로그
		var stTable = $('#tblListStockL').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			ajax: {
				url: "/sale/sale/listLogM",
				data:function(d){
					// d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					// d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			columns: [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{ data: 'LOG_DATE', name: 'LOG_DATE' , className: "dt-body-center" },
				{ data: 'LOG_DIV', name: 'LOG_DIV' , className: "dt-body-center" },
				{ data: 'MEM_ID', name: 'MEM_ID' , className: "dt-body-center" },
				{ data: 'FRNM', name: 'FRNM' , className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center" },
				{ data: 'BEFORE_TOTAL_SALE_AMT', name: 'BEFORE_TOTAL_SALE_AMT' , className: "dt-body-center" },
				{ data: 'TOTAL_SALE_AMT', name: 'TOTAL_SALE_AMT' , className: "dt-body-center"},
			
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

	});

</script>

@stop
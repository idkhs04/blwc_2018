<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미지급금 장부</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13.5px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
		<style>
			@page { margin: 50px 20px; }
			footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px; }
			footer .page a:before { 
				counter-increment: section;
			}
			footer .page a:after {  text-align:right; counter-reset: page 1;content: counter(page) " / " counter(section); }

		</style>
	</head>
	<body>
	<script type="text/php">

	if ( isset($pdf) ) {

		$size = 10;
		$color = array(0,0,0);
		if (class_exists('Font_Metrics')) {
			$font = Font_Metrics::get_font("NanumGothic");
			$text_height = Font_Metrics::get_font_height($font, $size);
			$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
		} elseif (class_exists('Dompdf\\FontMetrics')) {
			$font = $fontMetrics->getFont("NanumGothic");
			$text_height = $fontMetrics->getFontHeight($font, $size);
			$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
		}

		$foot = $pdf->open_object();

		$w = $pdf->get_width();
		$h = $pdf->get_height();

		// Draw a line along the bottom
		$y = $h - $text_height - 24;
		$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

		$pdf->close_object();
		$pdf->add_object($foot, "all");

		$text = "{PAGE_COUNT} - {PAGE_NUM}";  

		// Center the text
		$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

	}
	</script>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>미 지 급 장 부</span> 
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
						</tr>
					</table>
					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미지급금 조회" width="100%" style="border-bottom:1px solid block;margin-top:15px;">
						<caption>미지급금</caption>
						<thead style="border-bottom:3px double block;padding-top:5px;">
							<tr>
								<td style='text-align:center;' height="30px" width="70px">업&nbsp;체&nbsp;그&nbsp;룹</td>
								<td style='text-align:center;' height="30px" >상&nbsp;&nbsp;&nbsp;&nbsp;호</td>
								<td style='text-align:center;' height="30px" width="auto">사&nbsp;업&nbsp;자&nbsp;번&nbsp;호</td>
								<td style='text-align:center;'>총미지급액</td>
								<td style='text-align:center;'>출금액</th>
								<td style='text-align:center;'>잔&nbsp;&nbsp;&nbsp;&nbsp;액</th>
								<td style='text-align:center;'>출금액</th>
								<td style='text-align:center;'>잔&nbsp;&nbsp;&nbsp;&nbsp;액</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
							<tr>
								<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->CUST_GRP_NM}}</td>
								<td style='text-align:left;padding-left:5px;' height="30px"  >{{ $item->FRNM}}</td>
								<td style='text-align:left;padding-left:5px;' height="30px">{{ $item->ETPR_NO}}</td>
								<td style='text-align:right;padding-right:5px;'>{{ number_format( $item->TOTAL_PROV)}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							@endforeach
							<tr>
								<td style='text-align:center;' height="30px"  >총&nbsp;&nbsp;&nbsp;&nbsp;계</td>
								<td></td>
								<td></td>
								
								<td style='text-align:right;padding-right:5px;'>{{ number_format( $sum)}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
	
</html>

	



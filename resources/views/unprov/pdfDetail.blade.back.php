<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>미지급내역</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:11px; }
			thead{
				width:100%;
				/*position:fixed;*/
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
		</style>
		<style>
			@page { margin: 50px 20px; }
			footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px; }
			footer .page a:after {  text-align:right;content: counter(pages) " / " counter(page); }

		</style>
	</head>
	<body>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>미&nbsp;&nbsp;지&nbsp;&nbsp;급&nbsp;&nbsp;내&nbsp;&nbsp;역</span>

					<table id="tblHeader" border="0" cellpadding="0" cellspacing="0" summary="미지급내역 합계" width="100%" style="border:none;margin-bottom:3px;">
						<tr>
							<td style="text-align:left;">거래기간 : {{ $start_date }} ~ {{ $end_date}}</td>
							<td style="text-align:right;">단위(원, Kg)</td>
						</tr>
						<tr>
							<td style="text-align:left;">거래업체 : {{ $list[0]->FRNM }} &nbsp;&nbsp;사업자 등록번호 : {{ $list[0]->ETPR_NO }} &nbsp;&nbsp;대표 : {{ $list[0]->RPST}}</td>
							<td style="text-align:right;">미지급총액 : {{ number_format($sum) }} </td>
						</tr>
					</table>

					<table id="tblList" border="1" cellpadding="0" cellspacing="0" summary="미지급내역 조회" width="100%" style="border-bottom:3px solid block;margin-top:5px;border-left:none;border-right:none;">
						<caption>미수장부</caption>
						
						<thead>
							<tr>
								<td style="text-align:center;border-right:none;border-top:1px solid black;border-bottom:1px solid black;border-left:1px solid black" height="30px">일자</td>
								<td style="text-align:center;border-left:none;border-right:none;border-top:1px solid black;border-bottom:1px solid black" height="30px">어종</td>
								<td style="text-align:center;border-left:none;border-right:none;border-top:1px solid black;border-bottom:1px solid black" >규격</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >원산지</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >매입종량</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >매입단가</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >매입금액</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >지급</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >미지급</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >할인금액</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >누계금</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >비고</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($list as $key => $item)
							<tr>
								<td style="text-align:center;border-right:none;border-top:1px solid black;border-bottom:1px solid black;border-left:1px solid black" height="30px">{{$item->WRITE_DATE}}</td>
								<td style="text-align:center;border-left:none;border-right:none;border-top:1px solid black;border-bottom:1px solid black" height="30px">{{$item->PIS_NM}}</td>
								<td style="text-align:center;border-left:none;border-right:none;border-top:1px solid black;border-bottom:1px solid black" >{{$item->SIZES}}</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{$item->ORIGIN_NM}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{ number_format($item->QTY)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{ number_format($item->UNCS)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{ number_format($item->INPUT_AMT)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{ number_format($item->AMT_CHA)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($item->AMT_DAE)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($item->INPUT_DISCOUNT)}}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($item->PRE_UNPROV_SUM)}}</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{$item->REMARK}}</td>
							</tr> 
							
							@endforeach
							<tr>
								<td style="text-align:center;border-bottom:1px solid black;border-left:1px solid black" height="30px" colspan="4">합계</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($qtySum) }}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($uncsSum) }}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($inputAmtSum) }}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($chaSum) }}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($daeSum) }}</td>
								<td style="padding-right:5px;text-align:right;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" >{{number_format($disCountSum) }}</td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" ></td>
								<td style="text-align:center;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid black" ></td>

							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
	<footer>
		
		<div class="page">
			<div style="text-align:left; display:block;float:right;">{{$today}}</div>
			<a href="#" style="display:inline-block;height:10px;text-align:right;padding-top:10px;">페이지 </a>
			
		</div>
	</footer>
</html>

	



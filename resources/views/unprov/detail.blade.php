@extends('layouts.main')
@section('class','비용관리')
@section('title','매입 미지급 정보 상세조회')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter, #tblSujoUpdList_filter, #tblCustList_filter { display:none;}


	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}

	#tblList_Sum{
		float:right;
	}

	#SUM_QTY{ padding-left:2px;}
	td.details-control {
		background: url('/image/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.shown td.details-control {
		background: url('/image/details_close.png') no-repeat center center;
	}
	td th {
		width: 30px;
		text-align: right;
	}

	th.dt-body-right, .form-group.dt-body-right{ text-align:right;}

	.blue {color:blue}

	.headerSumQty > ul{ padding-left:0; margin-bottom:0;}
	.headerSumQty label{ margin-right:5px;}
	.headerSumQty{ text-align:right;}
	.headerSumQty li {
		display: inline-block;
		padding: 0 10px;
	}

	.SumQtyHeader {
		padding-top:6px;
		padding-bottom:0px;
	}

	.detail_detailList th{ height:25px;}

</style>


<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-5 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-success" id="btnAdd" ><i class="fa fa-plus"></i> 추가</button>
					<button type="button" class="btn btn-danger" id="btnDeleteUnprov" ><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-success btnPdfDetail"><i class="fa fa-file-pdf-o"></i> 출력</button>
					<!-- <button type="button" class="btn btn-info btnPdfDetailPreview"><i class="fa fa-file-image-o"></i> </button> -->
					<button type="button" class="btn btn-success" id="btnLog" ><i class="fa fa-code"></i> 이력</button>

				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div id="reportrange" class="pull-right" style="cursor: pointer; padding: 3px 10px; border: 1px solid #ccc; width: 100%">
					<label>일자</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">

					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="form-group dt-body-right headerSumQty">
				<ul class="">
					<li>
						<label for="txtDicountText"><span class="glyphicon glyphicon-leaf"></span> 거래처명 : </label>
						{{$CUST->FRNM}}
					</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-user"></span> 대표자명 : </label>
					{{$CUST->RPST}}
					</li>
					<li>
						<label for="txtSumSaleAll"><span class="glyphicon glyphicon-grain"></span> 사업자번호 : </label>
					{{$CUST->ETPR_NO}}
					</li>
					<li>
						<label for="txtSumUnprovAll"><span class="glyphicon glyphicon-inbox"></span> 총 미지급액 : </label>
						<span class="blue TotalRecord">총 미지급액 로딩중..</span>
					</li>
				</ul>
			</div>
		</div>
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="미지급 상세 목록" width="100%">
				<caption>미지급 상세조회</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="60px">일자</th>
						<th width="80px">어종</th>
						<th width="50px">규격</th>
						<th width="60px">원산지</th>
						<th width="60px">매입수량</th>
						<th width="60px">매입단가</th>
						<th width="60px">매입금액</th>
						<th width="50px">할인</th>
						<th width="60px">비고</th>
						<th width="60px">지급</th>
						<th width="60px">미지급</th>
						<th width="30px"></th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<th>합계</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th class='dt-body-right'></th>
					<th></th>
					<th class='dt-body-right'></th>
					<th></th>
					<th></th>
					<th></th>
					<th class='dt-body-right'></th>
					<th></th>
					<th></th>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		var index = 0;
		var start = moment().subtract(6, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');
		// 초기값 세팅

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 미지급금정보 입력 거래일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		});

		// 미지급금정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		$("#modal_stock_output_date").val(end.format('YYYY-MM-DD'))

		// 해당 거래처 총 미지급금
		$.getJSON('/unprov/unprov/getTotalUnprov', {
			_token : '{{ csrf_token() }}',
			PROV_CUST_MK : "{{Request::segment(4)}}"
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			$("span.TotalRecord").text(Number(data.TOTAL_PROV).format() + "원");
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/unprov/unprov/detailListData",
				data:function(d){
					d.textSearch	= $("input[name='textSearch']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					d.cust_mk		= "{{Request::segment(4)}}";

				}
			},
			order: [[ 1, 'desc' ]],
			columns: [
				{
					data: "CORP_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE', className: "dt-body-center" },
				{ data: 'PIS_NM', name: 'PIS_NM' },

				{
					data: 'SIZES',
					render: function ( data, type, row ) {
						if ( type === 'display') {
							return data;
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'ORIGIN_NM', name: 'ORIGIN_NM' ,className: "dt-body-center"},
				{
					data: 'QTY',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'DAN',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'INPUT_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'INPUT_DISCOUNT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'REMARK',	 name: 'REMARK' },
				{
					data: 'AMT_CHA',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					data: 'AMT_DAE',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				{
					data: 'STOCK_PIS_MK',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( row.STOCK_PIS_MK == "" || row.STOCK_PIS_MK === null){
								return "<button class='btn btn-success btnUpdate' ><span class='glyphicon glyphicon-on'></span>수정</button>";
							}
						}
						return '';
					},
					className: "dt-body-center"
				},


			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
				var qtys = 0;
				var j = 0;
				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;
				};

				// 총매입수량
				Amt = api.column( 5 ).data().reduce( function (a, b) {
					size = api.column( 3 ).data()[j++];

					if( size == "마리" && isConfigQty() ) {
						qtys += intVal(b);
						return intVal(a);
					}else{
						return intVal(a) + intVal(b);
					}

				}, 0 );

				if( isConfigQty()){
					$( api.column( 5 ).footer() ).html("Kg : " + Number(Amt).format() + " / 마리:" + Number(qtys).format() );
				}else{
					$( api.column( 5 ).footer() ).html("Kg : " + Number(Amt).format() );
				}

				// 총매입금액
				UnclAmt = api.column( 7 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 7 ).footer() ).html(Number(UnclAmt).format() + " 원");

				// 총 미지급금
				saleAmt = api.column( 11 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 11 ).footer() ).html(Number(saleAmt).format() + " 원");


			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$('#tblList tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = oTable.row( tr );

			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				// Open this row
				row.child( format( row.data()) ).show();
				tr.addClass('shown');
			}
		} );

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		$("#btnList").click(function(){ location.href="/unprov/unprov/index"; });


		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		function format ( d ) {

			var xmlDoc = jQuery.parseXML(d.DD);

			var table = '<table class="detail_detailList table table-bordered table-hover display nowrap" cellpadding="5" cellspacing="0" border="0"  width="100%">' +
							"<thead>" +
								"<tr>" +
									"<th width='60px'>입고일</th>" +
									"<th width='80px'>어종</th>" +
									"<th width='50px'>규격</th>" +
									"<th width='60px'>수량</th>" +
									"<th width='60px'>원가</th>" +
									"<th width='60px'>금액</th>" +
									"<th width='60px'>할인</th>" +
									"<th width='60px'>미지급</th>" +
									"<th width='60px'>원산지</th>" +
									"<th width='auto'>비고</th>" +
							"</thead><tbody>";


			$(xmlDoc).find("STOCK").each(function (i,e){

				table += '<tr>' +
							'<td class="dt-body-center" >'+ $(e).find('INPUT_DATE').text() + "</td>" +
							'<td class="dt-body-center" >'+ $(e).find('PIS_NM').text() + "</td>" +
							'<td class="dt-body-center" >'+ $(e).find('SIZES').text() + "</td>" +
							'<td class="dt-body-right">' + Number($(e).find('QTY').text()).format() + '</td>'+
							'<td class="dt-body-right">'+ Number($(e).find('UNCS').text()).format()  + "</td>" +
							'<td class="dt-body-right">' + Number($(e).find('AMT').text()).format()  + '</td>'+
							'<td class="dt-body-right">'+ Number($(e).find('INPUT_DISCOUNT').text()).format()  + "</td>" +
							'<td class="dt-body-right">' + Number($(e).find('INPUT_UNPROV').text()).format()  + '</td>'+
							'<td class="dt-body-center">' + $(e).find('ORIGIN_NM').text() + '</td>'+
							'<td>' + $(e).find('OUTLINE').text() + '</td>'+
						'</tr>';

			});
			table += "</tbody></table>";

			// `d` is the original data object for the row
			return table;
		}


		// 미지급금 입력 팝업
		$("#btnAdd").click(function(){
			defaltModalInsert();
			$("#mode").val("ins");
			$('#modal_unprov_insert').modal({show:true});
		});

		// 비고 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 미지급금 저장 버튼
		$("#btnInsertSave").click(function(){

			var DE_CR_DIV	= $("#modal_unprov_insert input[name='DE_CR_DIV']:checked").val();
			var mode = $("#mode").val();
			var url = "/unprov/unprov/setUnprovData";

			var JsonData =  {
				_token			: '{{ csrf_token() }}',
				WRITE_DATE		: $("#modal_write_dt").val() ,
				PROV_CUST_MK	: "{{Request::segment(4)}}" ,
				PROV_DIV		: DE_CR_DIV ,
				AMT				: removeCommas($("#modal_amt").val()),
				REMARK			: $("#modal_remark_text").val(),
			};

			if( mode == "upd"){
				url = "/unprov/unprov/updUnprovData";
				JsonData.SEQ = $("#SEQ").val();
			}
			$.getJSON( url, JsonData

			).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if (data.result == "success") {
					$('#modal_unprov_insert').modal("hide");
					oTable.draw() ;
					getTotal();
				}

			}).fail(function(xhr){
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();

			});
		});

		// 미지급금 입력 초기화
		function defaltModalInsert(){

			$("#modal_unprov_insert input[type='text']").val("");
			$("#modal_write_dt").val(today);

			$("#modal_unprov_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_unprov_insert input[name='modal_remark']:first").prop("checked", true);

			$("#modal_unprov_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_unprov_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$(".slideSearchCust" ).slideUp();
			$("#modal_remark_text").val( $("input[name='modal_remark']:checked").val());

			$("#modal_remark_text").focus();
		}

		// 미지급금 정보 삭제
		$("#btnDeleteUnprov").click(function(){

			var rowData = oTable.row( $("#tblList tbody tr > td > input[type=checkbox]:checked").parents('tr') ).data();
			var selectedSEQ = rowData.SEQ;


			console.log(rowData);

			if( rowData.STOCK_PIS_MK !== null ){
				$(".content").prepend( getAlert('danger', '경고', "입고와 연동된 자료이므로 삭제 불가능합니다.") ).show();
				return false;
			}
			if( chkInputTble("tblList", "미지급금 삭제") ){

				if( confirm('삭제하시겠습니까?')) {
					$.getJSON('/unprov/unprov/delete', {
						_token			: '{{ csrf_token() }}',
						WRITE_DATE		: rowData.WRITE_DATE,
						SEQ				: rowData.SEQ ,
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						if (data.result == "success") {
							$('#modal_unprov_insert').modal("hide");
							oTable.draw() ;
						}
						getTotal();

					}).fail(function(xhr){
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						info.slideDown();

					});
				}
			}
		});

		// 수정
		$("body").on("click", ".btnUpdate", function(){

			var arr_code = [];
			var row = null;

			var row = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(row.SUJO_NO);

			$.getJSON('/unprov/unprov/getDataDetail', {
				WRITE_DATE	: row.WRITE_DATE,
				SEQ			: row.SEQ,
				_token : '{{ csrf_token() }}'
			},function(data){

				$("#mode").val("upd");
				$('#modal_unprov_insert').modal({show:true});
				var item = data[0];

				$("#modal_write_dt").prop("readonly", true);
				$("#SEQ").val(item.SEQ);
				$("#modal_remark_text").prop("readonly", false);

				if( item.PROV_DIV == "0"){
					$("#DE_CR_DIV_0").prop("checked", true);
					$("#DE_CR_DIV_1").prop("checked", false);
				}else{
					$("#DE_CR_DIV_0").prop("checked", false);
					$("#DE_CR_DIV_1").prop("checked", true);
				}

				$("#modal_remark_text").val(item.REMARK);

				if(item.REMARK.trim() == "현금"){
					$("input[name='modal_remark']:eq(0)").prop("checked", true);
				}else if(item.REMARK.trim() == "통장(입금)"){
					$("input[name='modal_remark']:eq(1)").prop("checked", true);
				}else if(item.REMARK.trim() == "CARD"){
					$("input[name='modal_remark']:eq(2)").prop("checked", true);
				}else if(item.REMARK.trim() == "판매미지급"){
					$("input[name='modal_remark']:eq(4)").prop("checked", true);
				}else{
					$("input[name='modal_remark']:eq(3)").prop("checked", true);
				}

				$("#modal_write_dt").val(item.WRITE_DATE);
				$("#modal_remark_text").val(item.REMARK);
				$("#modal_amt").val(Number(item.AMT).format());
				getTotal();

			}),function(data){
				console.log(dat);
			};

		});

		$("#btnLog").click(function(){
			location.href = "/unprov/unprov/log";
		});

		/*
		$(".btnPdfDetail, .btnPdfDetailPreview").click(function(){

			if( $(this).hasClass('btnPdfDetailPreview')){
				$("form[name='getPdf'] > input[name='download']").val(1);
			}else{
				$("form[name='getPdf'] > input[name='download']").val(0);
			}

			$("form[name='getPdf'] > input[name='start_date']").val( $("#reportrange input[name='start_date']").val() );
			$("form[name='getPdf'] > input[name='end_date']").val( $("#reportrange input[name='end_date']").val() );
			$("form[name='getPdf']").submit();
		});
		*/

		$(".btnPdfDetail").click(function(){

			var width=740;
			var height=720;

			var PROV_CUST_MK = "{{Request::segment(4)}}";
			var start_date	= $("#reportrange input[name='start_date']").val();
			var end_date	= $("#reportrange input[name='end_date']").val();

			var url = "/unprov/unprov/PdfDetail/" + start_date + "/" + end_date + "/"+ PROV_CUST_MK;
			getPopUp(url , width, height);
		});

		function getTotal(){
			
			// 해당 거래처 총 미지급금
			$.getJSON('/unprov/unprov/getTotalUnprov', {
				_token : '{{ csrf_token() }}',
				PROV_CUST_MK : "{{Request::segment(4)}}"
			},function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("span.TotalRecord").text(Number(data.TOTAL_PROV).format() + "원");
			});
		
		}
	});

	//<a href="pdfcreate.php" target="_blank"> or <form method="post" action="pdfcreate.php" target="_blank">

</script>
<!--
<div id="pdf">
	<form name="getPdf" method="post" action="/unprov/unprov/PdfDetail" target="_blank">
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="PROV_CUST_MK" value="{{Request::segment(4)}}" />
		<input type="hidden" name="start_date" />
		<input type="hidden" name="end_date" />
		<input type="hidden" name="download" value='1'/>
	</form>
</div>
 -->
<!--
 -->
<!-- 미지급금 추가 팝업 -->
<div class="modal fade" id="modal_unprov_insert" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 미지급금 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<input type="hidden" id="mode" />
				<input type="hidden" id="SEQ" name="SEQ"/>
				<div class="form-group">
					<label for="modal_write_dt"><i class='fa fa-circle-o text-red'></i> 거래일자</label>
					<input type="text" class="form-control" id="modal_write_dt"  />
				</div>
				<div class="form-group">
					<label for="modal_remark_text"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					<input type="text" class="form-control" id="modal_remark_text" />
					<div>
						<label>
							<input type="radio" name="modal_remark" class="minimal" checked value="현금" />현금
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="통장(입금)" />통장(입금)
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="CARD" />CARD
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="판매미지급" />판매미지급
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="" />직접입력
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_div"><i class='fa fa-circle-o text-aqua'></i> 구분</label>
					<div>
						<label for="DE_CR_DIV_0">
							<input type="radio" name="DE_CR_DIV" class="minimal" checked value="0" id="DE_CR_DIV_0"/>지급
						</label>
						<label for="DE_CR_DIV_1">
							<input type="radio" name="DE_CR_DIV" class="minimal" value="1" id="DE_CR_DIV_1"/>미지급
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_amt"><i class='fa fa-circle-o text-aqua'></i> 금액</label>
					<input type="text" class="form-control numeric" id="modal_amt" placeholder="금액 입력">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.main')
@section('class','비용관리')
@section('title','매입 미지급 정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
	.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }

	#modal_unprov_insert label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_unprov_insert .option{
		margin-left:100px;
	}
	#modal_unprov_insert .option label{
		display:inline-block;
		width:85px;
		clear:both;
	}

	#modal_unprov_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{
		display:none;
	}
	.btn-info{
		vertical-align:baseline;
		margin-top:0;
	}
	.dt-body-right{text-align:right;}
	.waring-message { padding-top:4px;}
	.control-label.waring-message {font-size:12px;}

	.col-md-215 {width:215px;}

	.reportrange {display:none;}
	
	.input-group-sm>.form-control, .input-group-sm>.input-group-addon, .input-group-sm>.input-group-btn>.btn{
		height:22px;
		padding:0 10px;
	}

</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-1 col-xs-6">
				<div class="btn-group" >
					<button type="button" class="btn btn-success btnPdf" id="btnPdf"><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>

			<div class="col-md-3 col-xs-6">
				<div class="btn-group" style="">
					<input type="radio" name="chkZero" value="1"  />전체
					<input type="radio" name="chkZero" value="0" checked="checked" />미지급업체만
					<input type="radio" name="chkZero" value="2"  />날짜별
				</div>
			</div>
			
			<div class="col-md-4 col-xs-12 reportrange">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 2.5px 10px; border: 1px solid #ccc; width: 100%">
					<label>입고일</label> 
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-2 col-xs-12">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control cis-lang-ko"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition" id="srtCondition">
				</select>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="미지급금 정보조회" width="100%">
				<caption>미지급금 정보관리 조회</caption>
				<thead>
					<tr>
						<th class="check" width="30px"></th>
						<th class="name" width="80px">업체부호</th>
						<th class="name" width="100px">사업자번호</th>
						<th class="name" width="80px">업체명</th>
						<th class="name" width="80px">총미지급금</th>
						<th class="name" width="120px">관리</th>
						<th class="name" width="*">메모</th>
						<th class="name" width="10px"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th class="pull-right">전체 총미지급금액 : </th>
						<th class="dt-body-right"></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<!-- 본문  -->
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		
		var start = moment();
		var end = moment();
		//start = end;
		
		var isBack		= localStorage["IS_BACK"];
		var sText		= '';
		var sCondition	= 'ALL';
		
		// 초기값 세팅
		function cbSetDate(start, end) {
			
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		
		}
			
		// 미지급정보(입고) 일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		
		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",			
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		if( isBack == "Y"){
			$("input[name='chkZero'][value='" + localStorage["CHECK_ZERO"] +"']").prop("checked", true);
			//$("input[name='textSearch']").val(localStorage["SEARCH_TEXT"]);
			$("select[name='srtCondition'] > option[value='"+ localStorage["SEARCH_CONDITION"] + "']").prop("selected", true);

			$("input[name='start_date']").val(localStorage["START_DATE"]);
			$("input[name='end_date']").val(localStorage["END_DATE"]);
			
			$('#reportrange span').html(localStorage["START_DATE"] + ' - ' + localStorage["END_DATE"]);
			
		}

		if( $("input[name='chkZero']:checked").val() == "2"){
			$("div.reportrange").css("display", "block");
		}

		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},
		 
			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},
		 
			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		$.getJSON('/sale/sale/getCustGrp', {
				_token	: '{{ csrf_token() }}'
			}, function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				var $srtCondition = $("select[name='srtCondition']");
				$srtCondition.empty();
				$srtCondition.append("<option value='ALL' >전체</option>");

				$.each(data, function(){
					if( isBack == "Y"){
						if( this.CUST_GRP_CD == localStorage["SEARCH_CONDITION"] ) {
							$srtCondition.append('<option selected="selected" value="' + this.CUST_GRP_CD +'">' + this.CUST_GRP_NM + '</option>');
						}
						$srtCondition.append('<option value="' + this.CUST_GRP_CD +'">' + this.CUST_GRP_NM + '</option>');
					}else{
						$srtCondition.append('<option value="' + this.CUST_GRP_CD +'">' + this.CUST_GRP_NM + '</option>');
					}
				});
			}
		);

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 150,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/unprov/unprov/listData",
				data:function(d){
					d.srtCondition	= isBack == "Y" ? localStorage["SEARCH_CONDITION"] : $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.chkZero		= $("input[name='chkZero']:checked").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			aoColumnDefs: [
				{ "sType": "numeric-comma", "aTargets": [3] }
			],
			order: [[ 3, 'asc' ]],
			columns: [
				{
					data:   "PROV_CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'CUST_GRP_NM', name : 'CUST_GRP_NM'},
				{ 
					data: 'ETPR_NO', 
					render: function ( data, type, row ) {
						if (data !== null && data.length == 10)
						{
							firstNo		= data.substr(0, 3);
							secondNo	= data.substr(3, 2);
							thirdNo		= data.substr(5, 5);
							
							return firstNo + "-" + secondNo + "-" + thirdNo
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'FRNM',		 name: 'FRNM' },
				{ 
					data: 'TOTAL_PROV', 
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				
				{ 
					data: null,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return " <button class='btn btn-info btnAdd' ><i class='fa fa-plus-circle'></i> 추가 </button>" + 
									" <button class='btn btn-success btnGoDetail' ><i class='fa fa-arrow-circle-right'></i> 선택 </button>";
						}
						return data;
					},
					orderable:  false,
				},
				{ 
					data: 'ADDR_3',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							var memo = "";
							if( data != "null" && data !== null && data !== undefined ){
								memo = data;
							}
							return "<div class='input-group input-group-sm'> <input type='text' class='form-control' value='" +memo+ "'>" + 
									" <span class='input-group-btn'><button type='button' class='btn btn-info btn-flat btnMemoSave'><i class='fa fa-fw fa-save'></i>저장</button></span></div>";
							
						}
						return data;
					},
					orderable:  false,
				},
				{ 
					data: null,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "";
						}
						return data;
					},
					orderable:  false,
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;
	 
				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총미입금
				UnclAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				
				$( api.column( 4 ).footer() ).html(Number(UnclAmt).format() );
				

			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		localStorage["IS_BACK"]		= "N";

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
			//localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
			//localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
			//localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();
		});

		$("input[name='chkZero']").change(function(){
			oTable.draw() ;
			//localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();

			// 날짜검색일때
			if( $("input[name='chkZero']:checked").val() == "2"){
				$("div.reportrange").css("display", "block");
			}else{
				$("div.reportrange").css("display", "none");
			}
		})

		$("input[name='start_date']").on("change", function(){
			oTable.draw();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();
		});

		$("input[name='end_date']").on("change", function(){
			oTable.draw();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
			localStorage["CHECK_ZERO"]		= $("input[name='chkZero']:checked").val();
			localStorage["START_DATE"]		= $("input[name='start_date']").val();
			localStorage["END_DATE"]		= $("input[name='end_date']").val();
		});

		$("#btnLog").click(function(){
			location.href="/unprov/unprov/log";
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		// 상세페이지 이동
		$('#tblList tbody').on( 'click', 'button.btnGoDetail', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/unprov/unprov/detail2/" + data.PROV_CUST_MK;
		});

		// 추가페이지 이동 후 바로 입력되도록
		$('#tblList tbody').on( 'click', 'button.btnAdd', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/unprov/unprov/detail2/" + data.PROV_CUST_MK.trim() + "/INSERT";
		});

		// 메모저장
		$('#tblList tbody').on( 'click', 'button.btnMemoSave', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			$objMemoInput	 = $(this).parents('td').find("input:text");
			var memo =  $objMemoInput.val();
			
			$.getJSON('/custcd/custcd/updateMemo', {
				_token					: '{{ csrf_token() }}'
				, CUST_MK				: data.PROV_CUST_MK
				, CUST_UNPROV_MEMO		: memo
			}).success(function(xhr){
				//수정후 목록이 재갱신됨을 차단 요청 : 2017-04-24(한려수산)
				$objMemoInput.css("border-color", "#00a65a");
			}).error(function(xhr,status, response) {
				$objMemoInput.css("border-color", "#dd4b39");
				alert("저장 오류.. 100자 이내 입력 바랍니다");
			});
		});

		$(".btnAdd").click(function(){
			defaltModalInsert();
			$('#modal_unprov_insert').modal({show:true}); 
		});

		// 거래처 목록 
		$("#btnSearchCust").click(function(){ 
			$( ".slideSearchCust" ).show(function(){
				var cTable = $('#tblCustList').DataTable({
					processing: true,
					serverSide: true,
					retrieve: true,
					search:false,
					bInfo:false,
					iDisplayLength: 10,// 기본 10개 조회 설정
					bLengthChange: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("input[name='strSearchCust']").val();
						}
					},
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' },
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' },
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 추가</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();
					$("#modal_cust_mk").val(data.CUST_MK);
					$("#modal_cust_nm").val(data.FRNM);
					$("input[name='strSearchCust']").val('');
					$(".slideSearchCust" ).slideUp();	
				});

				$("input[name='strSearchCust']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			
				// 거래처 검색 닫기
				$(".btn_CustClose").click(function(){ $("div.slideSearchCust").hide(); });
			});
		});

		// 차대구분 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 미지급금 저장 버튼
		$("#btnInsertSave").click(function(){
			
			var DE_CR_DIV	= $("#modal_unprov_insert input[name='DE_CR_DIV']:checked").val();

			$.getJSON('/unprov/unprov/setUnprovData', {
				_token			: '{{ csrf_token() }}',
				WRITE_DATE		: $("#modal_write_dt").val() ,
				PROV_CUST_MK	: $("#modal_cust_mk").val() ,
				PROV_DIV		: DE_CR_DIV ,
				AMT				: removeCommas($("#modal_amt").val()), 
				REMARK			: $("#modal_remark_text").val(),
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if (data.result == "success") {
					$('#modal_unprov_insert').modal("hide"); 
					oTable.draw();
				}

			}).fail(function(xhr){
				var error = jQuery.parseJSON(xhr.responseText); 
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();
				
			});
		});
		
		// 미지급금 입력 초기화
		function defaltModalInsert(){
			
			$("#modal_unprov_insert input[type='text']").val("");
			$("#modal_write_dt").val(today);

			$("#modal_unprov_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_unprov_insert input[name='modal_remark']:first").prop("checked", true);
			
			$("#modal_unprov_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_unprov_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$('.edit_alert').hide().find('ul').empty();
			$(".slideSearchCust" ).slideUp();	

			$("#modal_remark_text").val( $("input[name='modal_remark']:checked").val());


			$("#modal_remark_text").focus();
		}

		// 미지급용출력
		$(".btnPdf").click(function(){
			var CUST_GRP_CD	= $("select[name='srtCondition'] option:selected").val()
			var testSearch	= $("input[name='textSearch']").val();

			var popUrl = "/unprov/unprov/pdfEmpty/" + CUST_GRP_CD + "/" + testSearch;	//팝업창에 출력될 페이지 URL
			var popOption = "width=740, height=720, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
			window.open(popUrl,"",popOption);

		});
		
		// 미지급 상세출력
		$(".btnPdfDetail").click(function(){
			
			var CUST_GRP_CD	= $("select[name='srtCondition'] option:selected").val()
			var testSearch	= $("input[name='textSearch']").val();
			var popUrl = "/uncl/uncl/pdfListDetail/" + CUST_GRP_CD + "/" + testSearch;	//팝업창에 출력될 페이지 URL
			var popOption = "width=740, height=720, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
			window.open(popUrl,"",popOption);
		});
	});

</script>
<!-- 미지급금 추가 팝업 -->
<div class="modal fade" id="modal_unprov_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 미지급금 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="modal_write_dt"><i class='fa fa-check-circle text-red'></i> 작성일자</label>
					<input type="text" class="form-control" id="modal_write_dt"  />
				</div>
				<div class="form-group">
					<label for="modal_cust_mk"><i class='fa fa-check-circle text-red'></i> 거래처</label>
					<input type="text" class="form-control" id="modal_cust_mk" placeholder="거래처코드" readonly="readonly">
					<input type="text" class="form-control" id="modal_cust_nm" placeholder="거래처명" readonly="readonly">
					<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5><span class="glyphicon glyphicon-lock"></span> 거래처 선택</h5>

							<label for="strSearchCust"><span class="glyphicon glyphicon-user"></span> 거래처 상호</label>
							<input type="text" class="form-control cis-lang-ko" name="strSearchCust" id="strSearchCust" />

							<button type="button" class="btn btn-danger pull-right btn_CustClose">
								<span class="glyphicon glyphicon-remove"></span> 닫기 
							</button>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>거래처 조회</caption>
									<thead>
										<tr>
											<th scope="col" class="check">코드</th>
											<th scope="col" class="subject">상호</th>
											<th scope="col" class="name">대표자</th>
											<th scope="col" class="name">전화번호</th>
											<th scope="col" class="name">선택</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_remark_text"><i class='fa fa-circle-o text-aqua'></i> 비고</label>
					<input type="text" class="form-control cis-lang-ko" id="modal_remark_text" readonly="readonly" />
					<div class='option'>
						<label>
							<input type="radio" name="modal_remark" class="minimal" checked value="현금" />현금
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="통장(입금)" />통장(입금)
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="CARD" />CARD
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="" />직접입력
						</label>
						<label>
							<input type="radio" name="modal_remark" class="minimal" value="판매미지급" />판매미지급
						</label>
						
					</div>
				</div>
				<div class="form-group">
					<label for="modal_div" style='float:left;'><i class='fa fa-check-circle text-red'></i> 차대구분</label>
					<div class='option'>
						<label for="DE_CR_DIV_1">
							<input type="radio" name="DE_CR_DIV" class="minimal" checked value="1" id="DE_CR_DIV_1"/>지급
						</label>
						<label for="DE_CR_DIV_0">
							<input type="radio" name="DE_CR_DIV" class="minimal" value="0" id="DE_CR_DIV_0"/>미지급
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="modal_amt"><i class='fa fa-circle-o text-aqua'></i> 금액</label>
					<input type="text" class="form-control numeric" id="modal_amt" placeholder="금액 입력">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
		</div>
	</div>
</div>
@stop
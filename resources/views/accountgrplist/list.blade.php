@extends('layouts.main')
@section('class','비용관리')
@section('title','계정그룹별 조회')
@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{
		margin-top: 0px;
		vertical-align: baseline;
		margin: 0 1px;
	}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<!--
			<div class="col-md-1 col-xs-2">
				<div class="btn-group">
					<button type="button" class="btn btn-success btnPdf" id="btnPdf"><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>
			-->

			<div class="col-md-3 col-xs-10">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<label>작성월</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="input-group">
					<!-- <input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}"> -->
					<input type="text" id="ACCOUNT_GRP_CD" name="ACCOUNT_GRP_CD" class="form-control" value="" placeholder="계정그룹" />
					<span class="input-group-btn">
						<button type="submit" name="custgrplist-search-btn" id="custgrplist-search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
					<input type="text" id="ACCOUNT_GRP_NM" name="ACCOUNT_GRP_NM" class="form-control" value="" placeholder="계정그룹명" />
					<span class="input-group-btn">
						<button type="button" name="search-btn" id="search-btn" class="btn btn-info">검색</button>
					</span>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="계정그룹 조회" width="100%">
				<caption>계정그룹</caption>
				<thead>
					<tr>
						<th width="80px">계정과목</th>
						<th width="80px">수입</th>
						<th width="80px">지출</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="pull-right">합계 : </th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		var start = moment().subtract(6, 'days');
		var end = moment();

		// 초기값 세팅 ★★★★ 초기값 아직 지정X ★★★★
		// 입고등록 > 입고일
		$("#modal_sujo_input_date").val(end.format('YYYY-MM-DD'));

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 입고일
		$("#modal_sujo_input_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		// 거래처그룹리스트 조회버튼
		$("#custgrplist-search-btn").click(function(){
			$('#modal_AccountGrpList').modal({show:true});
			// 거래처그룹리스트 값지정
			var pTable = $('#tableCustGrpList').DataTable({
				processing: true,
				serverSide: true,
				retrieve: true,
				iDisplayLength: 10,		// 기본 100개 조회 설정
				bLengthChange: false,
				ajax: {
					url: "/accountgrplist/cust_grp_info/getAccountGrp"
				},
				columns: [
					{ data: 'ACCOUNT_GRP_CD', name: 'ACCOUNT_GRP_CD', className: "dt-body-center" },
					{ data: 'ACCOUNT_GRP_NM',  name: 'ACCOUNT_GRP_NM', className: "dt-body-center" },
					{
						data: "ACCOUNT_GRP_CD",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								return "<button class='btn btn-success' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
							}
							return data;
						},
						className: "dt-body-left"
					},
				],
				"searching": true,
				"paging": true,
				"autoWidth": true,
				"oLanguage": {
					"sLengthMenu": "조회수 _MENU_ ",
					"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
					"sProcessing": "현재 조회 중입니다",
					"sEmptyTable": "조회된 데이터가 없습니다",
					"sZeroRecords": "조회된 데이터가 없습니다",
					"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
					"oPaginate": {
						"sFirst": "처음",
						"sLast": "끝",
						"sNext": "다음",
						"sPrevious": "이전"
					}
				}
			});

			$('#tableCustGrpList tbody').on( 'click', 'button', function () {
				var data = pTable.row( $(this).parents('tr') ).data();

				$("#ACCOUNT_GRP_CD").val(data.ACCOUNT_GRP_CD);
				$("#ACCOUNT_GRP_NM").val(data.ACCOUNT_GRP_NM);
				$('.glyphicon-remove').click();
				oTable.draw();
			});
		});

		//검색하기
		$("#search-btn").on("click",function(){
			oTable.draw();
		});

		//검색하기[값변경시]
		$("input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw();
		});

		// 비용계정그룹 리스트
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo:false,
			ajax: {
				url: "/accountgrplist/custcd/listData",
				data:function(d){
					d.ACCOUNT_GRP_CD	= $("input[name='ACCOUNT_GRP_CD']").val();
					d.start_date		= $("input[name='start_date']").val();
					d.end_date			= $("input[name='end_date']").val();
				}
			},
			columns: [
				{ data: 'ACCOUNT_NM', name: 'ACCOUNT_NM', className: "dt-body-center" },
				{
					data: "AMT2",
					render: function ( data, type, row ) {

						if ( type === 'display' ){
							if( data === null){
								return "0 원";
							}else{
								return Number(data).format()+"원";
							}
						}
						return data;
					}, className: "dt-body-right red"
				},
				{
					data: 'AMT1',
					render: function ( data, type, row ) {
						if( data === null){
							return "0 원";
						}else{
							return Number(data).format()+"원";
						}
						return data;
					}, className: "dt-body-right blue"
				},
				
				{
					"className":      '',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 차변
				UnclAmt = api.column( 1 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 1 ).footer() ).html(UnclAmt.format()+" 원").css({'text-align':'right'});

				// 대변
				UnclAmt2 = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(UnclAmt2.format()+" 원").css({'text-align':'right'});
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		// 숫자 타입에서 천단위 , 추가
		Number.prototype.format = function(){
			if(this==0) return 0;

			var reg = /(^[+-]?\d+)(\d{3})/;
			var n = (this + '');

			while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

			return n;
		};
	});

</script>

<!-- 계정그룹 검색 -->
<div class="modal fade" id="modal_AccountGrpList" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 계정그룹별 조회</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group SearchCustGrp">
					<table id="tableCustGrpList" class="table table-bordered table-hover display nowrap" summary="계정그룹별 조회" width="100%">
						<caption>계정그룹별 조회</caption>
						<thead>
							<tr>
								<th scope="col" class="name" width="40px">그룹코드</th>
								<th scope="col" class="name" width="80px">그룹명</th>
								<th scope="col" class="name" width="auto">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
@stop
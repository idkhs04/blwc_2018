@extends('layouts.main')
@section('class','코드관리')
@section('title','어종정보관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}

	td .editor-active { width:100%; }
	td select { width:80%; }
	.red{
		display:block;
		color:red;
		padding:2px;
	}
	.resultSale, .endResultSale{ display:none; }
	th { white-space: nowrap; }
	span.msg {text-align:center;}
	#modal_pis_insert label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_pis_insert .form-control{
		display:inline-block;
		width:291px;
		clear:both;
	}
	.slideSearchPisMk, .slideSearchCust{ display:none;}
	.btn-info{ vertical-align:baseline; margin-top:0 }

	div.file{ display:none;}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			
			<div class="col-md-3 col-xs-6">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnAdd" ><i class="fa fa-plus-circle"></i> 추가</button>
					<button type="button" class="btn btn-success" id="btnUpdate" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-success" id="btnDelete" ><i class="fa fa-remove"></i> 삭제</button>
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition" id="srtCondition">
					<option value="ALL">전체</option>
					<option value="PIS_MK">어종부호</option>
					<option value="PIS_NM">어종명</option>
				</select>
			</div>

			<div class="col-md-2 col-xs-6">
				<label>
					<input type="checkbox" name="chkSale" id="chkSALE" class='chkQuery' />
					판매
				</label>

				<label>
					<input type="checkbox"  name="chkCommon" id="chkCOMMON" class='chkQuery' />
					일반
				</label>
			</div>

			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="submit" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종 정보조회" width="100%">
				<caption>어종 정보관리 조회</caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="30px">어종부호</th>
						<th width="60px">어종명</th>
						<th width="100px">학명</th>
						<th width="60px">분류</th>
						<th width="auto">선택</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>	
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	$(function () {
		
		var start = moment().subtract(6, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');
		
		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
			$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
		}
			
		// 어종정보 작성일자
		$("#modal_write_dt").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		
		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			opens: "right",
			drops: "down",
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "",
			ranges: {
				'오늘': [moment(), moment()],
				'어제': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'지난 1주일': [moment().subtract(6, 'days'), moment()],
				'지난 30일': [moment().subtract(29, 'days'), moment()],
				'이번 달': [moment().startOf('month'), moment().endOf('month')],
				'지난 달': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);

		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"numeric-comma-pre": function ( a ) {
				var x = (a == "-") ? 0 : a.replace( /,/, "." );
				return parseFloat( x );
			},
		 
			"numeric-comma-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},
		 
			"numeric-comma-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		} );

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 10,		// 기본 10개 조회 설정
			bLengthChange: false,
			ajax: {
				url: "/pis/pis/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.chkSale		= $("#chkSALE").is(":checked") ? "1" : "0";
					d.chkCommon		= $("#chkCOMMON").is(":checked") ? "1" : "0";
				}
			},
			aoColumnDefs: [
				{ "sType": "numeric-comma", "aTargets": [2] }
			],
			order: [[ 1, 'asc' ]],
			columns: [
				{
					data:   "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='"+data+"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'PIS_MK',		 name: 'PIS_MK' },
				{ data: 'PIS_NM',		 name: 'PIS_NM' },
				{ data: 'HAKNM',		 name : 'HAKNM'},
				{ data: 'CLASS',		 name : 'CLASS'},
				{ 
					data: "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='btn btn-success btnGoDetail' ><i class='fa fa-location-arrow'></i> 선택</button>";
						}
						return data;
					},
					className: "dt-body-left" 
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date'], .chkQuery").on("change", function(){
			oTable.draw() ;
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
		});
		
		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});

		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/pis/pis/detail/" + data.PIS_MK;
		});

		// 어종추가
		$("#btnAdd").click(function(){
			$("#mode").val("ins");
			defaltModalInsert();

			$('#modal_pis_insert').modal({show:true}); 
			$("#modal_pis_mk, #modal_pis_nm").prop("readonly", false);
			$("#btnChkPrimary").prop("disabled", false)
		});

		// 입력창 닫기
		$('#modal_pis_insert').on('hidden.bs.modal', CbSetInitModalClose);
		
		// 차대구분 선택
		$("input[name='modal_remark']").click(function(){
			$("#modal_remark_text").val( $(this).val() );
			if( $(this).val() != ""){
				$("#modal_remark_text").prop("readonly", true);
			}else{
				$("#modal_remark_text").prop("readonly", false);
			}
			$("#modal_remark_text").focus();
		});

		// 어종 저장 버튼
		$("#btnInsertSave").click(function(){
			
			var SALE_TARGET	= $("#modal_pis_insert input[name='sale_target']:checked").val();
			var form = document.forms.namedItem("attach_form"); // high importance!, here you need change "yourformname" with the name of your form
			var formdata = new FormData(form); // high importance!
			var mode = $("#mode").val();
			//alert(mode);
			if( mode == "ins"){
				$.ajax( {
						url : '/pis/pis/setpisData',
						dataType:'json',
						type : 'POST',
						enctype: 'multipart/form-data',
						data : formdata,
						contentType: false,
						processData: false, // high importance!
					
						success : function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							if (data.result == "success") {
								$('#modal_pis_insert').modal("hide"); 
							}
							oTable.draw();

						},
						error : function(xhr){
							var error = jQuery.parseJSON(xhr.responseText); 
							var info = $('#modal_pis_insert .edit_alert');
							info.hide().find('ul').empty();
								for(var k in error.message){
									if(error.message.hasOwnProperty(k)){
										error.message[k].forEach(function(val){
											info.find('ul').append('<li>' + val + '</li>');
										});
									}
								}
							info.slideDown();
						}
				});

			}else{

				$.ajax( {
						url : '/pis/pis/updpisData',
						dataType:'json',
						type : 'POST',
						enctype: 'multipart/form-data',
						data : formdata,
						contentType: false,
						processData: false, // high importance!
					
						success : function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							if (data.result == "success") {
								$('#modal_pis_insert').modal("hide"); 
							}
							oTable.draw();

						},
						error : function(xhr){
							var error = jQuery.parseJSON(xhr.responseText); 
							var info = $('#modal_pis_insert .edit_alert');
							info.hide().find('ul').empty();
								for(var k in error.message){
									if(error.message.hasOwnProperty(k)){
										error.message[k].forEach(function(val){
											info.find('ul').append('<li>' + val + '</li>');
										});
									}
								}
							info.slideDown();
						}
				});
			}
		});

		// 어종 수정
		$("#btnUpdate").click(function(){
			defaltModalInsert();
			$("#mode").val("upd");

			$("#modal_pis_mk, #modal_pis_nm").prop("readonly", true);
			$("#btnChkPrimary").prop("disabled", true);

			var arr_code = [];
			//var row = null;

			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정은 한개만 선택해주세요") ).show();
				return false;
			}
		
			if( arr_code.length == 1){
				$('#modal_pis_insert').modal({show:true}); 
				$.getJSON('/pis/pis/getDetailData', {
					PIS_MK : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					data = data.result;

					$("#modal_pis_mk").val(data.PIS_MK);
					$("#modal_pis_nm").val(data.PIS_NM);
					$("#modal_haknm").val(data.HAKNM);
					$("#modal_class").val(data.CLASS);
					$("#modal_form_plce").val(data.FORM_PLCE);
					$("#modal_class_area").val(data.CLASS_AREA);
					$("#modal_normal_temp").val(data.NORMAL_TEMP);
					if( data.SALE_TARGET == "1" ){
						$("#SALE_TARGET1").prop("checked", true); 
						$("#SALE_TARGET0").prop("checked", false);
					}else{
						$("#SALE_TARGET0").prop("checked", true); 
						$("#SALE_TARGET1").prop("checked", false);
					}

					if( data.FILENAME !== null ){
						$("div.file").css("display", "block");
						$("div.file").append("<img src='/uploads/" + data.FILENAME + "' width='70%'/>");
					}else{
						$("div.file").css("display", "none").empty();
					}
				});
			}
		});
		
		// 어종 입력 초기화
		function defaltModalInsert(){
			
			$("#modal_pis_insert input[type='text']").val("");
			$("#modal_pis_insert input[type='file']").val("");
			$("#modal_write_dt").val(today);

			$("#modal_pis_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_pis_insert input[name='modal_remark']:first").prop("checked", true);
			
			$("#modal_pis_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_pis_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$('.edit_alert').hide().find('ul').empty();
			$(".slideSearchCust" ).slideUp();	
			$("#modal_remark_text").focus();
			$("span.msg").hide();
			$("div.file").css("display", "none").empty();
		}

		// 어종부호 중복체크
		$("#btnChkPrimary").click(function(){
			
			$.getJSON('/pis/pis/chkPIS_MK', {
				_token		: '{{ csrf_token() }}',
				PIS_MK		: $("#modal_pis_mk").val(),
				chkPIS_MK	: $("input[type='chkPIS_MK']").val(),
			}).success (function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				if( data.result == "success"){
					$("input[name='chkPIS_MK']").val(true);
					$(".edit_valert").html( getAlert('success', '확인', "사용가능합니다") ).show();
				}
				
			}).error(function(xhr){
				$("input[name='chkPIS_MK']").val(false);
				$(".edit_valert").html( getAlert('warning', '경고', "해당 어종코드는 사용 불가능합니다") ).show();
			});
		});

		$("#btnDelete").click(function(){

			
			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});

			$(".content > .alert").remove();

			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				if(confirm("선택된 어종을 삭제하시겠습니까?")){
					$.getJSON("/pis/pis/_delete", {
						PIS_MK: arr_code ,
						_token : '{{ csrf_token() }}'
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						
						if (data.result == "success") {
							oTable.draw();
						}
					}).error(function(xhr,status, response) {
						
						$(".content > .alert").remove();
						$(".content").prepend( getAlert('danger', '경고', "현 어종은 사용중이므로 삭제 할 수 없습니다.") ).show();
						return;

						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.dvAlert');
						info.append("<ul></ul>");
						info.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li>' + error.message + '</li>');
						}else{
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						
						info.slideDown();
						oTable.draw();
					});
				}
			}
		});
	});

</script>
<!-- 어종 추가 팝업 -->
<div class="modal fade" id="modal_pis_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<form name="attach_form" method="POST" enctype="multipart/form-data" action="">
				<input type="hidden" name="mode" id="mode" value="" />
				<div class="modal-header" style="padding:15px 35px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><span class="glyphicon glyphicon-th-large"></span> 어종 정보 입력 </h4>
				</div>
				<div class="modal-body" style="padding:20px 20px;">
					<div class="edit_valert"></div>
					<div class="edit_alert"><ul></ul></div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<input type="hidden" name="chkPIS_MK" value="" />
					<div class="form-group">
						<label for="modal_pis_mk"><i class='fa fa-circle-o text-red'></i> 어종부호</label>
						<input type="text" class="form-control" id="modal_pis_mk" name="PIS_MK" style="width:80px;" placeholder="어종부호" />
						<button type="button" name="search" id="btnChkPrimary" class="btn btn-info">중복체크</button>
						<input type="text" class="form-control" id="modal_pis_nm" name="PIS_NM" placeholder="어종명" style="width:22%;"/>
						<span class="msg red"></span>
					</div>
					<div class="form-group">
						<label for="modal_sale_target"><i class='fa fa-circle-o text-aqua'></i> 판매대상</label>
						<input type="radio"  name="SALE_TARGET" value="1" id="SALE_TARGET1" /> 판매어종
						<input type="radio"  name="SALE_TARGET" value="0" id="SALE_TARGET0" /> 일반어종
					</div>
					@role('sysAdmin')
					<div class="form-group">
						<label for="modal_haknm"><i class='fa fa-circle-o text-aqua'></i> 학명</label>
						<input type="text" class="form-control" id="modal_haknm" name="HAKNM" placeholder="학명" />
						
					</div>
					<div class="form-group">
						<label for="modal_class"><i class='fa fa-circle-o text-aqua'></i> 분류</label>
						<input type="text" class="form-control" id="modal_class" name="CLASS" placeholder="분류" />

					</div>
					<div class="form-group">
						<label for="modal_form_plce"><i class='fa fa-circle-o text-aqua'></i> 서식장소</label>
						<input type="text" class="form-control" id="modal_form_plce" name="FORM_PLCE"  placeholder="서식장소" />
					</div>
					<div class="form-group">
						<label for="modal_class_area"><i class='fa fa-circle-o text-aqua'></i> 분포지역</label>
						<input type="text" class="form-control" id="modal_class_area" name="CLASS_AREA" placeholder="분포지역">
					</div>
					<div class="form-group">
						<label for="modal_normal_temp"><i class='fa fa-circle-o text-aqua'></i> 적정수온</label>
						<input type="text" class="form-control" id="modal_normal_temp" name="NORMAL_TEMP" placeholder="적정수온">
					</div>
					
					<div class="form-group">
						<label for="modal_filename"><i class='fa fa-circle-o text-aqua'></i> 파일명</label>
						<input type="file" class="form-control" id="modal_filename" name="FILENAME" />
						<div class='file'> </div>
					</div>
					@endrole
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success pull-right" id="btnInsertSave">
						<span class="glyphicon glyphicon-off"></span> 저장
					</button>
					<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
						<span class="glyphicon glyphicon-remove"></span> 닫기 
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

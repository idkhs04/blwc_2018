@extends('layouts.main')
@section('class','코드관리')
@section('title','어종관리 상세')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	.headerSumQty li{
		display:inline-block;
		padding:0 10px;
	}

	#tblDetail th{
		background-color:khaki; 
		height:25px;
	}
	#tblDetail td, #tblDetail th{ padding-top:3px;padding-bottom:3px;}

	#modal_piscls_insert label{
		display:inline-block;
		width:100px;
		clear:both;
	}

	#modal_piscls_insert .form-control{
		display:inline-block;
		width:120px;
		clear:both;
	}
	.btn-info{
		vertical-align:baseline;
		margin-top:0;
	}
</style>
@include('include.search_header', ['pis' => $pis])
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-12 col-xs-12">
				<table id="tblDetail" class="table table-bordered table-hover display nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr><th width="100px">어종부호</th><td class='pis_mk'>{{$PIS_INFO->PIS_MK}}</td></tr>
						<tr><th>어종명</th><td class='pis_nm'>{{$PIS_INFO->PIS_NM}}</td></tr>
						<tr><th>학명</th><td>{{$PIS_INFO->HAKNM}}</td></tr>
						<tr><th>분류</th><td>{{$PIS_INFO->CLASS}}</td></tr>
						<tr><th>서식장소</th><td>{{$PIS_INFO->FORM_PLCE}}</td></tr>
						<tr><th>분포지역</th><td>{{$PIS_INFO->CLASS_AREA}}</td></tr>
						<tr><th>적정수온</th><td>{{$PIS_INFO->NORMAL_TEMP}}</td></tr>
						<tr><th>판매대상</th><td>{{$PIS_INFO->FILENAME}}</td></tr>
						<tr><th>파일명</th><td><a href="/uploads/{{$PIS_INFO->FILENAME}}" target="_blank"><img src="/uploads/{{$PIS_INFO->FILENAME}}" width="200px;"/></a></td></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="어종부호 목록 목록" width="100%">
				<caption></caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="60px">어종부호</th>
						<th width="60px">규격</th>
						<th width="auto">비고</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

$(function () {

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			ajax: {
				url: "/pis/pis/detailListData",
				data:function(d){
					d.PIS_MK		= "{{ Request::segment(4)}}";
					
				}
			},
			columns: [
				{
					data:   "PIS_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center chk"
				},
				{ data: 'PIS_MK', name: 'PIS_MK' },
				{ data: 'SIZES', name: 'SIZES' },
				{ data: "REMARK",  name: 'REMARK'},
				
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		
		// 규격코드 입력 추가
		$("#btnAdd").click(function(){
			defaltModalInsert();
			$("#mode").val("ins");

			$("#modal_piscls_mk").val("{{Request::segment(4)}}")
			$("#modal_piscls_nm").val($(".pis_mk").html())
			
			$("#btnPisInfoList").prop("disabled", false);
			$('#modal_piscls_insert').modal({show:true}); 
		});

		$('#modal_piscls_insert').on('hidden.bs.modal', defaltModalInsert);
		
		// 규격코드 수정

		// 수정
		$("#btnUpdate").click(function(){
			

			var row = oTable.row( $("#tblList tbody > tr > td > input[type='checkbox']:checked").parents('tr') ).data();
			var arr_code = [];
			
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			if( arr_code.length == 0){
				$(".main_alertMsg").html( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".main_alertMsg").html( getAlert('warning', '경고', "수정할 항목은 한개만 선택해주세요") ).show();
				return false;
			}
			
			if( arr_code.length == 1){
				
				if( row.SIZES == "마리" ){
					row.SIZES = "QTYS";
				}
				$.getJSON('/piscls/piscls/getDataDetail', {
					PIS_MK	: row.PIS_MK ,
					SIZES	: row.SIZES,
					_token		: '{{ csrf_token() }}'
				}).success(function(data){
					
					var item = data[0];

					$("#mode").val("upd");
					$("#btnPisInfoList").prop("disabled", true);
					$("#modal_piscls_insert input[name='radSIZES']").prop("disabled", false);

					$("#modal_piscls_insert").modal("show");
					$("#modal_piscls_insert input[name='SIZES']").prop("readonly", true);

					$("#modal_piscls_insert input[name='PIS_MK']").val(item.PIS_MK)
					$("#modal_piscls_insert input[name='PIS_NM']").val(item.PIS_NM)

					if( item.SIZES == "QTYS"){
						$("#modal_piscls_insert input[name='SIZES']").val("마리");
						$("#modal_piscls_insert input[name='radSIZES']:nth-child(1)").prop("checked", false);
						$("#modal_piscls_insert input[name='radSIZES']:nth-child(2)").prop("checked", true);
					}else{
						$("#modal_piscls_insert input[name='SIZES']").val(item.SIZES)
						$("#modal_piscls_insert input[name='radSIZES']:nth-child(1)").prop("checked", true);
						$("#modal_piscls_insert input[name='radSIZES']:nth-child(2)").prop("checked", false);
						
					}
					$("#modal_piscls_insert input[name='SIZES']").val(item.SIZES)

					$("#modal_piscls_insert input[name='REMARK']").val(item.REMARK)
					

					
				}).error(function(xhr,status, response) {
					
					var data = jQuery.parseJSON(JSON.stringify(xhr.responseJSON));
					$(".main_alertMsg").html( getAlert('danger', '경고', "오류입니다. 다시한번 시도하시고 안되면 관리자에게 문의하세요.") ).show();
				
				});	
			}
		});

		// 규격 저장(입력/저장) 버튼
		$("#btnInsertSave").click(function(){
			var info = $('#modal_piscls_insert .edit_alert');

			if( $("input[name='radSIZES']:checked").val() == "SIZES" && $("input[name='SIZES']").val().trim() == "마리"){				
				info.hide().find('ul').empty();
				info.find('ul').append('<li> [마리]로 선택할 경우 마리로 선택필수</li>');
				info.slideDown();
				return false;
			}
			
			var mode = $("#mode").val();
			var	url  = '/piscls/piscls/setpisClsData'; 
			var JsonData = {
								"PIS_MK"	: $("input[name='PIS_MK']").val(),
								"SIZES"		: ( $("input[name='radSIZES']:checked").val() == "SIZES" ? $("input[name='SIZES']").val() : $("#modal_qtys").val() ),
								"REMARK"	: $("input[name='REMARK']").val(),
			};
			
			if( mode != "ins"){
				url = '/piscls/piscls/updpisClsData'
			}
			$.ajax( {
				url : url,
				dataType:'json',
				type : 'GET',
				data : JsonData,			
				success : function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					if (data.result == "success") {
						$('#modal_piscls_insert').modal("hide"); 
					}
					oTable.draw();

				},
				error : function(xhr){
					var error = jQuery.parseJSON(xhr.responseText); 
					info.hide().find('ul').empty();
					if( error.result == "DUPLICATION"){
						info.find('ul').append('<li> 이미 동일한 데이터가 존재합니다.</li>');
					}else if( error.result == "DB_ERROR"){
						info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
					}else{
						for(var k in error.message){
							info.find('ul').append('<li>' + k + '</li>');
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					}
					info.slideDown();
				}
			});
		});

		// 삭제
		$("#btnDelete").click(function(){

			var row = oTable.row( $("#tblList tbody > tr > td > input[type='checkbox']:checked").parents('tr') ).data();
			var arr_code = [];
			
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			if( arr_code.length == 0){
				$(".main_alertMsg").html( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".main_alertMsg").html( getAlert('warning', '경고', "삭제할 항목은 한개만 선택해주세요") ).show();
				return false;
			}
			
			if( arr_code.length == 1){
				
				if( confirm("삭제하시겠습니까?") ){
					if( row.SIZES == "마리" ){
						row.SIZES = "QTYS";
					}
					$.getJSON('/piscls/piscls/_delete', {
						PIS_MK	: row.PIS_MK ,
						SIZES	: row.SIZES,
						_token		: '{{ csrf_token() }}'
					}).success(function(data){
						oTable.draw();
					}).error(function(xhr,status, response) {
						var info = $('#modal_pis_insert .edit_alert');
						var error = jQuery.parseJSON(xhr.responseText); 
						info.hide().find('ul').empty();

						if( error.result == "DUPLICATION"){
							info.find('ul').append('<li> 이미 동일한 데이터가 존재합니다.</li>');
						}else if( error.result == "DB_ERROR"){
							info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
						}else{
							for(var k in error.message){
								info.find('ul').append('<li>' + k + '</li>');
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}
						info.slideDown();

						$(".main_alertMsg").html( getAlert('danger', '경고', "현재 사용중이므로 삭제 불가능합니다.") ).show();
					
					});
				}
			}
		});

		// 어종 입력 초기화
		function defaltModalInsert(){
			
			$("#modal_piscls_insert input[type='text']").val("");

			$("#modal_piscls_insert input[name='modal_remark']").prop("checked", false);
			$("#modal_piscls_insert input[name='modal_remark']:first").prop("checked", true);
			
			$("#modal_piscls_insert input[name='DE_CR_DIV']").prop("checked", false);
			$("#modal_piscls_insert input[name='DE_CR_DIV']:first").prop("checked", true);

			$('.edit_alert').hide().find('ul').empty();
			$(".slideSearchCust" ).slideUp();	
			$("#modal_remark_text").focus();
			$("span.msg").hide();
		}

	});

</script>
<!-- 어종추가 팝업 -->
<div class="modal fade" id="modal_pis_insert" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content modal-sm">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 어종 추가 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="box box-primary">
					<div class="box-body">
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>

				<button type="button" class="btn btn-success btn-default pull-right" id="btnSavePis">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
			</div>
		</div>
	</div>
</div>


<!-- 규격코드 추가 팝업 -->
<div class="modal fade" id="modal_piscls_insert" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 규격코드 정보 입력 </h4>
			</div>
			<div class="modal-body" style="padding:20px 20px;">
				<div class="edit_alert"><ul></ul></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<input type="hidden" name="chkPIS_MK" value="" />
				<input type="hidden" id="mode" value="" />
				<div class="form-group">
					<label for="modal_piscls_mk"><i class='fa fa-check-circle text-red'></i> 어종부호</label>
					<input type="text" class="form-control" id="modal_piscls_mk" name="PIS_MK" placeholder="어종부호" readonly="readonly" />
					<input type="text" class="form-control" id="modal_piscls_nm" name="PIS_NM" placeholder="어종명" readonly="readonly" />
					<span class="msg"></span>
				</div>
				
				<div class="form-group">
					<label for="modal_sizes"><i class='fa fa-check-circle text-red'></i> 규격/마리</label>
					<input type="hidden" class="form-control" id="modal_qtys" value="QTYS" />
					<input type="text" class="form-control" id="modal_sizes" name="SIZES" placeholder="규격/마리" />
					<input type="radio"  name="radSIZES" value="SIZES" checked="checked" /> Kg
					<input type="radio"  name="radSIZES" value="QTYS" /> 마리
				</div>
				
				<div class="form-group">
					<label for="modal_piscls_remark"><i class='fa fa-check-circle text-aqua'></i> 비고</label>
					<input type="text" class="form-control" id="modal_piscls_remark" name="REMARK"  placeholder="비고"  style="width:70%"/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-default pull-right" id="btnInsertSave">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>
				<button type="button" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
			
		</div>
	</div>
</div>
@stop
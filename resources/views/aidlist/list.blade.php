@extends('layouts.main')
@section('title','계정별 조회')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{
		margin-top: 0px;
		vertical-align: baseline;
		margin: 0 1px;
	}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-1 col-xs-3">
				<div class="btn-group">
					<button type="button" class="btn btn-success btnPdf" ><i class="fa fa-file-pdf-o"></i> 출력</button>
				</div>
			</div>
			<div class="col-md-3 col-xs-9">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<label>작성일</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-4 col-xs-12">
				<div class="input-group">
					<input type="text" id="ACCOUNT_MK" name="ACCOUNT_MK" class="form-control" value="" placeholder="계정과목" />
					<span class="input-group-btn">
						<button type="submit" name="custgrplist-search-btn" id="custgrplist-search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>

					<input type="text" id="ACCOUNT_NM" name="ACCOUNT_NM" class="form-control" value="" placeholder="계정과목명" >

					<!-- <span class="input-group-btn">
						<button type="submit" name="search-btn" id="search-btn" class="btn btn-flat">검색</button>
					</span> -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="비용계정그룹 조회" width="100%">
				<caption>비용계정그룹</caption>
				<thead>
					<tr>
						<th width="60px">일자</th>
						<th width="60px">계정부호</th>
						<th width="80px">계정명</th>
						<th width="80px">상호</th>

						<th width="60px">수입금액</th>
						<th width="60px">지출금액</th>
						<th width="60px">잔액</th>
						<th width="auto">적요</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th class="pull-right">합계 : </th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>

</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		var start = moment().subtract(6, 'days');
		var end = moment();
		var sum=0;

		// 초기값 세팅 ★★★★ 초기값 아직 지정X ★★★★
		// 입고등록 > 입고일
		$("#modal_sujo_input_date").val(end.format('YYYY-MM-DD'));

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 입고일
		$("#modal_sujo_input_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",

			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		// 거래처그룹리스트 조회버튼
		$("#custgrplist-search-btn").click(function(){
			$('#modal_AccountGrpList').modal({show:true});
			// 거래처그룹리스트 값지정
			var pTable = $('#tableCustGrpList').DataTable({
				processing: true,
				serverSide: true,
				retrieve: true,
				iDisplayLength: 10,		// 기본 10개 조회 설정
				bLengthChange: false,
				bInfo : false,
				ajax: {
					url: "/aidlist/cust_grp_info/getAccountingCode"
				},
				columns: [
					{ data: 'ACCOUNT_GRP_NM',  name: 'ACCOUNT_GRP_NM' },
					{ data: 'ACCOUNT_NM',  name: 'ACCOUNT_NM' },
					{ data: 'DE_CR_DIV', name: 'DE_CR_DIV' },
					{
						data: "ACCOUNT_MK",
						render: function ( data, type, row ) {
							if ( type === 'display' ) {
								return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
							}
							return data;
						},
						className: "dt-body-center"
					},
				],
				"searching": true,
				"paging": true,
				"autoWidth": true,
				"oLanguage": {
					"sLengthMenu": "조회수 _MENU_ ",
					"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
					"sProcessing": "현재 조회 중입니다",
					"sEmptyTable": "조회된 데이터가 없습니다",
					"sZeroRecords": "조회된 데이터가 없습니다",
					"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
					"oPaginate": {
						"sFirst": "처음",
						"sLast": "끝",
						"sNext": "다음",
						"sPrevious": "이전"
					}
				}
			});

			$('#tableCustGrpList tbody').on( 'click', 'button', function () {
				var data = pTable.row( $(this).parents('tr') ).data();

				$("#ACCOUNT_MK").val(data.ACCOUNT_MK);
				$("#ACCOUNT_NM").val(data.ACCOUNT_NM);
				$('.glyphicon-remove').click();
				getBeforeAmt();
				oTable.draw();
			});
		});

		// 비용계정그룹 리스트
		getBeforeAmt();
		
		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/aidlist/custcd/listData",
				data:function(d){
					d.ACCOUNT_NM	= $("input[name='ACCOUNT_NM']").val();
					d.ACCOUNT_MK	= $("input[name='ACCOUNT_MK']").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			columns: [
				{ data: 'WRITE_DATE', name: 'WRITE_DATE', className: "dt-body-center" },
				{ data: 'ACCOUNT_MK', name: 'ACCOUNT_MK', className: "dt-body-center" },
				{ data: 'ACCOUNT_NM', name: 'ACCOUNT_NM', className: "dt-body-center" },
				{ data: 'FRNM', name: 'FRNM', className: "dt-body-left" },
				{
					data: 'AMT2',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format()+"원" ;
						}
						return data;
					},
					className: "dt-body-right red" 
				},
				{
					data: 'AMT1',

					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format()+"원" ;
						}
						return data;
					},
					className: "dt-body-right blue" 
				},
				
				{
					data: null,
					render: function ( data, type, row ) {
						sum = sum + (parseFloat(row.AMT2) - parseFloat(row.AMT1));
						if ( type === 'display' ) {
							return Number(sum).format()+"원";
						}
						return data;
					},
					className: "dt-body-right green"
				},
				{ data: 'OUTLINE', name: 'OUTLINE', className: "dt-body-left" },
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 수입금액
				UnclAmt = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(UnclAmt.format()+"원").css({'text-align':'center'});

				// 지출금액
				UnclAmt2 = api.column( 5 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 5 ).footer() ).html(UnclAmt2.format()+"원").css({'text-align':'center'});

				// 잔액
				//UnclAmt3 = (UnclAmt - UnclAmt2);
				//$( api.column( 6 ).footer() ).html(UnclAmt3.format()+"원").css({'text-align':'center'});
				sum=0;
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				  }
			}
		});

		//검색하기
		$("#search-btn").on("click",function(){
			getBeforeAmt();
			oTable.draw();
		});

		//검색하기[값변경시]
		$("input[name='start_date'], input[name='end_date']").on("change", function(){
			getBeforeAmt();
			oTable.draw();
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		/*
		$(".btnPdf").click(function(){

			$("form[name='getPdf'] > input[name='start_date']").val( $("#reportrange input[name='start_date']").val() );
			$("form[name='getPdf'] > input[name='end_date']").val( $("#reportrange input[name='end_date']").val() );
			$("form[name='getPdf'] > input[name='ACCOUNT_MK']").val( $("#ACCOUNT_MK").val() );
			$("form[name='getPdf']").submit();
		});
		*/

		$(".btnPdf").click(function(){

			var width=740;
			var height=720;
			var start_date		= $("#reportrange input[name='start_date']").val();
			var end_date		= $("#reportrange input[name='end_date']").val();
			var ACCOUNT_MK		= $("#ACCOUNT_MK").val();

			var url = "/aidlist/aidlist/PdfList?start_date=" + start_date + "&end_date=" + end_date + "&ACCOUNT_MK=" + ACCOUNT_MK;
			getPopUp(url , width, height);
		});

		function getBeforeAmt(){
			$.getJSON('/aidlist/aidlist/listDataBeforeSum', {
				start_date	:  $("input[name='start_date']").val()
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				if( data.BEFORE_SUM === null){
					data.BEFORE_SUM = 0;
				}
				sum  = parseFloat(data.BEFORE_SUM);
				console.log(sum);
			}).error(function(xhr,status, response) {

			});
		}
	});

</script>
<!--
<div id="pdf">
	<form name="getPdf" method="post" action="/aidlist/aidlist/PdfList" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="start_date" />
		<input type="hidden" name="end_date" />
		<input type="hidden" name="ACCOUNT_MK" />
	</form>
</div>
 -->
<!-- 회계계정코드 조회검색 -->
<div class="modal fade" id="modal_AccountGrpList" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 회계계정코드 조회</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group SearchCustGrp">
					<table id="tableCustGrpList" class="table table-bordered table-hover"  width="100%" summary="게시물 목록">
						<thead>
							<tr>
								<th class="name">그룹명</th>
								<th class="name">계정명</th>
								<th class="name">차/대구분</th>
								<th class="name">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<!-- <button type="submit" class="btn btn-success btn-block" id="btnUpdateAccountGrpOK">
					<span class="glyphicon glyphicon-off"></span> 선택
				</button> -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
@stop
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>매출계산서 합계</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:14px; }
			
			table > tr > td { border-top:1px solid block;border-bottom:1px solid block;padding:0 3px 0 3px;}
			td { height:20px;}
			table {
				border-collapse: collapse;
			}

			table, th, td {
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}

		</style>
			
		</head>
		<body>
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body">
						<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>{{ $title }}</span> 
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
							<tr>
								<td style="text-align:right;font-size:16px;text-decoration: underline;">{{ $corpnm }}</td>
							</tr>
						</table>

						<table id="tblList" summary="매출계산서 합계" width="100%" style="border-bottom:1px solid block;" border="1" cellpadding="0" cellspacing="0">
							<caption>매출계산서 합계</caption>
							<thead>	
							<tr>
								<!--
								<th style="text-align:center;" >월</th>
					<th style="text-align:center;">매출액</th>
					<th style="text-align:center;">매입계산서 금액</th>
					<th style="text-align:center;">매입건</th>
					<th style="text-align:center;">매출액 대비 매입 비율</th>
					<th style="text-align:center;">매입계산서 과부족</th>
					<th style="text-align:center;">매출계산서 금액</th>
					<th style="text-align:center;">매출건</th>
					<th style="text-align:center;">매출계산서 과부족</th>
					<th style="text-align:center;">비 고</th>
								-->
								<td style='text-align:center;' >월</td>
								<td style='text-align:center;''>매출액</td>
								<td style='text-align:center;''>매입계산서 금액</th>
								<td style='text-align:center;''>매입건</th>
								<td style='text-align:center;'>매출대비매입비율 (%)</th>
								<td style='text-align:center;''>매입계산서 과부족</th>
								<td style='text-align:center;'>매출계산서 금액 </th>
								<td style='text-align:center;''>매출건</th>
								<td style='text-align:center;''>매출계산서 과부족</th>
								<td style='text-align:center;''>비고</th>

							</tr>
							
							</thead>
							</tbody>
							@foreach ($model as $key => $item)
							<tr>
								<td style="text-align:center;">{{ $item->MONTHS }}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->dec_money)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->INPUT)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->INPUT_CNT)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->saleVSbuy)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->overBuy)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->OUT)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->OUT_CNT)}}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($item->overSale)}}</td>
								<td></td>
							</tr>
							@endforeach
							</tbody>
							<tr>
								<td style="text-align:center;">합&nbsp;&nbsp;계</td>
								<td style="text-align:right;"></td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($sumInput) }}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($sumInputQty) }}</td>
								<td style="text-align:right;"></td>
								<td style="text-align:right;"></td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($sumOut) }}</td>
								<td style="text-align:right;padding-right:3px;">{{ number_format($sumOutQty) }}</td>
								<td style="text-align:right;"></td>
								<td style="text-align:right;"></td>
							</tr>
							
						</table>
					</div>
				</div>
			<!-- 본문  -->
			</div>
		</body>
</html>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>판매일보</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:13.5px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				border-collapse: collapse;
				border-bottom:3px solid block;
			}

			table, th, td {
				/*border: 1px solid black;*/
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}
			td{padding-left:3px;}
			
			@page { margin-bottom:0px; }
			.page-break { page-break-after:always; }
			
			div.page-break:last-child{ page-break-after:avoid; }
		</style>
			
	</head>
	<body>
		{{--*/ $k  = 0 /*--}}
		@foreach($model as $model)
		<div class="page-break">
			@for($loop=0; $loop < 2; $loop++)
			<br/>
			<br/>
			<br/>
			<br/>
			<table width="100%" border="0.5" cellpadding="0" cellspacing="0" style="border-bottom:none;">
				<tr>
					<td rowspan="2" style="text-align:center;font-size:25px;border-right:none;width:40%;" >계&nbsp;&nbsp;&nbsp;&nbsp;산&nbsp;&nbsp;&nbsp;&nbsp;서</td>
					<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" >(</td>
					@if($loop==0)
					<td style="text-align:center;border-right:none;border-left:none;border-bottom:none;font-size:14px;" >공&nbsp;&nbsp;급&nbsp;&nbsp;자</td>
					@else
					<td style="text-align:center;border-right:none;border-left:none;border-bottom:none;font-size:14px;" >공급받는자</td>
					@endif
					<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" > )</td>
					<td rowspan="2" style="text-align:center;border-left:none;" ></td>
					<td style="text-align:center" >책&nbsp;&nbsp;번&nbsp;&nbsp;호</td>
					<td style="text-align:right;padding-right:3px;">권</td>
					<td style="text-align:right;padding-right:3px;">호</td>
				</tr>
				<tr>
					
					<td style="text-align:center;border-right:none;border-left:none;border-top:none;font-size:14px;">보&nbsp;&nbsp;관&nbsp;&nbsp;용</td>	
					<td style="text-align:center;padding-left:3px;">일련번호</td>
					<td colspan="2" style="padding-left:3px;">{{ $srl_no[$k] }}</td>
				</tr>
			</table>
			<table id="tblList1" summary="공급자 공급받는자 정보" width="100%" border="0.5" cellpadding="0" cellspacing="0">
				<tr>
					<td rowspan="4" style="text-align:center;width:20px;">공<br/>급<br/>자</td>
					<td style="text-align:center;">등록번호</td>
					<td colspan="3" style="padding-left:3px;">{{$model->CORP_ETPR_NO}}</td>
					<td rowspan="4" style="text-align:center;width:20px;">공<br/>급<br/>받<br/>는<br/>자</td>
					<td style="text-align:center;">등록번호</td>
					<td colspan="3" style="padding-left:3px;">{{$model->ETPR_NO}}</td>
				</tr>
				<tr>
					<td style="text-align:center;"> 상호(법인명)</td>
					<td style="padding-left:3px;"> {{$model->CORP_NM}}</td>
					<td style="text-align:center;">성명</td>
					<td style="padding-left:3px;"> {{$model->CORP_RPST}}</td>
					<td style="text-align:center;">상호(법인명)</td>
					<td style="padding-left:3px;"> {{$model->FRNM}}</td>
					<td style="text-align:center;">성명</td>
					<td style="padding-left:3px;"> {{$model->RPST}}</td>
				</tr>
				<tr>
					<td style="text-align:center;">사업장주소</td>
					<td colspan="3" style="padding-left:3px;">{{$model->CORP_ADDR1}} {{$model->CORP_ADDR2}}</td>
					<td style="text-align:center;">사업장주소</td>
					<td colspan="3" style="padding-left:3px;">{{$model->ADDR1}} {{$model->ADDR2}}</td>
				</tr>
				<tr>
					<td style="text-align:center;">업태</td>
					<td style="padding-left:3px;">{{$model->CORP_UPTE}}</td>
					<td style="text-align:center;">종목</td>
					<td style="padding-left:3px;">{{$model->CORP_UPJONG}}</td>
					<td style="text-align:center;">업태</td>
					<td style="padding-left:3px;">{{$model->UPTE}}</td>
					<td style="text-align:center;">종목</td>
					<td style="padding-left:3px;">{{$model->UPJONG}}</td>
				</tr>
			</table>
			<table id="tblList2" summary="어종별 재고조회" width="100%" border="0.5" cellpadding="0" cellspacing="0" style="border-bottom:none;">
				<tr>
					<td colspan="3" style="text-align:center;">작&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;성</td>
					<td colspan="12" style="text-align:center;">공&nbsp;&nbsp;&nbsp;&nbsp;급&nbsp;&nbsp;&nbsp;&nbsp;가&nbsp;&nbsp;&nbsp;&nbsp;액</td>
					<td style="text-align:center;width:25%;">비 고</td>
				</tr>
				<tr>
					<td style="text-align:center;height:25px;">년</td>
					<td style="text-align:center;">월</td>
					<td style="text-align:center;">일</td>
					<td style="text-align:center;">공란수</td>
					<td style="text-align:center;">백</td>
					<td style="text-align:center;">십</td>
					<td style="text-align:center;">억</td>
					<td style="text-align:center;">천</td>
					<td style="text-align:center;">백</td>
					<td style="text-align:center;">십</td>
					<td style="text-align:center;">만</td>
					<td style="text-align:center;">천</td>
					<td style="text-align:center;">백</td>
					<td style="text-align:center;">십</td>
					<td style="text-align:center;">일</td>
					<td></td>
				</tr>
				<tr>
					<td style="text-align:center;height:25px;">{{$model->M_YY}}</td>
					<td style="text-align:center;">{{ (int)$model->M_MM >= 10 ? $model->M_MM : "0".$model->M_MM }}</td>
					<td style="text-align:center;">{{ (int)$model->M_DD >= 10 ? $model->M_DD : "0".$model->M_DD}}</td>
					<td style="text-align:center;">{{$cntNull[$k]}}</td>
					@foreach($arrStrSum[$k] as $item)
					<td style="text-align:center;">{{$item}}</td>
					@endforeach
					<td style="text-align:left;padding-left:3px;"></td>
				</tr>
			</table>
			<table id="tblList3" summary="어종별 재고조회" width="100%" border="0.5" cellpadding="0" cellspacing="0" style="border-bottom:none;border-top:none;">
				<tr>
					<td style="text-align:center;width:20px;">월</td>
					<td style="text-align:center;width:20px;">일</td>
					<td style="text-align:center;width:45%;">품&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;목</td>
					<td style="text-align:center;">규&nbsp;&nbsp;격</td>
					<td style="text-align:center;">수&nbsp;&nbsp;량</td>
					<td style="text-align:center;">단&nbsp;&nbsp;가</td>
					<td style="text-align:center;">공급가액</td>
					<td style="text-align:center;">비&nbsp;&nbsp;고</td>
				</tr>
				
				<tr>
					<td style="text-align:center;">{{ (int)$model->MM >= 10 ? $model->MM : "0".$model->MM  }}</td>
					<td style="text-align:center;">{{$model->DD}}</td>
					<td style="text-align:left;padding-left:3px;">{{$model->GOODS}}</td>
					<td></td>
					<td style="text-align:center;">{{$model->QTY}}</td>
					<td style="text-align:right;padding-right:3px;">{{$model->UNCS}}</td>
					<td style="text-align:right;padding-right:3px;">{{ number_format($model->SUPPLY_AMT)}}</td>
					<td style="text-align:left;padding-left:3px;">{{$model->REMARK}}</td>
				</tr>
				
				@for ($x=0; $x < 3; $x++)
				<tr>
					<td style="text-align:center;"></td>
					<td style="text-align:center;"></td>
					<td style="text-align:center;"></td>
					<td></td>
					<td style="text-align:center;"></td>
					<td style="text-align:right;padding-right:3px;"></td>
					<td style="text-align:right;padding-right:3px;"></td>
					<td style="text-align:left;padding-left:3px;"></td>
				</tr>
				@endfor
			</table>
			<table id="tblList4" summary="어종별 재고조회" width="100%" border="0.5" cellpadding="0" cellspacing="0" style="border-top:none;">
				<tr>
					<td style="text-align:center;">합계금액</td>
					<td style="text-align:center;">현금</td>
					<td style="text-align:center;">수표</td>
					<td style="text-align:center;">어음</td>
					<td style="text-align:center;">외상미수금</td>
					<td rowspan="2" style="text-align:center;">이 금액을 {!! $model->CLAIM == "1" ? "청구" : "영수" !!} 함</td>
				</tr>
				<tr>
					<td style="text-align:right;padding-right:3px;height:50px;">{{number_format($model->SUPPLY_AMT)}}</td>
					<td style="text-align:right;padding-right:3px;"></td>
					<td style="text-align:right;padding-right:3px;"></td>
					<td style="text-align:right;padding-right:3px;"></td>
					<td style="text-align:right;padding-right:3px;"></td>
				</tr>
			</table>
			
			
			@if( $loop == 0 )
			<br/>
			<hr style="border-width:1px;border-style:dotted;" />
			@endif
			@endfor
			{{--*/ $k++ /*--}}
		</div>
		@endforeach
	</body>
</html>




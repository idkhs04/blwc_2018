<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>판매일보</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:12.5px; }
			thead{
				width:100%;position:fixed;
				height:109px;
			}
			table > tr > td { text-aglin:center; border-top:3px solid block;border-bottom:3px solid block;}
			td { height:20px;}
			table {
				
			}
 
			span.title { text-align:center;}
			td{padding-left:3px;}
		</style>
			
	</head>
	<body>
		<br/>
		<br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;">
			<tr>
				<td rowspan="2" style="text-align:center;font-size:25px;border-right:none;width:40%;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" ></td>
				<td style="text-align:center;border-right:none;border-left:none;border-bottom:none;font-size:14px;" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" > </td>
				<td rowspan="2" style="text-align:center;border-left:none;" ></td>
				<td style="text-align:center" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
			</tr>
			<tr>
				<td style="text-align:center;border-right:none;border-left:none;border-top:none;font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:center;padding-left:3px;"></td>
				<td colspan="2" style="padding-left:3px;"></td>
			</tr>
		</table>
		<br/>
		<table id="tblList1" summary="공급자 공급받는자 정보" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td rowspan="4" style="text-align:center;width:20px;"><br/><br/><br/><br/></td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;width:35px;height:20px;">{{$model->CORP_ETPR_NO}}</td>
				<td rowspan="4" style="text-align:center;width:20px;"><br/><br/><br/><br/></td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->ETPR_NO}}</td>
			</tr>
			<tr>
				<td style="text-align:center;height:20px;"></td>
				<td style="padding-left:3px;"> {{$model->CORP_NM}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:1px;width:100px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->CORP_RPST}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->FRNM}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;"> &nbsp;&nbsp;&nbsp;{{$model->RPST}}</td>
			</tr>
			<tr>
				<td style="text-align:center;height:20px;"></td>
				<td colspan="3" style="padding-left:3px;">{{$model->CORP_ADDR1}} {{$model->CORP_ADDR2}}</td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->ADDR1}} {{$model->ADDR2}}</td>
			</tr>
			<tr>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">{{$model->CORP_UPTE}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->CORP_UPJONG}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->UPTE}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;{{$model->UPJONG}}</td>
			</tr>
		</table>
		<table id="tblList2" summary="어종별 재고조회" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;">
			<tr>
				<td colspan="3" style="text-align:center;"></td>
				<td colspan="12" style="text-align:center;"></td>
				<td style="text-align:center;width:25%;"></td>
			</tr>
			<tr>
				<td style="text-align:center;height:15px;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td></td>
			</tr>
			<tr>
				<td style="text-align:center;height:25px;">{{$model->M_YY}}</td>
				<td style="text-align:center;">{{ (int)$model->M_MM >= 10 ? $model->M_MM : "0".$model->M_MM }}</td>
				<td style="text-align:center;">{{ (int)$model->M_DD >= 10 ? $model->M_DD : "0".$model->M_DD}}</td>
				<td style="text-align:center;">{{$cntNull}}</td>
				@foreach($arrStrSum as $item)
				<td style="text-align:center;">{{$item}}</td>
				@endforeach
				<td style="text-align:left;padding-left:3px;"></td>
			</tr>
		</table>
		<table id="tblList3" summary="품목 규격" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;border-top:none;">
			<tr>
				<td style="text-align:center;width:5px;height:25px;"></td>
				<td style="text-align:center;width:5px;"></td>
				<td style="text-align:center;width:45%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
			</tr>
			@foreach($mlist as $list)
			<tr>
				<td style="text-align:center;">{{ (int)$list->MM >= 10 ? $list->MM : "0".$list->MM  }}</td>
				<td style="text-align:left;">{{$list->DD}}</td>
				<td style="text-align:left;padding-left:3px;">{{$list->GOODS}}</td>
				<td></td>
				<td style="text-align:center;">{{$list->QTY}}</td>
				<td style="text-align:right;padding-right:3px;">{{$list->UNCS}}</td>
				<td style="text-align:right;padding-right:3px;">{{number_format($list->SUPPLY_AMT)}}</td>
				<td style="text-align:left;padding-left:3px;">{{$list->REMARK}}</td>
			</tr>
			@endforeach
			@for ($x=0; $x < (4-count($mlist)); $x++)
			<tr>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td></td>
				<td style="text-align:center;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:left;padding-left:3px;"></td>
			</tr>
			@endfor
			
		</table>
		<table id="tblList4" summary="합계/현급/수표/어음" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:none;">
			<tr>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td rowspan="2" style="text-align:right;">이 금액을 청구 함</td>
			</tr>
			<tr>
				<td style="text-align:center;padding-right:3px;height:50px;">{{number_format($model->SUPPLY_AMT)}}</td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
			</tr>
		</table>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;">
			<tr>
				<td rowspan="2" style="text-align:center;font-size:25px;border-right:none;width:40%;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" ></td>
				<td style="text-align:center;border-right:none;border-left:none;border-bottom:none;font-size:14px;" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td rowspan="2" style="text-align:center;border-right:none;border-left:none;font-size:25px;width:5px;" > </td>
				<td rowspan="2" style="text-align:center;border-left:none;" ></td>
				<td style="text-align:center" >&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
			</tr>
			<tr>
				<td style="text-align:center;border-right:none;border-left:none;border-top:none;font-size:14px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:center;padding-left:3px;"></td>
				<td colspan="2" style="padding-left:3px;"></td>
			</tr>
		</table>
		<br/><br/><br/><br/><br/>
		<br/>
		<table id="tblList1" summary="공급자 공급받는자 정보" width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td rowspan="4" style="text-align:center;width:20px;"><br/><br/><br/><br/></td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;width:35px;height:20px;">{{$model->CORP_ETPR_NO}}</td>
				<td rowspan="4" style="text-align:center;width:20px;"><br/><br/><br/><br/></td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->ETPR_NO}}</td>
			</tr>
			<tr>
				<td style="text-align:center;height:20px;"></td>
				<td style="padding-left:3px;"> {{$model->CORP_NM}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:1px;width:100px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->CORP_RPST}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->FRNM}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;"> &nbsp;&nbsp;&nbsp;{{$model->RPST}}</td>
			</tr>
			<tr>
				<td style="text-align:center;height:20px;"></td>
				<td colspan="3" style="padding-left:3px;">{{$model->CORP_ADDR1}} {{$model->CORP_ADDR2}}</td>
				<td style="text-align:center;"></td>
				<td colspan="3" style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->ADDR1}} {{$model->ADDR2}}</td>
			</tr>
			<tr>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">{{$model->CORP_UPTE}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->CORP_UPJONG}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$model->UPTE}}</td>
				<td style="text-align:center;"></td>
				<td style="padding-left:3px;">&nbsp;&nbsp;&nbsp;{{$model->UPJONG}}</td>
			</tr>
		</table>
		<table id="tblList2" summary="어종별 재고조회" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;">
			<tr>
				<td colspan="3" style="text-align:center;"></td>
				<td colspan="12" style="text-align:center;"></td>
				<td style="text-align:center;width:25%;"></td>
			</tr>
			<tr>
				<td style="text-align:center;height:25px;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td></td>
			</tr>
			<tr>
				<td style="text-align:center;height:15px;">{{$model->M_YY}}</td>
				<td style="text-align:center;">{{ (int)$model->M_MM >= 10 ? $model->M_MM : "0".$model->M_MM }}</td>
				<td style="text-align:left;">{{ (int)$model->M_DD >= 10 ? $model->M_DD : "0".$model->M_DD}}</td>
				<td style="text-align:center;">{{$cntNull}}</td>
				@foreach($arrStrSum as $item)
				<td style="text-align:center;">{{$item}}</td>
				@endforeach
				<td style="text-align:left;padding-left:3px;"></td>
			</tr>
		</table>
		<br/>
		<table id="tblList3" summary="어종별 재고조회" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:none;border-top:none;">
			<tr>
				<td style="text-align:center;width:5px;"></td>
				<td style="text-align:center;width:5px;"></td>
				<td style="text-align:center;width:45%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;">&nbsp;&nbsp;</td>
			</tr>
			@foreach($mlist as $list)
			<tr>
				<td style="text-align:center;">{{ (int)$list->MM >= 10 ? $list->MM : "0".$list->MM  }}</td>
				<td style="text-align:left;">{{$list->DD}}</td>
				<td style="text-align:left;padding-left:3px;">{{$list->GOODS}}</td>
				<td></td>
				<td style="text-align:center;">{{$list->QTY}}</td>
				<td style="text-align:right;padding-right:3px;">{{$list->UNCS}}</td>
				<td style="text-align:right;padding-right:3px;">{{number_format($list->SUPPLY_AMT)}}</td>
				<td style="text-align:left;padding-left:3px;">{{$list->REMARK}}</td>
			</tr>
			@endforeach
			@for ($x=0; $x < (4-count($mlist)); $x++)
			<tr>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td></td>
				<td style="text-align:center;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:left;padding-left:3px;"></td>
			</tr>
			@endfor
			
		</table>
		<table id="tblList4" summary="합계정보" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:none;">
			<tr>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td style="text-align:center;"></td>
				<td rowspan="2" style="text-align:right;">이 금액을 {!! $model->CLAIM == "1" ? "청구" : "영수" !!} 함</td>
			</tr>
			<tr>
				<td style="text-align:center;padding-right:3px;height:50px;">{{number_format($model->SUPPLY_AMT)}}</td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
				<td style="text-align:right;padding-right:3px;"></td>
			</tr>
		</table>

	</body>
</html>

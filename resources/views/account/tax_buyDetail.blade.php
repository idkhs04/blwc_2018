@extends('layouts.main')
@section('class','매입관리')
@section('title','매입세금계산서 등록')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	#modal_custCd label, #modal_custGrpEdit label{
		display:inline-block;
		width:24%;
		clear:both;
	}
	#modal_custCd .form-input-sm, #modal_custGrpEdit .form-input-sm{
		display:inline-block;
		width:10%;
		clear:both;
	}

	#modal_custCd .form-input-md, #modal_custGrpEdit .form-input-md{
		display:inline-block;
		width:24%;
		clear:both;
	}

	#modal_custCd .form-input-lg, #modal_custGrpEdit .form-input-lg{
		display:inline-block;
		width:74%;
		clear:both;
	}

	.SearchCustGrp , .SearchAreaCd, .SearchCustGrpEdit , .SearchAreaCdEdit{
		display:none;
	}

	#tableCustGrpList_filter, #tableAreaCdList_filter, #tableCustGrpEditList_filter, #tableAreaCdEditList_filter{
		display:none;
	}

	.ui-datepicker-calendar {
		display: none;
	 }

	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
	#tblDetail th {
		background-color:khaki;
		text-align:right;
	}

	.col-fixed-310{
		width:310px;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<table id="tblDetail" class="table table-bordered table-hover display nowrap">
				<tbody>
					<tr class='odd'>
						<th width="20%">업체명</th>
						<td width="30%">{{$model->FRNM}}</td>
						<th width="20%">전화번호</th>
						<td>{{$model->PHONE_NO}}</td>
					</tr>
					<tr class='even'>
						<th>대표명</th>
						<td>{{$model->RPST}}</td>
						<th>팩스</th>
						<td>{{$model->FAX}}</td>
					</tr>
					<tr class='odd'>
						<th>사업자등록번호</th>
						<td>{{$model->ETPR_NO}}</td>
						<th>주소</th>
						<td>{{$model->ADDR1}}</td>
					</tr>
					<tr class='even'>
						<th>주민번호</th>
						<td>{{$model->JUMIN_NO}}</td>
						<th>종목/업태</th>
						<td>{{$model->UPJONG}}{{$model->UPTE}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-fixed-310 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					<button type="button" class="btn btn-info" id="btnAddTax" ><i class="fa fa-plus-circle"></i> 추가</button>
					<button type="button" class="btn btn-success" id="btnUpdate" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDelete"><i class="fa fa-remove"></i> 삭제</button>
				</div>
			</div>
			<div class="col-fixed-310 col-xs-12">
				<div id="reportrange1" class="pull-right" style="cursor: pointer; padding: 4px 10px; border: 1px solid #ccc; width: 100%">
					<label>거래일</label> 
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
			
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">			
		<div class="box-body">
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="매입계산서 목록" width="100%">
				<caption>매입계산서</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th width="60px">거래일</th>
						<th width="60px">영수/청구구분</th>
						<th width="120px">품목</th>
						<th width="80px">공급가액</th>
						<th width="auto">비고</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		

		var start = moment().subtract(1, 'year').startOf('year');
		//var start = moment().startOf('years');
		var end = moment().endOf('years');
		//var end = moment().add('months', 1).date(1).subtract('days', 1)

		var month = moment().format("MM");
		var year = moment().format("YYYY");
		var day = moment().format("DD");
		
		function cbSetDate(start, end) {
			$('#reportrange1 span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 거래일 정보 날짜 선택
		$("#reportrange1").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			autoUpdateInput: false,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);

		$("input[name='txtMonth']").val( month );
		$("input[name='txtYear']").val(year);
		$("input[name='txtday']").val(day);

		$("#reportrange").val(year+"-"+month);
		$("#modal_insert_input_date").val(year+"-"+month+"-"+day);

		// 입고정보 : 입고일 
		$("#modal_insert_input_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				fromLabel: "시작일",
				toLabel: "종료일",
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, function(start, end){
			
			$("input[name='txtMonth']").val( start.format('MM'));
			$("input[name='txtYear']").val( start.format('YYYY'));
			$("input[name='txtday']").val( start.format('DD'));		
		});


		// 입고정보 : 검색
		$("#reportrange").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'yy-mm',
			prevText: '이전 달',
			nextText: '다음 달',
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNames: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			showMonthAfterYear: true,
			yearSuffix: '년',
			closeText : "확인",
			currentText: '이번달',
		}).focus(function() {
			var thisCalendar = $(this);
			$('.ui-datepicker-calendar').detach();
			$('.ui-datepicker-close').click(function() {
				month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				
				//day = $(".ui-datepicker-day :selected").val();
				thisCalendar.datepicker('setDate', new Date(year, month, 1));
				
				//console.log(day);
				month = parseInt(month) + 1
				
				if( month < 10){
					month = "0" + month;
				}
				$("input[name='txtMonth']").val( month);
				$("input[name='txtYear']").val(year);
				//$("input[name='txtday']").val(day);
				
			});
		});
		


		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			lengthChange: false,
			bInfo : false,
			ajax: {
				url: "/tax/tax/getTaxAccountList",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.year			= $("input[name='textYear']").val();
					d.month			= $("input[name='textMonth']").val();
					d.CUST_MK		= "{{ Request::segment(4)}}";

					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
					
				}
			},
			columns: [
				{
					data:   "CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center chk"
				},
				{ data: 'WRITE_DATE', name: 'WRITE_DATE' , className: "dt-body-center"},
				{
					data:   "RECPT_CLAIM_DIV",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data == "1" ) ischecked = "checked";
							else ischecked = "";
							return "<input type='checkbox' class='editor-active' value='" + data +"' " + ischecked + " readonly='readonly' disabled />영수";
						}
						return data;
					},
					className: "dt-body-center"
				},

				{ data: 'GOODS', name: 'GOODS' },
				{ 
					data: 'SUPPLY_AMT',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ data: 'REMARK', name: 'REMARK' , className: "dt-body-left" },
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});

		$("select[name='srtCondition'], input[name='start_date'], input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td.chk > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td.chk > input[type=checkbox]").prop("checked", true);
			}
		});

		$("#btnList").click(function(){ 
			localStorage["IS_BACK"]		= "Y";
			location.href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/indexBuy?page=1"; 
		});

		// 매입세금계산서 모달 클릭
		$("#btnAddTax").click(function(){ 
			$("input[name='txtMonth']").val( month );
			$("input[name='txtYear']").val(year);
			$("input[name='txtday']").val(day);

			$("#reportrange").val(year+"-"+month);
			$("#modal_insert_input_date").val(year+"-"+month+"-"+day);

			$('#modal_insert').modal({show:true}); 
		});

		// 계산서 자동 등록 불러오기
		$("#btnInsertTax").click(function(){
			
			$.getJSON('/tax/tax/getTaxBuyGeneration', {
					_token			: '{{ csrf_token() }}',
					INPUT_MONTH		: $("#modal_insert input[name='txtMonth']").val() ,
					INPUT_YEAR		: $("#modal_insert input[name='txtYear']").val() ,
					CUST_MK			:"{{ Request::segment(4)}}"
					
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					$("#modal_insert input[name='GOODS']").val(data.GOODS);
					$("#modal_insert input[name='AMT']").val(Number(data.SUM).format());
					var res = data.WRITE_DATE.split("-");

					$("#modal_insert_input_date").val(data.WRITE_DATE);

					$("#modal_insert input[name='txtYear']").val(res[0]);
					$("#modal_insert input[name='txtMonth']").val(res[1]);
					$("#modal_insert input[name='txtday']").val(res[2]);
					
					
					
				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_insert .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				});
		});
		
		// 매입계산서 등록
		$("#btnInsertBuyTax").click(function(){
			
			$.getJSON('/tax/tax/setTaxBuy', {
					_token			: '{{ csrf_token() }}',
					WRITE_DATE		: $("#modal_insert input[name='INPUT_DATE']").val() ,
					BUY_SALE_DIV	: "I", 
					GOODS			: $("#modal_insert input[name='GOODS']").val() ,
					SUPPLY_AMT		: removeCommas($("#modal_insert input[name='AMT']").val()) ,
					RECPT_CLAIM_DIV	: $("#modal_insert input[name='DIV']").is(":checked") ? 1 : 0 ,
					REMARK			: $("#modal_insert input[name='REMARK']").val() ,
					CUST_MK			:"{{ Request::segment(4)}}",
					MONTHS			: $("#modal_insert input[name='txtMonth']").val() ,
					DAYS			: $("#modal_insert input[name='txtday']").val(),
					
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if( data.result == "success"){
						$("#modal_insert" ).modal("hide");
						oTable.draw() ;
					}
					
				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_insert .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				});
		});

		// 매입계산서 수정 클릭
		$("#btnUpdate").click(function(){
			var arr_code = [];

			$(".content > div.alert").remove();

			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { arr_code.push($(this).val()); }
			});
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				$('#modal_update').modal({show:true}); 
				var data = oTable.row( $('#tblList tbody > tr > td.chk > input[type=checkbox]:checked').parents('tr') ).data();
				$.getJSON('/tax/tax/getTaxBuyItem', {
					_token			: '{{ csrf_token() }}',
					WRITE_DATE		: data.WRITE_DATE ,
					SEQ				: data.SEQ ,
					CUST_MK			: data.CUST_MK,
					BUY_SALE_DIV	: data.BUY_SALE_DIV,
					SRL_NO			: data.SRL_NO,
					
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));
					$("#modal_update_input_date").val(data.WRITE_DATE);

					if( data.RECPT_CLAIM_DIV == "1" ) {
						$("#modal_update_div").prop("checked", true);
					}else{
						$("#modal_update_div").prop("checked", false);
					}

					$("#modal_update_goods").val(data.GOODS);
					$("#modal_update_amt").val(Number(data.SUPPLY_AMT).format());
					$("#modal_update_tax").val(data.REMARK);
					$("#modal_update input[name='BUY_SALE_DIV']").val(data.BUY_SALE_DIV );

					$("#modal_update input[name='SEQ']").val(data.SEQ);
					$("#modal_update input[name='SRL_NO']").val(data.SRL_NO);
					
				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_update .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				});
			}
		});
		

		// 매입계산서 수정저장
		$("#btnUpdateSaleTax").click(function(){

			$.getJSON('/tax/tax/uptTaxSale', {
				_token			: '{{ csrf_token() }}',
				WRITE_DATE		: $("#modal_update_input_date").val() ,
				GOODS			: $("#modal_update_goods").val() ,
				SUPPLY_AMT		: removeCommas($("#modal_update_amt").val()) ,
				RECPT_CLAIM_DIV	: $("#modal_update_div").is(":checked") ? 1 : 0 ,
				REMARK			: $("#modal_update_tax").val() ,
				BUY_SALE_DIV	: $("#modal_update input[name='BUY_SALE_DIV']").val(),
				SEQ				: $("#modal_update input[name='SEQ']").val(),
				SRL_NO			: $("#modal_update input[name='SRL_NO']").val(),
				CUST_MK			: "{{ Request::segment(4)}}",
				
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				if( data.result == "success"){
					$("#modal_update" ).modal("hide");
					oTable.draw() ;
				}
				
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_update .edit_alert');
				info.hide().find('ul').empty();
					
				if( error.result=="DB_ERROR"){
					info.find('ul').append('<li>' + 'DB관련 에러..관리자에게 문의하세요!' + '</li>');
				
				}else{
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		//계산서 삭제
		$("#btnDelete").click(function(){
			var arr_code = [];

			//console.log($('#tblList tbody > tr > td.chk > input[type=checkbox]').length);
			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { arr_code.push($(this).val()); }
			});

			$(".content > div.alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목을 선택해주세요") ).show();
				return false;
			}
			
			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 항목은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				if(confirm('삭제 하시겠습니까?') ){

					var data = oTable.row( $('#tblList tbody > tr > td.chk > input[type=checkbox]:checked').parents('tr') ).data();

					$.getJSON('/tax/tax/getTaxSaleItem', {
						_token			: '{{ csrf_token() }}',
						WRITE_DATE		: data.WRITE_DATE ,
						SEQ				: data.SEQ ,
						CUST_MK			: data.CUST_MK,
						BUY_SALE_DIV	: data.BUY_SALE_DIV,
						SRL_NO			: data.SRL_NO,
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						
						$.getJSON('/tax/tax/deleteTaxSale', {
							_token			: '{{ csrf_token() }}',
							WRITE_DATE		: data.WRITE_DATE ,
							SEQ				: data.SEQ ,
							BUY_SALE_DIV	: data.BUY_SALE_DIV,
							SRL_NO			: data.SRL_NO,
							
						}).success(function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							//$(".main_alertMsg").html( getAlert('warning', '경고', "삭제") ).show();
							oTable.draw() ;
							
						}).error(function(xhr,status, response) {
							var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
							$(".main_alertMsg").html( getAlert('danger', '경고', "삭제 오류 관리자에게 문의하세요") ).show();
						});
						
					});
				}
			}
		});
		// 매출계산서 등록 닫기
		$('#modal_insert').on('hidden.bs.modal', setInitModalClose);

		function setInitModalClose(){
			
			$(".modal input").val("");
			$("#modal_insert_div").prop("checked", true);

		}
	});
</script>


<!-- 매입계산서 등록-->
<div class="modal fade" id="modal_insert" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 매입계산서 등록</h4>
				<button type="button" class="btn btn-info pull-right" id="btnInsertTax">계산서 자동 생성</button>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>


				<div class="form-group">					
					<label><i class='fa fa-circle-o text-red'></i> 연/월</label> 
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<input id="reportrange" type="text" readonly="readonly" />

					<input type='hidden' name="txtMonth" />
					<input type='hidden' name="txtYear" />
					<input type='hidden' name="txtday" />

				</div>

				<div class="form-group">
					<label for="FAX"><i class='fa fa-circle-o text-red'></i>  일자</label>
					<input type="text" class="form-control" id="modal_insert_input_date" placeholder="일자" name="INPUT_DATE" value="">
				</div>
				<div class="form-group">
					<label for="DIV"><i class='fa fa-circle-o text-aqua'></i>  영수구분</label>
					<input type="checkbox" class="" id="modal_insert_div" name="DIV" value="1" checked>
				</div>
				<div class="GOODS">
					<label for="ADDR1"><i class='fa fa-circle-o text-red'></i>  품목</label>
					<input type="text" class="form-control" id="modal_insert_goods" placeholder="품목" name="GOODS" value="" >
				</div>
				<div class="form-group">
					<label for="AMT"><i class='fa fa-circle-o text-red'></i>  공급가액</label>
					<input type="text" class="form-control numeric" id="modal_insert_amt" placeholder="공금가액" name="AMT" value="">
				</div>
				<div class="form-group">
					<label for="REMARK"><i class='fa fa-circle-o text-aqua'></i>  비고</label>
					<input type="text" class="form-control" id="modal_insert_tax" placeholder="비고" name="REMARK" value="">
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnInsertBuyTax">
					<span class="glyphicon glyphicon-off"></span> 등록
				</button>

				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>

<!-- 거래처 수정-->

<!-- 매입계산서 수정-->
<div class="modal fade" id="modal_update" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 매입계산서 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<input type='hidden' name="BUY_SALE_DIV" />
				<input type='hidden' name="SEQ" />
				<input type='hidden' name="SRL_NO" />

				<div class="form-group">
					<label for="modal_update_input_date"><i class='fa fa-circle-o text-red'></i>  거래일</label>
					<input type="text" class="form-control" id="modal_update_input_date" placeholder="일자" name="INPUT_DATE" value="">
				</div>
				<div class="form-group">
					<label for="modal_update_div"><i class='fa fa-circle-o text-aqua'></i> 영수구분</label>
					<input type="checkbox" class="" id="modal_update_div" name="DIV" value="1" >
				</div>
				<div class="GOODS">
					<label for="modal_update_goods"><i class='fa fa-circle-o text-red'></i>  품목</label>
					<input type="text" class="form-control" id="modal_update_goods" placeholder="품목" name="GOODS" value="" >
				</div>
				<div class="form-group">
					<label for="modal_update_amt"><i class='fa fa-circle-o text-red'></i>  공급가액</label>
					<input type="text" class="form-control numeric" id="modal_update_amt" placeholder="공금가액" name="AMT" value="">
				</div>
				<div class="form-group">
					<label for="modal_update_tax"><i class='fa fa-circle-o text-aqua'></i>  비고</label>
					<input type="text" class="form-control" id="modal_update_tax" placeholder="비고" name="REMARK" value="">
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success  pull-right" id="btnUpdateSaleTax">
					<span class="glyphicon glyphicon-off"></span> 수정저장
				</button>

				<button type="button" class="btn btn-danger  pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.main')
@section('class','계산서 관리')
@section('title','국세청 세금계산서 조회')
@section('content')
<link rel="stylesheet" type="text/css" href="/static/css/tax/tax.css" />
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-12 col-xs-12">
				<div class="btn-group">
					
					<button type="button" class="btn btn-success" id="btnUpdate" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-danger" id="btnDelete"><i class="fa fa-remove"></i> 삭제</button>
					<button type="button" class="btn btn-info padding" id="btnInsert" ><i class="fa fa-plus-circle"></i> 추가</button>
					
					@if ( $CORP_INFO->TAX_USE_YN == "Y")
					<button type="button" class="btn btn-success" id="btnSendTax"><i class="fa fa-send"></i> 송신</button>
					<button type="button" class="btn btn-success padding" id="btnReceiveTax"><i class="fa fa-reply"></i> 수신</button>
					@endif
					
					<button type="button" class="btn btn-success btnPdfAll"><i class="fa fa-file-pdf-o"></i> 출력</button>
					<button type="button" class="btn btn-success btnExcel print" id="btnExcelTax"><i class="fa fa-file-excel-o"></i> 국세청신고용 Excel</button>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == '' ? 1 : app('request')->input('page') }}" />
			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<label> 매출 <input type='radio' value="O" name="chkSaleStock" checked="checked" /></label>&nbsp;&nbsp;&nbsp;
					<label> 매입 <input type='radio' value="I" name="chkSaleStock" /></label>
				</div>
			</div>
			<div class="col-md-3 col-xs-6">
				<div id="reportrange" class="input-group" style="background: #fff; cursor: pointer; padding: 1.5px 10px;border:1px solid #ccc;display:inline-block;">
					<label> 작성일 </label> &nbsp;&nbsp;
					<span></span>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>

			<div class="col-md-2 col-xs-6 ">
				<select class="form-control" name="CUST_GRP" >
					<option value="ALL">거래처그룹</option>
					@foreach ( $CUST_GRP_INFO as $custGrp)
					<option value="{{$custGrp->CUST_GRP_CD}}">{{$custGrp->CUST_GRP_NM}}</option>
					@endforeach
				</select>
			</div>
			
			<div class="col-md-2 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="거래처 검색" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			@if ( $CORP_INFO->TAX_USE_YN == "Y")

			<div class="col-md-12 col-xs-12 ">
				<div class="input-group">
					<label>전송상태 </label>&nbsp;&nbsp;&nbsp;			
					<input type='radio' name='official_status' value="all" checked="checked" /> 전체
					<input type='radio' name='official_status' value="0" /> 작성중
					<input type='radio' name='official_status' value="1" /> 작성완료
					
					
					<input type='radio' name='official_status' value="2" /> 접수중
					<input type='radio' name='official_status' value="3" /> 접수중에러
					<input type='radio' name='official_status' value="4" /> 접수완료
					<input type='radio' name='official_status' value="5" /> 접수실패
					
					
				</div>
			</div>

			@endif
			
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="국세청 세금계산서 목록" width="100%">
				<caption>국세청 세금계산서 목록</caption>
				<thead>
					<tr>
						<th class="check" width="15px"><input type="checkbox" name="ALL" value='0' id='chkALL'/> </th>
						<th width="35px">구분</th>
						<th width="60px">작성일</th>
						<th width="60px">영수구분</th>
						<th width="180px">상호</th>
						
						<th width="60px">사업자번호</th>
						<th width="30px">대표자</th>
						<th width="60px">이메일</th>
						
						<th width="100px">공급가액</th>
						@if ( $CORP_INFO->TAX_USE_YN == "Y")
						<th width="35px">상태</th>
						<th width="60px">주소</th>
						<th width="80px">승인번호</th>
						<th width="60px">관리</th>
						@endif 
						
						<th width="auto"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th class="dt-body-right">합 계 : </th>
						<th class="dt-body-right"></th>
						@if ( $CORP_INFO->TAX_USE_YN == "Y")
						
						<th class="dt-body-right"></th>
						
						
						<th></th>
						<th></th>
						<th></th>
						
						
						@endif 
						<th></th>
						
					</tr>
				</tfoot>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	var oTable;
	var chkOnceOpen = 1;
	var arrErrMessage = [];
	$(function () {
			
		var start = moment().subtract(11, 'days');
		var end = moment();
		var today = end.format('YYYY-MM-DD');
		
		var month = moment().format("MM");
		var year = moment().format("YYYY");
		var day = moment().format("DD");

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}
		
		// 거래일 : 입고일 
		$("#modal_insert_input_date").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});

		$("input[name='txtMonth']").val( start.format('MM'));
		$("input[name='txtYear']").val( start.format('YYYY'));
		$("input[name='txtday']").val( start.format('DD'));

		$("#reportrange1").val(year+"-"+month);
		$("#modal_insert_input_date").val(year+"-"+month+"-"+day);


		// 거래일. : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			opens: "left",
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",
			
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				applyLabel: "확인",
				cancelLabel: "취소",
			}
		}, cbSetDate);
		cbSetDate(start, end);
		
		var nError		 = 0;
		var isBizChecked = false
		var nCount		 = 0;

		oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 15개 조회 설정
			bLengthChange: false,
			bInfo:true,
			bFilter: false,
			ajax: {
				url: "/tax/tax/elistData",
				data:function(d){
					d.chkSaleStock	= $("input[name='chkSaleStock']:checked").val();
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.status		= "all"; //$("input[name='official_status']:checked").val();
					d.cust_mk		= $("input[name='CustCd']").val();
					d.cust_grp		= $("select[name='CUST_GRP'] option:selected").val();
					d.start_date	= $("input[name='start_date']").val();
					d.end_date		= $("input[name='end_date']").val();
				}
			},
			fnRowCallback: function(nRow, nData){
				nCount++;
				
				// 공급하는자 사업자 등록번호 체크
				// 한번만 체크한다
				if( (nData.CORP_ETPR_NO == null || nData.CORP_ETPR_NO.length != 10) && ( isBizChecked == false ) ){
					arrErrMessage[nError++] = "공급자의 사업자 번호가 없습니다. 회사관리에서 사업자번호를 입력하세요";
					isBizChecked = true;
				}

				// 공급받는자 사업자 등록번호 체크
				if( nData.ETPR_NO == null || nData.ETPR_NO.length != 12){
					arrErrMessage[nError++] = nData.FRNM + "의 사업자 번호가 올바르지 않습니다";
					$(nRow).addClass("error_row");
				}
				// 공급받는자 대표자명
				if( nData.RPST == null || nData.RPST == ""){
					arrErrMessage[nError++] = nData.FRNM + "의 대표자명이 올바르지 않습니다";
					$(nRow).addClass("error_row");
				}
			},
			fnDrawCallback: function () {
				nCount		= 0;
				
				if( arrErrMessage.length > 0){

					var objUl = "<ul>";
					for(var i=0; i< arrErrMessage.length; i++){
						objUl += "<li>" + arrErrMessage[i] + "</li>";
					}
					objUl += "<ul>";
				
				
					$(".content > .alert").remove();
					$(".content").prepend( getAlert('warning', '경고', objUl ) ).show();
				}

				isBizChecked = false;
				arrErrMessage = [];
				nError	= 0;
			},
			order: [[ 2, 'DESC'], [ 4, 'ASC']],
			columns: [
				{
					data: "SEQ",
					name: 'TAX_M.SEQ',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' data-cust-mk = '" + row.CUST_MK + "' data-seq='"+ row.SEQ +"' data-wt='" +row.WRITE_DATE+ "' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					orderable:      false,
					className: "dt-body-center chk"
				},
				{ data: 'SALE_DIV_NM', name: 'TAX_M.BUY_SALE_DIV', className: "dt-body-center" },
				{ data: 'WRITE_DATE', name: 'TAX_M.WRITE_DATE', className: "dt-body-center" },
				{ data: 'RECPT_CLAIM_DIV_NM', name: 'TAX_M.RECPT_CLAIM_DIV', className: "dt-body-center" },
				//{ data: 'FRNM', name: 'CUST.FRNM' },
				{
					name: 'CUST.FRNM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return	row.FRNM + "  <button type='button' class='btn btn-success btnUpdate'><i class='fa fa-edit'></i></button>" ;
						}
					},
					
					className: "dt-body-left"
				},

				{ data: 'ETPR_NO', name: 'CUST.ETPR_NO' },
				{ data: 'RPST', name: 'CUST.RPST' },
				{ data: 'EMAIL', name: 'CUST.EMAIL' },
				{ 
					data: 'SUPPLY_AMT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				@if ( $CORP_INFO->TAX_USE_YN == "Y")

				{
					data:   "TAX_SEND_STATUS",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							if( data == null || data == "1"){
								return "작성완료" ;
							}else if( data === "2"){
								return "<span class='green'>접수중</span>" ;
							}else if( data === "3"){
								return "<span class='red'>접수중에러</span>" ;
							}else if( data === "4"){
								return "<span class='blue'>접수완료</span>" ;
							}else{
								return "<span class='red'>접수실패</span>" ;
							}
						}
						return data;
					},
					className: "dt-body-cetner",
				},
				
				
				{ data: 'TAX_SEND_DATE', name: 'TAX_SEND_DATE', className: "dt-body-center" },
				{ data: 'TAX_ISSUE_NUMBER', name: 'TAX_ISSUE_NUMBER', className: "dt-body-center" },
				{
					data: "TAX_ISSUE_NUMBER",
					name: 'TAX_M.TAX_ISSUE_NUMBER',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							// 승인된 경우 정정 할수 있도록 처리
							//console.log(row.TAX_ISSUE_NUMBER);
							if( row.TAX_ISSUE_NUMBER != "" && row.TAX_ISSUE_NUMBER != null && row.TAX_SEND_STATUS == "4"){
								return "<button type='button' class='btn bg-orange-active color-palette btnUptTax'>정정/취소/폐기</button>";
							}
						}
						
					},
					orderable:      false,
					className: "dt-body-center chk"
				},
				@endif
				
				{
					"className":      'dt-body-center',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
				
				
			],
			footerCallback: function ( row, data, start, end, display ) {
				
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 합계


				tax_sum= api.column( 8 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 8 ).footer() ).html(Number(tax_sum.toFixed(2)).format());

				$( api.column( 9 ).footer() ).html("총 : " + Number(nCount.toFixed(2)).format()  + " 건");
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.draw() ;
		});

		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		$("input[name='official_status'], #search-btn").on("click", function(){
			oTable.draw() ;
		});

		$("input[name='chkSaleStock']").on("change", function(){
			// 매입일때
			if( $(this).val() == "I" ){
				$(".btn.print").css("display", "none");
			}else{
				$(".btn.print").css("display", "block");
			}
			oTable.draw() ;
		});

		$("input[name='start_date']").on("change", function(){
			oTable.draw() ;
		});

		$("input[name='end_date']").on("change", function(){
			oTable.draw() ;
		});

		$("select[name='CUST_GRP']").on("change", function(){
			oTable.draw() ;
		});

		// 전체 체크 
		$("#chkALL").on("click", function(){
			if( $(this).is(":checked")){
				$("#tblList tbody td > input[type=checkbox]").prop("checked", true);
			}else{
				$("#tblList tbody td > input[type=checkbox]").prop("checked", false);
			}
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		

		// 송신
		$("#btnSendTax").click(function(){
			var arr_code = [];
			var cntError = 0;
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				var row		= oTable.row( $(this).parents('tr') ).data();

				if ($(this).is(":checked")) { 
					arr_code.push($(this).val()); 
						
					if( row.TAX_SEND_STATUS !=  "1" ){
						if( row.TAX_SEND_STATUS != null ){
							$(".content > .alert").remove();
							$(".content").prepend( getAlert('warning', '경고', row.FRNM + " 은 이미 송신한 문서입니다.") ).show();
							//cntError++;
						}
					}
				}
			});

			// 에러체크
			if( cntError > 0){
				return false;
			}

			$(".content > .alert").remove();

			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "송신할 항목을 1개 이상 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length >= 1){
				if(confirm('선택된 전자세금계서를 송신 하시겠습니까?') ){
					var data = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();

					$.getJSON('/tax/tax/eTaxSend', {
						_token			: '{{ csrf_token() }}',
						WRITE_DATE		: data.WRITE_DATE ,
						SEQ				: data.SEQ ,
						CUST_MK			: data.CUST_MK,
						BUY_SALE_DIV	: data.BUY_SALE_DIV,
						SRL_NO			: data.SRL_NO, 
						
					}).success(function(xhr){
						
						$(".content > .alert").remove();
						$(".content").prepend( getAlert('success', '확인', "송신완료되었습니다.") ).show();
						oTable.draw();
						return ;
					}).error(function(xhr,status, response) {
						
						$(".content > .alert").remove();
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = "<ul>";
							
						if( error.type == "infonull"){
							
							for(var i=0; i< error.message.length; i++){
								for(var k in error.message[i]){
									
									info += '<li>' + error.message[i][k] + '</li>';
								}
							}
							
						}else{
						
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}

						info += "</ul>";
						$(".content").prepend( getAlert('warning', '경고', info) ).show();
						//oTable.draw();
					});
				}
			}
		});

		// 수신버튼 클릭
		$("#btnReceiveTax").click(function(){
			
			var data = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();

			$.getJSON('/tax/tax/eTaxReceive', {
				_token			: '{{ csrf_token() }}',
				BUY_SALE_DIV	: $("input[name='chkSaleStock']:checked").val()

			}).success(function(xhr){
				oTable.draw();
			}).error(function(xhr,status, response) {
				
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
				

				$(".content > .alert").remove();
				$(".content").prepend( getAlert('warning', '경고', error.message ) ).show();
			
			});
		});

		// 수정버튼 클릭
		$("#btnUpdate").click(function(){
			
			$("#modal_eTax #mode").val("upd");
			//$("#btnInsertTax").css("display", "none");
			var arr_code = [];

			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { arr_code.push($(this).val()); }
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 계산서를 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정할 계산서는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				
				$('#modal_eTax').modal({show:true}); 
				var data = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();
				$.getJSON('/tax/tax/getTaxSaleItem', {
					_token			: '{{ csrf_token() }}',
					WRITE_DATE		: data.WRITE_DATE ,
					SEQ				: data.SEQ ,
					CUST_MK			: data.CUST_MK,
					BUY_SALE_DIV	: data.BUY_SALE_DIV,
					SRL_NO			: data.SRL_NO,
					
				}).success(function(xhr){
					var data1 = jQuery.parseJSON(JSON.stringify(xhr));
					getBindEdit(data1);
					
				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_eTax .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				});
			}
		});

		// 삭제버튼 클릭
		$("#btnDelete").click(function(){
			var arr_code = [];
			var arr_stts = [];
			var isDelete = true;

			//console.log($('#tblList tbody > tr > td.chk > input[type=checkbox]').length);
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { 
					arr_code.push($(this).val()); 
					
					var srow = oTable.row( $(this).parents('tr') ).data();
					arr_stts.push(srow.TAX_SEND_STATUS);
				}
			});


			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 계산서를 선택해주세요") ).show();
				return false;
			}
			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "삭제할 계산서는 한개만 선택해주세요") ).show();
				return false;
			}
			
			$(arr_stts).each( function(i, val){
				if( val == "2" || val == "4"){
					$(".content > .alert").remove();
					$(".content").prepend( getAlert('warning', '경고', "접수중문서, 접수완료 문서는 삭제 할 수 없습니다") ).show();
					isDelete = false;
					return false;
				}
			});

			if( arr_code.length == 1 && isDelete == true){
				if(confirm('삭제 하시겠습니까?') ){
					var data = oTable.row( $('#tblList tbody > tr > td > input[type=checkbox]:checked').parents('tr') ).data();
					$.getJSON('/tax/tax/getTaxSaleItem', {
						_token			: '{{ csrf_token() }}',
						WRITE_DATE		: data.WRITE_DATE ,
						SEQ				: data.SEQ ,
						CUST_MK			: data.CUST_MK,
						BUY_SALE_DIV	: data.BUY_SALE_DIV,
						SRL_NO			: data.SRL_NO,
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));
						
						$.getJSON('/tax/tax/deleteTaxSale', {
							_token			: '{{ csrf_token() }}',
							WRITE_DATE		: data.WRITE_DATE ,
							SEQ				: data.SEQ ,
							BUY_SALE_DIV	: data.BUY_SALE_DIV,
							SRL_NO			: data.SRL_NO,
							
						}).success(function(xhr){
							var data = jQuery.parseJSON(JSON.stringify(xhr));
							//$(".main_alertMsg").html( getAlert('warning', '경고', "삭제") ).show();
							oTable.draw() ;
							
						}).error(function(xhr,status, response) {
							var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
							$(".main_alertMsg").html( getAlert('danger', '경고', "삭제 오류 관리자에게 문의하세요") ).show();
						});
						
					});
				}
			}
		});

		
		
		var dTable ;
		// 세금계산서 등록 닫기
		$('#modal_eTax').on('hidden.bs.modal', CbSetInitModalClose);
		
		// 계산서 등록 버튼  팝업 
		$("#btnInsert").click(function(){
			
			
			$("#modal_eTax #mode").val("ins");

			$("#btnInsertTax").css("display", "block");

			$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", false);
			// 매입/매출 디폴트 
			$("#modal_eTax input[name='BUY_SALE_DIV'][value='"+ $("input[name='chkSaleStock']:checked").val() +"']").prop('checked', true);

			$("#btnSearchCust").prop("disabled", false);
			$("#modal_eTax input[name='MODAL_WRITE_DATE']").prop("disabled", false);
			$("#modal_eTax").modal({
				backdrop: 'static',
				keyboard: false,
			});
		});
		
		// 세금계산서 통합 저장
		$("#btnSaveTax").click(function(){
			
			var url		= '/tax/tax/setTaxIntergration';
			var mode	= $("#modal_eTax #mode").val();
			if( mode == "upd"){
				url = '/tax/tax/updTaxIntergration';
			}
			$.getJSON( url, {
				_token			: '{{ csrf_token() }}',
				BUY_SALE_DIV	: $("#modal_eTax input[name='BUY_SALE_DIV']:checked").val(),
				CUST_MK			: $("#modal_eTax input[name='CUST_MK']").val(),
				WRITE_DATE		: $("#modal_eTax input[name='MODAL_WRITE_DATE']").val() ,
				RECPT_CLAIM_DIV	: $("#modal_eTax input[name='RECPT_CLAIM_DIV']:checked").val(),
				GOODS			: $("#modal_eTax input[name='GOODS']").val() ,
				YEAR			: $("#modal_eTax input[name='txtYear']").val() ,
				MONTH			: $("#modal_eTax input[name='txtMonth']").val() ,
				DAY				: $("#modal_eTax input[name='txtday']").val() ,
				SUPPLY_AMT		: removeCommas($("#modal_eTax input[name='SUM_AMT']").val()) ,
				REMARK			: $("#modal_eTax input[name='REMARK']").val() ,
				TAX_USE_YN		: "{{$CORP_INFO->TAX_USE_YN}}",
				// 전자세금계산서
				TAX_TYPE		: $("#modal_eTax select[name='TAX_ACCOUNT_TYPE'] option:selected").val() ,
				BILLING_TYPE	: $("#modal_eTax select[name='TAX_BILLING_TYPE'] option:selected").val() ,
				TAX_PART		: $("#modal_eTax select[name='TAX_ACCOUNT_PART'] option:selected").val() ,
				//TAX_ISSUE_NUMBER: $("#modal_eTax input[name='TAX_ISSUE_NUMBER']").val() ,
				MODIFY_CODE		: $("#modal_eTax select[name='MODIFY_CODE'] option:selected").val() ,
				SRL_NO			: $("#modal_eTax input[name='SRL_NO']").val() ,
				SEQ				: $("#modal_eTax input[name='SEQ']").val() ,
				TAX_REPORT_DIVISION	: $("#modal_eTax select[name='TAX_REPORT_DIVISION'] option:selected").val(),
				TAX_ISSUE_DIVISION : $("#modal_eTax select[name='TAX_ISSUE_DIVISION'] option:selected").val(),
				

			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				if( data.result == "success"){
					$("#modal_eTax" ).modal("hide");
					oTable.draw() ;
				}

			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_eTax .edit_alert');
				info.hide().find('ul').empty();

				if( error.result=="DB_ERROR"){
					info.find('ul').append('<li>' + 'DB관련 에러..관리자에게 문의하세요!' + '</li>');

				}else{
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});

		$(".btnPdfAll").click(function(){
			
			var width=740;
			var height=720;
			
			var START_DATE	= $("input[name='start_date']").val();
			var END_DATE	= $("input[name='end_date']").val();
			var CUST_GRP	= $("select[name='CUST_GRP'] option:selected").val();
			var chkSaleStock= $("input[name='chkSaleStock']:checked").val();
			var status		= "all"; //$("input[name='official_status']:checked").val();
			var textSearch	=  $("input[name='textSearch']").val();

			var url = "/tax/tax/getPdfeTax/" + START_DATE + "/" + END_DATE + "/" + CUST_GRP + "/" + chkSaleStock + "/" + status + "/" + textSearch;
			getPopUp(url , width, height);
		});
		

		// Pdf 출력
		$(".btnPdf").click(function(){

			var arr_code = [];

			var row = oTable.row( $(this).parents('tr') ).data();

			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { arr_code.push($(this).val()); }
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "출력할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "출력은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				var data = oTable.row( $("#tblList tbody > tr > td.chk > input[type=checkbox]:checked").parents('tr') ).data();
				
				var width=740;
				var height=720;
				
				var WRITE_DATE	= data.WRITE_DATE;
				var SEQ			= data.SEQ;
				var CUST_MK		= data.CUST_MK;
				var WHITE		= "";
	
				// 백지출력
				if( $(this).hasClass("white")){
					WHITE	= 1;
				}else{
					WHITE	= 0;
				}
				var url = "/tax/tax/getPdfSaleTax/" + WRITE_DATE + "/" + SEQ + "/" + CUST_MK + "/" + WHITE ;
				getPopUp(url , width, height);
			}
		});
		// 엑셀 출력
		$("#btnExcel").click(function(){

			var arr_code = [];
			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { 
					arr_code.push($(this).val()); 
				}
			});

			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "출력할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "출력은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){

				var data = oTable.row( $("#tblList tbody > tr > td.chk > input[type=checkbox]:checked").parents('tr') ).data();
				
				var width=740;
				var height=720;
				
				var WRITE_DATE	= data.WRITE_DATE;
				var SEQ			= data.SEQ;
				var CUST_MK		= data.CUST_MK;

				var url = "/tax/tax/ExcelTaxBill/" + WRITE_DATE + "/" + SEQ + "/" + CUST_MK ;
				getPopUp(url , width, height);

			}

		});

		// 계산서 팝업 닫기
		function CbSetInitModalClose(){
			$(".edit_alert").css("display", "none");

			$("#modal_eTax .slideSearchCust" ).slideUp();
			$("#modal_eTax input[type='text']").val('');
			
			$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", false) ;
			$("#modal_eTax input[name='BUY_SALE_DIV']:eq(0)").prop("checked", true);
			$("#modal_eTax input[name='RECPT_CLAIM_DIV']:eq(0)").prop("checked", true);

			$("#modal_eTax select > option:nth-child(1)").prop("selected", true);

			$("#modal_eTax input[name='TAX_ISSUE_NUMBER']").prop("disabled", true);
			$("#modal_eTax select[name='MODIFY_CODE']").prop("disabled", true);

			$("#WRITE_DATE").val(moment().format("YYYY-MM-DD"));

			var year	= moment().format("YYYY");
			var month	= moment().format("MM");
			$("#TAX_YM").val(year+"-"+month);
			
			// 계산서 종류 : 계산서
			$("#modal_eTax select[name='TAX_ACCOUNT_TYPE'] option[value='03']").prop("selected", true);
			// 계산서 분류 : 영세율
			$("#modal_eTax select[name='TAX_ACCOUNT_PART'] option[value='02']").prop("selected", true);
			// 결제방법 : 현금
			$("#modal_eTax select[name='TAX_BILLING_TYPE'] option[value='10']").prop("selected", true);

			$("#btnInsertTax").css("display", "block");
		}
	});
</script>

<div class="modal fade" id="modal_eTax" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:5px 15px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 계산서 작성</h4>
				
				<label for="modal_insert_div"><i class='fa fa-check-circle text-aqua'></i> 현금만 계산</label>
				<input type="checkbox" class="" id="modal_insert_only_cash" name="ONLY_CASH" value="1" />
				<button type="button" class="btn btn-info pull-right" id="btnInsertTax">계산서 자동 생성</button>

			</div>
			<div class="box-body">
				<div class="edit_alert" style="padding:20px"><ul></ul></div>
				<input type="hidden" id="mode" name="mode" value="" />
				<input type="hidden" id="SEQ" name="SEQ" value="" />
				<input type="hidden" id="SRL_NO" name="SRL_NO" value="" />
				<div class="col-md-6">
					<div class="form-group">
						<label for="BUY_SALE_DIV"><i class='fa fa-check-circle text-red'></i> 구분</label>
						<input type="radio" value="O" name="BUY_SALE_DIV" checked="checked" >매출
						<input type="radio" value="I" name="BUY_SALE_DIV" >매입
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="CUST_FRNM"><i class='fa fa-check-circle text-red'></i> 거래처</label>
						<input type="hidden" id="CUST_MK" name="CUST_MK" />
						<input type="text" class="form-control" id="CUST_FRNM" name="CUST_FRNM" placeholder="거래처" readonly="readonly" />
						<button type="button" name="searchCust" id="btnSearchCust" class="btn btn-info">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group slideSearchCust">
						<div class="box box-primary">
							<h5><span class="glyphicon glyphicon-th-large"></span> 거래처 선택</h5>

							<label for="strSearchCust"><i class='fa fa-circle-o text-aqua'></i> 거래처 상호</label>
							<input type="text" class="form-control" name="strSearchCust" id="strSearchCust" />

							<button type="button" class="btn btn-danger  pull-right btn_CustClose">
								<span class="glyphicon glyphicon-remove"></span> 닫기
							</button>
							<div class="box-body">
								<table id="tblCustList" class="table table-bordered table-hover" summary="거래처 목록">
									<caption>거래처 조회</caption>
									<thead>
										<tr>
											<th width="30px">코드</th>
											<th width="120px">상호</th>
											<th width="70px">대표자</th>
											<th width="100px">전화번호</th>
											<th width="auto">선택</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="TAX_YM"><i class='fa fa-check-circle text-red'></i> 년/월</label>
						<input type="text" class="form-control" id="TAX_YM" name="TAX_YM" />
						<input type='hidden' name="txtMonth" />
						<input type='hidden' name="txtYear" />
						<input type='hidden' name="txtday" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="WRITE_DATE"><i class='fa fa-check-circle text-red'></i> 작성일자</label>
						<input type="text" class="form-control" id="WRITE_DATE" name="MODAL_WRITE_DATE" />
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="RECPT_CLAIM_DIV"><i class='fa fa-check-circle text-red'></i> 분류</label>
					<input type="radio" value="0" name="RECPT_CLAIM_DIV" checked="checked" />청구
					<input type="radio" value="1" name="RECPT_CLAIM_DIV" />영수
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="GOODS"><i class='fa fa-check-circle text-red'></i> 품목</label>
					<input type="text" class="form-control" id="GOODS" name="GOODS" placeholder="품목"  />
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="SUM_AMT"><i class='fa fa-check-circle text-red'></i> 공급가액</label>
					<input type="text" class="form-control numeric" id="SUM_AMT" name="SUM_AMT" placeholder="공급가액" />
					
				</div>
			</div>
			
			<div class="box-body">
				@if ( $CORP_INFO->TAX_USE_YN == "Y")
				<div class="col-md-6">
					<label for="TAX_ACCOUNT_TYPE"><i class='fa fa-check-circle text-red'></i> 계산서 종류</label>
					<select name="TAX_ACCOUNT_TYPE" class="form-control">
						<option value="01" selected='selected'>세금계산서</option>
						<option value="02">수정세금계산서</option>
						<option value="03">계산서</option>
						<option value="04">수정계산서</option>
					</select>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="TAX_BILLING_TYPE"><i class='fa fa-check-circle text-red'></i> 결제방법</label>
						<select class="form-control" id="TAX_BILLING_TYPE" name="TAX_BILLING_TYPE" >
							<option>선택</option>
							<option value="10" selected='selected'>현금</option>
							<option value="20">수표</option>
							<option value="30">어음</option>
							<option value="40">외상</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<label for="TAX_REPORT_DIVISION"><i class='fa fa-check-circle text-blue'></i> 신고구분</label>
						
						<select name="TAX_REPORT_DIVISION" id="TAX_REPORT_DIVISION" class="form-control">
							<option value="DD">즉시신고</option>
							<option value="CD">확인후신고(공급받는자 승인후 신고)</option>
							<option value="DE">익일신고(발급 익일 신고)</option>
							<option value="DM">이메일 전송</option>
							<option value="PC">결제용 취소</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<label for="TAX_ISSUE_DIVISION"><i class='fa fa-check-circle text-blue'></i> 수정구분</label>
						
						<select name="TAX_ISSUE_DIVISION" id="TAX_ISSUE_DIVISION" class="form-control">
							<option value="05">신규</option>
							<option value="06">정정</option>
							<option value="07">취소</option>
							<option value="08">폐기</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">		
					<div class="form-group">
						<label for="TAX_ACCOUNT_PART"><i class='fa fa-check-circle text-red'></i> 계산서분류</label>
						<select name="TAX_ACCOUNT_PART" class="form-control">
							<option value="01">일반</option>
							<option value="02" selected='selected'>영세율</option>
							<option value="03">위수탁</option>
							<option value="04">수입</option>
							<option value="05">영세율위수탁</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<label for="TAX_ACKNOWLEDGMENT_NUMBER"><i class='fa fa-check-circle text-red'></i> 기승인번호</label>
						<input type="text" class="form-control" id="TAX_ACKNOWLEDGMENT_NUMBER" name="TAX_ACKNOWLEDGMENT_NUMBER" placeholder="기승인번호" disabled="disabled" />
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<label for="MODIFY_CODE"><i class='fa fa-check-circle text-blue'></i> 수정코드</label>
						
						<select name="MODIFY_CODE" class="form-control" disabled="disabled">
							<option value="01">기재사항 착오,정정</option>
							<option value="02">공급가액 변동</option>
							<option value="03">환입</option>
							<option value="04">계약의 해제</option>
							<option value="05">내국신용장 사후개설</option>
							<option value="06">착오에 의한 이중발급</option>
						</select>
					</div>
				</div>
				
				@endif
				<div class="col-md-12">	
					<div class="form-group">
						<label for="REMARK"><i class='fa fa-check-circle text-blue'></i> 비고</label>
						<input type="text" class="form-control" id="REMARK" name="REMARK" placeholder="비고" style="width:431px;" />
					</div>
				</div>
			</div>
		
			<div class="box-footer">
				<button type="button" class="btn btn-success pull-right" id="btnSaveTax">
					<span class="glyphicon glyphicon-off"></span> 저장
				</button>

				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
			</div>
			
		</div>
	</div>
</div>


<script type="text/javascript">

	// 계산서 수정시 해당계산서를 불러와서 바인딩하는 함수
	// param : data :: 계산서 정보
	function getBindEdit(data){
			// 구분
			$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", true);
			$("#modal_eTax input[name='BUY_SALE_DIV'][value='" + data.BUY_SALE_DIV + "']").prop("checked", true)

			// 거래처명
			$("#modal_eTax input[name='CUST_FRNM']").val(data.FRNM);
			// 거래처코드
			$("#modal_eTax input[name='CUST_MK']").val(data.CUST_MK);
			$("#btnSearchCust").prop("disabled", true);

			// 작성일자
			$("#modal_eTax input[name='MODAL_WRITE_DATE']").val(data.WRITE_DATE);
			$("#modal_eTax input[name='MODAL_WRITE_DATE']").prop("disabled", true);
			// 청구/영수
			$("#modal_eTax input[name='RECPT_CLAIM_DIV'][value='" + data.RECPT_CLAIM_DIV + "']").prop("checked", true);
			// 품목
			$("#modal_eTax input[name='GOODS']").val(data.GOODS);
			// 공급가액
			$("#modal_eTax input[name='SUM_AMT']").val(Number(data.SUPPLY_AMT).format());
			// 년/월
			$("#modal_eTax input[name='TAX_YM']").val(moment(data.WRITE_DATE).format("YYYY-MM"));
			$("#modal_eTax input[name='txtYear']").val(moment(data.WRITE_DATE).format("YYYY"));
			$("#modal_eTax input[name='txtMonth']").val(data.MONTHS);
			$("#modal_eTax input[name='txtday']").val(data.DAYS);
			// 계산서 종류
			$("#modal_eTax select[name='TAX_ACCOUNT_TYPE'] option[value='"+data.TAX_ACCOUNT_TYPE+"']").prop('selected', true);
				
			if( data.TAX_ACCOUNT_TYPE == "02" ||  data.TAX_ACCOUNT_TYPE == "04"){
				$("#TAX_ISSUE_NUMBER, select[name='MODIFY_CODE']").prop("disabled", false);
			}
			// 신고구분
			$("#modal_eTax select[name='TAX_REPORT_DIVISION'] option[value='"+ data.TAX_REPORT_DIVISION+"']").prop('selected', true);
			// 수정시 발급구분
			$("#modal_eTax select[name='TAX_ISSUE_DIVISION'] option[value='"+data.TAX_ISSUE_DIVISION+"']").prop('selected', true);
			// 결제방법
			$("#modal_eTax select[name='TAX_BILLING_TYPE'] option[value='"+data.TAX_PAYWAY+"']").prop('selected', true);
			// 계산서 분류
			$("#modal_eTax select[name='TAX_ACCOUNT_PART'] option[value='"+data.TAX_ACCOUNT_PART+"']").prop('selected', true);					
			// 기승인번호
			$("#modal_eTax input[name='TAX_ISSUE_NUMBER']").val(data.TAX_ISSUE_NUMBER);
			// 수정코드
			
			$("#modal_eTax select[name='MODIFY_CODE'] option[value='"+data.TAX_MODIFY_CODE+"']").prop('selected', true);
			// 비고
			$("#modal_eTax input[name='REMARK']").val(data.REMARK);
			
			$("#modal_eTax input[name='SEQ']").val(data.SEQ);
			$("#modal_eTax input[name='SRL_NO']").val(data.SRL_NO);

			// 기 승인번호
			$("#modal_eTax input[name='TAX_ACKNOWLEDGMENT_NUMBER']").val(data.TAX_ACKNOWLEDGMENT_NUMBER);

	}

	$(function(){

		$("#WRITE_DATE").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			showDropdowns: true,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				}
		});
		$("#WRITE_DATE").val(moment().format("YYYY-MM-DD"));
		
		// 거래일 검색
		$("#TAX_YM").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'yy-mm',
			prevText: '이전 달',
			nextText: '다음 달',
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNames: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			showMonthAfterYear: true,
			yearSuffix: '년',
			closeText : "확인",
			currentText: '이번달',
			locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
					monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}).focus(function() {
			var thisCalendar = $(this);
			$('.ui-datepicker-calendar').detach();
			$('.ui-datepicker-close').click(function() {
				month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();

				day = $(".ui-datepicker-day :selected").val();
				thisCalendar.datepicker('setDate', new Date(year, month, 1));

				$("input[name='txtMonth']").val( parseInt(month) + 1);
				$("input[name='txtYear']").val(year);
				$("input[name='txtday']").val(day);

			});
		});
		var year	= moment().format("YYYY");
		var month	= moment().format("MM");
		$("#TAX_YM").val(year+"-"+month);

		var pCustMk		= "{{Request::segment(4)}}";
		var pWriteYM	= "{{Request::segment(5)}}";
		var pAmt		= "{{Request::segment(6)}}";
		var pSaleBuyDiv	= "{{Request::segment(7)}}";
		// http://blwc.ecis.co.kr/tax/tax/elist/C289/2016-11/35918000/1
		

		if( pCustMk.length > 1 && pWriteYM.length > 1 && pAmt.length > 1 && pSaleBuyDiv.length > 0){
			
			$.getJSON('/custcd/custcd/Edit/' + pCustMk + "/page=1", {
				_token			: '{{ csrf_token() }}',
				CUST_MK			: pCustMk , 
				
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				
				
				$("#btnInsert").trigger('click');
					
				// 매입/매출 구분
				if( pSaleBuyDiv == "1"){
					$("input[name='BUY_SALE_DIV'][value='O']").prop("checked", true);
				} else{
					$("input[name='BUY_SALE_DIV'][value='I']").prop("checked", true);
				}

				// 거래처코드
				$("#modal_eTax input[name='CUST_MK']").val(pCustMk);
				$("#modal_eTax input[name='CUST_FRNM']").val(data.FRNM);

				// 공급가액
				$("#modal_eTax input[name='SUM_AMT']").val(Number(pAmt).format());

				// 년/월
				$("#modal_eTax input[name='TAX_YM']").val(pWriteYM);

				// 품목 
				$("#modal_eTax input[name='GOODS']").val("활어");
				
			});

		}
	
		//정정/취소/폐기
		$("#tblList tbody").on("click", ".btnUptTax", function(){
			
			if( !confirm("정정/취소/폐기는 계산서가 복사되어 새로 생성됩니다\n계속 진행하시겠습니까? ")){
				return;
			}
			var row = oTable.row( $(this).parents('tr') ).data();
			
			var WRITE_DATE		= row.WRITE_DATE;
			var SEQ				= row.SEQ;
			var BUY_SALE_DIV	= row.BUY_SALE_DIV;
			
			$.getJSON('/tax/tax/eSetCopyNMod', {
					  SEQ	: SEQ
					, WRITE_DATE	: WRITE_DATE
					, BUY_SALE_DIV	: BUY_SALE_DIV
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("#modal_eTax #mode").val("upd");
				$('#modal_eTax').modal({show:true});
				
				oTable.draw() ;
				getBindEdit(data.data);
				
				// 추가적으로 정정/폐기/수정일 경우는 입력컨트롤수정을 수정모드로 세팅해준다

				$("select[name='TAX_ACCOUNT_TYPE']").val("02").change(function(){
					$(this).find("option[value='02']").attr("selected", true)
				});
				$("select[name='TAX_ISSUE_DIVISION']").val("06").change(function(){
					$(this).find("option[value='06']").attr("selected", true)
				});
				$("#modal_eTax .alert").remove();
				$("#modal_eTax .modal-content").prepend( getAlert('warning', 'success', "기승인번호, 수정구분, 수정코드를 반드시 확인하세요") ).show();
				

			}).error(function(xhr,status, response) {

				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_eTax .edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();
			});
		});
		

		// 계산성 작성시 거래처 조회
		$("#btnSearchCust").click(function(){
			// 거래처검색
			$( ".slideSearchCust" ).show(function(){
				
				$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", true) ;
				$('#tblCustList').dataTable().fnDestroy();
				$('#tblCustList tbody').off();

				var cTable = $('#tblCustList').DataTable({
					processing	: true,
					serverSide	: true,
					retrieve	: true,
					search		: false,
					info		: false,
					ajax: {
						url: "/buy/sujo/getPisCust",
						data:function(d){
							d.textSearch	= $("#modal_eTax input[name='strSearchCust']").val();
							d.corp_div		= $("#modal_eTax input[name='BUY_SALE_DIV']:checked").val() == "O" ? 'S' : "P"
						}
					},
					order: [[ 1, 'asc' ]],
					columns: [
						{ data: 'CUST_MK', name: 'CUST_MK' , className: "dt-body-center"},
						{ data: 'FRNM', name: 'FRNM' },
						{ data: 'RPST',  name: 'RPST' },
						{ data: 'PHONE_NO',  name: 'PHONE_NO' ,className: "dt-body-center"},
						{
							data:   "CUST_MK",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success ' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-left"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

				// 계산서 입력/수정 팝업에서 거래처 조회 닫기 버튼
				$('#tblCustList tbody').on( 'click', 'button', function () {
					var data = cTable.row( $(this).parents('tr') ).data();
					$("#modal_eTax #CUST_MK").val(data.CUST_MK);
					$("#modal_eTax #CUST_FRNM").val(data.FRNM);
					$("input[name='strSearchCust']").val('');
					$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", false) ;
					$(".slideSearchCust" ).slideUp();
				});
				
				// 계산서 입력/수정 팝업에서 거래처 검색입력시 실시간 검색 이벤트
				$("input[name='strSearchCust']").on("keyup", function(){
					cTable.search($(this).val()).draw() ;
				});
			});
		});

		// 거래처 검색 닫기
		$(".btn_CustClose").click(function(){ 
			$("div.slideSearchCust").hide(); 
			// 거래처 검색시에는 매입/매출 구분을 변경할수 없도록 막은걸 풀어준다
			$("#modal_eTax input[name='BUY_SALE_DIV']").prop("disabled", false) ;
		});

		// 계산서 입력 : 계산서 자동 등록 불러오기
		$("#btnInsertTax").click(function(){
			
			var custFrnm	= $("input[name='CUST_FRNM']").val();
			var year		= $("input[name='txtYear']").val();
			var month		= $("input[name='txtMonth']").val();
			
			$(".edit_alert > ul").empty();
			
			if( custFrnm == "" || custFrnm.length == 0){
				$(".edit_alert > ul").append( "<li>거래처를 먼저 선택하세요</li>").show();
				$(".edit_alert").css("display", "block");
				return false;
			}
			if( year == "" || year.length == 0 || month == "" || month.length == 0){
				$(".edit_alert > ul").append( "<li>년/월을 입력하세요</li>").show();
				$(".edit_alert").css("display", "block");
				return false;
			}

			// 매입/매출 구분
			var url = "/tax/tax/getTaxSaleGeneration";
			if( $("#modal_eTax input[name='BUY_SALE_DIV']:checked").val() == "I"){
				url = "/tax/tax/getTaxBuyGeneration"
			}

			$.getJSON(url, {
				_token			: '{{ csrf_token() }}',
				INPUT_MONTH		: $("#modal_eTax input[name='txtMonth']").val() ,
				INPUT_YEAR		: $("#modal_eTax input[name='txtYear']").val() ,
				CUST_MK			: $("#modal_eTax input[name='CUST_MK']").val() , 
				ONLY_CASH		: $("input[name='ONLY_CASH']").is(":checked") ? "1" : "0",
				
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));
				$("#modal_eTax input[name='GOODS']").val(data.GOODS);
				$("#modal_eTax input[name='SUM_AMT']").val(Number(data.SUM).format());
				var res = data.WRITE_DATE.split("-");

				//$("#modal_insert_input_date").val(data.WRITE_DATE);

				$("#modal_eTax input[name='txtYear']").val(res[0]);
				$("#modal_eTax input[name='txtMonth']").val(res[1]);
				$("#modal_eTax input[name='txtday']").val(res[2]);
				
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_insert .edit_alert');
				info.hide().find('ul').empty();
					for(var k in error.message){
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				info.slideDown();
			});
		});
		
		// 계산서 작성/수정 시 팝업창에서 계산서 종류를 변경했을때 이벤트
		$("#modal_eTax select[name='TAX_ACCOUNT_TYPE']").change(function(){
			
			var selVal = $(this).find("option:selected").val();
			if( selVal == "02" || selVal == "04"){
				$("#modal_eTax input[name='TAX_ISSUE_NUMBER']").prop("disabled", false);
				$("#modal_eTax select[name='MODIFY_CODE']").prop("disabled", false);
			}else{
				$("#modal_eTax input[name='TAX_ISSUE_NUMBER']").prop("disabled", true);
				$("#modal_eTax select[name='MODIFY_CODE']").prop("disabled", true);
			}
		
		});
		
		// 국세청 엑셀다운로드 버튼 클릭 이벤트
		$("#btnExcelTax").click(function(){
			var arr_code = [];
			$('#tblList tbody > tr > td.chk > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) { 
					arr_code.push($(this).val()); 
				}
			});

			var arr_data = [];
			$(".content > .alert").remove();

			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "출력할 항목을 1개 이상 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 100){
				$(".content").prepend( getAlert('warning', '경고', "출력할 항목은 최대 100건까지만 업로드할 수 있습니다 [현재 건수 : " + arr_code.length + "]") ).show();
				return false;
			}

			if( arr_code.length >= 1){
				var data = oTable.rows( $("#tblList tbody > tr > td > input[type=checkbox]:checked").parents('tr') ).data();
				for(var i=0; i<arr_code.length; i++){
					arr_data.push({ 'SEQ' : data[i].SEQ, 'CUST_MK' : data[i].CUST_MK, 'BUY_SALE_DIV':data[i].BUY_SALE_DIV, 'WRITE_DATE' : data[i].WRITE_DATE });
				}
				
				var url		= '/tax/tax/ExcelTaxBillDownload';
				$.fileDownload( url , {
					httpMethod: "POST",
					data: {query : arr_data, _token: '{{ csrf_token() }}',},
					successCallback: function (url) {
						//$preparingFileModal.dialog('close');
					},
				});
			}
		});
		
		// 팝업창이 닫혔을때 이벤트
		$('#modal_eTax').on('hidden.bs.modal', setInitModalClose);
		// 팝업창이 닫혔을때 이벤트 핸들러
		function setInitModalClose(){
			var info = $('.edit_alert');
			info.hide().find('ul').empty();
			$("#modal_eTax .alert").remove();
		}

		$("body").on("click", ".btnUpdate", function(){

			var arr_code = [];
		
			var data = oTable.row( $(this).parents('tr') ).data();
			arr_code.push(data.CUST_MK);
		
			if( arr_code.length == 1){
				var page = $("input[name='page']").val()	
				$.getJSON("/custcd/cust_grp_info/Edit/" + arr_code[0] + "/page=" + page+"", {
					SUJO_NO : arr_code[0] ,
					_token : '{{ csrf_token() }}'
				},function(data){

					// 거래처그룹 팝업 금지
					//$("#btnSearchCustGrpEdit").prop("disabled", true);
					
					$("#CUST_MK_EDIT").val(data.CUST_MK);
					$("#CUST_GRP_NM_EDIT").val(data.CUST_GRP_NM);
					$("#CUST_GRP_CD_EDIT").val(data.CUST_GRP_CD);
					$("input[name='CUST_GRP_CD_OLD']").val(data.CUST_GRP_CD);

					$("#CORP_DIV_EDIT option[value='" + data.CORP_DIV + "']").prop("selected", true);
				
					$("#FRNM_EDIT").val(data.FRNM);
					$("#RPST_EDIT").val(data.RPST);
					$("#RANK_EDIT > option").each(function(i , d){
						if( $(d).val() == data.RANK_EDIT){
							$(d).attr('selected','selected');
						}
					});
					$("#ETPR_NO_EDIT").val(data.ETPR_NO);
					$("#JUMIN_NO_EDIT").val(data.JUMIN_NO);
					$("#PHONE_NO_EDIT").val(data.PHONE_NO);
					$("#FAX_EDIT").val(data.FAX);
					$("#ADDR1_EDIT").val(data.ADDR1);
					$("#ADDR2_EDIT").val(data.ADDR2);
					$("#POST_NO_EDIT").val(data.POST_NO);
					$("#UPJONG_EDIT").val(data.UPJONG);
					$("#UPTE_EDIT").val(data.UPTE);
					$("#AREA_CD_EDIT").val(data.AREA_CD);
					$("#AREA_NM_EDIT").val(data.AREA_NM);
					$("#REMARK_EDIT").val(data.REMARK);
					$('#modal_custGrpEdit').modal({show:true});

					$("#WRITE_DATE_EDIT").val(data.WRITE_DATE);
					$("#EMAIL_EDIT").val(data.EMAIL);
					
					if( data.INTERFACE_YN !== null){
						$("input[name='INTERFACE_YN_EDIT']").prop('checked', false);
						$("input[name='INTERFACE_YN_EDIT'][value='" + data.INTERFACE_YN + "']").prop('checked', true);
					}else{
						$("input[name='INTERFACE_YN_EDIT'][value='Y']").prop('checked', false);
						$("input[name='INTERFACE_YN_EDIT'][value='N']").prop('checked', true);
					}

				});
			}
		});
		
		// 거래처수정 
		$("#btnUpdateCustCdOK").click(function(){
			//$.getJSON('/buy/sujo/addDataSujo', {
			$.getJSON('/custcd/cust_grp_info/Update', {
				_token		: '{{ csrf_token() }}'
				, CUST_GRP_CD_EDIT	: $("#CUST_GRP_CD_EDIT").val()
				, CUST_MK_EDIT		: $("#CUST_MK_EDIT").val()
				, CORP_DIV_EDIT		: $("#CORP_DIV_EDIT").val()
				, FRNM_EDIT			: $("#FRNM_EDIT").val()
				, RPST_EDIT			: $("#RPST_EDIT").val()
				//, RANK_EDIT		: $("#RANK_EDIT").val()
				, ETPR_NO_EDIT		: $("#ETPR_NO_EDIT").val()
				, JUMIN_NO_EDIT		: $("#JUMIN_NO_EDIT").val()
				, PHONE_NO_EDIT		: $("#PHONE_NO_EDIT").val()
				, FAX_EDIT			: $("#FAX_EDIT").val()
				, ADDR1_EDIT		: $("#ADDR1_EDIT").val()
				, ADDR2_EDIT		: $("#ADDR2_EDIT").val()
				, POST_NO_EDIT		: $("#POST_NO_EDIT").val()
				, UPJONG_EDIT		: $("#UPJONG_EDIT").val()
				, UPTE_EDIT			: $("#UPTE_EDIT").val()
				, AREA_CD_EDIT		: $("#AREA_CD_EDIT").val()
				, REMARK_EDIT		: $("#REMARK_EDIT").val()
				, WRITE_DATE_EDIT	: $("#WRITE_DATE_EDIT").val()
				, EMAIL_EDIT		: $("#EMAIL_EDIT").val()
				, INTERFACE_YN_EDIT	: $("input[name='INTERFACE_YN_EDIT']:checked").val()
			}).success(function(xhr){
				$('#modal_custGrpEdit').modal("hide");
				oTable.draw();
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				if(error.reason == "PrimaryKey"){
					alert("거래처 그룹코드가 있습니다.");
					return false;
				}else{
					var info = $('#modal_custGrpEdit .edit_alert');
					info.hide().find('ul').empty();
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
								});
							}
						}
					info.slideDown();
				}
			});
		});
	
	});
	
</script>
<!-- 거래처 수정-->
<div class="modal fade" id="modal_custGrpEdit" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="padding:20px 30px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 거래처 수정</h4>
			</div>
			<div class="modal-body" style="padding:20px 25px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="form-group">
					<label for="CUST_GRP_CD"><i class='fa fa-circle-o text-red'></i>  거래처그룹</label>
					<input type="text" class="form-control form-input-sm" id="CUST_GRP_CD_EDIT" placeholder="" name="CUST_GRP_CD" value="" readonly="readonly"/>
					<input type="hidden" placeholder="" name="CUST_GRP_CD_OLD" value="" />
					<button type="button" name="btnSearchCustGrpEdit" id="btnSearchCustGrpEdit" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md" id="CUST_GRP_NM_EDIT" placeholder="" name="CUST_GRP_NM" value="" readonly="readonly"/>
				</div>
				<!-- 거래처그룹 검색 -->
				<div class="form-group SearchCustGrpEdit">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 거래처그룹 선택</h5>					
						<label for="textSearchCustGrpCdEdit"><i class='fa fa-circle-o text-aqua'></i>  거래처 부호</label>
						<input type="text" class="" name="textSearchCustGrpCdEdit" id="textSearchCustGrpCdEdit" />
						
						<button type="button" class="btn btn-danger pull-right btn-CustCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>
				
						<div class="box-body">
							<table id="tableCustGrpEditList" class="table table-bordered table-hover display nowrap" summary="거래처그룹 목록">
								<caption>거래처그룹 선택</caption>
								<thead>
									<tr>
										<th class="name">그룹코드</th>
										<th class="name">그룹명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- 거래처그룹 검색 끝 -->
				<div class="form-group">
					<label for="CUST_MK"><i class='fa fa-circle-o text-red'></i>  거래처부호</label>
					<input type="text" class="form-control form-input-md" id="CUST_MK_EDIT" placeholder="" name="CUST_MK" value="" readonly="readonly">
					
					<div style="display:none;">
						<label for="INTERFACE_YN_EDIT" class='rightLabel' style="width:175px;"><i class='fa fa-circle-o text-aqua'></i> 미수/미지급 출력</label>
						<input type="radio" id="INTEFACE_Y_EDIT" placeholder="" name="INTERFACE_YN_EDIT" value="Y" checked="checked"> 예
						<input type="radio" id="INTEFACE_N_EDIT" placeholder="" name="INTERFACE_YN_EDIT" value="N"> 아니오
					</div>
				</div>
				<div class="form-group">
					<label for="CORP_DIV"><i class='fa fa-circle-o text-aqua'></i>  업체구분</label>
					<select class="form-control form-input-md" name="CORP_DIV" id="CORP_DIV_EDIT">
						<option value="J">활어조합</option>
						<option value="P">활어매입처</option>
						<option value="S">활어매출처</option>
						<option value="T">비용계정</option>
					</select>

					<label for="WRITE_DATE_EDIT" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 첫거래일자</label>
					<input type="text" class="form-control form-input-md" id="WRITE_DATE_EDIT" placeholder="" name="WRITE_DATE" value="" />
				</div>
				<div class="form-group">
					<label for="FRNM"><i class='fa fa-check-circle text-red'></i>  거래처상호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="FRNM_EDIT" placeholder="" name="FRNM" value="">
					<label for="RPST" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  대표자</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="RPST_EDIT" placeholder="" name="RPST" value="">
				</div>
				<div class="form-group">
					<label for="ETPR_NO_EDIT"><i class='fa fa-circle-o text-aqua'></i> 사업자등록번호</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="ETPR_NO_EDIT" data-inputmask="'mask': '999-99-99999'" placeholder="" name="ETPR_NO" value="">
					<label for="JUMIN_NO" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 생년월일</label>
					<input type="text" class="form-control form-input-md" id="JUMIN_NO_EDIT" placeholder="" name="JUMIN_NO" value="">
				</div>
				<div class="form-group">
					<label for="FAX"><i class='fa fa-circle-o text-aqua'></i> 휴대폰번호</label>
					<input type="text" class="form-control form-input-md" id="FAX_EDIT" placeholder="" name="FAX" value="">
					<label for="UPTE" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  업태</label>
					<input type="text" class="form-control form-input-md cis-lang-ko" id="UPTE_EDIT" placeholder="" name="UPTE" value="">
				</div>
				<div class="form-group">
					<label for="PHONE_NO"><i class='fa fa-circle-o text-aqua'></i>  전화번호</label>
					<input type="text" class="form-control form-input-md" id="PHONE_NO_EDIT" placeholder="" name="PHONE_NO" value="">
					
					<label for="UPJONG" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i>  종목</label>
					<input type="text" class="form-control form-input-md" id="UPJONG_EDIT" placeholder="" name="UPJONG" value="">
				</div>
				<div class="form-group">
					<label for="AREA_CD"><i class='fa fa-circle-o text-red'></i>  지역</label>
					<input type="text" class="form-control form-input-sm cis-lang-ko" id="AREA_CD_EDIT" placeholder="" name="AREA_CD" value="" readonly="readonly"/>
					<button type="button" name="btnSearchAreaCd_Edit" id="btnSearchAreaCd_Edit" class="btn btn-info">
						<i class="fa fa-search"></i>
					</button>
					<input type="text" class="form-control form-input-md" id="AREA_NM_EDIT" placeholder="" name="AREA_NM" value="" readonly="readonly"/>
				</div>
				<!-- 지역 검색 -->
				<div class="form-group SearchAreaCdEdit">
					<div class="box box-primary">
						<h5><span class="glyphicon glyphicon-th-large"></span> 지역코드 조회</h5>					
						<label for="textSearchAreaCdEdit"><i class='fa fa-circle-o text-aqua'></i>  지역명</label>
						<input type="text" class="" name="textSearchAreaCdEdit" id="textSearchAreaCdEdit" />
						
						<button type="button" class="btn btn-danger pull-right btn-AreaCd-Close">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>				
						<div class="box-body">
							<table id="tableAreaCdEditList" class="table table-bordered table-hover" summary="게시물 목록">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th class="name">지역코드</th>
										<th class="name">지역명</th>
										<th class="name">선택</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="POST_NO"><i class='fa fa-circle-o text-aqua'></i>  우편번호</label>
					<input type="text" class="form-control form-input-md" id="POST_NO_EDIT" placeholder="" name="POST_NO" value="" />
					<button type="button" name="searchPisMk" id="btnSearchPisMk" class="btn btn-info" onclick="sample6_execDaumPostcode('edit')" value="우편번호 찾기">우</button>

					<label for="EMAIL_EDIT" class='rightLabel'><i class='fa fa-circle-o text-aqua'></i> 이메일주소 </label>
					<input type="text" class="form-control form-input-sm cis-lang-en" id="EMAIL_EDIT" placeholder="" name="EMAIL" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR1"><i class='fa fa-circle-o text-aqua'></i>  주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR1_EDIT" placeholder="" name="ADDR1" value="" />
				</div>
				<div class="form-group">
					<label for="ADDR2"><i class='fa fa-circle-o text-aqua'></i>  상세주소</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="ADDR2_EDIT" placeholder="" name="ADDR2" value="">
				</div>
				<div class="form-group">
					<label for="REMARK"><i class='fa fa-circle-o text-aqua'></i>  비고</label>
					<input type="text" class="form-control form-input-lg cis-lang-ko" id="REMARK_EDIT" placeholder="" name="REMARK" value="">
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success pull-right" id="btnUpdateCustCdOK">
					<span class="glyphicon glyphicon-off"></span> 등록
				</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.main')
@section('class','매입관리')
@section('title','세금계산서 합계')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	#modal_custCd label, #modal_custGrpEdit label{
		display:inline-block;
		width:24%;
		clear:both;
	}
	#modal_custCd .form-input-sm, #modal_custGrpEdit .form-input-sm{
		display:inline-block;
		width:10%;
		clear:both;
	}

	#modal_custCd .form-input-md, #modal_custGrpEdit .form-input-md{
		display:inline-block;
		width:24%;
		clear:both;
	}

	#modal_custCd .form-input-lg, #modal_custGrpEdit .form-input-lg{
		display:inline-block;
		width:74%;
		clear:both;
	}

	.SearchCustGrp , .SearchAreaCd, .SearchCustGrpEdit , .SearchAreaCdEdit{
		display:none;
	}

	#tableCustGrpList_filter, #tableAreaCdList_filter, #tableCustGrpEditList_filter, #tableAreaCdEditList_filter{
		display:none;
	}

	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px; vertical-align:middle;}
	.btnSearch{ margin-top:0;}
	.dataTables_filter{ display:none;}

	
	.ui-datepicker-calendar {
		display: none;
	}

	tfoot th.dt-body-right { text-align:right;}

	#reportrange1 .btn-info{
		margin-top:-1px;
		font-size:13.5px;
		padding:1px 7px;
	}

	#btnPdf{display:none;}
	
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header">
			<ul class="nav nav-tabs"id="tabContent">
				<li class="active firstTab" data-id="custSum" ><a href="#custSum" data-toggle="tab">거래업체별 합계</a></li>
				<li class="" data-id="saleSum"><a href="#saleSum" data-toggle="tab">매출계산서 합계</a></li>
				<li class="" data-id="inputSum"><a href="#inputSum" data-toggle="tab">매입계산서 합계</a></li>
				<li class="" data-id="yearSum"><a href="#yearSum" data-toggle="tab">연간 합계</a></li>
			</ul>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="tab-content" id="tab_area">
			<div class="tab-pane firstTab active" id="details">
				<div class="box-body">
					<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
					<div class="col-md-4 col-xs-12">
						<div id="reportrange1" class="pull-right" style="background: #fff; cursor: pointer; padding: 1px 10px; border: 1px solid #ccc; width: 100%; font-size:12px;">
							<label>년-월 </label> 
							<span> </span>
							<input id="reportrange" type="text" readonly="readonly" style="width:60px;" />

							<input type='hidden' name="txtMonth" />
							<input type='hidden' name="txtYear" />
							<input type='hidden' name="txtday" />
							<input type='hidden' name="start_date" />
							<input type='hidden' name="end_date" />

							<input type='hidden' name="start_date_year" />
							<input type='hidden' name="end_date_year" />
							<button type="button" name="searchAll" id="search-All" class="btn btn-info">전체</button>
							<button type="button" name="searchAll" id="search-1half" class="btn btn-info">2016상반기</button>
							<button type="button" name="searchAll" id="search-2half" class="btn btn-info">2016하반기</button>
						</div>
					</div>

					<div class="col-md-2 col-xs-6">
						<select class="form-control" name="srtCondition">
							<option value="ALL">그룹</option>
							@foreach($custGrp as $item)							
							<option value="{{ $item ->CUST_GRP_CD }}">{{ $item->CUST_GRP_NM }}</option>
							@endforeach
						</select>
					</div>

					<div class="col-md-2 col-xs-6">
						<div class="input-group">
							<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
							<span class="input-group-btn">
								<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
					<div class="col-md-2 col-xs-6">
						<div class="input-group">
							
							<button type="button" name="searchAll" id="btnPdf" class="btn btn-info btnPdf">출력</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="tab-content" id="tab_area">
			<div class="tab-pane active" id="custSum">
				<div class="box-body">
					<table id="tblListCust" class="table table-bordered table-hover display nowrap" summary="거래업체별 합계" width="100%">
						<caption>거래업체별 합계</caption>
						<thead>
							<tr>
								<th width="60px">거래업체명</th>
								<th width="60px">매입금</th>
								<th width="60px">매입수</th>
								<th width="60px">매출금</th>
								<th width="60px">매출수</th>
								<th width="auto"></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane" id="saleSum">
				<div class="box-body">
					<table id="tblListSale" class="table table-bordered table-hover display nowrap" summary="매출계산서 합계" width="100%">
						<caption>매출계산서 합계</caption>
						<thead>
							<tr>
								<th width="60px">거래업체명</th>
								<th width="60px">사업등록번호</th>
								<th width="60px">매출금</th>
								<th width="60px">매출수</th>
								<th width="auto">비고</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane" id="inputSum">
				<div class="box-body">
					<table id="tblListInput" class="table table-bordered table-hover display nowrap" summary="매입계산서 합계" width="100%">
						<caption>매입계산서 합계</caption>
						<thead>
							<tr>
								<th width="60px">거개업체명</th>
								<th width="60px">사업등록번호</th>
								<th width="60px">매입금</th>
								<th width="60px">매입수</th>
								<th width="auto">비고</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane" id="yearSum">
				<div class="box-body">
					<!--
					<button type="button" class="btn btn-info btn-default pull-right" id="btnChgTax">
						<span class="glyphicon glyphicon-on"></span> 매출_신고분변경
					</button>
					-->

					<div class="col-md-12 col-xs-12" id="area">
						
					</div>

					<table id="tblListYear" class="table table-bordered table-hover display nowrap" summary="연간 합계" width="100%">
						<caption>연간 합계</caption>
						<thead>
							<tr>
								<th width="30px">월</th>
								<!--<th width="60px">매출액(신고분)</th>-->
								<th width="60px">매입금</th>
								<th width="60px">매입건</th>
								<!--<th width="80px">매출대비 매입 비율</th>-->
								<!--<th width="80px">매입계산서 과부족</th>-->
								<th width="60px">매출금</th>
								<th width="60px">매출건</th>
								<!--<th width="auto">매출계산서 과부족</th>-->
								<th width="auto"></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>합계</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								
							</tr>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>

	var year, month, day;

	

	$(function () {
		var start = moment().startOf('month');
		var end = moment().endOf('month');
		
		year = moment().format("YYYY");
		month = moment().startOf('month').format("MM");
		day = moment().startOf('day').format("DD");
		
		//console.log(year, month, day);
		$("input[name='start_date']").val(start.format('YYYY-MM-DD'));
		$("input[name='end_date']").val(end.format('YYYY-MM-DD'));
		
		$("#reportrange").val(start.format('YYYY'));
	
		var start_year = moment().startOf('year').format('YYYY-MM-DD');
		var end_year = moment().endOf('year').format('YYYY-MM-DD');

		$("input[name='start_date_year']").val( start_year);
		$("input[name='end_date_year']").val( end_year);
		
		var pre_year= moment().format('YYYY');

		$("#search-1half").html(pre_year + "상반기");
		$("#search-2half").html(pre_year + "하반기");

		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start);
			$("input[name='end_date']").val(end);
		}

		// 거래일 검색
		$("#reportrange").datepicker({
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'yy',
			closeText: "확인",
			onClose: function(dateText, inst) { 
				var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(year, 1));
				
				pre_year = year;
				$("#search-All").html(year + " 전체");
				$("#search-1half").html(year + " 상빈기");
				$("#search-2half").html(year + " 하빈기");
			},
			}).focus(function() {
				$(".ui-datepicker-month").hide();
				var thisCalendar = $(this);
				$('.ui-datepicker-calendar').detach();
				$('.ui-datepicker-close').click(function() {
					
					year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					thisCalendar.datepicker('setDate', new Date(year, month, 1));

					$("input[name='txtMonth']").val( parseInt(month) + 1);
					$("input[name='txtYear']").val(year);
					$("input[name='txtday']").val(day);

					$("input[name='start_date']").val( moment(pre_year + '-01-01').format('YYYY-MM-DD')).trigger('change');
					$("input[name='end_date']").val( moment(pre_year +'-12-31').format('YYYY-MM-DD')).trigger('change');

					$("input[name='start_date_year']").val( moment(pre_year + '-01-01').format('YYYY-MM-DD')).trigger('change');
					$("input[name='end_date_year']").val( moment(pre_year +'-12-31').format('YYYY-MM-DD')).trigger('change');
					
					$("#search-All").html(pre_year + " 전체");
					$("#search-1half").html(pre_year + " 상빈기");
					$("#search-2half").html(pre_year + " 하빈기");
				});
			});

		
		// 1. 거래처별 합계
		var oTable = $('#tblListCust').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/tax/tax/getTaxCustSumList",
				data:function(d){
					d.srtCondition		= $("select[name='srtCondition'] option:selected").val();
					d.textSearch		= $("input[name='textSearch']").val();
					d.WRITE_DATE_START	= $("input[name='start_date']").val();
					d.WRITE_DATE_END	= $("input[name='end_date']").val();
				}
			},
			columns: [
				{ data: 'FRNM', name: 'FRNM' },
				{ 
					data: 'INPUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{ 
					data: 'INPUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{ 
					data: 'OUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{ 
					data: 'OUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{
					data: 'CUST_MK',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return '';
						}
						return '';
					},
					className: "dt-body-right red"
					
				},
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총매입금
				buySum = api.column( 1 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 1 ).footer() ).html(Number(buySum).format() );
				
				// 총매입수
				buyCntSum = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(Number(buyCntSum).format() );
				
				// 총매출금
				saleSum = api.column( 3).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(saleSum).format() );

				// 총매출수
				saleQtySum = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(Number(saleQtySum).format() );
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		
		
		
		// 2. 매출계산서 합계
		var sTable = $('#tblListSale').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/tax/tax/getTaxSaleSumList",
				data:function(d){
					d.srtCondition		= $("select[name='srtCondition'] option:selected").val();
					d.textSearch		= $("input[name='textSearch']").val();
					d.WRITE_DATE_START	= $("input[name='start_date']").val();
					d.WRITE_DATE_END	= $("input[name='end_date']").val();
				}
			},
			columns: [
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'ETPR_NO', name: 'ETPR_NO', className: "dt-body-center" },
				{ 
					data: 'OUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{ 
					data: 'OUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{ data: 'FRNM', name: 'FRNM' }
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총매출금
				saleSum = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(Number(saleSum).format() );
				
				// 총매출수
				saleCntSum = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(saleCntSum).format() );
				
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 3. 매입계산서 합계
		var iTable = $('#tblListInput').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/tax/tax/getTaxInputSumList",
				data:function(d){
					d.srtCondition		= $("select[name='srtCondition'] option:selected").val();
					d.textSearch		= $("input[name='textSearch']").val();
					d.WRITE_DATE_START	= $("input[name='start_date']").val();
					d.WRITE_DATE_END	= $("input[name='end_date']").val();
				}
			},
			columns: [
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'ETPR_NO', name: 'ETPR_NO', className: "dt-body-center" },
				{ 
					data: 'INPUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{ 
					data: 'INPUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{ data: 'FRNM', name: 'FRNM' }
			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총매입금
				buySum = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(Number(buySum).format() );
				
				// 총매입수
				buyCntSum = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(buyCntSum).format() );
				
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		// 4. 연간 합계
		var yTable = $('#tblListYear').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 200,		// 기본 100개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/tax/tax/getTaxYearSumList",
				data:function(d){
					d.srtCondition		= $("select[name='srtCondition'] option:selected").val();
					d.textSearch		= $("input[name='textSearch']").val();
					d.WRITE_DATE_START	= $("input[name='start_date_year']").val();
					d.WRITE_DATE_END	= $("input[name='end_date_year']").val();
				}
			},
			columns: [
				{ data: 'MONTHS', name: 'MONTHS', className: "dt-body-center" },
				/*
				{ 
					data: 'dec_money',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				*/
				{ 
					data: 'INPUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				{ 
					data: 'INPUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right blue"
				},
				/*
				{ 
					data: 'saleVSbuy',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				{ 
					data: 'overBuy',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				*/
				{ 
					data: 'OUT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
				{ 
					data: 'OUT_CNT',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right red"
				},
					/*
				{ 
					data: 'overSale',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return Number(data).format();
						}
						return data;
					},
					className: "dt-body-right"
				},
				*/
				{ 
					data: 'FRNM',		
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return '';
						}
						return '';
					},
					className: "dt-body-right"
				},
				

			],
			footerCallback: function ( row, data, start, end, display ) {
				var api = this.api(), data;

				// Remove the formatting to get integer data for summation
				var intVal = function ( i ) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '')*1 :
						typeof i === 'number' ?
							i : 0;
				};
				// 총매입금
				buySum = api.column( 1 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 1 ).footer() ).html(Number(buySum).format() );
				
				// 총매입수
				buyCntSum = api.column( 2 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 2 ).footer() ).html(Number(buyCntSum).format() );

				// 총매출금
				saleSum = api.column( 3 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 3 ).footer() ).html(Number(saleSum).format() );
				
				// 총매출수
				saleCntSum = api.column( 4 ).data().reduce( function (a, b) {
						return intVal(a) + intVal(b);}, 0 );
				$( api.column( 4 ).footer() ).html(Number(saleCntSum).format() );
				
			},
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});
		
		$("#search-btn").click(function(){
			oTable.draw();
			sTable.draw();
			iTable.draw();
			yTable.draw();
		});

		$("select[name='srtCondition']").change(function(){
			
			oTable.draw();
			sTable.draw();
			iTable.draw();
			yTable.draw();
		});

		$("input[name='start_date'], input[name='end_date']").change(function(){
			oTable.draw();
			sTable.draw();
			iTable.draw();
		});
		$("input[name='start_date_year'], input[name='end_date_year']").change(function(){
			yTable.draw();
		});

		
		$("#search-All").click(function(){
			
			//console.log(year);
			$("input[name='start_date']").val( moment().year(year).startOf('year').format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val( moment().year(year).endOf('year').format('YYYY-MM-DD')).trigger('change');

			oTable.draw();
			sTable.draw();
			iTable.draw();
		});


		

		// 상반기
		$("#search-1half").click(function(){
			
			//console.log(year);
			
			$("input[name='start_date']").val( moment(pre_year + '-01-01').format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val( moment(pre_year + '-06-30').format('YYYY-MM-DD')).trigger('change');

			oTable.draw();
			sTable.draw();
			iTable.draw();
		});

		// 하반기
		$("#search-2half").click(function(){
			
			//console.log(year);
			$("input[name='start_date']").val( moment(pre_year + '-07-01').format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val( moment(pre_year +'-12-31').format('YYYY-MM-DD')).trigger('change');

			oTable.draw();
			sTable.draw();
			iTable.draw();
		});

		

		// 탭 클릭 버튼 보이기/가리기 기능
		$("body").on("click", "ul.nav-tabs > li", function(){
		
			var activeTabPdf = $(this).attr("data-id");
			//alert(activeTabPdf);
			if( activeTabPdf == "saleSum" || activeTabPdf== "inputSum" || activeTabPdf== "yearSum"  ){
				$("#btnPdf").css("display", "block");
			}else{
				$("#btnPdf").css("display", "none");
			}
			
		});
		
		// PDF 출력
		$("body").on("click","#btnPdf", function(){
			
			var activeTabPdf = $("ul.nav-tabs > li.active").attr("data-id");
			
			if( activeTabPdf != "custSum"){

				if( activeTabPdf == "inputSum"){

					var popUrl = "/tax/tax/PdfinputSum/" + $("input[name='start_date']").val() + "/" + $("input[name='end_date']").val() + "/" + $("select[name='srtCondition'] option:selected").val();	//팝업창에 출력될 페이지 URL
					var popOption = "width=740, height=720, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
				}else if( activeTabPdf == "saleSum" ){
				
					var popUrl = "/tax/tax/PdfsaleSum/" + $("input[name='start_date']").val() + "/" + $("input[name='end_date']").val() + "/" + $("select[name='srtCondition'] option:selected").val();	//팝업창에 출력될 페이지 URL
					var popOption = "width=740, height=720, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
				}else{

					var popUrl = "/tax/tax/PdfyearSum/" + $("input[name='start_date_year']").val() + "/" + $("input[name='end_date_year']").val() ;	//팝업창에 출력될 페이지 URL
					var popOption = "width=740, height=720, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
					window.open(popUrl,"",popOption);
				}
			}else{
				return false;
			}	
		});

		// 기존열린 탭들(거래처선택 탭제외)은 모두 삭제
		$('#modal_configTax').on('hidden.bs.modal', function () {
			$("input[name='textPercenter']").val("");
		});

		// 일일판매 입력
		// 한려수산에서 제거 요청
		$("#btnChgTax").click(function(){			
			$("#modal_configTax").modal({show:true}); 
		});

		$("#btnChangeTax").click(function(){
			
			$.getJSON('/tax/tax/setChangeDeclaration', {
				_token			: '{{ csrf_token() }}',
				PERCENTER		: $("input[name='textPercenter']").val(),
				YEAR			: $("input[name='start_date']").val().substr(0, 4),
			}).success(function(xhr){
				$('#modal_configTax').modal("hide"); 
			}).error(function(xhr,status, response) {

				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('.edit_alert');
				info.hide().find('ul').empty();
				if( error.result == "DB_ERROR"){
					info.find('ul').append('<li>DB Error!! 관리자에게 문의 바랍니다.</li>');
				}else{
					for(var k in error.message){
						info.find('ul').append('<li>' + k + '</li>');
						if(error.message.hasOwnProperty(k)){
							error.message[k].forEach(function(val){
								info.find('ul').append('<li>' + val + '</li>');
							});
						}
					}
				}
				info.slideDown();
			});
		});
	});
</script>
<div id="pdf">
	<form name="getPdf" method="post" action="/tax/tax/PdfSumSale" >
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<input type="hidden" name="WRITE_DATE_START" />
		<input type="hidden" name="WRITE_DATE_END" />
	</form>
</div>
<!-- 일일판매 입력 팝업 -->
<div class="modal fade" id="modal_configTax" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 35px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 비율변경 </h4>
			</div>
			<div class="modal-body" style="padding:20px 35px;">
				<div class="edit_alert"><ul></ul></div>
				<div class="box box-primary">
					<div class="box-body">						
						<div class="control-group">
							<div class="col-md-12 col-xs-12">
								<input type="text" name="textPercenter" placeholder="비율 입력" style="width:40%" />%
								<button type="button" class="btn btn-info pull-right" id="btnChangeTax">비율변경</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-default pull-right " data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 
				</button>
			</div>
		</div>
	</div>
</div>
@stop
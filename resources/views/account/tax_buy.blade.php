@extends('layouts.main')
@section('class','매입관리')
@section('title','매입세금계산서 등록')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>

	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
	.form-group.has-warning label {
		color: #f39c12;
		margin-top: 6px;
	}
</style>

<div class="col-md-12 headerSearch">
	<div class="box box-primary">
		<!--
		<div class="box-header">
			<h3 class="box-title"><i class="fonti um-search-minus"></i> 검색</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			</div>
		</div>
		-->
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-2 col-xs-4">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					
				</select>
			</div>

			<div class="col-md-2 col-xs-4">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력" value="{{ Request::Input('textSearch') }}">
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="form-group has-warning">
					<label class="control-label" for="inputWarning"><i class="fa fa-bell-o"></i>거래 업체를 먼저 선택하신 후 계산서를 작성하십시오</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">

			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="거래처 목록" width="100%">
				<caption>거래처 목록(매입처)</caption>
				<thead>
					<tr>
						<th class="check" width="15px"></th>
						<th class="subject" width="80px">그룹</th>
						<th class="subject" width="80px">상호</th>
						<th class="subject" width="60px">대표자</th>
						<th class="subject" width="35px">지역</th>
						<th class="subject" width="35px">전화번호</th>
						<th class="subject" width="35px">사업자번호</th>
						<th class="subject"></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
		

		var isBack		= localStorage["IS_BACK"];
		var sText		= '';
		var sCondition	= 'ALL';

		if( isBack == "Y"){
			$("input[name='textSearch']").val(localStorage["SEARCH_TEXT"]);
			$("select[name='srtCondition'] > option[value='"+ localStorage["SEARCH_CONDITION"] + "']").prop("selected", true);
			
		}

		$.getJSON('/cust/cust/getCustGrp', {
			//SUJO_NO : arr_code[0] ,
			_token : '{{ csrf_token() }}'
		},function(xhr){
			var data = jQuery.parseJSON(JSON.stringify(xhr));
			//console.log(data);
			if (data.length > 0) {
				$("select[name='srtCondition']").html("<option value='ALL'>전체</option>");
				
				$.each(data, function(i){
					if( isBack == "Y"){
						if( this.CUST_GRP_CD == localStorage["SEARCH_CONDITION"] ) {
							$("select[name='srtCondition']").append("<option selected='selected' value='" + data[i].CUST_GRP_CD+ "'>" + data[i].CUST_GRP_NM + "</option>");
						}else{
							$("select[name='srtCondition']").append("<option value='" + data[i].CUST_GRP_CD+ "'>" + data[i].CUST_GRP_NM + "</option>");
						}
					}else{
						$("select[name='srtCondition']").append("<option value='" + data[i].CUST_GRP_CD+ "'>" + data[i].CUST_GRP_NM + "</option>");
					}
				});
				localStorage["IS_BACK"]		= "N";
			}
		});

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 100,		// 기본 100개 조회 설정
			lengthChange: false,
			bInfo:false,
			ajax: {
				url: "/tax/tax/getIndexSaleCust",
				data:function(d){
					d.srtCondition	= isBack == "Y" ? localStorage["SEARCH_CONDITION"] : $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
					d.corp_div	= 'P';
				}
			},
			order: [[ 2, 'asc' ]],
			columns: [
				{
					data:   "CUST_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'CUST_GRP_NM', name: 'CUST_GRP_NM' },
				{ data: 'FRNM', name: 'FRNM' },
				{ data: 'RPST', name: 'RPST' },
				{ data: 'AREA_NM', name: 'AREA_NM' , className: "dt-body-center" },
				{ data: 'PHONE_NO', name: 'PHONE_NO' , className: "dt-body-center" },
				{ data: 'ETPR_NO', name: 'ETPR_NO' , className: "dt-body-center" },
				{
					"className":      'dt-body-left',
					"orderable":      false,
					"data":           "<button class='btn btn-success btnGoDetail' ><span class='glyphicon glyphicon-on'></span>선택</button>",
					"defaultContent": "<button class='btn btn-success btnGoDetail' ><span class='glyphicon glyphicon-on'></span>선택</button>"
				},

			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
			localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
			localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
		});
		$("#search-btn").click(function(){
			oTable.draw() ;
			localStorage["SEARCH_TEXT"]		= $("input[name='textSearch']").val();
			localStorage["SEARCH_CONDITION"]= $("select[name='srtCondition'] option:selected").val();
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
			
		$('#tblList tbody').on( 'click', 'button', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/tax/tax/detail/" + data.CUST_MK;
		});

		
		
	});
</script>
@stop
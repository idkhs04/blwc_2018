<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>매입계산서 합계</title>
		<style>
			caption{
				display:none;
			}
			@font-face{
				font-family:NanumGothic;  
				src: url("{{ public_path('font/NanumGothic.ttf') }}");
			}

			body { font-family:'NanumGothic', '나눔고딕', 'dotum', '돋움'; font-size:14px; }
			
			table > tr > td { border-top:1px solid block;border-bottom:1px solid block;padding:0 3px 0 3px;}
			td { height:20px;}
			table {
				border-collapse: collapse;
			}

			table, th, td {
				border-bottom: 1px solid black;
			}

			span.title { text-align:center;}

		</style>
			
	</head>

	<body>
		<script type="text/php">

			if ( isset($pdf) ) {

				$size = 10;
				$color = array(0,0,0);
				if (class_exists('Font_Metrics')) {
					$font = Font_Metrics::get_font("NanumGothic");
					$text_height = Font_Metrics::get_font_height($font, $size);
					$width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
				} elseif (class_exists('Dompdf\\FontMetrics')) {
					$font = $fontMetrics->getFont("NanumGothic");
					$text_height = $fontMetrics->getFontHeight($font, $size);
					$width = $fontMetrics->getTextWidth("Page 1 of 2", $font, $size);
				}

				$foot = $pdf->open_object();

				$w = $pdf->get_width();
				$h = $pdf->get_height();

				// Draw a line along the bottom
				$y = $h - $text_height - 24;
				$pdf->line(16, $y, $w - 16, $y, $color, 0.5);

				$pdf->close_object();
				$pdf->add_object($foot, "all");

				$text = "{PAGE_COUNT} - {PAGE_NUM}";  

				// Center the text
				$pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);

			}
		</script>
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<span class="title" style='text-align:center;font-size:23px;display:block;text-decoration: underline;'>{{ $title }}</span> 
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style="text-align:right"> 출력일자: {{ date("Y-m-d") }} </td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:5px;">
						<tr>
							<td style="text-align:left;">기간 : {{$start}} ~ {{$end }}</td>
							<td style="text-align:right;font-size:16px;text-decoration: underline;">{{ $corpnm }}</td>
						</tr>
					</table>

					<table id="tblList" summary="매입계산서 합계" width="100%" style="border-bottom:1px solid block;" border="1" cellpadding="0" cellspacing="0">
						<caption>매입계산서 합계</caption>
						
						<tr>
							<td style='text-align:center;' >번 호</td>
							<td style='text-align:center;''>거  래 처 명</td>
							<td style='text-align:center;''>사업자등록번호</th>
							<td style='text-align:center;''>매입계산서 금액</th>
							<td style='text-align:center;''>매 입 (건)</th>
							<td style='text-align:center;''>비 고</th>
						</tr>
						<tr>
							<td colspan="6" style="height:2px;"></td>
						</tr>
					
						@foreach ($model as $key => $item)
						<tr>
							<td style='text-align:center;'>{{ ++$key }}</td>
							<td style='text-align:left;padding-left:3px;'>{{ $item->FRNM}}</td>
							<td style='text-align:center;padding-left:3px;'>{{ $item->ETPR_NO}}</td>
							<td style='text-align:right;padding-right:3px;'>{{ number_format($item->INPUT)}}</td>
							<td style='text-align:right;padding-right:3px;'>{{ number_format($item->INPUT_CNT)}}</td>
							<td></td>
						</tr>
						@endforeach
						<tr>
							<td style='text-align:center;'>합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계</td>
							<td style='text-align:right;'></td>
							<td style='text-align:right;'></td>
							<td style='text-align:right;padding-right:3px;'>{{ number_format($sumInput) }}</td>
							<td style='text-align:right;padding-right:3px;'>{{ number_format($sumInputQty) }}</td>
							<td style='text-align:right;'></td>
						</tr>
						
					</table>
				</div>
			</div>
		<!-- 본문  -->
		</div>
	</body>
</html>

	



<header class="main-header">
	<!-- Logo -->
	<a href="/" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>B</b>LWC</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>활어유통시스템</b></span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<h1> @yield('title')</h1>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				
				<li class="dropdown messages-menu">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" onclick="startIntro();">
						<i class="fa fa-question-circle"></i>
						<span class="label label-danger rqst_error_cnt"></span>
					</a>
				</li>

				<!-- Messages: style can be found in dropdown.less-->          
				<li class="dropdown messages-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-envelope-o"></i>
						<span class="label label-danger rqst_error_cnt">0</span>
					</a>
					<ul class="dropdown-menu feedBacList">
						<li class="header">메시지 확인중..</li>
						<li class="footer"><a href="/config/index">모든 오류/개선 조회</a></li>
					</ul>
				</li>
				<!-- Notifications: style can be found in dropdown.less -->
				<!--
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">N</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">세금계산서 연동작업 후 활성화 예정</li>
						
						<li>
							
							<ul class="menu">
								<li>
									<a href="#"><i class="fa fa-users text-aqua"></i> 5 new members joined today</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
										page and may cause design problems
									</a>
								</li>
								<li><a href="#"><i class="fa fa-users text-red"></i> 5 new members joined </a></li>
								<li><a href="#"><i class="fa fa-shopping-cart text-green"></i> 25 sales made</a></li>
								<li> <a href="#"><i class="fa fa-user text-red"></i> You changed your username</a></li>
							</ul>
						</li>
						<li class="footer"><a href="#">View all</a></li>
						
					</ul>
				</li>
				-->
				<!-- Tasks: style can be found in dropdown.less -->
				<!--
				<li class="dropdown tasks-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-flag-o"></i>
						<span class="label label-danger">N</span>
					</a>
					<ul class="dropdown-menu">
					<li class="header">9개(차후 필요한 부 시스템 반영 예정입니다)</li>
					<li>
						
						<ul class="menu">
						  <li>
							<a href="#">
							  <h3>
								Design some buttons
								<small class="pull-right">20%</small>
							  </h3>
							  <div class="progress xs">
								<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">20% Complete</span>
								</div>
							  </div>
							</a>
						  </li>
						
						  <li>
							<a href="#">
							  <h3>
								Create a nice theme
								<small class="pull-right">40%</small>
							  </h3>
							  <div class="progress xs">
								<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">40% Complete</span>
								</div>
							  </div>
							</a>
						  </li>
					
						  <li>
							<a href="#">
							  <h3>
								Some task I need to do
								<small class="pull-right">60%</small>
							  </h3>
							  <div class="progress xs">
								<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">60% Complete</span>
								</div>
							  </div>
							</a>
						  </li>
					
						  <li>
							<a href="#">
							  <h3>
								Make beautiful transitions
								<small class="pull-right">80%</small>
							  </h3>
							  <div class="progress xs">
								<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
								  <span class="sr-only">80% Complete</span>
								</div>
							  </div>
							</a>
						  </li>
						 
						</ul>
					  </li>

					  <li class="footer">
						<a href="#">View all tasks</a>
					  </li>
				</ul>
			  </li>
			 -->
		<!-- User Account: style can be found in dropdown.less -->

		@if(Auth::guest())
		<!--
		<li class="dropdown user user-menu">
			<a href="/register">
				<i class="fa fa-whatsapp text-warning"></i>
				<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
				<span class="hidden-xs">사용가입(준비중)</span>
			</a>
		</li>
		 -->
		@endif
		<li class="dropdown user user-menu">
			@if(Auth::guest())
			<a href="/member/login">
				<i class="fa fa-circle text-warning"></i>
				<!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
				<span class="hidden-xs">로그인</span>
			</a>

				
			@else
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
				<i class="fa fa-circle text-success"></i>
				<span class="hidden-xs">
					{{ Auth::user()->corp_info->FRNM }} <strong>{{Auth::user()->NAME}}</strong>
					<span class='adminAuthName'><i class='fa fa-sign-in'></i></span>
				</span>

			</a>
			<ul class="dropdown-menu">
			  <!-- User image -->
			  <li class="user-header">
				<!--<img src="http://placehold.it/100x100" class="img-circle" alt="User Image">-->
				<p >
				   Online{{Auth::user()->NAME}}
				  <!-- <small>Member since Nov. 2012</small> -->
				</p>
			  </li>
			  <!-- Menu Body -->
			  <!-- <li class="user-body">
								<div class="row">
								  <div class="col-xs-4 text-center">
									<a href="#">Followers</a>
								  </div>
								  <div class="col-xs-4 text-center">
									<a href="#">Sales</a>
								  </div>
								  <div class="col-xs-4 text-center">
									<a href="#">Friends</a>
								  </div>
								</div> 
				
			  </li>
				-->
			  <!-- Menu Footer-->
			  <li class="user-footer">
				<div class="pull-left">
				  <a href="#" class="btn btn-default btn-flat">프로필</a>
				</div>
				<div class="pull-right">
				  <a href="/member/logout" class="btn btn-default btn-flat">로그아웃</a>
				</div>
			  </li>
			</ul>
			@endif
		</li>
		<!-- Control Sidebar Toggle Button -->
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li>
			</ul>
		</div>
	</nav>
</header>
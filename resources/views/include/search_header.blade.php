<div class="col-md-12 dvMainBtnSet" >
	<div class="box box-primary">
		<div class="box-body menu nav_menu_wapper">
			<ul>
			{{-- 회원관리 시작 --}}
			@if( strtolower(Request::segment(1)) == "corp")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@role('sysAdmin')
				<li><a class="btn btn-app" href="/corp/cust_grp_info/insert" id="btnAddCorp"><i class="fa fa-plus"></i>추가</a></li>
				@endrole
				<li><a class="btn btn-app" id="btnUpdateCorp"><i class="fa fa-edit"></i>수정</a></li>				
				@role('sysAdmin')
				<li><a class="btn btn-app" id="btnSetCopyCorp"><i class="fa  fa-copy"></i>업체복사</a></li>
				@endrole

				<li><a class="btn btn-app" id="btnSetBackup"><i class="fa fa-history"></i>백업/삭제</a></li>

				{{--<li><a class="btn btn-app" id="btnDeleteCorp"><i class="fa fa-remove"></i>삭제</a></li>--}}
			@endif
			@if( strtolower(Request::segment(1)) == "group")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAddGroup"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateGroup"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteGroup"><i class="fa fa-remove"></i>삭제</a></li>
			@endif
			@if( strtolower(Request::segment(1)) == "member")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@if( Session::has("CORP_MK"))
					<li><a class="btn btn-app" href="javascript:;" id="btnAddMember"><i class="fa fa-user-plus"></i>추가</a></li>
				@endif
				@if( strtolower(Request::segment(3)) == "index" || strtolower(Request::segment(3)) == "detail" || strtolower(Request::segment(3)) == "salary")
					<li><a class="btn btn-app" id="btnUpdateMember"><i class="fa fa-edit"></i>수정</a></li>
				@endif
				<li><a class="btn btn-app" id="btnDeleteMember"><i class="fa fa-user-times"></i>삭제</a></li>
			@endif
			{{-- 회원관리 끝 --}}

			{{-- 비용관리 시작 --}}
			@if( strtolower(Request::segment(1)) == "chitinfo")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>				
				<li><a class="btn btn-app" id="btnAddAccountGrp"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateAccountGrp"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteAccountGrp"><i class="fa fa-remove"></i>삭제</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				
			@endif
			@if( strtolower(Request::segment(1)) == "aidlist")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				{{--<li><a class="btn btn-app" id="btnAddCust"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateCust"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteCust"><i class="fa fa-remove"></i>삭제</a></li>--}}
			@endif
			@if( strtolower(Request::segment(1)) == "accountgrplist")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>

				<!-- <li><a class="btn btn-app" id="btnAddCust"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateCust"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteCust"><i class="fa fa-remove"></i>삭제</a></li> -->
			@endif
			@if( strtolower(Request::segment(1)) == "bringamtgrant")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAddBringamtgrant"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateBringamtgrant"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteBringamtgrant"><i class="fa fa-remove"></i>삭제</a></li>

				<li><a class="btn btn-app btnClose" href="javascript:;"><i class="fa fa-refresh"></i>마감</a></li>
			@endif
			{{-- 비용관리 끝 --}}

			{{-- 코드관리 시작 --}}
			@if( strtolower(Request::segment(1)) == "cust")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@if( strtolower(Request::segment(3)) != "log")
				<li><a class="btn btn-app" id="btnAddCust"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateCust"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteCust"><i class="fa fa-remove"></i>삭제</a></li>
				@endif
			@endif
			@if( strtolower(Request::segment(1)) == "custcd")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@if( strtolower(Request::segment(3)) != "log")
				<li><a class="btn btn-app" id="btnAddCustCd"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateCustCd"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" href="#" id="btnDeleteCust"><i class="fa fa-remove"></i>삭제</a></li>
				@endif
			@endif
			@if( strtolower(Request::segment(1)) == "accountgrp")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAddAccountGrp"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateAccountGrp"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteAccountGrp"><i class="fa fa-remove"></i>삭제</a></li>
			@endif
			@if( strtolower(Request::segment(1)) == "accountcd")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAddAccountCd"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnUpdateAccountCd"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDeleteAccountCd"><i class="fa fa-remove"></i>삭제</a></li>
			@endif
			{{-- 코드관리 끝 --}}


			{{-- 수조 관리 --}}
			@if( strtolower(Request::segment(1)) == "buy")
				@if( strtolower(Request::segment(3)) == "index")
				<li><button type='button' id="btnAddSujo" class='btn btn-warning btnInSujo pull-right'><i class="fa fa-plus-circle"></i>수조추가</button></li>	
				<li><button type='button' id="btnLog" class='btn btn-warning btnInSujo'><i class="fa  fa-code"></i>이력보기</button></li>	
				<!--
				<li><a class="btn btn-app" id="btnAddSujoIn" href="javascript:;"><i class="fa fa-plus"></i>입고</a></li>
				<li><a class="btn btn-app" id="btnUpdPrice" href="javascript:;"><i class="fa fa-krw"></i>단가수정</a></li>
				<li><a class="btn btn-app" id="btnMoveSujo" href="javascript:;"><i class="fa fa-refresh"></i>이고</a></li>
				<li><a class="btn btn-app" href="#" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>	
				-->
				@endif
				@if( strtolower(Request::segment(3)) == "log")	
				@endif
				
			@endif
			
			
			{{-- 어종별 재고관리 --}}
			@if( strtolower(Request::segment(1)) == "stock")					
				
				@if( strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>어종목록</a></li>
				<li><a class="btn btn-app" id="btnStockUpdate" href="javascript:;" data-target="#modal-lg"><i class="fa fa-wrench"></i>입고수정</a></li>
				<li><a class="btn btn-app" id="btnStockArranage" href="javascript:;"><i class="fa fa-circle-o-notch"></i>재고정리</a></li>
				<li><a class="btn btn-app" id="btnUpdate" href="javascript:;"><i class="fa fa-edit"></i>재고수정</a></li>
				<li><a class="btn btn-app" id="btnDelete" href="javascript:;"><i class="fa fa-remove"></i>삭제</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;" id="btnPdf"><i class="fa fa-file-pdf-o"></i>상세재고출력</a></li>
				<li><a class="btn btn-app" id="btnLog" href=""><i class="fa  fa-code"></i>이력보기</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "detaileditarr" || strtolower(Request::segment(3)) == "detailedit" )
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@endif
			@endif


			{{-- 매입계산서 등록 --}}
			@if( strtolower(Request::segment(1)) == "tax")
				
				@if( strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/indexBuy?page=1"><i class="fa fa-list"></i>거래처목록</a></li>
				<li><a class="btn btn-app" id="btnAddTax" href="javascript:;"><i class="fa fa-plus-circle"></i>등록</a></li>	
				<li><a class="btn btn-app" id="btnUpdate" href="javascript:;"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" href="#" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
				@endif
				
				@if( strtolower(Request::segment(3)) == "detailsale")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/indexSale?page=1"><i class="fa fa-list"></i>거래처목록</a></li>
				<li><a class="btn btn-app" id="btnAddTax" href="javascript:;"><i class="fa fa-plus-circle"></i>등록</a></li>	
				<li><a class="btn btn-app" id="btnUpdate" href="javascript:;"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" href="#" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
				<li><a class="btn btn-app btnPdf white" href="javascript:;"><i class="fa fa-file-pdf-o"></i>백지출력</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>Pdf출력</a></li>
				<li><a class="btn btn-app btnExcel" id="btnExcel" href="javascript:;"><i class="fa fa-file-excel-o"></i>Excel출력</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "indexsale")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/indexBuy?page=1"><i class="fa fa-list"></i>거래처목록</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "list")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/list"><i class="fa fa-list"></i>목록</a></li>
				<!--<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>-->
				@endif

				@if( strtolower(Request::segment(3)) == "indexbuy")
				<!--<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/list"><i class="fa fa-list"></i>목록</a></li>-->
				@endif

				@if( strtolower(Request::segment(3)) == "elist")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/elist"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id='btnInsert' href="javascript:;"><i class="fa fa-plus-circle"></i>작성</a></li>
				<li><a class="btn btn-app" id='btnUpdate' href="javascript:;"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id='btnSendTax' href="javascript:;"><i class="fa fa-send"></i>송신</a></li>
				<li><a class="btn btn-app" id='btnReceiveTax' href="javascript:;"><i class="fa fa-reply"></i>수신</a></li>
				 
				<li><a class="btn btn-app" href="#" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/elist"><i class="fa fa-list"></i>수신</a></li>
				@endif
			@endif
			

			{{-- 일일판매관리 --}}
			@if( strtolower(Request::segment(1)) == "sale")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				
				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app" href="javascript:;" id="btnAddSale"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnLog" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/log"><i class="fa  fa-code"></i>이력보기</a></li>
				<li><a class="btn btn-app" id="btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>판매일보</a></li>
				@endif

				@if( strtolower(Request::segment(3)) === "detaillist")
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>거래명세서출력</a></li>
				<li><a class="btn btn-app" id="btnLog" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/log"><i class="fa  fa-code"></i>이력보기</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "tax")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" href="javascript:;" id="btnAddSale"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" href="#" id="btnUpdate"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnLog" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/log"><i class="fa  fa-code"></i>이력보기</a></li>
				@endif
				@if( strtolower(Request::segment(3)) == "log")	
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@endif
			@endif


			{{-- 결산관리 --}}
			@if( strtolower(Request::segment(1)) == "closing")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>

				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app" href="javascript:;" id="btnSaveClosing"><i class="fa fa-plus"></i>결산저장</a></li>
				<li><a class="btn btn-app" href="javascript:;" id="btnList"><i class="fa fa-edit"></i>기간별 결산표</a></li>
				@endif
				
				@if( strtolower(Request::segment(3)) == "detaillistarrange" || strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif
			@endif


			{{-- 기간별판매현황 --}}
			@if( strtolower(Request::segment(1)) == "stcs")
				
				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "listcustsaledetail")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/listCustSale"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif
				
				@if( strtolower(Request::segment(3)) == "listpissale")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/listPisSale"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif
				
				@if( strtolower(Request::segment(3)) == "listcustinputdetail")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/listCustInput"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "listpisinput")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/listPisInput"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				@endif
				
			@endif
			
			
			{{-- 미수금관리 --}}
			@if( strtolower(Request::segment(1)) == "uncl")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>추가</a></li>
				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app btnPdf" href="javascript:;"><i class="fa fa-file-pdf-o"></i>수금용출력</a></li>
				<li><a class="btn btn-app btnPdfDetail" href="javascript:;"><i class="fa fa-file-pdf-o"></i>상세 출력</a></li>
				@endif
				@if( strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app btnPdfDetail" href="javascript:;"><i class="fa fa-file-pdf-o"></i>미수장부 출력</a></li>
				@endif
			@endif


			{{-- 미지급금관리 --}}
			@if( strtolower(Request::segment(1)) == "unprov")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app" id="btnLog" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/log"><i class="fa  fa-code"></i>이력보기</a></li>
				@endif
				@if( strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>추가</a></li>
				<li><a class="btn btn-app btnPdfDetail" href="javascript:;"><i class="fa fa-file-pdf-o"></i>출력</a></li>
				<li><a class="btn btn-app" id="btnLog" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/log"><i class="fa  fa-code"></i>이력보기</a></li>
				@endif
			@endif

			@if( strtolower(Request::segment(1)) == "pis")
				@if( strtolower(Request::segment(3)) == "index")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>어종추가</a></li>
				<li><a class="btn btn-app" id="btnUpdate"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
				@endif

				@if( strtolower(Request::segment(3)) == "detail")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>어종목록</a></li>
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>규격추가</a></li>
				<li><a class="btn btn-app" id="btnUpdate"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
				@endif

			@endif
			@if( strtolower(Request::segment(1)) == "piscls")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
				<li><a class="btn btn-app" id="btnAdd"><i class="fa fa-plus"></i>추가</a></li>			
				<li><a class="btn btn-app" id="btnUpdate"><i class="fa fa-edit"></i>수정</a></li>
				<li><a class="btn btn-app" id="btnDelete"><i class="fa fa-remove"></i>삭제</a></li>
			@endif

			@if( strtolower(Request::segment(1)) == "config")
				<li><a class="btn btn-app" href="/{{ Request::segment(1) }}/index?page=1"><i class="fa fa-list"></i>목록</a></li>
			@endif
			
			</ul>
		</div>
	</div>
</div>
<div class="col-md-12 main_alertMsg"></div>
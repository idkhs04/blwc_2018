<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!--
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		-->
		<ul class="sidebar-menu">
			<li class="treeview @if( Request::segment(1) == 'manage' || Request::segment(1) == 'corp'|| Request::segment(1) == 'group'  || Request::segment(1) == 'member') active @endif">
				<a href="javascript:void(0);" >
					<i class="fa fa-users"></i>
					<span>회원관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu @if( Request::segment(1) == 'corp') active @endif">
					<li class="@if( Request::segment(1) == 'corp') active @endif" >
						<a href="/corp/cust_grp_info/index"><i class="fa fa-circle-o"></i> 업체정보</a></li>
					<li class="@if( Request::segment(1) == 'group') active @endif" >
						<a href="/group/cust_grp_info/index"><i class="fa fa-circle-o"></i> 그룹정보</a></li>
					@role('sysAdmin')
					<li class="@if( Request::segment(1) == 'group') active @endif" >
						<a href="/entrust-gui/users"><i class="fa fa-circle-o"></i> 그룹권한정보</a></li>
					@endrole
					<li class="@if( Request::segment(1) == 'member') active @endif" >
						<a href="/member/cust_grp_info/index"><i class="fa fa-circle-o"></i> 사원정보</a></li>
				</ul>
			</li>
			<li class="treeview @if( (Request::segment(1) == 'sujo'|| Request::segment(1) == 'stock' || Request::segment(1) == 'buy' || Request::segment(1) == 'sale' && ( Request::segment(3) == 'index' || Request::segment(3) == 'detailList' || Request::segment(3) == 'detailForm' || Request::segment(3) == 'detailReturn' || Request::segment(3) == 'log'))  ) active @endif">
				<a href="#">
					<i class="fa fa-opencart"></i>
					<span>매입/매출 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="@if( Request::segment(1) == 'buy' && (  Request::segment(3) == 'index'|| Request::segment(3) == 'log' || Request::segment(3) == 'detailEdit')) active @endif">
						<a href="/buy/sujo/index?page=1"><i class="fa fa-circle-o"></i> 수조별 입고</a></li>
					<li class="@if( Request::segment(1) == 'sale' && ( Request::segment(3) == 'index' || Request::segment(3) == 'detailList' || Request::segment(3) == 'detailForm' || Request::segment(3) == 'detailReturn' || Request::segment(3) == 'log' )) active @endif">
						<a href="/sale/sale/index" ><i class="fa fa-circle-o"></i> 일일판매관리</a></li>
					<li class="@if( Request::segment(1) == 'stock' && ( Request::segment(3) == 'listPis')) active @endif">
						<a href="/stock/stock/listPis" ><i class="fa fa-circle-o"></i> 일일입고관리</a></li>
					<li class="@if( Request::segment(1) == 'stock' && ( Request::segment(3) == 'index'|| Request::segment(3) == 'detail')) active @endif">
						<a href="/stock/sujo/index"><i class="fa fa-circle-o"></i> 어종별 재고관리</a></li>
					<li class="@if( Request::segment(1) == 'stock' &&  Request::segment(3) == 'listAllData') active @endif">
						<a href="/stock/stock/listAllData"><i class="fa fa-circle-o"></i> 전체 입출고조회</a></li>
				</ul>
			</li>

			<li class="treeview @if( ( Request::segment(1) == 'uncl' || Request::segment(1) == 'unprov' || Request::segment(1) == 'closing' ) || (Request::segment(1) == 'stcs' && Request::segment(3) == 'index')  ) active @endif" >
				<a href="#">
					<i class="fa fa-bar-chart-o fa-fw"></i>
					<span>미수/결산 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					
					<li class="@if( Request::segment(1) == 'uncl') active @endif">
						<a href="/uncl/uncl/index"><i class="fa fa-circle-o"></i> 매출 미수 정보</a></li>
					<li class="@if( Request::segment(1) == 'unprov') active @endif">
						<a href="/unprov/unprov/index"><i class="fa fa-circle-o"></i> 매입 미지급 정보</a></li>

					<li class="@if( Request::segment(1) == 'closing' && ( Request::segment(3) == 'index' || Request::segment(3) == 'detail' || Request::segment(3) == 'detailListArrange' )) active @endif">
						<a href="/closing/closing/index"><i class="fa fa-circle-o"></i> 결산표</a></li>

					<li class="@if( Request::segment(1) == 'stcs' && ( Request::segment(3) == 'index')) active @endif">
						<a href="/stcs/stcs/index"><i class="fa fa-circle-o"></i> 기간별 판매현황</a></li>
				</ul>
			</li>
			<li class="treeview @if( Request::segment(1) == 'chitinfo' || Request::segment(1) == 'aidlist' ||  Request::segment(1) == 'accountgrplist' || 'bringamtgrant' ==  Request::segment(1)) active @endif" >
				<a href="#">
					<i class="fa fa-krw"></i>
					<span>비용 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					
					<li class="@if( Request::segment(1) == 'chitinfo' && Request::segment(3) == 'index') active @endif">
						<a href="/chitinfo/cust_grp_info/index"><i class="fa fa-circle-o"></i> 금전출납부관리</a></li>
					@role('CashBook')
					<li class="@if( Request::segment(1) == 'chitinfo' && Request::segment(3) == 'index2') active @endif" >
						<a href="/chitinfo/cust_grp_info/index2"><i class="fa fa-circle-o"></i> 회비관리</a></li>

					<li class="@if( Request::segment(1) == 'chitinfo'  && Request::segment(3) == 'index3') active @endif" >
						<a href="/chitinfo/cust_grp_info/index3"><i class="fa fa-circle-o"></i> 펌프대장관리</a></li>
					@endrole
					
					
					<li class="@if( (Request::segment(1) == 'aidlist' ) ) active @endif">
						<a href="/aidlist/cust_grp_info/index"><i class="fa fa-circle-o"></i> 계정별 조회</a></li>
					<li class="@if( (Request::segment(1) == 'accountgrplist' ) ) active @endif">
						<a href="/accountgrplist/cust_grp_info/index"><i class="fa fa-circle-o"></i> 계정그룹별 조회</a></li>
					<!--
					<li class="@if( (Request::segment(1) == 'bringamtgrant' ) ) active @endif">
						<a href="/bringamtgrant/cust_grp_info/index"><i class="fa fa-circle-o"></i> 이월금 정보</a></li>
					-->
				</ul>
			</li>
			<li class="treeview @if( 'bringamtgrant' ==  Request::segment(1) || ( Request::segment(1) == 'tax' && ( Request::segment(3) == 'list' || Request::segment(3) == 'elist' || Request::segment(3) == 'indexBuy')) || (Request::segment(1) == 'tax' && Request::segment(3) == 'listTaxCust') || (Request::segment(1) == 'tax' && Request::segment(3) == 'detailSale') || (Request::segment(1) == 'tax' && Request::segment(3) == 'indexSale') || (Request::segment(1) == 'tax'&& Request::segment(3) == 'detail') ) active @endif" >
				<a href="#">
					<i class="fa fa-calculator"></i>
					<span>계산서 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="@if( Request::segment(1) == 'tax' && Request::segment(3) == 'list') active @endif">
						<a href="/tax/tax/list"><i class="fa fa-circle-o"></i> 매입/매출계산서 합계</a></li>
					<li class="@if( (Request::segment(1) == 'tax' && Request::segment(3) == 'elist' ) ) active @endif">
						<a href="/tax/tax/elist"><i class="fa fa-circle-o"></i> 계산서 관리</a></li>
					<li class="@if( (Request::segment(1) == 'tax' && Request::segment(3) == 'listTaxCust' ) ) active @endif">
						<a href="/tax/tax/listTaxCust"><i class="fa fa-circle-o"></i> 년단위 계산서 관리 </a></li>
					<li class="@if( ((Request::segment(1) == 'tax') && ( Request::segment(3) == 'indexBuy' || Request::segment(3) == 'detail')) || ( Request::segment(1) == 'tax' && Request::segment(3) == 'detail')) active @endif">
						<a href="/tax/tax/indexBuy?page=1"><i class="fa fa-circle-o"></i> 매입계산서등록</a></li>

					<li class="@if( (Request::segment(3) == 'indexSale' || Request::segment(3) == 'detailSale' ) && ( Request::segment(1) == 'tax')) active @endif">
						<a href="/tax/tax/indexSale?page=1"><i class="fa fa-circle-o"></i> 매출 계산서 출력</a></li>
				</ul>
			</li>
			<li class="treeview @if( Request::segment(1) == 'cust'|| Request::segment(1) == 'custcd' || Request::segment(1) == 'accountgrp' || Request::segment(1) == 'accountcd' || Request::segment(1) == 'piscls' || Request::segment(1) == 'pis') active @endif">
				<a href="#">
					<i class="fa fa-edit"></i> <span>코드 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="@if( (Request::segment(1) == 'cust' ) ) active @endif">
						<a href="/cust/cust_grp_info/index"><i class="fa fa-circle-o"></i> 거래처그룹코드</a></li>
					<li class="@if( (Request::segment(1) == 'custcd' ) ) active @endif">
						<a href="/custcd/cust_grp_info/index"><i class="fa fa-circle-o"></i> 거래처코드</a></li>
					<li class="@if( (Request::segment(1) == 'accountgrp' ) ) active @endif">
						<a href="/accountgrp/cust_grp_info/index"><i class="fa fa-circle-o"></i> 계정그룹코드</a></li>
					<li class="@if( (Request::segment(1) == 'accountcd' ) ) active @endif">
						<a href="/accountcd/cust_grp_info/index"><i class="fa fa-circle-o"></i> 계정코드</a></li>
					<li class="@if( (Request::segment(1) == 'piscls' ) ) active @endif">
						<a href="/piscls/piscls/index"><i class="fa fa-circle-o"></i> 어종관리</a></li>
			
					<!--
					<li class="@if( (Request::segment(1) == 'pis' ) ) active @endif">
						<a href="/pis/pis/index"><i class="fa fa-circle-o"></i> 어종코드</a></li>
					-->
				
					
				</ul>
			</li>
			<li class="treeview @if( Request::segment(1) == 'stcs' && Request::segment(3) != 'index' ) active @endif">
				<a href="#">
					<i class=" fa fa-pie-chart"></i> <span>통계 관리</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="@if( (Request::segment(1) == 'stcs' ) && (Request::segment(3) == 'listCustSale' || Request::segment(3) == 'listCustSaleDetail' )) active @endif">
						<a href="/stcs/stcs/listCustSale"><i class="fa fa-circle-o"></i> 업체별 판매현황</a></li>
					<li class="@if( (Request::segment(1) == 'stcs' ) && (Request::segment(3) == 'listPisSale' )) active @endif">
						<a href="/stcs/stcs/listPisSale"><i class="fa fa-circle-o"></i> 어종별 판매현황</a></li>
					<li class="@if( (Request::segment(1) == 'stcs' && Request::segment(3) == 'listCustInput' ) || ( Request::segment(1) == 'stcs' && Request::segment(3) == 'listCustInputDetail')) active @endif">
						<a href="/stcs/stcs/listCustInput"><i class="fa fa-circle-o"></i> 업체별 매입현황</a></li>
					<li class="@if( ( Request::segment(1) == 'stcs' && Request::segment(3) == 'listPisInput') ) active @endif">
						<a href="/stcs/stcs/listPisInput"><i class="fa fa-circle-o"></i> 어종별 매입현황</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#">
					<i class=" fa  fa-question-circle"></i> <span>설명서</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<!--
					<li class="">
						<a href="/config/downMenual1"><i class="fa fa-circle-o"></i> 일반설명서</a></li>
					-->
					<li class="">
						<a href="/config/indexMenual"><i class="fa fa-circle-o"></i> 상세설명서</a></li>
					<li class="">
						<a href="/config/notice"><i class="fa fa-circle-o"></i> 개인정보취급방침</a></li>
				</ul>
			</li>
		</ul>

	</section>

	<!-- /.sidebar -->
</aside>
@extends('layouts.main')
@section('class','회원관리')
@section('title','업체정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
	ul.info li{ display:inline-block; width:45%;}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-7 col-xs-12">
				<div class="btn-group">
					<!-- System Admin 일 경우만 -->
					@role('sysAdmin')
					<button type="button" class="btn btn-info" id="btnAddCorp" ><i class="fa fa-plus-circle"></i> 추가</button>
					@endrole
					
					<button type="button" class="btn btn-success" id="btnUpdateCorp" ><i class="fa fa-edit"></i> 수정</button>
					<button type="button" class="btn btn-success" id="btnSetCopyCorp"><i class="fa fa-copy"></i> 업체복사</button>
					<button type="button" class="btn btn-success" id="btnGetExcel"><i class="fa fa-file-excel-o"></i> 데이터 엑셀백업 </button>

					<!-- Admin 사용자 일 경우만 -->
					@role('admin')
					<button type="button" class="btn btn-success" id="btnSetBackup"><i class="fa fa-history"></i> 백업/삭제 </button>
					@endrole('admin')
				</div>
			</div>

			<div class="col-md-2 col-xs-6">
				<select class="form-control" name="srtCondition">
					<option value="ALL">전체</option>
					<option value="FRNM">상호</option>
					<option value="CORP_MK">업체부호</option>
					<option value="RPST">대표자</option>
				</select>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="input-group">
					<input type="text" name="textSearch" class="form-control"  placeholder="검색어 입력 (상호, 업체부호, 대표자명...)" value="{{ Request::Input('textSearch') }}">
					
					<span class="input-group-btn">
						<button type="button" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="edit_alert"><ul></ul></div>
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="업체정보목록" width="100%">
				<caption>업체정보목록/caption>
				<thead>
					<tr>
						<th width="15px"></th>
						<th width="60px">업체구분</th>
						<th width="60px">업체부호</th>
						<th width="80px">상호</th>
						@role('sysAdmin')
						<th width="70px">관리자 접속</th>
						@endrole
						<th width="60px">대표자</th>
						<th width="50px">전화번호</th>
						<th width="30px">지역명</th>
						<th width="90px">사용자관리</th>
						<th width="60px">전자세금계산서 설정</th>
						<th width="auto">백업관리</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
	
		var start = moment().subtract(1, 'months');
		var end = moment();
		
		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			showCustomRangeLabel: false,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);

		function cbSetDate2(start, end) {
			$('#reportrange2 span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("#reportrange2 input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("#reportrange2 input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 입고정보 : 검색
		$("#reportrange2").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			showCustomRangeLabel: false,
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate2);
		cbSetDate2(start, end);

		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 25,		// 기본 10개 조회 설정
			bLengthChange: false,
			ajax: {
				url: "/corp/corp/listData",
				data:function(d){
					d.srtCondition	= $("select[name='srtCondition'] option:selected").val();
					d.textSearch	= $("input[name='textSearch']").val();
				}
			},
			order: [[ 1, 'DESC' ], [ 3, 'ASC' ]],
			columns: [
				{
					data:   "CORP_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<input type='checkbox' class='editor-active' value='" + data +"' >";
						}
						return data;
					},
					className: "dt-body-center"
				},
				{ data: 'CORP_DIV_NM', name: 'CORP_DIV_NM' },
				{
					data:   "CORP_MK",
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<a href='/corp/cust_grp_info/detail/"+data+"'>"+data+"</a>";
						}
						return data;
					},
					className: "dt-body-left"
				},
				{
					data:   'FRNM',
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<a href='/corp/cust_grp_info/detail/"+row.CORP_MK+"'>"+data+"</a>";
						}
						return data;
					},
					className: "dt-body-left"
				},
				@role('sysAdmin')
				{
					data:   null,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return "<button class='admLoginforce btn bg-maroon'><i class='fa fa-sign-in'></i> 접속</button>";
						}
						return data;
					},
					className: "dt-body-center"
				},
				@endrole
				{ data: 'RPST', name: 'RPST' ,className: "dt-body-center"},
				{ data: 'PHONE_NO', name: 'PHONE_NO' ,className: "dt-body-center"},
				{ data: 'AREA_NM', name: 'AREA_NM' ,className: "dt-body-center"},
				{
					data:   "ACTIVE",
					render: function ( data, type, row ) {

						if ( type === 'display' ) {
							
							if( row.ACTIVE == "1") {
								return  "<button class='btn btn-info btnAddUser'><i class='fa fa-user-plus'></i> 사용자추가</button>" + 
										" <button class='btn btn-success btnSetActive' value='0'><i class='fa fa-gear'></i> 사 용 중 &nbsp;&nbsp;[중지] </button>" ;
										
							}else{
								return  "<button class='btn btn-info btnAddUser'><i class='fa fa-user-plus'></i> 사용자추가</button>" + 
										" <button class='btn btn-danger btnSetActive' value='1'><i class='fa fa-gear'></i> 사용안함 [허용] </button>" ;
										
							}							
						}
						return data;

					},
					className: "dt-body-left"
				},
				{
					data:   "TAX_USE_YN",
					render: function ( data, type, row ) {

						if ( type === 'display' ) {							
							if ( row.TAX_USE_YN == "Y")
							{
								return " <button class='btn btn-success btnSetTax' value='N'><i class='fa fa-building-o'></i> 사용중&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[중지] </button>";
							}else {
								return " <button class='btn btn-danger btnSetTax' value='Y'><i class='fa fa-building-o'></i> 사용안함&nbsp;&nbsp;[사용] </button>";
							}
						}
						return data;

					},
					className: "dt-body-left"
				},
				{
					data:   "CORP_MK",
					render: function ( data, type, row ) {

						if ( type === 'display' ) {	
							return " <button class='btn btn-info btnGoBackup' data-corp='" + data + "'><i class='fa fa-building-o'></i> 백업관리 </button>";
						}
						return data;
					},
					className: "dt-body-left"
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("input[name='textSearch']").on("keyup", function(){
			oTable.search($(this).val()).draw() ;
		});
		$("select[name='srtCondition']").on("change", function(){
			oTable.ajax.data = {"srtCondition": $("select[name='srtCondition'] option:selected").val() , "textSearch" : $("input[name='textSearch']").val() }
			oTable.draw() ;
		});

		$("#search-btn").click(function(){
			oTable.draw() ;
		});

		/** 그리드 선택  **/
		$('#tblList tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", false);
			} else {
				oTable.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$(this).find("td > input[type=checkbox]").prop("checked", true);
			}
		});
		
		// 관리자의 업체계정 강제 로그인
		$('#tblList tbody').on( 'click', '.admLoginforce', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			localStorage['ADMIN_AUTH_CORP_NM'] = data.FRNM;
			location.href="/config/setLoginByAdmin/"+data.CORP_MK ;
		});
		
		// 백업조회
		$('#tblList tbody').on( 'click', 'button.btnGoBackup', function(){	
			
			var corp_mk = $(this).attr("data-corp");
			location.href="/corp/corp/backIdex/" + corp_mk;
		});

		// 업체추가
		$("#btnAddCorp").click(function(){
			location.href="/corp/cust_grp_info/insert";
		});

		// 업체정보수정
		$("#btnUpdateCorp").click(function(){

			var arr_code = [];
			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "수정할 항목을 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "수정은 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				window.location.href="/corp/cust_grp_info/update/"+arr_code[0]+"";
			}
		});

		// 회사 추가버튼
		$('#tblList tbody').on( 'click', 'button.btnAddUser', function () {
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href = "/member/cust_grp_info/insert/" + data.CORP_MK;
		});
		
		// 전자세금계산서 사용여부
		
		$('#tblList tbody').on( 'click', 'button.btnSetTax', function () {
			var row		= oTable.row( $(this).parents('tr') ).data();
			var tax_yn	= $(this).val();

			$.blockUI({ 
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() { 
					
					$.getJSON('/corp/corp/setCorpTax', {
						_token			: '{{ csrf_token() }}',
						CORP_MK			: row.CORP_MK ,
						TAX_USE_YN		: tax_yn ,
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							oTable.draw() ;
							$.unblockUI();
						}
						
					}).error(function(xhr,status, response) {
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						info.slideDown();
						oTable.draw() ;
						$.unblockUI();
					});
				}
			});

			
		});
		

		// 사용자 허가 
		$('#tblList tbody').on( 'click', 'button.btnSetActive', function () {

			var row		= oTable.row( $(this).parents('tr') ).data();
			var active	= $(this).val();
			$.blockUI({ 
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() { 
					
					$.getJSON('/corp/corp/setCorpActive', {
						_token			: '{{ csrf_token() }}',
						CORP_MK			: row.CORP_MK ,
						ACTIVE			: active ,
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							oTable.draw() ;
							$.unblockUI();
						}
						
					}).error(function(xhr,status, response) {
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						info.slideDown();
						oTable.draw() ;
						$.unblockUI();
					});
				}
			});
		});

		// 회사복사
		$("#btnSetCopyCorp").click(function(){
			
			var arr_code = [];
			var row		= oTable.row( $("#tblList tbody tr > td > input[type='checkbox']:checked").parents('tr') ).data();

			$('#tblList tbody > tr > td > input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					arr_code.push($(this).val());
				}
			});
			
			$(".content > .alert").remove();
			if( arr_code.length == 0){
				$(".content").prepend( getAlert('warning', '경고', "복사할 회사를 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length > 1){
				$(".content").prepend( getAlert('warning', '경고', "복사할 회사는 한개만 선택해주세요") ).show();
				return false;
			}

			if( arr_code.length == 1){
				$("#modal_corp_mk").val(row.CORP_MK)
				$("#modal_corp_nm").val(row.FRNM)
				$("#modal_copy_corp").modal({show:true});
			}
		});
	
		$('#modal_copy_corp').on('hidden.bs.modal', CbSetInitModalClose);

		// 복사실행
		$("#btnCopyCorpInfo").click(function(){
			
			$.blockUI({ 
				baseZ: 999999,
				message : "<img src='/image/ajax-loader2.gif' /> 처리중..",
				css : {
					backgroundColor: 'rgba(0,0,0,0.0)', //배경투명하게
					color: '#000000', border: '0px solid #a00' //테두리 없앰
				},
				onBlock: function() { 
					
					$.getJSON('/corp/corp/setCorpCopy', {
						_token			: '{{ csrf_token() }}',
						CORP_MK			: $("#modal_corp_mk").val() ,
						NEW_CORP_MK		: $("#modal_corp_new_mk").val() ,
						NEW_CORP_FRNM	: $("#modal_corp_new_nm").val() ,
						NEW_RPST		: $("#modal_corp_new_rpst").val() ,
						NEW_ETPR_NO		: $("#modal_corp_new_etpr_no").val()
						
					}).success(function(xhr){
						var data = jQuery.parseJSON(JSON.stringify(xhr));

						if (data.result == "success") {
							oTable.draw() ;
							$.unblockUI();

							$("#modal_copy_corp").modal("hide");
						}
						
					}).error(function(xhr,status, response) {
						var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
						var info = $('.edit_alert');
						info.hide().find('ul').empty();

						//console.log(error);
						if( error.result == 'duplicate' ){
							info.find('ul').append('<li>' + error.message + '</li>');
						
						}else{
						
							for(var k in error.message){
								if(error.message.hasOwnProperty(k)){
									error.message[k].forEach(function(val){
										info.find('ul').append('<li>' + val + '</li>');
									});
								}
							}
						}

						info.slideDown();
						//oTable.draw() ;
						$.unblockUI();
					});
				}
			});
		});

		// 백업/삭제 팝업
		$("#btnGetExcel").click(function(){
			$("#modal_Excel").modal({show:true});
		});

		// 백업 및 삭제 실행
		$("#btnGetBackupExcel").click(function(){
			$("input[name='START_DATE']").val($("input[name='start_date']").val());
			$("input[name='END_DATE']").val($("input[name='end_date']").val());

			$("#frmSetBackup").submit();
		});

		// 백업 및 삭제 버튼 클릭 (실행 화면 띄우기)
		$("#btnSetBackup").click(function(){	
			$("#modal_backup").modal({show:true});
		});

		$("#btnExcuteBackup").click(function(){
			var start_date	= $("#reportrange2 input[name='start_date']").val();
			var end_date	= $("#reportrange2 input[name='end_date']").val();
			
			$.post('/corp/corp/chkCorpBackup', {
				_token			: '{{ csrf_token() }}',				
				start_date		: start_date ,
				end_date		: end_date ,
				
			}).success(function(xhr){
				var data = jQuery.parseJSON(JSON.stringify(xhr));

				if (data.result == "success") {	
					//$("#modal_backup").modal("hide");
				}
				
			}).error(function(xhr,status, response) {
				var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
				var info = $('#modal_backup .edit_alert');
				info.hide().find('ul').empty();

				if( error.result == 'fail' ){

					console.log(error.message);
					$(error.message).each(function(k, msg){
						info.find('ul').append('<li>' + msg + '</li>');	
					});
				}
				info.slideDown();
			});
		});
	});
</script>
<form id="frmSetBackup" name='frmSetBackup' method="post" action="/corp/corp/setBackup" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="START_DATE" />
	<input type="hidden" name="END_DATE" />
</form>

<div class="modal fade" id="modal_backup" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 백업</h4>
			</div>
			<div class='edit_alert'> 
				<ul></ul>
			</div>
			<div class="alert alert-info alert-dismissible main_alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>
				<i class="icon fa fa-info"></i> 백업 항목</h4>
				<p> 아래 내역이 백업서버로 이동된 후 삭제됩니다</p>
				<ul class='info'>
					<li>매입정보</li>
					<li>매출정보</li>
					<li>비용/전표정보</li>
					<li>미수정보</li>
					<li>미지급정보</li>
					<li>결산정보</li>
				</ul>
			</div>
			<div class="box-body" style="padding:10px 20px 5px 20px;">
				<div class="col-md-12 col-xs-12">
					<div id="reportrange2" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
						<label>기간지정</label> 
						<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						<span></span> <b class="caret"></b>
						<input type='hidden' name="start_date" />
						<input type='hidden' name="end_date" />
					</div>
				</div>
			</div>

			<div class="modal-footer">			
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
				<button type="button" class="btn btn-success pull-right" id="btnExcuteBackup">
					<span class="glyphicon glyphicon-off"></span> 실행
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_Excel" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 백업</h4>
			</div>
			<div class="alert alert-info alert-dismissible main_alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>
				<i class="icon fa fa-info"></i> 엑셀변환 항목</h4>
				<p> 아래 내역이 엑셀파일로 변환 됩니다</p>
				<ul class='info'>
					<li>매입정보</li>
					<li>매출정보</li>
					<li>비용/전표정보</li>
					<li>미수정보</li>
					<li>미지급정보</li>
					<li>결산정보</li>
				</ul>
			</div>
			<div class="box-body" style="padding:10px 20px 5px 20px;">
				<div class="col-md-12 col-xs-12">
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
						<label>기간지정</label> 
						<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						<span></span> <b class="caret"></b>
						<input type='hidden' name="start_date" />
						<input type='hidden' name="end_date" />
					</div>
				</div>
			</div>

			<div class="modal-footer">			
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
				<button type="button" class="btn btn-success pull-right" id="btnGetBackupExcel">
					<span class="glyphicon glyphicon-off"></span> 실행
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_copy_corp" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 회사 복사</h4>
			</div>
			<div class="alert alert-info alert-dismissible main_alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>
				<i class="icon fa fa-info"></i> 복사항목</h4>
				<ul class='info'>
					<li>회사정보</li>
					<li>사용자정보 1명(회사아이디랑 동일)</li>
					<li>그룹코드</li>
					<li>거래처 그룹</li>
					<li>거래처 코드</li>
					<li>비용계정그룹</li>
					<li>비용계정</li>
					<li>비용전표</li>
				</ul>
			</div>

			<div class="modal-body" style="padding:10px 20px 5px 20px;">
				<div class='edit_alert' ><ul></ul></div>
				<div class="form-group">
					<label for="modal_corp_mk"><i class='fa fa-circle-o text-aqua'></i></span>회사 아이디</label>
					<input type="text" class="form-control" id="modal_corp_mk" readonly="readonly" />

					<label for="modal_corp_nm"><i class='fa fa-circle-o text-aqua'></i></span>회사명</label>
					<input type="text" class="form-control" id="modal_corp_nm" readonly="readonly" />
				</div>
				<div class="form-group">
					<label for="modal_corp_new_mk"><i class='fa fa-circle-o text-aqua'></i></span>새 회사아이디</label>
					<input type="text" class="form-control" id="modal_corp_new_mk" />

					<label for="modal_corp_new_nm"><i class='fa fa-circle-o text-aqua'></i></span>새 회사명</label>
					<input type="text" class="form-control" id="modal_corp_new_nm"  />
				</div>
				<div class="form-group">
					<label for="modal_corp_new_rpst"><i class='fa fa-circle-o text-aqua'></i></span> 새 대표자명</label>
					<input type="text" class="form-control" id="modal_corp_new_rpst"  />

					<label for="modal_corp_new_etpr_no"><i class='fa fa-circle-o text-aqua'></i></span>새 사업자번호</label>
					<input type="text" class="form-control" id="modal_corp_new_etpr_no"  />
				</div>
			
			</div>
			<div class="modal-footer">			
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기 </button>
				<button type="button" class="btn btn-success pull-right" id="btnCopyCorpInfo">
					<span class="glyphicon glyphicon-off"></span> 복사생성
				</button>
			</div>
		</div>
	</div>
</div>


@stop


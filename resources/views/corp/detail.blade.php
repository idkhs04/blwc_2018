@extends('layouts.main')
@section('title','업체정보')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:4px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}

	#tblDetail > tbody > tr > td{
		width:25%;
	}
	#tblDetail > tbody > tr > th{
		width:10%;
		background-color: khaki;
		text-align:right;
	}

</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />

			<div class="col-md-4 col-xs-12">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
					@role('sysAdmin')
					<button type="button" class="btn btn-success" id="btnAddCorp" ><i class="fa fa-plus"></i> 추가</button>
					@endrole
					<button type="button" class="btn btn-success" id="btnUpdateCorp" ><i class="fa fa-edit"></i> 수정</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
			<div class="col-md-12 col-xs-12">
				<table id="tblDetail" cellspacing="0" width="100%" class="table table-bordered table-hover display nowrap dataTable">
					<tbody>
						<tr>
							<th>업체부호</th>
							<td>{{$corp_info->CORP_MK}}</td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<th>상호</th>
							<td>{{$corp_info->FRNM}}</td>
							<th>대표자</th>
							<td>{{$corp_info->RPST}}</td>
						</tr>
						<tr>
							<th>사업자등록번호</th>
							<td>{{$corp_info->ETPR_NO}}</td>
							<th>업체구분</th>
							<td>{{$corp_info->CORP_DIV_NM}}</td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td>{{$corp_info->PHONE_NO}}</td>
							<th>종목</th>
							<td>{{$corp_info->UPJONG}}</td>
						</tr>
						<tr>
							<th>팩스</th>
							<td>{{$corp_info->FAX}}</td>
							<th>업태</th>
							<td>{{$corp_info->UPTE}}</td>
						</tr>
						<tr>
							<th>연락처</th>
							<td>{{$corp_info->HP_NO}}</td>
							<th>지역코드</th>
							<td>{{$corp_info->AREA_CD}}</td>
						</tr>
						<tr>
							<th>우편번호</th>
							<td>{{$corp_info->POST_NO}}</td>
							<th>가입일</th>
							<td>{{$corp_info->REG_DATE}}</td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3">{{$corp_info->ADDR1}}</td>
						</tr>
						<tr>
							<th>상세주소</th>
							<td colspan="3">{{$corp_info->ADDR2}}</td>
						</tr>
						<tr>
							<th>이메일</th>
							<td>{{$corp_info->RPST_EMAIL}}</td>
							<th>홈페이지</th>
							<td>{{$corp_info->HOMEPAGE}}</td>
						</tr>
						<tr>
							<th>은행명</th>
							<td colspan="3">{{$corp_info->BANK_NM}}</td>
						</tr>
						<tr>
							<th>예금주</th>
							<td>{{$corp_info->RPST}}</td>
							<th>계좌번호</th>
							<td>{{$corp_info->ACCOUNT_NO}}</td>
						</tr>
						<tr>
							<th>소개</th>
							<td colspan="3">{{$corp_info->INTRO}}</td>
						</tr>
						<tr>
							<th>사용여부</th>
							<td>{{$corp_info->ACTIVE1}}</td>
							<th>비조합원구분</th>
							<td>{{$corp_info->APP_DIV1}}</td>
						</tr>
						<tr>
							<th>전자세금계산서<br/>사용여부</th>
							<td>{{$corp_info->TAX_USE_YN1}}</td>
							<th>계산서 식별 ID / PW</th>
							<td>
								{{$corp_info->TAX_EDI_ID}}
								<span> / </span>
								{{$corp_info->TAX_EDI_PASS}}
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {

		// 목록바로가기
		$("#btnList").click(function(){
			location.href="/corp/corp/index?page=1";
		});
		// 추가 바로가기

		$("#btnAddCorp").click(function(){
			location.href="/corp/corp/insert";
		});

		// 수정바로가기
		$("#btnUpdateCorp").on("click",function(){
			window.location.href="/corp/cust_grp_info/update/{{$corp_info->CORP_MK}}";
		});
	});
</script>
@stop
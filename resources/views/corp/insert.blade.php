@extends('layouts.main')

@section('title','업체 추가')

@section('content')

<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>

<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	.dataTables_filter{ display:none;}

	#tblDetail > tbody > tr > td > textarea{
		width:100%;
		height:100px;
	}

	#tblDetail > tbody > tr > td{
		width:25%;
	}
	#tblDetail > tbody > tr > th{
		width:10%;
		background-color: khaki;
		text-align:right;
		padding-right:5px;
	}
	.form-control{ height:23px;padding-top:0px;}
	.center{ text-align:center;}
	table.dataTable tbody .btn {
		padding: 1px 12px;
		vertical-align:baseline;
		margin-top:0;
		margin-left:2px;
	}
</style>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-1 col-xs-2">
				<div class="btn-group">
					<button type="button" class="btn btn-success" id="btnList"><i class="fa fa-list"></i> 목록</button>
				</div>
			</div>
		</div>
	</div>
</div>
<form action="/corp/cust_grp_info/insert" method="post" name="CorpUpdateForm" id="CorpUpdateForm" >
	{{csrf_field()}}
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
				<input type="hidden" name="page" value="{{ app('request')->input('page') == '' ? 1 : app('request')->input('page') }}" />
				<div class="col-md-12 col-xs-12">
					<table id="tblDetail" class="table table-bordered table-hover display nowrap dataTable" cellspacing="0" width="100%" >
						<tbody>
							<tr>
								<th>업체부호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="CORP_MK" name="CORP_MK" value="{{ old('CORP_MK') }}" />
										@if ($errors->has('CORP_MK'))
											<div class="help-block">
												<strong>{{ $errors->first('CORP_MK') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>상호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="FRNM" name="FRNM" value="{{ old('FRNM') }}"/>
										@if ($errors->has('FRNM'))
											<div class="help-block">
												<strong>{{ $errors->first('FRNM') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>대표자</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="RPST" name="RPST" value="{{ old('RPST') }}"/>
										@if ($errors->has('RPST'))
											<div class="help-block">
												<strong>{{ $errors->first('RPST') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>사업자등록번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ETPR_NO" name="ETPR_NO" value="{{ old('ETPR_NO') }}"/>
										@if ($errors->has('ETPR_NO'))
											<div class="help-block">
												<strong>{{ $errors->first('ETPR_NO') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>업체구분</th>
								<td>
									<div class="col-xs-12">
										<select name="CORP_DIV" class="form-control" id="CORP_DIV" name="CORP_DIV" value="{{ old('CORP_DIV') }}">
											<option value="A">도매업체</option>
											<option value="J">조합</option>
											<option value="P">생산자</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<th>전화번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="PHONE_NO" name="PHONE_NO" value="{{ old('PHONE_NO') }}"/>
										@if ($errors->has('PHONE_NO'))
											<div class="help-block">
												<strong>{{ $errors->first('PHONE_NO') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>종목</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="UPJONG" name="UPJONG" value="{{ old('UPJONG') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>팩스</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="FAX" name="FAX" value="{{ old('FAX') }}"/>
									</div>
								</td>
								<th>업태</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="UPTE" name="UPTE" value="{{ old('UPTE') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>연락처</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="HP_NO" name="HP_NO" value="{{ old('HP_NO') }}"/>
									</div>
								</td>
								<th>지역코드</th>
								<td>
									<div class="col-xs-6">
										<input type="text" class="form-control" id="AREA_CD" placeholder="" name="AREA_CD" value="{{ old('AREA_CD') }}" readonly="readonly" style="float:left;"/>
										<button type="button" name="btnSearchAreaCd" id="btnSearchAreaCd" class="btn btn-info">
											<i class="fa fa-search"></i>
										</button>
									</div>
									<div class="col-xs-6">
										<input type="text" class="form-control" id="AREA_NM" placeholder="" name="AREA_NM" value="{{ old('AREA_NM') }}" readonly="readonly"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>우편번호</th>
								<td>
									<div class="col-xs-8">
										<input type="text" class="form-control" id="POST_NO" name="POST_NO" value="{{ old('POST_NO') }}" readonly="readonly"  style="float:left;" />
										<button type="button" name="searchPisMk" id="btnSearchPisMk" class="btn btn-info" onclick="sample6_execDaumPostcode()" value="우편번호 찾기">우</button>
									</div>
								</td>
								<th>가입일</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="REG_DATE" name="REG_DATE" value="{{ old('REG_DATE') }}" readonly="readonly" />
										@if ($errors->has('REG_DATE'))
											<div class="help-block">
												<strong>{{ $errors->first('REG_DATE') }}</strong>
											</div>
										@endif
									</div>
								</td>
							</tr>
							<tr>
								<th>주소</th>
								<td colspan="3">
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ADDR1" name="ADDR1" value="{{ old('ADDR1') }}" readonly="readonly"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>상세주소</th>
								<td colspan="3">
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ADDR2" name="ADDR2" value="{{ old('ADDR2') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>이메일</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="RPST_EMAIL" name="RPST_EMAIL" value="{{ old('RPST_EMAIL') }}"/>
										@if ($errors->has('RPST_EMAIL'))
											<div class="help-block">
												<strong>{{ $errors->first('RPST_EMAIL') }}</strong>
											</div>
										@endif
									</div>
								</td>
								<th>홈페이지</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="HOMEPAGE" name="HOMEPAGE" value="{{ old('HOMEPAGE') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>은행명</th>
								<td colspan="3">
									<div class="col-xs-12">
										<input type="text" class="form-control" id="BANK_NM" name="BANK_NM" value="{{ old('BANK_NM') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>예금주</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="SAVER" name="SAVER" value="{{ old('SAVER') }}"/>
									</div>
								</td>
								<th>계좌번호</th>
								<td>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="ACCOUNT_NO" name="ACCOUNT_NO" value="{{ old('ACCOUNT_NO') }}"/>
									</div>
								</td>
							</tr>
							<tr>
								<th>소개</th>
								<td colspan="3">
									<div class="col-xs-12">
										<textarea id="INTRO" name="INTRO" style="width:100%;" >{{ old('INTRO') }}</textarea>
									</div>
								</td>
							</tr>
							<tr>
								<th>활성화</th>
								<td>
									<div class="col-xs-12">
										<input type="checkbox" class='chkQuery' id="ACTIVE" name="ACTIVE" value="1" checked/>활성
									</div>
								</td>
								<th>비조합원구분</th>
								<td>
									<div class="col-xs-12">
										<input type="checkbox" class='chkQuery' id="APP_DIV" name="APP_DIV" value="1" checked/>조합원
									</div>
								</td>
							</tr>
							<tr>
								<th>전자세금계산서<br/>사용여부</th>
								<td>
									<div class="col-xs-12">
										<input type="checkbox" class='chkQuery' id="TAX_USE_Y" name="TAX_USE_YN" value="Y" checked /> 사용시 체크
									</div>
								</td>
								<th>계산서 식별 ID / PW</th>
								<td>
									<div class="col-xs-6">
										<input type="text" class="form-control" id="TAX_EDI_ID" name="TAX_EDI_ID" placeholder="송수신 식별자 아이디" value="{{ old('TAX_EDI_ID') }}"/>
										<div class="help-block">
											<strong>{{ $errors->first('TAX_EDI_ID') }}</strong>
										</div>
									</div>
									<div class="col-xs-6">
										<input type="text" class="form-control" id="TAX_EDI_PASS" name="TAX_EDI_PASS" placeholder="송수신 식별자 비밀번호" value="{{old('TAX_EDI_PASS') }}"/>

									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="col-xs-12 center">
						<button type="button" class="btn btn-danger" id="cancel">
							<span class="glyphicon glyphicon-remove"></span> 닫기 
						</button>

						<button type="submit" class="btn btn-success " id="btnSave">
							<span class="glyphicon glyphicon-off"></span> 저장
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	$(function () {

		// 목록
		$("#btnList").click(function(){
			location.href="/corp/cust_grp_info/index?page=1";
		});

		var start = moment().subtract(1500, 'days');
		var end = moment();
		
		$("#REG_DATE").val(end.format('YYYY-MM-DD'));

		// 활성화 및 비조합원 구분 
		$(".chkQuery").is(":checked") ? "1" : "0";
		$(".chkQuery").click(function(){
			var value = $(this).is(":checked") ? "1" : "0";
			$(this).val(value);
		});

		// 가입일
		$("#REG_DATE").daterangepicker({
			format: 'YYYY-MM-DD',
			autoUpdateInput: false,
			singleDatePicker: true,
				locale: {
					daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
					monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10일', '11월', '12월'],
				}
		});


		//지역검색 팝업
		$("#btnSearchAreaCd").click(function(){ 
			$('#modal_corpEdit').modal({show:true}); 
				var pTable = $('#tableAreaCdList').DataTable({
					processing: true,
					retrieve: true,
					serverSide: true,
					iDisplayLength: 25,		// 기본 10개 조회 설정
					bLengthChange: false,
					ajax: {
						url: "/custcd/cust_grp_info/getAreaCd",
						data:function(d){
							d.textSearch = $("input[name='textSearchAreaCdEdit']").val();
						}
					},
					columns: [
						{ data: 'AREA_CD', name: 'AREA_CD' },
						{ data: 'AREA_NM',  name: 'AREA_NM' },
						{
							data:   "AREA_CD",
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return "<button class='btn btn-success btn-block' ><span class='glyphicon glyphicon-on'></span> 선택</button>";
								}
								return data;
							},
							className: "dt-body-center"
						},
					],
					"searching": true,
					"paging": true,
					"autoWidth": true,
					"oLanguage": {
						"sLengthMenu": "조회수 _MENU_ ",
						"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
						"sProcessing": "현재 조회 중입니다",
						"sEmptyTable": "조회된 데이터가 없습니다",
						"sZeroRecords": "조회된 데이터가 없습니다",
						"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
						"oPaginate": {
							"sFirst": "처음",
							"sLast": "끝",
							"sNext": "다음",
							"sPrevious": "이전"
						}
					}
				});

			$('#tableAreaCdList tbody').on( 'click', 'button', function () {
				var data = pTable.row( $(this).parents('tr') ).data(); 
				$("#AREA_NM").val(data.AREA_NM);
				$("#AREA_CD").val(data.AREA_CD);
				$("input[name='textSearchAreaCdEdit']").val('');
				$('#modal_corpEdit_close').click();
			});

			$("input[name='textSearchAreaCdEdit']").on("keyup", function(){
				pTable.search($(this).val()).draw();
			});
				//지역검색 팝업 끝
		});

		
		

		//취소버튼
		$("#cancel").on("click",function(){
			window.location.href="/corp/cust_grp_info/index";
		});
	});
</script>

<script>
    function sample6_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = ''; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    fullAddr = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    fullAddr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
                if(data.userSelectedType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }
				
				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('POST_NO').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('ADDR1').value = fullAddr;
				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('ADDR2').focus();
            }
        }).open();
    }
</script>

<!-- 지역 검색 -->
<div class="modal fade" id="modal_corpEdit" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header" style="padding:10px 15px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-lock"></span> 지역코드 조회</h4>
			</div>
			<div class="modal-body" style="padding:10px 15px;">
				<div class="form-group SearchAreaCdEdit">
					<div class="box box-primary">
						<div class="box-header">
							<label for="textSearchAreaCdEdit"><span class="glyphicon glyphicon-user" style="float:left;"></span> 지역명</label>
							<input type="text" name="textSearchAreaCdEdit" id="textSearchAreaCdEdit" />
						</div>
						<div class="box-body">
							<table id="tableAreaCdList" class="table table-bordered table-hover display nowrap" summary="재역코드 목록" width="100%">
								<caption>지역코드 선택</caption>
								<thead>
									<tr>
										<th scope="col" class="name">지역코드</th>
										<th scope="col" class="name">지역명</th>
										<th scope="col" class="name">선택</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal" id="modal_corpEdit_close">
				<span class="glyphicon glyphicon-remove"></span> 닫기</button>
			</div>
		</div>
	</div>
</div>
@stop
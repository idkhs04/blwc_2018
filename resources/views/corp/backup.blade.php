@extends('layouts.main')
@section('class','회원관리')
@section('title','백업관리')
@section('content')
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="/static/js/jquery/jquery.js"></script>
<script src="/static/js/jquery-ui/jquery-ui.js"></script>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<style>
	input.cis-text {
		height: 34px;
		margin: 0px;
		vertical-align: top;
		padding-top: 0;
		margin-top: 0px;
	}
	.btn-info{ margin-top:0px;}
	.btnSearch{ margin-top:0;}
	#tblList_filter{ display:none;}
	ul.info li{ display:inline-block; width:45%;}

	li > span.darkblue{
		width: 106px;
		display: inline-block;
		text-align: right;
		font-weight:bold;
		color:darkblue;
	}

	.edit_alert > ul {
		padding:10px 0 0 30px;
	}
</style>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="col-md-3 col-xs-12">
				<div class="btn-group">

					<button type="button" class="btn btn-success" id="btnGoCorpList" ><i class="fa fa-list"></i> 회사정보 이동</button>
					@role('admin')
					<button type="button" class="btn btn-info" id="btnSetBackup" ><i class="fa fa-plus-circle"></i> 백업하기</button>
					@endrole
				</div>
			</div>
			
			<div class="col-md-4 col-xs-12">
				<div id="reportrange" class="pull-right" style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<label>백업일자</label>
					<span></span> <b class="caret"></b>
					<input type='hidden' name="start_date" />
					<input type='hidden' name="end_date" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<div class="edit_alert"><ul></ul></div>
			<table id="tblList" class="table table-bordered table-hover display nowrap" summary="업체정보목록" width="100%">
				<caption>백업정보목록</caption>
				<thead>
					<tr>
						<th width="80px">백업일자</th>
						<th width="80px">백업접속</th>
						<th width="80px">시작일자</th>
						<th width="80px">종료일자</th>
						<th width="250px">비고</th>
						<th width="auto"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="/static/js/bootstrap/bootstrap.js"></script>
<script>
	$(function () {
	
		var start = moment().subtract(1, 'months');
		var end = moment();
		
		// 초기값 세팅
		function cbSetDate(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("#reportrange input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("#reportrange input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 백업일자 : 검색
		$("#reportrange").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",		
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate);
		cbSetDate(start, end);

		function cbSetDate2(start, end) {
			$('#reportrange2').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$("#repo_div input[name='start_date']").val(start.format('YYYY-MM-DD')).trigger('change');
			$("#repo_div input[name='end_date']").val(end.format('YYYY-MM-DD')).trigger('change');
		}

		// 팝업내 일자 지정
		$("#reportrange2").daterangepicker({
			startDate: start,
			endDate: end,
			format: 'YYYY-MM-DD',
			showDropdowns: true,
			buttonClasses: "btn btn-sm",
			applyClass: "btn-success",
			cancelClass: "btn-default",		
			opens:'left',
			locale: {
				daysOfWeek: ['일', '월', '화', '수', '목', '금','토'],
				monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			}
		}, cbSetDate2);
		cbSetDate2(start, end);


		var oTable = $('#tblList').DataTable({
			processing: true,
			serverSide: true,
			iDisplayLength: 15,		// 기본15개 조회 설정
			bLengthChange: false,
			bInfo : false,
			ajax: {
				url: "/corp/corp/getBackList",
				data:function(d){
					d.START_DATE	= $("#reportrange input[name='start_date']").val();
					d.END_DATE		= $("#reportrange input[name='end_date']").val();
					d.CORP_MK		= "{{Request::segment(4)}}";
				}
			},
			columns: [
				{ data: 'BK_DATE', name: 'BK.BK_DATE' },
				{ 
					data: null,
					render: function ( data, type, row ) {
						if ( type === 'display' ) {
							return " <button class='btn btn-info btnGoSetBack' ><i class='fa fa-plus-circle'></i> 백업조회 </button>" ;
						}
						return data;
					},
					orderable:  false,
				},
				{ data: 'START_DATE', name: 'START_DATE' },
				{ data: 'END_DATE', name: 'END_DATE' },
				{ data: 'REMARK', name: 'REMARK' },
				{ 
					"className":      'dt-body-right',
					"orderable":      false,
					"data":           null,
					"defaultContent": ''
				},
			],
			"searching": true,
			"paging": true,
			"autoWidth": true,
			"oLanguage": {
				"sLengthMenu": "조회수 _MENU_ ",
				"sLoadingRecords" : "조회 중...잠시만 기다려주세요",
				"sProcessing": "현재 조회 중입니다",
				"sEmptyTable": "조회된 데이터가 없습니다",
				"sZeroRecords": "조회된 데이터가 없습니다",
				"sInfo": "전체 _TOTAL_ 건 ( _START_ ~ _END_ )",
				"oPaginate": {
					"sFirst": "처음",
					"sLast": "끝",
					"sNext": "다음",
					"sPrevious": "이전"
				}
			}
		});

		$("#reportrange input[name='start_date']").on("change", function(){
			oTable.draw();
		});

		$("#reportrange input[name='end_date']").on("change", function(){
			oTable.draw();
		});
		
		// 백업접속
		$('#tblList tbody').on( 'click', '.btnGoSetBack', function () {			
			var data = oTable.row( $(this).parents('tr') ).data();
			location.href="/config/setLoginByBackup/"+data.CORP_MK ;
			//location.href="/config/setLoginByBackup/"+data.BK_CORP_MK + "_" + data.BK_DATE;
		});
		
		// 회사정보 이동
		$("#btnGoCorpList").click(function(){
			location.href="/corp/corp/index";
		});

		// 백업 및 삭제 버튼 클릭 (실행 화면 띄우기)
		$("#btnSetBackup").click(function(){	
			$("#modal_backup .edit_alert > ul").empty();
			$("#modal_backup").modal({show:true});
		});

		$("#btnExcuteBackup").click(function(){

			
			var start_date	= $("#repo_div input[name='start_date']").val();
			var end_date	= $("#repo_div input[name='end_date']").val();
			var remark		= $("input[name='remark']").val();
			
			if( confirm("선택하신 백업기간은 " + start_date + " ~ " +end_date + " 까지 입니다.\n백업 후 해당기간의 데이터는 다시 업무영역으로 복구할 수 없으므로\n신중히 확인 후 진행 하시기 바랍니다. ") ) {

				$.post('/corp/corp/chkCorpBackup', {
					_token			: '{{ csrf_token() }}',
					START_DATE		: start_date ,
					END_DATE		: end_date ,
					REMARK			: remark,
				}).success(function(xhr){
					var data = jQuery.parseJSON(JSON.stringify(xhr));

					if (data.result == "success") {	
						$("#modal_backup").modal("hide");
						oTable.draw() ;
					}
					
				}).error(function(xhr,status, response) {
					var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
					var info = $('#modal_backup .edit_alert');
					info.hide().find('ul').empty();
					
					if( error.result == "vali_fail"  ){
						for(var k in error.message){
							if(error.message.hasOwnProperty(k)){
								error.message[k].forEach(function(val){
									info.find('ul').append('<li>' + val + '</li>');
									return;
								});
							}
						}
					}
					if( error.result == 'fail' ){
						$(error.message).each(function(k, msg){
							info.find('ul').append('<li>' + msg + '</li>');	
						});
					}
					
					if( error.result=="fail_none" ){
						info.find('ul').append('<li>' + error.message + '</li>');	
					}
					info.slideDown();
				});
			}
		});
	});
</script>
<form id="frmSetBackup" name='frmSetBackup' method="post" action="/corp/corp/setBackup" >
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input type="hidden" name="START_DATE" />
	<input type="hidden" name="END_DATE" />
</form>

<div class="modal fade" id="modal_backup" role="dialog">
	<div class="modal-dialog modal-md">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:15px 20px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><span class="glyphicon glyphicon-th-large"></span> 백업</h4>
			</div>
			<div class='edit_alert'> 
				<ul></ul>
			</div>
			<div class="alert alert-info alert-dismissible main_alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>
				<i class="icon fa fa-info"></i> 백업 항목</h4>
				<p class='red'> 백업은 하루에 한번만 가능합니다</p>
				<p> 아래 내역이 백업서버로 이동된 후 삭제됩니다</p>
				
				<ul class='info'>
					<li><span class='darkblue'>매입정보		: </span> 입고일기준</li>
					<li><span class='darkblue'>매출정보		: </span> 판매일기준</li>
					<li><span class='darkblue'>비용/전표정보	: </span> 작성일기준</li>
					<li><span class='darkblue'>미수정보		: </span> 판매일기준</li>
					<li><span class='darkblue'>미지급정보		: </span> 입고일기준</li>
					<li><span class='darkblue'>결산정보		: </span> 일일 기준 </li>
				</ul>
			</div>
			<div class="box-body" style="padding:10px 20px 5px 20px;">
				<div class="col-md-12 col-xs-12">
					<label class='col-sm-4 control-label'> <i class='fa fa-check-circle text-red'></i> 기간지정</label> 
					<div class="col-sm-8" id="repo_div">
						<input type="text" class="form-control" id="reportrange2" />
						<input type='hidden' name="start_date" />
						<input type='hidden' name="end_date" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12">
					<label class='col-sm-4 control-label'><i class='fa fa-check-circle text-aqua'></i> 비고</label>
					<div class='col-sm-8'>
						<input type='text' name="remark" class="form-control" />
					</div>
				</div>
			</div>

			<div class="modal-footer">			
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span> 닫기
				</button>
				<button type="button" class="btn btn-success pull-right" id="btnExcuteBackup">
					<span class="glyphicon glyphicon-off"></span> 실행
				</button>
			</div>
		</div>
	</div>
</div>

@stop


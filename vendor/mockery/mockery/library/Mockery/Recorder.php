@extends('layouts.main')

@section('title','입고수정')

@section('content')

<style>
	#tblCustList_filter { display:none;}
</style>
<div>
	<!-- board_search-->	
	<section >
		@include('include.search_header', ['stock' => $stock])
	</section>
	<!-- List_boardType01 -->
	<section >
		<div class="col-sm-12">
			<div class="box box-info">
	            <div class="box-header with-border">
					<h3 class="box-title">@yield('title') 등록</h3>
	            </div>
				
				@if (count($errors) > 0)

					<!-- Form Error List -->
						<div class="alert alert-danger">
							<strong>Whoops! Something went wrong!</strong>

							<br><br>

							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
				@endif

			    <!-- /.box-header -->
	            <!-- form start -->
				{{  Form::open(['url'=>'/stock/'.$stock."/". $mode = Request::segment(3) == "detailEdit" ? "setUpdateStock": "Insert", 'method' => 'POST', 'enctype' => "multipart/form-data", 'class'=>'form-horizontal', 'id'=>"frmEdit" ]) }}
				<input type="hidden" name="mode" value="{{ Request::segment(3)}}" />
				<input type="hidden" name="page" value="{{ app('request')->input('page') == "" ? 1 : app('request')->input('page') }}" />
				<input type="hidden" name="error" value="{{ count($errors)}}" />
				<input type="hidden" name="SEQ" value="{{ $model->SEQ }}" />
				<input type="hidden" name="PIS_MK" value="{{$model->PIS_MK}}" 
					<div class="box-body">
						<div class="form-group">
							<label for="SUJO_NO" class="col-sm-2 control-label">수조번호</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="SUJO_NO" placeholder="" name="SUJO_NO" value="{{$sujo_no}}" readonly="readonly" />
							</div>
						</div>
						<div class="form-group">
							<label for="PIS_NM" class="col-sm-2 control-label">어종</label>
							<div class="col-sm-2">
								
						 		<input type="text" class="form-control " id="CUST_GRP_NM" placeholder="" name="CUST_GRP_NM" value="{{$model->PIS_NM}}" readonly="readonly" />
								@if ($errors->has('PIS_NM')) 
									<p class="help-block">{{ $errors->first('PIS_NM') }}</p> 
								@endif
							</div>
							<div class="col-sm-2">
								
								<input type="text" class="form-control" id="SIZES" placeholder="" name="SIZES" value="{{$model->SIZES}}" readonly="readonly" >
								@if ($errors->has('SIZES')) <p class="help-block">{{ $errors->first('SIZES') }}</p> @endif
							</div>
						</div>
						<div class="form-group">
							<label for="ORIGIN_CD" class="col-sm-2 control-label">원산지</label>
							<div class="col-sm-2">
								{!! Form::select('modelOrgin',  (['' => '== 선 택 =='] + $modelOrgin ),  $model->ORIGIN_CD, ['class' => 'form-control']) !!}
								@if ($errors->has('ORIGIN_NM')) <p class="help-block">{{ $errors->first('ORIGIN_NM') }}</p> @endif
							</div>
						</div>
						<div class="form-group">
			
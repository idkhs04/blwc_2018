// 숫자 타입에서 쓸 수 있도록 format() 함수 추가
function fnParseformat ( num ){
	if(num == 0) return 0;
 
	var reg = /(^[+-]?\d+)(\d{3})/;
	var n = (num + '');
 
	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');
 
	return n;
};

// 테이블 rows당 체크박스 체크 여부 확인
// 테이블명, 버튼항목 명
function chkInputTble($tblNm, $word){

	var arr_code = [];
	$('#' + $tblNm + " tbody > tr > td > input[type=checkbox]").each(function() {
		if ($(this).is(":checked")) {
			arr_code.push($(this).val());
		}
	});

	$(".content > .alert").remove();
	if( arr_code.length == 0){
		$(".content").prepend( getAlert('warning', '경고', $word + " 할 항목을 선택해주세요") ).show();
		return false;
	}
	if( arr_code.length > 1){
		$(".content").prepend( getAlert('warning', '경고', $word + " 할 항목은 한개만 선택해주세요") ).show();
		return false;
	}
	if( arr_code.length == 1){
		return true;
	}else{
		return false;
	}
}

// 숫자 타입에서 천단위 , 추가
Number.prototype.format = function(){
	if(this==0) return 0;
 
	var reg = /(^[+-]?\d+)(\d{3})/;
	var n = (this + '');
 
	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');
 
	return n;
};

function CbSetInitModalClose(){

	var info = $('.edit_alert');
	if( info.length > 0){
		info.hide().find('ul').empty();
	}

	if( $(".modal input[type='radio']").length > 0){
		$(".modal input[type='radio']").prop("checked", false);
	}
	if( $(".modal input[type='checkbox']").length > 0){
		$(".modal input[type='checkbox']").prop("checked", false);
	}

	$(".modal input[type='text']").val("");	
	$(".edit_alert ul, .edit_valert").empty();
	
}

// 숫자/콤만값만 입력
$('body').on('keydown, focusin, focusout','.numeric',function(e){
	var val = $(this).val().comma();
	var re = /[^0-9\.\,\-]/gi;
	$(this).val( val.replace(re, '') );
});

// 영문만 입력되도록
$(".cis-lang-en-only").css('ime-mode', 'disabled');
// 한글모드 우선
$(".cis-lang-ko").css('ime-mode', 'active');


$(".cis-lang-ko").keyup(function(){
	/*
	$(this).attr("data-tran", $(this).val() );
	var trans = $(this).attr("data-tran");
	$(this).val( engTypeToKor(trans) );
	*/
});




// 영문모드 우선
$(".cis-lang-en").css('ime-mode', 'active');


String.prototype.comma = function() {

	tmp = this.split('.');
	var str = new Array();
	var v = tmp[0].replace(/,/gi,'');
	for(var i=0; i<=v.length; i++) {
		str[str.length] = v.charAt(v.length-i);
		if(i%3==0 && i!=0 && i!=v.length) {
			str[str.length] = '.';
		}
	}
	str = str.reverse().join('').replace(/\./gi,',');
	return (tmp.length==2) ? str + '.' + tmp[1] : str;
}

// 콤마 제거
function removeCommas(str) {
    return(str.replace(/,/g,''));
}

function getPopUp(popUrl, width, height){
	
	//var popUrl = "/uncl/uncl/pdfDetail";	//팝업창에 출력될 페이지 URL
	var popOption = "width=" + width + ", height=" + height + ", resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
	window.open(popUrl,"",popOption);
}



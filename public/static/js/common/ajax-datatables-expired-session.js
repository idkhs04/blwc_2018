/**
 * Requirements:
 * - jQuery (http://jquery.com/)
 * - DataTables (http://datatables.net/)
 * - BootboxJS (http://bootboxjs.com/)
 * ---------------------------------------------------------------------------
 * Credits to https://gist.github.com/flackend/9517696
 * ---------------------------------------------------------------------------
 * This monitors all AJAX calls that have an error response. If a user's
 * session has expired, then the system will return a 401 status,
 * "Unauthorized", which will trigger this listener and so prompt the user if
 * they'd like to be redirected to the login page.
 */

$(document).ajaxError(function(event, jqxhr, settings, exception) {
	
	if (jqxhr.responseJSON.message == 'unauth') {

		//alert('로그인 세션이 만료되었습니다');
		//window.location = '/login';
	}
});

$(document).ajaxError(function(event, jqxhr, settings, exception) {
	
	if (jqxhr.responseJSON.message == 'unauth') {

		//alert('로그인 세션이 만료되었습니다');
		//window.location = '/login';
	}
});

// disable datatables error prompt
$.fn.dataTable.ext.errMode = 'none';

function setUseChkSujoDisplay(){
	var config_SujoChk = $(this).is(":checked") ? 'Y' : 'N';
	$.getJSON('/config/setUseChkSujoDisplay', {
		'CONF_CHK_SUJO'	: config_SujoChk,
		_token			: '{{ csrf_token() }}'
	}).success(function(xhr){
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		alert('적용 확인을 위해서 새로고침을 하세요 (F5 버튼 누르세요)');
		
	}).error(function(xhr,status, response) {
	});

}
function setUseConfigSujocust(){
	var config_sujoCust = $(this).is(":checked") ? 'Y' : 'N';
	$.getJSON('/config/setUseConfigSujocust', {
		'SUJO_CUST_YN'	: config_sujoCust,
		_token			: '{{ csrf_token() }}'
	}).success(function(xhr){
		/*
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		if( data.result == "success"){
			if( data.model.SUJO_CUST_YN == "Y"){
				$(".config_qty").css("display", "block");
				$("#config_Qty").prop("checked", true);
			}else{
				$(".config_qty").css("display", "none");
				$("#config_Qty").prop("checked", false);
			}
		}
		*/
		
	}).error(function(xhr,status, response) {
	});
}

function setUseConfigQty(){
	var config_qty = $(this).is(":checked") ? '1' : '0';
	$.getJSON('/config/setConfigQty', {
		'CONFIG_QTY'	: config_qty,
		_token			: '{{ csrf_token() }}'
	}).success(function(xhr){
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		if( data.result == "success"){
			if( data.model.CONF_QTY == "1"){
				$(".config_qty").css("display", "block");
				$("#config_Qty").prop("checked", true);
			}else{
				$(".config_qty").css("display", "none");
				$("#config_Qty").prop("checked", false);
			}
		}
		
	}).error(function(xhr,status, response) {
	});
}

function isConfigQty(){
	return $("#config_Qty").is(":checked") ? true : false ;
}

function getConfig(){

	$.getJSON('/config/getConfig', {
		_token			: '{{ csrf_token() }}'
	}).success(function(xhr){
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		if( data.result == "success"){
			if( data.model.CONF_QTY == "1"){
				$(".config_qty").css("display", "block");
				$("#config_Qty").prop("checked", true);

			}else{
				$(".config_qty").css("display", "none");
				$("#config_Qty").prop("checked", false);
			}
			
			// 수조 거래처 보기 설정
			if( data.model.SUJO_CUST_YN != "N" ){
				$("#config_SujoCust").prop("checked", true);

			}else{
				$("#config_SujoCust").prop("checked", false);
			}

			// 수조 체크 보기 설정
			if( data.model.CONF_CHK_SUJO == "Y" ){
				$("#config_ChkSujo").prop("checked", true);

			}else{
				$("#config_ChkSujo").prop("checked", false);
			}

			// 스킨 받기
			$("body").attr("class", "hold-transition sidebar-mini " + data.model.CONF_SKIN); 

			// 좌측메뉴 토글 기능
			if( data.model.CONF_MENU_TOGGLE == "1"){
				$("#config_collapse").prop("checked", true);
				$("body").addClass("sidebar-collapse"); 
			}else{
				$("#config_collapse").prop("checked", false);
				$("body").removeClass("sidebar-collapse"); 
			}
		}
		
	}).error(function(xhr,status, response) {
		/*
		var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
		var info = $('.edit_alert');
		info.hide().find('ul').empty();
			for(var k in error.message){
				if(error.message.hasOwnProperty(k)){
					error.message[k].forEach(function(val){
						info.find('ul').append('<li>' + val + '</li>');
					});
				}
			}
		info.slideDown();
		*/
		//alert('시스템 설정 에러 : 관리자에게 문의해주세요 ((주)이씨스 김현섭 대리 : 010-9630-1944)');
	});
}

function setSkin(skin){
	
	$.getJSON('/config/setConfigSkin', {
		'CONFIG_SKIN'	: skin,
		_token			: '{{ csrf_token() }}'
	}).success(function(xhr){
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		// 스킨 받기
		$("body").attr("class", "hold-transition sidebar-mini " + data.model.CONF_SKIN); 
		$(".main_alertMsg").html( getAlert('info', '경고', "스킨이 적용되었습니다.") ).show();

	}).error(function(xhr,status, response) {
		$(".main_alertMsg").html( getAlert('warning', '경고', "스킨이 적용실패") ).show();
	});
}

function setMenutoggle( isChecked ){
	
	$.getJSON('/config/setMenutoggle', {
		'CONFIG_TOGGLE' : isChecked,
	}).success(function(xhr){
		var data = jQuery.parseJSON(JSON.stringify(xhr));
		// toggle 설정 받기
		if( data.model.CONF_MENU_TOGGLE == "1"){
			$("body").addClass("sidebar-collapse"); 
		}else{
			$("body").removeClass("sidebar-collapse"); 
		}
		$(".main_alertMsg").html( getAlert('info', '경고', "메뉴설정이 적용되었습니다.") ).show();

	}).error(function(xhr,status, response) {
		$(".main_alertMsg").html( getAlert('warning', '경고', "메뉴설정 적용실패") ).show();
	});
}


function chkSession(msg){

	var data = JSON.parse(msg);

	if(data.message != "undefined"){
		if(data.message == 'unauth'){
			alert('로그인 세션이 만료되었습니다');
			window.location = '/login';
		}
	}
}